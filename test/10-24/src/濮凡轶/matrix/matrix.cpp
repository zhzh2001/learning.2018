#include<bits/stdc++.h>
using namespace std;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}

using namespace std;

const int maxn = 2505;

struct Edge
{
	int from, to, dist;

	inline bool operator < (const Edge& other) const
	{
		return this->dist > other.dist;
	}
} e[maxn * maxn];

bool vis[maxn][maxn];

int m;

int a[maxn][maxn];

int main()
{
	freopen("matrix.in", "r", stdin);
	freopen("matrix.out", "w", stdout);
	int n;
	n = read();
	for(register int i = 1; i <= n; ++i)
	{
		for(register int j = 1; j <= n; ++j)
		{
			a[i][j] = read();
			if(i < j)
			{
				e[++m] = (Edge)
				{
					i, j, a[i][j]
				};
			}
			else if(a[i][j] != a[j][i])
			{
				puts("2");
				printf("%d %d 0", i, j);
				return 0;
			}
			if(i == j && a[i][j])
			{
				puts("1");
				printf("%d 0 0", i);
				return 0;
			}
		}
	}
	sort(e + 1, e + m + 1);
	for(register int i = 1; i <= m; ++i)
	{
		if(i == 1 || !vis[e[i].from][e[i].to])
		{
			int I = e[i].from, J = e[i].to;
			vis[e[i].from][e[i].to] = true;
			for(register int K = 1; K <= n; ++K)
			{
				if(!((e[i].dist == a[I][K] && e[i].dist >= a[J][K]) ||
				   (e[i].dist == a[J][K] && e[i].dist >= a[I][K]) ||
				   (a[I][K] == a[J][K] && a[I][K] >= e[i].dist)))
				{
					puts("3");
					printf("%d %d %d", I, J, K);
					return 0;
				}
				if(e[i].dist == a[I][K])
					vis[K][I] = vis[I][K] = true;
				if(e[i].dist == a[J][K])
					vis[K][J] = vis[J][K] = true;
			}
		}
	}
	puts("0");
	fclose(stdin);
	fclose(stdout);
	return 0;
}

/*
2
0 1
2 3
*/

/*

// 这是一个暴力 

#include <cstdio>
#include <algorithm>

using namespace std;

const int maxn = 505;

int a[maxn][maxn];

int main()
{
	int n;
	scanf("%d", &n);
	for(int i = 1; i <= n; ++i)
	{
		for(int j = 1; j <= n; ++j)
		{
			scanf("%d", &a[i][j]);
			if(i == j && a[i][j])
			{
				puts("1");
				return 0;
			}
		}
	}
	for(int i = 1; i <= n; ++i)
	{
		for(int j = 1; j <= n; ++j)
		{
			if(a[i][j] != a[j][i])
			{
				puts("1");
				return 0;
			}
			for(int k = 1; k <= n; ++k)
			{
				if(a[i][j] > max(a[i][k], a[k][j]))
				{
					puts("1");
					return 0;
				}
			}
		}
	}
	puts("0");
	return 0;
}
*/
