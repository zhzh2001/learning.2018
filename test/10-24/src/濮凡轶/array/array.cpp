// 这是一个暴力

#include <cstdio>
#include <cstring>
#include <bitset>
#include <iostream>
#include <algorithm>

using namespace std;

const int maxn = 1000005;
const int maxm = 18;
const int inf = 0x3f3f3f3f;

char c[maxm][maxn];
int aa[maxn];
int n, m;

inline void getzt(int x, int zt)
{
	zt = !zt;
//	cout << zt;
	for(register int i = 1; i <= m; ++i)
	{
		aa[i] += c[x][i] ^ 48 ^ zt;
//		cout << (c[x][i] ^ 48 ^ zt);
	}
//	cout << endl;
}

int main()
{
	freopen("array.in", "r", stdin);
	freopen("array.out", "w", stdout);
	scanf("%d%d\n", &n, &m);
	for(register int i = 1; i <= n; ++i)
		gets(c[i] + 1);
	int Ans = inf;
	for(register int zt = 0; zt < (1 << n); ++zt)
	{
		memset(aa, 0, sizeof(aa));
//		cout << "zt = " << bitset<5>(zt) << endl;
		for(register int i = 1; i <= n; ++i)
			getzt(i, zt & (1 << (i - 1)));
		int ans = 0;
		for(register int i = 1; i <= m; ++i)
		{
			ans += min(aa[i], n - aa[i]);
//			cout << aa[i] << ' ';
		}
//		cout << endl;
		Ans = min(Ans, ans);
	}
	printf("%d\n", Ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

/*
3 4
0110
1010
0111
*/
