#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 100005;
int n;
int to[N*2],nxt[N*2],head[N],cnt;
void insert(int x,int y){
	to[++cnt] = y; nxt[cnt] = head[x]; head[x] = cnt;
}
bool vis[N],Vis[N*2];
bool flag; int flag2;
int cri[N],numCri,wei[N];
int dep[N],DFN,dfn[N],tree[N],top[N],fa[N],size[N],son[N];
int Left[N],Right[N];
int edg[N],ans;
int rt[N];
int cov[N<<4],lazy[N<<4];
int ls[N<<4],rs[N<<4],Cnt;
int root[N];

void dfs(int x,int fa){
	vis[x] = true;
	for(int i = head[x];i != -1;i = nxt[i])
		if(to[i] != fa){
			if(vis[to[i]]){
				flag2 = to[i];
				flag = true;
				cri[++numCri] = x;
				return;
			}
			dfs(to[i],x);
			if(flag){
				if(flag2) cri[++numCri] = x;
				if(flag2 == x) flag2 = 0;
				return;
			}
		}
}

void dfs1(int x,int f){
	fa[x] = f; size[x] = 1; rt[x] = Cnt;
	for(int i = head[x];i != -1;i = nxt[i])
		if(!vis[to[i]] && to[i] != f){
			edg[to[i]] = i;
			dep[to[i]] = dep[x]+1;
			dfs1(to[i],x);
			size[x] += size[to[i]];
			if(son[x] == 0 || size[son[x]] < size[to[i]])
				son[x] = to[i];
		}
}

void dfs2(int x,int f){
	dfn[x] = ++DFN; tree[DFN] = edg[x];
	top[x] = f;
	if(son[x]) dfs2(son[x],f);
	for(int i = head[x];i != -1;i = nxt[i])
		if(!vis[to[i]] && to[i] != fa[x] && to[i] != son[x])
			dfs2(to[i],to[i]);
}

inline void Down(int num,int l,int r){
	cov[num] = r-l+1-cov[num];
	lazy[num] ^= 1;
}

inline void down(int num,int l,int r){
	if(ls[num] == 0) ls[num] = ++Cnt;
	if(rs[num] == 0) rs[num] = ++Cnt;
	int mid = (l+r)>>1;
	if(lazy[num]){
		Down(ls[num],l,mid);
		Down(rs[num],mid+1,r);
		lazy[num] = 0;
	}
}

inline void update(int num){
	cov[num] = 0;
	if(ls[num]) cov[num] += cov[ls[num]];
	if(rs[num]) cov[num] += cov[rs[num]];
}

void change(int& num,int l,int r,int x,int y){
	if(num == 0) num = ++Cnt;
	if(x <= l && r <= y){
		cov[num] = r-l+1-cov[num];
		lazy[num] ^= 1;
		return;
	}
	int mid = (l+r)>>1;
	down(num,l,r);
	if(x <= mid) change(ls[num],l,mid,x,y);
	if(y > mid) change(rs[num],mid+1,r,x,y);
	update(num);
}

void getlca(int x,int y,int Rt){
	int las = cov[root[Rt]];
	while(top[x] != top[y]){
		if(dep[top[x]] < dep[top[y]]) swap(x,y);
		change(root[Rt],Left[Rt]+1,Right[Rt],dfn[top[x]],dfn[x]);
		x = fa[top[x]];
	}
	if(x != y){
		if(dep[x] < dep[y]) swap(x,y);
		change(root[Rt],Left[Rt]+1,Right[Rt],dfn[y]+1,dfn[x]);
	}
	ans -= cov[root[Rt]]-las;
}

void gettotop(int x,int Rt){
	int las = cov[root[Rt]];
	while(top[x] != Rt){
		change(root[Rt],Left[Rt]+1,Right[Rt],dfn[top[x]],dfn[x]);
		x = fa[top[x]];
	}
	if(x != Rt) change(root[Rt],Left[Rt]+1,Right[Rt],dfn[Rt]+1,dfn[x]);
	ans -= cov[root[Rt]]-las;
}

signed main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n = read(); cnt = -1;
	int Q = read(); ans = n;
	memset(head,-1,sizeof head);
	for(int i = 1;i <= n;++i){
		int x = read(),y = read();
		insert(x,y); insert(y,x);
	}
	dfs(1,1);
//	puts("kira");
	memset(vis,0,sizeof vis);
	for(int i = 1;i <= numCri;++i) vis[cri[i]] = true;
	for(int i = 1;i <= numCri;++i){
		int x = cri[i]; wei[x] = i;
		dep[x] = 0;
		Left[x] = DFN+1;
		Cnt = x;
		dfs1(x,x);
		dfs2(x,x);
		Right[x] = DFN;
	}
//	puts("kira");
	for(int i = 1;i <= numCri;++i){
		int x = cri[i];
		int R = i+1; if(R == numCri+1) R = 1;
		int y = cri[R];
		for(int j = head[x];j != -1;j = nxt[j])
			if(to[j] == y){
				tree[DFN+i] = j;
				break;
			}
	}
	
	Cnt = 0;
	while(Q--){
		int x = read(),y = read();
		if(rt[x] == rt[y]){
			getlca(x,y,rt[x]);
		}else{
			gettotop(x,rt[x]);
			gettotop(y,rt[y]);
			int X = rt[x],Y = rt[y];
			if(wei[X] > wei[Y]) swap(X,Y);
			if(wei[Y]-wei[X] < numCri-wei[Y]+wei[X]){
				int las = cov[root[0]];
				change(root[0],DFN+1,DFN+numCri,DFN+wei[X],DFN+wei[Y]-1);
				int nows = cov[root[0]];
				if(las == numCri) --las;
				if(nows == numCri) --nows;
				ans -= nows-las;
			}else{
				int L = wei[X]-1; if(L == 0) L = numCri;
				int R = wei[X]+1; if(R == numCri+1) R = 1;
				int las = cov[root[0]];
				if(cri[L] < cri[R]){
					if(wei[X] > 1) change(root[0],DFN+1,DFN+numCri,DFN+1,DFN+wei[X]-1);
					change(root[0],DFN+1,DFN+numCri,DFN+wei[Y],DFN+numCri);
				}else change(root[0],DFN+1,DFN+numCri,DFN+wei[X],DFN+wei[Y]-1);
				int nows = cov[root[0]];
				if(las == numCri) --las;
				if(nows == numCri) --nows;
				ans -= nows-las;
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
