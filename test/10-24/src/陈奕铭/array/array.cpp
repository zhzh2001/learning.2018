#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 19,M = 100005;
int n,m;
char s[N][M];
pair<int,int> A1[N],A2[M];
int B1[N],B2[M];
int pos1,pos2;
int ans;
bool vis1[N],vis2[M];
bool flag;

inline void solve1(){
	if(A1[pos1].first*2 < m){
//		if(A1[pos1].first*2 == m && !vis1[A1[pos1].second])
//			vis1[A1[pos1].second] = true;
//		else{ flag = false;	return;}
		flag = false; return;
	}
	ans -= A1[pos1].first*2-m;
	int j = A1[pos1].second; A1[pos1].first = m-A1[pos1].first;
	for(int i = 1;i <= m;++i)
		if(s[j][A2[i].second] == '1'){
			--A2[i].first;
			s[j][A2[i].second] = '0';
		}else{
			++A2[i].first;
			s[j][A2[i].second] = '1';
		}
	sort(A2+1,A2+m+1); pos2 = m; --pos1;
}

inline void solve2(){
	if(A2[pos2].first*2 < n){
//		if(A2[pos2].first == n && !vis2[A2[pos2].second])
//			vis2[A2[pos2].second] = true;
//		else {flag = false;return;}
		flag = false; return;
	}
	ans -= A2[pos2].first*2-n;
	int j = A2[pos2].second; A2[pos2].first = n-A2[pos2].first;
	for(int i = 1;i <= n;++i)
		if(s[A1[i].second][j] == '1'){
			--A1[i].first;
			s[A1[i].second][j] = '0';
		}else{
			++A1[i].first;
			s[A1[i].second][j] = '1';
		}
	sort(A1+1,A1+n+1); pos1 = n; --pos2;
}

signed main(){
	freopen("array.in","r",stdin);
	freopen("array.out","w",stdout);
	n = read(); m = read();
	for(int i = 1;i <= n;++i) A1[i].second = i;
	for(int j = 1;j <= m;++j) A2[j].second = j;
		
	for(int i = 1;i <= n;++i){
		scanf("%s",s[i]+1);
		for(int j = 1;j <= m;++j){
			if(s[i][j] == '1'){
				++A1[i].first,++A2[j].first;
				++B1[i]; ++B2[j];
				++ans;
			}
		}
	}
	sort(A1+1,A1+n+1);
	sort(A2+1,A2+m+1);
	pos1 = n; pos2 = m; flag = true;
	while(flag){
		if(pos1 >= 1 && pos2 >= 1){
			if(A1[pos1].first*2-m > A2[pos2].first*2-n){
				solve1();
			}else{
				solve2();
			}
		}else if(pos1 >= 1){
			solve1();
		}else if(pos2 >= 1){
			solve2();
		}
	}
	printf("%d\n",ans);
	return 0;
}
