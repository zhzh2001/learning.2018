#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int PO = 32805,N = 17,M = 1005;
int n,m;
char s[N][M];
int num[PO],sum[PO];
int bin[N];
int ans;
int Nxt[PO];

signed main(){
	freopen("array.in","r",stdin);
	freopen("force.out","w",stdout);
	bin[0] = 1; for(int i = 1;i <= 15;++i) bin[i] = bin[i-1]<<1;
	n = read(); m = read();
	for(int i = 1;i <= n;++i){
		scanf("%s",s[i]+1);
	}
	for(int j = 1;j <= m;++j){
		int pos = 0;
		for(int i = 1;i <= n;++i)
			if(s[i][j] == '1') pos |= bin[i-1];
		++num[pos];
	}
	int la = 0;
	for(int j = 0;j < bin[n];++j)
		if(num[j] != 0) Nxt[la] = j,la = j;
	for(int i = 0;i < bin[n];++i){
		for(int j = 0;j < n;++j)
			if((i&bin[j]) != 0) ++sum[i];
//		sum[i] = sum[i]*2-n;
//		if(sum[i] < 0) sum[i] = 0;
		if(sum[i] > n-sum[i]) sum[i] = n-sum[i];
	}
	ans = n*m;
	for(int i = 0;i < bin[n];++i){
		int Ans = 0;
		for(int j = Nxt[0];j;j = Nxt[j])
			Ans += num[j]*sum[i^j];
		if(Ans < ans) ans = Ans;
	}
	printf("%d\n",ans);
	return 0;
}
