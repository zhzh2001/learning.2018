#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 2505;
int a[N][N];
int n;

signed main(){
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	n = read();
	for(int i = 1;i <= n;++i)
		for(int j = 1;j <= n;++j) 
			a[i][j] = read();
	for(int i = 1;i <= n;++i)
		if(a[i][i] != 0){
			puts("1");
			printf("%d %d %d\n",i,0,0);
			return 0;
		}	
	for(int i = 1;i <= n;++i)
		for(int j = i+1;j <= n;++j)
			if(a[i][j] != a[j][i]){
				puts("1");
				printf("%d %d %d\n",i,j,0);
				return 0;
			}
	for(int i = 1;i <= n;++i)
		for(int j = i+1;j <= n;++j)
			for(int k = 1;k <= n;++k)
				if(a[i][j] > max(a[i][k],a[j][k])){
					puts("1");
					printf("%d %d %d\n",i,j,k);
					return 0;
				}
	puts("0");
	return 0;
}
