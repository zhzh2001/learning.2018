/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
#define N 2555
//#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc()) 
	x=(x<<1)+(x<<3)+c-48;
	return x;
}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int n,m,ans,wzp;
int a[N][N],b[N],dis[N],pre[N],vis[N];
int Next[N*2],head[N],nedge,to[N*2],lon[N*2];
#define V to[i]
void add(int a,int b,int c){Next[++nedge]=head[a];head[a]=nedge;to[nedge]=b;lon[nedge]=c;}
void add_ne(int a,int b,int c){add(a,b,c);add(b,a,c);}
void gg(int p,int a,int b,int c){
	wrn(p);wrn(a,b,c);exit(0);
}
void dfs(int x,int y,int p){
	b[x]=p;
	if (b[x]!=a[wzp][x]) gg(3,wzp,x,y);
	go(i,x){
		if (V==y) continue;
		dfs(V,x,max(p,lon[i]));
	}
}
signed main(){
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	n=read();
	F(i,1,n) F(j,1,n) 
	a[i][j]=read();
	F(i,1,n){
		if (a[i][i]!=0) gg(1,i,0,0);
		F(j,1,n) if (a[i][j]!=a[j][i]) gg(2,i,j,0);
	}
	vis[1]=1;F(i,2,n) pre[i]=1,dis[i]=a[1][i];dis[0]=inf;
	F(i,1,n-1){
		int u=0;
		F(j,1,n) if (!vis[j]&&dis[j]<dis[u]) u=j;
		vis[u]=1;add_ne(pre[u],u,dis[u]);
		F(j,1,n){
			if (!vis[j]){
				if (a[u][j]<dis[j]) dis[j]=a[u][j],pre[j]=u;
			}
		}
	}
	F(i,1,n){
		wzp=i;
		dfs(i,0,0);
	}
	puts("0");
	return 0;
}
