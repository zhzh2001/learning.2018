/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
#define N 500055
//#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define getchar gc
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int n,m,ans,vis[N],fa[N],h_l,huan[N],hu[N],wzp[N],q,x,y;
int Next[N*2],head[N],nedge,to[N*2],lon[N*2];
#define V to[i]
void add(int a,int b){Next[++nedge]=head[a];head[a]=nedge;to[nedge]=b;}
void add_ne(int a,int b){add(a,b);add(b,a);}
void h_dfs(int x){
	vis[x]=1;
	go(i,x){
		if (V==fa[x]) continue;
		if (vis[V]){
			if (h_l>0) continue;
			for (int j=x;j!=V;j=fa[j]) hu[j]=++h_l,huan[h_l]=j;
			hu[V]=++h_l;huan[h_l]=V;
		}
		else fa[V]=x,h_dfs(V);
	}
}
int siz[N],dis[N],gfa[N],son[N],top[N],a[N],cnt;
void g_fa(int x,int y){
	siz[x]=1;gfa[x]=y;son[x]=0;
	go(i,x){
		if (hu[V]||V==fa[x]) continue;
		fa[V]=x;dis[V]=dis[x]+1;g_fa(V,y);siz[x]+=siz[V];
		if (siz[V]>siz[son[x]]) son[x]=V;
	}
}
void g_lian(int x){
	if (!top[x]) top[x]=x;
	if (!hu[x]) a[x]=++cnt;
	if (son[x]) top[son[x]]=top[x],g_lian(son[x]);
	go(i,x){
		if (V==son[x]||V==fa[x]||hu[V]) continue;
		g_lian(V);
	} 
}
void get_h(){
	//�һ�
	h_dfs(1);
	F(i,1,n) vis[i]=fa[i]=0;
	cnt=h_l;
	F(i,1,n){
		if (hu[i]) a[i]=cnt,g_fa(i,i),g_lian(i);
	}
}
struct xx{
	int l,r,num,a[2];
}t[N*4];
void build(int x,int l,int r){
	t[x].l=l;t[x].r=r;t[x].a[0]=(r-l+1);
	if (l==r) return;
	int mid=(l+r)>>1;
	build(x*2,l,mid);build(x*2+1,mid+1,r);
}
void down(int x){
	if (t[x].l==t[x].r) return;
	swap(t[x*2].a[0],t[x*2].a[1]);
	t[x*2].num^=1;
	swap(t[x*2+1].a[0],t[x*2+1].a[1]);
	t[x*2+1].num^=1;
	t[x].num=0;
}
void change(int x,int l,int r){
	if (l>r) return;
	if (t[x].num) down(x);
	if (t[x].l==l&&t[x].r==r){
		swap(t[x].a[0],t[x].a[1]);t[x].num=1;
		return;
	}
	int mid=(t[x].l+t[x].r)>>1;
	if (r<=mid) change(x*2,l,r);
	else if (l>mid) change(x*2+1,l,r);
	else change(x*2,l,mid),change(x*2+1,mid+1,r);
	t[x].a[0]=t[x*2].a[0]+t[x*2+1].a[0];t[x].a[1]=t[x*2].a[1]+t[x*2+1].a[1];
}
int query(int x,int l,int r){
	if (l>r) return 0;
	if (t[x].num) down(x);
	if (t[x].l==l&&t[x].r==r) return t[x].a[0];
	int mid=(t[x].l+t[x].r)>>1;
	if (r<=mid) return query(x*2,l,r);
	else if (l>mid) return query(x*2+1,l,r);
	else return query(x*2,l,mid)+query(x*2+1,mid+1,r);
}
void t_change(int x,int y){
	while (top[x]!=top[y]){
		if (dis[x]>dis[y]) swap(x,y);
		change(1,a[top[y]],a[y]);
		y=fa[top[y]];
	}
	if (dis[x]>dis[y]) swap(x,y);
	if (dis[y]!=dis[x]) change(1,a[x]+1,a[y]);
}
void h_change(int x,int y){
	if (h_l==2){
		change(1,1,1);
		return;
	}
	int fx=hu[x],fy=hu[y];
	if (fx>fy) swap(x,y),swap(fx,fy);
	int num1=fy-fx,num2=fx+h_l-fy;
	if (num1==num2){
		if (huan[fx+1]<huan[(fx+h_l-2)%h_l+1]) change(1,fx,fy-1);
		else change(1,fy,h_l),change(1,1,fx-1);
	}
	if (num1<num2) change(1,fx,fy-1);
	if (num1>num2) change(1,fy,h_l),change(1,1,fx-1);
}
void t_ch(int x,int y){
	if (gfa[x]==gfa[y]) t_change(x,y);
	else{
		t_change(x,gfa[x]);
		t_change(y,gfa[y]);
		h_change(gfa[x],gfa[y]);
	}
}
signed main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();q=read();
	F(i,1,n) add_ne(read(),read());
	get_h();
	build(1,1,n);
	int ans=0;
	while (q--){
		x=read();y=read();
		t_ch(x,y);
		ans=0;
		ans+=max(1,query(1,1,h_l));
		ans+=query(1,h_l+1,cnt);
		wrn(ans);
	}
	return 0;
}
