// #pragma GCC optimize(3)
#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 20
#define maxm 100010
// #define int long long
using namespace std;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline void write(int x){if(x<0){putchar('-');x=-x;}if(x>9) write(x/10);putchar(x%10+'0');}
inline char gc(){if(SSS==TTT)TTT=(SSS=LZH)+fread(LZH,1,L,stdin);return *SSS++;}
inline int read(){int d=0,w=1;char c=gc();for(;c<'0'||c>'9';c=gc())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=gc())d=(d<<1)+(d<<3)+c-48;return d*w;}
inline void wln(int x){write(x);putchar('\n');}
inline void wrs(int x){write(x);putchar(' ');}
int cnt[maxm][2],vis[maxn],n,m,ans,a[maxn][maxm];
char c;
void dfs(int k)
{
	if(k>n)
	{
		int tmp=0;
		for(int i=1;i<=m;i++)
			tmp+=max(cnt[i][0],cnt[i][1]);
		ans=max(ans,tmp);
		return;
	}
	vis[k]=0;
	dfs(k+1);
	vis[k]=1;
	for(int i=1;i<=m;i++)
		if(a[k][i]==0)
		{
			cnt[i][0]--;
			cnt[i][1]++;
		}
		else
		{
			cnt[i][0]++;
			cnt[i][1]--;
		}
	dfs(k+1);
	for(int i=1;i<=m;i++)
		if(a[k][i]==1)
		{
			cnt[i][0]--;
			cnt[i][1]++;
		}
		else
		{
			cnt[i][0]++;
			cnt[i][1]--;
		}
}
signed main()
{
	freopen("array.in","r",stdin);
	freopen("array.out","w",stdout);
	n=read();
	m=read();
	for(int i=1;i<=n;i++,gc())
		for(int j=1;j<=m;j++)
			a[i][j]=(gc()=='1');
	for(int i=1;i<=m;i++)
		for(int j=1;j<=n;j++)
			if(a[j][i]==1) cnt[i][1]++;
				else cnt[i][0]++;
	dfs(1);
	write(n*m-ans);
	return 0;
}
