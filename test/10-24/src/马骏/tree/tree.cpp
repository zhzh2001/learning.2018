// #pragma GCC optimize(3)
#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 1010
// #define int long long
using namespace std;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
// #define gc getchar
inline void write(int x){if(x<0){putchar('-');x=-x;}if(x>9) write(x/10);putchar(x%10+'0');}
inline char gc(){if(SSS==TTT)TTT=(SSS=LZH)+fread(LZH,1,L,stdin);return *SSS++;}
inline int read(){int d=0,w=1;char c=gc();for(;c<'0'||c>'9';c=gc())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=gc())d=(d<<1)+(d<<3)+c-48;return d*w;}
inline void wln(int x){write(x);putchar('\n');}
inline void wrs(int x){write(x);putchar(' ');}
int n,q,s,t,qe[maxn],vis[maxn],last[maxn],mp[maxn][maxn];
vector<int> a[maxn];
void dfs(int k)
{
	int l=1,r=0;
	qe[++r]=k;
	memset(vis,0,sizeof vis);
	vis[k]=1;
	last[k]=0;
	for(;l<=r;l++)
	{
		// wrs(qe[l]);
		for(int i=0;i<a[qe[l]].size();i++)
		{
			int tp=a[qe[l]][i];
			if(vis[tp]) continue;
			qe[++r]=tp;
			last[tp]=qe[l];
			vis[tp]=1;
			if(tp==t)
			{
				for(;last[tp]!=0;tp=last[tp])
				{
					mp[last[tp]][tp]^=1;
					mp[tp][last[tp]]^=1;
				}
				return;
			}
		}
	}
}
void ss(int k)
{
	vis[k]=1;
	for(int i=1;i<=n;i++)
		if(!vis[i]&&mp[k][i]) ss(i);
}
void daan()
{
	memset(vis,0,sizeof vis);
	int ans=0;
	for(int i=1;i<=n;i++)
		if(!vis[i])
		{
			ss(i);
			ans++;
		}
	wln(n-ans);
}
signed main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	q=read();
	for(int i=1;i<=n;i++)
	{
		int x=read(),y=read();
		a[x].push_back(y);
		a[y].push_back(x);
		mp[x][y]++;
		mp[y][x]++;
	}
	for(int i=1;i<=n;i++)
		sort(a[i].begin(),a[i].end());
	for(int i=1;i<=q;i++)
	{
		s=read();
		t=read();
		dfs(s);
		// putchar('\n');
		daan();
		// for(int i=1;i<=n;i++,putchar('\n'))
		// 	for(int j=1;j<=n;j++)
		// 		wrs(mp[i][j]);
	}
	return 0;
}
