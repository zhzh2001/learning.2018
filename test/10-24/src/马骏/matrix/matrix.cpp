// #pragma GCC optimize(3)
#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 2510
#define int long long
// #define inf 0x3f3f3f3f3f3f3f3f
// #define gc getchar
using namespace std;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline void write(int x){if(x<0){putchar('-');x=-x;}if(x>9) write(x/10);putchar(x%10+'0');}
inline char gc(){if(SSS==TTT)TTT=(SSS=LZH)+fread(LZH,1,L,stdin);return *SSS++;}
inline int read(){int d=0,w=1;char c=gc();for(;c<'0'||c>'9';c=gc())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=gc())d=(d<<1)+(d<<3)+c-48;return d*w;}
inline void wln(int x){write(x);putchar('\n');}
inline void wrs(int x){write(x);putchar(' ');}
int n,a[maxn][maxn];
signed main()
{
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;a[i][j++]=read());
	for(int i=1;i<=n;i++)
		if(a[i][i])
		{
			wln(1);
			wrs(i);
			wrs(0);
			write(0);
			return 0;
		}
	for(int i=1;i<n;i++)
		for(int j=i+1;j<=n;j++)
			if(a[i][j]!=a[j][i])
			{
				wln(2);
				wrs(i);
				wrs(j);
				write(0);
				return 0;
			}
	for(int i=(n>>1)+1;i<n;i++)
		for(int j=n;j>i;j--)
			for(int k=1;k<=n;k++)
				if(a[i][j]>a[i][k]&&a[i][j]>a[j][k])
				{
					wln(3);
					wrs(i);
					wrs(j);
					write(k);
					return 0;
				}
	for(int i=n>>1;i;i--)
		for(int j=n;j>i;j--)
			for(int k=1;k<=n;k++)
				if(a[i][j]>a[i][k]&&a[i][j]>a[j][k])
				{
					wln(3);
					wrs(i);
					wrs(j);
					write(k);
					return 0;
				}
	return 0;
}
