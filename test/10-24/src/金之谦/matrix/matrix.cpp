#include <bits/stdc++.h>
using namespace std;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}
inline void write(int x){
	if(x<0){x=-x;putchar('-');}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=2510;
int n,a[N][N],cnt=0;
int main()
{
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)a[i][j]=read();
	for(int i=1;i<=n;i++)if(a[i][i]!=0){
		puts("1");
		write(i);puts(" 0 0");
		return 0;
	}
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++)if(a[i][j]!=a[j][i]){
			puts("2");
			write(i);putchar(' ');write(j);puts(" 0");
			return 0;
		}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			for(int k=1;k<=n;k++){
				if(a[i][j]>max(a[i][k],a[k][j])){
					puts("3");
					write(i);putchar(' ');write(j);putchar(' ');writeln(k);
					return 0;
				}
				cnt++;
				if(cnt>1.3e8)break;
			}
	puts("0");
	return 0;
}
