#include <bits/stdc++.h>
using namespace std;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}
inline void write(int x){
	if(x<0){x=-x;putchar('-');}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=1e5+10;
struct ppap{int x,y,v;}a[N];
struct qqaq{int x,k;};
bool operator <(qqaq a,qqaq b){return a.x==b.x?a.k<b.k:a.x<b.x;}
vector<qqaq>e[N];
qqaq zy[1010][1010];
int n,m;
queue<int>q;
inline void bfs(int x){
	q.push(x);zy[x][x].x=-1;
	while(!q.empty()){
		int now=q.front();q.pop();
		for(int i=0;i<e[now].size();i++){
			int to=e[now][i].x;
			if(!zy[x][to].x){
				zy[x][to]=(qqaq){now,e[now][i].k};q.push(to);
			}
		}
	}
}
int fa[N];
inline int find(int x){
	return fa[x]==x?x:fa[x]=find(fa[x]);
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=n;i++){
		int x=read(),y=read();
		e[x].push_back((qqaq){y,i});
		e[y].push_back((qqaq){x,i});
		a[i].x=x;a[i].y=y;a[i].v=0;		
	}
	for(int i=1;i<=n;i++)sort(e[i].begin(),e[i].end());
	for(int i=1;i<=n;i++)bfs(i);
	for(int T=1;T<=m;T++){
		int x=read(),y=read();
		while(y!=x){
			qqaq kp=zy[x][y];
			a[kp.k].v^=1;
			y=kp.x;
		}
		for(int i=1;i<=n;i++)fa[i]=i;
		for(int i=1;i<=n;i++)if(a[i].v){
			int fx=find(a[i].x),fy=find(a[i].y);
			if(fx!=fy)fa[fx]=fy;
		}
		int ans=0;
		for(int i=1;i<=n;i++)if(fa[i]==i)ans++;
		writeln(ans);
	}
	return 0;
}
