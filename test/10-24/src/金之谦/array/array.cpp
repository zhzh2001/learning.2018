#include <bits/stdc++.h>
using namespace std;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}
inline void write(int x){
	if(x<0){x=-x;putchar('-');}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=1e5+10;
char s[20][N],p[10];
int f[2][(1<<18)+10],kp[(1<<18)+10],n,m;
int main()
{
	freopen("array.in","r",stdin);
	freopen("array.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)while(!isdigit(s[i][j]=gc()));
	int p=(1<<n)-1;
	for(int i=1;i<=p;i++){
		int x=i;
		while(x){
			kp[i]++;
			x-=x&-x;
		}
	}
	for(int i=1;i<=m;i++){
		int jzq=0;
		for(int j=1;j<=n;j++){
			jzq=jzq*2+s[j][i]-'0';
		}
		int qzj=jzq^p;
		for(int j=0;j<=p;j++){
			f[i&1][j]=1e9;
			int rp=jzq^j;
			f[i&1][j]=min(f[i&1][j],kp[rp]+f[i&1^1][j]);
			rp=qzj^j;
			f[i&1][j]=min(f[i&1][j],kp[rp]+f[i&1^1][j]);
		}
	}
	int ans=1e9;
	for(int j=0;j<=p;j++)ans=min(ans,f[m&1][j]);
	writeln(ans);
	return 0;
}
