#include<bits/stdc++.h>
using namespace std;
inline int bitcnt(int x)
{
	int ans=0;
	for(;x;x&=x-1) ans++;
	return ans;
}
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}
int a[100100];int n,m;
inline void init()
{
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++) for(int j=1;j<=m;j++)
	{
		char c=gc();while(c!='0'&&c!='1') c=gc();
		if(c=='1') a[j]|=(1<<(i-1));
	}
}
inline int calc(int X)
{
	int res=0;
	for(int i=1;i<=m;i++)
	{
		int x=bitcnt(a[i]^X);
		res+=min(x,n-x);
	}
	return res;
}
int tmp[300001];
int main()
{
	freopen("array.in","r",stdin);
	freopen("array.out","w",stdout);
	init();int ans=1e9;
	if(n>15||m>1000)
	{
		srand(233666888);
		int k=rand()%5;
		for(int i=0;i<(1<<n);i++) tmp[i]=i;
		while(k--) random_shuffle(tmp,tmp+(1<<n));
		for(int i=0;i<min(1<<n,30000000/m);i++) ans=min(ans,calc(tmp[i]));
		cout<<ans;return 0;
	}
	for(int i=0;i<(1<<n);i++) ans=min(ans,calc(i));
	cout<<ans;return 0;
}
/*
3 4
0110
1010
0111
*/
