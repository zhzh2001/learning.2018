#include<bits/stdc++.h>
using namespace std;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}
#define maxn 100100
int head[maxn],nxt[maxn<<1],ver[maxn<<1],eid[maxn<<1],tot;
int n,q;
inline void addedge(int a,int b,int Id)
{
	nxt[++tot]=head[a];ver[tot]=b;eid[tot]=Id;head[a]=tot;
	nxt[++tot]=head[b];ver[tot]=a;eid[tot]=Id;head[b]=tot;
}
int fa[maxn],dep[maxn],size[maxn],hson[maxn],lstedge[maxn],Fa[21][maxn];
inline void dfs1(int x,int fat)
{
	fa[x]=fat;dep[x]=dep[fat]+1;size[x]=1;int maxsize=0;Fa[0][x]=fat;
	for(int i=head[x];i;i=nxt[i])
	{
		int y=ver[i];if(y==fat) continue;
		dfs1(y,x);size[x]+=size[y];lstedge[y]=eid[i];
		if(size[y]>maxsize) maxsize=size[y],hson[x]=y;
	}
}
int top[maxn],id[maxn],id_cnt;
inline void dfs2(int x,int Top)
{
	top[x]=Top;id[x]=++id_cnt;
	if(hson[x]) dfs2(hson[x],Top);
	for(int i=head[x];i;i=nxt[i])
	{
		int y=ver[i];if(y==fa[x]||y==hson[x]) continue;
		dfs2(y,y);
	}
}
struct node
{
	int cnt1,cnt0;
	node(){cnt1=cnt0=0;}
	inline void setrev(){swap(cnt1,cnt0);}
};
inline node Merge(node a,node b){node ans;ans.cnt1=a.cnt1+b.cnt1;ans.cnt0=a.cnt0+b.cnt0;return ans;}
struct segment_tree
{
	#define ls (p<<1)
	#define rs (p<<1|1)
	node v[maxn<<2];bool rev[maxn<<2];
	inline void pushup(int p){v[p]=Merge(v[ls],v[rs]);}
	inline void pushdown(int p)
	{
		if(rev[p]==0) return;
		rev[ls]^=1;rev[rs]^=1;
		v[ls].setrev();v[rs].setrev();
		rev[p]=0;
	}
	inline void setrev(int p){rev[p]^=1;v[p].setrev();}
	inline void build(int p,int l,int r)
	{
		if(l==r){v[p].cnt1=0;v[p].cnt0=1;return;}
		int mid=(l+r)>>1;
		build(ls,l,mid);
		build(rs,mid+1,r);
		pushup(p);
	}
	inline void Reverse(int p,int l,int r,int ql,int qr)
	{
		if(ql<=l&&qr>=r) return (void)setrev(p);
		int mid=(l+r)>>1;pushdown(p);
		if(ql<=mid) Reverse(ls,l,mid,ql,qr);
		if(qr>mid) Reverse(rs,mid+1,r,ql,qr);
		pushup(p);
	}
	inline node Query(int p,int l,int r,int ql,int qr)
	{
		if(ql<=l&&qr>=r) return v[p];
		int mid=(l+r)>>1;node ans;pushdown(p);
		if(ql<=mid) ans=Merge(ans,Query(ls,l,mid,ql,qr));
		if(qr>mid) ans=Merge(ans,Query(rs,mid+1,r,ql,qr));
		return ans;
	}
}seg;
inline int lca(int x,int y)
{
	while(top[x]!=top[y])
	{
		if(dep[top[x]]<dep[top[y]]) swap(x,y);
		x=fa[top[x]];
	}
	if(dep[x]>dep[y]) swap(x,y);
	return x;
}
inline int Qdis(int x,int y){return dep[x]+dep[y]-(dep[lca(x,y)]<<1);}
inline node Query(int x,int y)
{
	node ans;
	while(top[x]!=top[y])
	{
		if(dep[top[x]]<dep[top[y]]) swap(x,y);
		ans=Merge(ans,seg.Query(1,1,n,id[top[x]],id[x]));
		x=fa[top[x]];
	}
	if(x==y) return ans;
	if(dep[x]>dep[y])swap(x,y);
	return Merge(ans,seg.Query(1,1,n,id[x]+1,id[y]));
}
inline void Reverse(int x,int y)
{
	while(top[x]!=top[y])
	{
		if(dep[top[x]]<dep[top[y]]) swap(x,y);
		seg.Reverse(1,1,n,id[top[x]],id[x]);
		x=fa[top[x]];
	}
	if(x==y) return;
	if(dep[x]>dep[y]) swap(x,y);
	seg.Reverse(1,1,n,id[x]+1,id[y]);
}
struct Union_set
{
	int fa[maxn];
	inline int find(int x){return x==fa[x]?x:fa[x]=find(fa[x]);}
	inline void Union(int x,int y){fa[find(x)]=find(y);}
	inline void init(){for(int i=1;i<=n;i++) fa[i]=i;}
}us;
int rx,ry,restid;bool rin;
//the edge not on the tree
inline void init()
{
	n=read();q=read();
	us.init();rin=0;
	for(int i=1,x,y;i<=n;i++)
	{
		x=read();y=read();
		if(us.find(x)==us.find(y))
		{
			rx=x;ry=y;restid=i;
			continue;
		}
		us.Union(x,y);
		addedge(x,y,i);
	}
	dfs1(1,0);dfs2(1,1);
	seg.build(1,1,n);
	for(int i=1;i<=20;i++) for(int j=1;j<=n;j++) Fa[i][j]=Fa[i-1][Fa[i-1][j]];
}
inline int Getfa(int x,int updep)
{
	for(int i=0;updep;updep>>=1,i++) if(updep&1) x=Fa[i][x];
	return x;
}
inline int Getid(int x,int y)
{
	//the edge near x
	if(x==y) assert(0);
	if(id[y]>=id[x]&&id[y]<=id[x]+size[x]-1)
	{
		int delta=dep[y]-dep[x];
		return lstedge[Getfa(y,delta-1)];
	}
	else return lstedge[x];
}
inline void change(int x,int y)
{
	int d1=Qdis(x,y),d2=Qdis(x,rx)+1+Qdis(y,ry),d3=Qdis(x,ry)+1+Qdis(y,rx);
	if(d1<min(d2,d3)) return (void)Reverse(x,y);
	if(d1>min(d2,d3))
	{
		rin^=1;
		if(d2<d3) Reverse(x,rx),Reverse(y,ry);
		else Reverse(x,ry),Reverse(y,rx);
		return;
	}
	if(d1==d3) swap(rx,ry);
	//Compare x->rx,x->y
	int l1=lca(x,rx),l2=lca(x,y);
	int Lca;
	if(Qdis(l1,x)<Qdis(l2,x)) Lca=l1;else Lca=l2;
	//get the first different id
	int G1=Lca==rx?ry:Getid(Lca,rx),G2=Getid(Lca,y);
	if(G1>G2) return (void)Reverse(x,y);
	Reverse(x,rx);rin^=1;Reverse(y,ry);
}
inline int Qans()
{
	int ans=seg.Query(1,1,n,2,n).cnt0+1;
	if(rin)
	{
		node p=Query(rx,ry);//cout<<p.cnt1<<' '<<p.cnt0<<endl;
		if(p.cnt0>0) ans--;
	}
	return ans;
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	init();int x,y;
	while(q--)
	{
		x=read();y=read();
		change(x,y);
		printf("%d\n",Qans());
	}
	return 0;
}
/*
6 2
4 6
4 3
1 2
6 5
1 5
1 4
2 5
2 6
*/
/*
for the edges on the tree:
one less edge->one more block
for the edge not on the tree:
if the two vertices is not connected -> one less block

need to be solved:
Query dis
Query edgeid

segment_tree:
sum of edges exist
sum of edges not exist
O(nlog^2n)
*/
