#include<vector>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
inline char _gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return (p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2))?EOF:*p1++;
}
#define gc c=_gc()
int read() {
	int x = 0, f = 1; char gc;
	for (; !isdigit(c); gc)if (c == '-')f = -1;
	for (; isdigit(c); gc)x = x * 10 + c - '0';
	return x * f;
}
#undef gc
const int MAXN=1e5+5;
struct Edge{
	Edge(){}
	Edge(int To,int Id){
		id=Id,to=To;
	}
	int to,id;
	bool operator<(const Edge &b)const{
		if(to!=b.to)return to<b.to;
		return id<b.id;
	}
};
vector<Edge>edge[MAXN];
vector<int>p[MAXN];
vector<int>v;
struct E{
	int x,y;
}e[MAXN];
int n,q,S,T;
int d[1005],f[1005];
bool used[1005];
void dfs(int x,int dis){
	if(dis>=d[x])return;
	d[x]=dis;
	p[x]=v;
	for(int i=0;i<(int)edge[x].size();i++){
		int y=edge[x][i].to;
		v.push_back(edge[x][i].id);
		dfs(y,dis+1);
		v.pop_back();
	}
}
int Find(int x){
	return f[x]=f[x]==x?x:Find(f[x]);
}
void bf1(){
	for(int i=1;i<=n;i++)p[i].clear();
	v.clear();
	memset(d,0x3f,sizeof(d));
	dfs(S,0);
	for(int i=0;i<(int)p[T].size();i++)used[p[T][i]]^=1;
	for(int i=1;i<=n;i++)f[i]=i;
	int ans=n;
	for(int i=1;i<=n;i++){
		if(used[i])continue;
		int x=Find(e[i].x),y=Find(e[i].y);
		if(x==y)continue;
		ans--;
		f[x]=y;
	}
	printf("%d\n",ans);
}
int cnt,id[MAXN],nxt[MAXN],pre[MAXN];
void init(int x){
	id[x]=++cnt;
	for(int i=0;i<(int)edge[x].size();i++){
		int y=edge[x][i].to;
		if(!id[y]){
			nxt[x]=y;
			pre[y]=x;
			init(y);
			return;
		}
	}
}
struct Segtree{
	int l,r,x,lazy;
}tree[MAXN<<2];
void pushup(int k){
	tree[k].x=tree[k<<1].x+tree[k<<1|1].x;
}
void pushdown(int k){
	if(!tree[k].lazy)return;
	tree[k<<1].lazy^=1;
	tree[k<<1|1].lazy^=1;
	tree[k<<1].x=tree[k<<1].r-tree[k<<1].l+1-tree[k<<1].x;
	tree[k<<1|1].x=tree[k<<1|1].r-tree[k<<1|1].l+1-tree[k<<1|1].x;
}
void build(int k,int l,int r){
	tree[k].l=l,tree[k].r=r;
	if(l==r){
		tree[k].x=1;
		return;
	}
	int mid=(l+r)>>1;
	build(k<<1,l,mid);build(k<<1,mid+1,r);
	pushup(k);
}
void upd(int k,int l,int r){
	if(tree[k].l>r||tree[k].r<l)return;
	if(l<=tree[k].l&&tree[k].r<=r){
		tree[k].lazy^=1;
		tree[k].x=tree[k].r-tree[k].l+1-tree[k].x;
		return;
	}
	pushdown(k);
	upd(k<<1,l,r);upd(k<<1|1,l,r);
	pushup(k);
}
int query(int k,int l,int r){
	if(tree[k].l>r||tree[k].r<l)return 0;
	if(l<=tree[k].l&&tree[k].r<=r)return tree[k].x;
	pushdown(k);
	return query(k<<1,l,r)+query(k<<1|1,l,r);
}
void bf2(){
	if(id[S]<id[T]){
		int len1=id[T]-id[S];
		int len2=n-id[T]+id[S];
		if(len1<len2)upd(1,id[S]+1,id[T]);
		else if(len2<len1){
			upd(1,id[T],n);
			upd(1,1,id[S]-1);
		}else{
			if(nxt[S]>pre[S]){
				upd(1,id[T],n);
				upd(1,1,id[S]-1);
			}else upd(1,id[S]+1,id[T]);
		}
	}else{
		int len1=id[S]-id[T];
		int len2=n-id[S]+id[T];
		if(len1<len2)upd(1,id[T],id[S]-1);
		else if(len1>len2){
			upd(1,id[S]+1,n);
			upd(1,1,id[T]);
		}else{
			if(nxt[S]<pre[S]){
				upd(1,id[S]+1,n);
				upd(1,1,id[T]);
			}else upd(1,id[T],id[S]-1);
		}
	}
	printf("%d\n",query(1,1,n));
}
int main() {
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read(),q=read();
	for(int i=1;i<=n;i++){
		int x=read(),y=read();
		edge[x].push_back(Edge(y,i));
		edge[y].push_back(Edge(x,i));
		e[i].x=x,e[i].y=y;
	}
	for(int i=1;i<=n;i++)sort(edge[i].begin(),edge[i].end());
	if(n>1000){
		init(1);
		for(int i=1;i<=n;i++)
			if(!nxt[i]){
				nxt[i]=1;
				pre[1]=i;
			}
		build(1,1,n);
	}
	for(int i=1;i<=n&&n<=1000;i++)used[i]=1;
	while(q--){
		S=read(),T=read();
		if(n<=1000)bf1();
		else bf2();
	}
}
