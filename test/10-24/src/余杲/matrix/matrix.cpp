#include<cstdio>
#include<cstdlib>
#include<iostream>
using namespace std;
inline char _gc() {
	static char buf[1 << 14], *p1 = buf, *p2 = buf;
	return (p1 == p2 && (p2 = (p1 = buf) + fread(buf, 1, 1 << 14, stdin), p1 == p2)) ? EOF : *p1++;
}
#define gc c=_gc()
int read() {
	int x = 0, f = 1; char gc;
	for (; !isdigit(c); gc)if (c == '-')f = -1;
	for (; isdigit(c); gc)x = x * 10 + c - '0';
	return x * f;
}
#undef gc
void Finish(int opt, int i, int j, int k) {
	printf("%d\n", opt);
	printf("%d %d %d", i, j, k);
	exit(0);
}
int a[3000][3000], ln[3000], col[3000];
int main() {
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	int n = read();
//	for (int i = 1; i <= n; i++)
//		for (int j = 1; j <= n; j++) {
//			for (int k = 1; k <= n; k++)
//				cout << "(" << i << "," << k << ")" << " (" << k << "," << j << ")\n";
//			cout << "------------------" << endl;
//		}
	for (register int i = 1; i <= n; i++)
		for (register int j = 1; j <= n; j++) {
			a[i][j] = read();
			if (i == j && a[i][j])Finish(1, i, 0, 0);
			if (i > j && a[i][j] != a[j][i])Finish(2, i, j, 0);
		}
//	for(int i=1;i<=n;i++){
//		ln[i]=1;
//		for(int j=2;j<=n;j++)
//			if(a[i][j]>a[i][ln[i]])ln[i]=j;
//	}
//	for(int i=1;i<=n;i++){
//		col[i]=1;
//		for(int j=2;j<=n;j++)
//			if(a[j][i]>a[col[i]][i])col[i]=j;
//	}
//	for(int i=1;i<=n;i++)
//		for(int j=1;j<=n;j++){
//			if(a[i][j]>a[i][ln[i]])Finish(3,i,j,ln[i]);
//			if(a[i][j]>a[col[j]][j])Finish(3,i,j,col[j]);
//		}
	for (register int i = 1; i <= n; i++)
		for (register int j = 1; j <= n; j++)
			for (register int k = 1; k <= n; k++)
				if (a[i][j] > max(a[i][k], a[k][j]))Finish(3, i, j, k);
	puts("0");
}
