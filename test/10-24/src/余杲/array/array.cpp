#include<cstdio>
#include<iostream>
using namespace std;
const int MAXM = 1e5;
int n, m, ans = 1e9;
int cnt[MAXM];
char s[20][MAXM];
void dfs(int k) {
	if (k > n) {
		int tmp = 0;
		for (register int i = 0; i < m; i++)
			if (n - cnt[i] < cnt[i])tmp += n - cnt[i];
			else tmp += cnt[i];
		ans = min(ans, tmp);
		return;
	}
	for (register int i = 0; i < m; i++)
		if (s[k][i]^'0')cnt[i]++;
	dfs(k + 1);
	for (register int i = 0; i < m; i++)
		if (s[k][i]^'0')cnt[i]--;
	for (register int i = 0; i < m; i++)
		if (s[k][i]^'0' ^ 1)cnt[i]++;
	dfs(k + 1);
	for (register int i = 0; i < m; i++)
		if (s[k][i]^'0' ^ 1)cnt[i]--;
}
int main() {
	freopen("array.in", "r", stdin);
	freopen("array.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (register int i = 1; i <= n; i++)scanf("%s", s[i]);
	dfs(1);
	printf("%d", ans);
}
