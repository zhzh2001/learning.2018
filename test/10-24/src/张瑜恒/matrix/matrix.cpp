#include<bits/stdc++.h>
using namespace std;
int n,a[2510][2510];
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}
int max(int l,int r)
  {
   if (l>r) return l;else return r;
  }
int main()
  {
   freopen("matrix.in","r",stdin);
   freopen("matrix.out","w",stdout);
	
   n=read();
   for (int i=1;i<=n;i++)
	 for (int j=1;j<=n;j++) a[i][j]=read();
   int t1=-1;
   for (int i=1;i<=n;i++) if (a[i][i]!=0) {t1=i;break;}
   if (t1!=-1) {printf("1\n");printf("%d 0 0\n",t1);return 0;}
   int x=-1;int y=-1;
   for (int i=1;i<=n;i++)
	 {
	  for (int j=1;j<=n;j++)
	    if (a[i][j]!=a[j][i]) {x=i;y=j;break;}
	  if (x!=-1) break;
     }
   if (x!=-1) {printf("2\n");printf("%d %d 0\n",x,y);return 0;}
   x=-1;y=-1;int z=-1;
   for (int i=1;i<=n;i++)
	 {
	  for (int j=1;j<=n;j++)
	    {
		 for (int k=1;k<=n;k++)
		   if (a[i][j]>max(a[i][k],a[k][j])) {x=i;y=j;z=k;break;}
         if (x!=-1) break;
        }
      if (x!=-1) break;
     }
   if (x!=-1) {printf("3\n");printf("%d %d %d\n",x,y,z);return 0;}
   printf("0\n");
   return 0;
  }
