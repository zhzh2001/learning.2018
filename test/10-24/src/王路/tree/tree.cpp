#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstring>
#include <cctype>
#include <limits>
#include <algorithm>
#include <functional>
#include <numeric>
#include <cassert>
#include <queue>
using namespace std;

namespace IO {
static const int kMegaBytes = 1 << 20;
char buf[kMegaBytes], *S = buf, *T = buf;
bool isneg;
inline bool Fetch() { return T = (S = buf) + fread(buf, 1, kMegaBytes, stdin), S != T; }
inline char NC() { return S == T ? (Fetch() ? *S++ : EOF) : *S++; }
template <typename Int> inline void RI(Int &x) {
  x = isneg = 0;
  char ch;
  for (ch = NC(); isspace(ch); ch = NC())
    ;
  if (ch == '-')
  	isneg = true, ch = NC();
  for (; isdigit(ch); ch = NC())
  	x = x * 10 + ch - '0';
  if (isneg) x = -x;
}
} // namespace IO

using IO::RI;

const int kMaxN = 1e5 + 5;
struct Atom { int from, to, eid; };
vector<Atom> G[kMaxN];
int dis[kMaxN], n, q, fa[kMaxN];
pair<int, int> lk[kMaxN];
bool vis[kMaxN], exi[kMaxN];
struct Edge { int u, v; } ee[kMaxN];
int Find(int x) { return x == fa[x] ? x : fa[x] = Find(fa[x]); }

namespace SubTask1 {
int Solve() {
  for (int i = 1, s, t; i <= q; ++i) {
    RI(s), RI(t);
    memset(dis, 0x3f, sizeof dis);
    memset(lk, 0x00, sizeof lk);
    queue<int> Q;
    Q.push(s);
    dis[s] = 1;
    while (!Q.empty()) {
      int u = Q.front();
      vis[u] = true;
      for (vector<Atom>::iterator it = G[u].begin(); it != G[u].end(); ++it) {
        if (dis[it->to] > dis[u] + 1) {
          dis[it->to] = dis[u] + 1;
          Q.push(it->to);
          vis[it->to] = true;
          lk[it->to] = make_pair(u, it->eid);
        } else if (dis[it->to] == dis[u] + 1) {
          if (u < lk[it->to].first)
            lk[it->to] = make_pair(u, it->eid);
          else if (u == lk[it->to].first && it->eid < lk[it->to].second)
            lk[it->to] = make_pair(u, it->eid);
        }
      }
      Q.pop();
    }
    // fprintf(stderr, "%d\n", dis[t]);
    // fprintf(stderr, "Hello, World!\n");
    for (int now = t; now != s; now = lk[now].first) {
      exi[lk[now].second] ^= 1;
      // fprintf(stderr, "%d\n", now);
    }
    for (int i = 0; i <= n; ++i)
      fa[i] = i;
    for (int i = 1; i <= n; ++i) {
      if (exi[i]) {
        int fx = Find(ee[i].u), fy = Find(ee[i].v);
        if (fx != fy)
          fa[fx] = fy;
      }
    }
    int ans = 0;
    for (int i = 1; i <= n; ++i) {
      ans += fa[i] == i;
    }
    printf("%d\n", ans);
  }
  return 0;
}
} // namespace SubTask1

int main() {
  freopen("tree.in", "r", stdin);
  freopen("tree.out", "w", stdout);
  RI(n), RI(q);
  for (int i = 1, a, b; i <= n; ++i) {
    RI(a), RI(b);
    G[a].push_back((Atom) {a, b, i});
    G[b].push_back((Atom) {b, a, i});
    ee[i] = (Edge) { a, b };
  }
  if (n <= 1000) // 30 pts
    return SubTask1::Solve();
  return 0;
}
