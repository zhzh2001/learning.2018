#pragma GCC optimize 2
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstring>
#include <cctype>
#include <limits>
#include <algorithm>
#include <functional>
#include <numeric>
using namespace std;

namespace IO {
static const int kMegaBytes = 1 << 20;
char buf[kMegaBytes], *S = buf, *T = buf;
bool isneg;
inline bool Fetch() { return T = (S = buf) + fread(buf, 1, kMegaBytes, stdin), S != T; }
inline char NC() { return S == T ? (Fetch() ? *S++ : EOF) : *S++; }
template <typename Int> inline void RI(Int &x) {
  x = isneg = 0;
  char ch;
  for (ch = NC(); isspace(ch); ch = NC())
    ;
  if (ch == '-')
  	isneg = true, ch = NC();
  for (; isdigit(ch); ch = NC())
  	x = x * 10 + ch - '0';
  if (isneg) x = -x;
}
inline void RS(char *s) {
	char ch;
	for (ch = NC(); isspace(ch); ch = NC())
		;
	for (; ch != EOF && !isspace(ch); ch = NC())
		*(s++) = ch;
	*s = 0;
}
} // namespace IO

using IO::RI;
using IO::RS;

const int kMaxN = 18, kMaxM = 1e5 + 5, kInf = 0x3f3f3f3f;

int n, m;
char mat[20][kMaxM], c[20][kMaxN], d[20][kMaxN];
int ans = kInf, ones;

namespace SubTask1 {
int Solve() {
	ans = ones;
	for (int i = 0; i < (1 << m); ++i) {
		for (int j = 0; j < m; ++j) {
			if ((i >> j) & 1) {
				for (int p = 1; p <= n; ++p) {
					mat[j + 1][p] ^= 1;
				}
			}
		}
		for (int j = 0; j < (1 << n); ++j) {
			for (int k = 0; k < n; ++k)
				if ((j >> k) & 1) {
					for (int p = 1; p <= m; ++p) {
						mat[k + 1][p] ^= 1;
					}
				}
			ones = 0;
			for (int i = 1; i <= n; ++i)
				for (int j = 1; j <= m; ++j)
					ones += mat[i][j];
			for (int i = 1; i <= n; ++i) {
				for (int j = 1; j <= m; ++j) {
					c[i][j] = c[i - 1][j] + mat[i][j];
				}
			}
			int tmp = 0;
			for (int i = 1; i <= n; ++i) {
				if (c[n][i] >= n / 2) {
					for (int j = 1; j <= m; ++j) {
						c[i][j] -= c[i - 1][j];
						c[i][j] ^= 1;
					}
				}
				for (int j = 1; j <= m; ++j)
					tmp += c[i][j];
			}
			ones = min(ones, tmp);
			tmp = 0;
			for (int i = 1; i <= n; ++i) {
				for (int j = 1; j <= m; ++j) {
					d[i][j] = d[i][j - 1] + mat[i][j];
				}
			}
			for (int i = 1; i <= m; ++i) {
				if (d[i][m] >= n / 2) {
					for (int j = 1; j <= n; ++j) {
						d[j][i] -= d[j][i - 1];
						d[j][i] ^= 1;
					}
				}
				for (int j = 1; j <= n; ++j)
					tmp += d[j][i];
			}
			ones = min(ones, tmp);
			ans = min(ans, ones);
			// if (ones == 3) {
			// 	for (int i = 1; i <= n; ++i)
			// 		for (int j = 1; j <= m; ++j)
			// 			fprintf(stderr, "%d%c", mat[i][j], " \n"[j == m]);
			// }
			for (int k = 0; k < n; ++k) {
				if ((j >> k) & 1) {
					for (int p = 1; p <= m; ++p) {
						mat[k + 1][p] ^= 1;
					}
				}
			}
			for (int j = 0; j < m; ++j) {
				if ((i >> j) & 1) {
					for (int p = 1; p <= n; ++p) {
						mat[j + 1][p] ^= 1;
					}
				}
			}
		}
	}
	printf("%d\n", ans);
	return 0;
}
} // namespace SubTask1

namespace Solution {
int Solve() {
	
	return 0;
}
} // namespace Solution

int main() {
  freopen("array.in", "r", stdin);
  freopen("array.out", "w", stdout);
  RI(n), RI(m);
  ones = 0;
  for (int i = 1; i <= n; ++i) {
  	RS(mat[i] + 1);
  	for (int j = 1; j <= m; ++j) {
  		mat[i][j] -= '0';
  		ones += mat[i][j];
  	}
  }
  if (n <= 10 && m <= 10) // ~ 30 pts
  	return SubTask1::Solve();
  return Solution::Solve();
}
