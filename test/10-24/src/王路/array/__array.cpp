#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstring>
#include <cctype>
#include <limits>
#include <algorithm>
#include <functional>
#include <numeric>
using namespace std;

namespace IO {
static const int kMegaBytes = 1 << 20;
char buf[kMegaBytes], *S = buf, *T = buf;
bool isneg;
inline bool Fetch() { return T = (S = buf) + fread(buf, 1, kMegaBytes, stdin), S != T; }
inline char NC() { return S == T ? (Fetch() ? *S++ : EOF) : *S++; }
template <typename Int> inline void RI(Int &x) {
  x = isneg = 0;
  char ch;
  for (ch = NC(); isspace(ch); ch = NC())
    ;
  if (ch == '-')
  	isneg = true, ch = NC();
  for (; isdigit(ch); ch = NC())
  	x = x * 10 + ch - '0';
  if (isneg) x = -x;
}
inline void RS(char *s) {
	char ch;
	for (ch = NC(); isspace(ch); ch = NC())
		;
	for (; ch != EOF && !isspace(ch); ch = NC())
		*(s++) = ch;
	*s = 0;
}
} // namespace IO

using IO::RI;
using IO::RS;

const int kMaxN = 18, kMaxM = 1e5 + 5, kInf = 0x3f3f3f3f;

int n, m, cnt[1 << kMaxN];
char mat[20][kMaxM];
int ans = kInf, ones;

namespace SubTask1 {
int Solve() {
	for (int i = 1; i <= n; ++i) {}
	return 0;
}
} // namespace SubTask1

int main() {
  freopen("array.in", "r", stdin);
  freopen("array.out", "w", stdout);
  RI(n), RI(m);
  ones = 0;
  for (int i = 1; i <= n; ++i) {
  	RS(mat[i] + 1);
  	for (int j = 1; j <= m; ++j) {
  		mat[i][j] -= '0';
  		ones += mat[i][j];
  	}
  }
  if (n <= 10 && m <= 10)
  	return SubTask1::Solve();
  return 0;
}
