#pragma GCC optimize 2
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstring>
#include <cctype>
#include <limits>
#include <algorithm>
#include <functional>
#include <ctime>
#include <numeric>
using namespace std;

namespace IO {
static const int kMegaBytes = 1 << 20;
char buf[kMegaBytes], *S = buf, *T = buf;
bool isneg;
inline bool Fetch() { return T = (S = buf) + fread(buf, 1, kMegaBytes, stdin), S != T; }
inline char NC() { return S == T ? (Fetch() ? *S++ : EOF) : *S++; }
template <typename Int> inline void RI(Int &x) {
  x = isneg = 0;
  char ch;
  for (ch = NC(); isspace(ch); ch = NC())
    ;
  if (ch == '-')
  	isneg = true, ch = NC();
  for (; isdigit(ch); ch = NC())
  	x = x * 10 + ch - '0';
  if (isneg) x = -x;
}
} // namespace IO

using IO::RI;

const int kMaxN = 2505;
int n, a[kMaxN][kMaxN], b[kMaxN][kMaxN], id[kMaxN];
pair<int, int> c[kMaxN], d[kMaxN];

namespace SubTask1 {
int Solve() {
	for (int i = 1; i <= n; ++i)
		id[i] = i;
	if (n >= 900)
		random_shuffle(id + 1, id + n + 1);
  for (int i = 1; i <= n; ++i) {
    for (int j = id[i] + 1; j <= n; ++j) {
      if (clock() > 1.7 * CLOCKS_PER_SEC)
        return puts("0"), 0;
      for (int k = 1; k <= n; ++k) {
        if (a[id[i]][j] > max(a[id[i]][k], a[k][j])) {
          return printf("%d\n%d %d %d\n", 3, id[i], j, k);
        }
      }
    }
  }
  return puts("0"), 0;
}
} // namespace SubTask1

namespace Solution {
// vector<pair<int, int> > g1[kMaxN], g2[kMaxN];
// bool vis[kMaxN];
// 前者固定左端点，后者固定右端点
int Solve() {
  // for (int i = 1; i <= n; ++i) {
  //   g1[i].reserve(n);
  //   g2[i].reserve(n);
  // }
  // for (int i = 1; i <= n; ++i) {
  //   for (int j = 1; j <= n; ++j) {
  //     g1[i].push_back(make_pair(a[i][j], j));
  //     g2[j].push_back(make_pair(a[i][j], i));
  //   }
  // }
  // for (int i = 1; i <= n; ++i) {
  //   sort(g1[i].begin(), g1[i].end());
  //   sort(g2[i].begin(), g2[i].end());
  // }
  // for (int i = 1; i <= n; ++i) {
  //   memset(vis, 0x00, sizeof vis);
  //   int qk = 0;
  //   for (int c = 0; c < (int)g1[i].size(); ++c) {
  //     int j = g1[i][c].second, w = g1[i][c].first;
  //     if (i == j) continue;

  //     vis[j] = true;
  //   }
  // }
  return puts("0"), 0;
}
} // namespace Solution

int main() {
  freopen("matrix.in", "r", stdin);
  freopen("matrix.out", "w", stdout);
  RI(n);
  for (int i = 1; i <= n; ++i) {
    for (int j = 1; j <= n; ++j) {
      RI(a[i][j]);
    }
    if (a[i][i]) {
      return printf("%d\n%d %d %d\n", 1, i, 0, 0), 0;
    }
  }
  for (int i = 1; i <= n; ++i) {
    for (int j = 1; j <= n; ++j)
      if (a[i][j] != a[j][i]) {
        return printf("%d\n%d %d %d\n", 2, i, j, 0), 0;
      }
  }
  return SubTask1::Solve(); // ~ 20 pts
}
