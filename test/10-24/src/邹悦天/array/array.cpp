#include<bits/stdc++.h>
#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cstring>
using namespace std;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}

namespace zuiyt
{
	const int N = 18, M = 1e5 + 10;
	bool arr[N][M];
	int n, m;
	inline void readchar(char &c)
	{
		do
			c = gc();
		while (c != '0' && c != '1');
	}
	int cnt[M], ans;
	void dfs(const int pos)
	{
		if (pos == n)
		{
			int tmp = 0;
			for (int i = 0; i < m; i++)
				tmp += min(cnt[i], n - cnt[i]);
			ans = min(ans, tmp);
			return;
		}
		for (int i = 0; i < m; i++)
			if (arr[pos][i])
				--cnt[i];
			else
				++cnt[i];
		dfs(pos + 1);
		for (int i = 0; i < m; i++)
			if (arr[pos][i])
				++cnt[i];
			else
				--cnt[i];
		dfs(pos + 1);
	}
	int work()
	{
		n = read();
		m = read();
		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++)
			{
				char c;
				readchar(c);
				arr[i][j] = (c == '1');
				cnt[j] += (c == '1');
			}
		ans = n * m;
		dfs(0);
		cout << ans;
		return 0;
	}	
}
int main()
{
	freopen("array.in", "r", stdin);
	freopen("array.out", "w", stdout);
	return zuiyt::work();	
}
