#include<bits/stdc++.h>
#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cstring>
using namespace std;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}

namespace zuiyt
{
	const int N = 1e5 + 10;
	int n, q, head[N], cnt, cir[N], ccnt, rot[N], pos[N], pre[N], nxt[N];
	bool incir[N], vis[N];
	struct edge
	{
		int to, id, next;
		bool mark;
	}e[N * 2];
	inline void add(const int a, const int b, const int c)
	{
		e[cnt] = (edge){b, c, head[a], false}, head[a] = cnt++;	
	}
	inline int abs(const int x)
	{
		return x > 0 ? x : -x;	
	}
	bool dfs_findcir(const int u, const int f)
	{
		if (vis[u])
		{
			cir[ccnt++] = u, incir[u] = true;
			return true;
		}
		vis[u] = true;
		for (int i = head[u]; ~i; i = e[i].next)
		{
			int v = e[i].to;
			if (v == f)
				continue;
			if (dfs_findcir(v, u))
			{
				pre[u] = i;
				nxt[cir[ccnt - 1]] = i ^ 1;
				if (cir[0] != u)
				{
					cir[ccnt++] = u, incir[u] = true;
					return true;
				}
				else
					return false;
			}
		}
		vis[u] = false;
		return false;
	}
	void dfs_rot(const int u, const int f)
	{
		rot[u] = rot[f];
		for (int i = head[u]; ~i; i = e[i].next)
		{
			int v = e[i].to;
			if (v == f || incir[v])
				continue;
			pre[v] = i;
			dfs_rot(v, u);
		}
	}
	void dfs_block(const int u)
	{
		vis[u] = true;
		for (int i = head[u]; ~i; i = e[i].next)
		{
			int v = e[i].to;
			if (!e[i].mark || vis[v])
				continue;
			dfs_block(v);
		}
	}
	int work()
	{
		n = read();
		q = read();
		memset(head, -1, sizeof(head));
		for (int i = 1; i <= n; i++)
		{
			int a, b;
			a = read(), b = read();	
			add(a, b, i);
			add(b, a, i);
		}
		dfs_findcir(1, 0);
		for (int i = 0; i < ccnt; i++)
		{
			rot[cir[i]] = cir[i];
			pos[cir[i]] = i;
			dfs_rot(cir[i], cir[i]);
		}
		while (q--)
		{
			int s, t;
			s = read(), t = read();
			while (s != rot[s])
			{
				e[pre[s]].mark = !e[pre[s]].mark;
				e[pre[s] ^ 1].mark = !e[pre[s] ^ 1].mark;
				s = e[pre[s] ^ 1].to;
			}
			while (t != rot[t])
			{
				e[pre[t]].mark = !e[pre[t]].mark;
				e[pre[t] ^ 1].mark = !e[pre[t] ^ 1].mark;
				t = e[pre[t] ^ 1].to;
			}
			int tmp = abs(pos[t] - pos[s]) * 2;
			if ((tmp < ccnt && pos[s] < pos[t]) || (tmp > ccnt && pos[s] > pos[t]) || (tmp == ccnt && e[nxt[s]].to < e[pre[s]].to) || 
				(tmp == ccnt && e[nxt[s]].to == e[pre[s]].to && e[nxt[s]].id < e[pre[s]].id))
				for (int i = nxt[s]; i != nxt[t]; i = nxt[e[i].to])
					e[i].mark = !e[i].mark, e[i ^ 1].mark = !e[i ^ 1].mark;
			else
				for (int i = pre[s]; i != pre[t]; i = pre[e[i].to])
					e[i].mark = !e[i].mark, e[i ^ 1].mark = !e[i ^ 1].mark;
			memset(vis, 0, sizeof(bool[n + 1]));
			int ans = 0;
			for (int i = 1; i <= n; i++)
				if (!vis[i])
					dfs_block(i), ans++;
			cout << ans << '\n';
		}
		return 0;
	}	
}
int main()
{
	freopen("tree.in", "r", stdin);
	freopen("tree.out", "w", stdout);
	return zuiyt::work();	
}
