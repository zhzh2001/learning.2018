#include<bits/stdc++.h>
#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cstring>
using namespace std;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}

namespace zuiyt
{
	const int N = 2510;
	int arr[N][N], n;
	struct node
	{
		int x, y;
		bool operator < (const node &b) const
		{
			return arr[x][y] > arr[b.x][b.y];	
		}	
	}buf[N * N / 2];
	int cnt;
	int work()
	{
		n = read();
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= n; j++)
			{
				arr[i][j] = read();
				if (j > i)
					buf[cnt++] = (node){i, j};
			}
		for (int i = 1; i <= n; i++)//test 1
			if (arr[i][i])
			{
				cout << "1\n" << i << " 0 0";
				return 0;
			}
		for (int i = 1; i <= n; i++)//test 2
			for (int j = i + 1; j <= n; j++)
				if (arr[i][j] != arr[j][i])
				{
					cout << "2\n" << i << ' ' << j << " 0";
					return 0;	
				}
		sort(buf, buf + cnt);
		for (int i = 0; i < cnt; i++)
			for (int k = 1; k <= buf[i].x; k++)
				if (arr[buf[i].x][buf[i].y] > max(arr[buf[i].x][k], arr[buf[i].y][k]))
				{
					cout << "3\n" << buf[i].x << ' ' << buf[i].y << ' ' << k;
					return 0;	
				}
		cout << 0;
		return 0;
	}
}

int main()
{
	freopen("matrix.in", "r", stdin);
	freopen("matrix.out", "w", stdout);
	return zuiyt::work();
}
