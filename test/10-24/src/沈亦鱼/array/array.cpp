#include<cstdio>
#include<algorithm>
using namespace std;
char ch;
int time,n,m,nm,s,ans,ls,a[20][110000][2],b[20],c[110000];
double th=0.08;
void dfs(int k){
	if(k>n){
		int s=0,sum=0;
		for(int i=1;i<=m;i++){
			sum=0;
			for(int j=1;j<=n;j++)
				sum+=a[j][i][b[j]];
			if(sum>nm)sum=n-sum;
			s+=sum;
			if(s>ans)return;
		}
		if(s<ans)ans=s;
		return;
	}
	b[k]=0;
	dfs(k+1);
	b[k]=1;
	dfs(k+1);
}
void dd(int k){
	if(k>n){
		int s=0,sum=0;
		for(int i=1;i<=m;i++){
			sum=0;
			for(int j=1;j<=n;j++)
				sum+=a[j][i][b[j]];
			if(sum>nm)sum=n-sum;
			s+=sum;
			if(s+int(th*c[i+1])>ans)return;
		}
		th=th*1.25;
		if(s<ans)ans=s;
		return;
	}
	b[k]=0;
	dd(k+1);
	b[k]=1;
	dd(k+1);
}
int main(){
	freopen("array.in","r",stdin);
	freopen("array.out","w",stdout);
	scanf("%d %d\n",&n,&m);
	nm=n/2;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			a[i][j][0]=getchar()-48;
			a[i][j][1]=1-a[i][j][0];
		}
		ch=getchar();
	}
	int s=0,sum=0;
	for(int i=m;i>0;i--){
		sum=0;
		for(int j=1;j<=n;j++)
			sum+=a[j][i][0];
		if(sum>nm)sum=n-sum;
		c[i]=c[i+1]+sum;
	}
	ans=c[1];
	if(n<=15&&m<=1000)dfs(1);
	else dd(1);
	printf("%d",ans);
	return 0;
}
