#include<bits/stdc++.h>
using namespace std;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}
int n,x[35],y[35],a[2600][2600];
int main(){
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++){
			a[i][j]=read();
			if(i==j&&a[i][j]>0){
				puts("1");
				printf("%d",i);
				puts(" 0 0");
				return 0;
			}
			if(i>j&&a[i][j]!=a[j][i]){
				puts("2");
				printf("%d %d",i,j);
				puts(" 0");
				return 0;
			}
			for(int k=1;k<=30;k++)
				if(a[i][j]>a[x[k]][y[k]]){
					for(int kk=30;kk>k;kk--){
						x[kk]=x[kk-1];
						y[kk]=y[kk-1];
					}
					x[k]=i;
					y[k]=j;
					break;
				}
		}
	if(n>900){
		for(int i=1;i<=30;i++)
			for(int k=1;k<=n;k++)
				if(max(a[x[i]][k],a[k][y[i]])<a[x[i]][y[i]]){
					puts("3");
					printf("%d %d %d",x[i],y[i],k);
					return 0;
				}
		puts("0");
		return 0;
	}
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++)
			for(int k=1;k<=n;k++)
				if(max(a[i][k],a[k][j])<a[i][j]){
					puts("3");
					printf("%d %d %d",i,j,k);
					return 0;
				}
	puts("0");
	return 0;
}
