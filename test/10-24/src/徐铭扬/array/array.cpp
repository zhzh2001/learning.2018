#include<bits/stdc++.h>
using namespace std;
const int N=1<<18;
int n,m,i,j,a[N],f[N],x,s,ans,cnt;
char s1[N];
int main(){
	freopen("array.in","r",stdin);
	freopen("array.out","w",stdout);
	scanf("%d%d",&n,&m);ans=1e9;
	for (i=0;i<n;i++){
		scanf("%s",s1);
		for (j=0;j<m;j++) a[j]=a[j]<<1|(s1[j]^48);
	}
	for (i=0;i<(1<<n);i++){
		for (x=i,cnt=0;x;x^=x&-x,cnt++);
		f[i]=min(cnt,n-cnt);
	}
	for (i=0;i<(1<<n);i++,ans=min(ans,s))
		for (j=s=0;j<m;j++) s+=f[a[j]^i];
	printf("%d",ans);
}

