#include<bits/stdc++.h>
using namespace std;
int n,i,j,a[2502][2502],k,t;
inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline int rd(){
	int x=0,fl=1;char ch=gc();
	for (;ch<48||ch>57;ch=gc())if(ch=='-')fl=-1;
	for (;48<=ch&&ch<=57;ch=gc())x=(x<<3)+(x<<1)+(ch^48);
	return x*fl;
}
int main(){
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	n=rd();
	for (i=1;i<=n;i++)
		for (j=1;j<=n;j++) a[i][j]=rd();
	for (i=1;i<=n;i++)
		if (a[i][i]){
			printf("1\n%d 0 0",i);
			return 0;
		}
	for (i=1;i<=n;i++)
		for (j=i+1;j<=n;j++)
			if (a[i][j]!=a[j][i]){
				printf("2\n%d %d 0",i,j);
				return 0;
			}
	t=min(n,(int)2e8/n/n);
	for (i=1;i<=n;i++)
		for (j=i+1;j<=n;j++)
			for (k=1;k<=t;k++)
				if (a[i][j]>a[i][k] && a[i][j]>a[k][j]){
					printf("3\n%d %d %d",i,j,k);
					return 0;
				}
	puts("0");
}

