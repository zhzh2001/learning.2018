#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
const int B=20,N=100005,inf=0x3f3f3f3f;
int n,m,mp[B][N],h0[B],h1[B],wa[N],up; char s[N];
inline int solve(int z)
{
	int i,j,tmp=0; memset(wa,0,sizeof wa);
	for(i=1;i<=n;i++)
	{
		if(z&(1<<(i-1)))
		{
			tmp+=h0[i]; for(j=1;j<=m;j++)if(!mp[i][j])wa[j]++;
		}else{tmp+=h1[i];for(j=1;j<=m;j++)if(mp[i][j])wa[j]++;}
	}for(j=1;j<=m;j++)if(wa[j]>up)tmp-=((wa[j]<<1)-n);return tmp;
}
int main()
{
	freopen("array.in","r",stdin);
	freopen("array.out","w",stdout);
	int i,j,re=inf; scanf("%d%d",&n,&m); up=n>>1;
	for(i=1;i<=n;i++)
	{
		scanf("%s",s+1); for(j=1;j<=m;j++){mp[i][j]=s[j]-'0';if(mp[i][j])h1[i]++;else h0[i]++;}
	}for(i=0;i<((1<<n)-1);i++)re=min(re,solve(i)); printf("%d\n",re);
}
