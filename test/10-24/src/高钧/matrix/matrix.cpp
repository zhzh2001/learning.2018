#include <map>
#include <cstdio>
#include <iostream>
using namespace std;
const int L=1000000; char LZH[L],*SSS,*TTT;
inline char gc(){if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);return *SSS++;}
inline int read(){int x=0; char c=gc(); for (;c<'0'||c>'9';c=gc()); for (;c>='0'&&c<='9';c=gc())x=(x<<1)+(x<<3)+c-48;return x;}
const int N=2505,B=6300005;
int n,a[N][N],tot=0,b[N][2];
map<int,bool>mp;
inline void jud1(){int i; for(i=1;i<=n;i++)if(a[i][i]){printf("1\n%d 0 0\n",i); exit(0);}}
inline void jud2(){int i,j;for(i=2;i<=n;i++){for(j=1;j<=i-1;j++)if(a[i][j]!=a[j][i]){printf("2\n%d %d 0\n",i,j); exit(0);}}}
inline void jud3()
{
	int i,j,k;
	for(i=2;i<n;i++)
	{
		for(j=1;j<=i-1;j++)
		{
			k=i+1;
			if(a[i][j]>max(a[j][k],a[i][k])){printf("3\n%d %d %d\n",i,j,k);exit(0);}
			if(a[i][k]>max(a[i][j],a[j][k])){printf("3\n%d %d %d\n",i,k,j);exit(0);}
			if(a[j][k]>max(a[i][j],a[i][k])){printf("3\n%d %d %d\n",j,k,i);exit(0);}
		}
	}
}
inline void foc()
{
	int i,j,k;
	for(i=2;i<=n;i++)
	{
		for(j=1;j<=i-1;j++)
		{
			for(k=1;k<=n;k++)if(a[i][j]>max(a[i][k],a[j][k])){printf("3\n%d %d %d\n",i,j,k);exit(0);}
		}
	}
}
inline bool jud(){mp.clear(); int i,j,ct=0;for(i=1;i<=n;i++){for(j=1;j<=n;j++){if(!mp[a[i][j]])mp[a[i][j]]=1,ct++;}}if(ct>2)return false;return true;}
int main()
{
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	int i,j; n=read();
	for(i=1;i<=n;i++)
	{
		for(j=1;j<=n;j++)a[i][j]=read();
	}jud1(); jud2(); if(jud())return 0*puts("0"); if(n>900)jud3();else foc(); puts("0");
}
