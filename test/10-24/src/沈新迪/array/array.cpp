#include<bits/stdc++.h>
using namespace std;
const int L=1000000;
int mp[100005];
int n,m,ans;
int shu[300005];
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	if (c>='0'&&c<='9')
		x=(x<<1)+(x<<3)+c-48;
	return x;
}
int main()
{
	freopen("array.in","r",stdin);
	freopen("array.out","w",stdout);
	scanf("%d%d",&n,&m);
	ans=n*m;
	for(int i=0;i<n;++i) 
	for(int j=1;j<=m;++j) 
	if(read()) mp[j]|=(1<<i);
	for(int i=1;i<(1<<n);++i) shu[i]=shu[i-(i&-i)]+1;
	for(int i=0;i<(1<<n);++i)
	{
		int sum=0;
		for(int j=1;j<=m;++j)
		{
			int tmp=shu[mp[j]^i];
			sum+=min(tmp,n-tmp);
		}
		ans=min(ans,sum);
	}
	printf("%d",ans);
	return 0;
}
//stdakking
