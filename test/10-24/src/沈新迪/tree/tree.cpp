#include<bits/stdc++.h>
using namespace std;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}
inline void write(int aa)
{
	if(aa<0) putchar('-'),aa=-aa;
	if(aa>9) write(aa/10);
	putchar(aa%10+'0');
	return;
}
int n,q,cnt,toop,now,bj,mx,tmp,qwq,hh;
int tot=1,head[100005],nx[200005],to[200005];
int sz[100005],col[100005],pre[100005],low[100005],zhan[100005];
int dep[100005],dfx[100005],son[100005],top[100005],sum[100005],fa[100005];
int RT[100005],huan[100005],jd[100005];
int bianshu[100005],huanbian,ans;
struct node
{
	int sum[2],lz;
}tta[400005],ttb[400005];
void jia(int aa,int bb)
{
	tot++;
	nx[tot]=head[aa];
	to[tot]=bb;
	head[aa]=tot;
	return;
}
void dfs(int rt,int fa)
{
	++cnt;
	pre[rt]=cnt;
	low[rt]=cnt;
	zhan[++toop]=rt;
	for(int i=head[rt];i;i=nx[i])
	if(i!=(fa^1))
	{
		int yy=to[i];
		if(!pre[yy]) dfs(yy,i),low[rt]=min(low[rt],low[yy]);
		else if(!col[yy]) low[rt]=min(low[rt],pre[yy]);
	}
	if(low[rt]==pre[rt])
	{
		now++;
		while(toop)
		{
			int kk=zhan[toop--];
			sz[now]++;
			col[kk]=now;
			if(kk==rt) break;
		}
	}
	if(sz[now]>mx) mx=sz[now],bj=now;
	return;
}
void dfs1(int rt,int faa)
{
	RT[rt]=qwq;
	fa[rt]=faa;
	dep[rt]=dep[faa]+1;
	sum[rt]=1;
	int mx=0;
	for(int i=head[rt];i;i=nx[i])
	{
		int yy=to[i];//if(rt==1) cout<<c<<endl;
		if(col[yy]==bj||yy==faa) continue;
		dfs1(yy,rt);
		sum[rt]+=sum[yy];
		if(sum[yy]>mx) mx=sum[yy],son[rt]=yy;
	}
	return;
}
void dfs2(int rt)
{
	dfx[rt]=++tmp;
	if(son[rt]) top[son[rt]]=top[rt],dfs2(son[rt]);
	for(int i=head[rt];i;i=nx[i])
	{
		int yy=to[i];
		if(yy==fa[rt]||col[yy]==bj||yy==son[rt]) continue;
		top[yy]=yy;
		dfs2(yy);
	}
	return;
}
void gethuan(int rt,int faa)
{
	if(huan[rt]) return;
	huan[rt]=++hh;jd[hh]=rt;
	for(int i=head[rt];i;i=nx[i])
	{
		int yy=to[i];
		if(col[yy]!=bj||yy==faa) continue;
		gethuan(yy,rt);
	}
	return;
}
void upa(int rt)
{
	tta[rt].sum[0]=tta[rt<<1].sum[0]+tta[rt<<1|1].sum[0];
	tta[rt].sum[1]=tta[rt<<1].sum[1]+tta[rt<<1|1].sum[1];
	return;
}
void bta(int rt,int ll,int rr)
{
	if(ll==rr)
	{
		tta[rt].sum[0]=1;
		return;
	}
	int mid=(ll+rr)>>1;
	bta(rt<<1,ll,mid);
	bta(rt<<1|1,mid+1,rr);
	upa(rt);
	return;
}
void upb(int rt)
{
	ttb[rt].sum[0]=ttb[rt<<1].sum[0]+ttb[rt<<1|1].sum[0];
	ttb[rt].sum[1]=ttb[rt<<1].sum[1]+ttb[rt<<1|1].sum[1];
	return;
}
void btb(int rt,int ll,int rr)
{
	if(ll==rr)
	{
		ttb[rt].sum[0]=1;
		return;
	}
	int mid=(ll+rr)>>1;
	btb(rt<<1,ll,mid);
	btb(rt<<1|1,mid+1,rr);
	upb(rt);
	return;
}
void pusha(int rt)
{
	if(!tta[rt].lz) return;
	tta[rt<<1].lz^=1;swap(tta[rt<<1].sum[0],tta[rt<<1].sum[1]);
	tta[rt<<1|1].lz^=1;swap(tta[rt<<1|1].sum[0],tta[rt<<1|1].sum[1]);
	tta[rt].lz=0;
	return;
}
void bha(int rt,int ll,int rr,int L,int R)
{
	if(ll==L&&rr==R)
	{
		swap(tta[rt].sum[0],tta[rt].sum[1]);
		tta[rt].lz^=1;
		return;
	}
	int mid=(ll+rr)>>1;pusha(rt);
	if(R<=mid) bha(rt<<1,ll,mid,L,R);
	else if(L>mid) bha(rt<<1|1,mid+1,rr,L,R);
	else bha(rt<<1,ll,mid,L,mid),bha(rt<<1|1,mid+1,rr,mid+1,R);
	upa(rt);
	return;
}
void pushb(int rt)
{
	if(!ttb[rt].lz) return;
	ttb[rt<<1].lz^=1;swap(ttb[rt<<1].sum[0],ttb[rt<<1].sum[1]);
	ttb[rt<<1|1].lz^=1;swap(ttb[rt<<1|1].sum[0],ttb[rt<<1|1].sum[1]);
	ttb[rt].lz=0;
	return;
}
void bhb(int rt,int ll,int rr,int L,int R)
{
	if(ll==L&&rr==R)
	{
		swap(ttb[rt].sum[0],ttb[rt].sum[1]);
		ttb[rt].lz^=1;
		return;
	}
	int mid=(ll+rr)>>1;pushb(rt);
	if(R<=mid) bhb(rt<<1,ll,mid,L,R);
	else if(L>mid) bhb(rt<<1|1,mid+1,rr,L,R);
	else bhb(rt<<1,ll,mid,L,mid),bhb(rt<<1|1,mid+1,rr,mid+1,R);
	upb(rt);
	return;
}
int query(int rt,int ll,int rr,int L,int R)
{
	if(ll==L&&rr==R) return tta[rt].sum[1];
	int mid=(ll+rr)>>1;pusha(rt);
	if(R<=mid) return query(rt<<1,ll,mid,L,R);
	else if(L>mid) return query(rt<<1|1,mid+1,rr,L,R);
	else return query(rt<<1,ll,mid,L,mid)+query(rt<<1|1,mid+1,rr,mid+1,R);
}
void update(int x,int y,int noccs)
{
	int X=top[x],Y=top[y];
	while(X!=Y)
	{
		if(dep[X]>dep[Y])
		{
			bha(1,1,n,dfx[X],dfx[x]);
			x=fa[X];X=top[x];
		}
		else
		{
			bha(1,1,n,dfx[Y],dfx[y]);
			y=fa[Y];Y=top[y];
		}
	}
	if(x!=y)
	{
		if(dep[x]>dep[y]) swap(x,y);
		bha(1,1,n,dfx[x]+1,dfx[y]);
	}//cout<<sz[1]<<endl;
	bianshu[noccs]=query(1,1,n,dfx[noccs],dfx[noccs]+sum[noccs]-1);
	return;
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();q=read();ans=n;
	for(int i=1;i<=n;++i)
	{
		int x=read(),y=read();
		jia(x,y);
		jia(y,x);
	}
	dfs(1,0);
	for(int i=1;i<=n;++i) if(col[i]==bj) 
	{
		qwq=i;
		dfs1(i,i);
		top[i]=i;
		dfs2(i);
	}
	gethuan(qwq,qwq);jd[0]=jd[hh];jd[hh+1]=jd[1];
	bta(1,1,n);btb(1,1,hh);
	int cs=0;
	while(q--)
	{
		cs++;
		int x=read(),y=read(),xxx=x,ff=1;
		if(RT[x]!=RT[y])
		{
			ans-=(sz[RT[x]]-1-bianshu[RT[x]]);
			update(RT[x],x,RT[x]);
			ans+=(sz[RT[x]]-1-bianshu[RT[x]]);
			ans-=(sz[RT[y]]-1-bianshu[RT[y]]);
			update(RT[y],y,RT[y]);
			ans+=(sz[RT[y]]-1-bianshu[RT[y]]);
			ans-=(hh-ttb[1].sum[1]);
			x=RT[x],y=RT[y];
			if(huan[x]>huan[y]) swap(x,y),ff=-1;
			int cha=huan[y]-huan[x];
			if(cha*2<hh||(cha*2==hh&&jd[huan[xxx]+ff]>jd[huan[xxx]-ff])) {bhb(1,1,hh,huan[x],huan[y]-1);}
			else 
			{
				if(huan[x]!=1) bhb(1,1,hh,1,huan[x]-1);
				bhb(1,1,hh,huan[y],hh);
			}
			ans+=(hh-ttb[1].sum[1]);
		}
		else
		{
			ans-=(sz[RT[x]]-1-bianshu[RT[x]]);
			update(x,y,RT[x]);
			ans+=(sz[RT[x]]-1-bianshu[RT[x]]);
		}
		write(ans);puts("");
	}
	return 0;
}
//stdakking
