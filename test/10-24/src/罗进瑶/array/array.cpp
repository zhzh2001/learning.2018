#include<bits/stdc++.h>
typedef int ll;
using namespace std;
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;
ch = getchar();}while(ch >= '0' && ch <= '9'){x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) x = -x, putchar('-');if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("array.in", "r", stdin);
	freopen("array.out", "w", stdout);
}
const int N = 101000;
int n, m;
char s[N];
bool a[20][N];
int c[N], d[N];
inline int solve(int s) {
	for(register int i = 1; i <= m; ++i) c[i] = d[i];
	for(register int i = 1; i <= n; ++i) {
		if(s & (1 << i - 1)) {
			for(register int j = 1; j <= m; ++j)
				c[j] += (a[i][j] ^ 1) - a[i][j];		
		}
	}
	int res = 0;
	for(register int i = 1; i <= m; ++i) res += min(c[i], n - c[i]);
	return res;
}
int main() {
	int t1=clock();
	setIO();
	int ans = 0;
	n = read(), m = read();
	for(register int i = 1; i <= n; ++i) {
		scanf("%s", s + 1);
		for(register int j = 1; j <= m; ++j)
			a[i][j] = s[j] - '0', ans += a[i][j], d[j] += a[i][j];
	}
	int B = 1 << n;
	for(register int S = 0; S < B; ++S) {
		if(clock()-t1 > 1700)break;
		ans = min(ans, solve(S));
	}
	writeln(ans);
	return 0;
}

/*

                       |                   
                __\--__|_ 
II=======OOOOO[/ ��02 ___|           
          _____\______|/-----. 
        /____________________|              
         \����������/ 
           ~~~~~~~~~~~~~~~~ 
*/
