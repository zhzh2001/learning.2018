#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;
ch = getchar();}while(ch >= '0' && ch <= '9'){x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) x = -x, putchar('-');if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("array.in", "r", stdin);
	freopen("array.out", "w", stdout);
}
const int N = 101000;
int n, m;
char s[N];
bool a[20][N], b[20][N];
int solve(int s, int t) {
	for(int i = 1; i <= n; ++i)
		for(int j = 1; j <= m; ++j)
			b[i][j] = a[i][j];
	for(
}
int main() {
	int ans = 0;
	n = read(), m = read();
	for(int i = 1; i <= n; ++i) {
		scanf("%s", s + 1);
		for(int j = 1; j <= m; ++j)
			a[i][j] = s[j] - '0', ans += a[i][j];
	}
	int B = 1<<n;
	int BT = 1<<m; 
	for(int S = 0; S < B; ++S)
		for(int T = 0; T < BT; ++T)
			ans = min(ans, solve(S, T));
	writeln(ans);
	return 0;
}
