#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;
ch = getchar();}while(ch >= '0' && ch <= '9'){x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) x = -x, putchar('-');if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("array.in", "r", stdin);
	freopen("array.out", "w", stdout);
}
const int N = 101000;
int n, m;
char s[N];
bool a[20][N], c[20][N];
void cr(int x) {
	for(int i = 1;  i <= m; ++i)
		a[x][i] ^= 1;
}
void cc(int x) {
	for(int i = 1;  i <= n; ++i)
		a[i][x] ^= 1; 
}
int b[5];
int main() {
	srand(time(NULL));
	int ans = 0;
	n = read(), m = read();
	for(int i = 1; i <= n; ++i) {
		scanf("%s", s + 1);
		for(int j = 1; j <= m; ++j)
			c[i][j] = a[i][j] = s[j] - '0', ans += a[i][j];
	}
	for(int i = 1; i <= 4; ++i)
		b[i] = rand() % (n + m) + 1;
	int ct = 0;
	do {
		printf("CASE #%d:\n", ++ct); 
		for(int T = 1; T <= 4; ++T) {
			if(b[T] > n) cc(b[T] - n);
			else cr(b[T]);
		}		
		for(int i = 1; i <= n; ++i, puts(""))
			for(int j = 1; j <= m; ++j)
				putchar(a[i][j] + '0');
		puts("");
		for(int i = 1; i <= n; ++i)
			for(int j = 1; j <= m; ++j)
				a[i][j] = c[i][j];
	}while(next_permutation(b + 1, b + 1 + 46));

	return 0;
}
