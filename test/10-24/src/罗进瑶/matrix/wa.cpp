#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;
ch = getchar();}while(ch >= '0' && ch <= '9'){x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) x = -x, putchar('-');if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("matrix.in", "r", stdin);
	freopen("matrix.out", "w", stdout);
} 
const int N = 2520;
int a[N][N];
int n; 
int q[N], head, tail;
void solve() {
	for(int i = 1; i <= n; ++i) {
		head = 1, tail = 0;
		for(int j = 1; j <= n; ++j) {
			while(head <= tail && a[i][j] > a[i][q[tail]]) --tail;
			if(!(head <= tail) && j > 1) {
				printf("3\n%d %d %d\n", i, j, q[tail]);
				return;
			}
			q[++tail] = j;
		}
	}
	for(int j = 1; j <= n; ++j) {
		head = 1, tail = 0;
		for(int i = 1; i <= n; ++i) {
			while(head <= tail && a[i][j] > a[q[tail]][j]) --tail;
			if(!(head <= tail) && i > 1) {
				printf("3\n%d %d %d\n", i, j, q[tail]);
				return;
			}
			q[++tail] = i;
		}
	}
}
int main() {
	setIO();
	n =  read();
	for(int i = 1; i <= n; ++i)
		for(int j = 1; j <= n; ++j)
			a[i][j] = read();
	for(int i = 1; i <= n; ++i)
		if(a[i][i]) {
			puts("1");
			printf("%d %d 0\n", i, i, 0);
			return 0;
		}
	for(int i = 1; i <= n; ++i)
		for(int j = 1; j <= n; ++j)
			if(a[i][j] ^ a[j][i]) {
				puts("2");
				printf("%d %d %d\n", i, j, 0); 
			}
	solve();
	return 0;
}
