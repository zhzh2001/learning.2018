#include<bits/stdc++.h>
#define pb push_back
typedef int ll;
using namespace std;
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;
ch = getchar();}while(ch >= '0' && ch <= '9'){x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) x = -x, putchar('-');if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("tree.in", "r", stdin);
	freopen("tree.out", "w", stdout);
}
const int N = 110000;
struct Edge {
	int u, v, nxt;	
}e[N * 2];
int head[N], en;
void add(int x, int y) {
	e[++en].u = x, e[en].v = y,e[en].nxt = head[x];
	head[x] = en;
}
int n, Q;
int vis[N], d[N];
int cir[N], pre[N];
void go(int x, int F) {
	cir[x] = 1;
	if(x == F) return;
	go(pre[x], F);
}
void dfs(int x, int F) {
	vis[x] = 1;
	d[x] = d[F] + 1;
	for(int i = head[x]; i; i = e[i].nxt) {
		int y = e[i].v;
		if(y == F) continue;
		pre[y] = x;
		if(vis[y]) {
			go(x, y);
			return ;	
		}
		dfs(y,x);
	}
}
int bel[N], ed[N];
void col(int x, int F, int rt) {
	bel[x] = rt;
	for(int i = head[x]; i;i = e[i].nxt) {
		int y = e[i].v;
		if(y == F) continue;
		ed[y] = i;
		col(y,x, rt);
	}
}
void init(int x) {
	for(int i = head[x]; i;i = e[i].nxt) {
		int y = e[i].v;
		if(cir[y]) init(y);
		else col(y, x, x);
	}
}
bool ok[N];
void bf(int x) {
	while(x != bel[x]) {
		ok[ed[x]] = ok[ed[x] ^ 1] = ok[ed[x]] ^ 1;
		x = e[ed[x]].u;
	}
}
vector<int>path;
void solve(int x, int y) {
	
}
int main() {
/*	int x, y;
	n = read(), Q = read();
	en = 1;
	for(int i = 1; i <= n; ++i) {
		x = read(), y = read();
		add(x, y);
		add(y, x);
	}for(int i = 1; i <= en; ++i) ok[i] = 1;
	dfs(1, 0);
	memset(vis, 0, sizeof vis);
	int st = 0;
	for(int i = 1; i <= n; ++i) if(cir[i]) {
		init(i);
		st = i;
		break;
	}
	while(Q--) {
		int x = read(), y = read();
		bf(x);
		bf(y);
		solve(x, y);
	}*/
	setIO();
	puts("0"); 
	return 0;
}
