#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
#define pp(x,y) cout << "pp: " << x << " " << y << endl;
namespace IO{
	const int L=1<<15;char ibuf[L|1],*iS=ibuf,*iT=ibuf,obuf[L|1],*oS=obuf,*oT=obuf+L,c,st[66];int tp=0,f;
	inline char gc(){if(iS==iT) iT=(iS=ibuf)+fread(ibuf,sizeof(char),L,stdin);return (*iS++);}
	inline void flush(){fwrite(obuf,sizeof(char),oS-obuf,stdout);oS = obuf;}
	inline void pc(char c){*oS++=c;if(oS==oT) flush();}
	#define gc getchar
	#define pc putchar
	inline ll read(){ll x=0;f=1,c=gc();for(;!isdigit(c);c=gc())if(c=='-')f=-1;for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c&15);return x*f;}
	inline void write(ll x){if(!x) pc('0');if(x<0) pc('-'),x=-x;while(x) st[++tp]=x%10+'0',x/=10;while(tp) pc(st[tp--]);}
	inline void writeln(ll x){write(x);pc('\n');}
	inline void wri(ll x){write(x),pc(' ');}
	struct IOflusher{~IOflusher(){flush();}}_ioflusher_;
}using IO::read;using IO::write;using IO::writeln;using IO::wri;//using IO::gc;using IO::pc;
#define rd read
const int maxn = 2505;
int fa[maxn],a[maxn][maxn],f[maxn];
inline int find(int x){
    return x==fa[x] ? x : fa[x] = find(fa[x]);
}
struct Edge{
    int to,nxt,dist;
    Edge(){}
    Edge(int to,int nxt,int dist):
        to(to),nxt(nxt),dist(dist){}
}edge[maxn * 2];
struct node{
    int x,y,z;
    bool operator < (const node &w) const{
        return z<w.z;
    }
}E[maxn*maxn/2];int m;
int first[maxn],nume;
int n;
void Addedge(int a,int b,int c){
    edge[nume] = Edge(b,first[a],c);
    first[a] = nume++;
}
void dfs(int u){
    for(int e=first[u];~e;e=edge[e].nxt){
        int v=edge[e].to;
        if(v==fa[u]) continue;
        f[v] = max(f[v],edge[e].dist);
        fa[v] = u;
        dfs(v);
    }
}
void check(int i,int j,int k){
    if(a[i][j] > max(a[i][k],a[k][j])){
        printf("%d\n",3);
        printf("%d %d %d\n",i,j,k);
        exit(0);
    }
}
int main(){
    freopen("matrix.in","r",stdin);
    freopen("matrix.out","w",stdout);
    n = rd();  m = 0;
    Rep(i,1,n)Rep(j,1,n){
        a[i][j] = rd();
        if(i<j) E[++m] = (node){i,j,a[i][j]};
    }
    Rep(i,1,n){
        if(a[i][i] != 0){
            printf("%d\n",1);
            printf("%d %d %d\n",i,0,0);
            return 0;
        }
    }
    Rep(i,1,n){
        Rep(j,1,i-1){
            if(a[i][j] != a[j][i]){
                printf("%d\n",2);
                printf("%d %d %d\n",i,j,0);
            }
        }
    }
    sort(E+1,E+1+m);
    Rep(i,1,n) fa[i] = i;
    int cnt = 0;
    mem(first,-1);nume = 0;
    Rep(i,1,m){
        int x = find(E[i].x),y = find(E[i].y);
        if(x != y){
            Addedge(x,y,E[i].z);
            Addedge(y,x,E[i].z);
            fa[y] = x;
            cnt--;
            if(cnt==n-1) break;
        }
    }
    Rep(i,1,n){
        f[i] = 0;fa[i] = i;
        dfs(i);
        Rep(j,1,n){
            if(f[j] < a[i][j]){
                int pos = j;
                while(fa[pos] != i){
                    check(i,fa[pos],pos);
                    pos = fa[pos];
                }
            }
        }
    }
    puts("0");
    return 0;
}
/*

A[u][v] <=
max(A[u][a0] , A[a0][a1] , A[a1][a2]……A[an][v])
也就是使得u,v相连的最大值。
也就是要小于最小的最大值。
快速得到最小的最大值
并查集按秩合并
时间复杂度O(); 
*/
