#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
#define pp(x,y) cout << "pp: " << x << " " << y << endl;
namespace IO{
	const int L=1<<15;char ibuf[L|1],*iS=ibuf,*iT=ibuf,obuf[L|1],*oS=obuf,*oT=obuf+L,c,st[66];int tp=0,f;
	inline char gc(){if(iS==iT) iT=(iS=ibuf)+fread(ibuf,sizeof(char),L,stdin);return (*iS++);}
	inline void flush(){fwrite(obuf,sizeof(char),oS-obuf,stdout);oS = obuf;}
	inline void pc(char c){*oS++=c;if(oS==oT) flush();}
	#define gc getchar
	#define pc putchar
	inline ll read(){ll x=0;f=1,c=gc();for(;!isdigit(c);c=gc())if(c=='-')f=-1;for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c&15);return x*f;}
	inline void write(ll x){if(!x) pc('0');if(x<0) pc('-'),x=-x;while(x) st[++tp]=x%10+'0',x/=10;while(tp) pc(st[tp--]);}
	inline void writeln(ll x){write(x);pc('\n');}
	inline void wri(ll x){write(x),pc(' ');}
	struct IOflusher{~IOflusher(){flush();}}_ioflusher_;
}using IO::read;using IO::write;using IO::writeln;using IO::wri;//using IO::gc;using IO::pc;
#define rd read
const int mod = 1e9+7;
const int inv2 = (mod+1) / 2;
int n,m;
char a[19][100005];
int cnt[1 << 18],v[1 << 18],ans[1 << 18];
void FWT(int a[],int n,int flag){
    for(int i=1;i<n;i<<=1){
        for(int j=0;j<n;j+=(i<<1)){
            for(int k=0;k<i;++k){
                int x = a[j+k],y = a[i+j+k];
                if(flag==1){
                    a[j+k] = 1ll * (x+y) % mod;
                    a[i+j+k] = 1ll * (x-y) % mod;
                } else{
                    a[j+k] = 1ll * (x+y) * inv2 % mod;
                    a[i+j+k] = 1ll * (x-y) * inv2 % mod;
                }
            }
        }
    }
}
int main(){
    freopen("array.in","r",stdin);
    freopen("array.out","w",stdout);
    n = rd(),m = rd();
    rep(i,0,n) scanf("%s",a[i]);
    mem(cnt,0);
    rep(j,0,m){
        int S = 0;
        rep(i,0,n) S=(S<<1)+a[i][j]-'0';
        cnt[S]++;
    }
    rep(i,0,1<<n) v[i] = v[i>>1] + (i&1);
    rep(i,0,1<<n) v[i] = min(v[i],n-v[i]);
    FWT(cnt,1<<n,1);
    FWT(v,1<<n,1);
    rep(i,0,1<<n) ans[i] = 1ll * cnt[i] * v[i] % mod;
    FWT(ans,1<<n,-1);
    ll answ = n*m;;
    rep(i,0,1<<n) answ = min(answ,(ll)ans[i]);
    writeln(answ);
    return 0;
}
