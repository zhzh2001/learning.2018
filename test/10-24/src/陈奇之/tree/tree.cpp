#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
#define pp(x,y) cout << "pp: " << x << " " << y << endl;
namespace IO{
	const int L=1<<15;char ibuf[L|1],*iS=ibuf,*iT=ibuf,obuf[L|1],*oS=obuf,*oT=obuf+L,c,st[66];int tp=0,f;
	inline char gc(){if(iS==iT) iT=(iS=ibuf)+fread(ibuf,sizeof(char),L,stdin);return (*iS++);}
	inline void flush(){fwrite(obuf,sizeof(char),oS-obuf,stdout);oS = obuf;}
	inline void pc(char c){*oS++=c;if(oS==oT) flush();}
	#define gc getchar
	#define pc putchar
	inline ll read(){ll x=0;f=1,c=gc();for(;!isdigit(c);c=gc())if(c=='-')f=-1;for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c&15);return x*f;}
	inline void write(ll x){if(!x) pc('0');if(x<0) pc('-'),x=-x;while(x) st[++tp]=x%10+'0',x/=10;while(tp) pc(st[tp--]);}
	inline void writeln(ll x){write(x);pc('\n');}
	inline void wri(ll x){write(x),pc(' ');}
	struct IOflusher{~IOflusher(){flush();}}_ioflusher_;
}using IO::read;using IO::write;using IO::writeln;using IO::wri;//using IO::gc;using IO::pc;
const int maxn = 100050;
namespace SGT{
    int T[maxn<<2],tag[maxn<<2];
    #define lson (o<<1)
    #define rson (o<<1|1)
    #define mid ((l+r)>>1)
    void pushdown(int o,int l,int r){
        if(tag[o]){
            tag[lson] ^= 1;
            tag[rson] ^= 1;
            T[lson] = (mid - l + 1) - T[lson];
            T[rson] = (r - (mid+1) +1) - T[rson];
        }
        tag[o] = 0;
    }
    inline int query(int o,int l,int r,int x,int y){
        if(l==x&&r==y) return T[o];
        int ans = 0;
        pushdown(o,l,r);
        if(y<=mid) ans=query(lson,l,mid,x,y); else
        if(mid+1<=x) ans=query(rson,mid+1,r,x,y); else
        ans = query(lson,l,mid,x,mid)+query(rson,mid+1,r,mid+1,y);
        return ans;
    }
    inline void modify(int o,int l,int r,int x,int y){
        if(l==x&&r==y){
            T[o] = (r-l+1) - T[o];
            tag[o] ^= 1;
            return ;
        }
        pushdown(o,l,r);
        if(y<=mid) modify(lson,l,mid,x,y); else
        if(mid+1<=x) modify(rson,mid+1,r,x,y); else
        modify(lson,l,mid,x,mid),modify(rson,mid+1,r,mid+1,y);
        T[o] = T[lson] + T[rson];
    }
}
using SGT::modify;
using SGT::query;

struct Edge{
    int to,nxt,id;
    Edge(){}
    Edge(int to,int nxt,int id):to(to),nxt(nxt),id(id){}
}edge[maxn * 2];
int first[maxn],nume = 0;
void Addedge(int a,int b,int c){
    edge[nume] = Edge(b,first[a],c);
    first[a] = nume++;
}


#define rd read
int posC[maxn];//对于环上的点，得到它在环的位置
int C[maxn];//环
int bel[maxn];//每个点维护一条边
int ans = 0;
int fa[maxn],wson[maxn],top[maxn],Ipos[maxn];
int size[maxn],pos[maxn],dfsclk=0;
int n,Q;

namespace pre_work{
int vis[maxn];
int fae[maxn],fa[maxn];
bool find_c(int u){
    vis[u]=1;
    for(int e=first[u];~e;e=edge[e].nxt){
        int v=edge[e].to;
        if(v==fa[u]) continue;
        if(vis[v]){
            C[0] = 0;
            int x = u;
            while(x != v){
                C[++C[0]] = x;bel[x] = fae[x];
                posC[x]=C[0];
                x = fa[x];
            }
            C[++C[0]] = v;bel[v] = edge[e].id;
            posC[v]=C[0];
            return true;
        }
        fae[v] = edge[e].id;
        fa[v] = u;
        if(find_c(v)) return true;
    }
    return false;
}
}

int ftop[maxn],dep[maxn];
void dfs(int u,int ft){
    ftop[u] = ft;
    size[u]=1;wson[u]=0;
    for(int e=first[u];~e;e=edge[e].nxt){
        int v=edge[e].to;
        if(v==fa[u]) continue;
        fa[v]=u;
        bel[v]=edge[e].id;
        dep[v] = dep[u] + 1;
        dfs(v,ft);
        size[u]+=size[v];
        if(!wson[u]||size[v]>size[wson[u]])
            wson[u]=v;
    }
}
void divide(int u,int chain){
    pos[u] = ++dfsclk;
    top[u] = chain;
    Ipos[pos[u]] = u;
    if(wson[u]) divide(wson[u],chain);
    for(int e=first[u];~e;e=edge[e].nxt){
        int v=edge[e].to;
        if(v==wson[u] || v==fa[u]) continue;
        divide(v,v);
    }
}
void init(int u,int last){
    dep[u] = 0;
    fa[u] = last;
    dfs(u,u);divide(u,u);
}


   
void reverse(int l,int r){
//    printf("reverse(%d %d)\n",l,r);
/*
    Rep(i,l,r){
        printf("It's reverse(%d)\n",Ipos[i]);
    }
*/    //l <= r
    int len = r - l + 1;
    int one = query(1,1,n,l,r);
    ans += one;
    ans -= (len - one);
        //原来有这么one个1，len-one个0
    modify(1,1,n,l,r); 
}
int solve(int u){
//    printf("solve(%d)\n",u);
    for(;;){
        reverse(pos[top[u]],pos[u]);
        u = fa[top[u]];
        if(pos[u] <= C[0]) return u;//到环上 
    }
}
void wzpak(int x,int y){
//    puts("??");
    for(;top[x]!=top[y];){
//        printf("[%d %d]\n",top[x],top[y]);
        if(dep[fa[top[x]]] > dep[fa[top[y]]]){
            reverse(pos[top[x]],pos[x]);
            x=fa[top[x]];
        } else{
            reverse(pos[top[y]],pos[y]);
            y=fa[top[y]];
        }
//        printf("{%d %d}\n",x,y);
    }
    if(dep[x] > dep[y]) swap(x,y);
    if(pos[x]+1<=pos[y]) reverse(pos[x]+1,pos[y]);
}
void rev(int x,int y){
    if(x <= y){
        reverse(x,y);
    } else{
        reverse(x,C[0]);
        reverse(1,y);
    }
}

int pre(int x){
    return x==1 ? C[0] : x-1;
}
int suf(int x){
    return x==C[0] ? 1 : x+1;
}
void fuck(int S,int T){
    if(S==T) return ;
    int len = max(pos[T],pos[S]) - min(pos[S],pos[T]);
    if(len == C[0] / 2){
//        puts("HERE");
        //int suf = S==C[0] ? 1 : S+1;
        /*debug(bel[S]);
        debug(bel[C[pre(pos[S])]]);*/
        if(C[pre(pos[S])] < C[suf(pos[S])] || (C[pre(pos[S])] == C[suf(pos[S])] && bel[C[pre(pos[S])]] < bel[S])){
            rev(pos[T],pre(pos[S]));
            //T,T+1……S-1 
        } else{
            rev(pos[S],pre(pos[T]));
            //S,S+1,S+2……T-1
        }
    } else{
        if(pos[S] > pos[T]) swap(S,T);
        if(len < C[0] / 2){
//            puts("HERE");
            rev(pos[S],pre(pos[T]));
        } else{
//            puts("HERE");
  //          debug(T);
    //        debug(pos[T]);
            rev(pos[T],pre(pos[S]));
        }
    }
}
int main(){
    freopen("tree.in","r",stdin);
    freopen("tree.out","w",stdout);
    n = rd(),Q = rd();
    ans = n;
    mem(first,-1);nume = 0;
    Rep(i,1,n){
        int a = rd(),b = rd();
        Addedge(a,b,i);
        Addedge(b,a,i);
    }
    
    mem(pre_work::vis,0);
    pre_work::fa[1] = 0;
    pre_work::find_c(1);
    mem(pos,0);
    dfsclk = 0;
    Rep(i,1,C[0]){
        pos[C[i]] = ++dfsclk;
    }
//Rep(i,1,C[0]){
//    printf("{%d }",C[i]);
//}puts("");
    Rep(i,1,C[0]){
        int u = C[i];
        dep[u] = -1;
        for(int e=first[u];~e;e=edge[e].nxt){
            int v=edge[e].to;
            if(pos[v]) continue;
            bel[v]=edge[e].id;
            init(v,u);
        }
    }
  //  Rep(i,1,n){
//        printf("%d %d\n",i,pos[i]);
  //  }
//    return 0;
    
    for(int i=1;i<=Q;++i){
//        debug(i);
        int S = rd(),T = rd();
        if(pos[S] > C[0] && pos[T] > C[0] && ftop[S]==ftop[T]){
            wzpak(S,T);
            //在同一个子树内！ 
        } else{
            if(pos[S] > C[0]) S = solve(S);//移动到环上 
            if(pos[T] > C[0]) T = solve(T);//先移动到环上 
            fuck(S,T);
        }
        if(query(1,1,n,1,C[0]) == C[0]){
            writeln(ans-1);
        }//环上的边全部存在
        else {
            writeln(ans);
        }
    }
    return 0;
}
