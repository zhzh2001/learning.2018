#include<bits/stdc++.h>
using namespace std;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if(SSS==TTT)
		TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline int read(){
	int x=0;
	char c=gc();
	for(;c<'0'||c>'9';c=gc());
	for(;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}
int n;
int mp[2501][2501];
int main(){
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			mp[i][j]=read();
	for(int i=1;i<=n;i++)
		if(mp[i][i])
			return printf("1\n%d 0 0",i),0;
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++)
			if(mp[i][j]!=mp[j][i])
				return printf("2\n%d %d 0",i,j),0;
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++)
			for(int k=1;k<=n;k++)
				if(mp[i][j]>max(mp[i][k],mp[k][j]))
					return printf("3\n%d %d %d",i,j,k),0;
	printf("0");
	return 0;
}
