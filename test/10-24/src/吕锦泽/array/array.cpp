#include<bits/stdc++.h>
#define ll int
#define inf (1<<30)
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline int read(){
	int x=0; char c=getchar();
	for (;c<'0'||c>'9';c=getchar());
	for (;c>='0'&&c<='9';c=getchar())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}
ll n,M,m,f[2][300000],ans=inf,sum[300000],now; char a[20][100005];
ll get(ll x){
	ll num=0;
	for (;x;x>>=1) num+=x&1;
	return num;
}
int main(){
	freopen("array.in","r",stdin);
	freopen("array.out","w",stdout);
	n=read(); m=read(); M=(1<<n)-1;
	rep(i,0,M) sum[i]=get(i);
	rep(i,1,n) scanf("%s",a[i]+1);
	rep(i,1,m){
		ll tmp=0,cnt=0; now^=1;
		rep(j,1,n) tmp|=(a[j][i]=='1')*(1<<(j-1));
		rep(j,0,(1<<n)-1)
			f[now][j]=min(f[now^1][j]+sum[j^tmp],f[now^1][j]+sum[j^(tmp^M)]);
	}
	rep(i,0,(1<<n)-1) ans=min(f[now][i],ans);
	printf("%d",ans);
}
