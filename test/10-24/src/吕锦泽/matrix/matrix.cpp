#include<bits/stdc++.h>
#define ll int
#define N 2505
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}
ll n,a[N][N];
ll get(ll l,ll r) { return l+rand()%(r-l+1); }
ll judge(ll i,ll j,ll k) { return a[i][j]>max(a[i][k],a[k][j]); }
int main(){
	srand(time(0));
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	n=read();
	rep(i,1,n){
		rep(j,1,n){
			a[i][j]=read();
			if (i>j&&a[i][j]!=a[j][i]) return printf("2\n%d %d 0",i,j)&0;
		}
		if (a[i][i]) return printf("1\n%d 0 0",i)&0;
	}
	ll t1=get(1,n),t2=get(1,n);
	rep(k,t1,n) per(i,n,1){
		per(j,t2-1,1) if (judge(i,j,k)) return printf("3\n%d %d %d",i,j,k)&0;
		rep(j,t2,n) if (judge(i,j,k)) return printf("3\n%d %d %d",i,j,k)&0;
	}
	per(k,t1-1,1) rep(i,1,n){
		rep(j,t2,n) if (judge(i,j,k)) return printf("3\n%d %d %d",i,j,k)&0;
		per(j,t2-1,1) if (judge(i,j,k)) return printf("3\n%d %d %d",i,j,k)&0;
	}
	puts("0");
}
