#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
const int N=2503,md=100000;
int n,a[N][N],m;
struct edge{
	int i,j,v;
	edge(){
		i=j=v=0;
	}
	edge(int _i,int _j,int _v){
		i=_i;
		j=_j;
		v=_v;
	}
}p[N*N/2],e[N*N/2];
int en[100003],ne[N*N/2],et;
inline void add(int p,edge v){
	et++;
	e[et]=v;
	ne[et]=en[p];
	en[p]=et;
}
int f[N],c[N];
int getf(int v){
	if(f[v]==v)return v;
	return f[v]=getf(f[v]);
}
inline void addf(int a,int b){
	a=getf(a);
	b=getf(b);
	if(a==b)return;
	if(c[a]>c[b])swap(a,b);
	f[a]=b;
	c[b]+=c[a];
}
void prt(int i,int j){
	for(int k=1;k<=n;k++)if(a[i][j]>max(a[i][k],a[k][j])){
		printf("3\n%d %d %d\n",i,j,k);
		return;
	}
}
int main(){
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++){
			a[i][j]=read();
		}
		if(a[i][i]!=0){
			printf("1\n%d 0 0\n",i);
			return 0;
		}
	}
	m=0;
	for(int i=1;i<=n;i++){
		for(int j=i+1;j<=n;j++){
			p[++m]=edge(i,j,a[i][j]);
			if(a[i][j]!=a[j][i]){
				printf("2\n%d %d 0\n",i,j);
				return 0;
			}
		}
	}
	et=0;
	for(int i=0;i<md;i++){
		en[i]=-1;
	}
	for(int i=1;i<=m;i++){
		add(p[i].v%md,p[i]);
	}
	m=0;
	for(int i=md-1;i>=0;i--){
		for(int j=en[i];j!=-1;j=ne[j]){
			p[++m]=e[j];
		}
	}
	et=0;
	for(int i=0;i<md;i++){
		en[i]=-1;
	}
	for(int i=1;i<=m;i++){
		add(p[i].v/md,p[i]);
	}
	m=0;
	for(int i=0;i<md;i++){
		for(int j=en[i];j!=-1;j=ne[j]){
			p[++m]=e[j];
		}
	}
	p[0].v=p[1].v-1;
	for(int i=1;i<=n;i++){
		f[i]=i;
		c[i]=1;
	}
	for(int i=1,j=1;i<=m;i++){
		if(p[i].v!=p[i-1].v){
			for(;j<i;j++)addf(p[j].i,p[j].j);
		}
		if(getf(p[i].i)==getf(p[i].j)){
			prt(p[i].i,p[i].j);
			return 0;
		}
	}
	puts("0");
	return 0;
}
