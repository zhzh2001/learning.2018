#include <iostream>
#include <algorithm>
#include <cstdio>
#include <bitset>
using namespace std;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
inline int rc(){
	int dd;
	while(!isgraph(c))dd;
	return c;
}
#undef dd
int n,m,ans,tot,x;
bitset<18>a[100003];
int main(){
	freopen("array.in","r",stdin);
	freopen("array.out","w",stdout);
	n=read();
	m=read();
	for(int i=0;i<n;i++){
		for(int j=1;j<=m;j++){
			a[j][i]=rc()-'0';
		}
	}
	ans=n*m;
	for(int i=0;i<(1<<n);i++){
		tot=0;
		bitset<18>pi=i;
		for(int j=1;j<=m;j++){
			x=(a[j]^pi).count();
			tot+=min(x,n-x);
		}
		ans=min(ans,tot);
	}
	printf("%d\n",ans);
	return 0;
}
