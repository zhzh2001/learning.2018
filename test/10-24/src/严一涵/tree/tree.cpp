#include <iostream>
#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
int n,m,ans;
int lis[100003],lt,tp[100003];
int tf[100003],td[100003],ro[100003];
bool v[100003];
int f[100003];
void init(){for(int i=1;i<=n;i++)f[i]=i;}
int getf(int v){return f[v]==v?v:(f[v]=getf(f[v]));}
void add(int a,int b){f[getf(a)]=getf(b);}
vector<int>e[100003];
bool dfs(int u,int fa){
	if(u==lis[2])return 1;
	for(vector<int>::iterator i=e[u].begin();i!=e[u].end();i++){
		if(*i==fa)continue;
		if(dfs(*i,u)){
			lis[++lt]=u;
			return 1;
		}
	}
	return 0;
}
void rf(int u,int fa,int tde){
	tf[u]=fa;
	td[u]=tde;
	ro[u]=ro[fa];
	for(vector<int>::iterator i=e[u].begin();i!=e[u].end();i++){
		if(*i==fa||tp[*i]>0)continue;
		rf(*i,u,tde+1);
	}
}
void calt(int x,int y){
	while(x!=y){
		if(td[x]<td[y])swap(x,y);
		v[x]=!v[x];
		if(v[x])ans--;else ans++;
		x=tf[x];
	}
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	m=read();
	init();
	lt=1;
	for(int i=1;i<=n;i++){
		static int x,y;
		x=read();
		y=read();
		if(getf(x)==getf(y)){
			lis[0]=x;
			lis[1]=y;
		}else{
			add(x,y);
			e[x].push_back(y);
			e[y].push_back(x);
		}
	}
	dfs(lis[0],-1);
	for(int i=1;i<=lt;i++){
		tp[lis[i]]=i;
	}
	for(int i=1;i<=lt;i++){
		ro[lis[i]]=lis[i];
		rf(lis[i],lis[i],0);
	}
	ans=n;
	for(int i=1;i<=m;i++){
		static int x,y;
		x=read();
		y=read();
		if(ro[x]==ro[y]){
			calt(x,y);
		}else{
			calt(x,ro[x]);
			calt(y,ro[y]);
			x=ro[x];
			y=ro[y];
			if(tp[x]>tp[y])swap(x,y);
			if(tp[y]-tp[x]<tp[x]+n-tp[y]||(tp[y]-tp[x]==tp[x]+n-tp[y]&&lis[(x==1?lt:x-1)]<lis[x+1])){
				for(int i=x;i<y;i++){
					v[lis[i]]=!v[lis[i]];
					if(v[lis[i]])ans--;
					else ans++;
				}
			}else{
				for(int i=y;i<=n;i++){
					v[lis[i]]=!v[lis[i]];
					if(v[lis[i]])ans--;
					else ans++;
				}
				for(int i=1;i<x;i++){
					v[lis[i]]=!v[lis[i]];
					if(v[lis[i]])ans--;
					else ans++;
				}
			}
		}
		bool f=1;
		for(int i=1;i<=lt;i++)f&=v[lis[i]];
		printf("%d\n",ans+f);
	}
	return 0;
}
