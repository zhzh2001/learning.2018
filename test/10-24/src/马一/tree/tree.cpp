#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
#include<vector>
#include<queue>
using namespace std;
namespace IO
{
	const int L=1000000;
	char LZH[L];
	char *SSS,*TTT;
	inline char gc(){
		if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
		return *SSS++;
	}
	inline int read(){
		int x=0;
		char c=gc();
		for (;c<'0'||c>'9';c=gc());
		for (;c>='0'&&c<='9';c=gc())
			x=(x<<1)+(x<<3)+c-48;
		return x;
	}
}
namespace Dango
{
	const int MAXN=1000005;
	struct Edge
	{
		int from,to;
	};
	vector<Edge> edges;
	vector<int> g[MAXN];
	int n,q,cnt,fa[MAXN],pre[MAXN],from[MAXN],ans;
	bool vis[MAXN],flag[MAXN*2];
	int ffind(int a){return fa[a]==a?a:fa[a]=ffind(fa[a]);}
	bool cmp(int a,int b){return edges[a].to<edges[b].to;}
	void add(int u,int v)
	{
		edges.push_back((Edge){u,v});
		edges.push_back((Edge){v,u});
		int s=edges.size();
		g[u].push_back(s-2);
		g[v].push_back(s-1);
	}
	void bfs(int a,int b)
	{
		queue<int> q;
		memset(vis,0,sizeof(vis));
		q.push(a);
		vis[a]=true;
		while(!q.empty())
		{
			int u=q.front();
			if(u==b)break;
			q.pop();
			for(int i=0;i<g[u].size();i++)
			{
				Edge& e=edges[g[u][i]];
				if(!vis[e.to])
				{
					vis[e.to]=true;
					pre[e.to]=g[u][i];
					q.push(e.to);
				}
			}
		}
		for(int x=b;x!=a;x=edges[pre[x]].from)
			flag[pre[x]/2]=flag[pre[x]/2]?false:true;
	}
	void getans()
	{
		ans=n;
		for(int i=1;i<=n;i++)
			fa[i]=i;
		for(int i=0;i<n;i++)
		{
			if(flag[i])
			{
				int a=ffind(edges[i*2].from),b=ffind(edges[i*2].to);
				if(a==b)continue;
				fa[a]=b;
				ans--;
			}
		}
	}
	int work()
	{
		n=IO::read();q=IO::read();
		for(int i=1;i<=n;i++)
		{
			int u=IO::read(),v=IO::read();
			add(u,v);
		}
		for(int i=1;i<=n;i++)
			sort(g[i].begin(),g[i].end(),cmp);
		for(int i=1;i<=q;i++)
		{
			int u=IO::read(),v=IO::read();
			bfs(u,v);
			getans();
			printf("%d\n",ans);
		}
		return 0;
	}
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	return Dango::work();
}
