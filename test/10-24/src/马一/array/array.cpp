#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
using namespace std;
namespace Dango
{
	const int MAXN=18,MAXM=100005;
	char st[MAXM];
	int n,m,g[MAXM],ans=0x3f3f3f3f,x,dp[1<<MAXN],tmp[1<<MAXN],num[1<<MAXN];
	int init(int a)
	{
		int result=0;
		while(a)
		{
			if(a&1)result++;
			a>>=1;
		}
		return result;
	}
	int work()
	{
		scanf("%d%d",&n,&m);
		x=(1<<n)-1;
		for(int i=1;i<=n;i++)
		{
			scanf("%s",st);
			for(int j=0;j<m;j++)
				g[j+1]=(g[j+1]<<1)+st[j]-'0';
		}
		for(int i=0;i<=x;i++)
			num[i]=init(i);
		for(int i=1;i<=m;i++)
		{
			memset(tmp,0x3f3f3f3f,sizeof(tmp));
			int a=g[i],b=g[i-1];
			tmp[a]=min(dp[b]+num[a],dp[b^x]+num[a]);
			tmp[a^x]=min(dp[b]+num[a^x],dp[b^x]+num[a^x]);
			for(int j=0;j<=(1<<n)-1;j++)
			{
				int y=j^b;
				tmp[a^y]=min(tmp[a^y],dp[j]+num[a^y]);
				tmp[a^x^y]=min(tmp[a^x^y],dp[j]+num[a^x^y]);
			}
			for(int j=0;j<=(1<<n)-1;j++)
				dp[j]=tmp[j];
		}
		for(int i=0;i<=(1<<n)-1;i++)
			ans=min(ans,dp[i]);
		printf("%d",ans);
		return 0;
	}
}
int main()
{
	freopen("array.in","r",stdin);
	freopen("array.out","w",stdout);
	return Dango::work();
}
