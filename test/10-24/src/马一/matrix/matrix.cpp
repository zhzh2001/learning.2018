#include<cstdio>
#include<iostream>
#include<cstring>
using namespace std;
namespace IO
{
	const int L=1000000;
	char LZH[L];
	char *SSS,*TTT;
	inline char gc(){
		if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
		return *SSS++;
	}
	inline int read(){
		int x=0;
		char c=gc();
		for (;c<'0'||c>'9';c=gc());
		for (;c>='0'&&c<='9';c=gc())
			x=(x<<1)+(x<<3)+c-48;
		return x;
	}
}
namespace Dango
{
	const int MAXN=2505;
	int n,num[MAXN][MAXN];
	int wrong1(int a)
	{
		printf("1\n%d 0 0",a);
		return 0;
	}
	int wrong2(int a,int b)
	{
		printf("2\n%d %d 0",a,b);
		return 0;
	}
	int wrong3(int a,int b,int c)
	{
		printf("3\n%d %d %d",a,b,c);
		return 0;
	}
	int work()
	{
		ios::sync_with_stdio(false);
		n=IO::read();
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
			{
				num[i][j]=IO::read();
				if(i==j&&num[i][j])return wrong1(i);
				if(i>j&&num[i][j]!=num[j][i])return wrong2(i,j);
			}
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				for(int k=1;k<=n;k++)
					if(num[i][j]>max(num[k][i],num[k][j]))
						return wrong3(i,j,k);
		printf("0");
		return 0;
	}
}
int main()
{
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	return Dango::work();
}
