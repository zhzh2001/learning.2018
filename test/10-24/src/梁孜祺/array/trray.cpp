#include<bits/stdc++.h>
using namespace std;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
#define gc getchar
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}
int n, m, s[100005], ans;
char c[20][100005];
inline void solve() {
  int sum = 0;
  for (int i = 1; i <= m; i++)
    sum += min(n-s[i],s[i]);
  ans = min(ans, sum);
}
inline void dfs(int x) {
  if (x > n) {
    solve();
    return;
  }
  dfs(x+1);
  for (int i = 1; i <= m; i++) {
    if (c[x][i] == '1') s[i]--;
    else s[i]++;
    c[x][i] = (c[x][i] == '0')?'1':'0';
  }
  dfs(x+1);
  for (int i = 1; i <= m; i++) {
    if (c[x][i] == '1') s[i]--;
    else s[i]++;
    c[x][i] = (c[x][i] == '0')?'1':'0';
  }
}
int main() {
  freopen("array.in","r",stdin);
  freopen("array.out","w",stdout);
  n = read();
  m = read();
  for (int i = 1; i <= n; i++) 
    scanf("%s",c[i]+1);
  for (int i = 1; i <= n; i++)
    for (int j = 1; j <= m; j++)
      s[j] += (c[i][j] == '1'), ans++;
  for (int i = 1; i <= n; i++)
  printf("%d\n", ans);
  return 0;
}
/*
3 4
0110
1010
0111
*/
