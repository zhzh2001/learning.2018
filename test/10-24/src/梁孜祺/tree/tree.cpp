#include<bits/stdc++.h>
using namespace std;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
#define gc getchar
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}
#define N 100005
int head[N], cnt, n;
struct edge{
  int nxt, to, vis;
}e[N*2];
inline void insert(int u, int v) {
  e[++cnt] = (edge){head[u], v}; head[u] = cnt;
  e[++cnt] = (edge){head[v], u}; head[v] = cnt;
}
struct data{
  int u, v;
  bool operator <(const data &b)const{
    return (u == b.u)?(v>b.v):(u<b.u);
  }
}a[N];
int q[1005], f[1005], from[1005], Q;
inline void dfs(int u, int fa) {
  for (int i = head[u]; i ; i = e[i].nxt)
    if (e[i].to != fa) {
      if (f[e[i].to] > f[u]+1) {
        f[e[i].to] = f[u]+1;
        from[e[i].to] = u;
        dfs(e[i].to, u);
      }
    }
}
int top, vis[N], Flag, bl[N];
inline void dfs_huan(int x, int fa) {
  q[++top] = x;
  vis[x] = 1;
  for (int y, i = head[x]; i; i = e[i].nxt)
    if (!e[i].vis) {
      y = e[i].to;
      if (vis[y] == 1) {
        Flag = 1;
    //    cout << y << endl;
        while(q[top] != y) bl[q[top]] = n+1,top--;
        bl[q[top]] = n+1;
        return;
      }
      e[i].vis = e[i^1].vis = 1;
      dfs_huan(y, x);
      if (Flag) return;
      top--;
    }
}
int sum1, sum2;
map <int, int> mp[1005];
int main() {
  freopen("tree.in","r",stdin);
  freopen("tree.out","w",stdout);
  n = read(); Q = read();
  int tot = 0;
  for (int i = 1; i <= n; i++) {
    a[++tot] = (data){read(), read()};
    tot++;
    a[tot] = (data){a[tot-1].v, a[tot-1].u};
  }
  sort(a+1,a+1+tot);
  for (int i = 1; i <= tot; i++)
    insert(a[i].u, a[i].v);
  dfs_huan(1, 0);
  for (int i = 1; i <= cnt; i++)
    e[i].vis = 0;
  for (int i = 1; i <= n; i++)
    if (bl[i]) sum1++;
  sum2 = n-sum1;
//  cout << sum1 << endl;
  while(Q--) {
    int u = read(), v = read();
    memset(f, 63, sizeof f);
    memset(from, 0, sizeof from);
    f[u] = 0;
    dfs(u, 0);
    while(v != u) {
      int flag;
      if (!mp[from[v]][v]) mp[from[v]][v] = 1, flag = -1;
      else flag = 1;
      if (bl[v] == n+1) sum1 += flag;
      else sum2 += flag;
      v = from[v];
    }
    printf("%d\n", max(sum1-1, 0)+sum2);
  }
  return 0;
}
/*
6 2
4 6
4 3
1 2
6 5
1 5
1 4
2 5
2 6
*/
