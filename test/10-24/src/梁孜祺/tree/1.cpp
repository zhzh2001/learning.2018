#include<bits/stdc++.h>
using namespace std;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
#define gc getchar
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}
#define N 100005
int n, Q, head[N], cnt = 1, vis[N];
struct edge{
  int nxt, to, vis;
}e[N<<1];
inline void insert(int u, int v) {
  e[++cnt] = (edge) {head[u], v}; head[u] = cnt;
  e[++cnt] = (edge) {head[v], u}; head[v] = cnt;
}
int q[N], top, bl[N];
bool flag;
inline void dfs_huan(int x, int fa) {
  q[++top] = x;
  for (int y, i = head[x]; i; i = e[i].nxt)
    if (!e[i].vis) {
      y = e[i].to;
      if (vis[y] == 1) {
        flag = 1;
        while(q[top] == y) bl[q[top]] = n+1,top--;
        bl[q[top]] = n+1;
        return;
      }
      e[i].vis = e[i^1].vis = 1;
      dfs_huan(y, x);
      if (flag) return;
      top--;
    }
}
struct data{
  int u, v;
}a[N];
int fa[N][18], bin[18], sz[N], son[N], dep[N];
inline void dfs(int x) {
  sz[x] = 1;
  for (int y, i = head[x]; i; i = e[i].nxt)
    if ((y = e[i].to) != fa[x][0]) {
      dep[y] += dep[x]+1;
      dfs(y);
      sz[x] += sz[y];
      if (sz[son[x]] < sz[y]) son[x] = y;
    }
}
int l[N], ind, belg[N];
inline void dfs(int x, int chain) {
  l[x] = ++ind;
  if (son[x]) dfs(son[x], chain);
  for (int i = head[x]; i; i = e[i].nxt)
    if(e[i].to != fa[x][0]&&e[i].to != son[x])
      dfs(e[i].to, e[i].to);
}
struct tree {
  int tag, sum;
};
inline void solve(int u, int v) {
  if (bl[u] == bl[v] && bl[u] == n+1) {
    
  }
}
int main() {
  for (int i = 1; i < 18; i++) bin[i] = bin[i-1]<<1;
  n = read(); Q = read();
  for (int i = 1; i <= n; i++) {
    a[i] = (data){read(), read()};
    insert(a[i].u, a[i].v);
  }
  dfs_huan(1, 0);
  cnt = 0;
  memset(head, 0, sizeof head);
  for (int i = 1; i <= n; i++) {
    int u = a[i].u, v = a[i].v;
    if (bl[u] == n+1 && !bl[v]) insert(n+1, v);
    else if (bl[v] == n+1 && !bl[u]) insert(n+1, u);
    else if (!bl[u] && !bl[v]) insert(u, v);
  }
  dfs(1);
  dfs(1,1);
  while(Q--) {
    int u = read(), v = read();
    solve(u, v);
  }
  return 0;
}
