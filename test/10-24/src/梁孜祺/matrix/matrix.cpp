#include<bits/stdc++.h>
using namespace std;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
#define gc getchar
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}
const int N = 2505;
int n, a[N][N], tot, b[10];
inline bool pd(int i, int j, int k) {
  if (a[i][j] == a[i][k]&&a[i][k] == a[j][k])
    return 0;
  b[1] = a[i][j];
  b[2] = a[i][k];
  b[3] = a[j][k];
  sort(b+1,b+4);
  if (b[3] == b[2]) return 0;
  return 1;
}
int main() {
  freopen("matrix.in","r",stdin);
  freopen("matrix.out","w",stdout);
  n = read();
  for (int i = 1; i <= n; i++)
    for (int j = 1; j <= n; j++)
      a[i][j] = read();
  for (int i = 1; i <= n; i++)
    if (a[i][i]) {
      puts("1");
      printf("%d 0 0", i);
      return 0;
    }
  for (int i = 1; i <= n; i++)
    for (int j = 1; j <= n; j++)
      if (a[i][j] != a[j][i]) {
        puts("2");
        printf("%d %d 0", i, j);
        return 0;
      }
  for (int i = 1; i <= n; i++)
    for (int j = 1; j <= n; j++)
      for (int k = 1; k <= n; k++)
        if(pd(i,j,k)) {
          printf("%d %d %d", i, j, k);
          return 0;
        }
  puts("0");
  return 0;
}
/*
3
0 1 2
1 0 2
2 2 0

2
0 1
2 3
a[i][j] <= max(a[i][k], a[j][k]);
a[i][k] <= max(a[i][j], a[j][k]);
a[j][k] <= max(a[i][k], a[j][k]);
这三个数中必须有两个数相同且为最大值 
.....所以怎么做。。。 
*/
