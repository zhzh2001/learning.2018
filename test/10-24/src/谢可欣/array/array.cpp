#include <iostream>
#include <cstdio>
using namespace std;
char s[100005];
int n,m,ans=100000000,c[20][100005],suml[100005];
/*void dfso(int rt){
	if(ans==0)return;
	int sum=0;
	for(int i=1;i<=n;i++)
	    for(int j=1;j<=m;j++)
	        sum+=(c[i][j]==1);
//	cout<<sum<<endl;
	ans=min(ans,sum);
	if(rt>n+m)return;
	for(int j=1;j<=m;j++){
		if(!l[j]){
			int tmp=0;
			for(int i=1;i<=n;i++)tmp+=(c[i][j]==1);
			if(tmp>=n/2){
				l[j]=1;
				for(int i=1;i<=n;i++)c[i][j]=1-c[i][j];
				dfs(rt+1);
				for(int i=1;i<=n;i++)c[i][j]=1-c[i][j];
				l[j]=0;
			}
		}
	}
	for(int i=1;i<=n;i++){
		if(!r[i]){
			int tmp=0;
			for(int j=1;j<=m;j++)tmp+=(c[i][j]==1);
			if(tmp>=m/2){
				r[i]=1;
				for(int j=1;j<=m;j++)c[i][j]=1-c[i][j];
				dfs(rt+1);
				for(int j=1;j<=m;j++)c[i][j]=1-c[i][j];
				r[i]=0;
			}
		}
	}
}*/
void dfs(int rt){
	if(ans==0)return;
//	cout<<n<<" "<<rt<<endl;
	if(rt>n){
		int sum=0;
		for(int i=1;i<=m;i++){
			if(suml[i]>=((n+1)/2))sum+=n-suml[i];
			else sum+=suml[i];
		}
		ans=min(sum,ans);
		return;
	}
	dfs(rt+1);
	for(int j=1;j<=m;j++){
		if(c[rt][j])suml[j]--;
		else suml[j]++;
	    c[rt][j]=1-c[rt][j];
	}
	dfs(rt+1);
	for(int j=1;j<=m;j++){
		if(c[rt][j])suml[j]--;
		else suml[j]++;
	    c[rt][j]=1-c[rt][j];
	}
}
int main()
{
	freopen("array.in","r",stdin);
	freopen("array.out","w",stdout);
	cin>>n>>m;
	for(int i=1;i<=n;i++){
//		cout<<n<<endl;
		scanf("%s",s+1);
		for(int j=1;j<=m;j++){
//			cout<<"a"<<n<<endl;
			if(s[j]=='1')c[i][j]=1;
		}
	}
//	for(int i=1;i<=m;i++)cout<<c[1][i]<<" ";puts("");
	for(int j=1;j<=m;j++){
		for(int i=1;i<=n;i++)suml[j]+=(c[i][j]==1);
	}
//	cout<<n<<" "<<m<<endl;
//	for(int i=1;i<=m;i++)cout<<suml[i]<<" ";puts("");
//	dfso(1);
    dfs(1);
	cout<<ans<<endl;
	return 0;
}
/*
10 10
0011010110
1010100000
0100111011
1110010111
0011010111
0001110101
0010101011
1101010000
1010010101
1111111000
*/
