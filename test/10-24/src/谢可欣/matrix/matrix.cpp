#include <iostream>
#include <cstdio>
using namespace std;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}
int a[3000][3000];
int main()
{
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	int n=read();
	for(int i=1;i<=n;i++)
	    for(int j=1;j<=n;j++) a[i][j]=read();
	for(int i=1;i<=n;i++)
	    if(a[i][i]){
	    	puts("1");
	    	printf("%d 0 0",i);
	    	return 0;
		}
	for(int i=1;i<=n;i++)
		for(int j=1;j<i;j++)
			if(a[i][j]!=a[j][i]){
				puts("2");
				printf("%d %d 0",i,j);
				return 0;
			}
	for(int i=1;i<=n;i++){
		for(int j=1;j<i;j++){
			for(int k=1;k<=n;k++){
				if(a[i][j]>max(a[i][k],a[k][j])){
					puts("3");
					printf("%d %d %d",i,j,k);
					return 0;
				}
			}
		}
	}
	puts("0");
	return 0;
}
