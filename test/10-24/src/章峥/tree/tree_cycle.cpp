#include<cstdio>
#include<vector>
#include<algorithm>
using namespace std;
const int L=1000000;
FILE *fin=fopen("tree.in","r"),*fout=fopen("tree.ans","w");
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,fin);
	return *SSS++;
}
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}
const int N=100005;
struct edge
{
	int v,id;
	edge(int v,int id):v(v),id(id){}
	bool operator<(const edge& rhs)const
	{
		if(v==rhs.v)
			return id<rhs.id;
		return v<rhs.v;
	}
};
vector<edge> mat[N];
struct state
{
	int v,pred,e;
}que[N];
bool exist[N],vis[N];
int f[N],cycle[N],pos[N];
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
pair<int,int> E[N];
struct node
{
	int cnt;
	bool lazy;
}tree[N*4];
inline void pushdown(int id,int l,int r)
{
	if(tree[id].lazy)
	{
		int mid=(l+r)/2;
		tree[id*2].lazy^=1;
		tree[id*2].cnt=mid-l+1-tree[id*2].cnt;
		tree[id*2+1].lazy^=1;
		tree[id*2+1].cnt=r-mid-tree[id*2+1].cnt;
		tree[id].lazy=false;
	}
}
void modify(int id,int l,int r,int L,int R)
{
	if(L<=l&&R>=r)
	{
		tree[id].cnt=r-l+1-tree[id].cnt;
		tree[id].lazy^=1;
	}
	else
	{
		pushdown(id,l,r);
		int mid=(l+r)/2;
		if(L<=mid)
			modify(id*2,l,mid,L,R);
		if(R>mid)
			modify(id*2+1,mid+1,r,L,R);
		tree[id].cnt=tree[id*2].cnt+tree[id*2+1].cnt;
	}
}
int main()
{
	int n=read(),q=read();
	for(int i=1;i<=n;i++)
	{
		E[i].first=read();E[i].second=read();
		mat[E[i].first].push_back(edge(E[i].second,i));
		mat[E[i].second].push_back(edge(E[i].first,i));
	}
	for(int i=1;i<=n;i++)
		sort(mat[i].begin(),mat[i].end());
	cycle[1]=1;
	for(int i=2;i<=n;i++)
		if(mat[cycle[i-1]][0].v!=cycle[i-2])
			cycle[i]=mat[cycle[i-1]][0].v;
		else
			cycle[i]=mat[cycle[i-1]][1].v;
	for(int i=1;i<=n;i++)
		pos[cycle[i]]=i;
	while(q--)
	{
		int s=read(),t=read();
		s=pos[s];t=pos[t];
		if(s<t)
		{
			int l=s-1;
			if(l==0)
				l=n;
			if(t-s<n-(t-s)||(t-s==n-(t-s)&&cycle[s+1]<cycle[l]))
				modify(1,1,n,s,t-1);
			else
			{
				if(s>1)
					modify(1,1,n,1,s-1);
				modify(1,1,n,t,n);
			}
		}
		else if(s>t)
		{
			int r=s+1;
			if(r==n+1)
				r=1;
			if(s-t<n-(s-t)||(s-t==n-(s-t)&&cycle[s-1]<cycle[r]))
				modify(1,1,n,t,s-1);
			else
			{
				if(t>1)
					modify(1,1,n,1,t-1);
				modify(1,1,n,s,n);
			}
		}
		fprintf(fout,"%d\n",max(n-tree[1].cnt,1));
	}
	return 0;
}
