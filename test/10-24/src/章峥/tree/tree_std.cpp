#include<bits/stdc++.h>
using namespace std;
#define file "tree"
typedef long long ll;
const int INF=0x3f3f3f3f;
const int mod=1000000007;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}
const int maxn=100010;
struct segtree{
	int n;
	int sum[maxn*4],tag[maxn*4];
	int ul,ur;
	void update(int o,int l,int r){
		if (ul<=l && ur>=r){
			sum[o]=r-l+1-sum[o];
			tag[o]^=1;
			return;
		}
		int mid=(l+r)/2;
		if (ul<=mid)
			update(o*2,l,mid);
		if (ur>mid)
			update(o*2+1,mid+1,r);
		sum[o]=sum[o*2]+sum[o*2+1];
		if (tag[o])
			sum[o]=r-l+1-sum[o];
	}
	void update(int l,int r){
		if (l>r)
			return;
		ul=l,ur=r;
//		fprintf(stderr,"update %d %d\n",l,r);
		update(1,1,n);
	}
	int query(){
		return sum[1];
	}
}t,t2;
struct graph{
	int n,m;
	graph(){
		m=1;
	}
	struct edge{
		int from,to,next;
	}e[maxn*2];
	int first[maxn];
	void addedge(int from,int to){
		e[++m]=(edge){from,to,first[from]};
		first[from]=m;
	}
	bool vis[maxn],found;
	int l[maxn],idl[maxn],cur;
	int rt[maxn],fa[maxn],dep[maxn],top[maxn],id[maxn],sz[maxn],son[maxn],cl;
	void dfs0(int u){
		vis[u]=1;
		if (found)
			return;
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (i==(fa[u]^1))
				continue;
			if (vis[v]){
				int now=u;
				l[++cur]=u;
				idl[u]=cur;
//				fprintf(stderr,"now=%d\n",now);
				while(now!=v){
					now=e[fa[now]].from;
					l[++cur]=now;
					idl[now]=cur;
				}
				found=1;
			}else{
				fa[v]=i;
				dfs0(v);
			}
			if (found)
				return;
		}
	}
	void dfs(int u){
//		fprintf(stderr,"dfs %d\n",u);
		sz[u]=1;
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (sz[v] || idl[v])
				continue;
			dep[v]=dep[u]+1;
			fa[v]=u;
			rt[v]=rt[u];
			dfs(v);
			sz[u]+=sz[v];
			if (sz[v]>sz[son[u]])
				son[u]=v;
		}
	}
	void dfs2(int u,int tp){
		if (idl[u])
			id[u]=cl;
		else id[u]=++cl;
//		fprintf(stderr,"id[%d]=%d\n",u,id[u]);
		top[u]=tp;
		if (son[u])
			dfs2(son[u],tp);
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (!id[v] && !idl[v])
				dfs2(v,v);
		}
	}
	void upt(int u,int v){ //update on same tree
		for(;top[u]!=top[v];u=fa[top[u]]){
			if (dep[top[u]]<dep[top[v]])
				swap(u,v);
			t.update(id[top[u]],id[u]);
		}
		if (dep[u]>dep[v])
			swap(u,v);
//		fprintf(stderr,"u=%d\n",u);
		t.update(id[u]+1,id[v]);
	}
	void upl(int u,int v){
		if (cur<=2)
			return;
		if (u==v)
			return;
		u=idl[u],v=idl[v];
		
		pair<int,int> nxt=make_pair(u>v?v+cur-u+1:v-u+1,l[u==cur?1:u+1]),lst=make_pair(u>v?u-v+1:u+cur-v+1,l[u==1?cur:u-1]);
		if (nxt<lst){
			if (u<v)
				t2.update(u,v-1);
			else t2.update(u,cur),t2.update(1,v-1);
		}else{
			if (u>v)
				t2.update(v,u-1);
			else t2.update(1,u-1),t2.update(v,cur);
		}
	}
	void pre(){
		dfs0(1);
		t2.n=cur;
		t2.update(1,cur);
//		fprintf(stderr,"WTF\n");
		memset(fa,0,sizeof(fa));
//		for(int i=1;i<=cur;i++)
//			fprintf(stderr,"l[%d]=%d\n",i,l[i]);
		for(int i=1;i<=n;i++)
			if (idl[i]){
//				fprintf(stderr,"dfs %d\n",i);
				rt[i]=i;
				dfs(i);
//				fprintf(stderr,"dfs %d\n",i);
				dfs2(i,i);
			}
//		fprintf(stderr,"cl=%d\n",cl);
		t.n=cl;
		t.update(1,cl);
	}
	void update(int u,int v){
//		fprintf(stderr,"outer update %d %d\n",u,v);
		if (rt[u]==rt[v]){
			upt(u,v);
			return;
		}
		upt(u,rt[u]);
		upt(v,rt[v]);
		upl(rt[u],rt[v]);
	}
}g;
int main(){
	freopen(file".in","r",stdin);
	freopen(file".ans","w",stdout);
	int n=g.n=read(),q=read();
	for(int i=1;i<=n;i++){
		int u=read(),v=read();
		g.addedge(u,v);
		g.addedge(v,u);
	}
//	fprintf(stderr,"WTF\n");
	g.pre();
//	fprintf(stderr,"WTF\n");
	while(q--){
		int u=read(),v=read();
		g.update(u,v);
		printf("%d\n",t.query()+max(t2.query(),1));
	}
}
