#include<cstdio>
#include<vector>
#include<algorithm>
#include<utility>
using namespace std;
const int L=1000000;
FILE *fin=fopen("tree.in","r"),*fout=fopen("tree.out","w");
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,fin);
	return *SSS++;
}
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}
const int N=100005;
int head[N],v[N*2],nxt[N*2],e,cycle[N],csz,pos[N],stack[N],sp;
bool ins[N];
inline void add_edge(int u,int v)
{
	::v[e]=v;
	nxt[e]=head[u];
	head[u]=e++;
}
struct SGT
{
	struct node
	{
		int cnt;
		bool lazy;
	}tree[N*4];
	inline void pushdown(int id,int l,int r)
	{
		if(tree[id].lazy)
		{
			int mid=(l+r)/2;
			tree[id*2].lazy^=1;
			tree[id*2].cnt=mid-l+1-tree[id*2].cnt;
			tree[id*2+1].lazy^=1;
			tree[id*2+1].cnt=r-mid-tree[id*2+1].cnt;
			tree[id].lazy=false;
		}
	}
	void modify(int id,int l,int r,int L,int R)
	{
		if(L<=l&&R>=r)
		{
			tree[id].cnt=r-l+1-tree[id].cnt;
			tree[id].lazy^=1;
		}
		else
		{
			pushdown(id,l,r);
			int mid=(l+r)/2;
			if(L<=mid)
				modify(id*2,l,mid,L,R);
			if(R>mid)
				modify(id*2+1,mid+1,r,L,R);
			tree[id].cnt=tree[id*2].cnt+tree[id*2+1].cnt;
		}
	}
}Tcycle,Ttree;
bool find_cycle(int u,int fat)
{
	ins[u]=true;
	stack[++sp]=u;
	for(int k=head[u];~k;k=nxt[k])
		if(k!=fat)
		{
			if(ins[v[k]])
			{
				for(;stack[sp]!=v[k];sp--)
				{
					cycle[++csz]=stack[sp];
					pos[stack[sp]]=csz;
				}
				cycle[++csz]=v[k];
				pos[v[k]]=csz;
				return true;
			}
			if(find_cycle(v[k],k^1))
				return true;
		}
	sp--;
	ins[u]=false;
	return false;
}
int sz[N],heavy[N],id[N],t,root[N],top[N],f[N];
void dfs(int u,int fat,int rt)
{
	root[u]=rt;
	sz[u]=1;
	for(int k=head[u];~k;k=nxt[k])
		if(!pos[v[k]]&&v[k]!=fat)
		{
			f[v[k]]=u;
			dfs(v[k],u,rt);
			sz[u]+=sz[v[k]];
			if(sz[v[k]]>sz[heavy[u]])
				heavy[u]=v[k];
		}
}
void dfs2(int u,int anc)
{
	id[u]=++t;
	top[u]=anc;
	if(heavy[u])
		dfs2(heavy[u],anc);
	for(int k=head[u];~k;k=nxt[k])
		if(!pos[v[k]]&&v[k]!=f[u]&&v[k]!=heavy[u])
			dfs2(v[k],v[k]);
}
void modify(int u)
{
	if(u==root[u])
		return;
	while(top[u]!=root[u])
	{
		Ttree.modify(1,1,t,id[top[u]],id[u]);
		u=f[top[u]];
	}
	Ttree.modify(1,1,t,id[root[u]]+1,id[u]);
}
int main()
{
	int n=read(),q=read();
	fill(head+1,head+n+1,-1);
	for(int i=1;i<=n;i++)
	{
		int u=read(),v=read();
		add_edge(u,v);
		add_edge(v,u);
	}
	find_cycle(1,0);
	for(int i=1;i<=csz;i++)
	{
		dfs(cycle[i],0,cycle[i]);
		dfs2(cycle[i],cycle[i]);
	}
	while(q--)
	{
		int s=read(),t=read();
		modify(s);modify(t);
		s=pos[root[s]];t=pos[root[t]];
		if(s<t)
		{
			int l=s-1;
			if(l==0)
				l=csz;
			if(t-s<csz-(t-s)||(t-s==csz-(t-s)&&cycle[s+1]<cycle[l]))
				Tcycle.modify(1,1,csz,s,t-1);
			else
			{
				if(s>1)
					Tcycle.modify(1,1,csz,1,s-1);
				Tcycle.modify(1,1,csz,t,csz);
			}
		}
		else if(s>t)
		{
			int r=s+1;
			if(r==csz+1)
				r=1;
			if(s-t<csz-(s-t)||(s-t==csz-(s-t)&&cycle[s-1]<cycle[r]))
				Tcycle.modify(1,1,csz,t,s-1);
			else
			{
				if(t>1)
					Tcycle.modify(1,1,csz,1,t-1);
				Tcycle.modify(1,1,csz,s,csz);
			}
		}
		int ans=n-Tcycle.tree[1].cnt-Ttree.tree[1].cnt;
		if(Tcycle.tree[1].cnt==csz)
			ans++;
		fprintf(fout,"%d\n",ans);
	}
	return 0;
}
