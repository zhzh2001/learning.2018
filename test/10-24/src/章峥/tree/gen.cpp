#include<fstream>
#include<random>
#include<windows.h>
#include<algorithm>
#include<set>
using namespace std;
ofstream fout("tree.in");
const int n=100,q=100;
int f[n+5];
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
int main()
{
	minstd_rand gen(GetTickCount());
	fout<<n<<' '<<q<<endl;
	for(int i=1;i<=n;i++)
		f[i]=i;
	uniform_int_distribution<> dn(1,n);
	set<pair<int,int>> S;
	for(int i=1;i<n;)
	{
		int u=dn(gen),v=dn(gen);
		if(getf(u)!=getf(v))
		{
			fout<<u<<' '<<v<<endl;
			f[getf(u)]=getf(v);
			i++;
			S.insert(make_pair(u,v));
			S.insert(make_pair(v,u));
		}
	}
	int u=dn(gen),v=dn(gen);
	while(u==v||S.find(make_pair(u,v))!=S.end())
	{
		u=dn(gen);v=dn(gen);
	}
	fout<<u<<' '<<v<<endl;
	for(int i=1;i<=q;i++)
	{
		int u=dn(gen),v=dn(gen);
		fout<<u<<' '<<v<<endl;
	}
	return 0;
}
