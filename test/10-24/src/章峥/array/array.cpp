#include<fstream>
#include<string>
#include<algorithm>
using namespace std;
ifstream fin("array.in");
ofstream fout("array.out");
const int N=20,P=1<<18;
int mat[P],f[N][P];
int main()
{
	int n,m;
	fin>>n>>m;
	for(int i=1;i<=n;i++)
	{
		string s;
		fin>>s;
		for(int j=0;j<m;j++)
			if(s[j]=='1')
				mat[j+1]|=1<<(i-1);
	}
	for(int i=1;i<=m;i++)
		f[0][mat[i]]++;
	for(int i=0;i<n;i++)
		for(int j=n;j;j--)
			for(int k=0;k<1<<n;k++)
				f[j][k]+=f[j-1][k^(1<<i)];
	int ans=n*m;
	for(int i=0;i<1<<n;i++)
	{
		int now=0;
		for(int j=0;j<=n;j++)
			now+=min(j,n-j)*f[j][i];
		ans=min(ans,now);
	}
	fout<<ans<<endl;
	return 0;
}
