#include<cstdio>
#include<algorithm>
using namespace std;
const int L=1000000;
FILE *fin=fopen("matrix.in","r"),*fout=fopen("matrix.out","w");
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,fin);
	return *SSS++;
}
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}
const int N=2505,INF=0x3f3f3f3f;
int mat[N][N],d[N],pred[N],mx[N][N],head[N],nxt[N*2],v[N*2],e;
bool vis[N];
inline void add_edge(int u,int v)
{
	::v[++e]=v;
	nxt[e]=head[u];
	head[u]=e;
}
void dfs(int u,int fat,int root)
{
	for(int i=head[u];i;i=nxt[i])
		if(v[i]!=fat)
		{
			mx[root][v[i]]=max(mx[root][u],mat[u][v[i]]);
			dfs(v[i],u,root);
		}
}
int main()
{
	int n=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			mat[i][j]=read();
	for(int i=1;i<=n;i++)
		if(mat[i][i])
		{
			fprintf(fout,"%d\n%d %d %d\n",1,i,0,0);
			return 0;
		}
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++)
			if(mat[i][j]!=mat[j][i])
			{
				fprintf(fout,"%d\n%d %d %d\n",2,i,j,0);
				return 0;
			}
	fill(d,d+n+1,INF);
	d[1]=0;
	for(int i=1;i<=n;i++)
	{
		int j=0;
		for(int k=1;k<=n;k++)
			if(!vis[k]&&d[k]<d[j])
				j=k;
		vis[j]=true;
		add_edge(pred[j],j);
		add_edge(j,pred[j]);
		for(int k=1;k<=n;k++)
			if(!vis[k]&&mat[j][k]<d[k])
			{
				d[k]=mat[j][k];
				pred[k]=j;
			}
	}
	for(int i=1;i<=n;i++)
		dfs(i,0,i);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			if(mat[i][j]>mx[i][j])
				for(int k=1;k<=n;k++)
				{
					if(mat[i][j]>max(mat[i][k],mat[k][j]))
					{
						fprintf(fout,"%d\n%d %d %d\n",3,i,j,k);
						return 0;
					}
					if(mat[i][k]>max(mat[i][j],mat[j][k]))
					{
						fprintf(fout,"%d\n%d %d %d\n",3,i,k,j);
						return 0;
					}
					if(mat[j][k]>max(mat[j][i],mat[i][k]))
					{
						fprintf(fout,"%d\n%d %d %d\n",3,j,k,i);
						return 0;
					}
				}
	fputs("0\n",fout);
	return 0;
}
