#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=19;
const int M=100005;
string s[N];
int f[1<<18];
int x[M];
int k[1<<18];
int ans;
int n,m;
int main()
{
	freopen("array.in","r",stdin);
	freopen("array.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=n;i++)
		cin>>s[i];
	for(int i=1;i<=m;i++){
		x[i]=0;
		for(int j=1;j<=n;j++)
			x[i]=x[i]*2+(int(s[j][i-1]-'0'));
	}
	for(int i=(1<<n)-1;i>=0;i--){
		int tmp=0;
		int now=i;
		while(now){
			tmp+=now&1;
			now/=2;
		}
		k[i]=max(n-tmp,tmp);
//		cout<<i<<' '<<k[i]<<endl;
	}
//	for(int i=1;i<=m;i++)
//		cout<<x[i]<<' ';
//	cout<<endl;
	for(int i=(1<<n)-1;i>=0;i--){
		for(int j=1;j<=m;j++){
			f[i]+=k[i^x[j]];
		}
		ans=max(f[i],ans);
	}
	writeln(n*m-ans);
}
/*
3 4
0110
1010
0111
*/
