#include<bits/stdc++.h>
using namespace std;
using namespace std;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
#define ll long long
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}
inline void write(ll x)
{
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=2505;
int dis[N];
int a[N][N];
int head[N],tail[2*N],nxt[2*N],e[2*N],from[N];
int f[N],t,n;
inline void addto(int x,int y,int z)
{
	nxt[++t]=head[x];
	head[x]=t;
	tail[t]=y;
	e[t]=z;
}
void dfs(int k,int fa)
{
	for(int i=head[k];i;i=nxt[i]){
		int v=tail[i];
		if(v==fa)continue;
		f[v]=max(f[k],e[i]);
		dfs(v,k);
	}
}
inline bool get(int x,int y)
{
	for(int i=1;i<=n;i++)
		if(a[x][y]>max(a[x][i],a[i][y])){
			puts("3");
			write(x),putchar(' '),write(y),putchar(' '),write(i);
			return true;
		}
	return false;
}
bool vis[N][N];
int main()
{
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			a[i][j]=read();
	for(int i=1;i<=n;i++)
		if(a[i][i]){
			puts("1");
			write(i),puts(" 0 0");
			return 0;
		}
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++)
			if(a[i][j]!=a[j][i]){
				puts("2");
				write(i),putchar(' '),write(j),puts(" 0");
				return 0;
			}
//	for(int t=2;t<=n;t++){
//		int ma=1;
//		for(int i=2;i<t;i++)
//			if(a[ma][t]<a[i][t])ma=i;
//		addto(ma,t,a[ma][t]);
//		addto(t,ma,a[t][ma]);
//		memset(f,0,sizeof(f));
//		dfs(t,t);
//		for(int i=1;i<t;i++)
//			if(a[i][t]>f[i]){
//				get(i,t);
//				return 0;
//			}
//	}
	srand(time(0));
	int t,x;
	if(n<=500)t=1,x=n-1;
	else if(n==700)t=50,x=600;
	else if(n==900)t=300,x=300;
	else if(n<=1600){
		for(int i=1;i<=50000;i++){
			int x=rand()%n+1;
			int y=rand()%n+1;
			while(vis[x][y])x=rand()%n+1,y=rand()%n+1;
			if(get(x,y))return 0;
		}
		puts("0");
		return 0;
	}
	else if(n<=2500){
		for(int i=1;i<=40000;i++){
			int x=rand()%n+1;
			int y=rand()%n+1;
			while(vis[x][y])x=rand()%n+1,y=rand()%n+1;
			if(get(x,y))return 0;
		}
		puts("0");
		return 0;
	}
	for(int i=t;i<=t+x;i++)
		for(int j=t;j<=t+x;j++)
			if(get(i,j))return 0;
	puts("0");
}
