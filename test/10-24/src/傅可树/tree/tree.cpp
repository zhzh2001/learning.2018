#include<cstdio>
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=100005;
struct edge{
	int link,next;
}e[N<<1];
int n,q,tot,head[N];
inline void add_edge(int u,int v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
}
inline void insert(int u,int v){
	add_edge(u,v); add_edge(v,u);
}
inline void init(){
	n=read(); q=read();
	for (int i=1;i<=n;i++){
		insert(read(),read());
	}
}
int pos[N],cnt,id[N],vis[N];
void dfs(int u,int fa){
	vis[u]=1;
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (!vis[v]) id[v]=++cnt,pos[cnt]=v,dfs(v,u);
	}
}
struct node{
	int sum,rev;
}a[N<<2];
bool flag;
inline void pushup(int k){
	a[k].sum=a[k<<1].sum+a[k<<1|1].sum;
}
void build(int k,int l,int r){
	a[k].rev=-1;
	if (l==r) {
		a[k].sum=1;
		return;
	}
	int mid=(l+r)>>1; build(k<<1,l,mid); build(k<<1|1,mid+1,r); pushup(k);
}
inline void rever(int k,int l,int r){
	a[k].sum=(r-l+1)-a[k].sum;
	a[k].rev^=1;
}
inline void pushdown(int k,int l,int mid,int r){
	if (~a[k].rev){
		rever(k<<1,l,mid); rever(k<<1|1,mid+1,r);
		a[k].rev=-1;
	}
}
void update(int k,int l,int r,int x,int y){
	if (x>y) return;
	if (l==x&&r==y){
		rever(k,l,r);
		return;
	}
	int mid=(l+r)>>1; pushdown(k,l,mid,r);
	if (mid>=y) update(k<<1,l,mid,x,y);
		else if (mid<x) update(k<<1|1,mid+1,r,x,y);
			else update(k<<1,l,mid,x,mid),update(k<<1|1,mid+1,r,mid+1,y);
	pushup(k);
}
inline void solve(){
	dfs(1,0); flag=1; //��1��n������  	
	id[1]=1;
	build(1,1,n-1);
	for (int i=1;i<=q;i++){
		int u=read(),v=read();
		int dis1,dis2,nxt=pos[id[u]%n+1],pre=pos[(id[u]-2+n)%n+1];
		if (id[u]<id[v]) dis1=id[v]-id[u],dis2=id[u]+n-id[v]-1;
			else dis1=n-id[u]+id[v]-1,dis2=id[u]-id[v];
		if (dis1>dis2) {
			if (id[u]<id[v]){
				update(1,1,n-1,id[u],id[v]-1);
			}else{
				update(1,1,n-1,id[u],n-1); update(1,1,n-1,1,id[v]-1); flag^=1;
			}
		}else{
			if (dis1<dis2){
				if (id[u]<id[v]) update(1,1,n-1,1,id[u]-1),update(1,1,n-1,id[v],n-1),flag^=1;
					else{
						update(1,1,n-1,id[v],id[u]-1);
					}
			}else{
				if (nxt<pre) {
					if (id[u]<id[v]) update(1,1,n-1,id[u],id[v]-1);
						else {
							update(1,1,n-1,id[u],n-1); update(1,1,n-1,1,id[v]-1); flag^=1;
						}
				}else{
					if (id[u]<id[v]) update(1,1,n-1,1,id[u]-1),update(1,1,n-1,id[v],n-1),flag^=1;
					else{
						update(1,1,n-1,id[v],id[u]-1);
					}
				}
			}
		}
		writeln(a[1].sum+flag-1);
	}
}
int main(){
	freopen("tree.in","r",stdin); freopen("tree.out","w",stdout);
	init(); solve();
	return 0;
}
/*
6 4
1 2
2 3
3 4
4 5
5 6
6 1
1 3
2 6
3 4
5 6
*/
