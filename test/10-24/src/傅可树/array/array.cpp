
#include<bits/stdc++.h>
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=1e5+5,INF=1e9;
int n,m,a[19][N];
char s[N];
inline void init(){
	n=read(); m=read();
	for (int i=1;i<=n;i++){
		scanf("%s",s+1);
		for (int j=1;j<=m;j++){
			a[i][j]=s[j]-'0';
		}
	}
}
int ans,cho[N],c[N],b[1<<18];
inline void work(){
	for (int i=0;i<(1<<n);i++) b[i]=i;
	random_shuffle(b,b+(1<<n));
	ans=INF;
	for (int i=0;i<(1<<n);i++){
		memcpy(c,cho,sizeof c);
		for (int j=1;j<=n;j++){
			if (b[i]&(1<<(j-1))){
				for (int k=1;k<=m;k++) {
					c[k]+=(a[j][k]==1)?-2:2;
				}
			}
		}
		int sum=0;
		for (int j=1;j<=m;j++){
			if (c[j]>0) c[j]=-c[j];
			sum+=(c[j]+n)/2;
			if (ans<=sum) break;
		}
		ans=min(ans,sum);
		if (clock()>1800) break;
	}
	writeln(ans);
}
inline void solve(){
	for (int i=1;i<=m;i++){
		for (int j=1;j<=n;j++){
			cho[i]+=(a[j][i]==1)?1:-1;
		}
	}
	work();
}
int main(){
	freopen("array.in","r",stdin); freopen("array.out","w",stdout);
	init(); solve();
	return 0;
}//��o2+��ʱ�� 
