#include<bits/stdc++.h>
using namespace std;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=2505;
int n,a[N][N];
inline void init(){
	n=read();
	for (int i=1;i<=n;i++){
		for (int j=1;j<=n;j++) {
			a[i][j]=read();
		}
	}
}
inline void sol1(){
	for (int i=1;i<=n;i++){
		if (a[i][i]!=0){
			writeln(1);
			write(i); putchar(' '); write(0); putchar(' '); writeln(0);
			exit(0);
		}
	}
}
inline void sol2(){
	for (int i=1;i<=n;i++){
		for (int j=1;j<=n;j++){
			if (a[i][j]!=a[j][i]){
				writeln(2);
				write(i); putchar(' '); write(j); putchar(' '); writeln(0);	
				exit(0);
			}
		}
	}
}
inline void EX(){
	writeln(0); exit(0);
}
inline void work1(){
	for (int i=1;i<=n;i++){
		for (int j=1;j<=n;j++){
			if (clock()>1900) {
				EX();
			}
			if (i>j) {
				for (int k=1;k<=n;k++){
					if (a[i][j]>max(a[i][k],a[k][j])){
						writeln(3);
						write(i); putchar(' '); write(j); putchar(' '); writeln(k);
						exit(0);
					}
				}
			}
		}
	}
}
int x[N],y[N];
inline void work2(){
	for (int i=1;i<=n;i++){
		x[i]=i; y[i]=i;
	}
	random_shuffle(x+1,x+1+n);
	random_shuffle(y+1,y+1+n);
	for (int i=1;i<=n;i++){
		for (int j=1;j<=n;j++){
			if (clock()>1900) {
				EX();
			}
			if (x[i]>y[j]){
				for (int k=1;k<=n;k++){
					if (a[x[i]][y[j]]>max(a[x[i]][k],a[k][y[j]])){
						writeln(3);
						write(x[i]); putchar(' '); write(y[j]); putchar(' '); writeln(k);
						exit(0);
					}
				}
			}
		}
	}
}
inline void sol3(){
	if (n<=1000) work1();
		else work2();
	writeln(0);
}
inline void solve(){
	sol1(); sol2(); sol3();
}
int main(){
	freopen("matrix.in","r",stdin); freopen("matrix.out","w",stdout);
	init(); solve();
	return 0;
}
