program arrayy;
 uses math;
 var
  a:array[0..19,0..100001] of char;
  d:array[0..100001] of longint;
  i,j,k,m,n:longint;
  ssum,smax:longint;
 begin
  assign(input,'array.in');
  assign(output,'array.out');
  reset(input);
  rewrite(output);
  readln(n,m);
  for i:=1 to n do
   begin
    for j:=1 to m do
     read(A[i,j]);
    readln;
   end;
  smax:=1008208820;
  for i:=0 to 1<<n-1 do
   begin
    ssum:=0;
    for j:=1 to m do
     d[j]:=0;
    for j:=1 to n do
     for k:=1 to m do
      if (a[j,k]='1') and ((1<<j) and i=0) or (a[j,k]='0') and ((1<<j) and i=1<<j) then inc(d[k]);
    for j:=1 to m do
     inc(ssum,min(d[j],n-d[j]));
    smax:=min(smax,ssum);
   end;
  writeln(smax);
  close(input);
  close(output);
 end.
