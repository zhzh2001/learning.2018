#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 20, M = 1e5+10;
int n,m,a[N][M];
char c[M];
namespace Subtask1{
	int ans,p[20],Sum[33000];
	bitset<16>A,b[300000],c;
	inline void Main(){
		p[0]=1;
		For(i,1,n) p[i]=p[i-1]<<1;
		For(i,0,p[n]-1) For(j,1,n) b[i][j]=((i&p[j-1])>0);
		int x;
		For(j,1,m){
			For(i,1,n) A[i]=(a[i][j]==1);
			For(i,0,p[n]-1) c=A^b[i],x=c.count(),Sum[i]+=min(x,n-x);
		}
		ans=n*m;
		For(i,0,p[n]-1) ans=min(ans,Sum[i]);
		printf("%d",ans);
	}
}
int main(){
	freopen("array.in","r",stdin);
	freopen("array.out","w",stdout);
	n=read(),m=read();
	For(i,1,n){
		scanf("%s",c+1);
		For(j,1,m) a[i][j]=c[j]-'0';
	}
	if (n<=15&&m<=1000) return Subtask1::Main(),0;
}
