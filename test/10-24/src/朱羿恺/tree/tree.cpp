#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 1e5+10;
int n,Q,s,t,x[N],y[N];
int tot,first[N],last[N<<1],to[N<<1],Pos[N<<1],d[N];
inline void Add(int x,int y,int z){to[++tot]=y,Pos[tot]=z,last[tot]=first[x],first[x]=tot;}
namespace Subtask1{
	int h,t,q[N],pre[N],pos[N],dis[N];
	bool vis[N];
	inline void bfs(int x,int y){
		For(i,1,n) vis[i]=pre[i]=pos[i]=0,dis[i]=1e9;
		h=0,dis[q[t=1]=x]=0,vis[x]=1;
		while (h<t){
			int u=q[++h];if (dis[u]>dis[y]) break;
			cross(i,u){
				int v=to[i];
				if (!vis[v]) q[++t]=v,pre[v]=u,pos[v]=i,dis[v]=dis[u]+1,vis[v]=1;
				else if (dis[u]+1==dis[v]&&u<pre[v]) pre[v]=u,pos[v]=i; 
			}
		}
	}
	bool flag[N];
	inline void update(int x,int y){
		bfs(x,y);
		for (int now=y;now!=x;now=pre[now]) flag[Pos[pos[now]]]^=1;
	}
	int fa[N];
	inline int Find(int x){return fa[x]==x?x:fa[x]=Find(fa[x]);}
	inline int Query(){
		For(i,1,n) fa[i]=i;
		For(i,1,n) 
			if (flag[i]){
				int X=Find(x[i]),Y=Find(y[i]);
				if (X!=Y) fa[Y]=X;
			}
		int ans=0;
		For(i,1,n) ans+=(fa[i]==i);
		return ans;
	}
	inline void Main(){while (Q--) s=read(),t=read(),update(s,t),printf("%d\n",Query());}
}
namespace Subtask2{
	int cnt,idx[N],pos[N];
	bool vis[N];
	inline void dfs(int u){
		vis[u]=1,pos[++cnt]=u,idx[u]=cnt;
		cross(i,u) if (!vis[to[i]]) dfs(to[i]);
	}
	const int M = 400;
	int blo,ans,id[N],sum[M][2],v[N],lazy[M];
	inline void Build(int u,int l,int r){
		For(i,l,r) v[i]^=1;
		if (lazy[u]){
			For(i,(u-1)*blo+1,u*blo) v[i]^=1;
			lazy[u]=0;
		}
		sum[u][0]=sum[u][1]=0;
		For(i,(u-1)*blo+1,u*blo) sum[u][v[i]]++;
	}
	inline void change(int l,int r){
		Build(id[l],l,min(r,blo*id[l]));
		if (id[l]!=id[r]) Build(id[r],(id[r]-1)*blo+1,r);
		For(i,id[l]+1,id[r]-1) lazy[i]^=1,swap(sum[i][0],sum[i][1]);
	}
	inline void update(int l,int r,int opt){
	//	printf("%d %d %d\n",l,r,opt);
		if (opt) change(1,l),change(r,n);
			else change(l,r);
	}
	inline void update(int s,int t){
		if (s==t) return;bool flag=0,f=0;
		if (idx[t]<idx[s]) swap(s,t),flag=1;
		int Dis=idx[t]-idx[s];
		int sl=(idx[s]==1)?n:idx[s]-1,sr=(idx[s]==n)?1:idx[s]+1;
		int tl=(idx[t]==1)?n:idx[t]-1,tr=(idx[t]==n)?1:idx[t]+1;
		if (!(n&1)&&Dis==n/2){
		//	if (flag) f=(pos[sl]<pos[sr]);
		//		else f=(pos[tl]<pos[tr]);
			if (pos[sl]<pos[sr]){
				if (sl==n) update(idx[t],sl,0);
					else update(sl,idx[t],1);
			} else update(idx[s],idx[t]-1,0);
		} else {
			if (Dis<n/2) update(idx[s],idx[t]-1,0);
			else {
				if (sl==n) update(idx[t],sl,0);
					else update(sl,idx[t],1);
			}
		}
	}
	inline int Query(){
		int ans=0;
		For(i,1,id[n]) ans+=sum[i][0];
		return ans;
	}
	inline void Main(){
		dfs(1),blo=sqrt(n);
		For(i,1,n) id[i]=(i-1)/blo+1,sum[id[i]][0]++,v[i]=0;
	//	For(i,1,n) printf("%d ",idx[i]);puts("");
	//	For(i,1,n) printf("%d ",pos[i]);puts("");
		while (Q--) s=read(),t=read(),update(s,t),printf("%d\n",Query());
	}
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read(),Q=read();
	For(i,1,n) x[i]=read(),y[i]=read(),Add(x[i],y[i],i),Add(y[i],x[i],i),d[x[i]]++,d[y[i]]++;
	if (n<=1000) return Subtask1::Main(),0;
	bool flag=1;
	For(i,1,n) flag&=(d[i]==2);
	if (flag) return Subtask2::Main(),0;
}
/*
10 5
1 2
2 3
3 4
4 5
5 6
6 7
7 8
8 9
9 10
10 1

7 6
1 6
9 1
3 1
10 9
*/
