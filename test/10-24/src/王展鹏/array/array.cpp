#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define int long long
inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
#define ld long double
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}
const int N=1<<18|3;
int n,m,A[N],B[N],a[N],cnt[N];
char ch[N];
void FWT(int *a){
	for(int i=0;i<n;i++){
		int t=1<<i;
		for(int j=0;j<(1<<n);j+=(t<<1)){
			for(int k=j;k<(j+t);k++){
				int x=a[k],y=a[k+t];
				a[k]=x-y; a[k+t]=x+y;
			}
		}
	}
}
void UFWT(int *a){
	for(int i=0;i<n;i++){
		int t=1<<i;
		for(int j=0;j<(1<<n);j+=(t<<1)){
			for(int k=j;k<(j+t);k++){
				int x=a[k],y=a[k+t];
				a[k]=x+y; a[k+t]=y-x;
			}
		}
	}
}
/*
for(int i=0;i<n;i++)for(int j=0;j<(1<<n);j++)if(j>>i&1)f[j]+=f[j^(1<<i)]*/
signed main(){
	freopen("array.in","r",stdin); freopen("array.out","w",stdout);
	n=read(); m=read();
	for(int i=0;i<n;i++){
		scanf("%s",ch+1);
		for(int j=1;j<=m;j++)a[j]^=(ch[j]-'0')<<i;
	}
	for(int j=1;j<=m;j++)A[a[j]]++;
	for(int j=0;j<(1<<n);j++){
		if(j)cnt[j]=cnt[j>>1]+(j&1);
		B[j]=min(cnt[j],n-cnt[j]);
	}
	FWT(A); FWT(B);
	for(int i=0;i<(1<<n);i++)A[i]=A[i]*B[i];
	UFWT(A); 
	int ans=1e18;
	for(int i=0;i<(1<<n);i++)ans=min(ans,A[i]); 
	cout<<(ans>>n)<<endl;
}
/*
每一个数为ai
x的权值为b[x] 
要对每一个数异或一个y
使得权值最小 
*/
