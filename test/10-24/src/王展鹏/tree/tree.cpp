#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
#define ld long double
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}
const int M=300005,N=100005;
int n,m,nedge,vis[N],fa[N],top[N],peo[N],wson[N],in[N],ri[N],rt[N],quan,ti;
int lazy[M],tree[M],len[M],q[N],f[N],son[N],nextt[M],ed[M];
void aedge(int a,int b){
	nextt[++nedge]=son[a]; son[a]=nedge; ed[nedge]=b;
}
inline int yi(int x){
	return (x&1)?x+1:x-1;
}
void dfs(int p,int la){
	if(m)return;
	vis[p]=1;
	for(int i=son[p];i;i=nextt[i])if(i!=yi(la)){
		if(vis[ed[i]]&&!m){
			for(int j=p;;j=fa[j]){
				//if(j)cout<<j<<" wzp "<<ed[i]<<endl;
				q[m++]=j; if(j==ed[i])break;
			}
			return;
		}
		fa[ed[i]]=p;
		dfs(ed[i],i);
	}
}
void findcircle(){
	dfs(1,0);
}
void dfs1(int p,int f){
	fa[p]=f; peo[p]=1; rt[p]=quan;
	for(int i=son[p];i;i=nextt[i])if(ed[i]!=f&&!ri[ed[i]]){
		dfs1(ed[i],p);
		peo[p]+=peo[ed[i]]; if(!wson[p]||peo[ed[i]]>peo[wson[p]])wson[p]=ed[i];
	}
}
void dfs2(int p){
	in[p]=++ti;
	if(wson[fa[p]]==p)top[p]=top[fa[p]]; else top[p]=p;
	if(wson[p])dfs2(wson[p]);
	for(int i=son[p];i;i=nextt[i])if(ed[i]!=fa[p]&&!ri[ed[i]]&&ed[i]!=wson[p]){
		dfs2(ed[i]);
	}
}
inline void cao(int nod){
	tree[nod]=len[nod]-tree[nod]; lazy[nod]^=1;
}
inline void pushdown(int nod){
	if(lazy[nod]){
		cao(nod<<1); cao(nod<<1|1); lazy[nod]=0;
	}
}
void insert(int l,int r,int i,int j,int nod){//if(nod==1)cout<<i<<" wzp "<<j<<endl;
	if(i>j)return;
	if(l==i&&r==j){
		cao(nod); return;
	}
	pushdown(nod);
	int mid=(l+r)>>1;
	if(j<=mid)insert(l,mid,i,j,nod<<1); else if(i>mid)insert(mid+1,r,i,j,nod<<1|1);
	else {
		insert(l,mid,i,mid,nod<<1); insert(mid+1,r,mid+1,j,nod<<1|1);
	}
	tree[nod]=tree[nod<<1]+tree[nod<<1|1];
}
int ask(int l,int r,int i,int j,int nod){
	if(i>j)return 0;
	if(l==i&&r==j)return tree[nod];
	pushdown(nod);
	int mid=(l+r)>>1;
	if(j<=mid)return ask(l,mid,i,j,nod<<1); else if(i>mid)return ask(mid+1,r,i,j,nod<<1|1);
	else return ask(l,mid,i,mid,nod<<1)+ask(mid+1,r,mid+1,j,nod<<1|1);
}
void bao(int x){
	if(ri[x])return;
	for(int i=x;i;i=fa[top[i]]){
		insert(1,n,in[top[i]],in[i],1); 
	}
}
void insert(int x,int y){
	if(x==y)return; x++; y++; //cout<<x<<" wzp "<<y<<endl;
	insert(1,n,min(x,y),max(x,y)-1,1);
	if(x>y)insert(1,n,1,m,1);
}
void build(int l,int r,int nod){
	tree[nod]=0; len[nod]=r-l+1;
	if(l==r)return;
	int mid=(l+r)>>1;
	build(l,mid,nod<<1); build(mid+1,r,nod<<1|1);
	tree[nod]=tree[nod<<1]+tree[nod<<1|1];
}
inline int dis(int x,int y){
	if(x>y)return y+m-x; else return y-x;
}
int main(){
	freopen("tree.in","r",stdin); freopen("tree.out","w",stdout);
	n=read(); int Q=read(); 
	for(int i=1;i<=n;i++){
		int x=read(),y=read();
		aedge(x,y); aedge(y,x);
	}
	findcircle(); 
	for(int i=0;i<m;i++){ in[q[i]]=++ti; ri[q[i]]=1;}
	for(int i=0;i<m;i++){
		top[q[i]]=q[i]; rt[q[i]]=i;
		for(int j=son[q[i]];j;j=nextt[j])if(!ri[ed[j]]){
			quan=i; dfs1(ed[j],0); dfs2(ed[j]);
		}
	}
	if(m%2==0)for(int i=0;i<m;i++){
		f[i]=m==2?(i^1):(q[(i+1)%m]<q[(i+m-1)%m]);
		//if(q[(i+1)%m]!=q[(i+m-1)%m])
	}
	build(1,n,1);
	while(Q--){
		int x=read(),y=read();
		//cout<<x<<" "<<y<<" "<<rt[x]<<" "<<rt[y]<<endl;
		//for(int i=x;i;i=fa[i])cout<<i<<endl; for(int i=y;i;i=fa[i])cout<<i<<endl;
		bao(x); bao(y); //cout<<rt[x]<<" ajl "<<rt[y]<<" "<<dis(3,0)<<endl;
		if(rt[x]!=rt[y]){
			if(dis(rt[x],rt[y])<dis(rt[y],rt[x])){
				insert(rt[x],rt[y]);
			}else if(dis(rt[x],rt[y])>dis(rt[y],rt[x])){
				insert(rt[y],rt[x]);
			}else if(f[rt[x]]){
				insert(rt[x],rt[y]);
			}else {
				insert(rt[y],rt[x]);
			}
		}
		printf("%d\n",n-ask(1,n,1,n,1)+(ask(1,n,1,m,1)==m));
		//cout<<ask(1,n,1,n,1)<<" "<<endl;
	}
}
/*
4 2
1 2
2 3
3 4
4 1
1 3
1 2

*/
