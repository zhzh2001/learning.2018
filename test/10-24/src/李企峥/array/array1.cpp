#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for(int i=a;i<=b;i++)
#define Rep(i,a,b) for(int i=b;i>=a;i--)
using namespace std;
ll n,m,sum;
ll a[20][100100];
ll b[20];
ll c[100100];
bool f1[20];
bool f2[100100];
inline ll read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
bool do1()
{
	bool flag=false;
	For(i,1,n)
	{
		if((b[i]==m-b[i]&&f1[i])||b[i]>m-b[i])
		{
			For(j,1,m)
			{
				if(a[i][j]==0)
				{
					c[j]++;
					a[i][j]=1;
				}
				else
				{
					c[j]--;
					a[i][j]=0;
				}
				f2[j]=true;
			}
			flag=true;
			b[i]=m-b[i];
		}
	}
	return flag;
}
bool do2()
{
	bool flag=false;
	For(j,1,m)
	{
		if((c[j]==n-c[j]&&f2[j])||c[j]>n-c[j])
		{
			For(i,1,n)
			{
				if(a[i][j]==0)
				{
					b[i]++;
					a[i][j]=1;
				}
				else
				{
					b[i]--;
					a[i][j]=0;
				}
				f1[i]=true;
			}
			flag=true;
			c[j]=n-c[j];
		}
	}
	return flag;
}
int main()
{
	freopen("array.in","r",stdin);
	freopen("array.out","w",stdout);
	n=read();m=read();
	For(i,1,n)
	{
		For(j,1,m)
		{
			char ch=getchar();
			while(ch!='0'&&ch!='1')ch=getchar();
			a[i][j]=ch-'0';
			b[i]+=a[i][j];
			c[j]+=a[i][j];
			f1[i]=true;
			f2[j]=true;
		}
	}
	while(true)
	{
		bool flag1,flag2;
		flag1=do1();
		/*For(i,1,n)
		{
			For(j,1,m)cout<<a[i][j];
			cout<<endl;
		}*/
		flag2=do2();
		/*For(i,1,n)
		{
			For(j,1,m)cout<<a[i][j];
			cout<<endl;
		}*/
		if(!(flag1||flag2))break;
	}
	For(i,1,n)
		For(j,1,m)
			sum+=a[i][j];
	cout<<sum<<endl;
	return 0;
}

