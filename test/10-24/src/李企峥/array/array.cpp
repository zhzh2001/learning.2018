#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for(int i=a;i<=b;i++)
#define Rep(i,a,b) for(int i=b;i>=a;i--)
using namespace std;
inline ll read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
string s[20];
int f[1<<18];
int k[1<<18];
int x[100100];
int ans,n,m;
int main()
{
	freopen("array.in","r",stdin);
	freopen("array.out","w",stdout);
	n=read();m=read();
	For(i,1,n)cin>>s[i];
	For(i,1,m)
	{
		x[i]=0;
		For(j,1,n)x[i]=x[i]*2+(int(s[j][i-1]-'0'));
	}
	for(int i=(1<<n)-1;i>=0;i--)
	{
		int tmp=0;
		int now=i;
		while(now)
		{
			tmp+=now&1;
			now/=2;
		}
		k[i]=max(n-tmp,tmp);
	}
	for(int i=(1<<n)-1;i>=0;i--)
	{
		For(j,1,m)
			f[i]+=k[i^x[j]];
		ans=max(f[i],ans);
	}
	cout<<n*m-ans<<endl;
	return 0;
}
/*
3 4
0110
1010
0111
*/
