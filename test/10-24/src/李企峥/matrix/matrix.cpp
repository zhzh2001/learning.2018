#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for(int i=a;i<=b;i++)
using namespace std;
ll n;
int a[3000][3000];
inline ll read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
int main()
{
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	srand(time(0));
	n=read();
	For(i,1,n)
	{
		For(j,1,n)
		{
			a[i][j]=read();
		}
	}
	For(i,1,n)
	{
		if(a[i][i]!=0)
		{
			cout<<1<<endl;
			cout<<i<<" 0 0"<<endl;
			return 0;
		}
	}
	For(i,1,n)
	{
		For(j,1,n)
			if(a[i][j]!=a[j][i])
			{
				cout<<2<<endl;
				cout<<i<<" "<<j<<" 0"<<endl;
				return 0;
			}
	}
	For(kkk,1,2e7/n)
	{
		int i=rand()%n+1;
		int j=rand()%n+1;
		For(k,1,n)
		{
			if(a[i][j]<=max(a[i][k],a[k][j]))continue;
			cout<<3<<endl;
			cout<<i<<" "<<j<<" "<<k<<endl;
			return 0;
		}
	}
	cout<<0<<endl;
	return 0;
}

