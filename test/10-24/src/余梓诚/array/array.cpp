#include <ctime>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

void judge(){
    freopen("array.in","r",stdin);
    freopen("array.out","w",stdout);
}

inline int read(){
	int x=0;
	char c=getchar();
	for (;c<'0'||c>'9';c=getchar());
	for (;c>='0'&&c<='9';c=getchar())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}

const int N=110000,inf=1<<30;
int n,m,a[20][N],sumy[N],ans=inf,b[N];
char s[N];

int main(){
	judge();
	n=read(); m=read();
	for (register int i=1;i<=n;i++){
		scanf("%s",s+1);
		for (register int j=1;j<=m;j++){
			a[i][j]=s[j]-'0';
			sumy[j]+=a[i][j];
		}
	}
	int Num=(1<<n)-1;
	for (register int s=0;s<=Num;s++){
		for (register int i=1;i<=m;i++) b[i]=sumy[i];
		for (register int i=1;i<=n;i++){
			if (!(s&(1<<(i-1)))) continue;
			for (int j=1;j<=m;j++){
				if (a[i][j]==0) b[j]++;
				else b[j]--;
			}
		}
		int t=0;
		for (register int i=1;i<=m;i++) t+=min(b[i],n-b[i]);
		ans=min(ans,t);
	}
	printf("%d\n",ans);
	return 0;
}

/*
1100
0010
1101
0011
*/
