#pragma GCC optimize 2
#include <bits/stdc++.h>
#define N 2520
using namespace std;

inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}

inline void success(int type, int i, int j, int k) {
  printf("%d\n", type);
  printf("%d %d %d\n", i, j, k);
  exit(0);
}
inline void success(int i, int j, int k) {
  success(3, i, j, k);
}
inline void success(int i, int j) {
  success(2, i, j, 0);
}
inline void success(int i) {
  success(1, i, 0, 0);
}

int a[N][N], cnt;
bool vis[N][N];
typedef bitset<N> bst;
bst bto[N];

struct edge {
  int x, y, len;
}e[N*N/2];

inline bool cmp(const edge &a, const edge &b) {
  return a.len < b.len;
}
inline void insert(int x, int y, int len) {
  e[++ cnt].x = x;
  e[cnt].y = y;
  e[cnt].len = len;
}


int main(int argc, char const *argv[]) {
  
  freopen("matrix.in", "r", stdin);
  freopen("matrix.out", "w", stdout);

  int n = read();

  for (int i = 1; i <= n; ++ i) {
    for (int j = 1; j <= n; ++ j) {
      a[i][j] = read();
    }
  }

  for (int i = 1; i <= n; ++ i) {
    if (a[i][i]) {
      success(i);
    }
  }

  for (int i = 1; i <= n; ++ i) {
    for (int j = i + 1; j <= n; ++ j) {
      if (a[i][j] != a[j][i]) {
        success(i, j);
      }
      insert(i, j, a[i][j]);
    }
  }
  sort(e + 1, e + cnt + 1, cmp);
  // find (m, x) < (x, y) && (m, y) < (x, y)
  // the solution is (i=x, j=y, k=m)
  for (int i = 1, j = 1; i <= cnt; i = ++ j) {
    while (j < cnt && e[j + 1].len == e[i].len) {
      ++ j;
    }

    for (int k = i; k <= j; ++ k) {
      int x = e[k].x, y = e[k].y;
      bst ass = bto[x] & bto[y];
      if (ass.count() > 0) {
        // get the solution
        for (int m = 1; m <= n; ++ m) {
          if (ass[m]) {
            // printf("%d %d\n", a[x][y], (int) bto[x][y]);
            // printf("%d %d\n", a[m][x], (int) bto[m][x]);
            // printf("%d %d\n", a[m][y], (int) bto[m][y]);
            success(x, y, m);
          }
        }
      }
    }

    for (int k = i; k <= j; ++ k) {
      int x = e[k].x, y = e[k].y;
      bto[x][y] = 1;
      bto[y][x] = 1;
    }
  }

  puts("0");

  return 0;
}