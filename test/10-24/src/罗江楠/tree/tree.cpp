#include <bits/stdc++.h>
#define N 100020
using namespace std;

inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}

int to[N<<1], nxt[N<<1], cnt, head[N];
inline void insert(int x, int y) {
  to[++ cnt] = y; nxt[cnt] = head[x]; head[x] = cnt;
  to[++ cnt] = x; nxt[cnt] = head[y]; head[y] = cnt;
}

struct __segment_t {
  bool lz[N<<2];
  int ans0[N<<2], ans1[N<<2];
  inline void push_down(int x) {
    int ls = x << 1, rs = x << 1 | 1;
    if (lz[x]) {
      swap(ans0[ls], ans1[ls]);
      swap(ans0[rs], ans1[rs]);
      lz[ls] ^= 1;
      lz[rs] ^= 1;
      lz[x] = 0;
    }
  }
  inline void push_up(int x) {
    int ls = x << 1, rs = x << 1 | 1;
    ans0[x] = ans0[ls] + ans0[rs];
    ans1[x] = ans1[ls] + ans1[rs];
  }
  void build(int x, int l, int r) {
    if (l == r) {
      ans0[x] = 1;
      return;
    }
    int mid = (l + r) >> 1;
    build(x << 1, l, mid);
    build(x << 1 | 1, mid + 1, r);
    push_up(x);
  }
  // reverse [l, r] (0 -> 1, 1 -> 0)
  void update(int x, int l, int r, int L, int R) {
    // if (x == 1) printf("update: %d %d of [%d, %d]\n", l ,r, L, R);
    if (l == L && r == R) {
      swap(ans0[x], ans1[x]);
      lz[x] ^= 1;
      return;
    }
    push_down(x);
    int mid = (L + R) >> 1;
    if (r <= mid) update(x << 1, l, r, L, mid);
    else if (l > mid) update(x << 1 | 1, l, r, mid + 1, R);
    else update(x << 1, l, mid, L, mid), update(x << 1 | 1, mid + 1, r, mid + 1, R);
    push_up(x);
  }
  int query1(int x, int l, int r, int L, int R) {
    if (l == L && r == R) {
      return ans1[x];
    }
    push_down(x);
    int mid = (L + R) >> 1;
    if (r <= mid) return query1(x << 1, l, r, L, mid);
    if (l > mid) return query1(x << 1 | 1, l, r, mid + 1, R);
    return query1(x << 1, l, mid, L, mid) + query1(x << 1 | 1, mid + 1, r, mid + 1, R);
  }
}sp, hp;

bool vis[N], ishuan[N];
int q[N], qtop, huan_size, huan[N];
bool find_huan(int x, int f) {
  vis[x] = true;
  q[++ qtop] = x;
  for (int i = head[x]; i; i = nxt[i]) {
    if (to[i] == f) continue;
    if (vis[to[i]]) {
      while (q[qtop + 1] != to[i]) {
        ishuan[q[qtop]] = true;
        huan[++ huan_size] = q[qtop];
        -- qtop;
      }
      return true;
    }
    if (find_huan(to[i], x)) {
      return true;
    }
  }
  vis[x] = false;
  -- qtop;
  return false;
}

int son[N], fa[N], belong[N], dep[N];
int dfs_tree(int x, int bel) {
  belong[x] = bel;
  int max_size = 0, total_size = 1, max_point = 0;
  for (int i = head[x]; i; i = nxt[i]) {
    if (to[i] == fa[x] || ishuan[to[i]]) {
      continue;
    }
    fa[to[i]] = x;
    dep[to[i]] = dep[x] + 1;
    int sz = dfs_tree(to[i], bel);
    if (!max_point || sz > max_size) {
      max_size = sz;
      max_point = to[i];
    }
    total_size += sz;
  }
  son[x] = max_point;
  return total_size;
}
int inq[N], inq_cnt, top[N], out[N];
void dfs_top(int x, int tp) {
  inq[x] = ++ inq_cnt;
  top[x] = tp;
  if (son[x]) dfs_top(son[x], tp);
  for (int i = head[x]; i; i = nxt[i]) {
    if (ishuan[to[i]] || to[i] == fa[x] || to[i] == son[x]) {
      continue;
    }
    dfs_top(to[i], to[i]);
  }
  out[x] = inq_cnt;
}

void shulianpoufen(int x, int y) {
  int topx = top[x], topy = top[y];
  while (topx != topy) {
    if (dep[topx] < dep[topy]) {
      swap(topx, topy);
      swap(x, y);
    }
    sp.update(1, inq[topx], inq[x], 1, inq_cnt);
    x = fa[topx];
    topx = top[x];
  }
  // now they are in the same lian
  if (dep[x] == dep[y]) return;
  if (dep[x] < dep[y]) swap(x, y);
  sp.update(1, inq[y] + 1, inq[x], 1, inq_cnt);
}

int edges[N], total_edges;

// x, y must in the same tree
void resolve_tree(int x, int y) {
  shulianpoufen(x, y);
  int root = belong[x];
  int now = sp.query1(1, inq[root], out[root], 1, inq_cnt);
  total_edges += now - edges[root];
  edges[root] = now;
}

int hid[N], hpre[N], hnxt[N], total_huans;
void resolve_huan(int x, int y) {
  // printf("resolve_huan: %d %d\n", x, y);
  if (x == y) puts("timber is akking");
  if (hid[x] > hid[y]) swap(x, y);
  int rlen = hid[y] - hid[x] - 1;
  int llen = huan_size - 2 - rlen;
  if (rlen < llen || (rlen == llen && hnxt[x] < hpre[x])) {
    hp.update(1, hid[x], hid[y] - 1, 1, huan_size);
  } else {
    if (hid[x] > 1) {
      hp.update(1, 1, hid[x] - 1, 1, huan_size);
    }
    hp.update(1, hid[y], huan_size, 1, huan_size);
  }
  total_huans = hp.query1(1, 1, huan_size, 1, huan_size);
  if (total_huans == huan_size) {
    -- total_huans;
  }
}

void resolve(int x, int y) {
  if (belong[x] == belong[y]) {
    // works in a tree
    resolve_tree(x, y);
    return;
  }
  resolve_tree(x, belong[x]);
  resolve_tree(y, belong[y]);
  resolve_huan(belong[x], belong[y]);
}

map<pair<int, int>, int> checker;
int main(int argc, char const *argv[]) {
  
  freopen("tree.in", "r", stdin);
  freopen("tree.out", "w", stdout);

  int n = read(), Q = read();
  for (int i = 1; i <= n; ++ i) {
    int x = read(), y = read();
    if (x > y) swap(x, y);
    insert(x, y);
    pair<int, int> rsk = make_pair(x, y);
    if (checker[rsk]) {
      huan_size = 2;
      huan[1] = x;
      huan[2] = y;
    }
    checker[rsk] = 1;
  }

  if (!huan_size) {
    find_huan(1, 0);
  }

  for (int i = 1; i <= huan_size; ++ i) {
    hid[huan[i]] = i;
    hnxt[huan[i]] = huan[i + 1];
    hpre[huan[i]] = huan[i - 1];
    dfs_tree(huan[i], huan[i]);
    dfs_top(huan[i], huan[i]);
    // printf("%d ", huan[i]);
  }
  // puts("");
  hnxt[huan[huan_size]] = huan[1];
  hpre[huan[1]] = huan[huan_size];

  sp.build(1, 1, inq_cnt);
  hp.build(1, 1, huan_size);

  for (int i = 1; i <= Q; ++ i) {
    int x = read(), y = read();
    resolve(x, y);
    // printf("total_edges = %d\n", total_edges);
    // printf("total_huans = %d\n", total_huans);
    printf("%d\n", n - total_huans - total_edges);
  }

  return 0;
}

/*

树链剖分？？？

怒写 6k 代码

好像有重边？

2 2
1 2
1 2
1 2
1 2

Answer: 1 2
Old Output: Runtime Error
Fixed Output: 1 2

RP++

*/