#include <bits/stdc++.h>
#define N 300020
using namespace std;

inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}

int popcnt[N], mpt[N], xs[N];

char str[20][100020];

int main(int argc, char const *argv[]) {

  freopen("array.in", "r", stdin);
  freopen("array.out", "w", stdout);

  int n = read(), m = read();
  int max_size = 1 << n;
  for (int i = 1; i < max_size; ++ i) {
    popcnt[i] = popcnt[i >> 1] + (i & 1);
  }
  for (int i = 1; i < max_size; ++ i) {
    mpt[i] = min(popcnt[i], n - popcnt[i]);
  }

  for (int i = 1; i <= n; ++ i) {
    scanf("%s", str[i] + 1);
  }
  for (int i = 1; i <= m; ++ i) {
    int x = 0;
    for (int j = 1; j <= n; ++ j) {
      x = (x << 1) + (str[j][i] == '1');
    }
    xs[i] = x;
  }

  int ans = 1 << 30;
  for (int i = 0; i < max_size; ++ i) {
    int res = 0;
    for (int j = 1; j <= m; ++ j) {
      res += mpt[xs[j] ^ i];
    }
    ans = min(ans, res);
  }

  printf("%d\n", ans);

  return 0;
}