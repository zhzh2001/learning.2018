#include<bits/stdc++.h>
#define For(i,j,k) for (int i=j;i<=k;i++)
using namespace std;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}
inline int read01(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	return c-'0';
}
int n,m,ans;
char a[20][100005];
int f[20][(1<<18)+1];
int main(){
	freopen("array.in","r",stdin);
	freopen("array.out","w",stdout);
	n=read(),m=read();
	ans=1e9;
	For(i,1,n) For(j,1,m) a[i][j]=read01();
	For(i,1,m){
		int tmp=0;
		For(j,1,n)
			tmp=tmp*2+a[j][i];
		f[0][tmp]++;
	}
	For(i,0,n-1)
		for (int j=n;j;j--)
			For(k,0,(1<<n)-1)
				f[j][k]+=f[j-1][k^(1<<i)];
	For(k,0,(1<<n)-1){
		int tmp=0;
		For(i,0,n) tmp+=min(i,n-i)*f[i][k];
		ans=min(ans,tmp);
	}
	printf("%d",ans);
}
