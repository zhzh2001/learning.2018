#include<cstdio>
#include<cstring>
#include<string>
#include<algorithm>
#include<iostream>
#define re register
#define rep(x,a,b) for (re int x=(int)a;x<=b;++x)
#define drp(x,a,b) for (re int x=(int)a;x>=b;--x)
#define LL long long
#define inf 0x3f3f3f3f
using namespace std;
const int N=1010;
inline LL read(){
    LL x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
int n,T,Q[N<<1],QQ[N<<1],pre[N];
bool Map[N][N],a[N][N],vis[N],Color[N];
inline void BFS(int st){
	int hh=0,tt=1;QQ[1]=st;Color[st]=1;
	while (hh<tt){
		int Now=QQ[++hh];
		rep(i,1,n){
			if ((i!=Now)&&(!Color[i])&&(Map[Now][i])){
				Color[i]=1;
				QQ[++tt]=i;
			}
		}
	}
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read(),T=read();//printf("%d ",T);
	rep(i,1,n){
		int x=read(),y=read();
		a[x][y]=a[y][x]=1;
	}
	memset(Map,0,sizeof(Map));
	rep(i,1,T){
		int cnt=0;
		int st=read(),en=read();
		memset(vis,0,sizeof(vis));
		memset(Q,0,sizeof(Q));
		int h=0,t=1;Q[1]=st;vis[st]=1;
		while (h<t){
			bool flag=0;
			h++;
			int u=Q[h];
		//	printf("%d\n",u);
			rep(j,1,n){
			//	printf("%d %d %d\n",j,vis[]);
				if ((j!=u)&&(!vis[j])&&(a[j][u])){
					Q[++t]=j;
					vis[j]=1;
					pre[j]=u;
					if (j==en) {
						flag=1;
						break;
					}
				}
			}
			if (flag) break;
		}
	//	printf("%d %d",a[2][1],vis[2]);
	//	rep(j,1,n) printf("%d ",vis[j]);
	//	printf("1");
	//	printf("%d ",t);
	//	rep(j,1,t) printf("%d ",Q[j]);
	//	printf("1");
		int j=en;
		//printf("%d\n",en);
	//	rep(j,1,n) printf("%d ",pre[j]);
	//	printf("\n");
	//	j=en;
		while (j!=st) {
		//	edg[++cnt].x=pre[j];
		//	edg[cnt].y=j;
		//	printf("%d ",j);
			Map[j][pre[j]]=!Map[j][pre[j]];
			Map[pre[j]][j]=Map[j][pre[j]];
			j=pre[j];
		//	printf("%d\n",j);
		}
		//printf("\n");
		int ans=0;
		memset(Color,0,sizeof(Color));
		while (1){
			int k=0;
			rep(i,1,n) 
				if(!Color[i]) {
					k=i;break;
				}
		//	printf("%d\n",k);
			if (k==0) break;
			//printf("%d ",Map[1][2]);
		//	rep(j,1,n) if (Color[j]) printf("%d ",j);
		//	printf("\n");
			ans++;
			BFS(k);				
		}
		printf("%d\n",ans);
	}
	return 0;
}

