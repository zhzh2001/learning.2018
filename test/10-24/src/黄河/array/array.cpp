#include <cstdio>
#include <cstring>
namespace hh_AK_IOI{
	int n, m;
	char a[20][100005];
	int s[100005], ans, sum;
	int Main(){
		freopen("array.in", "r", stdin);
		freopen("array.out", "w", stdout);
		scanf("%d%d", &n, &m);
		for (register int i = 0; i < n; ++i){
			scanf("%s", a[i] + 1);
			for (register int j = 1; j <= m; ++j) a[i][j] -= 48;
		}
		for (register int j = 1; j <= m; ++j)
			for (register int i = 0; i < n; ++i) s[j] += a[i][j], ans += a[i][j];
		for (register int p = 1, t = (1 << n); p < t; ++p){
			for (register int i = 0, x = p & -p; x; x >>= 1, ++i)
				for (register int j = 1; j <= m; ++j)
					a[i][j] ? --s[j] : ++s[j], a[i][j] = !a[i][j];
			sum = 0;
			for (register int i = 1; i <= m; ++i)
				sum += (s[i] << 1) <= n ? s[i] : n - s[i];
			sum < ans ? ans = sum : 0;
		}
		printf("%d", ans);
	}
}
int main(){
	return hh_AK_IOI :: Main(), 0;
}
