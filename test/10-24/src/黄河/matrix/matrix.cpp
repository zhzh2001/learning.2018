#include<cstdio>
#include<cstring>
#include<string>
#include<algorithm>
#include<iostream>
#include<cctype>
#include<ctime>
#define re register
#define rep(x,a,b) for (re int x=(int)a;x<=b;++x)
#define drp(x,a,b) for (re int x=(int)a;x>=b;--x)
#define LL long long
#define inf 0x3f3f3f3f
using namespace std;
const int N=2510;
inline LL read(){
    LL x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
int n,a[N][N];
int main(){
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	srand(time(NULL));
	n=read();
	rep(i,1,n){
		rep(j,1,n) a[i][j]=read();
	}
	rep(i,1,n){
		if (a[i][i]!=0){
			printf("1\n");
			printf("%d 0 0",i);
			return 0;
		}
	}
	rep(i,1,n){
		rep(j,i+1,n){
			if (a[i][j]!=a[j][i]){
				printf("2\n");
				printf("%d %d 0",i,j);
				return 0;
			}
		}
	}
	if (n<=500){
		rep(i,1,n){
			rep(j,1,n){
				rep(k,1,n){
					if (a[i][j]>max(a[i][k],a[k][j])){
						printf("3\n");
						printf("%d %d %d",i,j,k);
						return 0;
					}
				}
			}
		}		
	}else{
		rep(i,1,90000000){
			int x=rand()%n+1,y=rand()%n+1,z=rand()%n+1;
			if (a[x][y]>max(a[x][z],a[z][y])){
						printf("3\n");
						printf("%d %d %d",x,y,z);
						return 0;				
			}
		}
	}
	printf("0\n");
	return 0;
}
