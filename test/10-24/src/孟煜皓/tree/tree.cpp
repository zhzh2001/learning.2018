#include <cstdio>
#include <cctype>
#include <cstring>
#include <algorithm>
#include <vector>
using std :: vector;
namespace fy_AK_IOI{
	int read(){
		register int x = 0;
		register int f = 1, ch = getchar();
		for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = 0;
		for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
		return f ? x : -x;
	}
	#define N 100005
	int n, q;
	vector<int> E[N], ex[N];
	int top, sta[N], stae[N], vis[N], len, cy[N], cye[N], id[N];
	int fa[N], fae[N], rt[N], dep[N];
	bool find_cycle(int u, int fa = 0){
		vis[u] = 1, sta[++top] = u;
		for (register int i = 0, v; i < E[u].size(); ++i)
			if ((v = E[u][i]) != fa){
				stae[top] = i;
				if (vis[v]){
					while (sta[top + 1] != v)
						cy[++len] = sta[top], cye[len] = stae[top], id[sta[top]] = len, vis[sta[top--]] = 0;
					return 1;
				}
				else if (find_cycle(v, u)) return 1;
			}
		return --top, vis[u] = 0;
	}
	void dfs(int u, int Rt){
		rt[u] = Rt;
		for (register int i = 0, v; i < E[u].size(); ++i)
			if ((v = E[u][i]) != fa[u] && !id[v]) fa[v] = u, dep[v] = dep[u] + 1, dfs(v, Rt);
			else if (v == fa[u]) fae[v] = i;
	}
	void Not(int &x){ x = !x; }
	void Solve_Tree(int u, int v){
		while (u != v){
			if (dep[u] < dep[v]) std :: swap(u, v);
			Not(ex[u][fae[u]]), u = fa[u];
		}
	}
	int Nxt(int u, int add){
		return cy[(id[u] + add + len - 1) % len + 1];
	}
	void Solve_Cycle(int u, int v){
		if (id[u] < id[v]) std :: swap(u, v);
		if (id[u] - id[v] < id[v] + len - id[u] || id[u] - id[v] == id[v] + len - id[u] && Nxt(u, 1) > Nxt(u, -1))
			while (u != v) Not(ex[u][cye[id[u]]]), u = Nxt(u, -1);
		else
			while (u != v) Not(ex[v][cye[id[v]]]), v = Nxt(v, -1);
	}
	int Fa[N];
	int find(int x){
		return Fa[x] == x ? x : (Fa[x] = find(Fa[x]));
	}
	void merge(int x, int y){
		int fx = find(x), fy = find(y);
		if (fx != fy) Fa[fy] = fx;
	}
	int Main(){
		freopen("tree.in", "r", stdin);
		freopen("tree.out", "w", stdout);
		n = read(), q = read();
		for (register int i = 1; i <= n; ++i){
			int u = read(), v = read();
			E[u].push_back(v), E[v].push_back(u);
			ex[u].push_back(0), ex[v].push_back(0);
		}
		for (register int i = 1; i <= n; ++i) std :: sort(E[i].begin(), E[i].end());
		find_cycle(1);
		for (register int i = 1; i <= len; ++i) dfs(cy[i], cy[i]);
		while (q--){
			int u = read(), v = read();
			if (rt[u] == rt[v]) Solve_Tree(u, v);
			else Solve_Tree(u, rt[u]), Solve_Tree(v, rt[v]), Solve_Cycle(rt[u], rt[v]);
			for (register int i = 1; i <= n; ++i) Fa[i] = i;
			for (register int i = 1; i <= n; ++i)
				for (register int j = 0; j < E[i].size(); ++j)
					if (ex[i][j]) merge(i, E[i][j]);
			int ans = 0;
			for (register int i = 1; i <= n; ++i) ans += Fa[i] == i;
			printf("%d\n", ans);
		}
	}
}
int main(){
	return fy_AK_IOI :: Main(), 0;
}
