#include <cstdio>
#include <cctype>
#include <cstdlib>
namespace zyk_AK_IOI{
	char buf[65000000], *ps = buf;
	int read(){
		register int x = 0;
		for (; !isdigit(*ps); ++ps) ;
		for (; isdigit(*ps); ++ps) x = (x << 1) + (x << 3) + (*ps ^ '0');
		return x;
	}
	#define N 2505
	int n, a[N][N], r[N], c[N];
	int Main(){
		freopen("matrix.in", "r", stdin);
		freopen("matrix.out", "w", stdout);
		srand(19260817);
		buf[fread(buf, 1, 65000000, stdin)] = -1;
		n = read();
		for (register int i = 1; i <= n; ++i)
			for (register int j = 1; j <= n; ++j)
				a[i][j] = read();
		for (register int i = 1; i <= n; ++i)
			if (a[i][i]) return printf("1\n%d 0 0\n", i), 0;
		for (register int i = 1; i <= n - 1; ++i)
			for (register int j = i + 1; j <= n; ++j)
				if (a[i][j] != a[j][i]) return printf("2\n%d %d 0\n", i, j), 0;
		if (n <= 700){
			for (register int i = 1; i <= n - 1; ++i)
				for (register int j = i + 1; j <= n; ++j)
					for (register int k = 1; k <= n; ++k)
						if (a[i][j] > a[i][k] && a[i][j] > a[k][j])
							return printf("3\n%d %d %d\n", i, j, k), 0;
			return printf("0"), 0;
		}
		for (register int i = 1; i <= n; ++i)
			for (register int j = 1; j <= n; ++j){
				if (!r[i] || a[i][j] < a[i][r[i]]) r[i] = j;
				if (!c[j] || a[i][j] < a[c[j]][j]) c[j] = i;
			}
		for (register int i = 1; i <= n; ++i)
			for (register int j = 1; j <= n; ++j){
				if (a[i][j] > a[i][r[i]] && a[i][j] > a[r[i]][j])
					return printf("3\n%d %d %d\n", i, j, r[i]), 0;
				if (a[i][j] > a[c[j]][j] && a[i][j] > a[i][c[j]])
					return printf("3\n%d %d %d\n", i, j, c[j]), 0;
			}
		for (register int t = 1; t <= 10000000; ++t){
			register int i = rand() % n + 1, j = rand() % n + 1, k = rand() % n + 1;
			if (a[i][j] > a[i][k] && a[i][j] > a[k][j])
				return printf("3\n%d %d %d\n", i, j, k), 0;
		}
		return printf("0"), 0;
	}
}
int main(){
	return zyk_AK_IOI :: Main(), 0;
}
