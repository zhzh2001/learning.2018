#include <bits/stdc++.h>

#define Max 3000

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

int n,a[Max][Max];

inline bool check(int x,int y){
	for(int i=1;i<=n;i++){
		if(a[x][y]>max(a[x][i],a[i][y])){
			puts("3");
			write(x);putchar(' ');write(y);putchar(' ');writeln(i);
			return true;
		}
	}
	return false;
}

int main(){
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++){
			a[i][j]=read();
		}
	}
	for(int i=1;i<=n;i++){
		if(a[i][i]){
			puts("1");
			write(i);putchar(' ');putchar('0');putchar(' ');puts("0");
			return 0;
		}
		for(int j=1;j<i;j++){
			if(a[i][j]!=a[j][i]){
				puts("2");
				write(i);putchar(' ');write(j);putchar(' ');puts("0");
				return 0;
			}
		}
	}
	for(int i=n;i>=1;i--){
		for(int j=1;j<i;j++){
			if(check(i,j))return 0;
		}
	}
	puts("0");
	return 0;
}
