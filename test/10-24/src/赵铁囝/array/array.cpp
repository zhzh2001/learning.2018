#include <bits/stdc++.h>

#define Max 1005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

int n,m,now,ans,sum[Max],a[16][Max],num[1<<16];
char s[16][Max];

inline int calc(int x){
	int sum=0;
	while(x){
		if(x&1)sum++;
		x>>=1;
	}
	return sum;
}

int main(){
	freopen("array.in","r",stdin);
	freopen("array.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=n;i++){
		scanf("%s",s[i]+1);
	}
	for(int j=1;j<=m;j++){
		for(int i=1;i<=n;i++){
//			cout<<s[i][j]<<endl;
			sum[j]=(sum[j]<<1)+s[i][j]-'0';
		}
	}
	for(int i=0;i<(1<<n);i++){
		num[i]=calc(i);
	}
//	for(int i=1;i<=m;i++)cout<<sum[i]<<endl;
	ans=1e9;
	for(int i=0;i<(1<<n);i++){
		now=0;
		for(int j=1;j<=m;j++){
//			cout<<i<<" "<<j<<" "<<num[i^sum[j]]<<endl;
			now+=min(num[i^sum[j]],n-num[i^sum[j]]);
		}
//		cout<<i<<" "<<now<<endl;
		ans=min(ans,now);
	}
	writeln(ans);
	return 0;
}
