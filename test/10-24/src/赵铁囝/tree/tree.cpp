#include <bits/stdc++.h>

#define Max 100005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

struct Edge{
	int v,to;
	bool flag;
}e[Max*2];

struct Segtree{
	int l,r,sum,lazy;
}st[Max*4],st2[Max*4];

int n,m,l,r,x,y,stk_size,size,cnt,cnt2,num2[Max],ffa[Max],id[Max],id2[Max],num[Max],a[Max],fa[Max],dep[Max],son[Max],hs[Max],top[Max],stk[Max],head[Max];
bool flag,root[Max];

inline void add(int u,int v){
	e[++size].v=v;e[size].to=head[u];head[u]=size;
}

inline void dfs(int u,int fa,int root){
	stk[++stk_size]=u;
	for(int i=head[u];i;i=e[i].to){
		if(i/2==fa/2)continue;
		int v=e[i].v;
		if(v==root){
			flag=true;
			return;
		}
		dfs(v,i,root);
		if(flag)return;
	}
	stk_size--;
}

inline void dfs2(int u,int last){
	son[u]=1;
	for(int i=head[u];i;i=e[i].to){
		int v=e[i].v;
		if(root[v])v=n+1;
		if(v==last)continue;
		dfs2(v,u);
		son[u]+=son[v];
		if(son[v]>son[hs[u]]){
			hs[u]=v;
		}
	}
	return;
}

inline void dfs3(int u,int last,int topf){
	id[u]=++cnt;
	num[cnt]=u;
//	cout<<cnt<<" "<<u<<" "<<last<<endl;
	fa[cnt]=id[last];
	dep[cnt]=dep[fa[cnt]]+1;
	if(!topf)topf=id[u];
	top[cnt]=id[u];
	if(hs[u])dfs3(hs[u],u,topf);
	for(int i=head[u];i;i=e[i].to){
		int v=e[i].v;
		if(root[v])v=n+1;
		if(v==last||v==hs[u])continue;
		dfs3(v,u,0);
	}
	return;
}

inline void dfs4(int u,int fa){
	ffa[u]=ffa[fa];
	for(int i=head[u];i;i=e[i].to){
		int v=e[i].v;
		if(root[v]){
			ffa[u]=v;
			v=n+1;
		}
		if(v==fa)continue;
		dfs4(v,u);
	}
}

inline void up(int node){
	st[node].sum=st[node<<1].sum+st[node<<1|1].sum;
	return;
}

inline void down(int node){
	if(!st[node].lazy)return;
//	cout<<node<<" "<<st[node].l<<" "<<st[node].r<<endl;
	st[node<<1].lazy^=1;
	st[node<<1|1].lazy^=1;
	st[node<<1].sum=st[node<<1].r-st[node<<1].l+1-st[node<<1].sum;
	st[node<<1|1].sum=st[node<<1|1].r-st[node<<1|1].l+1-st[node<<1|1].sum;
	st[node].lazy=0;
}

inline void build(int node,int l,int r){
	st[node].l=l;st[node].r=r;
	if(l==r){
		st[node].sum=0;
		return;
	}
	int mid=(l+r)>>1;
	build(node<<1,l,mid);
	build(node<<1|1,mid+1,r);
	up(node);
}

inline void change(int node,int l,int r){
	if(st[node].l>r||st[node].r<l)return;
	if(st[node].l>=l&&st[node].r<=r){
		st[node].lazy^=1;
		st[node].sum=st[node].r-st[node].l+1-st[node].sum;
//		cout<<node<<" "<<st[node].l<<" "<<st[node].r<<" "<<st[node].lazy<<" "<<st[node].sum<<endl;;
		return;
	}
	down(node);
	change(node<<1,l,r);
	change(node<<1|1,l,r);
	up(node);
}

inline int query(int node,int l,int r){
	if(st[node].l>r||st[node].r<l)return 0;
	if(st[node].l>=l&&st[node].r<=r)return st[node].sum;
	down(node);
	return query(node<<1,l,r)+query(node<<1|1,l,r);
}

inline void change_link(int x,int y){
	while(top[x]!=top[y]){
		if(dep[top[x]]<dep[top[y]])swap(x,y);
//		cout<<top[x]<<" "<<x<<endl;
		change(1,top[x],x);
		x=fa[top[x]];
	}
	if(dep[x]>dep[y])swap(x,y);
//	cout<<x<<" "<<y<<endl;
	change(1,x,y);
	return;
}

inline void up2(int node){
	st2[node].sum=st2[node<<1].sum+st2[node<<1|1].sum;
}

inline void down2(int node){
	if(!st2[node].lazy)return;
	st2[node<<1].lazy^=1;
	st2[node<<1|1].lazy^=1;
	st2[node<<1].sum=st2[node<<1].r-st2[node<<1].l+1-st2[node<<1].sum;
	st2[node<<1|1].sum=st2[node<<1|1].r-st2[node<<1|1].l+1-st2[node<<1|1].sum;
	st2[node].lazy=0;
}

inline void build2(int node,int l,int r){
	st2[node].l=l;st2[node].r=r;
	if(l==r){
		st2[node].sum=0;
		return;
	}
	int mid=(l+r)>>1;
	build2(node<<1,l,mid);
	build2(node<<1|1,mid+1,r);
	up2(node);
}

inline void change2(int node,int l,int r){
	if(st2[node].l>r||st2[node].r<l)return;
	if(st2[node].l>=l&&st2[node].r<=r){
		st2[node].lazy^=1;
		st2[node].sum=st2[node].r-st2[node].l+1-st2[node].sum;
//		cout<<st2[node].l<<" "<<st2[node].r<<" "<<st2[node].lazy<<" "<<st2[node].sum<<endl;
		return;
	}
	down2(node);
	change2(node<<1,l,r);
	change2(node<<1|1,l,r);
	up2(node);
}

inline int query2(int node,int l,int r){
	if(st2[node].l>r||st2[node].r<l)return 0;
	if(st2[node].l>=l&&st2[node].r<=r)return st2[node].sum;
	down2(node);
	return query2(node<<1,l,r)+query2(node<<1|1,l,r);
}

int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();m=read();
	size=1;
	for(int i=1;i<=n;i++){
		l=read();r=read();
		add(l,r);
		add(r,l);
	}
	dfs(1,0,1);
	for(int i=1;i<=stk_size;i++){
		a[i]=stk[i];
		root[a[i]]=true;
		id2[a[i]]=++cnt2;
		ffa[a[i]]=a[i];
		num2[cnt2]=a[i];
//		cout<<a[i]<<" ";
	}
	build2(1,1,stk_size);
	for(int i=1;i<=n;i++){
		if(root[i]){
			for(int j=head[i];j;j=e[j].to){
				int v=e[j].v;
				if(root[v])continue;
				add(n+1,v);
			}
		}
	}
	cnt=0;
	fa[n+1]=n+1;
	dfs2(n+1,n+1);
	dfs3(n+1,n+1,1);
	dfs4(n+1,n+1);
	build(1,1,cnt);
	while(m--){
		l=read();r=read();
		if(root[l])x=n+1;else x=l;
		if(root[r])y=n+1;else y=r;
		if(x!=y){
//			cout<<id[x]<<" "<<id[y]<<endl;
			change_link(id[x],id[y]);
		}
		x=id2[ffa[l]];
		y=id2[ffa[r]];
//		cout<<x<<" "<<y<<endl;
		if(x>y)swap(x,y);
		bool flag=false;
		if(y-x<stk_size-y+x){
//			cout<<x<<" "<<y-1<<endl;
//			change2(1,x,y-1);
		}else{
			if(y-x==stk_size-y+x){
				if(x==1){
					if(num2[stk_size]<num2[x+1])flag=true;
				}else{
					if(x==n){
						if(num2[x-1]<num2[1])flag=true;
					}else{
						if(num2[x-1]<num2[x+1])flag=true;
					}
				}
			}else{
				flag=true;
			}
//			cout<<1<<" "<<x-1<<endl;
//			cout<<y<<" "<<stk_size<<endl;
		}
		if(flag){
//			cout<<1<<" "<<x-1<<endl;
//			cout<<y<<" "<<stk_size<<endl;
			change2(1,1,x-1);
			change2(1,y,stk_size);
		}else{
//			cout<<x<<" "<<y-1<<endl;
			change2(1,x,y-1);
		}
//		writeln(query(1,2,cnt));
//		writeln(query2(1,1,stk_size));
		writeln(cnt-1-query(1,2,cnt)+max(stk_size-query2(1,1,stk_size),1));
	}
	return 0;
}
