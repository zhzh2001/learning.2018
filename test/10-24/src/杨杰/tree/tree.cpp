#include <cstdio>
#include <cstring>
using namespace std;
#define dd c=getchar()
inline int read() {int s=0,w=1;char c;while (dd,c>'9' || c<'0') if (c=='-') w=-1;while (c>='0' && c<='9') s=s*10+c-'0',dd;return s*w;}
#undef dd
inline void write(int x) {if (x<0) putchar('-'),x=-x;if (x>=10) write(x/10);putchar(x%10|'0');}
inline void wln(int x) {write(x);puts("");}
const int N=1e5+7;
struct edge {
	int t,nxt;
}e[N<<1];
int n,Q,cnt,tp,T;
int head[N],in[N],vis[N],st[N],huan[N],siz[N],Bis[N],top[N],dep[N],dfn[N],id[N],inh[N],F[N],bF[N];
void add(int u,int t) {
	e[++cnt].t=t;e[cnt].nxt=head[u];head[u]=cnt;
}
void dfs(int u,int fa) {
	st[++tp]=u;in[u]=1;vis[u]=1;
	for (int i=head[u];i;i=e[i].nxt) if (e[i].t!=fa){
		int t=e[i].t;
		if (vis[t]) {
			if (in[t]) for (int j=tp;j;j--) {
				huan[++*huan]=st[j];inh[st[j]]=*huan;
				if (st[j]==t) break;
			}
		}
		else dfs(t,u);
	}
	tp--;in[u]=0;
}
void dfs1(int u,int fa) {
	if (inh[u]) bF[u]=u;else bF[u]=bF[fa];
	siz[u]=1;dep[u]=dep[fa]+1;F[u]=fa;
	for (int i=head[u];i;i=e[i].nxt) {
		int t=e[i].t;
		if (t==fa || vis[t]) continue;
		dfs(t,u);
		siz[u]+=siz[t];
		if (siz[t]>siz[Bis[u]]) Bis[u]=t;
	}
}
void dfs2(int u,int tp) {
	vis[u]=1;
	dfn[u]=++T;id[T]=u;
	if (Bis[u]) dfs2(Bis[u],tp);
	for (int i=head[u];i;i=e[i].nxt) {
		int t=e[i].t;
		if (vis[t] || t==Bis[u]) continue;
		dfs2(t,t);
	}
}
int Lca(int a,int b) {
	for (;top[a]!=top[b];dep[top[a]]>dep[top[b]]?a=F[top[a]]:b=F[top[b]]);
	return dep[a]>dep[b]?b:a;
}
#define ls (ts<<1)
#define rs (ts<<1|1)
struct tree1 {
	int s[N<<2],flg[N<<2];
	void pushup(int ts) {s[ts]=s[ls]+s[rs];}
	void build(int ts,int l,int r) {
		if (l==r) {
			s[ts]=1;
			return;
		}
		int mid=(l+r)>>1;
		build(ls,l,mid);build(rs,mid+1,r);
		pushup(ts);
	}
	void pushdown(int ts,int ln,int rn) {
		if (!flg[ts]) return;flg[ts]=0;
		s[ls]=ln-s[ls];s[rs]=rn-s[rs];
		flg[ls]^=1;flg[rs]^=1;
	}
	void update(int ts,int l,int r,int L,int R) {
		if (l==L && r==R) {
			s[ts]=r-l+1-s[ts];
			flg[ts]^=1;
			return;
		}
		int mid=(l+r)>>1;
		pushdown(ts,mid-l+1,r-mid);
		if (R<=mid) update(ls,l,mid,L,R);
		else if (L>mid) update(rs,mid+1,r,L,R);
		else update(ls,l,mid,L,mid),update(rs,mid+1,r,mid+1,R);
		pushup(ts);
	}
	void change(int x,int tp) {
		while (dep[top[x]]>=dep[tp]) update(1,1,n,dfn[top[x]]+1,dfn[x]),x=F[top[x]];
		update(1,1,n,dfn[tp]+1,dfn[x]);
	}
}T1;
struct tree2 {
	int s[N<<2],flg[N<<2],zg[N],lc[N],rc[N];
	void pushup(int ts) {
		lc[ts]=lc[ls];rc[ts]=rc[rs];
		s[ts]=s[ls]+s[rs]-(rc[ls]==lc[rs]);
	}
	void build(int ts,int l,int r) {
		if (l==r) {
			s[ts]=lc[ts]=rc[ts]=1;
			return;
		}
		int mid=(l+r)>>1;
		build(ls,l,mid);build(rs,mid+1,r);
		pushup(ts);
	}
	void change(int u,int v) {
		
	}
}T2;
int main() {
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();Q=read();
	for (int i=1,u,v;i<=n;i++) u=read(),v=read(),add(u,v),add(v,u);
	dfs(1,0);
	memset(vis,0,sizeof vis);
	dfs1(huan[1],0);
	memset(vis,0,sizeof vis);
	dfs2(huan[1],huan[1]);
	T1.build(1,1,n);T2.build(1,1,*huan);
	for (int i=1,u,v;i<=Q;i++) {
		u=read(),v=read();
		if (inh[u] && inh[v]) T2.change(u,v);
		else {
			int lca=Lca(u,v);
			if (!inh[lca]) T1.change(u,lca),T1.change(v,lca);
			else {
				T1.change(u,bF[u]);T1.change(v,bF[v]);
				T2.change(bF[u],bF[v]);
			}
		}
		wln(n-T1.s[1]+T2.s[1]);
	}
}
