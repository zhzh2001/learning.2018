#include <cstdio>
#include <algorithm>
using namespace std;
const int M=1e5+7;
int n,m,ans=1e9;
char s[19][M];
int a[M],v[M*3];
inline int find(int x) {
	int s=0;while (x) x-=x&-x,s++;
	return min(s,n-s);
}
int main() {
	freopen("array.in","r",stdin);
	freopen("array.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=0;i<n;i++) scanf("%s",s[i]);
	for (int i=0;i<m;i++)
		for (int j=0;j<n;j++) {
			if (s[j][i]=='1') a[i]|=1<<j;
		}
	for (int i=0;i<(1<<n);i++) v[i]=find(i);
	for (int i=0;i<(1<<n);i++) {
		int now=0;
		for (int j=0;j<m;j++) {
			now+=v[a[j]^i];
		}
		ans=min(ans,now);
	}
	printf("%d\n",ans);
}
