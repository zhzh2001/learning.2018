#include <cstdio>
#include <algorithm>
using namespace std;
const int M=1e5+7;
int n,m,ans=1e9;
char s[19][M];
int v[M<<2];
struct tree{
	int s,ans;
}tr[M<<2];
#define ls ts<<1
#define rs ts<<1|1
void pushup(int ts) {tr[ts].s=tr[ls].s|tr[rs].s;tr[ts].ans=tr[ls].ans+tr[rs].ans;}
void build(int ts,int l,int r) {
	if (l==r) {
		for (int i=0;i<n;i++) if (s[i][l]=='1') tr[ts].s|=(1<<i);
		tr[ts].ans=v[tr[ts].s];
		return;
	}
	int mid=(l+r)>>1;
	build(ls,l,mid);build(rs,mid+1,r);
	pushup(ts);
}
void update(int ts,int l,int r,int S) {
	if ((tr[ts].s^S)==tr[ts].s) return;
	if (l==r) {
		tr[ts].s^=S;tr[ts].ans=v[tr[ts].s];
		return;
	}
	int mid=(l+r)>>1;
	update(ls,l,mid,S);update(rs,mid+1,r,S);
	pushup(ts);
}
inline int find(int x) {
	int s=0;while (x) x-=x&-x,s++;
	return min(s,n-s);
}
int main() {
	freopen("array.in","r",stdin);
//	freopen("array.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=0;i<n;i++) scanf("%s",s[i]+1);
	for (int i=0;i<(1<<n);i++) v[i]=find(i);
	build(1,1,m);
	for (int i=0;i<(1<<n);i++) {
		if (i!=0) {
			update(1,1,m,i^(i-1));
		}
		ans=min(ans,tr[1].ans);
	}
	printf("%d\n",ans);
}
