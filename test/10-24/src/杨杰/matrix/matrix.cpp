#include <cstdio>
#include <algorithm>
using namespace std;
#define dd c=getchar()
inline int read() {int s=0,w=1;char c;while (dd,c>'9' || c<'0') if (c=='-') w=-1;while (c>='0' && c<='9') s=s*10+c-'0',dd;return s*w;}
#undef dd
inline void write(int x) {if (x<0) putchar('-'),x=-x;if (x>=10) write(x/10);putchar(x%10|'0');}
inline void wln(int x) {write(x);puts("");}
inline void wsp(int x) {write(x);putchar(' ');}
const int N=2505;
int n;
int a[N][N];
int main() {
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	n=read();
	for (int i=1;i<=n;i++) for (int j=1;j<=n;j++) a[i][j]=read();
	for (int i=1;i<=n;i++) if (a[i][i]) {
		puts("1");printf("%d 0 0\n",i);
		return 0;
	}
	for (int i=1;i<=n;i++) for (int j=i+1;j<=n;j++) if (a[i][j]!=a[j][i]) {
		puts("2");printf("%d %d 0\n",i,j);
		return 0;
	}
	for (int i=1;i<=n;i++) for (int j=i+1;j<=n;j++) for (int k=1;k<=n;k++) if (a[i][j]>max(a[i][k],a[j][k])) {
		puts("3");printf("%d %d %d\n",i,j,k);
		return 0;
	}
	puts("0");
}
