#include<bits/stdc++.h>
using namespace std;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}
const int N=2505;
int n,a[N][N];
bool vis[N];
int main(){
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++)a[i][j]=read();
		if(a[i][i]!=0){cout<<1<<'\n'<<i<<" 0 0";return 0;}
	}
	for(int i=2;i<=n;i++)
		for(int j=1;j<i;j++)
			if(a[i][j]!=a[j][i])
				{cout<<2<<'\n'<<i<<' '<<j<<" 0";return 0;}
	if(n<1400){
		for(int i=1;i<=n-2;i++)
			for(int j=i+1;j<=n-1;j++)
				for(int k=j+1;k<=n;k++){
					if(a[i][j]>max(a[i][k],a[k][j]))
						{cout<<3<<'\n'<<i<<' '<<j<<' '<<k;return 0;}
					if(a[i][k]>max(a[i][j],a[j][k]))
						{cout<<3<<'\n'<<i<<' '<<k<<' '<<j;return 0;}
					if(a[k][j]>max(a[k][i],a[i][j]))
						{cout<<3<<'\n'<<k<<' '<<j<<' '<<i;return 0;}
			}
	}else{
		srand(time(NULL));
		int i=0,j=0,k=0;
		for(int o=1;o<=7500000;o++){
			i=(i+rand())%n+1;
			j=(j+rand()%n+i)%n+1;
			k=(k+rand()%n+j)%n+1;
			if(i==j||j==k||i==k){o--;continue;}
			if(a[i][j]>max(a[i][k],a[k][j]))
				{cout<<3<<'\n'<<i<<' '<<j<<' '<<k;return 0;}
			if(a[i][k]>max(a[i][j],a[j][k]))
				{cout<<3<<'\n'<<i<<' '<<k<<' '<<j;return 0;}
			if(a[k][j]>max(a[k][i],a[i][j]))
				{cout<<3<<'\n'<<k<<' '<<j<<' '<<i;return 0;}
		}
	}
	cout<<0;
	return 0;
}
