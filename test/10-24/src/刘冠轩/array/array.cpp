#include<bits/stdc++.h>
using namespace std;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}
const int N=100005;
int n,m,s[N],ans=0x7fffffff;
char a[20][N],ch;
void calc(){
	int sum=0;
	for(int i=1;i<=m;i++)sum+=min(s[i],n-s[i]);
	ans=min(ans,sum);
}
void dfs(int x){
	if(x>n){calc();return;}
	dfs(x+1);
	for(int i=1;i<=m;i++)
		if(a[x][i]=='1')s[i]--;else s[i]++;
	dfs(x+1);
	for(int i=1;i<=m;i++)
		if(a[x][i]=='1')s[i]++;else s[i]--;
}
int main(){
	freopen("array.in","r",stdin);
	freopen("array.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=n;i++){
		ch='2';
		for(;ch<'0'||ch>'1';ch=gc());
		for(int j=1;j<=m;j++,ch=gc())s[j]+=((a[i][j]=ch)=='1');
	}
	dfs(1);
	cout<<ans;
	return 0;
}
