#include<bits/stdc++.h>
using namespace std;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}
const int N=100005;
int n,Q,x,y,head[N],cnt;
struct edge{int nt,to;}e[N];
void add(int u,int v){
	e[++cnt]=(edge){v,head[u]};
	head[u]=cnt;
}

int main(){
	n=read();Q=read();
	for(int i=1;i<=n;i++){
		x=read();y=read();
		add(x,y);add(y,x);
	}
	while(Q--){
		x=read();y=read();
		dfs(x);
	}
	return 0;
}
