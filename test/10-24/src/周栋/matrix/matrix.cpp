#include <bits/stdc++.h>
using namespace std;
typedef int ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=2510,INF=0x3f3f3f3f;
ll n,a[N][N],dist[N],fa[N];
bool vis[N];
inline bool dijstra(ll x){
	memset(dist,0x3f,sizeof dist);
	memset(vis,0,sizeof vis);
	dist[x]=0;
	do{
		ll k=-1;
		for (ll i=1;i<=n;++i) if (!vis[i]&&(k==-1||dist[k]>dist[i])) k=i;
		if (k==-1) break;
		vis[k]=1;
		for (ll i=1;i<=n;++i) if (!vis[i]){
			if (dist[i]<a[k][i]&&fa[i]==fa[k]){
				puts("3");
				wr(i),pc(' '),wr(k),pc(' '),wr(fa[i]);
				return 0;
			}
			if (dist[i]>a[k][i]) dist[i]=a[k][i],fa[i]=k;
		}
	}while (1);
	return 1;
}
inline bool check(){
	ll T=clock();
	do{
		if (!dijstra(rand()%n+1)) return 0;
	}while (clock()-T<=1700);
	return 1;
}
int main(){
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	srand((ll)time(NULL));
//	ll T=clock();
	read(n);
	for (ll i=1;i<=n;++i)
		for (ll j=1;j<=n;++j)
			read(a[i][j]);
	for (ll i=1;i<=n;++i) if (a[i][i]) {puts("1"),wr(i),puts(" 0 0");return 0;}
	for (ll i=1;i<=n;++i)
		for (ll j=1;j<=n;++j)
			if (a[i][j]!=a[j][i]) {puts("2"),wr(i),pc(' '),wr(j),puts(" 0");return 0;}
	if (n<=300){
		for (ll i=1;i<=n;++i)
			for (ll j=1;j<=n;++j){
				for (ll k=1;k<=n;++k)
					if (a[i][j]>max(a[i][k],a[k][j])){
						puts("3");
						wr(i),pc(' '),wr(j),pc(' ');
						wr(k);
						return 0;
					}
			}
		puts("0");
		return 0;
	}
//	do{
//		ll i=rand()%n+1,j=rand()%n+1;
//		for (ll k=1;k<=n;++k)
//			if (a[i][j]>max(a[i][k],a[k][j])){
//				puts("3");
//				wr(i),pc(' '),wr(j),pc(' ');
//				wr(k);
//				return 0;
//			}
//	}while(clock()-T<=1900);
	if (check()) puts("0");
	return 0;
}
//sxdakking
/*
4
0	3	1	3
3	0	2	3
1	2	0	3
3	3	3	0
*/
