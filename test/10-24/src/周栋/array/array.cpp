#include <iostream>
#include <cstdio>
using namespace std;
typedef int ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=1e5+10;
ll n,m,a[20][N],tmp[1<<19],val[1<<19],ans,Min;
char s[20][N];
int main(){
	freopen("array.in","r",stdin);
	freopen("array.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (ll i=1;i<=n;++i) scanf("%s",s[i]+1);
	for (ll i=1;i<=n;++i) for (ll j=1;j<=m;++j) a[i][j]=s[i][j]-48;
	Min=n*m;
	for (ll k=0;k<(1<<n);++k){
		ans=0;
		for (ll j=1;j<=m;++j){
			tmp[1]=tmp[0]=0;
			for (ll i=1;i<=n;++i) if (k&(1<<(i-1))) ++tmp[a[i][j]];else ++tmp[!a[i][j]];
			ans+=min(tmp[1],tmp[0]);
		}
		Min=min(Min,ans);
	}
/*	for (ll i=1;i<=m;++i){
		ll tem=0;
		for (ll j=1;j<=n;++j) tem|=a[j][i]*(1<<(j-1));
		++tmp[tem];
	}
	for (ll k=0;k<(1<<n);++k){
		ll tem=0;
		for (ll i=1;i<=n;++i) if (k&(1<<(i-1))) ++tem;
		val[k]=min(n-tem,tem);
		ans+=val[k]*tmp[k];
	}
	Min=ans;
	for (ll k=0;k<(1<<n);++k){
		ll tem=0;
		for (ll i=0;i<(1<<n);++i) tem+=val[i]*tmp[i^k];
		Min=min(Min,tem);
	}
	*/
	wr(Min);
	return 0;
}
//sxdakking
