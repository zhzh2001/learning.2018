#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<string>
#include<iostream>
#include<algorithm>
#include<cmath>
#include<cctype>
#include<ctime>
#include<queue>
#include<vector>
#include<set>
#include<map>
using namespace std;

namespace TYC
{
	namespace Read
	{
		const int L=1000000;
		char LZH[L];
		char *SSS,*TTT;
		inline char gc(){
			if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
			return *SSS++;
		}
		inline int read(){
			int x=0;
			char c=gc();
			for (;c<'0'||c>'9';c=gc());
			for (;c>='0'&&c<='9';c=gc())
				x=(x<<1)+(x<<3)+c-48;
			return x;
		}
	}
	
	using Read::read;
	
	const int N=100010;
	int n,cnt=1,ques,L,R,tot,Head[N],vis[N],ffa[N],mark[N],Loop[N];
	
	inline void write(int x)
	{
		if(x<0) putchar('-'),x=-x;
		int len=0;
		static int bask[15];
		if(!x) putchar('0');
		else
		{
			while(x) bask[++len]=x%10,x/=10;
			for(int i=len;i;i--) putchar('0'+bask[i]);
		}
	}
	
	struct edge
	{
		int to,next,tag;
	}E[N<<1];
	
	inline void add(int u,int v)
	{
		E[++cnt]=(edge){v,Head[u]};
		Head[u]=cnt;
	}
	
	bool find_loop(int u,int f)
	{
		if(vis[u])
		{
			R=f;
			for(int i=f;i!=u;i=ffa[i])
				mark[i]=1,Loop[++tot]=i;
			mark[u]=1;
			Loop[++tot]=u;
			return true;
		}
		vis[u]=1;
		ffa[u]=f;
		for(int i=Head[u];i;i=E[i].next)
		{
			int v=E[i].to;
			if(v==f) {f=0;continue;}
			if(find_loop(v,u)) return true;
		}
		vis[u]=0;
		return false;
	}
	
	namespace Big
	{
		int id[N];
		
		void work()
		{
			for(int i=1;i<=tot;i++)
				id[Loop[i]]=i;
			while(ques--)
			{
				int u=read(),v=read(),ans;
				if(id[u]>id[v]) swap(u,v);
				int a=id[v]-id[u],b=tot-id[v]+id[v];
				ans=n-min(a,b);
				write(ans);
				puts("");
			}
		}
	}
	
	namespace Small
	{
		typedef pair<int,int> pa;
		const int inf=0x3f3f3f3f;
		int dis[N],vis[N],deg[N],f[N],pre[N];
		vector<int> G[N];
		
		void Dijkstra(int s)
		{
			priority_queue<pa,vector<pa>,greater<pa> > q;
			memset(dis,inf,sizeof(dis));
			memset(vis,0,sizeof(vis));
			dis[s]=0;
			q.push(pa(0,s));
			while(!q.empty())
			{
				int u=q.top().second;q.pop();
				if(vis[u]) continue;
				vis[u]=1;
				for(int i=Head[u];i;i=E[i].next)
				{
					int v=E[i].to;
					if(dis[v]>dis[u]+1)
					{
						dis[v]=dis[u]+1;
						if(!vis[v]) q.push(pa(dis[v],v));
					}
				}
			}
		}
		
		void rebuild()
		{
			memset(deg,0,sizeof(deg));
			for(int i=1;i<=n;i++) G[i].clear();
			for(int u=1;u<=n;u++)
				for(int i=Head[u];i;i=E[i].next)
				{
					int v=E[i].to;
					if(dis[v]==dis[u]+1)
						E[i].tag=1,deg[v]++;
					else E[i].tag=0;
				}
		}

		void topsort()
		{
			memset(f,inf,sizeof(f));
			memset(pre,0,sizeof(pre));
			queue<int> q;
			for(int i=1;i<=n;i++)
				if(!deg[i]) f[i]=0,pre[i]=0,q.push(i);
			while(!q.empty())
			{
				int u=q.front();q.pop();
				for(int i=Head[u];i;i=E[i].next)
					if(E[i].tag)
					{
						int v=E[i].to;
						if(f[v]>f[u]+1)
							f[v]=f[u]+1,pre[v]=i;
						else if(f[v]==f[u]+1&&E[pre[v]^1].to>u)
							pre[v]=i;
						deg[v]--;
						if(!deg[v]) q.push(v);
					}
			}
		}
		
		void work()
		{
			while(ques--)
			{
				int u=read(),v=read();
				Dijkstra(u);
				rebuild();
				topsort();
				int ans=n-f[v];
				write(ans);
				puts("");
			}
		}
	}

	void work()
	{
		n=read(),ques=read();
		for(int i=1;i<=n;i++)
		{
			int u=read(),v=read();
			add(u,v),add(v,u);
		}
		find_loop(1,0);
		if(n>=1000) Big::work();
		else Small::work();
	}
}

int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	TYC::work();
	return 0;
}
