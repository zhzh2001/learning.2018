#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<string>
#include<iostream>
#include<algorithm>
#include<cmath>
#include<cctype>
#include<ctime>
#include<queue>
#include<vector>
#include<set>
#include<map>
using namespace std;

namespace TYC
{
	namespace Read
	{
		const int L=1000000;
		char LZH[L];
		char *SSS,*TTT;
		inline char gc(){
			if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
			return *SSS++;
		}
		inline int read(){
			int x=0;
			char c=gc();
			for (;c<'0'||c>'9';c=gc());
			for (;c>='0'&&c<='9';c=gc())
				x=(x<<1)+(x<<3)+c-48;
			return x;
		}
	}
	
	using Read::read;
	const int N=2510;
	int n,arr[N][N];
	
	inline void write(int x)
	{
		if(x<0) putchar('-'),x=-x;
		int len=0;
		static int bask[15];
		if(!x) putchar('0');
		else
		{
			while(x) bask[++len]=x%10,x/=10;
			for(int i=len;i;i--) putchar('0'+bask[i]);
		}
		putchar(' ');
	}
	
	bool solve1()
	{
		int flag=0;
		for(int i=1;i<=n;i++)
			if(arr[i][i]!=0)
			{
				if(!flag)
				{
					write(1),puts("");
					flag=1;
				}
				write(i),write(0),write(0),puts("");
			}
		return flag;
	}

	bool solve2()
	{
		int flag=0;
		for(int i=1;i<=n;i++)
			for(int j=i+1;j<=n;j++)
				if(arr[i][j]!=arr[j][i])
				{
					if(!flag)
					{
						write(2),puts("");
						flag=1;
					}
					write(i),write(j),write(0),puts("");
				}
		return flag;
	}
	
	bool solve3()
	{
		int flag=0;
		for(int i=1;i<=n;i++)
			for(int j=i+1;j<=n;j++)
				for(int k=1;k<=n;k++)
					if(arr[i][j]>max(arr[i][k],arr[k][j]))
					{
						if(!flag)
						{
							write(3),puts("");
							flag=1;
						}
						write(i),write(j),write(k),puts("");
					}
		return flag;
	}
	
	void work()
	{
		n=read();
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				arr[i][j]=read();
		if(!solve1()&&!solve2()&&!solve3()) puts("0");
	}
}

int main()
{
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	TYC::work();
	return 0;
}
