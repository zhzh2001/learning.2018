#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<string>
#include<iostream>
#include<algorithm>
#include<cmath>
#include<cctype>
#include<ctime>
#include<queue>
#include<vector>
#include<set>
#include<map>
using namespace std;

namespace TYC
{
	namespace Read
	{
		const int L=1000000;
		char LZH[L];
		char *SSS,*TTT;
		inline char gc(){
			if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
			return *SSS++;
		}
		inline int read(){
			int x=0;
			char c=gc();
			for (;c<'0'||c>'9';c=gc());
			for (;c>='0'&&c<='9';c=gc())
				x=(x<<1)+(x<<3)+c-48;
			return x;
		}
	}

	using Read::read;
	using Read::gc;
	const int N=21,M=100010,B=320;
	const int inf=0x3f3f3f3f;
	int n,m,ans=inf,block,Block_num,bin[M],cnt[M],belong[M],tot[M],arr[N][M];

	inline int count()
	{
		int sum=0;
		for(int i=1;i<=Block_num;i++) sum+=tot[i];
		return sum;
 	}


	void dfs(int now)
	{
  		ans=min(ans,count());

		for(int i=now;i<=n;i++)
		{
			for(int j=1;j<=m;j++)
			{
				int be=belong[j];
				tot[be]-=min(cnt[j],n-cnt[j]);
				if(arr[i][j]) cnt[j]--;
				else cnt[j]++;
				tot[be]+=min(cnt[j],n-cnt[j]);
			}

			dfs(i+1);

			for(int j=1;j<=m;j++)
			{
				int be=belong[j];
				tot[be]-=min(cnt[j],n-cnt[j]);
				if(arr[i][j]) cnt[j]++;
				else cnt[j]--;
				tot[be]+=min(cnt[j],n-cnt[j]);
			}

		}
	}

	void work()
	{
		n=read(),m=read();
		char ch;
		for(int i=1;i<=n;i++)
		{
			do ch=gc(); while(!isdigit(ch));
			for(int j=1;j<=m;j++)
				arr[i][j]=(ch-'0'),ch=gc();
		}

		for(int i=1;i<=n;i++)
			for(int j=1;j<=m;j++)
				cnt[j]+=arr[i][j];

		block=sqrt(m);
		for(int i=1;i<=m;i++)
		{
			belong[i]=(i-1)/block+1;
			tot[belong[i]]+=min(cnt[i],n-cnt[i]);
		}
		Block_num=belong[m];

		dfs(1);

		printf("%d\n",ans);
	}
}

int main()
{
	freopen("array.in","r",stdin);
	freopen("array.out","w",stdout);
	TYC::work();
	return 0;
}
