#include<bits/stdc++.h>
using namespace std;
const int N=20,M=100005;
char c[N][M];
int n,m,ans,f[M];
inline void dfs(int x)
{
	if (x==n+1)
	{
		int sum=0;
		for (int i=1;i<=m;++i)
			sum+=min(f[i],n-f[i]);
		ans=min(ans,sum);
		return;
	}
	dfs(x+1);
	for (int i=1;i<=m;++i)
		f[i]+=(c[x][i]=='0'?1:-1);
	dfs(x+1);
	for (int i=1;i<=m;++i)
		f[i]+=(c[x][i]=='0'?-1:1);
}
int main()
{
	freopen("array.in","r",stdin);
	freopen("array.out","w",stdout);
	scanf("%d%d",&n,&m);
	ans=n*m;
	for (int i=1;i<=n;++i)
		scanf("%s",c[i]+1);
	for (int i=1;i<=n;++i)
		for (int j=1;j<=m;++j)
			f[j]+=(c[i][j]-'0');
	dfs(2);
	cout<<ans;
	return 0;
}