#include<bits/stdc++.h>
using namespace std;
#define pi pair<int,int>
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
const int N=100005;
int la[N],to[N*2],pr[N*2];
int q[N],pos[N];
int t[N*4],f[N*4];
int cnt,n,x,y,Q;
int dis[N];
int ans;
inline void add(int x,int y)
{
	to[++cnt]=y;
	pr[cnt]=la[x];
	la[x]=cnt;
}
inline void bl()
{
	while (Q--)
	{
		read(x);
		read(y);
		for (int i=1;i<=n;++i)
			dis[i]=0;
		dis[y]=1;
		int l=0,r=1;
		q[1]=y;
		while (l<r)
		{
			int now=q[++l];
			for (int i=la[now];i;i=pr[i])
				if (!dis[to[i]])
				{
					dis[to[i]]=dis[now]+1;
					q[++r]=to[i];
				}
		}
		int p=x;
		for (int i=1;i<dis[x];++i)
		{
			int pos=n+2,edg=0;
			for (int j=la[p];j;j=pr[j])
				if (dis[to[j]]==dis[p]-1&&to[j]<pos)
				{
					pos=to[j];
					edg=j;
				}
			f[edg]^=1;
			f[edg^1]^=1;
			p=pos;
		}
		for (int i=1;i<=n;++i)
			dis[i]=0;
		ans=0;
		for (int i=1;i<=n;++i)
			if (!dis[i])
			{
				++ans;
				l=0;
				r=1;
				q[1]=i;
				dis[i]=1;
				while (l<r)
				{
					int now=q[++l];
					for (int i=la[now];i;i=pr[i])
						if (!dis[to[i]]&&f[i])
						{
							dis[to[i]]=1;
							q[++r]=to[i];
						}
				}
			}
		printf("%d\n",ans);
	}
}
int top;
inline void dfs(int x)
{
	dis[x]=1;
	q[++top]=x;
	pos[x]=top;
	for (int i=la[x];i;i=pr[i])
		if (!dis[to[i]])
			dfs(to[i]);
}
inline void build(int l,int r,int x)
{
	t[x]=r-l+1;
	if (l==r)
		return;
	int mid=(l+r)/2;
	build(l,mid,x*2);
	build(mid+1,r,x*2+1);
}
inline void modify(int l,int r,int L,int R,int x)
{
	if (l==L&&r==R)
	{
		f[x]^=1;
		if (l!=r)
		{
			t[x]=t[x*2]+t[x*2+1];
			if (f[x])
				t[x]=(r-l+1)-t[x];
		}
		else
			t[x]=1-f[x];
		return;
	}
	int mid=(l+r)/2;
	if (R<=mid)
		modify(l,mid,L,R,x*2);
	if (L>mid)
		modify(mid+1,r,L,R,x*2+1);
	if (L<=mid&&R>mid)
	{
		modify(l,mid,L,mid,x*2);
		modify(mid+1,r,mid+1,R,x*2+1);
	}
	t[x]=t[x*2]+t[x*2+1];
	if (f[x])
		t[x]=(r-l+1)-t[x];
}
inline void work_cir()
{
	dfs(1);
	build(1,n,1);
	q[0]=q[n];
	q[n+1]=1;
	while (Q--)
	{
		read(x);
		read(y);
		x=pos[x];
		y=pos[y];
		if (x<y)
		{
			int len=y-x;
			if (len<=n/2||(len==n/2&&q[x+1]<q[x-1]))
				modify(1,n,x,y-1,1);
			else
			{
				if (x!=1)
					modify(1,n,1,x-1,1);
				modify(1,n,y,n,1);
			}
		}
		else
		{
			int len=x-y;
			if (len<=n/2||(len==n/2&&q[x-1]<q[x+1]))
				modify(1,n,y,x-1,1);
			else
			{
				if (y!=1)
					modify(1,n,1,y-1,1);
				modify(1,n,x,n,1);
			}
		}
		cout<<max(1,t[1])<<'\n';
	}
}
set<pi> s;
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	read(n);
	read(Q);
	cnt=1;
	for (int i=1;i<=n;++i)
	{
		read(x);
		read(y);
		if (x>y)
			swap(x,y);
		if (s.count((pi){x,y}))
		{
			add(x,n+1);
			add(n+1,x);
			add(y,n+1);
			add(n+1,y);
		}
		else
		{
			s.insert((pi){x,y});
			add(x,y);
			add(y,x);
		}
	}
	if (cnt>n*2+1)
		++n;
	if (n<=1001)
		bl();
	else
		work_cir();
	return 0;
}