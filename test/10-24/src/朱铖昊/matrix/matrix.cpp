#include<bits/stdc++.h>
using namespace std;
#define pi pair<int,int> 
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}
const int N=2505;
bitset<N> b[N];
int a[N][N];
int n,m;
pi f[N*N];
inline void check1()
{
	for (int i=1;i<=n;++i)
		if (a[i][i]!=0)
		{
			printf("1\n%d 0 0",i);
			exit(0);
		}
}
inline void check2()
{
	for (int i=1;i<=n;++i)
		for (int j=1;j<i;++j)
			if (a[i][j]!=a[j][i])
			{
				printf("2\n%d %d 0",i,j);
				exit(0);
			}
}
bool cmp(pi x,pi y)
{
	return a[x.first][x.second]>a[y.first][y.second];
}
inline void check3()
{
	for (int i=2;i<=n;++i)
		for (int j=1;j<i;++j)
			f[++m]=(pi){i,j};
	sort(f+1,f+1+m,cmp);
	// cout<<clock()<<'\n';
	f[m+1]=(pi){0,0};
	int l=1;
	for (int i=1;i<=m+1;++i)
	{
		if (a[f[i].first][f[i].second]!=a[f[i-1].first][f[i-1].second])
		{
			// cout<<i<<'\n';
			for (int j=l;j<i;++j)
				if ((b[f[j].first]|b[f[j].second]).count()!=n)
				{
					for (int k=1;k<=n;++k)
						if (!(b[f[j].first][k]|b[f[j].second][k]))
						{
							printf("3\n%d %d %d",f[j].first,f[j].second,k);
							// cout<<' '<<a[f[j].first][f[j].second]<<' '<<a[f[j].first][k]<<' '<<a[k][f[j].second];
							exit(0);
							// break;
						}
				}
				else
				{	
					if ((long long)n*j>(long long)1e8)
					{
						// cout<<"! "<<j<<'\n';
						return;
					}
				}
			l=i;
		}
		b[f[i].first][f[i].second]=b[f[i].second][f[i].first]=1;
	}
	puts("?");
}
int main()
{
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	n=read();
	for (int i=1;i<=n;++i)
		for (int j=1;j<=n;++j)
			a[i][j]=read();
	check1();
	check2();
	check3();
	puts("0");
	// cout<<clock();
	return 0;
}