#include<bits/stdc++.h>
#define rep(i,l,r) for(int i=l;i<=r;i++)
#define dep(i,r,l) for(int i=r;i>=l;i--)
#define fy 20021218020021218
#define N 5000
using namespace std;
int n,m,i,j,k,l,r,h;
int a[N][N];
int read()
{
	int x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
int main()
{
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	n=read();
	rep(i,1,n) 
	  rep(j,1,n) a[i][j]=read();
	rep(i,1,n)
	  if(a[i][i])
	  {
	    printf("1\n");
	  	printf("%d 0 0",i);
	  	return 0;
	  }
	rep(i,1,n)
	  rep(j,1,n)
	    if(a[i][j]!=a[j][i])
	    {
	    	printf("2\n");
	    	printf("%d %d 0",i,j);
	    	return 0;
	    }
	if(n<=500)
	{
		rep(i,1,n)
		  rep(j,1,n)
		    rep(k,1,n)
		    if(a[i][j]>max(a[i][k],a[k][j]))
	    	  {
	    	  	printf("3\n");
			  	printf("%lld %lld %lld",i,j,k);
			  	return 0;
			  }
		printf("0");
		return 0;
	}
	rep(i,1,n)
	{
		h=0; j=1;
		rep(k,1,n) 
		  if(a[i][k]>=h) j=k,h=a[i][k];
		rep(k,1,n)
		  if(a[i][j]>max(a[i][k],a[k][j]))
		  {
		  	printf("3\n");
		  	printf("%lld %lld %lld",i,j,k);
		  	return 0;
		  }
	}
	rep(j,1,n)
	{
		h=0; i=1;
		rep(k,1,n)
		  if(a[k][j]>=h) i=k,h=a[k][j];
		rep(k,1,n)
		  if(a[i][j]>max(a[i][k],a[k][j]))
		  {
		  	printf("3\n");
		  	printf("%lld %lld %lld",i,j,k);
		  	return 0;
		  }
	}
	printf("0");
	return 0;
}
