#include<cstdio>
#include<cstring>
#include<string>
#include<algorithm>
#include<iostream>
#define re register
#define rep(x,a,b) for (register int x=(int)a;x<=b;++x)
#define drp(x,a,b) for (register int x=(int)a;x>=b;--x)
#define LL long long
#define inf 20021218020021218
using namespace std;
inline LL read()
{
    LL x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
int n,m,ans,b[20];
char a[20][100010];
int Min(int l,int r){return l<r?l:r;}
void check()
{
   int sum=0;
   rep(i,0,m-1)
   {
	  int suma=0,sumb=0;
	  rep(j,1,n)
		if (b[j]==1)
		  {
		   if (a[j][i]=='0') suma++;
		   if (a[j][i]=='1') sumb++;
          }
          else
		  {
			if (a[j][i]=='0') sumb++;
			if (a[j][i]=='1') suma++;
		  }
	  sum=sum+Min(suma,sumb);
   }
   if (sum<ans) ans=sum;
   return;
}
void dfs(int k)
{
   if (k==n+1){check();return;}
   b[k]=0; dfs(k+1);
   b[k]=1; dfs(k+1);
}
int main()
{
   freopen("array.in","r",stdin);
   freopen("array.out","w",stdout);
   n=read(),m=read();
   rep(i,1,n) scanf("%s",a[i]);
   ans=inf;dfs(1);
   printf("%d\n",ans);
   return 0;
}
