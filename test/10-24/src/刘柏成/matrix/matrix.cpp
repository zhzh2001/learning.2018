#include<bits/stdc++.h>
using namespace std;
#define file "matrix"
typedef long long ll;
const int INF=0x3f3f3f3f;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}
int a[2502][2502],v[2502],fa[2502];
bool vis[2502];
int m;
struct edge{
	int to,cost,next;
}e[5002];
int first[2502];
void addedge(int from,int to,int cost){
	e[++m]=(edge){to,cost,first[from]};
	first[from]=m;
}
int dep[2502];
int rt;
void dfs(int u,int fa){
	// printf("dfs %d %d\n",u,fa);
	if (dep[u]!=a[rt][u]){
		printf("3\n%d %d %d\n",rt,u,fa);
		// printf("%.5f\n",1.0*clock()/CLOCKS_PER_SEC);
		exit(0);
	}
	for(int i=first[u];i;i=e[i].next){
		int v=e[i].to;
		if (v!=fa){
			dep[v]=max(dep[u],e[i].cost);
			dfs(v,u);
		}
	}
}
int main(){
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	int n=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++){
			a[i][j]=read();
			// printf("a[%d][%d]=%d\n",i,j,a[i][j]);
		}
	for(int i=1;i<=n;i++)
		if (a[i][i])
			return printf("1\n%d 0 0\n",i),0;
	for(int i=1;i<=n;i++)
		for(int j=1;j<i;j++)
			if (a[i][j]!=a[j][i])
				return printf("2\n%d %d 0\n",i,j),0;
	memset(v,0x3f,sizeof(v));
	v[1]=0;
	for(int i=1;i<=n;i++){
		int mn=0;
		for(int j=1;j<=n;j++)
			if (!vis[j] && v[j]<v[mn])
				mn=j;
		if (fa[mn]){
			// printf("addedge %d %d\n",fa[mn],mn);
			addedge(fa[mn],mn,v[mn]);
			addedge(mn,fa[mn],v[mn]);
		}
		vis[mn]=1;
		for(int j=1;j<=n;j++)
			if (!vis[j] && a[mn][j]<v[j])
				v[j]=a[mn][j],fa[j]=mn;
	}
	for(int i=1;i<=n;i++){
		dep[i]=0;
		rt=i;
		dfs(i,0);
	}
	puts("0");
	// printf("%.5f\n",1.0*clock()/CLOCKS_PER_SEC);
}