#include<bits/stdc++.h>
using namespace std;
#define file "array"
typedef long long ll;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}
inline int getx(){
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	return c-'0';
}
const int maxn=(1<<18)+2;
ll f[maxn],g[maxn];
void fwt(ll *a,int l,int r){
	if (l==r)
		return;
	int mid=(l+r)/2;
	for(int i=l,j=mid+1;i<=mid;i++,j++){
		ll x=a[i]+a[j],y=a[i]-a[j];
		a[i]=x;
		a[j]=y;
	}
	fwt(a,l,mid);
	fwt(a,mid+1,r);
}
int a[maxn];
int main(){
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	int n=read(),m=read();
	ll ans=1LL*n*m;
	for(int i=0;i<n;i++)
		for(int j=1;j<=m;j++)
			a[j]|=(getx())<<i;
	for(int j=1;j<=m;j++)
		g[a[j]]++;
	for(int i=0;i<(1<<n);i++)
		f[i]=f[i>>1]+(i&1);
	for(int i=0;i<(1<<n);i++)
		f[i]=min(f[i],n-f[i]);
	fwt(f,0,(1<<n)-1);
	fwt(g,0,(1<<n)-1);
	for(int i=0;i<(1<<n);i++)
		f[i]*=g[i];
	fwt(f,0,(1<<n)-1);
	for(int i=0;i<(1<<n);i++)
		ans=min(ans,f[i]>>n);
	printf("%lld",ans);
}