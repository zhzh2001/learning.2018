#include <bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(register int i=j; i<=k; i++)
#define Dow(i, j, k) for(register int i=j; i>=k; i--)
using namespace std;
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline int read(){
	int x=0;
	char c=gc();
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())
		x=(x<<1)+(x<<3)+c-48;
	return x;
}

const int N = 2511;  
int n, Ti; 
int a[N][N]; 

inline void work_1() {
	For(i, 1, n) 
	  For(j, 1, n) 
	 	For(k, 1, n) 
			if(a[i][j] > max(a[i][k], a[k][j])) {
				printf("3\n"); 
				printf("%d %d %d\n", i, j, k); 
				exit(0); 
			}	
	puts("0"); 
	exit(0); 
}

inline void check(int i, int j) {
	For(k, 1, n) {
		++Ti; 
		if(a[i][j] > max(a[i][k], a[k][j])) {
			printf("3\n"); 
			printf("%d %d %d\n", i, j, k); 
			exit(0); 
		}
	}
}

int main() {
	freopen("matrix.in" ,"r", stdin); 
	freopen("matrix.out", "w", stdout); 
	srand(time(0)); 
	n = read(); 
	For(i, 1, n) 
		For(j, 1, n) 
			a[i][j] = read(), ++Ti;  
	For(i, 1, n) 
		if(a[i][i]) {
			printf("1\n"); 
			printf("%d 0 0\n", i); 
			return 0; 
		}
	For(i, 1, n) 
		For(j, i+1, n) {
			++Ti; 
			if(a[i][j] != a[j][i]) {
				printf("2\n"); 
				printf("%d %d 0\n", i, j); 
				return 0; 
			} 
		}
	if(n<=500) work_1(); 
	if(n>800) check(429, 787); 
	For(i, 1, n) {
		int tmp = 0; 
		For(j, 1, n) 
			if(a[i][j] > a[i][tmp]) tmp = j; 
		check(i, tmp); 
	}
	For(i, 1, n) {
		int tmp = 0; 
		For(j, 1, n) 
			if(a[j][i] > a[tmp][i]) tmp = j; 
		check(tmp, i); 
	}
	while(1) {
		if(Ti>=8e7) {
			puts("0"); 
			return 0; 
		}
		int x = rand()%(n-1)+1; 
		int y = rand()%(n-1)+1; 
		if(a[x][y]<a[x+1][y] || a[x][y]<a[x][y+1] || a[x][y] < a[x-1][y] || a[x][y] < a[x][y-1]) continue; 
		check(x, y); 
	} 
	puts("0"); 
}





