#include <bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(register int i=j; i<=k; i++)
#define Dow(i, j, k) for(register int i=j; i>=k; i--)
using namespace std;
inline GG read(){
	GG k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	k=k*10+ch-'0';
	return k*f;
}
inline void write(GG x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(GG x){ write(x);puts("");}

const int N = 20;  
int n, m, Mn, ans; 
int x[2*N], y[N]; 
int a[N][N], tmp[N][N]; 

void dfs(int u) {
	if(u>n+m) {
		int sum = 0; 
		For(i, n+1, n+m) 
			y[i-n] = x[i]; 
		For(i, 1, n) 
			For(j, 1, m) tmp[i][j] = a[i][j];  
		For(i, 1, n) 
			For(j, 1, m) {
				if(x[i]^y[j]) tmp[i][j]^=1; 
				sum += tmp[i][j]; 
			}
		if(sum < Mn) Mn = sum; 
		return; 
	}
	x[u] = 0; dfs(u+1); 
	x[u] = 1; dfs(u+1); 
}

int main() {
	freopen("array.in", "r", stdin); 
	freopen("array.out", "w", stdout); 
	n = read(); m = read(); 
	For(i, 1, n) 
		For(j, 1, m) {
			a[i][j] = read(); 
			Mn += a[i][j]; 
		}
	if(n+m <= 22) {
		dfs(1); 
		printf("%d\n", Mn); 
		return 0; 
	}
	ans = Mn; 
	For(i, 1, n) For(j, 1, m) tmp[i][j] = a[i][j]; 
	For(i, 1, m) {
		int sum = 0; 
		For(j, 1, n) {
			sum += a[j][i];
			if(!a[j][i]) --sum; 
		}
		if(sum > 0) {
			For(j, 1, n) a[j][i]^=1;  
			Mn -= sum; 
		}
	} 
	For(i, 1, n) {
		int sum = 0; 
		For(j, 1, m) {
			sum += a[i][j]; 
			if(!a[i][j]) --sum; 
		}
		if(sum > 0) Mn -= sum; 
	}
	
	swap(ans, Mn); 
	For(i, 1, n) For(j, 1, m) a[i][j] = tmp[i][j]; 
	For(i, 1, n) {
		int sum = 0; 
		For(j, 1, m) {
			sum += a[i][j]; 
			if(!a[i][j]) --sum; 
		}
		if(sum > 0) {
			For(j, 1, m) a[i][j]^=1; 
			Mn -= sum; 
		}
	}
	For(i, 1, m) {
		int sum = 0; 
		For(j, 1, n) {
			sum += a[j][i];
			if(!a[j][i]) --sum; 
		}
		if(sum > 0) Mn -= sum; 
	} 
	if(Mn < ans) ans = Mn; 
	printf("%d\n", ans); 
}



/*

3 4
0110
1010
0111

ans 



*/




