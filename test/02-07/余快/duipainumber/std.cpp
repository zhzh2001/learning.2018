#include <cmath>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#include <queue>
#include <set>
#define N 1000005
#define ll long long
#define inf 1000000005
#define mod 1000000007
#include <map>
#define int ll
using namespace std;
inline int read(){
    char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
    int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);putchar('\n');}
inline void wri(int x){wr(x);putchar(' ');}
int n,t,vis[N],ans;
signed main(){
	n=read();t=sqrt(n);
	for (int i=2;i<=t;i++){
		if (!vis[i]){
			for (int j=i;j<=n;j*=i){
				if (j!=i) ans++;
				if (j<=t) vis[j]=1;
			}
		}
	}
	ans++;
	wrn(ans);
	return 0;
}
