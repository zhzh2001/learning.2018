#include <cmath>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#include <queue>
#include <set>
#define N 800005
#define ll long long
#define inf 1000000005
#define mod 1000000007
#include <map>
using namespace std;
inline int read(){
    char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
    int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);putchar('\n');}
inline void wri(int x){wr(x);putchar(' ');}
int n,m,fa[550],t,vis[N],ans;
int gf(int x){
	return fa[x]==x?x:gf(fa[x]);
}
struct xx{
	char str[100];
	int lon;
}z[100];
//第二题这么水总觉得不太对。。 
signed main(){
	freopen("rev.in","r",stdin);
	freopen("rev.out","w",stdout);
	t=read();
	while (t--){
		n=read();
		for (int i=1;i<=n;i++) fa[i]=i;
		for (int i=1;i<=n;i++){
			scanf("%s",z[i].str+1);
			z[i].lon=strlen(z[i].str+1);
			for (int j=1;j<=z[i].lon/2;j++){
				if (z[i].str[j*2-1]>z[i].str[j*2]&&j*2-1!=z[i].lon) swap(z[i].str[j*2-1],z[i].str[j*2]);
			}
			for (int j=1;j<i;j++){
				if (z[i].lon==z[j].lon){
					if (gf(i)==gf(j)) continue;
					int pd=1;
					for (int k=1;k<=z[i].lon/2;k++){
						vis[z[i].str[k*2-1]*400+z[i].str[k*2]]++;vis[z[j].str[k*2-1]*400+z[j].str[k*2]]--;
					}
					for (int k=1;k<=z[i].lon/2;k++){
						if (vis[z[i].str[k*2-1]*400+z[i].str[k*2]]) pd=0;
					}
					if ((z[i].lon&1)&&(z[i].str[z[i].lon]!=z[j].str[z[i].lon])) pd=0;
					if (pd) fa[gf(i)]=gf(j);
					for (int k=1;k<=z[i].lon/2;k++){
						vis[z[i].str[k*2-1]*400+z[i].str[k*2]]--;vis[z[j].str[k*2-1]*400+z[j].str[k*2]]++;
					}
				}
			}
		}
		ans=0;
		for (int i=1;i<=n;i++) vis[gf(i)]++;
		for (int i=1;i<=n;i++){
			ans+=(vis[i]/2)*2;
			vis[i]=0;
		} 
		wrn(n-ans);
	}
	return 0;
}
