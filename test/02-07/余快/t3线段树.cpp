#include <cmath>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#include <queue>
#include <set>
#define N 200005
#define ll long long
#define inf 1000000005
#define mod 1000000007
#include <map>
#define int ll
using namespace std;
inline int read(){
    char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
    int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(ll x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(ll x){wr(x);putchar('\n');}
inline void wri(int x){wr(x);putchar(' ');}
int n,m,a[N],x,y;
struct xx{
	ll max,num,l,r,mid;
}tree[N*4];
ll ans;
void build(int x,int l,int r){
	tree[x].l=l;tree[x].r=r;
	if (l==r){
		tree[x].max=a[l];return;
	}
	tree[x].mid=(tree[x].l+tree[x].r)>>1;
	build(x*2,l,tree[x].mid);build(x*2+1,tree[x].mid+1,r);
	tree[x].max=max(tree[x*2].max,tree[x*2+1].max);
}
ll query(int x,int l,int r){
	if (tree[x].l==l&&tree[x].r==r) return tree[x].max; 
	if (tree[x].l!=tree[x].r&&tree[x].num>0){
		tree[x*2].num=max(tree[x*2].num,tree[x].num);tree[x*2].max=max(tree[x*2].max,tree[x*2].num);
		tree[x*2+1].num=max(tree[x*2+1].num,tree[x].num);tree[x*2+1].max=max(tree[x*2+1].max,tree[x*2+1].num);
		tree[x].num=0;	
	}
	if (r<=tree[x].mid) return query(x*2,l,r);
	else if (l>tree[x].mid) return query(x*2+1,l,r);
	else return max(query(x*2,l,tree[x].mid),query(x*2+1,tree[x].mid+1,r));
}
void change(int x,int l,int r,ll k){
	if (tree[x].l!=tree[x].r&&tree[x].num>0){
		tree[x*2].num=max(tree[x*2].num,tree[x].num);tree[x*2].max=max(tree[x*2].max,tree[x*2].num);
		tree[x*2+1].num=max(tree[x*2+1].num,tree[x].num);tree[x*2+1].max=max(tree[x*2+1].max,tree[x*2+1].num);
		tree[x].num=0;
	}
	if (tree[x].l==l&&tree[x].r==r){
		tree[x].num=k;tree[x].max=max(tree[x].max,tree[x].num);return;
	}
	if (r<=tree[x].mid) change(x*2,l,r,k);
	else if (l>tree[x].mid) change(x*2+1,l,r,k);
	else change(x*2,l,tree[x].mid,k),change(x*2+1,tree[x].mid+1,r,k);
	tree[x].max=max(tree[x*2].max,tree[x*2+1].max);
}
signed main(){
	n=read();
	for (int i=1;i<=n;i++) a[i]=read();
	build(1,1,n);
	m=read();
	for (int i=1;i<=m;i++){
		x=read();y=read();
		ans=query(1,1,x);
		wrn(ans);
		change(1,1,x,(ll)ans+y);
	}
	return 0;
}
