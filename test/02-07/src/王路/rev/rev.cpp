#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
using namespace std;

ifstream fin("rev.in");
ofstream fout("rev.out");

const int MaxM = 55, MaxN = 1005;

string str;
vector<int> s[MaxM];
bool del[MaxM];

int main() {
	freopen("rev.in", "r", stdin);
	freopen("rev.out", "w", stdout);
	int T, n;
	fin >> T;
	while (T--) {
		fin >> n;
		memset(del, 0x00, sizeof del);
		for (int i = 1; i <= n; ++i) {
			fin >> str;
			if (str.length() & 1)
				str = str + '!';
			s[i].clear();
			int len = str.length();
			for (int j = 0; j < len / 2; ++j) {
				if (str[j * 2] > str[j * 2 + 1])
					swap(str[j * 2], str[j * 2 + 1]);
				s[i].push_back((int)(str[j * 2]) * 100 + (int)(str[j * 2 + 1]));
			}
			sort(s[i].begin(), s[i].end());
		}
		sort(s + 1, s + n + 1);
		for (int i = 1; i < n; ++i)
			if (s[i] == s[i + 1] && !del[i])
				del[i] = del[i + 1] = true;
		int pcnt = 0;
		for (int i = 1; i <= n; ++i)
			if (!del[i]) 
				++pcnt;
		fout << pcnt << endl;
	}
	return 0;
}
