#include <bits/stdc++.h>
#include <Windows.h>
using namespace std;

inline int Uniform(int l, int r) {
	return rand() % (r - l + 1) + l;
}

int main() {
	freopen("number.in", "w", stdout);
	srand(GetTickCount());
	int n = Uniform(1, 1e6);
	cout << n << endl;
	return 0;
}