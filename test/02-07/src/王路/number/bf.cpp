#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
using namespace std;

typedef long long ll;
typedef long double ld;

const int MinN = 1e6 + 5;

bool vis[MinN];
ll n, ans;

int main() {
	freopen("number.in", "r", stdin);
	freopen("bf.out", "w", stdout);
	scanf("%lld", &n);
	ans = 1;
	if (n <= 1e6) {
		for (ll i = 2; i * i <= n; ++i) {
			for (ll j = i * i; j <= n; j *= i) {
				if (!vis[j]) {
					vis[j] = true;
					++ans;
				}
			}
		}
		printf("%lld\n", ans);
		return 0;
	}
	return 0;
}