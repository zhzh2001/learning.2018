#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <cmath>
using namespace std;

typedef long long ll;
typedef long double ld;
typedef double db;

const int MaxLimit = 1e6;
ll n;
bool used[MaxLimit];

int main() {
	freopen("number.in", "r", stdin);
	freopen("number.out", "w", stdout);
	scanf("%lld", &n);
	ll ans = 1, res = 0;
	for (int i = 2; i <= MaxLimit; ++i) {
		if (!used[i]) {
			for (ll k = i; k <= n / i; ) {
				k *= i;
				if (k <= 1e6)
					used[k] = true;
				++ans;
				if (k > 1e6 && k <= sqrt(n))
					++res;
			}
		}
	}
	if (sqrtl(n) > 1e6)
		ans += sqrtl(n) - 1e6 - res;
	printf("%lld\n", ans);
	return 0;
}