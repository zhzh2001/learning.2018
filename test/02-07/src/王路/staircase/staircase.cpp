#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
using namespace std;

template <typename T>
inline void Read(T &x) {
	x = 0;
	char ch;
	T flag = 1;
	for (ch = getchar(); isspace(ch); ch = getchar())
		;
	if (ch == '-') {
		ch = getchar();
		flag = -1;
	}
	for (; isdigit(ch); ch = getchar())
		x = x * 10 + ch - '0';
	x *= flag;
}

template <typename T>
inline void Write(T x) {
	static int dig[20], cnt;
	if (x < 0)
		putchar('-'), x = -x;
	cnt = 0;
	do {
		dig[++cnt] = x % 10;
	} while (x /= 10);
	for (int i = cnt; i >= 1; --i)
		putchar(dig[i] + '0');
}

typedef long long ll;

const int kMaxN = 100005;
int n, m, a[kMaxN], rt;

struct Node {
	int ls, rs;
	ll mx, cov;
} tr[kMaxN * 4];

inline void PushDown(int x) {
	if (~tr[x].cov) {
		tr[tr[x].ls].mx = tr[tr[x].rs].mx = tr[x].cov;
		tr[tr[x].ls].cov = tr[tr[x].rs].cov = tr[x].cov;
		tr[x].cov = -1;
	}
}

inline void PushUp(int x) {
	tr[x].mx = max(tr[x << 1].mx, tr[x << 1 | 1].mx);
}

inline void TreeBuild(int x, int l, int r) {
	if (l == r) {
		tr[x].mx = a[l];
		tr[x].cov = -1;
		tr[x].ls = tr[x].rs = 0;
	} else {
		tr[x].cov = -1;
		int mid = (l + r) >> 1;
		TreeBuild(tr[x].ls = (x << 1), l, mid);
		TreeBuild(tr[x].rs = (x << 1 | 1), mid + 1, r);
		PushUp(x);
	}
}

inline ll TreeQueryMax(int x, int l, int r, int ql, int qr) {
	if (ql <= l && r <= qr)
		return tr[x].mx;
	PushDown(x);
	int mid = (l + r) >> 1;
	if (qr <= mid)
		return TreeQueryMax(tr[x].ls, l, mid, ql, qr);
	if (ql > mid)
		return TreeQueryMax(tr[x].rs, mid + 1, r, ql, qr);
	return max(TreeQueryMax(tr[x].ls, l, mid, ql, qr), TreeQueryMax(tr[x].rs, mid + 1, r, ql, qr));
}

inline void TreeSetMax(int x, int l, int r, int ql, int qr, ll newval) {
	if (ql <= l && r <= qr) {
		tr[x].mx = tr[x].cov = newval;
		return;
	}
	PushDown(x);
	int mid = (l + r) >> 1;
	if (qr <= mid) {
		TreeSetMax(tr[x].ls, l, mid, ql, qr, newval);
	} else if (ql > mid) {
		TreeSetMax(tr[x].rs, mid + 1, r, ql, qr, newval);
	} else {
		TreeSetMax(tr[x].ls, l, mid, ql, qr, newval);
		TreeSetMax(tr[x].rs, mid + 1, r, ql, qr, newval);
	}
	PushUp(x);
}

int main() {
	freopen("staircase.in", "r", stdin);
	freopen("staircase.out", "w", stdout);
	Read(n);
	for (int i = 1; i <= n; ++i)
		Read(a[i]);
	rt = 1;
	TreeBuild(rt, 1, n);
	Read(m);
	for (int i = 1, x, y; i <= m; ++i) {
		Read(x), Read(y);
		ll height = TreeQueryMax(rt, 1, n, 1, x);
		Write(height);
		TreeSetMax(rt, 1, n, 1, x, height + y);
		putchar('\n');
	}
	return 0;
}