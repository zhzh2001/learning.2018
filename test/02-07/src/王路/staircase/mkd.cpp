#include <bits/stdc++.h>
#include <Windows.h>
using namespace std;

inline int Uniform(int l, int r) {
	return rand() % (r - l + 1) + l;
}

int main() {
	freopen("staircase.in", "w", stdout);
	srand(GetTickCount());
	int n = Uniform(1, 100);
	ios::sync_with_stdio(false);
	cout << n << endl;
	for (int i = 1; i <= n; ++i) {
		cout << Uniform(1, 1e9) << ' ';
	}
	cout << endl;
	int m = Uniform(1, 100);
	cout << m << endl;
	for (int i = 1; i <= m; ++i) {
		cout << Uniform(1, n) << ' ' << Uniform(1, 1e9) << endl;
	}
	return 0;
}