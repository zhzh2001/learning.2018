#include <bits/stdc++.h>
using namespace std;

const int kMaxN = 1e5 + 5;
long long a[kMaxN];

int main() {
	freopen("staircase.in", "r", stdin);
	freopen("bf.out", "w", stdout);
	int n, m;
	ios::sync_with_stdio(false);
	cin >> n;
	for (int i = 1; i <= n; ++i)
		cin >> a[i];
	cin >> m;
	for (int i = 1, x, y; i <= m; ++i) {
		cin >> x >> y;
		long long mx = 0;
		for (int i = 1; i <= x; ++i)
			mx = max(mx, a[i]);
		cout << mx << "\n";
		long long nh = mx + y;
		for (int i = 1; i <= x; ++i)
			a[i] = nh;
	}
	cout << flush;
	return 0;
}