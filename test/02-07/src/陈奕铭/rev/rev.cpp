#include<bits/stdc++.h>
using namespace std;
const int N = 55;
char s[N][N];
int len[N];
int n,T;
int ans;
char a1,a2;
char b1,b2;
bool pos2[N];
bool vis[N];

inline bool check(char a1,char a2,char b1,char b2){
	if(a1 == b1 && a2 == b2) return true;
	if(a1 == b2 && a2 == b1) return true;
	return false;
}

int main(){
	freopen("rev.in","r",stdin);
	freopen("rev.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d",&n);
		for(int i = 1;i <= n;++i){
			scanf("%s",s[i]+1);
			len[i] = strlen(s[i]+1);
		}
		memset(vis,true,sizeof vis);
		ans = n;
		for(int i = 1;i <= n;++i)
			if(vis[i]){
				for(int j = i+1;j <= n;++j)
					if(vis[j] && len[i] == len[j]){
						if(len[i]%2 != 0)
							if(s[i][len[i]] != s[j][len[j]]) continue;
						int num = len[i]/2;
						bool flag = false;
						memset(pos2,false,sizeof pos2);
						for(int k1 = 1;k1 <= num;++k1){
							a1 = s[i][k1*2]; a2 = s[i][k1*2-1];
							flag = false;
							for(int k2 = 1;k2 <= num;++k2)
								if(!pos2[k2]){
									b1 = s[j][k2*2]; b2 = s[j][k2*2-1];
									if(check(a1,a2,b1,b2)){
										pos2[k2] = true;
										flag = true;
										break;
									}
								}
							if(!flag) break;
						}
						if(!flag) continue;
						vis[j] = false;
						ans -= 2;
						break;
					}
			}
		printf("%d\n", ans);
	}
	return 0;
}