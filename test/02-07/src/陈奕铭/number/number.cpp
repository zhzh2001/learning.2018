#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const ll Q = 60;
ll p[Q] = {0,1,1000000000LL,1000000LL,31622,3981,1000,372,177,100,63,43,31,24,19,15,13,11,10,8,7,7,6,6,5,5,4,4,4,4,3,3,3,3,};

ll quickmi(ll x,ll num){
	ll ans = 1;
	for(;num;num >>= 1,x = x*x)
		if(num&1) ans = ans*x;
	return ans;
}

ll erfen(ll i,ll n){
	ll l = 2,r = p[i],ans = 1;
	while(l <= r){
		ll mid = (l+r)>>1;
		if(quickmi(mid,i) <= n){
			l = mid+1;
			ans = mid;
		}
		else r = mid-1;
	}
	return ans;
}

ll solved(ll n){
	ll st = 1,ans = 1;
	while((1ll<<(st+1)) <= n) ++st;
	for(ll i = 2;i <= st;++i){
		ll num = erfen(i,n);
		ans += num-solved(num);
	}
	return ans;
}

signed main(){
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	ll n = read();
	for(ll i = 33;i <= 37;++i) p[i] = 3;
	for(ll i = 38;i < Q;++i) p[i] = 2;
	printf("%lld\n",solved(n));
	return 0;
}