#include <bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 100000005;
bool pos[N];
ll n;
ll ans;

void solved(){
	ans = 1;
	ll num = sqrt(n);
	for(int i = 2;i <= num;++i){
		ll x = i;
		for(int j = 2;j < 60;++j){
			x = x*i;
			if(x > n) break;
			if(!pos[x]){
				// printf("%lld\n", x);
				ans++;
				pos[x] = true;
			}
		}
	}
	printf("%lld\n",ans);
}

int main(){
	n = read();
	solved();
	return 0;
}