#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll readll(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 100005;
int nxt[N];
int now;
ll tail[N];
int n,q;

int main(){
	freopen("staircase.in","r",stdin);
	freopen("staircase.out","w",stdout);
	n = read();
	now = 0;
	for(int i = 1;i <= n;++i){
		tail[i] = readll();
		if(tail[i] == tail[i-1])
			nxt[now] = i;
		else{
			nxt[now] = i;
			now = i;
		}
	}
	q = read();
	now = 0;
	while(q--){
		int x = read();
		ll leng = readll();
		while(now < x)
			now = nxt[now];
		printf("%lld\n", tail[now]);
		tail[now] += leng;
		while(now != n && tail[nxt[now]] <= tail[now]){
			tail[nxt[now]] = tail[now];
			now = nxt[now];
		}
	}
	return 0;
}