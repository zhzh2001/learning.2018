#include <bits/stdc++.h>
using namespace std;
int read()
{
	int data=0,w=1;
	char ch=0;
	while(ch!='-'&&(ch<'0'||ch>'9'))
		ch=getchar();
	if(ch=='-')
	{
		w=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9')
	{
		data=data*10+ch-'0';
		ch=getchar();
	}
	data*=w;
	return data;
}
void write(int x)
{
     if(x<0)
	 {
		 putchar('-');
		 x=-x;
	 }
     if(x>9) write(x/10);
	 putchar(x%10+'0');
}
int main()
{
	freopen(".in","r",stdin);
	freopen(".out","w",stdout);
}