#include <bits/stdc++.h>
int t,n,ans,a[100],f[100],d[100000],e[100000],x;
char ch,c[100][100];
using namespace std;
/*int read()
{
	int data=0,w=1;
	char ch=0;
	while(ch!='-'&&(ch<'0'||ch>'9'))
		ch=getchar();
	if(ch=='-')
	{
		w=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9')
	{
		data=data*10+ch-'0';
		ch=getchar();
	}
	data*=w;
	return data;
}
void write(int x)
{
     if(x<0)
	 {
		 putchar('-');
		 x=-x;
	 }
     if(x>9) write(x/10);
	 putchar(x%10+'0');
}*/
int main()
{
	freopen("rev.in","r",stdin);
	freopen("rev.out","w",stdout);
	scanf("%d",&t);
	while(t--)
	{
		scanf("%d%c",&n,&ch);
		for(int i=1;i<=n;scanf("%s",c[i++]));
		for(int i=1;i<=n;i++)
			a[i]=strlen(c[i]);
		memset(f,0,sizeof f);
		ans=0;
		for(int i=1;i<n;i++)
			for(int j=i+1;j<=n;j++)
				if(!f[i]&&!f[j]&&a[i]==a[j])
				{
					if(a[i]&1&&c[i][a[i]-1]!=c[j][a[i]-1]) continue;
					x=(a[i]|1)-1;
					memset(d,0,sizeof d);
					memset(e,0,sizeof e);
					for(int k=0;k<=x>>1;k++)
					{
						d[c[i][k<<1]*c[i][k<<1|1]]++;
						e[c[j][k<<1]*c[j][k<<1|1]]++;
					}
					int fff=1;
					for(int k=0;k<130*130;k++)
						if(d[k]!=e[k])
						{
							fff=0;
							break;
						}
					if(fff)
					{
						ans+=2;
						f[i]=f[j]=1;
					}
				}
		printf("%d\n",n-ans);
	}
}