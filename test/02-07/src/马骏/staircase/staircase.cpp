#include <bits/stdc++.h>
#define LL long long
LL x,y,m,n,ans,tree[1000000],add[1000000],a[1000000];
using namespace std;
LL read()
{
	LL data=0,w=1;
	char ch=0;
	while(ch!='-'&&(ch<'0'||ch>'9'))
		ch=getchar();
	if(ch=='-')
	{
		w=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9')
	{
		data=data*10+ch-'0';
		ch=getchar();
	}
	data*=w;
	return data;
}
void write(LL x)
{
     if(x<0)
	 {
		 putchar('-');
		 x=-x;
	 }
     if(x>9) write(x/10);
	 putchar(x%10+'0');
}
void pushup(LL o)
{
	tree[o]=max(tree[o<<1],tree[o<<1|1]);
}
void build(LL cnt,LL l,LL r)
{
	if(l==r) tree[cnt]=a[l];
	else
	{
		LL mid=l+r>>1;
		build(cnt<<1,l,mid);
		build(cnt<<1|1,mid+1,r);
		pushup(cnt);
	}
}
void pushdown(LL o)
{
	if(add[o])
	{
		add[o<<1]=add[o<<1|1]=tree[o<<1]=tree[o<<1|1]=add[o];
		add[o]=0;
	}
}
void update(LL cnt,LL l,LL r,LL ql,LL qr,LL addv)
{
    if(ql<=l&&qr>=r)
	{
		add[cnt]=addv;
		tree[cnt]=addv;
		return;
	}
	pushdown(cnt);
	LL m=r+l>>1;
	if(ql<=m) update(cnt<<1,l,m,ql,qr,addv);
	if(qr>=m+1) update(cnt<<1|1,m+1,r,ql,qr,addv);
	pushup(cnt);
}
LL query(LL o,LL l,LL r,LL ql,LL qr)
{
	if(ql<=l&&qr>=r) return tree[o];
	pushdown(o);
	LL m=r+l>>1,ans=0;
	if(ql<=m) ans=query(o<<1,l,m,ql,qr);
	if(qr>=m+1) ans=max(ans,query(o<<1|1,m+1,r,ql,qr));
	return ans;
}
int main()
{
	freopen("staircase.in","r",stdin);
	freopen("staircase.out","w",stdout);
	n=read();
	for(int i=1;i<=n;a[i++]=read());
	build(1,1,n);
	m=read();
	for(int i=1;i<=m;i++)
	{
		x=read();
		y=read();
		ans=query(1,1,n,1,x);
		write(ans);
		puts("");
		update(1,1,n,1,x,ans+y);
	}
	return 0;
}