#include <iostream>
#include <cstdio>
#include <cstring>
#include <string>
#include <algorithm>
using namespace std;
typedef long long ll;
string s;ll n,ans,len[100];
struct node{char g[2];}a[100][100];
bool operator<(node x,node y)
{
	return min(x.g[1],x.g[0])==min(y.g[1],y.g[0])?
	max(x.g[1],x.g[0])<max(y.g[1],y.g[0]):min(x.g[1],x.g[0])<min(y.g[1],y.g[0]);
}
bool vis[1000];
inline bool pd(ll x,ll y)
{
	if (len[x]!=len[y]) return 0;
	if (len[x]&1&&(a[x][len[x]/2+1].g[1]!=a[y][len[y]/2+1].g[1])) return 0;
	for (ll i=1;i<=len[x]/2;i++)
		if (a[x][i].g[1]!=a[y][i].g[0]||a[x][i].g[0]!=a[y][i].g[1])
			if (a[x][i].g[1]!=a[y][i].g[1]||a[x][i].g[0]!=a[y][i].g[0]) return 0;
	return 1;
}
int main()
{
	freopen("rev.in","r",stdin);
	freopen("rev.out","w",stdout);
	ios::sync_with_stdio(false);
	ll T;cin>>T;
	while (T--)
	{
		cin>>n;ans=n;
		memset(vis,0,sizeof(vis));
		for (ll i=1;i<=n;i++)
		{
			cin>>s;
			len[i]=s.size();
			s='.'+s;
			for (ll j=1;j<=len[i];j++)
				a[i][(j-1)/2+1].g[j&1]=s[j];
			sort(a[i]+1,a[i]+1+len[i]/2);
//	for (ll j=1;j<=len[i]/2;j++) cout<<a[i][j].g[1]<<a[i][j].g[0]<<' ';cout<<'\n';
			for (ll j=1;j<i;j++)
				if (!vis[j]&&pd(i,j))
				{
					vis[i]=vis[j]=1;
					ans-=2;
					break;
				}
		}
		cout<<ans<<'\n';
	}
	return 0;
}
