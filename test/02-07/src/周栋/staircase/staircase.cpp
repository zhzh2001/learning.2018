#include <iostream>
#include <cstdio>
using namespace std;
typedef long long ll;
#define dd c=getchar()
inline ll read()
{
	ll x=0,f=1;char dd;
	for(;!isdigit(c);dd)if(c=='-')f=-1;
	for(;isdigit(c);dd)x=x*10+c-'0';
	return x*f;
}
#undef dd
ll n,m,a[200000],w,h;
int main()
{
	freopen("staircase.in","r",stdin);
	freopen("staircase.out","w",stdout);
	n=read();
	for (ll i=1;i<=n;i++) a[i]=read();
	m=read();
	while (m--)
	{
		w=read(),h=read();
		printf("%lld\n",max(a[w],a[1]));
		a[1]=max(a[w],a[1])+h;
	}
	return 0;
}
