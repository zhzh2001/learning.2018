#include <iostream>
#include <cstdio>
#include <cmath>
using namespace std;
typedef long long ll;
#define dd c=getchar()
inline ll read()
{
	ll x=0,f=1;char dd;
	for(;!isdigit(c);dd)if(c=='-')f=-1;
	for(;isdigit(c);dd)x=x*10+c-'0';
	return x*f;
}
#undef dd
ll n,ans=1,sqrtn,t;
bool vis[100000010];
int main()
{
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	n=read();sqrtn=sqrt(n);
	ll m=min(sqrtn,1000000ll);
	for (ll i=2;i<=m;i++)
		if (!vis[i])
		{
			ll j=i;
			while (j<=n/i)
			{
				j*=i;
				if (j<=1000000) vis[j]=1;
				ans++;
				if(j>1000000&&j<=sqrtn) t++;
			}
		}
	if (sqrtn<=1000000) printf("%lld",ans);
	else printf("%lld",ans+sqrtn-m-t);
	return 0;
}
