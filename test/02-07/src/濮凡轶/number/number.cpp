#include <fstream>
#include <algorithm>
#include <cstdio>
using namespace std;
typedef long long LL;

bool ck(LL a,LL b,LL c)
{
	if(!b)
	{
		return 1;
	}
	while(c--)
		a/=b;
	return a>0;
}
LL sqrtn(LL a,LL n)
{
	LL mid;
	LL l=0;
	LL r=a;
	while(l<r)
	{
		mid=(l+r+1)>>1;
		if(ck(a,mid,n))
			l=mid;
		else
			r=mid-1;
	}
	return l;
}

LL n,A,B,ans=1,v,w,f[103];

int main()
{
	ifstream fin("number.in");
	ofstream fout("number.out");
	fin>>n;
	for(LL i=2;i<=64;++i)
	{
		w=1-f[i];
		for(LL j=i+i;j<=64;j+=i)
			f[j]+=w;
		v=sqrtn(n,i);
		ans+=w*(v-1);
	}
	fout<<ans<<endl;
	fin.close();
	fout.close();
	return 0;
}
