#include <cstdio>
#include <algorithm>
#define int long long
using namespace std;

int t[100005];

inline int read()
{
	char c=getchar();
	int t=0;
	bool flag=false;
	while(c<'0'||c>'9')
	{
		if(c=='-')
		{
			flag=true;
		}
		c=getchar();
	}
	while(c>='0'&&c<='9')
	{
		t*=10;
		t+=c-'0';
		c=getchar();
	}
	return t;
}
inline void write(int n)
{
	if(n<0)
	{
		putchar('-');
		n=-n;
	}
	if(n<=9)
	{
		putchar(n+'0');
		return;
	}
	write(n/10);
	putchar(n%10+'0');
	return;
}
inline void writeln(int n)
{
	write(n);
	puts("");
}

signed main()
{
	freopen("staircase.in","r",stdin);
	freopen("staircase.out","w",stdout);
	int n=read();
	for(int i=1;i<=n;++i)
	{
		t[i]=read();
	}
	int m=read();
	while(m--)
	{
		int a=read();
		int b=read();
		if(t[1]<t[a])
		{
			writeln(t[a]);
			t[a]+=b;
			t[1]=t[a];
		}
		else
		{
			writeln(t[1]);
			t[1]+=b;
		}
	}
	fclose(stdin);
	fclose(stdout);
}
