#include <bits/stdc++.h>
using namespace std;
int main(int argc, char const *argv[]) {
	freopen("staircase.in", "w", stdout);
	srand(time(0));
	int n = 100000, m = 100000;
	cout << n << endl;
	int first = 1;
	for (int i = 1; i <= n; i++) {
		cout << first << " ";
		first += rand() % 5;
	}
	cout << endl << m << endl;
	for (int i = 1; i <= m; i++) {
		cout << rand() % n + 1 << " " << rand() * rand() << endl;
	}
	return 0;
}