#include <bits/stdc++.h>
#define ll long long
#define N 100020
using namespace std;
inline ll read() {
	ll x=0;bool f=1;char ch=getchar();
	while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
	while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
ll mx[N];
// vl -> sons all;
// mx -> the max of sons.
ll ask_max(int l, int r) {
	ll res = mx[l];
	for (int i = l + 1; i <= r; i++)
		res = max(res, mx[i]);
	return res;
}
void update(int l, int r, ll v) {
	for (int i = l; i <= r; i++)
		mx[i] = v;
}
int main(int argc, char const *argv[]) {
	freopen("staircase.in", "r", stdin);
	freopen("staircase.ans", "w", stdout);
	int n = read();
	for (int i = 1; i <= n; i++)
		mx[i] = read();
	int m = read();
	while (m --) {
		int w = read();
		ll h = read();
		ll k = ask_max(1, w);
		printf("%lld\n", k);
		update(1, w, k + h);
	}
	return 0;
}