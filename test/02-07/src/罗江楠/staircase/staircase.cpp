#include <bits/stdc++.h>
#define ll long long
#define N 100020
using namespace std;
inline ll read() {
	ll x=0;bool f=1;char ch=getchar();
	while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
	while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
ll mx[N<<2], vl[N<<2];
// vl -> sons all;
// mx -> the max of sons.
void push_down(int x) {
	if (!vl[x]) return;
	vl[x << 1] = mx[x << 1] = vl[x];
	vl[x<<1|1] = mx[x<<1|1] = vl[x];
	vl[x] = 0;
}
ll ask_max(int x, int l, int r, int L, int R) {
	// printf("%d %d %d %d %d\n", x, l, r, L, R);
	if (l == L && r == R)
		return mx[x];
	push_down(x);
	int mid = L + R >> 1;
	if (l >  mid) return ask_max(x<<1|1, l, r, mid + 1, R);
	if (r <= mid) return ask_max(x << 1, l, r, L, mid);
	return max(ask_max(x<<1, l, mid, L, mid), ask_max(x<<1|1, mid + 1, r, mid + 1, R));
}
void update(int x, int l, int r, int L, int R, ll v) {
	// printf("%d %d %d %d %d %d\n", x, l, r, L, R, v);
	if (l == L && r == R) {
		vl[x] = mx[x] = v;
		return;
	}
	push_down(x);
	int mid = L + R >> 1;
	if (l > mid) update(x<<1|1, l, r, mid + 1, R, v);
	else if (r <= mid) update(x << 1, l, r, L, mid, v);
	else update(x << 1, l, mid, L, mid, v), update(x<<1|1, mid + 1, r, mid + 1, R, v);
	mx[x] = max(mx[x << 1], mx[x<<1|1]);
}
void build(int x, int l, int r) {
	// cout << "build::" << x << " " << l << " " << r << endl;
	if (l == r) {
		mx[x] = read();
		// cout << x << " is " << mx[x] << endl;
		return;
	}
	int mid = l + r >> 1;
	build(x << 1, l, mid);
	build(x<<1|1, mid + 1, r);
	mx[x] = max(mx[x << 1], mx[x<<1|1]);
}
int main(int argc, char const *argv[]) {
	freopen("staircase.in", "r", stdin);
	freopen("staircase.out", "w", stdout);
	int n = read();
	build(1, 1, n);
	int m = read();
	while (m --) {
		int w = read();
		ll h = read();
		ll k = ask_max(1, 1, w, 1, n);
		printf("%lld\n", k);
		update(1, 1, w, 1, n, k + h);
	}
	return 0;
}