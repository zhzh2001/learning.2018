#include <bits/stdc++.h>
#define ll long long
#define N 60
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
	while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
/*                                    *\

31. 已知 f(x) = x^2-ax+b 在 [1, 2] 有根，问 a^2+2b^2-4b 的最小值.

解:

原式 = a^2+2(b-1)^2-2

(1-a+b)(4-2a+b) <= 0

i)  { 1-a+b <= 0
    { 4-2a+b >= 0
    =>
    { a >= b+1
    { a <= b/2+2
    =>
    b+1 <= a <= b/2+2
    b+1 <= b/2+2
    b/2 <= 1
    b   <= 2
    when b = 1, 2 <= a <= 5/2, min = 2

ii) ...(same)
    b/2+2 <= a <= b+1
    b/2+2 <= b+1
    b >= 2
    when b = 2, 3 <= a <= 3, min = ... faq
\*                                    */
int main(int argc, char const *argv[]) {
    int n = read();
    /**************************************************************\
    |   but when it all falls down and it's time to say goodbye.   |
    |   when it all falls down                                     |
    |   when it all falls down                                     |
    |   I'll be fine.                                              |
    \**************************************************************/
    return 0;
}