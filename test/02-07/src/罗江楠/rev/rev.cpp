#include <bits/stdc++.h>
#define ll long long
#define N 60
using namespace std;
inline int read() {
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
	while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
char ch[N][N];
int len[N];
int msk[N][N];
int hsh(char a, char b) {
	int i = a - 'a' + 1;
	int j = b - 'a' + 1;
	return (i * 107 + j) * (j * 107 + i);
}
map<int, bool> mp;
int main(int argc, char const *argv[]) {
	freopen("rev.in", "r", stdin);
	freopen("rev.out", "w", stdout);
	int T = read();
	while (T --) {
		int n = read();
		int ans = n;
		mp.clear();
		for (int i = 1; i <= n; i++) {
			scanf("%s", ch[i] + 1);
			len[i] = strlen(ch[i] + 1);
			for (int j = 1; j + 1 <= len[i]; j += 2)
				msk[i][j + 1 >> 1] = hsh(ch[i][j], ch[i][j + 1]);
			if (len[i] & 1)
				msk[i][len[i] + 1 >> 1] = ch[i][len[i]] - 'a' + 1;
			sort(msk[i] + 1, msk[i] + (len[i] + 1 >> 1) + 1);
			int sk = 0;
			for (int j = 1; j <= len[i]; j += 2)
				sk = sk * 10007 + msk[i][j + 1 >> 1];
			if (mp[sk]) ans -= 2, mp[sk] = 0;
			else mp[sk] = 1;
		}
		cout << ans << endl;
	}
	return 0;
}