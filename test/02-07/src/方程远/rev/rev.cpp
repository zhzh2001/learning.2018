#include<bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){
	char c=getchar();while (c!='-'&&(c<'0'||c>'9'))c=getchar();
	int k=1,kk=0;if (c=='-')c=getchar(),k=-1;
	while (c>='0'&&c<='9')kk=kk*10+c-'0',c=getchar();return kk*k;
}
int i,j,k,t,n,l,ans;
int a[1000],f[1000][1000],flag[1000];
char s[1000];
signed main()
{
	freopen("rev.in","r",stdin);
	freopen("rev.out","w",stdout);
	t=read();
	while(t--)
	{
		n=read();
		for(i=1;i<=n;i++)
		{
			scanf("%s",s);
			l=strlen(s);
			a[i]=(l+1)/2;
			for(j=1;j<=a[i];j++)
			{
				if(s[j*2-2]>s[j*2-1])
					swap(s[j*2-2],s[j*2-1]);
				f[i][j]=(s[j*2-2]+1)*1000+s[j*2-1];
			}
			sort(f[i]+1,f[i]+1+a[i]);
		}
		memset(flag,false,sizeof(flag));
		ans=n;
		for(i=2;i<=n;i++)
			for (j=1;j<i;j++)
				if(!flag[j]&&a[i]==a[j])
				{
					bool fl=true;
					for(k=1;k<=a[i];k++)
						fl&=f[i][k]==f[j][k];
					if(fl)
					{
						ans-=2;
						flag[i]=flag[j]=1;
						break;
					}
				}
		cout<<ans<<"\n";
	}
}
