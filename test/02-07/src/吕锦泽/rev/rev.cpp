#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 55
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll T,n,ans,fa[N],size[N],len[N],a[N],b[N]; char s[N][N];
ll find(ll x) { return fa[x]==x?x:fa[x]=find(fa[x]); }
void merge(ll u,ll v){
	ll p=find(u),q=find(v);
	if (p!=q) { size[p]+=size[q]; fa[q]=p; }
}
ll judge(ll x,ll y){
	if ((len[x]&1)&&(s[x][len[x]]!=s[y][len[y]])) return 0;
	for (ll i=1;i*2<=len[x];++i){
		ll x1=s[x][i*2-1],x2=s[x][i*2];
		ll y1=s[y][i*2-1],y2=s[y][i*2];
		if (x1<x2) swap(x1,x2); a[i]=x1*10000+x2;
		if (y1<y2) swap(y1,y2); b[i]=y1*10000+y2;
	}
	sort(a+1,a+1+len[x]/2); sort(b+1,b+1+len[y]/2);
	for (ll i=1;i*2<=len[x];++i)
		if (a[i]!=b[i]) return 0;
	return 1;
}
void work(){
	n=read(); memset(s,0,sizeof s); ans=0;
	rep(i,1,n) scanf("%s",s[i]+1),len[i]=strlen(s[i]+1);
	rep(i,1,n) fa[i]=i,size[i]=1;
	rep(i,1,n) rep(j,i+1,n){
		if (len[i]==len[j]){
			if (judge(i,j)) merge(i,j);
		}
	}
	rep(i,1,n){
		ll tmp=find(i);
		if (size[tmp]&1) ++ans,size[tmp]=0;
	}
	printf("%d\n",ans);
}
int main(){
	freopen("rev.in","r",stdin);
	freopen("rev.out","w",stdout);
	T=read(); while (T--) work();
}
