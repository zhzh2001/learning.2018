#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 100005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,maxn[N<<2],cov[N<<2];
void work(ll p,ll v) { cov[p]=maxn[p]=v; }
void pushdown(ll p){
	work(p<<1,cov[p]); work(p<<1|1,cov[p]); cov[p]=0;
}
void build(ll l,ll r,ll p){
	if (l==r) { maxn[p]=read(); return;	}
	ll mid=l+r>>1;
	build(l,mid,p<<1); build(mid+1,r,p<<1|1);
	maxn[p]=max(maxn[p<<1],maxn[p<<1|1]);
}
ll query(ll l,ll r,ll s,ll t,ll p){
	if (l==s&&r==t) return maxn[p];
	ll mid=l+r>>1; if (cov[p]) pushdown(p);
	if (t<=mid) return query(l,mid,s,t,p<<1);
	else if (s>mid) return query(mid+1,r,s,t,p<<1|1);
	else return max(query(l,mid,s,mid,p<<1),query(mid+1,r,mid+1,t,p<<1|1));
}
void cover(ll l,ll r,ll s,ll t,ll v,ll p){
	if (l==s&&r==t) { work(p,v); return; }
	ll mid=l+r>>1; if (cov[p]) pushdown(p);
	if (t<=mid) cover(l,mid,s,t,v,p<<1);
	else if (s>mid) cover(mid+1,r,s,t,v,p<<1|1);
	else cover(l,mid,s,mid,v,p<<1),cover(mid+1,r,mid+1,t,v,p<<1|1);
	maxn[p]=max(maxn[p<<1],maxn[p<<1|1]);
}
int main(){
	freopen("staircase.in","r",stdin);
	freopen("staircase.out","w",stdout);
	n=read(); build(1,n,1);	m=read();
	rep(i,1,m){
		ll w=read(),h=read();
		ll tmp=query(1,n,1,w,1);
		printf("%lld\n",tmp); cover(1,n,1,w,tmp+h,1);
	}
}
