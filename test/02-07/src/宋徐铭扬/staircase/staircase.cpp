#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstdlib>
#define LL long long
using namespace std;
LL f[100010];
inline int read(){
	int s(0),w(1);
	char ch=getchar();
	while(ch<='0'||ch>'9'){if(ch=='-')w=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){s=(s<<3)+(s<<1)+ch-48;ch=getchar();}
	return s*w;
}	
int main()
{
	freopen("staircase.in","r",stdin);
	freopen("staircase.out","w",stdout);
	int n=read();
	for(int i=1;i<=n;++i)
	 f[i]=read();
	int m=read();
	for(int i=1;i<=m;++i)
	{
		int w=read(),h=read();
		printf("%lld\n",max(f[w],f[1]));
		f[1]=max(f[w],f[1])+h;
	}
	return 0;
}
