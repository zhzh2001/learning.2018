#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstdlib>
#include<bitset>
#define LL long long
using namespace std;
bitset<1000000010> f;
int main()
{
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	LL n,ans(1);
	f[1]=1;
	scanf("%lld",&n);
	for(int i=2;i<=sqrt(n);++i)
	if(!f[i])
	{
		LL t(i);
		while(t*=i,t<=n)
		{
			f[t]=1;
			++ans;
		}
	}
	printf("%lld",ans);
	return 0;
}
