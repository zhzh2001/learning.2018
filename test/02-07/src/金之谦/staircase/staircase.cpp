#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int n,a[100010],t[400010],add[400010];
inline void build(int l,int r,int nod){
	if(l==r){t[nod]=a[l];return;}
	int mid=l+r>>1;
	build(l,mid,nod*2);build(mid+1,r,nod*2+1);
	t[nod]=max(t[nod*2],t[nod*2+1]);
}
inline void pushdown(int nod){
	if(!add[nod])return;
	add[nod*2]=add[nod*2+1]=add[nod];
	t[nod*2]=t[nod*2+1]=add[nod];
	add[nod]=0;
}
inline void xg(int l,int r,int x,int v,int nod){
	if(r<=x){t[nod]=add[nod]=v;return;}
	pushdown(nod);
	int mid=l+r>>1;
	xg(l,mid,x,v,nod*2);
	if(x>mid)xg(mid+1,r,x,v,nod*2+1);
	t[nod]=max(t[nod*2],t[nod*2+1]);
}
inline int smax(int l,int r,int x,int nod){
	if(r<=x)return t[nod];
	pushdown(nod);
	int mid=l+r>>1;
	int ans=smax(l,mid,x,nod*2);
	if(x>mid)ans=max(ans,smax(mid+1,r,x,nod*2+1));
	return ans;
}
signed main()
{
	freopen("staircase.in","r",stdin);
	freopen("staircase.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	build(1,n,1);
	int q=read();
	while(q--){
		int x=read(),y=read();
		int p=smax(1,n,x,1);
		writeln(p);xg(1,n,x,p+y,1);
	}
	return 0;
}
