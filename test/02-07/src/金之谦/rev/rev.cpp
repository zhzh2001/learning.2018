#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
struct ppap{char x,y;}a[60],b[60];
char s[60][60];
int n,d[60][60],B[60];
inline bool cmp(ppap a,ppap b){return a.x==b.x?a.y<b.y:a.x<b.x;}
int main()
{
	freopen("rev.in","r",stdin);
	freopen("rev.out","w",stdout);
	for(int T=read();T;T--){
		n=read();memset(d,0,sizeof d);
		for(int i=1;i<=n;i++)scanf("%s",s[i]+1);
		for(int i=1;i<=n;i++){
			int l=strlen(s[i]+1);
			for(int j=1;j<=l;j+=2)a[(j+1)/2]=(ppap){min(s[i][j],s[i][j+1]),max(s[i][j],s[i][j+1])};
			sort(a+1,a+(l+1)/2+1,cmp);
			for(int j=i+1;j<=n;j++){
				int L=strlen(s[j]+1);if(l!=L)continue;
				if(l&1&&s[j][L]!=s[i][l])continue;
				for(int k=1;k<=l;k+=2)b[(k+1)/2]=(ppap){min(s[j][k],s[j][k+1]),max(s[j][k],s[j][k+1])};
				sort(b+1,b+(l+1)/2+1,cmp);
				bool flag=1;
				for(int k=1;k<=(l+1)/2;k++)if(a[k].x!=b[k].x||a[k].y!=b[k].y)flag=0;
				if(flag)d[i][j]=d[j][i]=1;
			}
		}
		memset(B,0,sizeof B);int ans=n;
		for(int i=1;i<=n;i++)if(!B[i]){
			for(int j=i+1;j<=n;j++)if(!B[j]&&d[i][j]){
				B[j]=B[i]=1;ans-=2;break;
			}
		}
		writeln(ans);
	}
	return 0;
}
