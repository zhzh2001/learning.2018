#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <bitset>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
bitset<1000000010>b;
int rk[1000010];
signed main()
{
	freopen("number.in","r",stdin);
	freopen("baoli.out","w",stdout);
	int n=read();int ans=1,p=sqrt(n);
	for(int k=2;k<=p;k++){
		int rp=0,x=n;
		while(x){
			x/=k;rp++;
		}
		rp--;rk[k]=rp;
	}
	for(int i=2;i*i<=n;i++)if(!b[i]){
		for(int j=i,l=1;l<rk[i];j*=i,l++){
			ans++;
			if(j*i<=p)b[j*i]=1;
		}
	}
	writeln(ans);
	return 0;
}
