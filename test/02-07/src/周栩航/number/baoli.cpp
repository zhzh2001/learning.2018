#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
#define For(i,j,k)	for(ll i=j;i<=k;++i)
#define Dow(i,j,k)	for(ll i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-48,c=getchar();
	return t*f;
}
inline void write(ll x){if(x>=10)	write(x/10);putchar(x%10+48);}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
ll n;
ll lim,ans;
ll q[2000001],top;
bool vis[20000001];
inline ll sqr(ll x){return x*x;}
inline int get(int x)
{
	int tmp=0;
	ll now=x;
	while(1)
	{
		now*=x;
		if(now>n)	return tmp;
		q[++top]=now;
		tmp++;
	}
}
int main()
{
	freopen("number.in","r",stdin);
	freopen("number1.out","w",stdout);
	n=read();
	lim=sqrt(n);
	if(sqr(lim+1)<=n)	lim++;
	if(sqr(lim-1)>n)	lim--;
	For(i,2,lim)
	{
		if(vis[i])	continue;
		ll tmp=i*i;
		while(1)
		{	
			if(tmp>lim)	break;
			vis[tmp]=1;
			tmp=tmp*i;
			if(tmp>lim)	break;
		}
	}
	ans=1;
	For(i,2,lim)	
	{
		if(vis[i])	continue;
		ans+=get(i);
	}
	writeln(ans);
}
//1000000000000000000

