#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
#define For(i,j,k)	for(ll i=j;i<=k;++i)
#define Dow(i,j,k)	for(ll i=k;i>=j;--i)
#define ll long long
#define eps 1e-8
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-48,c=getchar();
	return t*f;
}
inline void write(ll x){if(x>=10)	write(x/10);putchar(x%10+48);}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
ll n;
ll lim,ans,tn;
ll q[2000001],top;
bool vis[2000001];
inline ll sqr(ll x){return x*x;}
inline ll get(ll x)
{
	ll tmp=0;
	ll now=x;
	while(1)
	{
		now*=x;
		if(now>tn)	return tmp;
		q[++top]=now;
		tmp++;
	}
}
inline ll ksm(ll x,ll y){ll sum=1;for(;y;y/=2){if(y&1)	sum=sum*x;x=x*x;}	return sum;}
inline double ksm1(double x,ll y){double sum=1.0;for(;y;y/=2)	{if(y&1)	sum=sum*x;x=x*x;}	return sum;}
inline ll get1(ll x)
{
	ll l=1,r=60,ans=0;
	while(l<=r)
	{
		ll mid=l+r>>1;
		if(ksm(x,mid)<=n&&ksm1(x*1.0,mid)-eps<=n*1.0)	ans=mid,l=mid+1;else r=mid-1;
	}
	return ans-1;
}
inline ll Solve(ll x)
{
		ll l=1,r=tn,ans=0;
		while(l<=r)
		{
			ll mid=l+r>>1;
			if(ksm(mid,x)<=n&&ksm1(mid*1.0,x)-eps<=n*1.0)	ans=mid,l=mid+1;else	r=mid-1;
		}
		return ans-1;
}
int main()
{
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	n=read();
	tn=sqrt(n);
	lim=sqrt(tn);
	if(sqr(lim+1)<=tn)	lim++;
	For(i,2,lim)
	{
		if(vis[i])	continue;
		ll tmp=i*i;
		while(1)
		{	
			if(tmp>lim)	break;
			vis[tmp]=1;
			tmp=tmp*i;
		}
	}
	ans=1;
	For(i,2,lim)	if(!vis[i])	get(i);
	For(i,1,top)
		ans-=get1(q[i]);///cout<<i<<' '<<q[i]<<' '<<get1(q[i])<<endl;
	For(i,2,100)
	{
		if(ksm(2,i)>n)	break;
		if(ksm(2.0,i)>n*1.0+eps)	break;
		ans+=Solve(i);//	cout<<Solve(i)<<' '<<i<<' '<<tn<<endl;
	}
	writeln(ans);
}
