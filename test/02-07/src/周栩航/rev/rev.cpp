#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<ctime>
#include<algorithm>
#define For(i,j,k)	for(ll i=j;i<=k;++i)
#define Dow(i,j,k)	for(ll i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-48,c=getchar();
	return t*f;
}
inline void write(ll x){if(x>=10)	write(x/10);putchar(x%10+48);}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
char s[60][60];
int n,vis[101],ans,q[101][3],qvis[101],top,cnt[10001];
inline bool check(int x,int y)
{
	bool flag=1;
	int len1=strlen(s[x]+1),len2=strlen(s[y]+1);
	if(len1!=len2)	return 0;
	if(len1&1)	if(s[x][len1]!=s[y][len1])	return 0;
	top=0;
	For(i,1,len1/2)
	{
		char c1=s[x][i*2-1]-'a'+1,c2=s[x][i*2]-'a'+1;
		if(c1>c2)	swap(c1,c2);
		cnt[c1*28+c2]++;
	}
	For(i,1,len2/2)
	{
		char c1=s[y][i*2-1]-'a'+1,c2=s[y][i*2]-'a'+1;
		if(c1>c2)	swap(c1,c2);
		if(!cnt[c1*28+c2])	flag=0;
		else	cnt[c1*28+c2]--;
		if(flag==0)	break;
	}
	For(i,1,len1/2)
	{
		char c1=s[x][i*2-1]-'a'+1,c2=s[x][i*2]-'a'+1;
		if(c1>c2)	swap(c1,c2);
		if(cnt[c1*28+c2])	cnt[c1*28+c2]--;
	}
	return flag;
}
int T;
int main()
{
	freopen("rev.in","r",stdin);freopen("rev.out","w",stdout);
	T=read();
	while(T--)
	{
		n=read();
		For(i,1,n)	scanf("\n%s",s[i]+1),vis[i]=0;
		For(i,1,n)
			if(vis[i])	continue;
			else
				For(j,i+1,n)	
					if(check(i,j))	{vis[j]=vis[i]=1;break;}
		ans=0;
		For(i,1,n)	if(!vis[i])		ans++;	
		writeln(ans);
	}
}
/*
2
5
esprit
god
redotopc
odcpoter
dog
14
rats
live
stressed
to
act
as
star
desserts
of
evil
cat
sa
fo
ot
*/
