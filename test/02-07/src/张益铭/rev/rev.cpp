#include<cstdio>
#include<cstring>
#include<string>
#include<queue>
#include<iostream>
#define m(a) memset(a,0,sizeof(a))
using namespace std;
string a[51];
int l[51],f[51],n;
inline string fz(string a,int k){
	string s;
	for(int i=0;i<k;i++) s[i]=a[k-i-1];
	for(int i=k;i<a.length();i++) s[i]=a[i];
	return s;
}
void bfs(string s,int num){
	queue<string>q;
	while(!q.empty()) q.pop();
	q.push(s);
	while(!q.empty()){
		if(q.size()>2000000) return;
		s=q.front();
		q.pop();
		for(int i=2;i<=s.length();i+=2){
			string k=fz(s,i);
			for(int j=num+1;j<=n;j++) if(a[j]==k&&f[j]&&f[num]){
				f[j]=f[num]=1;
				return;
			}
			q.push(k);
		}
	}
}
int main(){
	freopen("rev.in","r",stdin);
	freopen("rev.out","w",stdout);
	int t;
	scanf("%d",&t);
	while(t--){
		m(f);
		int ans=0;
		scanf("%d",&n);
		for(int i=1;i<=n;i++){
			cin>>a[i];
			l[i]=a[i].length();
		}
		for(int i=1;i<=n;i++) if(!f[i]) bfs(a[i],i);
		for(int i=1;i<=n;i++) if(!f[i]) ans++;
		printf("%d\n",ans);
	}
	return 0;
}
