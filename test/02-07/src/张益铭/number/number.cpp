#include<cstdio>
#include<cmath>
#define ll long long
using namespace std;
int p[70],pr[70];
inline bool is(int n){
	int s=sqrt(n);
	for(int i=2;i<=s;i++) if(!(n%i)) return 0;
	return 1;
}
int main(){
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	ll n;
	scanf("%I64d",&n);
	p[2]=sqrt(n);
	ll s=log2(n);
	for(ll i=3;i<=s;i++)
		for(ll j=3;;j++){
			ll ss=1,f=0;
			for(ll k=1;k<=i;k++){
				ss*=j;
				if(ss>n){
					f=1;
					break;
				}
			}
			if(f){
				p[i]=j-1;
				break;
			}
		}
	ll ss=0,ans=p[2];
	for(ll i=2;i<=s;i++) if(is(i)) pr[++ss]=i;
	for(ll i=2;i<=ss;i++){
		ll num=0;
		for(ll j=1;j<i;j++){
			for(ll k=2;;k++){
				ll q=pow(k,pr[j]);
				if(q>p[pr[i]]) break;
				num++;
			}
		}
		ans+=p[pr[i]]-num-1;
	}
	printf("%lld",ans);
	return 0;
}
