#include<cstdio>
#define ll long long
const int N=1e5+1;
int a[N];
struct Segment_Tree{
	ll v,tag,mx;
}tree[N<<2];
inline int max(int a,int b){
	return a>b?a:b;
}
void build(int r,int s,int t){
	if(s==t) tree[r].v=tree[r].mx=a[s];
	else{
		int mid=s+t>>1,r1=r<<1,r2=r<<1|1;
		build(r1,s,mid);
		build(r2,mid+1,t);
		tree[r].v=tree[r1].v+tree[r2].v;
		tree[r].mx=max(tree[r1].mx,tree[r2].mx);
	}
}
inline void push(int r,int s,int t){
	if(tree[r].tag){
		int mid=s+t>>1,r1=r<<1,r2=r<<1|1;
		tree[r1].tag=tree[r].tag;
		tree[r2].tag=tree[r].tag;
		tree[r1].v=tree[r].tag*(mid-s+1);
		tree[r2].v=tree[r].tag*(t-mid);
		tree[r1].mx=tree[r2].mx=tree[r].tag;
		tree[r].tag=0;
	}
}
void update(int r,int ns,int nt,int s,int t,int k){
	if(nt<s||ns>t) return;
	if(ns>=s&&nt<=t){
		tree[r].v=k*(nt-ns+1);
		tree[r].mx=k;
		tree[r].tag=k;
		return;
	}
	push(r,ns,nt);
	int mid=ns+nt>>1,r1=r<<1,r2=r<<1|1;
	update(r1,ns,mid,s,t,k);
	update(r2,mid+1,nt,s,t,k);
	tree[r].v=tree[r1].v+tree[r2].v;
	tree[r].mx=max(tree[r1].mx,tree[r2].mx);
}
int query(int r,int ns,int nt,int s,int t){
	if(nt<s||ns>t) return 0;
	if(ns>=s&&nt<=t) return tree[r].mx;
	push(r,ns,nt);
	int mid=ns+nt>>1,r1=r<<1,r2=r<<1|1;
	return max(query(r1,ns,mid,s,t),query(r2,mid+1,nt,s,t));
}
inline int read(){
	char ch=getchar();
	int x=0;
	while(ch<'0'||ch>'9') ch=getchar();
	while(ch>='0'&&ch<='9') x=x*10+(ch^48),ch=getchar();
	return x;
}
int main(){
	freopen("staircase.in","r",stdin);
	freopen("staircase.out","w",stdout);
	int n=read(),m,x,y;
	for(int i=1;i<=n;i++) a[i]=read();
	build(1,1,n);
	m=read();
	while(m--){
		x=read(),y=read();
		int k=query(1,1,n,1,x);
		printf("%d\n",k);
		update(1,1,n,1,x,k+y);
	}
	return 0;
}
