#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline long long read(){
	long long t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-48,c=getchar();
	return t*f;
}
inline void write(long long x){if(x>=10)	write(x/10);putchar(x%10+48);}
inline void writeln(long long x){write(x);puts("");}
inline void write_p(long long x){write(x);putchar(' ');}
long long tr[1000001],tag[1000001],a[200001],n,m,x,y;
inline void Push(int x){
	if(tag[x]){
		tr[x<<1]=tag[x<<1]=tr[x<<1|1]=tag[x<<1|1]=tag[x];
		tag[x]=0;
	}
}
inline void Build(int x,int l,int r){
	if(l==r){
		tr[x]=a[l];
		return;
	}
	int mid=(l+r)>>1;
	Build(x<<1,l,mid);Build(x<<1|1,mid+1,r);
	tr[x]=max(tr[x<<1],tr[x<<1|1]);
}
inline void Upd(int x,int l,int r,int ql,int qr,ll val){
	if(ql<=l&&r<=qr){
		tr[x]=val,tag[x]=val;
		return;
	}
	Push(x);
	int mid=l+r>>1;
	if(ql<=mid){
		Upd(x<<1,l,mid,ql,qr,val);
	}
	if(qr> mid){
		Upd(x<<1|1,mid+1,r,ql,qr,val);
	}
	tr[x]=max(tr[x<<1],tr[x<<1|1]);
}
inline long long Get(int x,int l,int r,int ql,int qr){
	if(ql<=l&&r<=qr){
		return tr[x];
	}
	Push(x);
	int mid=l+r>>1;
	long long tmp=0;
	if(ql<=mid){
		tmp=max(tmp,Get(x<<1,l,mid,ql,qr));
	}
	if(qr> mid){
		tmp=max(tmp,Get(x<<1|1,mid+1,r,ql,qr));
	}
	tr[x]=max(tr[x<<1],tr[x<<1|1]);
	return tmp;	
}
int main(){
	freopen("staircase.in","r",stdin);
	freopen("staircase.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		a[i]=read();
	}
	Build(1,1,n);
	m=read();
	for(int i=1;i<=m;i++){
		x=read(),y=read();
		long long  ans=Get(1,1,n,1,x);
		writeln(ans);
		Upd(1,1,n,1,x,ans+y);
	}
	return 0;
}
