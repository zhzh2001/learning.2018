#include<fstream>
//#include<iostream>
#include<math.h>
#include<stdio.h>
#include<fstream>
#include<string.h>
#include<algorithm>
using namespace std;
ifstream fin("rev.in");
ofstream fout("rev.out");
int T,n,ans;
string s[55];
int num[55][55],fa[55],block[55],num2[55][60];
inline int find(int x){
	if(fa[x]==x){
		return x;
	}
	return fa[x]=find(fa[x]);
}
inline void uni(int x,int y){
	int fax=find(x),fay=find(y);
	if(fax!=fay){
		fa[fax]=fay;
	}
}
int main(){
	fin>>T;
	while(T--){
		ans=0;
		memset(num,0,sizeof(num));
		memset(num2,0,sizeof(num2));
		memset(block,0,sizeof(block));
		fin>>n;
		for(int i=1;i<=n;i++){
			fin>>s[i];
			fa[i]=i;
			for(int j=0;j<s[i].length();j++){
				num[i][s[i][j]-'a'+1]++;
			}
			for(int j=0;j<s[i].length();j+=2){
				if(j!=(s[i].length()-1)){
					num2[i][++num2[i][0]]=(s[i][j]-'a'+1)*((s[i][j+1]-'a'+1));
				}
			}
			sort(num2[i]+1,num2[i]+53);
		}
		for(int i=1;i<=n;i++){
			for(int j=i+1;j<=n;j++){
				int len=s[i].length();
				if(len==s[j].length()){
					bool yes=true;
					for(int k=1;k<=26;k++){
						if(num[i][k]!=num[j][k]){
							yes=false;
							break;
						}
					}
					for(int k=1;k<=51;k++){
						if(num2[i][k]!=num2[j][k]){
							yes=false;
							break;
						}
					}
					if(len%2==1){
						if(yes&&s[i][len-1]==s[j][len-1]){
							uni(i,j);
						}
					}else{
						if(yes){
							uni(i,j);
						}
					}
				}
			}
		}
		for(int i=1;i<=n;i++){
			block[fa[i]]++;
		}
		for(int i=1;i<=n;i++){
			ans+=block[i]/2;
		}
		fout<<n-ans*2<<endl;
	}
	return 0;
}
/*

in:
2
5
esprit
god
redotopc
odcpoter
dog
14
rats
live
stressed
to
act
as
star
desserts
of
evil
cat
sa
fo
ot

out:
3
0

*/
