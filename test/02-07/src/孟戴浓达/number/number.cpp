#include<fstream>
//#include<iostream>
#include<math.h>
#include<stdio.h>
#include<fstream>
using namespace std;
ifstream fin("number.in");
ofstream fout("number.out");
long long n,ans,num,sqrn;
bool mark[1000003];
int main(){
	fin>>n;
	sqrn=sqrt(n);
	for(int i=2;i<=1000000;i++){
		if(!mark[i]){
			long long k=i;
			while(k<=n/i){
				k*=i;
				if(k<=1000000){
					mark[k]=true;
				}
				ans++;
				if(k>1000000&&k<=sqrn){
					num++;
				}
			}
		}
	}
	if(sqrn>1000000){
		ans+=sqrn-1000000-num;
	}
	ans++;
	fout<<ans<<endl;
	return 0;
}
/*

in:
36
out:
9

in:
10
out:
4

*/
