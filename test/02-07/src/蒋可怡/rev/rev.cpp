#include<cstdio>
#include<iostream>
#include<cstring>
#include<string>
#include<algorithm>
using namespace std;
int T,n,ans;
string a[55];
bool v[55];
struct st{
	char a,b;
}t[55];
inline bool cmp(st a,st b){
	if(a.a!=b.a)return a.a<b.a;
	return a.b<b.b;
}
inline string work(string s)
{
	int k=0;
	string res="";
	memset(t,0,sizeof(t));
	for(int i=0;i<s.length()-1;i+=2)
	{
		t[++k].a=s[i],t[k].b=s[i+1];
		if(t[k].a>t[k].b)swap(t[k].a,t[k].b);
	}
	sort(t+1,t+k+1,cmp);
	for(int i=1;i<=k;i++)
	res=res+t[i].a+t[i].b;
	if(s.size()&1)
	res+=s[s.length()-1];
	return res;
}
int main(){
	freopen("rev.in","r",stdin);
	freopen("rev.out","w",stdout);
	cin>>T;
	while(T--){
		cin>>n;
		ans=n;
		memset(v,0,sizeof(v));
		for(int i=1;i<=n;i++)
		{
			cin>>a[i];
			a[i]=work(a[i]);
		}
		for(int i=1;i<n;i++)
			for(int j=i+1;j<=n;j++)
				if(!v[i]&&!v[j]&&a[i]==a[j])
				{
					v[i]=1;v[j]=1;
					ans-=2;
					break;
				}
		cout<<ans<<endl;
	}
}
