#include<cstdio>
#include<iostream>
using namespace std;
long long n,m,w,h,ans,a[100001];
int main(){
	freopen("staircase.in","r",stdin);
	freopen("staircase.out","w",stdout);
	scanf("%lld",&n);
	for(int i=1;i<=n;i++)scanf("%lld",a+i);
	scanf("%lld",&m);
	while(m--){
		scanf("%lld%lld",&w,&h);
		ans=max(a[1],a[w]);
		printf("%lld\n",ans);
		a[1]=ans+h;
	}
}
