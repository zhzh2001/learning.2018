#include<cstdio>
#include<iostream>
using namespace std;
typedef unsigned long long ull;
ull n,ans,l,r,mid,num,p,sqrtn,curtn;
bool f[1000005];
int main(){
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	scanf("%lld",&n);
	l=1;r=1000000000;
	while(l<=r){
		mid=(l+r)>>1;
		if(mid*mid<=n)sqrtn=mid,l=mid+1;else r=mid-1;
	}
	l=1;r=1000000;
	while(l<=r){
		mid=(l+r)>>1;
		if(mid*mid*mid<=n)curtn=mid,l=mid+1;else r=mid-1;
	}
	for(ull i=2;i<=curtn;i++)
		if(!f[i]){
			p=i;
			while(p<=n/i){
				p*=i;
				if(p<=curtn)f[p]=1;
				ans++;
				if(p>curtn&&p<=sqrtn)num++;
			}
		}
	printf("%lld",ans+1+sqrtn-curtn-num);
}
