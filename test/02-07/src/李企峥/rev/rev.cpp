#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
struct D{
	string s;
	int l;
}a[100];
ll n,x,y,m,mx,ans,T;
bool fl[1000];
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
bool check(int x,int y)
{
	if(a[x].l!=a[y].l)return false;
	int f[100][100];
	memset(f,0,sizeof(f));
	int xl,yl,xx,yy;
	string s1,s2;
	xl=a[x].l;
	yl=a[y].l;
	s1=a[x].s;
	s2=a[y].s;
	if(xl%2==1)if(s1[xl]!=s2[yl])return false;
	For(i,1,xl/2)
	{
		xx=s1[i*2-2]-'A';
		yy=s1[i*2-1]-'A';
		if(xx>yy)swap(xx,yy);
		f[xx][yy]++;
	}
	For(i,1,yl/2)
	{
		xx=s2[i*2-2]-'A';
		yy=s2[i*2-1]-'A';
		if(xx>yy)swap(xx,yy);
		if(f[xx][yy]==0)return false;
				else f[xx][yy]--;
	}
	return true;
}
int main()
{
	freopen("rev.in","r",stdin);
	freopen("rev.out","w",stdout);
	T=read();
	while(T--)
	{
		n=read();
		For(i,1,n)cin>>a[i].s,a[i].l=a[i].s.length();
		ans=0;
		memset(fl,true,sizeof(fl));
		For(i,1,n)
		{
			if(fl[i]==false)continue;
			For(j,i+1,n)
			{
				if(fl[j]==false)continue;
				if(check(i,j))
				{
					fl[j]=false;
					fl[i]=false;
					ans+=2;
					break;
				}
			}
		}
		ans=n-ans;
		cout<<ans<<endl;
	}
	return 0;
}  
