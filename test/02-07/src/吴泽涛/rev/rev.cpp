#include <bits/stdc++.h>
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
#define LL long long
using namespace std;
inline int read() {
	int x = 0, f = 1;
	char ch = getchar();
	while(ch<'0' || ch>'9') { if(ch=='-') f=-1; ch = getchar(); }
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48; ch = getchar(); }
	return x * f;
}

const int N = 66; 
int T,n,ans; 
char s[N][N],s1,s2,s3,s4; 
int l[N],f[N],t[N]; 
int a[301],b[301]; 

inline bool check(int x, int y) {
	int L = l[x]; if(L&1) L--; 
	for(int i=1; i<=L; i++) t[i] = 0; 
	for(int i=1; i<=L; i+=2) {
		bool flag = 0; 
		for(int j=1; j<=L; j+=2) {
			if(t[j]) continue; 
			s1 = s[x][i]; s2 = s[x][i+1]; 
			s3 = s[y][j]; s4 = s[y][j+1]; 			
			if(s1 > s2) swap(s1, s2); 
			if(s3 > s4) swap(s3, s4); 
			if(s1 == s3 && s2 == s4) {
				flag = 1; t[j] = 1; break; 
			}
		}
		if(!flag) return 0; 
	}
	for(int j=1; j<=L; j+=2) if(t[j]!=1) return 0;  
	return 1; 
}

int main() {
	freopen("rev.in","r",stdin); 
	freopen("rev.out","w",stdout); 
	
	T = read(); 
	while(T--) {
		n = read(); ans = n; 
		For(i, 1, n) scanf("%s",s[i]+1); 
		For(i, 1, n) f[i] = 0; 
		For(i, 1, n) l[ i ] = strlen(s[i]+1); 
		For(i, 1, n-1) 
		  For(j, i+1, n) {
		  	if(l[i] != l[j] || f[i] || f[j]) continue; 
		  	For(k, 0, 300) a[k]=0, b[k]=0;
		  	For(k, 1, l[i]) a[s[i][k]]++;
		  	For(k, 1, l[j]) b[s[j][k]]++;
		  	bool flag = 0; 
		  	For(k, 0, 300) if(a[k]!=b[k]) { flag = 1; break; }
		  	if(l[i]%2==1 && s[i][l[i]]!=s[j][l[i]]) flag = 1; 
		  	if(flag) continue; 
		  	if(check(i,j)) {
		  		ans-=2; 
		  		f[i] = 1; f[j] = 1; 
			  }
		  }
		printf("%d\n",ans); 
	}
	return 0; 
}





