#include <bits/stdc++.h>
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
#define LL long long
using namespace std;
inline int read() {
	int x = 0, f = 1;
	char ch = getchar();
	while(ch<'0' || ch>'9') { if(ch=='-') f=-1; ch = getchar(); }
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48; ch = getchar(); }
	return x * f;
}
inline void write(LL x) {
	if(x<0) putchar('-'), x = -x;
	if(x>9) write(x/10);
	putchar(x%10+48);
}
inline void writeln(LL x) {
	write(x); putchar('\n');
}

const int N = 100011; 
struct node{
	int l,r,flag; 
	LL mx, cover;
}tree[4*N];
int n,Q; 
int h[N]; 

inline void push_up(int root) {
	tree[root].mx = max(tree[root*2].mx, tree[root*2+1].mx); 
}
inline void build(int root, int l, int r) {
	tree[root].l = l; tree[root].r = r; tree[root].flag = 0; 
	if(l == r) {
		tree[root].mx = h[l]; 
		return; 
	} 
	int mid = (l+r)/2; 
	build(root*2, l, mid); 
	build(root*2+1, mid+1, r); 
	push_up(root); 
}

inline void push_down(int root) {
	if(!tree[root].flag) return; 
	tree[root*2].mx = tree[root*2].cover = tree[root].cover; 
	tree[root*2].flag = 1; 
	tree[root*2+1].mx = tree[root*2+1].cover = tree[root].cover; 
	tree[root*2+1].flag = 1; 
	tree[root].cover = tree[root].flag = 0; 
}

void Cover(int root, int l, int r, LL val) {
	push_down(root); 
	if(l == tree[root].l && r == tree[root].r) {
		tree[root].mx = tree[root].cover = val; tree[root].flag = 1; 
		return; 
	}
	int mid = (tree[root].l + tree[root].r)/2; 
	if(r <= mid) Cover(root*2, l, r, val); 
	else if(l > mid) Cover(root*2+1, l, r, val); 
	else Cover(root*2, l, mid, val), Cover(root*2+1, mid+1, r,val); 
	
	push_up(root); 
}

inline LL Query(int root, int l, int r) {
	push_down(root); 
	if(l == tree[root].l && r == tree[root].r) 
		return tree[root].mx; 
	int mid = (tree[root].l + tree[root].r)/2; 
	if(r <= mid) return Query(root*2, l, r); 
	if(l > mid)  return Query(root*2+1, l, r); 
	return max(Query(root*2, l, mid), Query(root*2+1, mid+1, r)); 
}

int main() {
	freopen("staircase.in","r",stdin);
	freopen("staircase.out","w",stdout); 
	n = read(); 
	For(i, 1, n) h[i] = read(); 
	Q = read(); 
	build(1, 1, n); 
	
	while(Q--) {
		int R = read(); 
		LL val = read(), ans; 
		ans = Query(1, 1, R); 
		writeln(ans); 
		val += ans;  
		Cover(1, 1, R, val); 
	}
	return 0; 
} 




