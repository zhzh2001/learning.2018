#include <bits/stdc++.h>
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
#define LL long long
using namespace std;
inline LL read() {
	LL x = 0, f = 1;
	char ch = getchar();
	while(ch<'0' || ch>'9') { if(ch=='-') f=-1; ch = getchar(); }
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48; ch = getchar(); }
	return x * f;
}

const int N = 1e6+11; 
LL n,ans,T; 
int f[N]; 

int main() {
	freopen("number.in","r",stdin); 
	freopen("number.out","w",stdout); 
	n = read(); 
	ans = 1;
	T = (LL)(sqrt(n));  
	for(LL i=2; i<=T; i++) 
		if(!f[i]) {
			LL x = i*i; 
			bool flag = 0; 
			while(x <= n) {
				++ans; 
				if(x <= T) f[x] = 1; 
				x *= i; 
			} 
		}
	printf("%lld\n",ans); 
	return 0; 
}




