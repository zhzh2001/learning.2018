program rev;
 var
  a:array[0..51,'a'..'z'] of longint;
  b:array[0..51,'A'..'Z'] of longint;
  i,j,i1,n,t,ssum,upass:longint;
  jc:char;
  s:string;
 begin
  assign(input,'rev.in');
  assign(output,'rev.out');
  reset(input);
  rewrite(output);
  readln(t);
  for i1:=1 to t do
   begin
    filldword(a,sizeof(a)>>2,0);
    filldword(b,sizeof(b)>>2,0);
    readln(n);
    for i:=1 to n do
     begin
      readln(s);
      for j:=1 to length(s) do
       if s[j]>='a' then inc(a[i,s[j]])
                    else inc(b[i,s[j]]);
     end;
    ssum:=0;
    for i:=1 to n-1 do
     for j:=i+1 to n do
      begin
       upass:=1;
       for jc:='a' to 'z' do
        if a[i,jc]<>a[j,jc] then upass:=0;
       for jc:='A' to 'Z' do
        if b[i,jc]<>b[j,jc] then upass:=0;
       if upass=1 then inc(ssum,2);
      end;
    writeln(n-ssum);
   end;
  close(input);
  close(output);
 end.
