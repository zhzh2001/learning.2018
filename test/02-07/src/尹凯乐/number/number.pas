program number2;
 var
  a:array[0..1000001] of longint;
  i,j,n,p,upass,lpass:longint;
  ssum:int64;
 begin
  assign(input,'number2.in');
  assign(output,'number2.out');
  reset(input);
  rewrite(output);
  readln(n);
  filldword(a,length(a),0);
  ssum:=1;
  for i:=1 to n do
   begin
    upass:=0;
    for j:=2 to i-1 do
     begin
      p:=i;
      while p mod j=0 do
       p:=p div j;
      if p=1 then a[i]:=1;
     end;
   end;
  for i:=1 to n do
   inc(ssum,a[i]);
  writeln(ssum);
  close(input);
  close(output);
 end.
