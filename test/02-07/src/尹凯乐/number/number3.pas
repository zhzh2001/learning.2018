program number;
 uses math;
 const
  maxn=80;
 var
  a,b,c,d:array[0..maxn+1] of longint;
  o,p,m,n,ssum,upass:int64;
  ps,pm:double;
  i,j:longint;
 function hahagcd(x,y:longint):longint;
  begin
   if x<y then exit(hahagcd(y,x));
   if x mod y=0 then exit(y);
   exit(hahagcd(y,x mod y));
  end;
 begin
  assign(input,'number.in');
  assign(output,'number.out');
  reset(input);
  rewrite(output);
  filldword(a,length(a)>>2,0);
  readln(n);
  a[1]:=1;
  for i:=2 to trunc(sqrt(maxn)) do
   for j:=2 to maxn div i do
    a[i*j]:=1;
  filldword(c,length(c),0);
  c[1]:=0;
  for i:=2 to maxn do
   begin
    o:=0;
    p:=i;
    for j:=2 to i do
     if (a[j]=0) and (p mod j=0) then
      begin
       inc(o);
       d[o]:=0;
       while p mod j=0 do
        begin
         inc(d[o]);
         p:=p div j;
        end;
      end;
    upass:=0;
    p:=d[1];
    for j:=2 to o do
     p:=hahagcd(p,d[j]);
    if p=1 then c[i]:=1;
   end;
  b[0]:=0;
  for i:=1 to maxn do
   b[i]:=b[i-1]+c[i];
  ssum:=1;
  for i:=2 to maxn do
   //if a[i]=0 then
    begin
     ps:=ln(n)/i;
     o:=1;
     p:=1008208820;
     while o<=p do
      begin
       m:=(o+p)>>1;
       pm:=ln(m);
       if pm>ps then p:=m-1
                else o:=m+1;
      end;
     //if o-1>0 then dec(o);
     inc(ssum,b[o-1]);
     //writeln(o);
    end;
  writeln(ssum);
  //writeln(ln(6):0:6);
  close(input);
  close(output);
 end.
