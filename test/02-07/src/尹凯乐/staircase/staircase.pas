program staircase;
 uses math;
 var
  //b:array[0..400001] of tree;
  a:array[0..100001] of int64;
  i,m,n,p,trees,x,y,z:longint;
  smax:int64;
 begin
  assign(input,'staircase.in');
  assign(output,'staircase.out');
  reset(input);
  rewrite(output);
  readln(n);
  for i:=1 to n do
   read(a[i]);
  readln;
  a[0]:=0;
  p:=1;
  readln(m);
  for i:=1 to m do
   begin
    readln(x,y);
    smax:=a[1];
    while x>=p do
     begin
      smax:=max(smax,a[p]);
      inc(p);
     end;
    writeln(smax);
    a[1]:=smax+y;
   end;
  close(input);
  close(output);
 end.
