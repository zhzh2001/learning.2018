program staircase;
 uses math;
 type
  tree=record
   l,r,m,d:int64;
  end;
 var
  b:array[0..400001] of tree;
  a:array[0..100001] of longint;
  i,m,n,trees,x,y,z:longint;
  smax:int64;
 function hahabud(x,y:longint):int64;
  var
   m,p:longint;
  begin
   inc(trees);
   p:=trees;
   m:=(x+y)>>1;
   if x=y then b[trees].m:=a[x];
   if x=y then exit(trees);
   b[p].l:=hahabud(x,m);
   b[p].r:=hahabud(m+1,y);
   b[p].m:=max(b[b[p].l].m,b[b[p].r].m);
   hahabud:=p;
  end;
 function hahachit(k,x,y,xc,yc:longint):int64;
  var
   m:longint;
  begin
   m:=(x+y)>>1;
   if b[k].d<>0 then
    begin
     b[b[k].l].d:=b[k].d;
     b[b[k].r].d:=b[k].d;
     b[k].m:=b[k].d;
     b[k].d:=0;
    end;
   hahachit:=0;
   if (xc<=x) and (yc>=y) then exit(b[k].m);
   if xc<=m then hahachit:=max(hahachit,hahachit(b[k].l,x,m,xc,yc));
   if yc>m then hahachit:=max(hahachit,hahachit(b[k].r,m+1,y,xc,yc));
  end;
 procedure hahafft(k,x,y,xc,yc:longint;z:int64);
  var
   m:longint;
  begin
   m:=(x+y)>>1;
   if (x>=xc) and (y<=yc) then
    begin
     b[k].d:=z;
     exit;
    end;
   if xc<=m then hahafft(b[k].l,x,m,xc,yc,z);
   if yc>m then hahafft(b[k].r,m+1,y,xc,yc,z);
  end;
 begin
  assign(input,'staircase.in');
  assign(output,'staircase.out');
  reset(input);
  rewrite(output);
  fillqword(b,sizeof(b)>>3,0);
  readln(n);
  for i:=1 to n do
   read(a[i]);
  readln;
  trees:=0;
  hahabud(1,n);
  //writeln(b[1].m);
  readln(m);
  for i:=1 to m do
   begin
    readln(x,y);
    smax:=hahachit(1,1,n,1,x);
    writeln(smax);
    hahafft(1,1,n,1,x,smax+y);
   end;
  close(input);
  close(output);
 end.
