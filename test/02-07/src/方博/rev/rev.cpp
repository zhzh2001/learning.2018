#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read(){char ch=getchar();ll x=0,f=1;for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')ch=getchar();for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';return x*f;}
inline void write(ll x){if(x<0){x=-x;putchar('-');}if(x>9)write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
const int N=55;
string s[N];
string p[N],s1;
int main()
{
	freopen("rev.in","r",stdin);
	freopen("rev.out","w",stdout);
	int T=read();
	while(T--){
		ll n=read();
		for(int i=1;i<=n;i++){
			cin>>s1;
			if(s1.length()%2==1)s1=s1+'0';
			for(int j=1;j<=s1.length()/2;j++)s[j].clear();
			int t=1;
			s[t].clear();
			for(int j=0;j<s1.length();j++){
				s[t]+=s1[j];
				if(s[t].length()==2){
					if(s[t][0]>=s[t][1])swap(s[t][0],s[t][1]);
					t++,s[t].clear();
				}
			}
			sort(s+1,s+t);
			p[i].clear();
			for(int j=0;j<=t;j++){
				p[i]+=s[j];
			}
		}
		sort(p+1,p+n+1);
		ll ans=n;
		for(int i=1;i<n;i++)
			if(p[i]==p[i+1])p[i].clear(),p[i+1].clear(),ans-=2;
		writeln(ans);
	}
	return 0;
}
