#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read(){char ch=getchar();ll x=0,f=1;for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')ch=getchar();for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';return x*f;}
inline void write(ll x){if(x<0){x=-x;putchar('-');}if(x>9)write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
ll n;
ll ans,t;
const int N=1e6+5;
bool use[N];
int main()
{
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	n=read();
	ans=1;
	for(int i=2;i<=1e6;i++)
		if(!use[i]){
			ll k=i;
			while(k<=n/i){
				k*=i;
				if(k<=1e6)use[k]=true;
				ans++;
				if(k>1e6&&k<=sqrt(n))t++;
			}
		}
	if(sqrt(n)>1e6)ans+=sqrt(n)-1e6-t;
	writeln(ans);
}
