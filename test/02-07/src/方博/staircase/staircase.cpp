#include<bits/stdc++.h>
#define ll long long
using namespace std;
ll n,x,y,m,mx,ans;
ll a[100005];
inline ll read(){char ch=getchar();ll x=0,f=1;for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')ch=getchar();for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';return x*f;}
inline void write(ll x){if(x<0){x=-x;putchar('-');}if(x>9)write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
int main()
{
	freopen("staircase.in","r",stdin);
	freopen("staircase.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	m=read();
	for(int i=1;i<=m;i++){
		x=read();y=read();
		if(i==1){
			writeln(a[x]);
			a[1]=a[x]+y;
			mx=x;
		}
		else{
			if(x<=mx)ans=a[1];
			else{
				mx=x;
				if(a[1]>a[x])ans=a[1];
					else ans=a[x];
			}
			writeln(ans);
			a[1]=ans+y;
		}
	}
	return 0;
}
