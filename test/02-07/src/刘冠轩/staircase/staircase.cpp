#include <algorithm>
#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#define L(u) (u<<1)
#define R(u) (u<<1)+1
using namespace std;
typedef long long LL;

inline LL read(){LL a=0,b=1;char c=getchar();
while(!isdigit(c)&&c!='-') c=getchar();if(c=='-'){b=-b;c=getchar();}
while(isdigit(c)){a=a*10+c-'0';c=getchar();}return a*b;}
inline void write(LL t){if(t<0) {putchar('-');t=-t;}if(t>9) write(t/10);putchar(t%10+48);}
inline void writeln(LL x){write(x);putchar('\n');}

int n,m;
LL a[100005],t[400005],tt[400005],w,h;

void pushup(int u){
    t[u]=max(t[R(u)],t[L(u)]);
}

void pushdown(int u){
    tt[R(u)]=tt[u];
    tt[L(u)]=tt[u];
    t[L(u)]=tt[u];
    t[R(u)]=tt[u];
    tt[u]=0;
}

void update(int u,int l,int r,int ll,int rr,LL v){
	if (l==ll && r==rr){t[u]=max(t[u],v);tt[u]=t[u];return;}
	if (tt[u]>0) pushdown(u);
	int mid=(l+r)>>1;
	if (rr<=mid) update(L(u),l,mid,ll,rr,v);
	else if (ll>mid) update(R(u),mid+1,r,ll,rr,v);
	else{
		update(L(u),l,mid,ll,mid,v);
		update(R(u),mid+1,r,mid+1,rr,v);
	}
	pushup(u);
}

void build(int u,int l,int r){
    if (l==r){t[u]=a[l];return;}
    int mid=(l+r)>>1;
    build(L(u),l,mid);
    build(R(u),mid+1,r);
    pushup(u);
}

LL query(int u,int l,int r,int ll,int rr){
    if (l==ll && r==rr) return t[u];
    int mid=(l+r)>>1;
    if (tt[u]>0) pushdown(u);
	if (rr<=mid) return query(L(u),l,mid,ll,rr);
	else if (ll>mid) return query(R(u),mid+1,r,ll,rr);
	else return	max(query(L(u),l,mid,ll,mid),query(R(u),mid+1,r,mid+1,rr));
}

int main(){
    freopen("staircase.in","r",stdin);
    freopen("staircase.out","w",stdout);
    n=read();
    for (int i=1;i<=n;i++) a[i]=read();
    build(1,1,n);
    m=read();
    for (int i=1;i<=m;i++){
        w=read();h=read();
        LL ans=query(1,1,n,1,w);
        writeln(ans);
        update(1,1,n,1,w,ans+h);
    }
    return 0;
}
