#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
using namespace std;
typedef long long LL;

LL f[1000005],n;
bool b[1000005];

int main(){
    freopen("number.in","r",stdin);
    freopen("number.out","w",stdout);
    cin>>n;LL ans=1,T=sqrt(n),s=0,k;
	for(k=2;k<=T;k++){
		LL sum=0,x=n;
		while(x){x/=k;sum++;}
		sum--;f[k]=sum;
		if (k*k*k>n) break;
	}
	for(LL i=2;i<k;i++)if(!b[i]){
		for(LL j=i,l=1;l<f[i];j*=i,l++){
			ans++;
			if(i*j<k)b[i*j]=1;
			else if(j*i<=T)s++;
		}
	}
	cout<<(ans+T-k-s+1);
    return 0;
}
