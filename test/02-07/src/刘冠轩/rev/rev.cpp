#include <algorithm>
#include <iostream>
#include <cstdio>
#include <cmath>
#include <cstring>
#include <cstdlib>
#define LL long long
using namespace std;

inline LL read(){LL a=0,b=1;char c=getchar();
while(!isdigit(c)&&c!='-') c=getchar();if(c=='-'){b=-b;c=getchar();}
while(isdigit(c)){a=a*10+c-'0';c=getchar();}return a*b;}
inline void write(LL t){if(t<0) {putchar('-');t=-t;}if(t>9) write(t/10);putchar(t%10+48);}
inline void writeln(LL x){write(x);putchar('\n');}

char str[100][100];
int f[100][100],len[100];

int main(){
    freopen("rev.in","r",stdin);
    freopen("rev.out","w",stdout);
    LL T=read();
    while (T--){
        LL n=read();LL ans=0;
        memset(f,0,sizeof(f));
        for (int i=1;i<=n;i++){
            gets(str[i]);
            len[i]=strlen(str[i]);
            if (len[i]%2==1)str[i][len[i]++]='1';
            for (int j=1;j<=len[i]/2;j++)
                f[i][j]=str[i][j*2-2]+str[i][j*2-1]+max(str[i][j*2-2],str[i][j*2-1])*17;
            sort(f[i]+1,f[i]+len[i]/2+1);
//            for (int j=1;j<=len[i]/2;j++)cout<<f[i][j]<<' ';cout<<endl;
            for (int j=1;j<i;j++)
                if (len[i]==len[j]){
                        int ff=0;
                        for (int k=1;k<=len[i]/2;k++)
                            if(f[i][k]!=f[j][k]){ff=1;
                      //  cout<<ff<<endl;
                      }
                        if (ff==1) continue;else {ans+=2;f[i][1]=1;f[j][1]=1;break;}
                    }
        }
        cout<<n-ans<<endl;
    }
    return 0;
}
