#include<bits/stdc++.h>
#define int long long
inline int read(){
	char c=getchar();while (c!='-'&&(c<'0'||c>'9'))c=getchar();
	int k=1,kk=0;if (c=='-')c=getchar(),k=-1;
	while (c>='0'&&c<='9')kk=kk*10+c-'0',c=getchar();return kk*k;
}using namespace std;
void write(int x){if (x<0)putchar('-'),x=-x;if (x)write(x/10),putchar(x%10+'0');}
void writeln(int x){write(x);if (x==0)putchar('0');puts("");}
int t,n,l,a[1000],f[1000][1000],flag[1000],ans;
char s[1000];
signed main(){
	freopen("rev.in","r",stdin);freopen("rev.out","w",stdout);
	t=read();while (t--){
		n=read();for (int i=1;i<=n;i++){
			scanf("%s",s);l=strlen(s);a[i]=(l+1)/2;
			for (int j=1;j<=a[i];j++){
				if (s[j*2-2]>s[j*2-1])swap(s[j*2-2],s[j*2-1]);f[i][j]=(s[j*2-2]+1)*1000+s[j*2-1];
			}sort(f[i]+1,f[i]+1+a[i]);
		}memset(flag,0,sizeof(flag));ans=n;
		for (int i=2;i<=n;i++)
			for (int j=1;j<i;j++)if (!flag[j]&&a[i]==a[j]){
				bool ff=1;for (int k=1;k<=a[i];k++)ff&=f[i][k]==f[j][k];
				if (ff){ans-=2;flag[i]=flag[j]=1;break;}
			}
		writeln(ans);
	}
}//我们定义2i-1和2i位为一组由题意得我们可以交换组内次序和组之间的相对次序，但是不能拆分组。暴力n^3判相同即可 
/*
2
5
esprit
god
redotopc
odcpoter
dog
14
rats
live
stressed
to
act
as
star
desserts
of
evil
cat
sa
fo
ot
*/
