#include<bits/stdc++.h>
#define int long long
inline int read(){
	char c=getchar();while (c!='-'&&(c<'0'||c>'9'))c=getchar();
	int k=1,kk=0;if (c=='-')c=getchar(),k=-1;
	while (c>='0'&&c<='9')kk=kk*10+c-'0',c=getchar();return kk*k;
}using namespace std;
void write(int x){if (x<0)putchar('-'),x=-x;if (x)write(x/10),putchar(x%10+'0');}
void writeln(int x){write(x);if (x==0)putchar('0');puts("");}
int n,m,x,y,z,ma[2000000],a[2000000],lazy[2000000];
void pushdown(int x){
	ma[x*2]=ma[x*2+1]=lazy[x*2]=lazy[x*2+1]=lazy[x];lazy[x]=0;
}void build(int l,int r,int d){
	if (l==r){ma[d]=a[l];return;}
	int m=(l+r)>>1;build(l,m,d*2);build(m+1,r,d*2+1);
	ma[d]=max(ma[d*2],ma[d*2+1]);
}int findit(int x,int y,int l,int r,int d){
	if (x<=l&&y>=r)return ma[d];
	int m=(l+r)>>1,ans=0;if (lazy[d])pushdown(d);
	if (x<=m)ans=max(ans,findit(x,y,l,m,d*2));
	if (y>m)ans=max(ans,findit(x,y,m+1,r,d*2+1));
	return ans;
}void putit(int x,int y,int z,int l,int r,int d){
	if (x<=l&&y>=r){ma[d]=lazy[d]=z;return;}
	int m=(l+r)>>1,ans=0;if (lazy[d])pushdown(d);
	if (x<=m)putit(x,y,z,l,m,d*2);
	if (y>m)putit(x,y,z,m+1,r,d*2+1);
	ma[d]=max(ma[d*2],ma[d*2+1]);
}
signed main(){
	freopen("staircase.in","r",stdin);freopen("staircase.out","w",stdout);
	n=read();for (int i=1;i<=n;i++)a[i]=read();build(1,n,1);
	m=read();while (m--){
		x=read();y=read();z=findit(1,x,1,n,1);
		writeln(z);z+=y;putit(1,x,z,1,n,1); 
	} 
}//前缀min前缀覆盖，这里为了方便打的都是区间操作 

