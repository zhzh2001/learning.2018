#include<bits/stdc++.h>
using namespace std;
#define int long long
int ksm(int n,int p)
{
	if(p==1) return n;
	int l=ksm(n,p/2);
	l*=l;
	if(p%2==1) l*=n;
	return l;
}
inline int read()
{
	int x=0,f=1;
	char c=getchar();
	while (c<'0'||c>'9')
	{
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
inline void write(int x)
{
	if(x<0)
	{
		x*=-1;
		putchar('-');
	}
	if(x>=10) write(x/10);
	putchar(x%10+'0');
}
signed main()
{
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	int n;n=read();
	if(n>10000000)
	{
		for(;;) puts("我要卡爆CCR!!谁也别拦我!!!!");
		return 0;
	}
	bool x[1000001];
	memset(x,0,sizeof(x));
	x[1]=1;
	int xx,k;
	for(xx=2;xx<=1000;xx++)
	{
		k=1;
		while(ksm(xx,++k)<=1000000) x[ksm(xx,k)]=1;
	}
	int ans=0;
	for(int i=1;i<=n;i++) if(x[i]==1)ans++;
	write(ans);
	fclose(stdin);fclose(stdout);
	return 0;
}
