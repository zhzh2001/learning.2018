#include<bits/stdc++.h>
using namespace std;
#define int long long
inline int read()
{
	int x=0,f=1;
	char c=getchar();
	while (c<'0'||c>'9')
	{
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
inline void write(int x)
{
	if(x<0)
	{
		x*=-1;
		putchar('-');
	}
	if(x>=10) write(x/10);
	putchar(x%10+'0');
}
signed main()
{
	freopen("staircase.in","r",stdin);
	freopen("staircase.out","w",stdout);
	int n;n=read();
	int h[n+1];
	for(int i=1;i<=n;i++) h[i]=read();
	int k;
	k=read();
	for(int u=1;u<=k;u++)
	{
		int w,dh;
		w=read();dh=read();
		int maxn=max(h[1],h[w]);
		write(maxn);
		putchar('\n');
		h[1]=maxn+dh;
	}
	fclose(stdin);fclose(stdout);
	return 0;
}
