#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
string a[10005];
bool lg[10000005];
struct liang {char a[2];};
bool cmp(liang a,liang b) {
	if(a.a[0]==b.a[0]) return a.a[1]<b.a[1];
	else return a.a[0]<b.a[0];
}
inline string gb(string b) {
	string ans="";
	liang ch[10005];
	int i=0;
	while(i*2<b.size()-1) {
		ch[i].a[0]=b[i*2];
		ch[i].a[1]=b[i*2+1];
		if(ch[i].a[0]>ch[i].a[1])swap(ch[i].a[0],ch[i].a[1]);
		i++;
	}
	sort(ch,ch+i,cmp);
	for(int j=0; j*2<b.size()-1;++j) ans=ans+ch[j].a[0]+ch[j].a[1];
	if(ans.size()<b.size()) ans=ans+b[b.size()-1];
	return ans;
}
bool pd(string aa,string bb) {
	if(aa.size()!=bb.size()) return false;
	else for(int j=0;j<aa.size();j++)if(aa[j]!=bb[j]) return false;
	return true;
}
int main() {
	freopen("rev.in","r",stdin);freopen("rev.out","w",stdout);
	int t;
	cin>>t;
	while(t--) {
		int n,mx;
		memset(lg,true,sizeof(lg));
		cin>>n;
		mx=n;
		for(int i=1; i<=n; ++i) {cin>>a[i];a[i]=gb(a[i]); }
		for(int i=1; i<=n-1; ++i)
			for(int j=i+1; j<=n; ++j) {
				if(lg[i]&&lg[j]&&pd(a[i],a[j])) {
					lg[i]=false;lg[j]=false;
					mx-=2;break;
				}
			}
		cout<<mx<<endl;
	}
}
