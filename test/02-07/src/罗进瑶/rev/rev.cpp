#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=55;
struct String {
	char str[maxn];
	int length;
} s[maxn];
int vis[255255]= {0},fa[maxn],cnt[maxn]= {0};
int get(int x) {
	return fa[x]==x?x:fa[x]=get(fa[x]);
}
inline int read() {
	char c=getchar();
	int tot=1;
	while ((c<'0'|| c>'9')&&c!='-') c=getchar();
	if (c=='-') {
		tot=-1;
		c=getchar();
	}
	int sum=0;
	while (c>='0'&&c<='9') {
		sum=sum*10+c-'0';
		c=getchar();
	}
	return sum*tot;
}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);putchar('\n');}
int main() {
	freopen("rev.in","r",stdin);
	freopen("rev.out","w",stdout);
	int T,n;
	T=read();
	while(T--) {
		n=read();
		for(int i=1; i<=n; i++) {
			scanf("%s",s[i].str+1);
			fa[i]=i;
			s[i].length=strlen(s[i].str+1);
			for(int j=2; j<=s[i].length; j+=2)if(s[i].str[j]<s[i].str[j-1])swap(s[i].str[j],s[i].str[j-1]);
			for(int j=1; j<i; j++) if(s[j].length==s[i].length) {
					if(get(i)==get(j))continue;
					for(int k=2; k<=s[i].length; k+=2) {
						vis[s[i].str[k-1]*1000+s[i].str[k]]++;
						vis[s[j].str[k-1]*1000+s[j].str[k]]--;
					}
					bool pd=true;
					for(int k=2; k<=s[i].length; k+=2)if(vis[s[i].str[k-1]*1000+s[i].str[k]]) {
							pd=false;
							break;
						}
					if((s[i].length&1)&&s[i].str[s[i].length]!=s[j].str[s[i].length])pd=false;
					if(pd)fa[get(i)]=fa[get(j)];
					for(int k=2; k<=s[i].length; k+=2) {
						vis[s[i].str[k-1]*1000+s[i].str[k]]--;
						vis[s[j].str[k-1]*1000+s[j].str[k]]++;
					}
				}
		}
		int ans=0;
		for(int i=1; i<=n; i++)cnt[fa[i]]++;
		for(int i=1; i<=n; i++) {
			ans+=(cnt[i]/2)*2;
			cnt[i]=0;
		}
		wrn(n-ans);
	}
	return 0;
}

