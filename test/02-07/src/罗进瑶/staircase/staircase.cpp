#include<cstdio>
#define ll long long
const ll MAXN=100100;
struct Tree {
	ll l,r,maxv,add;
#define l(x) tree[x].l
#define r(x) tree[x].r
#define maxv(x) tree[x].maxv
#define add(x) tree[x].add
#define lch (o<<1)
#define rch (o<<1|1)
} tree[MAXN<<2];
inline ll max(ll a,ll b) {
	return a>b?a:b;
}
ll h[MAXN],n,m;
void build(ll o,ll l,ll r) {
	l(o)=l,r(o)=r,add(o)=0;
	if(l==r) {
		maxv(o)=h[l];
		return;
	}
	ll mid=(l+r)>>1;
	if(l<=mid)build(lch,l,mid);
	if(r>mid)build(rch,mid+1,r);
	maxv(o)=max(maxv(lch),maxv(rch));
}
void spread(ll o) {
	if(add(o)) {
		maxv(lch)=max(maxv(lch),add(o));
		maxv(rch)=(maxv(rch),add(o));
		add(lch)=(add(lch),add(o));
		add(rch)=(add(rch),add(o));
		add(o)=0;
	}
}
void change(ll o,ll l,ll r,ll v) {
	if(l(o)>=l&&r(o)<=r) {
		add(o)=max(add(o),v);
		maxv(o)=max(maxv(o),v);
		return;
	}
	spread(o);
	ll mid=(l(o)+r(o))>>1;
	if(l<=mid)change(lch,l,r,v);
	if(r>mid)change(rch,l,r,v);
	maxv(o)=max(maxv(lch),maxv(rch));
}
ll ask(ll o,ll l,ll r) {
	if(l(o)>=l&&r(o)<=r)return maxv(o);
	spread(o);
	ll mid=(l(o)+r(o))>>1,val=0;
	if(l<=mid)val=ask(lch,l,r);
	if(r>mid)val=max(val,ask(rch,l,r));
	return val;
}
inline ll read(){
	char ch=getchar(),w=1;
	ll x=0;
	while(ch>'9'||ch<'0'){
		if(ch=='-')w=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}
	return w*x;
}
inline void write(ll x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void wrln(ll x){
	write(x);
	putchar('\n');
}
int main() {
	freopen("staircase.in","r",stdin);
	freopen("staircase.out","w",stdout);
	ll w,a,ans;
	n=read();
	for(int i=1; i<=n; i++)h[i]=read();
	build(1,1,n);
	m=read();
	while(m--) {
		w=read();a=read();
		ll MAXS=ask(1,1,w);
		ask(1,1,n);
		wrln(MAXS);
		change(1,1,w,MAXS+a);
	}
	return 0;
}



