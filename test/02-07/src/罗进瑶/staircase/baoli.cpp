#include<cstdio>
#include<algorithm>
using namespace std;
#define ll long long
const int maxn=100010;
struct Tree {
	ll l,r,v,maxv,add;
#define l(x) tree[x].l
#define r(x) tree[x].r
#define maxv(x) tree[x].maxv
#define add(x) tree[x].add
#define lch (o<<1)
#define rch (o<<1|1)
} tree[maxn<<2];
ll h[100000],n,m;
inline ll max(ll a,ll b) {
	return a>b?a:b;
}
void build(int o,int l,int r) {
	l(o)=l,r(o)=r,add(o)=0;
	if(l==r) {
		maxv(o)=h[l];
		return;
	}
	int mid=(l+r)>>1;
	if(l<=mid)build(lch,1,mid);
	if(r>mid)build(rch,mid+1,r);
	maxv(o)=max(maxv(lch),maxv(rch));
}
void spread(int o) {
	if(add(o)) {
		maxv(lch)=max(maxv(lch),add(o));
		maxv(rch)=max(maxv(rch),add(o));
		add(o)=0;
	}
}
void change(int o,int l,int r,ll v) {
	if(l(o)>=l&&r(o)<=r) {
		add(o)=max(add(o),v);
		maxv(o)=max(maxv(o),add(o));
		return;
	}
	spread(o);
	int mid=(l(o)+r(o))>>1;
	if(l<=mid)change(lch,l,r,v);
	if(r>mid)change(rch,l,r,v);
	maxv(o)=max(maxv(lch),maxv(rch));
}
ll ask(int o,int l,int r) {
	if(l<=l(o)&&r>=r(o))return maxv(o);
	int mid=(l(o)+r(o))>>1;
	ll val=0;
	spread(o);
	if(l<=mid)val=ask(lch,l,r);
	if(r>mid)val=max(val,ask(rch,l,r));
	return val;
}
int main() {
	ll w,a,ans;
	scanf("%lld",&n);
	for(int i=1; i<=n; i++)scanf("%lld",&h[i]);
	build(1,1,n);
	scanf("%lld",&m);
	while(m--) {
		scanf("%lld%lld",&w,&a);
		ll MAXS=ask(1,1,w);
		ask(1,1,n);
//		printf("MAXS=%lld\n",MAXS);
		change(1,1,w,MAXS+a);
		printf("1-3 :%lld\n",ask(1,1,3));

	}
	//printf("%lld\n",ask(1,1,n));
	return 0;
}
