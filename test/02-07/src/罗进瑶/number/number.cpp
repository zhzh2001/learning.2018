#include<cstdio>
#include<cmath>
#define ll long long
bool vis[10000105]={0};
int main() {
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	ll n,a=0,b=0;
	scanf("%lld",&n);
	ll m1=sqrt(n),m2=(int)pow(n,1.0/3.0);
	if((m2+1)*(m2+1)<n)m2++;
	for(ll i=2;i<=m2;i++)if(!vis[i]){
		for(ll j=i;j<=n/i;j*=i){
			if(j<=m2)vis[j]=true;
			a++;
			if(j>m2&&j<=m1)b++;
		}
	}
	printf("%lld",a-b+m1-m2+1);
}
