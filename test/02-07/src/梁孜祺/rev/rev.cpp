#include<bits/stdc++.h>
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define ll long long
#define N 55
using namespace std;
struct data{
	int a,b;
	bool operator <(const data &qwq)const{
		return (a==qwq.a)?(b<qwq.b):a<qwq.a;
	}
}s[N][N];
int n,ans,vis[N],a[N];
inline bool pd(int x,int y){
	For(i,1,a[x]/2) if(s[x][i].a!=s[y][i].a||s[x][i].b!=s[y][i].b) return 0;
	if(a[x]&1&&s[x][a[x]/2+1].a!=s[y][a[x]/2+1].a) return 0;
	return 1;
}
inline void solve(){
	ans=n;memset(vis,0,sizeof vis);
	For(i,1,n) sort(s[i]+1,s[i]+1+a[i]/2);
	For(i,1,n) if(!vis[i]){
		For(j,i+1,n) if(!vis[j]){
			if(a[i]==a[j]&&pd(i,j)){vis[i]=vis[j]=1;ans-=2;break;}
		}
	}
	printf("%d\n",ans);
}
int main(){
	freopen("rev.in","r",stdin);
	freopen("rev.out","w",stdout);
	int T;scanf("%d",&T);
	while(T--){
		scanf("%d",&n);memset(s,0,sizeof s);
		For(i,1,n){
			char p[N];scanf("%s",p+1);
			a[i]=strlen(p+1);
			For(j,1,a[i]/2) s[i][j]=(data){p[j*2-1],p[j*2]};
			if(a[i]&1) s[i][a[i]/2+1]=(data){p[a[i]],127};
			For(j,1,a[i]/2) if(s[i][j].a>s[i][j].b) swap(s[i][j].a,s[i][j].b);
		}
		solve();
	}
	return 0;
}
