#include<bits/stdc++.h>
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define N 100005
#define ll long long
using namespace std;
ll t[N];
int n;
int main(){
	freopen("staircase.in","r",stdin);
	freopen("staircase.out","w",stdout);
	scanf("%d",&n);
	For(i,1,n) scanf("%lld",&t[i]);
	int m;scanf("%d",&m);
	while(m--){
		int x;ll y;scanf("%d%lld",&x,&y);
		ll Mx=max(t[1],t[x]);
		printf("%lld\n",Mx);
		t[1]=Mx+y;
	}
	return 0;
}
