#include<cstdio>
#include<algorithm>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x=0,f=1; char c=getchar();
	while (c<'0'||c>'9'){
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
void write(ll x){
	if (x<0){
		putchar('-');
		x=-x;
	}
	if (x>9) write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x){
	write(x);
	puts(""); 
}
const int maxn=1e5+5;
int n,m;
ll h[maxn];
inline void init(){
	n=read();
	for (int i=1;i<=n;i++){
		h[i]=read();
	}
}
struct node{
	ll mx,cov;
}a[maxn*4];
inline void pushup(int k){
	a[k].mx=max(a[k<<1].mx,a[k<<1|1].mx);
}
void build(int k,int l,int r){
	a[k].cov=-1;
	if (l==r){
		a[k].mx=h[l];
		return;
	}
	int mid=(l+r)>>1;
	build(k<<1,l,mid); build(k<<1|1,mid+1,r);
	pushup(k); 
} 
inline void cover(int k,ll v){
	a[k].cov=v;
	a[k].mx=max(a[k].mx,v);
}
inline void pushdown(int k){
	if (a[k].cov!=-1){
		cover(k<<1,a[k].cov); cover(k<<1|1,a[k].cov);
		a[k].cov=-1;
	}
}
ll query(int k,int l,int r,int x,int y){
	if (l==x&&r==y){
		return a[k].mx;
	}
	pushdown(k);
	int mid=(l+r)>>1;
	if (mid>=y){
		return query(k<<1,l,mid,x,y);
	}else{
		if (mid<x){
			return query(k<<1|1,mid+1,r,x,y);
		}else{
			return max(query(k<<1,l,mid,x,mid),query(k<<1|1,mid+1,r,mid+1,y));
		}
	}
}
void update(int k,int l,int r,int x,int y,ll v){
	if (l==x&&r==y){
		cover(k,v);
		return;
	}
	pushdown(k);
	int mid=(l+r)>>1;
	if (mid>=y){
		update(k<<1,l,mid,x,y,v);
	}else{
		if (mid<x){
			update(k<<1|1,mid+1,r,x,y,v);
		}else{
			update(k<<1,l,mid,x,mid,v); update(k<<1|1,mid+1,r,mid+1,y,v);
		}
	}
	pushup(k);
}
inline void solve(){
	m=read();
	build(1,1,n);
	for (int i=1;i<=m;i++){
		ll w=read(),h=read();
		ll temp=query(1,1,n,1,w);
		update(1,1,n,1,w,h+temp);
		writeln(temp);
	}
}
int main(){
	freopen("staircase.in","r",stdin);
	freopen("staircase.out","w",stdout);
	init();
	solve();
	return 0;
}
