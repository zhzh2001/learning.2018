#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
const int maxn=55;
int n;
char s[maxn][maxn];
inline void init(){
	scanf("%d",&n);
	for (int i=1;i<=n;i++){
		scanf("%s",s[i]+1);
	}
}
int T;
inline string reverse(char s[],int l){
	int len=strlen(s+1);
	for (int i=1;i<=l/2;i++){
		swap(s[i],s[l-i+1]);
	}
	return s;
}
inline int get(char s1[],char s2[]){
	int len1=strlen(s1+1),len2=strlen(s2+1);
	if (len1!=len2) return 0;
	for (int i=len1;i;i--){
		if (i%2==1){
			if (s1[i]!=s2[i]){
				return 0;
			}
			continue;
		}
		for (int j=1;j<=len1;j++){
			if (s1[j]==s2[i]){
				if (j%2==0){
					reverse(s1,j);
					reverse(s1,i);
				}else{
					reverse(s1,j+1);
					reverse(s1,2);
					reverse(s1,i);
				}
				break;
			}
		}
	}
	return 1;
}
int ans,del[maxn];
inline void solve(){
	ans=n;
	for (int i=1;i<n;i++){
		for (int j=i+1;j<=n;j++){
			if (!del[j]) {
				if (get(s[i],s[j])){
					del[i]=del[j]=1;
					ans-=2;
				}
			}
		}
	}
	printf("%d\n",ans);
}
int main(){
	freopen("rev.in","r",stdin);
	freopen("rev.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		init();
		solve();
	}
	return 0;
}
