#include<cstdio>
#include<cmath>
using namespace std;
typedef long long ll;
ll n;
inline void init(){
	scanf("%lld",&n);
}
const int maxn=100;
bool flag[maxn];
int mu[maxn],pri[maxn],tot;
inline void prepare(){
	mu[1]=1;
	for (int i=2;i<=70;i++){
		if (!flag[i]){
			pri[++tot]=i;
			mu[i]=-1;
		}
		for (int j=1;i*pri[j]<70&&j<=tot;j++){
			flag[pri[j]*i]=1;
			if (i%pri[j]==0) {
				mu[i*pri[j]]=0;
				break;
			}
			mu[i*pri[j]]=-mu[i];
		}
	}
}
const double eps=1e-7;
int ans;
inline void solve(){
	prepare(); ans=1; 
	double temp=log(n); 
	int lim=(int) (temp/log(2)+eps);
	for (int i=2;i<=lim;i++){
		int temp=pow(n,(double)1/i)+eps;
		ans-=(int)(temp-1)*mu[i];
	}
	printf("%d\n",ans);
}
int main(){
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	init();
	solve();
	return 0;
}
