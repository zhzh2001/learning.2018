#include<cstdio>
#include<cmath>
#include<algorithm>
using namespace std;
typedef long long ll;
ll n;
inline void init(){
	scanf("%lld",&n);
}
const int maxn=1e6+5;
bool flag[maxn];
int pri[maxn],tot;
inline void prepare(){
	int lim=sqrt(n);
	for (int i=2;i<=lim;i++){
		if (!flag[i]){
			pri[++tot]=i;
		}
		for (int j=1;pri[j]*i<=lim&&j<=tot;j++){
			if (i%pri[j]==0){
				break;
			}
			flag[i*pri[j]]=1;
		}
	}
}
int ans;
inline void solve(){
	prepare(); ans=1;
	double temp=log(n);
	for (int i=1;i<=tot;i++){
		ans+=(int)temp/log(pri[i]);
	}
	printf("%d\n",ans);
}
int main(){
	init();
	solve();
	return 0;
}
