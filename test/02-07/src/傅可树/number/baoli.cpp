#include<cstdio>
#include<cmath>
using namespace std;
typedef long long ll;
const int maxn=1e6+5;
ll n,ans,lim;
bool flag[maxn];
int main(){
	freopen("number.in","r",stdin);
	freopen("baoli.out","w",stdout);
	scanf("%lld",&n);
	ans=1; lim=(ll) sqrt(n);
	for (int i=2;i<=lim;i++){
		ll now=i*i;
		while (now<=n){
			flag[now]=1;
			now*=i;
		}
	}
	for (int i=2;i<=n;i++){
		if (flag[i]){
			ans++;
	//		printf("%d ",i);
		}
	}
	printf("%lld\n",ans);
	return 0;
}
