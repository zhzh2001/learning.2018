#include<cstdio>
#include<cmath>
#include<algorithm>
using namespace std;
long long x[100001];
long long i,j,n,m,p,t,maxh;
inline int read(){
    int out=0,fh=1;
    char cc=getchar();
    if (cc=='-') fh=-1;
    while (cc>'9'||cc<'0') cc=getchar();
    while (cc>='0'&&cc<='9') {out=out*10+cc-'0';cc=getchar();}
    return out*fh;
}
int main()
{
	freopen("staircase.in","r",stdin);
	freopen("staircase.out","w",stdout);
	n=read();
	for(i=1;i<=n;i++)
		x[i]=read();
	m=read();
	for(i=1;i<=m;i++)
	{
		maxh=0;
		p=read();
		t=read();
		printf("%lld\n",max(x[1],x[p]));
		x[1]=max(x[1],x[p])+t;
	}
	return 0;
}
