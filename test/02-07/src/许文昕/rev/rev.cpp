#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int read()
{
    int out=0,fh=1;
    char cc=getchar();
    if (cc=='-') fh=-1;
    while(cc>'9'||cc<'0')cc=getchar();
    while(cc>='0'&&cc<='9')
	{
	    out=out*10+cc-'0';
		cc=getchar();
	}
    return out*fh;
}
int i,j,k,t,n,l,ans;
int a[1000],f[1000][1000],flag[1000];
char s[1000];
signed main()
{
	freopen("rev.in","r",stdin);
	freopen("rev.out","w",stdout);
	t=read();
	do
	{
		t--;
		n=read();
		for(i=0;i<n;i++)
		{
			scanf("%s",s);
			l=strlen(s);
			a[i]=(1+l)/2;
			for(j=1;j<=a[i];j++)
			{
				if(s[j*2-2]>s[j*2-1])swap(s[j*2-2],s[j*2-1]);
				f[i][j]=(s[j*2-2]+1)*1000+s[j*2-1];
			}
			sort(f[i]+1,f[i]+1+a[i]);
		}
		memset(flag,false,sizeof(flag));
		ans=n;
		for(i=2;i<=n;i++)
			for (j=1;j<i;j++)
				if(!flag[j]&&a[i]==a[j])
				{
					bool fl=true;
					for(k=1;k<=a[i];k++)
						fl&=f[i][k]==f[j][k];
					if(fl)
					{
						ans-=2;
						flag[i]=flag[j]=1;
						break;
					}
				}
		cout<<ans<<"\n";
	}while(t>0);
}
