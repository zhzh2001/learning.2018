#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline LL read(){LL a=0,b=1;char dd;while(!isdigit(c)&&c!='-')dd;
	if(c=='-'){b=-b;dd;}while(isdigit(c)){a=a*10+c-'0';dd;}return a*b;}
#undef dd
inline LL sqr(LL v){return v*v;}
bool ck(LL a,LL b,LL c){
	if(b==0)return 1;
	while(c--)a/=b;
	return a>0;
}
LL sqrtn(LL a,LL n){
	LL mid;
	LL l=0,r=a;
	while(l<r){
		mid=(l+r+1)>>1;
		if(ck(a,mid,n))l=mid;
		else r=mid-1;
	}
	return l;
}
LL n,A,B,ans=1,v,w,f[103];
int main(){
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	n=read();
	for(LL i=2;i<=64;i++){
		w=1-f[i];
		for(LL j=i+i;j<=64;j+=i)f[j]+=w;
		v=sqrtn(n,i);
		if(v>0)ans+=w*(v-1);
	}
	printf("%lld\n",ans);
	return 0;
}
