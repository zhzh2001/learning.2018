#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;
typedef long long LL;
const LL t9=1e9,t6=1e6;
#define dd c=getchar()
inline LL read(){LL a=0,b=1;char dd;while(!isdigit(c)&&c!='-')dd;
	if(c=='-'){b=-b;dd;}while(isdigit(c)){a=a*10+c-'0';dd;}return a*b;}
#undef dd
LL n,A,B,ans=1,v,w;
bool vis[5003];
int main(){
	freopen("number.in","r",stdin);
	freopen("number.ans","w",stdout);
	n=read();
	for(LL i=2;i<=n;i++){
		v=i;
		while(n/v>=i){
			v*=i;
			vis[v]=1;
		}
	}
	for(LL i=2;i<=n;i++)ans+=vis[i];
	printf("%lld\n",ans);
	return 0;
}
