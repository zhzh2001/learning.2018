#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstring>
#include <string>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline LL read(){LL a=0,b=1;char dd;while(!isdigit(c)&&c!='-')dd;
	if(c=='-'){b=-b;dd;}while(isdigit(c)){a=a*10+c-'0';dd;}return a*b;}
#undef dd
struct grp{
	char a,b;
	grp(){
		a=b=' ';
	}
	grp(char x,char y){
		a=min(x,y);
		b=max(x,y);
	}
}p[103];
bool operator<(grp a,grp b){
	return a.a!=b.a?a.a<b.a:a.b<b.b;
}
LL T,n,l,ans;
char c[53];
bool vis[53];
string s[53];
int main(){
	freopen("rev.in","r",stdin);
	freopen("rev.out","w",stdout);
	T=read();
	while(T--){
		ans=n=read();
		for(LL i=1;i<=n;i++){
			scanf("%s",c+1);
			l=strlen(c+1);
			for(LL j=2;j<=l;j+=2){
				p[j>>1]=grp(c[j-1],c[j]);
			}
			sort(p+1,p+l/2+1);
			for(LL j=2;j<=l;j+=2){
				c[j-1]=p[j>>1].a;
				c[j]=p[j>>1].b;
			}
			s[i]=c+1;
		}
		sort(s+1,s+n+1);
		memset(vis,0,sizeof(vis));
		for(LL i=2;i<=n;i++)if(s[i]==s[i-1]&&!vis[i-1]){
			vis[i]=1;
			ans-=2;
		}
		printf("%lld\n",ans);
	}
	return 0;
}
