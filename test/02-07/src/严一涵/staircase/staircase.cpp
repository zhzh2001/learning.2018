#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline LL read(){LL a=0,b=1;char dd;while(!isdigit(c)&&c!='-')dd;
	if(c=='-'){b=-b;dd;}while(isdigit(c)){a=a*10+c-'0';dd;}return a*b;}
#undef dd
#define ls (nod<<1)
#define rs (nod<<1|1)
LL tree[400003],lazy[400003],a[100003];
void pushup(LL nod,LL l,LL r){
	tree[nod]=max(tree[ls],tree[rs]);
}
void pushdown(LL nod,LL l,LL r){
	if(lazy[nod]>0){
		tree[ls]=lazy[ls]=lazy[nod];
		tree[rs]=lazy[rs]=lazy[nod];
		lazy[nod]=0;
	}
}
void build(LL nod,LL l,LL r){
	lazy[nod]=0;
	if(l==r){
		tree[nod]=a[l];
		return;
	}
	LL mid=(l+r)>>1;
	build(ls,l,mid);
	build(rs,mid+1,r);
	pushup(nod,l,r);
}
void chg(LL nod,LL l,LL r,LL x,LL y,LL v){
	if(l==x&&r==y){
		tree[nod]=lazy[nod]=v;
		return;
	}
	pushdown(nod,l,r);
	LL mid=(l+r)>>1;
	if(x<=mid)chg(ls,l,mid,x,min(y,mid),v);
	if(y>mid)chg(rs,mid+1,r,max(x,mid+1),y,v);
	pushup(nod,l,r);
}
LL query(LL nod,LL l,LL r,LL x,LL y){
	if(l==x&&r==y)return tree[nod];
	pushdown(nod,l,r);
	LL mid=(l+r)>>1,ans=0;
	if(x<=mid)ans=max(ans,query(ls,l,mid,x,min(y,mid)));
	if(y>mid)ans=max(ans,query(rs,mid+1,r,max(x,mid+1),y));
	return ans;
}
#undef ls
#undef rs
LL n,m,x,y,z;
int main(){
	freopen("staircase.in","r",stdin);
	freopen("staircase.out","w",stdout);
	n=read();
	for(LL i=1;i<=n;i++)a[i]=read();
	build(1,1,n);
	m=read();
	for(LL i=1;i<=m;i++){
		x=read();
		y=read();
		z=query(1,1,n,1,x);
		chg(1,1,n,1,x,z+y);
		printf("%lld\n",z);
	}
	return 0;
}
