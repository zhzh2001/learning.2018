#include <iostream>
#include <cstring>
#include <cstdio>
using namespace std;
int d[100000],e[100000],a[55],f[55];
char c[55][55],ch;
int main()
{
    freopen("rev.in","r",stdin);
    freopen("rev.out","w",stdout);
	int t;
	scanf("%d",&t);
	while(t--)
	{
		int n,ans=0,x;
		scanf("%d%c",&n,&ch);
		for(int i=1;i<=n;i++)
		{
		scanf("%s",c[i]);
		a[i]=strlen(c[i]);
	    }
	    memset(f,0,sizeof(f));
	    ans=0;
	    for(int i=1;i<n;i++)
	    for(int j=i+1;j<=n;j++)
	    {
	    	if(!f[i]&&!f[j]&&a[i]==a[j])
	    	{
	    		if(a[i]&1&&c[i][a[i]-1]!=c[j][a[i]-1])continue;// 奇数位最后一位需相同； 
	    		x=(a[i]|1)-1;//奇数->偶数； 
	    		memset(d,0,sizeof(d));
	    		memset(e,0,sizeof(e));
	    		for(int k=0;k<=x>>1;k++)
	    		{
	    			d[c[i][k<<1]*c[i][k<<1|1]]++;
	    			e[c[j][k<<1]*c[j][k<<1|1]]++;	
				}
				bool flag=0;
				for(int k=0;k<=127*127;k++) //ascii码到128 ;
				{
					if(d[k]!=e[k])
					{
						flag=1;
						break;
					 } 
				}
				if(flag==0)
				{
					ans+=2;
					f[i]=f[j]=1;
				}
			}
		}
		printf("%d",n-ans);
		puts("");
	}
}
