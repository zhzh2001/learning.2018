#include<bits/stdc++.h>
#define L(u) (u<<1)
#define R(u) (u<<1)+1
typedef long long ll;
long long d[4*100003],n,m,a[100003],g[4*100003],w,h,sum[4*100003];
using namespace std;
void pushup(int u){
	sum[u]=max(sum[L(u)],sum[R(u)]);
}
void build(int u,int l,int r){
	if(l==r) {sum[u]=a[l]; return;}
	int mid=(l+r)>>1;
	build(L(u),l,mid);
	build(R(u),mid+1,r);
	pushup(u);
}
void pushdown(int u,int l,int r,int mid){
	d[L(u)]=d[u];
	d[R(u)]=d[u];
	sum[L(u)]=d[u];
	sum[R(u)]=d[u];
	d[u]=0;
}
void update(int u,int l,int r,int ll,int rr,long long v){
	if(l==ll&&r==rr){
		d[u]=max(d[u],v); sum[u]=d[u];
		return;
	}
	int mid=(l+r)>>1;
	if(d[u]) pushdown(u,l,r,mid);
	if(rr<=mid) update(L(u),l,mid,ll,rr,v);
	else if(ll>mid) update(R(u),mid+1,r,ll,rr,v);
	else{
		update(L(u),l,mid,ll,mid,v);
		update(R(u),mid+1,r,mid+1,rr,v);
	}
	pushup(u);
}
long long query(int u,int l,int r,int ll,int rr){
	if(l==ll&&r==rr) return sum[u];
	int mid=(l+r)>>1;
	if(d[u]) pushdown(u,l,r,mid);
	if(rr<=mid) return query(L(u),l,mid,ll,rr);
	else if(ll>mid) return query(R(u),mid+1,r,ll,rr);
	else return max(query(L(u),l,mid,ll,mid),query(R(u),mid+1,r,mid+1,rr));
	pushup(u);
}
int main()
{
	freopen("staircase.in","r",stdin);
	freopen("staircase.out","w",stdout);
	long long n; cin>>n;
	for(int i=1;i<=n;i++) cin>>a[i];
	build(1,1,n);
	cin>>m;
	for(int i=1;i<=m;i++){
		cin>>w>>h;
		long long hh=query(1,1,n,1,w);
	cout<<hh<<endl;
		update(1,1,n,1,w,h+hh);
	}	
}
