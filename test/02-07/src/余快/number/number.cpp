#include <cmath>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#include <queue>
#include <set>
#define ll long long
#define N 1000005
#define inf 1000000005
#define int ll
using namespace std;
ll ans;
ll n,mu[N],vis[N],z[N],tot;
inline int read(){
	char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
	int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
//int nedge,Next[N*2],head[N],to[N*2];
//void add(int a,int b){nedge++;Next[nedge]=head[a];head[a]=nedge;to[nedge]=b;}
int ksm(int x,int k){
	int num=x,sum=1;
	while (k){
		if (k&1){
			if (num>n/sum) return -1;
			sum=sum*num;
		}
		k=k>>1;
		if (k&&num>n/num) return -1;
		if (k) num=num*num;
	}
	return sum;
}
int check(int x){
	int l=1,r=sqrt(n);
	while (l<r){
		int mid=(l+r+1)>>1;
		int t=ksm(mid,x);
		if (t==-1) r=mid-1;
		else l=mid;
	}
	return l;
}
signed main()
{
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	n=read();
	mu[1]=1;
	for (int i=2;i<=70;i++){
		if (!vis[i]){
			mu[i]=-1;
			z[++tot]=i;
		}
		for (int j=1;j<=tot&&z[j]*i<=70;j++){
			vis[z[j]*i]=1;
			if (i%z[j]==0) {mu[i*z[j]]=0;break;}
			else mu[i*z[j]]=-mu[i];
		}
	}
	for (int i=2;i<=64;i++){
		ans-=(check(i)-1)*mu[i];
	}
	ans++;
	printf("%lld",ans);
	return 0;
}
