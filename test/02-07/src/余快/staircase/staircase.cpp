
#include <cmath>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#include <queue>
#include <set>
#define N 1000005
#define ll long long
#define inf 1000000005
#define mod 1000000007
#define int ll
#include <map>
using namespace std;
inline int read(){
    char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
    int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);putchar('\n');}
inline void wri(int x){wr(x);putchar(' ');}
int n,m,a[N],x,y,amax;
signed main(){
	freopen("staircase.in","r",stdin);
	freopen("staircase.out","w",stdout);	
	n=read();for (int i=1;i<=n;i++) a[i]=read(),a[i]=max(a[i-1],a[i]);m=read();
	for (int i=1;i<=m;i++){
		x=read();y=read();amax=max(a[x],a[1]); 
		wrn(amax);a[1]=amax+y;
	}
	return 0;
}
