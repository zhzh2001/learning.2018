#include<bits/stdc++.h>
using namespace std;
unsigned long long maxx,a[1000005]={0};
inline unsigned long long read()
{
    unsigned long long X=0,w=0; char ch=0;
    while(!isdigit(ch)) {w|=ch=='-';ch=getchar();}
    while(isdigit(ch)) X=(X<<3)+(X<<1)+(ch^48),ch=getchar();
    return w?-X:X;
}
inline void write(unsigned long long x)
{
     if(x<0) putchar('-'),x=-x;
     if(x>9) write(x/10);
     putchar(x%10+'0');
} 
signed main()
{
	freopen("staircase.in","r",stdin);
    freopen("staircase.out","w",stdout);
	unsigned long long n,m,w,h,i,j;
	n=read();
	for(i=1;i<=n;i++)
	  a[i]=read(),a[i]=max(a[i-1],a[i]);
	m=read();
	for(i=1;i<=m;i++)
	  {
		 w=read(); h=read();
		 maxx=max(a[w],a[1]);
		 write(maxx); printf("\n");
		 a[1]=maxx+h;
	  }
	return 0;
}
