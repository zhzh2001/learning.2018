#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read()
{
    ll X=0,w=0; char ch=0;
    while(!isdigit(ch)) {w|=ch=='-';ch=getchar();}
    while(isdigit(ch)) X=(X<<3)+(X<<1)+(ch^48),ch=getchar();
    return w?-X:X;
}
inline void write(ll x)
{
     if(x<0) putchar('-'),x=-x;
     if(x>9) write(x/10);
     putchar(x%10+'0');
}
bool ck(ll a,ll b,ll c)
{
	if(b==0) return 1;
	while(c--) a/=b;
	return a>0;
}
ll sqrtn(ll a,ll n)
{
	ll mid,r=a,l=0;
	while(l<r)
	  {
		mid=(l+r+1)>>1;
		if(ck(a,mid,n)) l=mid;
		else r=mid-1;
	  }
	 return l;  
}
ll ans=1,v,w,f[103],n;
int main()
{
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	n=read();
	for(ll i=2;i<=64;i++)
	  {
	  	 w=1-f[i];
	  	 for(ll j=i+i;j<=64;j+=i) f[j]+=w;
	  	 v=sqrtn(n,i);
	  	 if(v>0) ans+=w*(v-1);
	  }
	write(ans);
	return 0;
}
