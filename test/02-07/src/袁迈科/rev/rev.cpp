#include<bits/stdc++.h>
using namespace std;
#define int long long
inline int read()
{
    int X=0,w=0; char ch=0;
    while(!isdigit(ch)) {w|=ch=='-';ch=getchar();}
    while(isdigit(ch)) X=(X<<3)+(X<<1)+(ch^48),ch=getchar();
    return w?-X:X;
}
inline void write(int x)
{
     if(x<0) putchar('-'),x=-x;
     if(x>9) write(x/10);
     putchar(x%10+'0');
}
int t,n,l,a[1000],f[1000][1000],flag[1000],ans;
char s[1000];
signed main()
{
	freopen("rev.in","r",stdin);
	freopen("rev.out","w",stdout);
	t=read();
	while(t--)
	  {
	  	n=read();
	  	for(int i=1;i<=n;i++)
	  	  {
	  	  	scanf("%s",s); l=strlen(s); a[i]=(l+1)/2;
	  	  	for(int j=1;j<=a[i];j++)
	  	  	  {
	  	  	  	if(s[j*2-2]>s[j*2-1]) swap(s[j*2-2],s[j*2-1]);
	  	  	  	f[i][j]=(s[j*2-2]+1)*1000+s[j*2-1];
			  }
			  sort(f[i]+1,f[i]+1+a[i]);
		  }
		  memset(flag,0,sizeof(flag));  ans=n;
		for(int i=2;i<=n;i++)
		  for(int j=1;j<i;j++)
		    if(!flag[j]&&a[i]==a[j])
		      {
		      	bool ff=1;
		      	for(int k=1;k<=a[i];k++)
		      	ff&=f[i][k]==f[j][k];
		      	if(ff) {ans-=2; flag[i]=flag[j]=1; break;}
			  }
		write(ans); printf("\n");	  
	  }
	  return 0;
}

