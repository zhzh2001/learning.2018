#include<iostream>
#include<cstdio>
#include<cmath>
#define ll unsigned long long
using namespace std;

ll n,m1,m2,a,b;
bool vis[10000005];

int main(){
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	ios::sync_with_stdio(false);
	cin>>n;
	m1=sqrt(n);
	if((m1+1)*(m1+1)<=n)m1++;
	m2=int(pow(n,1.00/3.00));
	if((m2+1)*(m2+1)*(m2+1)<=n)m2++;
	a=0;b=0;
	for(ll i=2;i<=m2;i++){
		if(!vis[i]){
			ll j=i;
			while(j<=n/i){
				j*=i;
				if(j<=m2)vis[j]=true;
				++a;
				if(j>m2&&j<=m1)++b;
			}
		}
	}
	cout<<a-b+m1-m2+1;
	return 0;
}
