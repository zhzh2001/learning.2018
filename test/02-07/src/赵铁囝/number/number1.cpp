#include<iostream>
#include<cstdio>
#include<cmath>
#define ll unsigned long long
using namespace std;

inline ll read(){
	ll k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}

inline void write(ll x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(ll x){
	write(x);puts("");
}

ll n,m,ans;
bool vis[1000000005];

int main(){
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	n=read();
	m=int(sqrt(n));
	cout<<m<<endl;
	ans=1;
	for(ll i=2;i<=m;i++){
		if(!vis[i]){
			for(ll j=i*i;j<=n;j*=i){
				if(j<=m)vis[j]=true;
				++ans;
			}
		}
		cout<<i<<'\n';
	}
	cout<<ans;
	return 0;
}
