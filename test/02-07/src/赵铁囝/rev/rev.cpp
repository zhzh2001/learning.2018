#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
string a[10005]; 
bool lg[10000005];
struct liang{
	char a[2];
};
bool cmp(liang aaa,liang bbb)
{
	if(aaa.a[0]==bbb.a[0])
	{
		return aaa.a[1]<bbb.a[1];
	}
	else return aaa.a[0]<bbb.a[0];
}
inline string gb(string b)
{
	string ans="";
	liang ch[10005];
	int ll=b.size()-1;
	int i=0;
	while(i*2<ll)
	{
		ch[i].a[0]=b[i*2];
		ch[i].a[1]=b[i*2+1];
		if(ch[i].a[0]>ch[i].a[1])
		swap(ch[i].a[0],ch[i].a[1]);
		i++;
	}
	sort(ch,ch+i,cmp);
	for(int j=0;j*2<ll;++j) ans=ans+ch[j].a[0]+ch[j].a[1];
	if(ans.size()<b.size()) ans=ans+b[ll];
	return ans;
}
bool cha(string aa,string bb)
{
	if(aa.size()!=bb.size()) return false;
	else 
	{
		int ll=aa.size();
		for(int j=0;j<=ll-1;++j)
		if(aa[j]!=bb[j]) return false;
		return true;
	}
}
int main()
{
	freopen("rev.in","r",stdin);
	freopen("rev.out","w",stdout);
	int t;
	cin>>t;
	while(t--)
	{
		int n,mx;
		memset(lg,true,sizeof(lg));
		cin>>n;
		mx=n;
		for(int i=1;i<=n;++i)
		{
			cin>>a[i];
			a[i]=gb(a[i]); //cout<<a[i]<<endl;
		}
		for(int i=1;i<=n-1;++i)
		for(int j=i+1;j<=n;++j)
		{   //cout<<a[i]<<" "<<a[j]<<endl;
			if(lg[i]&&lg[j]&&cha(a[i],a[j]))
			{    
				lg[i]=false;
				lg[j]=false;
				mx-=2;
				break;
			}
		}
		cout<<mx<<endl;
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
