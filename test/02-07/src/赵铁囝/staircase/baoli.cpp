#include<iostream>
#include<cstdio>
using namespace std;

int n,m,w,ans,h,a[1000000];

int main(){
	freopen("staircase.in","r",stdin);
	freopen("staircase1.out","w",stdout);
	cin>>n;
	for(int i=1;i<=n;i++){
		cin>>a[i];
	}
	cin>>m;
	for(int i=1;i<=m;i++){
		cin>>w>>h;
		ans=0;
		for(int i=1;i<=w;i++){
			ans=max(ans,a[i]);
		}
		cout<<ans<<endl;
		for(int i=1;i<=w;i++){
			a[i]=ans+h;
		}
	}
	return 0;
}
