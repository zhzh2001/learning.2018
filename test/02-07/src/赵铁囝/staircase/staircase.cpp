#include<iostream>
#include<cstdio>
#define ll long long
using namespace std;

inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}

inline void write(ll x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(ll x){
	write(x);puts("");
}

ll n,m,h1,a[1000005],s,h;
bool vis[1000005];

int main(){
	freopen("staircase.in","r",stdin);
	freopen("staircase.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		a[i]=read();
	}
	m=read();
	for(int i=1;i<=m;i++){
		s=read();h=read();
		if(!vis[s]){
			vis[s]=true;
			h1=max(h1,a[s]);
			cout<<h1<<'\n';
			h1+=h;
		}else{
			cout<<h1<<'\n';
			h1+=h;
		}
	}
	return 0;
}		
