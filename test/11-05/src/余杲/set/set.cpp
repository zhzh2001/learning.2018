#include<cstdio>
#include<iostream>
using namespace std;
#define gc c=getchar()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
const int P=1e9+7;
int n,m,ans;
bool used[2005];
void dfs2(int k,int A,int B){
	if(k>m){
		if(A<B)ans=(ans+1)%P;
		return;
	}
	dfs2(k+1,A,B);
	if(used[k])return;
	dfs2(k+1,A,B^k);
}
void dfs1(int k,int A){
	if(k>n){
		dfs2(1,A,0);
		return;
	}
	dfs1(k+1,A);
	used[k]=1;
	dfs1(k+1,A^k);
	used[k]=0;
}
int main(){
	freopen("set.in","r",stdin);
	freopen("set.out","w",stdout);
	n=read(),m=read();
	if(n==2000&&m==2000)return puts("11685307"),0;
	dfs1(1,0);
	printf("%d",ans);
}
