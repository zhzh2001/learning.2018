#include<cstdio>
#include<cstring>
#include<iostream>
using namespace std;
inline char _gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
#define gc c=_gc()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
const int P=998244353;
const int MAXN=1e7+5;
int nedge,head[MAXN];
struct Edge{
	int to,nxt;
}edge[MAXN];
void add(int x,int y){
	edge[++nedge].to=y;
	edge[nedge].nxt=head[x];
	head[x]=nedge;
}
int siz[MAXN];
void dfs(int x){
	siz[x]=1;
	for(int i=head[x];i;i=edge[i].nxt){
		int y=edge[i].to;
		dfs(y);
		siz[x]+=siz[y];
	}
}
int pw(int a,int n){
	int ans=1;
	while(n){
		if(n&1)ans=1LL*ans*a%P;
		a=1LL*a*a%P;
		n>>=1;
	}
	return ans;
}
void Add(int &x,int y){
	x=x+y>=P?x+y-P:x+y;
}
int n,ans,fac[MAXN],inv[MAXN],p[MAXN],f[MAXN];
int C(int n,int m){
	return 1LL*fac[n]*inv[m]%P*inv[n-m]%P;
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	for(int i=2;i<=n;i++)add(read(),i);
	fac[0]=1;
	for(int i=1;i<=n;i++)fac[i]=1LL*fac[i-1]*i%P;
	inv[n]=pw(fac[n],P-2);
	for(int i=n-1;~i;i--)inv[i]=1LL*(i+1)*inv[i+1]%P;
	for(int i=0;i<=n;i++)p[i]=1LL*fac[i]*fac[n-i-1]%P;
	dfs(1);
	memset(f,-1,sizeof(f));
	for(int i=1;i<=n;i++){
		if(f[siz[i]]<0){
			int tmp=0;
			for(int j=0;j<=n-siz[i];j++)
				Add(tmp,1LL*C(n-siz[i],j)*p[j]%P);
			f[siz[i]]=tmp;
		}
		Add(ans,f[siz[i]]);
	}
	printf("%lld",1LL*ans*inv[n]%P);
}
