#include<fstream>
#include<algorithm>
#include<cstring>
using namespace std;
ifstream fin("graph.in");
ofstream fout("graph.out");
const int N=18,MOD=998244353;
int n,m,dp[N][1<<N],f[N][1<<N];
bool mat[N][N];
int dfs(int u,int state)
{
	if(state==(1<<n)-1)
	{
		f[u][state]=1<<u;
		return 1;
	}
	if(~dp[u][state])
		return dp[u][state];
	dp[u][state]=0;
	f[u][state]=1<<u;
	bool flag=false;
	for(int v=0;v<n;v++)
		if(mat[u][v]&&!(state&(1<<v)))
		{
			int mask=state|(1<<v);
			dp[u][state]=(dp[u][state]+1ll*dfs(v,mask)*dfs(u,state|f[v][mask]))%MOD;
			f[u][state]|=f[v][mask];
			flag=true;
		}
	if(!flag)
 		dp[u][state]=1;
	return dp[u][state];
}
int main()
{
	fin>>n>>m;
	while(m--)
	{
		int u,v;
		fin>>u>>v;
		u--;v--;
		mat[u][v]=mat[v][u]=true;
	}
	int ans=0;
	memset(dp,-1,sizeof(dp));
	for(int i=0;i<n;i++)
		ans=(ans+dfs(i,1<<i))%MOD;
	fout<<ans<<endl;
	return 0;
}
