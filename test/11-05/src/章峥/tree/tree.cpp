#include<fstream>
using namespace std;
ifstream fin("tree.in");
ofstream fout("tree.out");
const int N=10000005,MOD=998244353;
int p[N],sz[N],inv[N];
int main()
{
	int n;
	fin>>n;
	for(int i=2;i<=n;i++)
		fin>>p[i];
	for(int i=1;i<=n;i++)
		sz[i]=1;
	for(int i=n;i>1;i--)
		sz[p[i]]+=sz[i];
	inv[1]=1;
	for(int i=2;i<=n;i++)
		inv[i]=1ll*inv[MOD%i]*(MOD-MOD/i)%MOD;
	int ans=0;
	for(int i=1;i<=n;i++)
		ans=(ans+inv[sz[i]])%MOD;
	fout<<ans<<endl;
	return 0;
}
