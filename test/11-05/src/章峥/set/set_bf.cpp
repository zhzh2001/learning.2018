#include<fstream>
using namespace std;
ifstream fin("set.in");
ofstream fout("set.ans");
int main()
{
	int n,m;
	fin>>n>>m;
	int ans=0;
	for(int i=0;i<1<<n;i++)
		for(int j=0;j<1<<m;j++)
			if((i&j)==0)
			{
				int a=0;
				for(int t=0;t<n;t++)
					if(i&(1<<t))
						a^=t+1;
				int b=0;
				for(int t=0;t<m;t++)
					if(j&(1<<t))
						b^=t+1;
				ans+=a<b;
			}
	fout<<ans<<endl;
	return 0;
}
