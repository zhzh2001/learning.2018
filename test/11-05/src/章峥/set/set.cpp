#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("set.in");
ofstream fout("set.out");
const int N=2050,MOD=1e9+7;
int dp[2][N][2];
int main()
{
	int n,m;
	fin>>n>>m;
	int ans=0;
	for(int i=0;i<=10;i++)
	{
		fill_n(&dp[0][0][0],sizeof(dp)/sizeof(int),0);
		dp[0][0][0]=1;
		for(int j=1;j<=n||j<=m;j++)
			for(int k=0;k<2048;k++)
				for(int b=0;b<2;b++)
				{
					dp[j&1][k][b]=dp[(j&1)^1][k][b];
					if(j<=n)
						dp[j&1][k][b]=(dp[j&1][k][b]+dp[(j&1)^1][k^j][b])%MOD;
					if(j<=m)
						dp[j&1][k][b]=(dp[j&1][k][b]+dp[(j&1)^1][k^j][b^((j>>i)&1)])%MOD;
				}
		for(int k=0;k<2048;k++)
			if(31-__builtin_clz(k)==i)
				ans=(ans+dp[max(n,m)&1][k][1])%MOD;
	}
	fout<<ans<<endl;
	return 0;
}
