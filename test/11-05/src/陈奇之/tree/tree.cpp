//我是多么愚蠢，我不能再继续颓废下去
//galgame什么意思都没有，只是浪费时间和精力
//我要继续努力，变得更强
#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
namespace IO{
#define gc getchar
#define pc putchar
char c,st[66];int tp=0,f;
inline ll read(){ll x=0;f=1,c=gc();for(;!isdigit(c);c=gc())if(c=='-')f=-1;for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c&15);return x*f;}
inline void write(ll x){if(!x) pc('0');if(x<0) pc('-'),x=-x;while(x) st[++tp]=x%10+'0',x/=10;while(tp) pc(st[tp--]);}
inline void writeln(ll x){write(x);pc('\n');}
inline void wri(ll x){write(x),pc(' ');}
};using IO::read;using IO::writeln;using IO::write;using IO::wri;
const int maxn = 1e7+233;
const int mod = 998244353;
int fac[maxn],inv[maxn],p[maxn];
int n,cnt;
bool mark[maxn];
inline int A(int n,int m){
	if(n<m) return 0;
	return 1ll * fac[n] * inv[m] % mod * inv[n-m] % mod;
}
inline int qpow(int a,int b){
	int ans = 1;
	for(;b;b>>=1,a=1ll*a*a%mod)
		if(b&1) ans=1ll*ans*a%mod;
	return ans;
}
int sz[maxn];
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n = read();
	inv[0] = inv[1] = 1;
	for(int i=2;i<=n;++i)
		inv[i] = 1ll * inv[mod % i] * (mod - mod / i) % mod;
	Rep(i,2,n) p[i] = read();
	Rep(i,1,n) sz[i] = 1;
	Dep(i,n,2) sz[p[i]]+=sz[i];
	ll ans = 0;
	Dep(i,n,1) ans = (ans + 1ll * n * inv[sz[i]]) % mod;
	writeln(ans * inv[n] % mod);
	return 0;
}
