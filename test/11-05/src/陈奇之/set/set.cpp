#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
namespace IO{
#define gc getchar
#define pc putchar
char c,st[66];int tp=0,f;
inline ll read(){ll x=0;f=1,c=gc();for(;!isdigit(c);c=gc())if(c=='-')f=-1;for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c&15);return x*f;}
inline void write(ll x){if(!x) pc('0');if(x<0) pc('-'),x=-x;while(x) st[++tp]=x%10+'0',x/=10;while(tp) pc(st[tp--]);}
inline void writeln(ll x){write(x);pc('\n');}
inline void wri(ll x){write(x),pc(' ');}
};using IO::read;using IO::writeln;using IO::write;using IO::wri;
const int mod = 1e9+7;
int f[2][2048][2][2];
int n,m,ans;
inline void add(int &x,int v){
	x+=v;
	if(x>=mod) x-=mod;
}
void solve(int bit){
	mem(f[0&1],0);
	f[0&1][0][0][0] = 1;
	rep(i,0,max(n,m)){
		mem(f[(i+1)&1],0);
		rep(j,0,1<<(10-bit)){//1 << (10-bit-1)
			rep(k,0,2)
				rep(l,0,2)if(f[i&1][j][k][l]){
					if(i+1<=n)add(f[(i+1)&1][j^((i+1)>>bit>>1)][k^((i+1)>>bit&1)][l],f[i&1][j][k][l]);
					if(j+1<=m)add(f[(i+1)&1][j^((i+1)>>bit>>1)][k][l^((i+1)>>bit&1)],f[i&1][j][k][l]);
					add(f[(i+1)&1][j][k][l],f[i&1][j][k][l]);
				}
		}
	}
	add(ans,f[max(n,m)&1][0][0][1]);
}
int main(){
	freopen("set.in","r",stdin);
	freopen("set.out","w",stdout);
	n = read(),m = read();
	ans = 0;
	rep(bit,0,11) solve(bit);
	writeln(ans);
	return 0;
}
/*
f[i][j][k]
处理到第i个字母
A=j,B=k的方案数有多少种。
考虑第i个如果给第一个集合 
f[i][j][k] => f[i+1][j^i][k]
如果给第二个集合
f[i][j][k] => f[i+1][j][k^i]
考虑分治？？
处理出前面的
处理出后面的，两者合并？？
f[A][B]  g[C][D]
	转移到
h[A^C][B^D] 
枚举一纬A，一纬C
f[A] * g[C] => h[A^C]
n log^3  
*/

