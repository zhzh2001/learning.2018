//我是多么愚蠢，我不能再继续颓废下去
//动画片什么意思都没有，只是浪费时间和精力
//我要继续努力，变得更强
#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
namespace IO{
#define gc getchar
#define pc putchar
char c,st[66];int tp=0,f;
inline ll read(){ll x=0;f=1,c=gc();for(;!isdigit(c);c=gc())if(c=='-')f=-1;for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c&15);return x*f;}
inline void write(ll x){if(!x) pc('0');if(x<0) pc('-'),x=-x;while(x) st[++tp]=x%10+'0',x/=10;while(tp) pc(st[tp--]);}
inline void writeln(ll x){write(x);pc('\n');}
inline void wri(ll x){write(x),pc(' ');}
};using IO::read;using IO::writeln;using IO::write;using IO::wri;
const int mod = 998244353;
int n,m,f[21][1<<20];
vector<int> edge[23];
inline int gS(int x,int S){
	//不经过S中的点，最大的集合
	int q[23],vis[23];
	mem(vis,false);
	int res = 0,front,rear;
	front = rear = 0;
	q[rear++] = x;
	while(front < rear){
		int u = q[front++];
		res |= 1 << (u-1);
		for(unsigned e=0;e<edge[u].size();++e){
			int v=edge[u][e];
			if(S >> (v-1) & 1) continue;
			if(vis[v]) continue;
			q[rear++] = v;
			vis[v] = true;
		}
	} 
	return res;
}
inline int fac(int x){
	int ans = 1;
	Rep(i,1,x) ans = 1ll * ans * i % mod;
	return ans;
}
int dfs(int i,int S){
//	printf("dfs(%d %d)\n",i,S);
	if(S == (1<<n)-1) return 1;
	map<int,int> vis;
	int b[23],m=0;
	if(f[i][S]!=-1) return f[i][S];
	int &ans = f[i][S];ans = 0;
	for(int j=1;j<=n;++j){
		if(!(S >> (j-1) & 1)){
			vis.clear();
			m = 0;
			for(unsigned e=0;e<edge[j].size();++e){
				int k=edge[j][e];
				if(S >> (k-1) & 1) continue;
				int T = gS(k,S|(1<<(j-1)));
				if(!vis[T]){
					++m;b[m] = 0;
					vis[T] = true;
				}
				(b[m] += dfs(k,((1<<n)-1)^T))%=mod;
			}
			int res = 1;
			Rep(i,1,m) res = 1ll * res * b[i] % mod;
			res = 1ll * res * fac(m) % mod;
			ans = (ans + res) % mod;
		}
	}
	return ans;
}
int main(){
	n = read(),m = read();
	rep(i,0,m){
		int a = read(),b = read();
		edge[a] . push_back(b);
		edge[b] . push_back(a);
	}
	Rep(i,1,n) edge[0].push_back(i);
	mem(f,-1);
	writeln(dfs(0,0));
	return 0;
}
/*
f[i][S]
从i出发，把S集合访问一遍的方案数

g[i][S]
到i结束，已经走了集合S的方案数

考虑g[i][S]的转移

枚举一个点j，满足(i,j),S中不包括j

令T = S | (1 << j) 

根据T，把所有没有访问过的点划分成若干个集合，设为A[i] 
对于所有A[i]，求出B[i] = f[][]之和 
g[j][All ^ A[i]] += g[i][S] * TMS * cnt! *  
*/
