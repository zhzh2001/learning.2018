// ans = \sum (1 / siz)
// 凉凉不会线性推逆元
// 加个记忆化假装能多过几个点的样子 
// 3s假装不虚的样子 
// 然而还是虚到用exgcd 

#include <cstdio>
#include <cctype>
#include <iostream>

using namespace std;

inline char gc()
{
	static const int L = 233333;
	static char sxd[L], *sss = NULL, *ttt = NULL;
	if(sss == ttt)
	{
		ttt = (sss = sxd) + fread(sxd, 1, L, stdin);
		if(sss == ttt)
			return EOF;
	}
	return *sss++;
}

#define dd c = getchar()
template <class T>
inline int read(T& x)
{
	register bool f = (x = 0);
	register char dd;
	for(; !isdigit(c); dd)
	{
		if(c == '-')
			f = true;
		if(c == EOF)
			return -1;
	}
	for(; isdigit(c); dd)
		x = (x << 1) + (x << 3) + (c ^ 48);
	if(f)
		x = -x;
	return 1;
}
#undef dd

typedef long long LL;

const int mod = 998244353;
const int maxn = 10000005;

int siz[maxn];
int fa[maxn];
int inv[maxn];

inline void exgcd(int a, int b, int& x, int& y)
{
	if(!b)
	{
		x = 1, y = 0;
		return;
	}
	exgcd(b, a % b, y, x);
	y -= a / b * x;
}

inline int ny(int X)
{
	int x, y;
	exgcd(X, mod, x, y);
	return y;
}

inline int pow(int a, int b)
{
	if(inv[a])
		return a;
	int ta = a;
	int ans = 1;
	for(; b; b >>= 1, a = (LL) a * a % mod)
		if(b & 1)
			ans = (LL) ans * a % mod;
	return inv[ta] = ans;
}

int main()
{
	freopen("tree.in", "r", stdin);
	freopen("tree.out", "w", stdout);
	int n;
	read(n);
	siz[1] = 1;
	for(int i = 2; i <= n; ++i)
	{
		siz[i] = 1;
		read(fa[i]);
	}
	int ans = 0;
	for(int i = n; i > 1; --i)
	{
		siz[fa[i]] += siz[i];
		(ans += pow(siz[i], mod - 2)) %= mod;
	}
	(ans += pow(siz[1], mod - 2)) %= mod;
//	for(int i = 1; i <= n; ++i)
//		cout << siz[i] << ' ';
//	puts("");
	printf("%d\n", ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

// sxdakking
// zdakqueen
// stdakking
// 奶一口sxd今天AK 
