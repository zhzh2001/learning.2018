// 2^n * n^3
// 1,528,823,808
// 假装能过的样子 
// f[i][j]表示以i为根节点状态为j
// 大力转移
// 信仰总还是要有的，万一卡过去了呢？（逃 

#include <cstring>
#include <bitset>
#include <fstream>
#include <iostream>
#include <algorithm>

using namespace std;

typedef long long LL;

const int maxn = 20;
const int mod = 998244353;

int mmap[maxn][maxn];
int f[maxn][1 << 19];
int n, m;

int all_zt;
int col_zt[maxn];
int col[maxn];
int col_ans[maxn];
int zt;
inline void dfs(int now)
{
	col_zt[col[now]] |= 1 << (now - 1);
	all_zt |= 1 << (now - 1);
	for(int to = 1; to <= n; ++to)
	{
		if((zt & (1 << (to - 1))) && mmap[now][to] && !col[to])
		{
			col[to] = col[now];
			dfs(to);
		}
	}
}

int fac[maxn];

int main()
{
	ifstream fin("graph.in");
	ofstream fout("graph.out");
	fin >> n >> m;
	for(int i = 1, F, T; i <= m; ++i)
	{
		fin >> F >> T;
		mmap[F][T] = mmap[T][F] = 1;
	}
	fac[0] = 1;
	for(int i = 1; i <= n; ++i)
	{
		f[i][1 << (i - 1)] = 1;
		fac[i] = (LL) i * fac[i - 1] % mod;
	}
	for(zt = 0; zt < (1 << n); ++zt)
	{
		for(int now = 1; now <= n; ++now)
		{
			if((zt & (1 << (now - 1))) && (zt ^ (1 << (now - 1))))
			{
//				cout << now << ' ' << bitset<3>(zt) << endl;
				int col_cnt = 0;
				all_zt = 0;
				memset(col_ans, 0, sizeof(col_ans));
				memset(col, 0, sizeof(col));
				memset(col_zt, 0, sizeof(col_zt));
				zt ^= (1 << (now - 1));
				for(int to = 1; to <= n; ++to)
				{
					if(to != now && !col[to] && (1 << (to - 1)) & zt)
					{
						col[to] = ++col_cnt;
						dfs(to);
					}
				}
				if(all_zt != zt)
				{
					zt |= (1 << (now - 1));
					continue;
				}
//				cout << now << ' ' << bitset<3>(zt) << endl;
				for(int to = 1; to <= n; ++to)
					if(to != now && mmap[now][to] && ((1 << (to - 1)) & zt))
						(col_ans[col[to]] += f[to][col_zt[col[to]]]) %= mod;
//				cout << "col_cnt = " << col_cnt << endl;
//				cout << "col_zt[1] = " << bitset<3>(col_zt[1]) << endl;
//				cout << "col_ans[1] = " << col_ans[1] << endl;
				int ans = fac[col_cnt];
				for(int i = 1; i <= col_cnt; ++i)
					ans = (LL) ans * col_ans[i] % mod;
				zt |= (1 << (now - 1));
				f[now][zt] = ans;
			}
		}
	}
//	for(int i = 1; i <= n; ++i)
//		for(int j = 0; j < (1 << n); ++j)
//			cout << i << ' ' << bitset<3>(j) << ' ' << f[i][j] << endl;
	int ans = 0;
	for(int i = 1, tmp = (1 << n) - 1; i <= n; ++i)
		(ans += f[i][tmp]) %= mod;
	fout << ans;
	fin.close();
	fout.close();
	return 0;
}

// sxdakking
// zdakqueen
// stdakking
// 奶一口sxd今天AK 
