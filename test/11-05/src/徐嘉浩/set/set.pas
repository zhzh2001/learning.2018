var i,j:longint; n,m,ans,p:int64;
    c:array[0..2000,0..2000]of int64;
begin
 assign(input,'set.in');
 assign(output,'set.out');
 reset(input);
 rewrite(output);
 p:=1000000007;
 read(n,m);
 if m>n then begin n:=n+m; m:=n-m; n:=n-m; end;
 for i:=0 to 2000 do c[0,i]:=1;
 for i:=1 to 2000 do
  for j:=i to 2000 do c[i,j]:=(c[i,j-1]+c[i-1,j-1])mod p;
 for i:=0 to n do
  for j:=i to m-i do
   if(i<>0)or(j<>0) then ans:=(ans+c[j,m-i])mod p;
 writeln(ans);
 close(input);
 close(output);
end.