var n,m,i,x,y,t:longint; ans:int64;
    a:array[0..100,0..100]of boolean;
    b:array[0..100]of boolean;
    c,d:array[0..100]of longint;
function pd():boolean;
var i,t:longint;
begin
 t:=1; d[1]:=c[1];
 if not a[c[1],c[2]] then exit(false);
 inc(t); d[t]:=c[2];
 for i:=3 to n do
  if a[c[i],d[t]] then
  begin
   inc(t);
   d[t]:=c[3];
  end
  else if t>1 then dec(t)
  else exit(false);
 exit(true);
end;
procedure dfs(k:longint);
var i:longint;
begin
 if k=n+1 then
 begin
  if pd then inc(ans);
  exit;
 end;
 for i:=1 to n do
  if not b[i] then
  begin
   b[i]:=true;
   c[k]:=i;
   dfs(k+1);
   b[i]:=false;
  end;
end;
begin
 assign(input,'graph.in');
 assign(output,'graph.out');
 reset(input);
 rewrite(output);
 read(n,m);
 for i:=1 to m do
 begin
  read(x,y);
  a[x,y]:=true;
  a[y,x]:=true;
 end;
 dfs(1);
 writeln(ans);
 close(input);
 close(output);
end.