#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 1e7+10, mod = 998244353; 
int n,ans,fa[N],size[N],inv[N];
inline int power(int a,int b){
	int ans=1;
	for (;b;b>>=1,a=1ll*a*a%mod) if (b&1) ans=1ll*ans*a%mod;
	return ans;
}
namespace Subtask1{
	const int maxn = 1<<20;
	int ans,f[21],b[maxn];
	inline void dfs(int now,int sum,int Sum){
	//	printf("%d %d %d\n",now,sum,Sum);
		if (now==((1<<n)-1)){ans=(ans+1ll*sum*power(Sum,mod-2)%mod)%mod;return;}
		For(i,1,n) if (!(now&(1<<i-1))) dfs(now|f[i],sum+1,Sum*b[now]);
	}
	inline void Main(){
		For(i,1,n) f[i]=1;
		For(i,2,n)
			for (int now=i;now!=1;now=fa[now]) f[i]+=(1<<now-1);
		For(i,0,(1<<n)-1) For(j,1,n) if (!(i&(1<<j-1))) b[i]++;
		dfs(0,0,1);printf("%d",ans); 
	}
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	For(i,2,n) fa[i]=read();
	//if (n<=20) return Subtask1::Main(),0;
	For(i,1,n) size[i]=1;
	Dow(i,n,2) size[fa[i]]+=size[i];
	inv[1]=1;For(i,2,n) inv[i]=1ll*inv[mod%i]%mod*(mod-mod/i)%mod;
	For(i,1,n) ans=(ans+inv[size[i]]/*power(size[i],mod-2)*/)%mod;
	printf("%d\n",ans);
}
