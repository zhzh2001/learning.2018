#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 19, maxn = 1<<18, mod = 998244353;
int n,m,x,y,E[N];
bool e[N][N];
inline void Mod(int &x){if (x>=mod) x-=mod;}
namespace Subtask1{
	int ans,a[N];
	bool vis[N];
	inline void dfs(int k,int last,int now){
		if (k>n){Mod(++ans);return;}//printf("%d %d %d\n",k,a[last],now);
		while ((E[a[last]]&now)==E[a[last]]) --last;
		For(i,1,n)
			if (!vis[i]&&e[a[last]][i]){
				vis[i]=1;a[k]=i;
				dfs(k+1,k,now+(1<<i-1));
				vis[i]=0;
			}
	}
	inline void Main(){
		For(i,1,n){
			vis[i]=1,a[1]=i;
			dfs(2,1,1<<(i-1));
			vis[i]=0;
		}
		printf("%d\n",ans);
	}
}
int main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read(),m=read();
	For(i,1,m) x=read(),y=read(),e[x][y]=e[y][x]=1;
	For(i,1,n) For(j,1,n) if (e[i][j]) E[i]+=(1<<j-1);
	Subtask1::Main();
}
