#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int mod = 1e9+7;
int n,m;
namespace Subtask1{
	int ans;
	bool vis[41];
	inline void Dfs(int k,int sum,int Sum){
		if (k>n){ans+=(sum<Sum);return;}
		Dfs(k+1,sum,Sum);
		if (!vis[k]) Dfs(k+1,sum^k,Sum);
	}
	inline void dfs(int k,int sum){
		if (k>m){Dfs(1,0,sum);return;}
		vis[k]=0,dfs(k+1,sum);
		vis[k]=1,dfs(k+1,sum^k);
	}
	inline void Main(){
		dfs(1,0);
		printf("%d\n",ans%mod);
	}
}
int main(){
	freopen("set.in","r",stdin);
	freopen("set.out","w",stdout);
	n=read(),m=read();
	if (n<=10&&m<=10) return Subtask1::Main(),0;
}
