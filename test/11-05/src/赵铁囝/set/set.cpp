#include <bits/stdc++.h>

#define Max 6005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

const int wzp=1e9+7;

int n,m,ans,maxn,f[2][Max][Max];

int main(){
	freopen("set.in","r",stdin);
	freopen("set.out","w",stdout);
	n=read();m=read();maxn=max(n,m);
	f[0][0][0]=1;
	for(int i=1;i<=maxn;i++){
		for(int j=0;j<=maxn*2;j++){
			for(int k=0;k<=maxn*2;k++){
				f[i&1][j][k]=0;
			}
		}
		for(int j=0;j<=maxn*2;j++){
			for(int k=0;k<=maxn*2;k++){
				f[i&1][j][k]=f[(i+1)&1][j][k];
				if(i<=n){
					f[i&1][j][k]=(f[i&1][j][k]+f[(i+1)&1][j^i][k])%wzp;
				}
				if(i<=m){
					f[i&1][j][k]=(f[i&1][j][k]+f[(i+1)&1][j][k^i])%wzp;
				}
//				cout<<i<<" "<<j<<" "<<k<<" "<<f[i][j][k]<<endl;
			}
		}
	}
	for(int j=0;j<=maxn*2;j++){
		for(int k=j+1;k<=maxn*2;k++){
			ans=(ans+f[n&1][j][k])%wzp;
		}
	}
	writeln(ans);
	return 0;
}
