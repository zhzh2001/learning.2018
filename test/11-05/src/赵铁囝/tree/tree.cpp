#include <bits/stdc++.h>

#define ll long long
#define Max 10000005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(ll x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(ll x){
	write(x);puts("");
}

const ll wzp=998244353;

struct Edge{
	ll v,to;
}e[Max];

ll n,size,f[Max],son[Max],head[Max];

inline void add(ll u,ll v){
	e[++size].v=v;e[size].to=head[u];head[u]=size;
}

inline ll pow(ll x,ll y){
	ll ans=1;
	while(y){
		if(y&1)ans=ans*x%wzp;
		x=x*x%wzp;
		y>>=1;
	}
	return ans;
}

inline void dfs(ll u){
	son[u]=1;
	for(ll i=head[u];i;i=e[i].to){
		ll v=e[i].v;
		dfs(v);
		son[u]+=son[v];
		f[u]=(f[u]+f[v])%wzp;
	}
	f[u]=(f[u]+pow(son[u],wzp-2))%wzp;
	return;
}

int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	for(ll i=2;i<=n;i++)add(read(),i);
	dfs(1);
	writeln(f[1]);
	return 0;
}
