#include <bits/stdc++.h>

#define Max 20

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

const int wzp=998244353;

struct Edge{
	int v,to;
}e[Max*2];

int n,m,l,r,size,ans,son[Max],deg[Max],now[Max],head[Max],a[Max];
bool vis[Max];

inline void add(int u,int v){
	e[++size].v=v;e[size].to=head[u];head[u]=size;
}

inline bool check(){
	for(int i=1;i<=n;i++)now[i]=deg[i];
	for(int i=2;i<=n;i++){
		bool flag=false;
		for(int j=head[a[i-1]];j;j=e[j].to){
			int v=e[j].v;
			if(v==a[i]){
				flag=true;
				now[j]--;
				now[a[i]]--;
				break;
			}
		}
		if(!flag){
			for(int j=i-1;j>=1;j--){
				if(now[j]){
					for(int k=head[j];k;k=e[k].to){
						int v=e[k].v;
						if(v==a[i]){
							now[a[i]]--;
							now[j]--;
							flag=true;
							break;
						}
					}
					if(!flag)return false;
				}
			}
		}
	}
	return true;
}

inline void dfs(int x){
	if(x==n+1){
		if(check())ans++;
	}
	for(int i=1;i<=n;i++){
		if(!vis[i]){
			a[x]=i;
			vis[i]=true;
			dfs(x+1);
			vis[i]=false;
		}
	}
}

int main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=m;i++){
		l=read();r=read();
		add(l,r);
		add(r,l);
		deg[l]++;deg[r]++;
	}
	dfs(1);
	writeln(ans);
	return 0;
} 
