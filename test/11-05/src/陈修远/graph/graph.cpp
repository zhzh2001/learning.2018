#include <bits/stdc++.h>
#define ll long long

using namespace std;

int n,m;
bool d[20][20];
bool vis[20];
int last[20];
ll ans=0;
int MOD=998244353;

void dfs(int x,bool re)
{
	bool flag1=1,flag2=1;
	vis[x]=1;
	for(int i=1;i<=n;++i)
	{
		if(!vis[i])
		{
			flag1=0;
			if(d[i][x])
			{
				flag2=0;
				last[i]=x;
				dfs(i,0);
			}
		}
	}
	if(flag1) ans=(ans+1)%MOD;
	else if(flag2)
	{
		dfs(last[x],1);
	}
	if(!re)vis[x]=0;
}

int main()
{
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	cin>>n>>m;
	int a,b;
	for(int i=1;i<=m;++i)
	{
		cin>>a>>b;
		d[a][b]=d[b][a]=1;
	}
	for(int i=1;i<=n;++i) dfs(i,0);
	cout<<ans<<endl;
	return 0;
}