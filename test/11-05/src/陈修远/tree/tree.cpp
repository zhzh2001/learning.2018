#include <bits/stdc++.h>

#define ll long long

using namespace std;

int f[10000007];
int v[10000007];
int tot[10000007];
int ch[10000007];
bool leaf[10000007];
int r=0;
int MOD=998244353;
ll ni[10000007];

void qiuni(int n)
{
	ni[1]=1;
	for(int i=2;i<=n;++i)
	{
		ni[i]=(MOD-MOD/i)*ni[MOD%i]%MOD;
	}
}

void draw(int x)
{
	v[f[x]]=(v[f[x]]+v[x])%MOD;
	tot[f[x]]+=tot[x];
	ch[f[x]]--;
	if(ch[f[x]]==0)
	{
		v[f[x]]=(v[f[x]]+ni[tot[f[x]]])%MOD;
		draw(f[x]);
	}
}

int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	int t;
	int n;
	ios::sync_with_stdio(0);
	cin>>n;
	qiuni(n);
	for(int i=1;i<=n;++i)
		leaf[i]=1,tot[i]=1;
	for(int i=2;i<=n;++i)
	{
		cin>>t;
		f[i]=t;
		ch[t]++;
		leaf[t]=0;
	}
	for(int i=1;i<=n;++i)
	{
		if(leaf[i]) v[i]=1,draw(i);
	}
	cout<<v[1];
	return 0;
}
//212,370,440,130,137,957