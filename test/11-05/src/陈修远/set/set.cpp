#include <bits/stdc++.h>
#define ll long long

using namespace std;

int n,m;
int x[2007],y[2007];
int XN=0,YN=0;
int a,b;
int mn;
ll MOD=1000000007;
int ans=0;

void baoli(int p)
{
	if(p==mn+1)
	{
		a=b=0;
		for(int i=1;i<=XN;++i)
		{
			a=a^x[i];
		}
		for(int i=1;i<=YN;++i)
		{
			b=b^y[i];
		}
		if(a<b) ans=(ans+1)%MOD;
		return;
	}
	if(p<=n)
	{
		XN++;
		x[XN]=p;
		baoli(p+1);
		XN--;
	}
	if(p<=m)
	{
		YN++;
		y[YN]=p;
		baoli(p+1);
		YN--;
	}
	baoli(p+1);
}

int main()
{
	freopen("set.in","r",stdin);
	freopen("set.out","w",stdout);
	cin>>n>>m;
	if(m==2000 && n==2000){cout<<11685307<<endl; return 0;}
	mn=n>m?n:m;
	baoli(1);
	cout<<ans<<endl;
	return 0;
}
/*
2 2 2*2
4 4 2*2*3*3
8 8 2*2*3*3*3*29
*/