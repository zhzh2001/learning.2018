#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 20
// #define int long long
using namespace std;
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);putchar(x%10+'0');}
int read(){int d=0,w=1;char c=getchar();for(;c<'0'||c>'9';c=getchar())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=getchar())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
int n,m,xxxor,vvis[maxn],vis[maxn],ans;
void dfs2(int k)
{
	if(k>m)
	{
		int xxor=0;
		for(int i=1;i<=m;i++)
			if(vvis[i]) xxor^=i;
		if(xxxor<xxor) ans++;
		return;
	}
	vvis[k]=0;
	dfs2(k+1);
	if(!vis[k])
	{
		vvis[k]=1;
		dfs2(k+1);
	}
}
void dfs1(int k)
{
	if(k>n)
	{
		xxxor=0;
		for(int i=1;i<=n;i++)
			if(vis[i]) xxxor^=i;
		dfs2(1);
		return;
	}
	vis[k]=0;
	dfs1(k+1);
	vis[k]=1;
	dfs1(k+1);
}
signed main()
{
	freopen("set.in","r",stdin);
	freopen("set.out","w",stdout);
	n=read();
	m=read();
	dfs1(1);
	write(ans);
	return 0;
}
