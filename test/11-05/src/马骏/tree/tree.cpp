#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 10000010
#define MOD 998244353
// #define int long long
using namespace std;
inline char gc()
{
	static char buf[100000],*p1=buf+100000,*pend=buf+100000;
	if(p1==pend)
	{
		p1=buf;
		pend=buf+fread(buf,1,100000,stdin);
	}
	return *p1++;
}
inline void write(int x){if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);putchar(x%10+'0');}
inline int read(){int d=0,w=1;char c=gc();for(;c<'0'||c>'9';c=gc())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=gc())d=(d<<1)+(d<<3)+c-48;return d*w;}
inline void wln(int x){write(x);putchar('\n');}
inline void wrs(int x){write(x);putchar(' ');}
int siz[maxn],n,ans,ni[maxn];
vector<int> b[maxn];
inline void init(int n,int mod)
{
	ni[0]=0;
	ni[1]=1;
	for(int i=2;i<=n;i++)
		ni[i]=(-((long long)mod/i)*(long long)ni[mod%i]%mod+mod)%mod;
}
inline void dfs(int k)
{
	siz[k]=1;
	for(int i=0;i<b[k].size();i++)
	{
		dfs(b[k][i]);
		siz[k]+=siz[b[k][i]];
	}
}
inline int lk(int a,int b)
{
	if(a+b>MOD) return a+b-MOD;
		else return a+b;
}
signed main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	for(int i=2;i<=n;i++)
		b[read()].push_back(i);
	init(n,MOD);
	dfs(1);
	for(int i=1;i<=n;i++)
		ans=lk(ans,ni[siz[i]]);
	write(ans);
	return 0;
}
