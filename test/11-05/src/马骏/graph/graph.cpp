#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 20
#define maxm 160
#define maxbin 300000
// #define int long long
using namespace std;
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);putchar(x%10+'0');}
int read(){int d=0,w=1;char c=getchar();for(;c<'0'||c>'9';c=getchar())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=getchar())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
int n,m,ans,vis[maxn],last[maxn];
vector<int> a[maxn];
void dfs(int k)
{
	// wln(k);
	// for(int i=1;i<=n;i++)
	// 	wrs(vis[i]);
	// putchar('\n');
	int cnt=0;
	for(int i=1;i<=n;i++)
		cnt+=vis[i];
	if(cnt==n)
	{
		ans++;
		return;
	}
	// queue<int> q;
	for(int i=0;i<a[k].size();i++)
		if(!vis[a[k][i]])
		{
			// q.push(a[k][i]);
			last[a[k][i]]=k;
			vis[a[k][i]]=1;
			dfs(a[k][i]);
			vis[a[k][i]]=0;
		}
	cnt=0;
	for(int i=1;i<=n;i++)
		cnt+=vis[i];
	if(last[k]!=0&&cnt!=n) dfs(last[k]);
	// for(;!q.empty();q.pop())
	// 	vis[q.front()]=0;
}
signed main()
{
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read();
	m=read();
	for(int i=1;i<=m;i++)
	{
		int x=read(),y=read();
		a[x].push_back(y);
		a[y].push_back(x);
	}
	for(int i=1;i<=n;i++)
	{
		memset(vis,0,sizeof vis);
		vis[i]=1;
		last[i]=0;
		dfs(i);
	}
	write(ans);
	return 0;
}
