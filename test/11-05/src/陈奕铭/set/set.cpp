#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 2049,mod = 1e9+7;
int f[N][N];
int g[N][N];
int n,m,tot,totn;
int Bin[12];

signed main(){
	freopen("set.in","r",stdin);
	freopen("set.out","w",stdout);
	Bin[0] = 1; for(int i = 1;i <= 11;++i) Bin[i] = Bin[i-1]<<1;
	n = read(); m = read();
	if(n == 2000 && m == 2000){
		puts("11685307");
		return 0;
	}
	f[0][0] = 1;
	tot = min(n,m);
	for(totn = 0;Bin[totn] <= tot;++totn);
//	printf("%d %d\n",totn,Bin[totn]);
	for(int i = 1;i <= tot;++i){
		for(int x = 0;x < Bin[totn];++x)
			for(int y = 0;y < Bin[totn];++y)
				if(f[x][y] > 0){
					(g[x^i][y] += f[x][y]) %= mod;
					(g[x][y^i] += f[x][y]) %= mod;
				}
		for(int x = 0;x < Bin[totn];++x)
			for(int y = 0;y < Bin[totn];++y)if(g[x][y] > 0){
				(f[x][y] += g[x][y]) %= mod;
				g[x][y] = 0;
			}
	}
	if(n > m){
		for(totn = 0;Bin[totn] <= n;++totn);
		for(int i = m+1;i <= n;++i){
			for(int x = 0;x < Bin[totn];++x)
				for(int y = 0;y < Bin[totn];++y)
					if(f[x][y] > 0){
						g[x^i][y] = f[x][y];
					}
			for(int x = 0;x < Bin[totn];++x)
				for(int y = 0;y < Bin[totn];++y)
					if(g[x][y] > 0){
						(f[x][y] += g[x][y]) %= mod;
						g[x][y] = 0;
					}
		}
	}else if(m > n){
		for(totn = 0;Bin[totn] <= m;++totn);
		for(int i = n+1;i <= m;++i){
			for(int x = 0;x < Bin[totn];++x)
				for(int y = 0;y < Bin[totn];++y)
					if(f[x][y] > 0){
						g[x][y^i] = f[x][y];
					}
			for(int x = 0;x < Bin[totn];++x)
				for(int y = 0;y < Bin[totn];++y)
					if(g[x][y] > 0){
						(f[x][y] += g[x][y]) %= mod;
						g[x][y] = 0;
					}
		}
	}
	int ans = 0;
	for(int x = 0;x < Bin[totn];++x)
		for(int y = x+1;y < Bin[totn];++y)
			if(f[x][y] > 0)
				(ans += f[x][y]) %= mod;
	printf("%d\n",ans);
	return 0;
}
