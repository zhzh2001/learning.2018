#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 10000005,mod = 998244353;
int n;
int sum[N],pre[N];
int size[N],fa[N];
int ans;

inline int qpow(int x,int n){
	int ans = 1;
	while(n){
		if(n&1) ans = 1LL*ans*x%mod;
		x = 1LL*x*x%mod;
		n >>= 1;
	}
	return ans;
}

inline ll C(int n,int m){
	return (1LL*sum[n]*pre[m])%mod*pre[n-m]%mod;
}

signed main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n = read(); size[1] = 1;
	sum[0] = 1; for(int i = 1;i <= n;++i) sum[i] = 1LL*sum[i-1]*i%mod;
	pre[n] = qpow(sum[n],mod-2); for(int i = n;i >= 1;--i) pre[i-1] = 1LL*pre[i]*i%mod;
	for(int i = 2;i <= n;++i)
		fa[i] = read(),size[i] = 1;
	for(int i = n;i >= 2;--i)
		size[fa[i]] += size[i];
	for(int i = 1;i <= n;++i){
//		printf("%d %d ",size[i],pre[i]);
//		printf("%lld %lld %lld\n",C(n,size[i]));
		ans = (ans+(1LL*C(n,size[i])*sum[n-size[i]]%mod)*sum[size[i]-1]%mod)%mod;
	}
	printf("%lld\n",(1LL*ans*pre[n]%mod+mod)%mod);
	return 0;
}
