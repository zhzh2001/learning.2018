#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

#define lowbit(x) ((x)&(-x))

const int N = 19,M = 262145,mod = 998244353;
int n,m;
int mp[N][N];
int To[N][M],Nxt[N][M];
int f[N][M];
int gantan[N];
int Bin[N];
int wei[M];
int que[25],head,tail;
bool vis[N];

signed main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	wei[1] = 1; for(int i = 2;i < M;++i) wei[i] = wei[i>>1]+1;
	gantan[0] = 1; for(int i = 1;i <= 18;++i) gantan[i] = (1LL*gantan[i-1]*i)%mod;
	Bin[0] = 1; for(int i = 1;i <= 18;++i) Bin[i] = (Bin[i-1]<<1) %mod;
	n = read(); m = read();
	for(int i = 1;i <= m;++i){
		int x = read(),y = read();
		mp[x][y] = mp[y][x] = 1;
	}
	
	for(int k = 0;k < Bin[n];++k)
		for(int i = 1;i <= n;++i){
			for(int j = 1;j <= n;++j)
				if(i != j && (k&(Bin[j-1])) > 0 && mp[i][j])
					Nxt[i][k] |= Bin[j-1];
			if(k&(Bin[i-1])){
				To[i][k] = Bin[i-1];
				head = 0;
				que[tail = 1] = i;
				vis[i] = true;
				while(head < tail){
					int x = que[head+1];
					for(int j = 1;j <= n;++j)
						if((k&(Bin[j-1])) > 0 && mp[x][j] && !vis[j]){
							que[++tail] = j;
							vis[j] = true;
							To[i][k] |= Bin[j-1];
						}
					++head;
				}
				for(int j = 1;j <= n;++j) vis[j] = false;

			}
		}
//	for(int i = 1;i <= n;++i,puts(""))
//		for(int j = 0;j < Bin[n];++j)
//			printf("%d ",To[i][j]);
	for(int i = 1;i <= n;++i) f[i][Bin[i-1]] = 1;
	for(int k = 1;k < Bin[n];++k)
		for(int I = k,i = wei[lowbit(I)];I;I -= lowbit(I),i = wei[lowbit(I)]){
//			printf("# %d %d \n",k,lowbit(I));
			if(f[i][k] == 0){
				int num = 0,sum = 1,tpa = (k^Bin[i-1]);
				for(int J = Nxt[i][tpa],j = wei[lowbit(J)];J;J -= lowbit(J),j = wei[lowbit(J)]){
//					printf("# %d %d %d\n",k,i,lowbit(J));
					if(mp[i][j]){
//						printf("# %d %d %d %d %d ",k,i,j,f[j][To[j][tpa]],To[j][tpa]);
						if((tpa&(Bin[j-1]))){	//加入一个新的集合 
							int Sum = 0,TT = To[j][tpa];
							for(int P = Nxt[i][TT],p = wei[lowbit(P)];P;P -= lowbit(P),p = wei[lowbit(P)])
								if(mp[i][p]){
									(Sum += f[p][TT]) %= mod;
								}
							sum = 1LL * sum * Sum % mod;
							++num; tpa ^= TT;
//							printf("%d %d",num,To[j][tpa]);
						}
//						puts("");
					}
				}
				f[i][k] = 1LL*gantan[num]*sum % mod;
//				printf("@ %d %d %d \n",k,i,f[i][k]);
			}
		}
	int ans = 0;
	for(int i = 1;i <= n;++i)
		ans = (ans+f[i][Bin[n]-1])%mod;
	printf("%d\n",ans);
	return 0;
}
