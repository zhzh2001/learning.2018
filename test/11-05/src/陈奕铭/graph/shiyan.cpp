#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

#define lowbit(x) ((x)&(-x))
int n;

signed main(){
	n = 17;
//	printf("@ %d\n",lowbit(n));
	for(int i = n;i;i -= lowbit(i))
		printf("%d\n",lowbit(i));
	return 0;
}
