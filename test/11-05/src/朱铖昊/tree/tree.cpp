#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int mod=998244353;
const int N=10000005;
int n,x,ans,f[N],si[N];
int la[N],to[N],pr[N],cnt;
inline void add(int x,int y)
{
	to[++cnt]=y;
	pr[cnt]=la[x];
	la[x]=cnt;
}
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline int ksm(int x,int y)
{
	int ans=1;
	for (;y;y/=2,x=(ll)x*x%mod)
		if (y&1)
			ans=(ll)ans*x%mod;
	return ans;
}
inline void dfs(int x)
{
	si[x]=1;
	for (int i=la[x];i;i=pr[i])
	{
		dfs(to[i]);
		si[x]+=si[to[i]];
	}
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	// for (int i=1;i<=10;++i)
	// 	cout<<ksm(i,mod-2)<<' ';
	read(n);
	f[1]=1;
	for (int i=2;i<=n;++i)
		f[i]=(ll)f[mod%i]*(mod-mod/i)%mod;
	for (int i=2;i<=n;++i)
	{
		read(x);
		add(x,i);
	}
	dfs(1);
	ans=0;
	for (int i=1;i<=n;++i)
		(ans+=f[si[i]])%=mod;
	cout<<ans;
	return 0;
}	