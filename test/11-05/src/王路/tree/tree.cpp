#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <limits>
#include <functional>
#include <utility>
#include <numeric>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

namespace IO {
  static const int kMegaBytes = 1 << 20;
  char buf[kMegaBytes], *S = buf, *T = buf;
  bool isneg;
  inline bool Fetch() { return T = (S = buf) + fread(buf, 1, kMegaBytes, stdin), S != T; }
  inline char NC() { return S == T ? (Fetch() ? *S++ : EOF) : *S++; }
  template <typename Int> inline void RI(Int &x) {
    x = isneg = 0;
    char ch;
    for (ch = NC(); isspace(ch); ch = NC())
      ;
    if (ch == '-')
      isneg = 1, ch = NC();
    for (; isdigit(ch); ch = NC())
      x = x * 10 + ch - '0';
    if (isneg) x = -x;
  }
}

using IO::RI;

const int kMod = 998244353;
const int kMaxN = 2e7 + 5;
int n, p[kMaxN];
int fact[kMaxN], rfact[kMaxN], sz[kMaxN];
ll fracd;

ll QuickPow(ll x, ll y) {
  x %= kMod;
  ll ret = 1;
  for (; y; y >>= 1, x = x * x % kMod) {
    if (y & 1) {
      ret = ret * x % kMod;
    }
  }
  return ret;
}

inline ll Combin(int n, int m) {
  if (n < m || m < 0) {
    return 0;
  }
  return 1LL * fact[n] * rfact[m] % kMod * rfact[n - m] % kMod;
}

int main() {
  freopen("tree.in", "r", stdin);
  freopen("tree.out", "w", stdout);
  RI(n);
  for (int i = 2; i <= n; ++i) {
    RI(p[i]);
  }
  for (int i = n; i >= 1; --i) {
    sz[i] += 1;
    sz[p[i]] += sz[i];
  }
  fact[0] = 1;
  for (int i = 1; i <= n * 2; ++i) {
    fact[i] = (1LL * fact[i - 1] * i) % kMod;
  }
  rfact[n] = fracd = QuickPow(fact[n], kMod - 2);
  for (int i = n; i >= 1; --i) {
    rfact[i - 1] = (1LL * rfact[i] * i) % kMod;
  }
  ll ans = 0;
  for (int i = 1; i <= n; ++i) {
    ans = (ans + 1LL * Combin(n, n - sz[i]) * fact[sz[i] - 1] % kMod * (sz[i] - 1) % kMod) % kMod;
  }
  printf("%lld\n", ((1LL * fact[n] * n % kMod - ans) % kMod + kMod) % kMod * fracd % kMod);
  return 0;
}
