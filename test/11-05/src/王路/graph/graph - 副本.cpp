#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <limits>
#include <functional>
#include <utility>
#include <numeric>
#include <queue>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

namespace IO {
  static const int kMegaBytes = 1 << 20;
  char buf[kMegaBytes], *S = buf, *T = buf;
  bool isneg;
  inline bool Fetch() { return T = (S = buf) + fread(buf, 1, kMegaBytes, stdin), S != T; }
  inline char NC() { return S == T ? (Fetch() ? *S++ : EOF) : *S++; }
  template <typename Int> inline void RI(Int &x) {
    x = isneg = 0;
    char ch;
    for (ch = NC(); isspace(ch); ch = NC())
      ;
    if (ch == '-')
      isneg = 1, ch = NC();
    for (; isdigit(ch); ch = NC())
      x = x * 10 + ch - '0';
    if (isneg) x = -x;
  }
}

using IO::RI;

const int kMod = 998244353, kMaxN = 20;
int f[1 << kMaxN][kMaxN], mat[kMaxN][kMaxN];
int head[kMaxN], ecnt, n, m;
struct Edge { int to, nxt; } e[kMaxN * kMaxN];

inline void Insert(int x, int y) {
  e[++ecnt] = (Edge) { y, head[x] };
  head[x] = ecnt;
}

int ans = 0, dep[kMaxN], ins[kMaxN], id[kMaxN];
bool vis[kMaxN], flag = true;
int stk[kMaxN];

int main() {
  freopen("graph.in", "r", stdin);
  freopen("graph.out", "w", stdout);
  RI(n), RI(m);
  for (int i = 1, a, b; i <= m; ++i) {
    RI(a), RI(b);
    --a, --b;
    Insert(a, b);
    Insert(b, a);
    mat[a][b] = mat[b][a] = 1;
  }
  if (n <= 10) {
    for (int i = 0; i < n; ++i) {
      id[i] = i;
    }
    ans = 0;
    do {
      memset(vis, 0x00, sizeof vis);
      flag = true;
      int now = 0, isdone = 0;
      for (int i = 0; i < n - 1; ++i) {
        stk[++now] = id[i];
        vis[id[i]] = true;
        while (!mat[stk[now]][id[i + 1]] && now > 0) {
          bool isdone = true;
          for (int j = 0; j < n; ++j) {
            if (mat[stk[now]][j] && !vis[j]) {
              isdone = false;
            }
          }
          if (!isdone)
            break;
          --now;
        }
        if (!mat[stk[now]][id[i + 1]] || now == 0) {
          flag = false;
          break;
        }
      }
      ans += flag;
    } while (next_permutation(id, id + n));
    printf("%d\n", ans);
    return 0;
  }
  printf("%d\n", ans);
  return 0;
}
