#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <limits>
#include <functional>
#include <utility>
#include <numeric>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

namespace IO {
  static const int kMegaBytes = 1 << 20;
  char buf[kMegaBytes], *S = buf, *T = buf;
  bool isneg;
  inline bool Fetch() { return T = (S = buf) + fread(buf, 1, kMegaBytes, stdin), S != T; }
  inline char NC() { return S == T ? (Fetch() ? *S++ : EOF) : *S++; }
  template <typename Int> inline void RI(Int &x) {
    x = isneg = 0;
    char ch;
    for (ch = NC(); isspace(ch); ch = NC())
      ;
    if (ch == '-')
      isneg = 1, ch = NC();
    for (; isdigit(ch); ch = NC())
      x = x * 10 + ch - '0';
    if (isneg) x = -x;
  }
}

using IO::RI;
const int kMod = 1e9 + 7;
const int kMaxN = 277;

int n, m, f[2][kMaxN][kMaxN];

inline void Update(int &x, int y) {
  x = x + y;
  if (x > kMod) {
    x -= kMod;
  }
}

int main() {
  freopen("set.in", "r", stdin);
  freopen("set.out", "w", stdout);
  RI(n), RI(m);
  if (n < m) {
    swap(n, m);
  }
  // 60 pts ??? O(n ^ 3)
  f[0][0][0] = 1;
  int lim = 256;
  for (int i = 1; i <= m; ++i) {
    int cur = i & 1;
    memset(f[cur], 0x00, sizeof f[cur]);
    for (int j = 0; j < lim; ++j) {
      for (int k = 0; k < lim; ++k) {
        Update(f[cur][j ^ i][k], f[cur ^ 1][j][k]);
        Update(f[cur][j][k ^ i], f[cur ^ 1][j][k]);
        Update(f[cur][j][k], f[cur ^ 1][j][k]);
      }
    }
  }
  for (int i = m + 1; i <= n; ++i) {
    int cur = i & 1;
    memset(f[cur], 0x00, sizeof f[cur]);
    for (int j = 0; j < lim; ++j) {
      for (int k = 0; k < lim; ++k) {
        Update(f[cur][j ^ i][k], f[cur ^ 1][j][k]);
        Update(f[cur][j][k], f[cur ^ 1][j][k]);
      }
    }
  }
  int p = n & 1, ans = 0;
  for (int j = 0; j < lim; ++j) {
    for (int k = j + 1; k < lim; ++k) {
      Update(ans, f[p][j][k]);
    }
  }
  printf("%d\n", ans);
  return 0;
}
