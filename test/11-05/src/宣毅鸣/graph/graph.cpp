#include<bits/stdc++.h>
using namespace std;
const int N=18,M=998244353;
int fi[N],ans,v[N][1<<N],x,y,ne[N*N],zz[N*N],tot,n,m;
void jb(int x,int y){
	ne[++tot]=fi[x];
	fi[x]=tot;
	zz[tot]=y;
}
struct C{
	int num,x;
}f[N][1<<N];
C dfs(int x,int y){
	if (v[x][y])return f[x][y];
	v[x][y]=1;int flag=0;
	for (int i=fi[x];i;i=ne[i])
		if (!((1<<zz[i])&y)){
			C k1=dfs(zz[i],y|(1<<zz[i])),k2=dfs(x,y|k1.x);
			(f[x][y].num+=(long long)k1.num*k2.num%M)%=M;
			f[x][y].x=k2.x;
			flag=1;
		}
	if (!flag){
		f[x][y].num=1;
		f[x][y].x=y;
	}	
	return f[x][y];
}
int main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	scanf("%d%d",&n,&m);
	while (m--){
		scanf("%d%d",&x,&y);
		x--;y--;
		jb(x,y);jb(y,x);
	}
	for (int i=0;i<n;i++){
		C k=dfs(i,1<<i);
		(ans+=k.num)%=M;
	}
	printf("%d\n",ans);
	return 0;
}
