#include<bits/stdc++.h>
using namespace std;
const int N=305,M=1e9+7;
int n,m,f[N][N][N],ans;
int main(){
	freopen("set.in","r",stdin);
	freopen("set.out","w",stdout);
	scanf("%d%d",&n,&m);
	int l=max(n,m);
	f[0][0][0]=1;
	for (int i=1;i<=l;i++)
		for (int j=0;j<=256;j++)
			for (int k=0;k<=256;k++){
				(f[i][j][k]+=f[i-1][j][k])%=M;
				if (i<=n)(f[i][j][k]+=f[i-1][j^i][k])%=M;
				if (i<=m)(f[i][j][k]+=f[i-1][j][k^i])%=M;
			}
	for (int i=0;i<=256;i++)
		for (int j=i+1;j<=256;j++)
			(ans+=f[l][i][j])%=M;
	printf("%d",ans);		
	return 0;
}
