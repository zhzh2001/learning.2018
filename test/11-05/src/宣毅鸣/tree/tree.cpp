#include<bits/stdc++.h>
using namespace std;
const int N=1e7+5,M=998244353;
typedef long long ll;
ll inv[N],ans;
int size[N],fi[N],n,x,ne[N],zz[N],tot;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline int read(){
	int x=0;char c=0;
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())x=x*10+c-48;
	return x;
}
inline void dfs(int x){
	size[x]=1;
	for (int i=fi[x];i;i=ne[i])dfs(zz[i]),size[x]+=size[zz[i]];
	(ans+=inv[size[x]])%=M;
}
inline void jb(int x,int y){
	ne[++tot]=fi[x];
	fi[x]=tot;
	zz[tot]=y;
}
signed main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	inv[0]=inv[1]=1;
	for (int i=2;i<=n;i++)inv[i]=(-(M/i)*inv[M%i]%M+M)%M;
	for (int i=2;i<=n;i++){
		x=read();
		jb(x,i);
	}
	dfs(1);
	printf("%lld",ans);
	return 0;
}
