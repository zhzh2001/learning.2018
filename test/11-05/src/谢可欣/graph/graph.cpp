#include <iostream>
#include <cstring>
#include <cstdio>
#include <set>
using namespace std;
inline int read(){int tot=1;char c=getchar();while(c!='-'&&(c<'0'||c>'9'))c=getchar();if(c=='-')tot=-1,c=getchar();
int sum=0;while(c<='9'&&c>='0')sum=sum*10+c-'0',c=getchar();return sum*tot;}
const int mod=998244353,N=1000;
int n,m,ans,pos,du[300],a[300],st[300],vis[300],tmp[300];
int e[300][300];
//set<int> g[30],t[30];
bool check(){
//	for(int i=1;i<=n;i++) cout<<a[i]<<" "; puts("");
	pos=0; 
//	for(int i=1;i<=n;i++)t[i]=g[i];
	for(int i=1;i<=n;i++)tmp[i]=du[i];
	for(int i=1;i<=n;i++){
		if(i!=1&&!e[st[pos]][a[i]])return 0;
//		if(i!=1)tmp[st[pos]]--; 
		if(i!=1){
			for(int j=1;j<i;j++)if(e[a[j]][a[i]])tmp[a[i]]--,tmp[a[j]]--;
		}
		st[++pos]=a[i];
		while(!tmp[st[pos]]&&pos>=1)pos--; 
	}
//	cout<<"ok"<<endl;
	return 1;
}
void dfs(int rt){
	if(rt>n){
		if(check())ans=(ans+1)%mod;
		return;
	}
	for(int i=1;i<=n;i++){
		if(!vis[i]){
			vis[i]=1;
			a[rt]=i;
			dfs(rt+1);
			vis[i]=0;
		}
	}
}
int main()
{
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read(),m=read();
	for(int i=1;i<=m;i++){
		int x=read(),y=read();
		e[x][y]=1;e[y][x]=1; 
		du[x]++; du[y]++;
//		g[x].insert(y);g[y].insert(x);
	}
	dfs(1);
	cout<<ans%mod<<endl;
	return 0;
}
