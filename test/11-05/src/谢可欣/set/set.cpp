#include <iostream>
#include <cstdio>
#define int long long
using namespace std;
const int mod=1e9+7;
int n,m,ans,a[5000],b[5000];
bool check(){
	int A=0,B=0;
	for(int i=1;i<=n;i++){
		if(a[i])A^=i;
		else if(b[i])B^=i;
	}
	return A<B;
}
void dfs2(int rt){
	if(rt>m){
		if(check()){
		    ans=(ans+1)%mod;
//		    for(int i=1;i<=n;i++)cout<<i<<a[i]<<b[i]<<" ";puts("");
		}
		return;
	}
	dfs2(rt+1);
	if(!a[rt]){
		b[rt]=1;
		dfs2(rt+1);
		b[rt]=0;
	}
}
void dfs1(int rt){
	if(rt>n){
		dfs2(1); return;
	}
	dfs1(rt+1);
	a[rt]=1;
	dfs1(rt+1);
	a[rt]=0;
}
signed main()
{
	freopen("set.in","r",stdin);
	freopen("set.out","w",stdout);
    cin>>n>>m;
	dfs1(1);
	cout<<ans%mod<<endl;
	return 0;
}
