#include <iostream>
#include <cstdio>
#define int long long
using namespace std;
inline int read(){int tot=1;char c=getchar();while(c!='-'&&(c<'0'||c>'9'))c=getchar();if(c=='-')tot=-1,c=getchar();
int sum=0;while(c<='9'&&c>='0')sum=sum*10+c-'0',c=getchar();return sum*tot;}
const int N=1e7+5,mod=998244353;
int n,ans,vis[N],fa[N];
inline int power(int x,int k){
	int ans=1ll;
	for(;k;k>>=1,x=1ll*x*x%mod)if(k&1)ans=1ll*ans*x%mod;
	return ans%mod;
}
void change(int x,int w){
	while(fa[x]!=0){
		vis[x]+=w;
		x=fa[x];
	}
	vis[x]+=w;
}
void dfs(int bs,int iv){
//	cout<<bs<<endl;
//    cout<<"kkk"<<bs<<" "<<iv<<" ";for(int i=1;i<=n;i++)cout<<vis[i]<<" ";puts("");
	int flag=0;
	for(int i=1;i<=n;i++)
		if(!vis[i]) flag++;
	if(!flag){
		ans=(ans+bs*power(iv,mod-2))%mod; return;
	}
	for(int i=1;i<=n;i++){
		if(!vis[i]){
			change(i,1);
			dfs(bs+1,iv*flag);
			change(i,-1);
		}
	}
}
signed main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	for(int i=2;i<=n;i++){
		int x=read();  fa[i]=x;
	}
	dfs(0,1);
	cout<<ans%mod<<endl;
	return 0;
}
/*
13
1 2 3 4 5 6 7 2 2 2 2 12 
7 13 7 
*/
