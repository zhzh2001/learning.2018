#include<bits/stdc++.h>
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
using namespace std;
const int N=18;
const int M=(N)*(N-1)+5;
//int f[N+1][(1<<N)-1];
int vis[N];
vector<int>q;
map<vector<int>,int>mmp;
int res[(1<<N)-1];
int head[N],tail[M],nxt[M],t,n,m;
ll ans;
inline void addto(int x,int y)
{
	nxt[++t]=head[x];
	head[x]=t;
	tail[t]=y;
}
void dfs(int k,int from,int d,int s)
{
//	cout<<k<<' '<<from<<' '<<d<<' '<<s<<endl;
	if(s==n){
		if(!mmp[q])ans++;
		mmp[q]=1;
		return;
	}
	vis[k]=true;
	q.push_back(k);
	bool b=false;
	int t=s-1;
	while(!b&&t>=0){
		int x=q[t];
		for(int i=head[x];i;i=nxt[i]){
			int v=tail[i];
			if(vis[v])continue;
			b=true;
			dfs(v,from,d|(1<<v-1),s+1);
		}
		t--;
	}
	q.pop_back();
	vis[k]=false;
}
/*void dfs2(int k,int from,int d,int s)
{
//	cout<<k<<' '<<from<<' '<<d<<' '<<s<<endl;
//	cout<<n-n/2<<endl;
	if(s>n-n/2-1){
//		cout<<"!"<<endl;
		ans+=f[k][res[d]|(1<<k-1)];
		cout<<k<<' '<<(res[d])<<endl;
		return;
	}
	vis[k]=true;
	q[s]=k;
	bool b=false;
	int t=s;
	while(!b&&t>0){
		int x=q[t];
		for(int i=head[x];i;i=nxt[i]){
			int v=tail[i];
			if(vis[v])continue;
			b=true;
			dfs2(v,from,d|(1<<v-1),s+1);
		}
		t--;
	}
	q[s]=0;
	vis[k]=false;
}*/
int main()
{
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=m;i++){
		int x=read(),y=read();
		addto(x,y);
		addto(y,x);
	}
	int r=(1<<n)-1;
	for(int i=0;i<=(1<<n)-1;i++)
		res[i]=r^i;
	for(int i=1;i<=n;i++)
		dfs(i,i,(1<<i-1),1);
//	for(int i=1;i<=n;i++){
//		for(int j=1;j<=(1<<n)-1;j++)
//			cout<<f[i][j]<<' ';
//		cout<<endl;
//	}
//	for(int i=1;i<=n;i++)
//		dfs2(i,i,(1<<i-1),1);
	writeln(ans);
	return 0;
}
