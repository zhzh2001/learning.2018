#include <bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int mod=1e9+7;
int N,M;
int ans;
void change(int &a,int b) {
	a=a+b;
	if(a>=mod) a-=mod;
}
int get(int x,int S) {
	return (S>>x)&1;
}
bool bb;
int f[256][256][256];
void solve(){
	f[0][0][0]=1;
	if(N>M)swap(N,M),bb=true;
	for(int i=0;i<N;++i)
		for(int j=0;j<256;++j)
			for(int k=0;k<256;++k)
				if(f[i][j][k]){
					change(f[i+1][j^(i+1)][k],f[i][j][k]);
					change(f[i+1][j][k^(i+1)],f[i][j][k]);
					change(f[i+1][j][k],f[i][j][k]);
				}
	for(int i=N;i<M;++i)
		for(int j=0;j<256;++j)
			for(int k=0;k<256;++k)
				if(f[i][j][k]){
					change(f[i+1][j][k^(i+1)],f[i][j][k]);
					change(f[i+1][j][k],f[i][j][k]);
				}
	for(int j=0;j<256;++j)
		for(int k=j+1;k<256;++k)
			if(!bb)change(ans,f[M][j][k]);
			else change(ans,f[M][k][j]);
	writeln(ans);
}
int main() 
{
	freopen("set.in","r",stdin);
	freopen("set.out","w",stdout);
	N=read();M=read();
	solve();
	return 0;
}
