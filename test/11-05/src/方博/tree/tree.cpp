#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int L=1000000;
char LZH[L];
char *SSS,*TTT;
inline char gc(){
	if (SSS==TTT) TTT=(SSS=LZH)+fread(LZH,1,L,stdin);
	return *SSS++;
}
inline ll read()
{
	ll x=0,f=1;char ch=gc();
	for(;ch<'0'||ch>'9';ch=gc())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=gc())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int mod=998244353;
const int N=10000005;
ll fa[N];
ll sz[N];
ll ksm(ll a,ll b){
	ll ans=1;
	for(;b;b>>=1){
		if(b&1)ans=ans*a%mod;
		a=a*a%mod;
	}
	return ans%mod;
}
int n;
ll ans;
int inv[N];
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	sz[1]=1;
	for(register int i=2;i<=n;i++)
		fa[i]=read(),sz[i]=1;
	for(register int i=n;i>1;i--)
		sz[fa[i]]+=sz[i];
	ll fz=0,fm=1;
	for(register int i=1;i<=n;i++){
		fz=(fz*sz[i]+fm);
		if(fz>1e9)fz%=mod;
		fm=(fm*sz[i]);
		if(fm>1e9)fm%=mod;
	}
	fz%=mod,fm%=mod;
	writeln(fz*ksm(fm,mod-2)%mod);
	return 0;
}
