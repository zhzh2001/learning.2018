#include <bits/stdc++.h>
#define mod 998244353

using namespace std;

inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}

int mp[18][18], cnt[18];
int f[1<<18][18];
bool g[1<<18][18][18], h[1<<18][18];

int main(int argc, char const *argv[]) {
  freopen("graph.in", "r", stdin);
  freopen("graph.out", "w", stdout);

  int n = read(), m = read();
  for (int i = 0; i < m; ++ i) {
    int x = read() - 1, y = read() - 1;
    mp[x][y] = mp[y][x] = 1;
    g[1 << x][x][y] = 1;
    g[1 << y][y][x] = 1;
  }
  for (int i = 0; i < n; ++ i) {
    for (int j = 0; j < n; ++ j) {
      cnt[i] |= (mp[i][j] << j);
    }
  }

  for (int i = 0; i < n; ++ i) {
    f[1 << i][i] = 1;
    h[1 << i][i] = true;
  }
  /*
  g[s][i][j] 表示 j 是否能在 s 以 i 结尾之后遍历到。(i in s, j not in s)
  h[s][i] s 能否以 i 结尾

  for h[s][i]:
    if h[s][i]:
      for each k where mp[i][k]:
        g[s][i][k] = true
        h[s|(1<<k)][k] = true
        if k has no other points connected with out s:
          g[s|(1<<k)][k] <= copy = g[s][i]
  */
  for (int s = 0; s < (1 << n); ++ s) {
    vector<int> vec;
    int ak = 0;
    for (int i = 0; i < n; ++ i) {
      if (h[s][i]) {
        for (int k = 0; k < n; ++ k) if (!(s >> k & 1) && mp[i][k]) {
          g[s][i][k] = true;
          h[s | (1 << k)][k] = true;
          ak |= (1 << k);
          if (!(cnt[k] & ~s)) {
            vec.push_back(k);
          }
        }
      }
    }
    for (size_t i = 0; i < vec.size(); ++ i) {
      int k = vec[i], t = s | (1 << k);
      for (int j = 0; j < n; ++ j) if (!(t >> j & 1)) {
        g[t][k][j] |= (ak >> j & 1);
      }
    }
  }

  for (int s = 0; s < (1 << n); ++ s) {
    for (int i = 0; i < n; ++ i) {
      if (!(s >> i & 1)) {
        for (int j = 0; j < n; ++ j) {
          if ((s >> j & 1) && g[s][j][i]) {
            f[s | (1 << i)][i] += f[s][j];
          }
        }
      }
    }
  }

  int ans = 0;
  for (int i = 0; i < n; ++ i) {
    ans += f[(1 << n) - 1][i];
  }

  printf("%d\n", ans);

  return 0;
}
