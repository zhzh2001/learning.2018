#include <bits/stdc++.h>
#define mod 1000000007

using namespace std;

inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}

const int maxn = 256;
int f[2][maxn][maxn];

int main(int argc, char const *argv[]) {
  freopen("set.in", "r", stdin);
  freopen("set.out", "w", stdout);

  int n = read(), m = read();
  if (n > 200 || m > 200) {
    return 2147483647;
  }

  int now = 0, pre = 1;
  f[now][0][0] = 1;
  for (int i = 1; i <= max(n, m); ++ i) {
    swap(now, pre);
    memcpy(f[now], f[pre], sizeof(int) * maxn * maxn);
    for (int j = 0; j < maxn; ++ j) {
      for (int k = 0; k < maxn; ++ k) {
        if (i <= n) f[now][j ^ i][k] = (f[now][j ^ i][k] + f[pre][j][k]) % mod;
        if (i <= m) f[now][j][k ^ i] = (f[now][j][k ^ i] + f[pre][j][k]) % mod;
      }
    }
  }

  int ans = 0;
  for (int i = 0; i < maxn; ++ i) {
    for (int j = i + 1; j < maxn; ++ j) {
      ans = (ans + f[now][i][j]) % mod;
    }
  }
  printf("%d\n", ans);

  return 0;
}