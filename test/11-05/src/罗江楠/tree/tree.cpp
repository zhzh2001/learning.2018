#include <bits/stdc++.h>
#define N 10000020
#define mod 998244353

using namespace std;

typedef long long ll;

inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}

int sz[N], fa[N];
ll inv[N];

int main(int argc, char const *argv[]) {
  freopen("tree.in", "r", stdin);
  freopen("tree.out", "w", stdout);

  int n = read();
  inv[1] = 1;
  for (int i = 2; i <= n; ++ i) {
    fa[i] = read();
    inv[i] = inv[mod % i] * (mod - mod / i) % mod;
  }
  ll ans = 0;
  for (int i = n; i; -- i) {
    sz[fa[i]] += ++ sz[i];
    ans = (ans + inv[sz[i]]) % mod;
  }

  printf("%lld\n", ans);

  return 0;
}