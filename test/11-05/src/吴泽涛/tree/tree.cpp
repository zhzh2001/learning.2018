#include <bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(register int i=j; i<=k; i++)
#define Dow(i, j, k) for(register int i=j; i>=k; i--)
using namespace std;
GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
void writeln(GG x) { write(x); putchar('\n'); }

const int N = 1e7+11, Mod = 998244353; 
int n, tot, ans; 
int num[N], a[N], b[50], inv[N]; 


inline int ksm(int x) {
  int ans = 1;
  Dow(i, tot, 1) {
    ans = 1ll*ans*ans% Mod; 
    if(b[i]) ans = 1ll*ans*x%Mod; 
  }
}

int main() {
  freopen("tree.in", "r", stdin); 
  freopen("tree.out", "w", stdout); 
  n = read(); 
  For(i, 2, n) a[i] = read(); 
  For(i, 1, n) num[i] = 1; 
  Dow(i, n, 2) num[a[i]] += num[i]; 
  int y = Mod-2; 
  while(y) { 
    b[++tot] = y&1; 
    y/=2; 
  }
  inv[0] = inv[1] = 1; 
  For(i, 2, n+7)
		inv[i]=1ll*inv[Mod%i]*(Mod-Mod/i)%Mod;
  For(i, 1, n) 
      ans = (1ll*ans+inv[num[i]] )%Mod; 
  writeln(ans); 
}









