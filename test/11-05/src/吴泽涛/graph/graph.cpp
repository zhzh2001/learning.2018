#include <bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(register int i=j; i<=k; i++)
#define Dow(i, j, k) for(register int i=j; i>=k; i--)
using namespace std;
GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
void writeln(GG x) { write(x); putchar('\n'); }

const int N = 40, Mod = 998244353; 
int n, sum, m, Ti; 
int mp[N][N], vis[N], b[N]; 

inline bool check(int v, int x) {
  if(x==0) return 1; 
  if(Ti>=23e7) {
    writeln(sum); 
    exit(0); 
  }
  Dow(i, x, 1) {
    int u = b[i]; ++Ti; 
    if(mp[u][v]) return 1; 
    For(j, 1, n) 
      if(mp[u][j] && !vis[j]) return 0; 
  }
  return 0;
}

void dfs(int x) {
  if(x>n) {
    ++sum;  
    return; 
  }
  Dow(i, n, 1) {
    if(!vis[i] && check(i, x-1)) {
      vis[i] = 1; 
      b[x] = i; 
      dfs(x+1); 
      vis[i] = 0; 
    }
  }
}

int main() {
  freopen("graph.in", "r", stdin); 
  freopen("graph.out", "w", stdout); 
  n = read(); m = read(); 
  For(i, 1, m) {
    int x = read(), y = read(); 
    mp[x][y] = 1; mp[y][x] = 1; 
  }
  bool flag = 0; 
  For(i, 1, n) 
    For(j, 1, n) 
    if(i != j && !mp[i][j]) {
      flag = 1; 
      break; 
    }
  if(!flag) {
    int res = 1; 
    For(i, 1, n) res = 1ll*res*i %Mod; 
    writeln(res); 
    return 0; 
  }
  For(i, 1, n) mp[0][i] = 1, mp[i][0] = 1;
  dfs(1); 
  writeln(sum); 
}

/*




*/








