#include <bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(register int i=j; i<=k; i++)
#define Dow(i, j, k) for(register int i=j; i>=k; i--)
using namespace std;
GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
void writeln(GG x) { write(x); putchar('\n'); }

const int N = 50; 
int n, m;
int a[N], b[N], vis[N], x_sum, y_sum, sum; 
void dfs_2(int x) {
  if(x > m) {
    if(x_sum < y_sum) ++sum; 
    return; 
  }
  dfs_2(x+1); 
  if(!a[x]) {
    y_sum^=x; dfs_2(x+1); y_sum^=x; 
  }
}
void dfs_1(int x) {
  if(x > n) {
    y_sum = 0; 
    dfs_2(1); 
    return; 
  }
  a[x] = 0; dfs_1(x+1); 
  a[x] = 1; x_sum^=x; dfs_1(x+1); x_sum^=x; 
}

int main() {
  freopen("set.in", "r", stdin); 
  freopen("set.out", "w", stdout); 
  n = read(); m = read(); 
  dfs_1(1); 
  writeln(sum); 
}










