#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <vector>
#define ll long long
#define file "tree"
using namespace std;
const int N=10000009;
const int mod=998244353;
struct Node{
	int siz;
	vector<int>son;
}tree[N];
int read(){
	char c;int num,f=1;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;num=c-'0';
	while(c=getchar(), isdigit(c))num=num*10+c-'0';
	return f*num;
}
int Pow(int a,int p){
	int ans=1;
	for(;p;p>>=1,a=(1ll*a*a)%mod)
		if(p&1)ans=1ll*a*ans%mod;
	//cout<<ans<<endl;
	return ans;
}
int inv(int x){
	return Pow(x,mod-2);
}
void dfs(int x){
	for(int i=0;i<tree[x].son.size();i++){
		dfs(tree[x].son[i]);
		tree[x].siz+=tree[tree[x].son[i]].siz;
	}
	tree[x].siz+=1;
}
int n,m,f[N];
int get(int x){
	if(f[x])return f[x];
	int tot=tree[x].siz,k=inv(tot),tmp=0;
	for(int i=0;i<tree[x].son.size();i++){
		tmp+=get(tree[x].son[i]);
		if(tmp>=mod)tmp-=mod;
	}
	f[x]=tmp+k;
	if(f[x]>=mod)f[x]-=mod;
	return f[x];
}
int main()
{
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	n=read();
	for(int i=1;i<n;i++)
		tree[read()].son.push_back(i+1);
	dfs(1);
	printf("%d\n",get(1));
	return 0;
}

