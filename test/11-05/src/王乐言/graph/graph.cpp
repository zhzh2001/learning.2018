#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cmath>
#define ll long long
#define file "graph"
using namespace std;
const int mod=998244353;
const int N=20,M=N*(N-1)*2;
int read(){
	char c;int num,f=1;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;num=c-'0';
	while(c=getchar(), isdigit(c))num=num*10+c-'0';
	return f*num;
}
int n,m,vis[N],ans=0,f[(1<<N)+1][N];
int head[N],nxt[M],ver[M],tot=1,g[(1<<N)+1][N];
void add(int u,int v){
	ver[++tot]=v;nxt[tot]=head[u];head[u]=tot;
	ver[++tot]=u;nxt[tot]=head[v];head[v]=tot;
}
void print(int x){
	while(x){
		cout<<(x&1);
		x>>=1;
	}
}
int dfs1(int s,int x){
	if(g[s][x])return g[s][x];
	g[s][x]|=(1<<x-1);
	//if(x==2)cout<<s<<endl;
	for(int i=head[x];i;i=nxt[i]){
		int y=ver[i];
		//if(x==2&&y==1)cout<<(s&(1<<y-1))<<endl;
		if(s&(1<<y-1))continue;
		dfs1(s|((1<<y-1)),y);
		g[s][x]|=g[s|((1<<y-1))][y];	
	}
	return g[s][x];
}
//以当前点为根节点的搜索树的状态 
int dfs(int s,int u){
	if(f[s][u])return f[s][u];
	f[s][u]=1;
	if(!head[u])return f[s][u];
	int gg[20][4],cnt=0,k,flag=0;
	memset(gg,0,sizeof(gg));
	//print(s);cout<<"  "<<u<<endl;
	
	for(int i=head[u];i;i=nxt[i]){
		int y=ver[i];flag=0;
		if(s&(1<<y-1))continue;
		k=dfs1(s|(1<<y-1),y);
		//k是状态
		//if(u==4&&s==8&&k==6)cout<<y<<endl;
		
		for(int j=1;j<=cnt;j++){
			if(gg[j][2]==k){
				flag=1;gg[j][1]+=dfs(s|(1<<y-1),y);
				if(gg[j][1]>=mod)gg[j][1]-=mod;
				break;
			}
		}
		//遍历原来的状态 
		if(!flag){
			gg[++cnt][1]=dfs(s|(1<<y-1),y);
			gg[cnt][2]=k;
		}
		//新开状态 
	}
	/*if(u==4&&s==8){
		cout<<gg[1][2]<<endl;
	}*/
	for(int i=1;i<=cnt;i++)
		f[s][u]=1ll*f[s][u]*i%mod*gg[i][1]%mod;
	return f[s][u];
}
int main()
{
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=m;i++){
		int u=read(),v=read();
		add(u,v);	
	}
	for(int i=1;i<=n;i++){
		add(n+1,i);
	}
	dfs((1<<n),n+1);
	//cout<<g[12][3]<<endl;
	printf("%d\n",f[1<<n][n+1]);
	return 0;
}
/* 
3 2
1 2
2 3

4


5 7
1 2
3 1
4 2
5 2
3 5
3 2
1 5

48

考虑状压
到一个点之后，他的出边有两类，一类是后面会联通的
一类是后面不连通的我们发现如果联通的话，他们可以互相抵达
那么方案数应该是累加的
如果不连通的话，方案数应该是累乘的

我们发现如果两个出边指向的节点在删掉上面的点之后仍联通
那么我们最多只会选用一种遍历方式，也就是说两个遍历
是互相影响的，我们用加法原理。  
如果两个点互相不可达，我们用乘法原理。  
然后再排列数计算一下遍历顺序即可。 
g[s][i]表示已遍历s,现在从i点开始跑，能抵达的节点的状态
 
f[s][i]表示已经遍历s状态，现在从i节点开始遍历
以i为根的搜索树的数量 
dfs搜索所有当前点可达的所有未被遍历的节点
查询它的可达状态，如果是新状态，新开点
如果和以前状态重复，那么累加
答案就是所有状态乘积乘状态数的排列数 
*/


