program tree;
 var
  d:array[0..10000001] of longint;
  fac,binv:array[0..10000001] of int64;
  i,n,x,ssum,mot,spum:longint;
  orzs:int64;
 function hahaksm(x,y:int64):int64;
  begin
   hahaksm:=1;
   while y>0 do
    begin
     if y and 1=1 then hahaksm:=hahaksm*x mod mot;
     y:=y>>1;
     x:=x*x mod mot;
    end;
  end;
 begin
  assign(input,'tree.in');
  assign(output,'tree.out');
  reset(input);
  rewrite(output);
  mot:=998244353;
  filldword(d,sizeof(d)>>2,0);
  readln(n);
  for i:=1 to n-1 do
   begin
    read(x);
    inc(d[x]);
   end;
  readln;
  spum:=0;
  for i:=1 to n do
   if d[i]=0 then inc(spum);
  fac[0]:=1;
  for i:=1 to n do
   fac[i]:=fac[i-1]*i mod mot;
  //writeln(fac[n]*hahaksm(fac[ssum],mot-2) mod mot*hahaksm(fac[n-ssum],mot-2) mod mot);
  binv[0]:=1;
  binv[1]:=1;
  for i:=2 to n do
   binv[i] := (mot-mot div i)*binv[mot mod i] mod mot;
  for i:=1 to n do
   binv[i]:=binv[i-1]*binv[i] mod mot;
  orzs:=binv[n];
  ssum:=0;
  for i:=spum-1 to n-1 do
   ssum:=(ssum+fac[i]*binv[spum-1] mod mot*binv[i-spum+1] mod mot*(i+1) mod mot) mod mot;
  writeln(ssum*orzs mod mot);
  close(input);
  close(output);
 end.
