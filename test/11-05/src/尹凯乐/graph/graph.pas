program graph;
 var
  a:array[0..19,0..19] of longint;
  f,g:array[0..258888,0..19] of int64;
  i,j,k,m,n,x,y,mot:longint;
  ssum:int64;
 function hahadfss(x,y:longint):int64;
  var
   i:longint;
  begin
   if g[x,y]<>0 then exit(g[x,y]);
   g[x,y]:=x;
   for i:=0 to n-1 do
    if (a[y,i+1]=1) and (x and (1<<i)=0) then g[x,y]:=g[x,y] or hahadfss(x or (1<<i),i+1);
   exit(g[x,y]);
  end;
 function hahadfsss(x,y:longint):int64;
  var
   i,j:longint;
  begin
   if f[x,y]<>0 then exit(f[x,y]);
   if g[x,y]=x then exit(1);
   f[x,y]:=0;
   for i:=1 to n do
    if ((1<<(i-1)) and x=0) and (a[y,i]=1) then f[x,y]:=(f[x,y]+hahadfsss(x or (1<<(i-1)),i)*hahadfsss(g[x or (1<<(i-1)),i],y)) mod mot;
   //f[x,y]:=f[x,y]*hahadfsss(g[x,y],y);
   exit(f[x,y]);
  end;
 begin
  assign(input,'graph.in');
  assign(output,'graph.out');
  reset(input);
  rewrite(output);
  mot:=998244353;
  filldword(a,sizeof(a)>>2,0);
  readln(n,m);
  for i:=1 to m do
   begin
    readln(x,y);
    a[x,y]:=1;
    a[y,x]:=1;
   end;
  for i:=1 to n do
   a[0,i]:=1;
  fillqword(f,sizeof(f)>>3,0);
  f[0,0]:=1;
  fillqword(g,sizeof(g)>>3,0);
  //hahadfss(0,0);
  for i:=0 to 1<<n-1 do
   for j:=1 to n do
    if g[i,j]=0 then hahadfss(i,j);
  {for i:=0 to 1<<n-1 do
   for j:=1 to n do
    if i and (1<<(j-1))=0 then
     for k:=0 to n do
      if a[k,j]=1 then f[i or (1<<(j-1)),j]:=(f[i or (1<<(j-1)),j]+f[i,k]) mod mot; }
  ssum:=0;
  for i:=1 to n do
   ssum:=(ssum+hahadfsss(1<<(i-1),i)) mod mot;
  {ssum:=0;
  for i:=1 to n do
   ssum:=(ssum+f[1<<n-1,i]) mod mot;}
  writeln(ssum);
  close(input);
  close(output);
 end.
