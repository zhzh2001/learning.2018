#include<bits/stdc++.h>
using namespace std;
// const int N=20,M=300005;
const int mod=998244353;
#define ll long long
// int n,m,p,x,y,ans;
// int a[N][N];
// int f[N][M];
const int N=20;
int a[N][N],st[N],vis[N],n,top,pr[N],ans,m,x,y;
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void dfs()
{
	if (top==n)
	{
		++ans;
		return;
	}
	int p=top;
	while (1)
	{
		int y=st[p];
		bool b=0;
		for (int i=1;i<=n;++i)
			if (!vis[i]&&a[y][i])
			{
				b=1;
				st[++top]=i;
				vis[i]=1;
				pr[top]=p;
				dfs();
				--top;
				vis[i]=0;
			}
		if (b)
			break;
		else
			p=pr[p];
	}
}
inline void tp()
{
	ans=1;
	for (int i=1;i<=n;++i)
		ans=(ll)ans*i%mod;
	cout<<ans;
	exit(0);
}
int main()
{
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	read(n);
	read(m);
	if (m==n*(n-1)/2)
		tp();
	// for (int i=1;i<=n;++i)
	// 	a[i][0]=a[0][i]=1;
	// for (int i=1;i<=m;++i)
	// {
	// 	read(x);
	// 	read(y);
	// 	a[x][y]=a[y][x]=1;
	// }
	// f[0][0]=1;
	// p=1<<n;
	// for (int i=0;i<p;++i)
	// 	for (int j=0;j<=n;++j)
	// 		if (f[j][i])
	// 		{
	// 			for (int k=1;k<=n;++k)
	// 				if (a[j][k]&&((i>>(k-1))&1)==0)
	// 					(f[k][i|(1<<(k-1))]+=f[j][i])%=mod;
	// 		}
	// for (int i=1;i<=n;++i)
	// 	(ans+=f[i][p])%=mod;
	// cout<<ans;
	for (int i=1;i<=m;++i)
	{
		read(x);
		read(y);
		a[x][y]=a[y][x]=1;
	}
	for (int i=1;i<=n;++i)
	{
		st[top=1]=i;
		vis[i]=1;
		pr[1]=0;
		dfs();
		vis[i]=0;
	}
	cout<<ans;
	return 0;
}