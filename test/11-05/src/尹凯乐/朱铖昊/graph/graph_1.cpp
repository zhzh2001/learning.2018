#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int N=20,M=300005;
const int mod=998244353;
int f[N][M];
int col[N][M][N];
int n,m;
int a[N][N];
int used[N][M][N];
int cnt,ti;
int x,y,ans;
inline void rs(int c,int x,int y,int z)
{
	col[z][y][x]=c;
	for (int i=1;i<=n;++i)
		if (col[z][y][i]!=c&&a[x][i]&&((y>>(i-1))&1))
			rs(c,i,y,z);
}
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void dfs(int x,int y)
{
	if (f[x][y])
		return;
	f[x][y]=1;
	if (y==0)
		return;
	// for (int i=1;i<=n;++i)
	// 	used[x][y][i]=0;
	cnt=0;
	for (int i=1;i<=n;++i)
		if (!used[x][y][i]&&a[x][i]&&((y>>(i-1))&1))
		{
			++cnt;
			int sum=0;
			f[x][y]=(ll)f[x][y]*cnt%mod;
			rs(++ti,i,y,x);
			int p=0;
			for (int j=1;j<=n;++j)
				if (col[x][y][j]==ti)
					p|=(1<<(j-1));
			for (int j=1;j<=n;++j)
				if (!used[x][y][j]&&a[x][j]&&col[x][y][j]==ti)
				{
					dfs(j,p^(1<<(j-1)));
					used[x][y][j]=1;
					(sum+=f[j][p^(1<<(j-1))])%=mod;
				}
			f[x][y]=(ll)f[x][y]*sum%mod;
		}
}
int main()
{
	freopen("graph.in","r",stdin);
	freopen("graph_1.out","w",stdout);
	read(n);
	read(m);
	for (int i=1;i<=m;++i)
	{
		read(x);
		read(y);
		a[x][y]=1;
		a[y][x]=1;
	}
	int p=1<<n;
	for (int i=1;i<=n;++i)
	{
		dfs(i,p-1-(1<<(i-1)));
		(ans+=f[i][p-1-(1<<(i-1))])%=mod;
	}
	cout<<ans;
	return 0;
}