#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int mod=1e9+7;
const int inv2=5e8+4;
const int N=2055;
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
int f[N],g[N][N],h[N][N],a[N];
int n,x,y;
int ans;
// inline void fwt(int *a)
// {
// 	for (int p=1;p<=n;++p)
// 	{
// 		for (int i=2;i<=n;i*=2)
// 			for (int j=1;j<=n;j+=i)
// 				for (int bc=i/2,k=j;k<i+bc;++k)
// 				{
// 					int x=a[p][k],y=a[p][k+bc];
// 					a[p][k]=(x+y)%mod;
// 					a[p][k+bc]=((x-y)%mod+mod)%mod;
// 				}
// 	}
// 	for (int p=1;p<=n;++p)
// 	{
// 		for (int i=2;i<=n;i*=2)
// 			for (int j=1;j<=n;j+=i)
// 				for (int bc=i/2,k=j;k<i+bc;++k)
// 				{
// 					int x=a[k][p],y=a[k+bc][p];
// 					a[k][p]=(x+y)%mod;
// 					a[k+bc][p]=((x-y)%mod+mod)%mod;
// 				}
// 	}
// }
inline void work()
{
	f[0]=1;
	for (int i=1;i<=x;++i)
		for (int j=0;j<n;++j)
			if ((j^i)>j)
			{
				int p=f[j],q=f[j^i];
				(f[j]+=q*2%mod)%=mod;
				(f[j^i]+=p*2%mod)%=mod;
			}
	for (int i=1;i<n;++i)
		(ans+=f[i])%=mod;
	for (int i=0;i<n;++i)
		f[i]=0;
	cout<<(ll)ans*inv2%mod<<',';
}
inline void bl()
{
	g[0][0]=1;
	h[0][0]=1;
	for (int i=1;i<=min(x,y);++i)
	{
		for (int j=0;j<n;++j)
			for (int k=0;k<n;++k)
			{
				(h[j^i][k]+=g[j][k])%=mod;
				(h[j][k^i]+=g[j][k])%=mod;
			}
		for (int j=0;j<n;++j)
			for (int k=0;k<n;++k)
				g[j][k]=h[j][k];
	}
	while (n<=x||n<=y)
		n*=2;
	if (x<=y)
	{
		a[0]=1;
		for (int i=x+1;i<=y;++i)
			for (int j=0;j<n;++j)
				if ((j^i)>j)
				{
					int p=a[j],q=a[j^i];
					(a[j]+=q)%=mod;
					(a[j^i]+=p)%=mod;
				}
		for (int i=0;i<n;++i)
		{
			for (int j=1;j<n;++j)
				(g[j][i]+=g[j-1][i])%=mod;
			for (int j=0;j<i;++j)
				ans+=(ll)a[j]*g[(j^i)-1][i];
			for (int j=i+1;j<n;++j)
				ans+=(ll)a[j]*g[(j^i)-1][i];
		}
	}
	else
	{
		a[0]=1;
		for (int i=y+1;i<=x;++i)
			for (int j=0;j<n;++j)
				if ((j^i)>j)
				{
					int p=a[j],q=a[j^i];
					(a[j]+=q)%=mod;
					(a[j^i]+=p)%=mod;
				}
		for (int i=0;i<n;++i)
		{
			for (int j=1;j<n+2;++j)
				(g[i][j]+=g[i][j-1])%=mod;
			for (int j=0;j<n;++j)
				ans+=(ll)a[j]*((g[i][n-1]-g[i][j^i]+mod)%mod)%mod;
		}
	}
	for (int i=0;i<n;++i)
		a[i]=0;
	for (int i=0;i<n;++i)
		for (int j=0;j<n;++j)
			g[i][j]=h[i][j]=0;
	// for (int i=0;i<n;++i)
	// 	for (int j=i+1;j<n;++j)
	// 		(ans+=g[i][j])%=mod;
	cout<<ans<<',';
}
int main()
{
	freopen("biao.out","w",stdout);
	for (x=1;x<=2000;++x)
		for (y=1;y<=2000;++y)
		{
			ans=0;
			n=1;
			while (n<=x&&n<=y)
				n*=2;
			if (x==y)
				work();
			else
				bl();
		}
	return 0;
}