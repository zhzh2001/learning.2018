#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline LL read(){
	LL a=0,b=1;int dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
const LL md=998244353;
int n,m,e[18],a[1<<18];
LL b[1<<18][18],ans,x,y,z,o;
int main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read();
	m=read();
	for(int i=1;i<=m;i++){
		x=read()-1;
		y=read()-1;
		e[x]|=1<<y;
		e[y]|=1<<x;
	}
	m=1<<n;
	for(int i=0;i<n;i++)a[1<<i]=1;
	for(int i=1;i<m;i++){
		if(a[i]==0)continue;
		for(register int j=0;j<n;j++)if(i&(1<<j)){
			x=e[j];
			if((i|x)==i)continue;
			x=x&!i;
			for(register int k=0;k<n;k++){
				if(e[j]&(1<<k))a[i|(1<<k)]=1;
			}
		}
	}
	for(int i=1;i<m;i++){
		if(a[i]==1){
			a[i]=i;
			continue;
		}
		for(register int j=0;j<n;j++)if(i&(1<<j)){
			a[i]=max(a[i],a[i^(1<<j)]);
		}
	}
	for(int i=1;i<m;i++)if(a[i]==i){
		for(register int j=0;j<n;j++)if(i&(1<<j)){
			x=i^(1<<j);
			b[i][j]=1;
			o=0;
			while(x>0){
				o++;
				y=a[x];
				z=0;
				for(register int k=0;k<n;k++)if(e[j]&(1<<k))if(y&(1<<k)){
					z+=b[y][k];
				}
				x^=y;
				b[i][j]=b[i][j]*(z%md)%md;
			}
			for(register int k=1;k<=o;k++)b[i][j]=b[i][j]*k%md;
		}
	}
	ans=0;
	for(int i=0;i<n;i++){
		ans+=b[m-1][i];
	}
	ans%=md;
	printf("%lld\n",ans);
	return 0;
}
