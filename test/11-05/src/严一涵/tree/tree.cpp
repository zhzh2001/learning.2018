#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
const LL md=998244353;
int n,p[10000003];
LL f[10000003],v[10000003],ans;
void exgcd(LL a,LL b,LL&x,LL&y){
	if(b==0){
		x=1;
		y=0;
		return;
	}
	exgcd(b,a%b,y,x);
	y-=a/b*x;
}
LL inv(LL v){
	LL x,y;
	exgcd(v,md,x,y);
	return (x%md+md)%md;
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	p[1]=0;
	for(int i=1;i<n;i++){
		p[i+1]=read();
	}
	f[0]=1;
	for(int i=1;i<=n;i++){
		f[i]=f[i-1]*i%md;
	}
	v[n]=inv(f[n]);
	for(int i=n;i>0;i--){
		v[i-1]=v[i]*i%md;
	}
	for(int i=1;i<=n;i++){
		v[i]=v[i]*f[i-1]%md;
	}
	ans=0;
	for(int i=1;i<=n;i++){
		f[i]=1;
	}
	for(int i=n;i>=1;i--){
		f[p[i]]+=f[i];
		ans+=v[f[i]];
	}
	printf("%lld\n",ans%md);
	return 0;
}
