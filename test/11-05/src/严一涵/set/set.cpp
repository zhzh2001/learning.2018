#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
int n,m,x,y,ans;
int main(){
	freopen("set.in","r",stdin);
	freopen("set.out","w",stdout);
	n=read();
	m=read();
	ans=0;
	for(int i=0;i<(1<<n);i++){
		x=0;
		for(int j=0;j<n;j++)if(i&(1<<j)){
			x^=j+1;
		}
		for(int j=0;j<(1<<m);j++){
			if(i&j)continue;
			y=0;
			for(int k=0;k<m;k++)if(j&(1<<k)){
				y^=k+1;
			}
			if(x<y)ans++;
		}
	}
	printf("%d\n",ans);
	return 0;
}
