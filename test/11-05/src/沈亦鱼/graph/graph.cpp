#include<cstdio>
using namespace std;
int mo=998244353,n,m,u,v,s,b[25],c[270000],g[270000],f[20][270000];
inline int read(){
	int x=0,f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9'){
		if(ch=='-')f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}
//inline void adg(int u,int v){
//	tot++;
//	p[tot]=v;
//	ne[tot]=he[u];
//	he[u]=tot;
//}
/*
void sor(int l,int r){
	int i=l,j=r;
	ull x=a[(l+r)>>1];
	while(i<=j){
		while(a[i]<x)i++;
		while(x<a[j])j--;
		if(i<=j){
			swap(a[i],a[j]);
			i++;
			j--;
		}
	}
	if(l<j)sor(l,j);
	if(i<r)sor(i,r);
}
*/
int main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	b[1]=1;
	for(int i=2;i<=19;i++)
		b[i]=b[i-1]<<1;
	n=read();
	m=read();
	for(int i=1;i<=m;i++){
		u=read();
		v=read();
//		adg(u,v);
//		adg(v,u);
		g[u]|=b[v];
		g[v]|=b[u];
	}
	for(int i=1;i<=n;i++){
		f[i][b[i]]=1;
		c[b[i]]=g[i];
		for(int j=b[i];j<b[n+1];j++)
			for(int k=1;k<=n;k++)
				if((b[k]&j)==0&&(b[k]&c[j])>0){
					f[i][j|b[k]]=(f[i][j|b[k]]+f[i][j])%mo;
					c[j|b[k]]=c[j]|g[k];
				}
//	printf("%d\n",f[i][b[n+1]-1]);
		s=(s+f[i][b[n+1]-1])%mo;
	}
	printf("%d",s);
	return 0;
}
