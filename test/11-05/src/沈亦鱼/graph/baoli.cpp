#include<cstdio>
using namespace std;
int mo=998244353,n,m,u,v,s,ans,tot,cnt,rt,d[20],b[20],fac[20],p[50],ne[50],he[20],g[270000];
inline int read(){
	int x=0,f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9'){
		if(ch=='-')f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}
void adg(int u,int v){
	cnt++;
	p[cnt]=v;
	ne[cnt]=he[u];
	he[u]=cnt;
}
int sol(int k){
	int num=0;
	long long s=1;
	for(int i=he[k];i;i=ne[i]){
		num++;
		s=s*sol(p[i]);
	}
	s=s*fac[num]%mo;
	return s;
}
void dfs(int k,int m,int no,int x){printf("%d %d\n",no,tot);
	if(no>tot+1){
		if(tot+1<n)return;
		ans+=sol(rt);
		return;
	}
	while(m<=n&&((g[k]&b[m])==0||(x&b[m])>0))m++;printf("%d\n",m);
	if(m>n){
		dfs(d[no],1,no+1,x);
		return;
	}
	tot++;
	d[tot]=m;
	adg(k,m);
	dfs(k,m+1,no,x|b[m]);
	cnt--;
	tot--;
	dfs(k,m+1,no,x);
}
int main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	b[1]=1;
	fac[1]=fac[0]=1;
	for(int i=2;i<=19;i++){
		b[i]=b[i-1]<<1;
		fac[i]=fac[i-1]*i%mo;
	}
	n=read();
	m=read();
	for(int i=1;i<=m;i++){
		u=read();
		v=read();
		g[u]|=b[v];
		g[v]|=b[u];
	}
	for(int i=1;i<=n;i++){
		ans=0;
		tot=0;
		rt=i;
		dfs(i,1,1,b[i]);
		s=(s+ans)%mo;
	}
	printf("%d",s);
	return 0;
}
