#include<cstdio>
using namespace std;
int mo=998244353,n,m,u,v,s,h,t,tot,b[20],p[400],ne[400],he[20],g[20],d[5400000],z[5400000],us[20][270000],f[20][270000];
inline int read(){
	int x=0,f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9'){
		if(ch=='-')f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}
int main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	b[1]=1;
	for(int i=2;i<=19;i++)
		b[i]=b[i-1]<<1;
	n=read();
	m=read();
	for(int i=1;i<=m;i++){
		u=read();
		v=read();
		g[u]|=b[v];
		g[v]|=b[u];
	}
	h=0;
	t=n;
	for(int i=1;i<=n;i++){
		f[i][b[i]]=1;
		us[i][b[i]]=1;
		d[i]=i;
		z[i]=b[i];
	}
	while(h<t){
		h++;
		if(z[h]==b[n+1]-1){
			s=(s+f[d[h]][z[h]])%mo;
			continue;
		}
		for(int i=1;i<=n;i++)
			if((z[i]&b[p[i]])==0){
				u=p[i];
				v=z[i]|b[p[i]];
				f[u][v]=(f[u][v]+f[i][z[i]])%mo;
				if(!us[u][v]){
					us[u][v]=1;
					t++;
					d[t]=u;
					z[t]=v;
				}
			}
	}
	printf("%d",s);
	return 0;
}
