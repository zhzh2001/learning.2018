#include<bits/stdc++.h>
using namespace std;
int mo=998244353,n,s,fa[11000000];
long long fm[11000000],sz[11000000];
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
//#define gc getchar
inline int read(){
	int x = 0; char ch = gc();
	for (; !isdigit(ch); ch = gc());
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return x;
}
long long pwr(long long x,int y){
	long long z=1;
	while(y){
		if(y&1)z=z*x%mo;
		x=x*x%mo;
		y>>=1;
	}
	return z;
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	sz[1]=1;
	for(int i=2;i<=n;i++){
		sz[i]=1;
		fa[i]=read();
	}
	for(int i=n;i>1;i--)
		sz[fa[i]]+=sz[i];
	for(int i=1;i<=n;i++)
		fm[i]=sz[i];
	for(int i=n;i>1;i--)
		fm[fa[i]]=(fm[fa[i]]*fm[i])%mo;
	for(int i=1;i<=n;i++)
		if(sz[i]==1)s=(s+fm[1])%mo;
		else s=(s+fm[1]*pwr(sz[i],mo-2)%mo)%mo;
//	long long ss=1;
//	for(int i=1;i<=n;i++)
//		ss*=sz[i];
//	if(ss>mo)puts("!");
//	printf("%lld\n",ss);
//	printf("%lld %lld\n",s,fm[1]);
	printf("%lld",1ll*s*pwr(fm[1],mo-2)%mo);
	return 0;
}
