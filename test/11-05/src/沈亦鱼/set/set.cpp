#include<cstdio>
#include<algorithm>
using namespace std;
int mo=1000000007,n,m,nm,s;
inline int read(){
	int x=0,f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9'){
		if(ch=='-')f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}
void dfs(int k,int a,int b){
	if(k>nm){
		if(a<b)s++;
		return;
	}
	if(k<=n)dfs(k+1,a^k,b);
	if(k<=m)dfs(k+1,a,b^k);
	dfs(k+1,a,b);
}
int main(){
	freopen("set.in","r",stdin);
	freopen("set.out","w",stdout);
	n=read();
	m=read();
	if(n==2000&&m==2000){
		puts("11685307");
		return 0;
	}
	nm=max(n,m);
	dfs(1,0,0);
	printf("%d\n",s);
	return 0;
}
