#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for(int i=a;i<=b;i++)
#define Rep(i,a,b) for(int i=b;i>=a;i--)
using namespace std;
ll n,sum[10001000],fa[10001000],f[10001000];
ll ans,mo,fz,fm,q;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
inline ll ksm(ll x,ll y)
{
	ll z=1;
	while(y>0)
	{
//		cout<<x<<" "<<y<<" "<<z<<endl;
		if(y%2==1)
		{
			z*=x;
			z%=mo;
		}
		x*=x;
		x%=mo;
		y/=2;
	}
	return z;
}
ll gcd(ll x,ll y)
{
	if(y==0)return x;
	return gcd(y,x%y);
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	sum[1]=1;
	For(i,2,n)fa[i]=read(),sum[i]=1;;
	Rep(i,2,n)sum[fa[i]]+=sum[i];
	mo=998244353; 
	For(i,1,n)
	{
		if(f[sum[i]]==0)f[sum[i]]=ksm(sum[i],mo-2);
		ans+=f[sum[i]];
		ans%=mo;
	}
	cout<<ans<<endl;
	return 0;
}
//609967024
