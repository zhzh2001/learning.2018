#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for(int i=a;i<=b;i++)
#define Rep(i,a,b) for(int i=b;i>=a;i--)
using namespace std;
ll n,sum[10001000],fa[10001000];
ll ans,mo,fz,fm,q;
inline ll read()
{
    ll x=0;char ch=getchar();
    while(ch<'0'||ch>'9')ch=getchar();
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x;
}
inline ll ksm(ll x,ll y)
{
	ll z=1;
	while(y>0)
	{
//		cout<<x<<" "<<y<<" "<<z<<endl;
		if(y%2==1)
		{
			z*=x;
			z%=mo;
		}
		x*=x;
		x%=mo;
		y/=2;
	}
	return z;
}
/*ll gcd(ll x,ll y)
{
	if(y==0)return x;
	return gcd(y,x%y);
}*/
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	sum[1]=1;
	For(i,2,n)fa[i]=read(),sum[i]=1;;
//	int t=clock();
	Rep(i,2,n)sum[fa[i]]+=sum[i];
	mo=998244353; 
	fz=0;fm=1;
	For(i,1,n)
	{
		//q=sum[i]/gcd(sum[i],fm);
		//fz*=q;
		//fm*=q;
		//fz+=(fm/sum[i]);
		fz*=sum[i];
		fz+=fm;
		fm*=sum[i];
		fz%=mo;
		fm%=mo;
//		fz%=mo;
	}
//	cout<<fz<<" "<<fm<<endl;
	ans=(fz*ksm(fm,mo-2))%mo;
	cout<<ans<<endl;
//	t=clock()-t;
//	cout<<t<<endl;
	return 0;
}
