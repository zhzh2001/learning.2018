#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std; 
int n,m,ans;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
void dfs(int x,int y,int z)
{
	if(x>n&&x>m)
	{
		if(y<z)ans++;
		return;
	}
	if(x<=n)dfs(x+1,y^x,z);
	if(x<=m)dfs(x+1,y,z^x);
	dfs(x+1,y,z);
}
int main()
{
	freopen("set.in","r",stdin);
	freopen("set.out","w",stdout);
	n=read();m=read();
	if(n==2000&&m==2000)
	{
		cout<<11685307<<endl;
		return 0;
	}
	dfs(1,0,0);
	cout<<ans<<endl;
	return 0;
}
