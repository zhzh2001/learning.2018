#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
#define pb push_back
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
using namespace std;
ll ans;
ll n,m;
vector<int>a[20];
int fr[20],b[20];
bool f[20];
void dfs(int x,int wz)
{
	b[x]=wz;
	if(x==n)
	{
		ans++;
//		For(i,1,n)cout<<b[i]<<" ";
//		cout<<endl;
		return;
	}
	bool b=false;
	while(!b)
	{
		For(i,0,a[wz].size()-1)
		{
			if(f[a[wz][i]])continue;
			b=true;
			fr[a[wz][i]]=wz;
			f[a[wz][i]]=true;
			dfs(x+1,a[wz][i]);
			f[a[wz][i]]=false;
		}
		wz=fr[wz];
	}
}
int main()
{
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read();m=read();
	For(i,1,m)
	{
		int x=read(),y=read();
		a[x].pb(y);
		a[y].pb(x);
	}
	For(i,1,n)
	{
		memset(fr,0,sizeof(0));
		memset(f,false,sizeof(f));
		f[i]=true;
		dfs(1,i);
	}
	cout<<ans<<endl;
	return 0;
}
