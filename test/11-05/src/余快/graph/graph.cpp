/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000005
#define mod 998244353
#define put putchar('\n')
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
#define N 500055
//#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
//#define getchar gc
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int n,m,ans;
int nedge,head[N],Next[N*2],to[N*2];
#define V to[i]
void add(int a,int b){
	Next[++nedge]=head[a];head[a]=nedge;to[nedge]=b;
}
void add_ne(int a,int b){
	add(a,b);add(b,a);
}
inline void tadd(int &x,int k){x+=k;x-=(x>=mod)?mod:0;}
int a[19][270000];
int f[19][270000];
int dfs(int x,int k){
	if (k==(1<<n)-1){
		a[x][k]=1<<(x-1);return 1;
	}
	if (f[x][k]!=-1) return f[x][k];
	int t=0,pd=0;f[x][k]=0;a[x][k]=1<<(x-1);
	go(i,x){
		if (!(k&(1<<(V-1)))){
			pd=1;
			int p=k|(1<<(V-1));
			t=dfs(V,p);
			a[x][k]|=a[V][p];
			tadd(f[x][k],1LL*t*dfs(x,k|a[V][p])%mod);
		}
	}
	if (!pd) f[x][k]=1;
	return f[x][k];
}
signed main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read();m=read();memset(f,-1,sizeof(f));
	F(i,1,m) add_ne(read(),read());
	F(i,1,n){
		tadd(ans,dfs(i,1<<(i-1)));
	}
	wrn(ans);
	return 0;
}
