#include <bits/stdc++.h>
#define ll long long
#define pa pair<int,int>
#define be begin()
#define en end()
#define mp make_pair
#define pb push_back
#define V to[i]
#define gc getchar
#define put putchar('\n')
#define re register
#define F(i,a,b) for (re int i=(a);i<=(b);i++)
#define D(i,a,b) for (re int i=(a);i>=(b);i--)
#define go(i,x) for (re int i=head[x];i;i=Next[i])
#define mod 1000000007
#define N 500055
//#define int ll
using namespace std;
inline int read(){
	char c;
	int f=1,x=0;
	for (c=gc();c<'0'||c>'9';c=gc()) if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc()) x=x*10+c-'0';
	return (f==1)?x:-x;
}
inline void wr(int x){if (x<0) putchar('-'),x=-x;if (x>=10) wr(x/10);putchar('0'+x%10);}
inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x){wr(x);put;}
inline void wrn(int x,int y){wri(x);wrn(y);}
int n,m,yk,num[50],g[1300][6],f[1300][6],p,ans;
inline void add(int &x,int k){x+=k;x-=(x>=mod)?mod:0;}
void solve(int p,int a,int b){
	F(i,0,yk-1){
		F(j,0,3){
			if (p==1){
				add(g[i^a][j^b],f[i][j]);
			}
			else{
				add(g[i^a][j^(b*2)],f[i][j]);
			}
		}
	}
}
void init(){
	F(i,0,yk-1) F(j,0,3) add(f[i][j],g[i][j]),g[i][j]=0;
}
signed main(){
	freopen("set.in","r",stdin);
	freopen("set.out","w",stdout);
	n=read();m=read();p=max(n,m);num[0]=1;F(i,1,10) num[i]=num[i-1]*2;
	//f[i][j]表示前面的相等情况，钦定的情况0 
	F(i,0,10){//钦定在哪一位 
		yk=num[10-i];
		F(j,0,yk-1) F(k,0,3) f[j][k]=0;
		f[0][0]=1;
		F(j,1,p){
			init();
			int t=j>>(i+1),q=(j>>i)&1;
			if (j<=n) solve(1,t,q);
			if (j<=m) solve(2,t,q);
		}
		init();
		add(ans,f[0][2]);
	}
	wrn(ans);
	return 0;
}
