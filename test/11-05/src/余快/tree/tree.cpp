#include <bits/stdc++.h>
#define ll long long
#define pa pair<int,int>
#define be begin()
#define en end()
#define mp make_pair
#define pb push_back
#define V to[i]
#define put putchar('\n')
#define re register
#define F(i,a,b) for (re int i=(a);i<=(b);i++)
#define D(i,a,b) for (re int i=(a);i>=(b);i--)
#define go(i,x) for (re int i=head[x];i;i=Next[i])
#define mod 998244353
#define N 500055
//#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
//#define gc getchar
inline int read(){
	char c;
	int f=1,x=0;
	for (c=gc();c<'0'||c>'9';c=gc()) if (c=='-') f=-1;
	for (;c>='0'&&c<='9';c=gc()) x=x*10+c-'0';
	return (f==1)?x:-x;
}
inline void wr(int x){if (x<0) putchar('-'),x=-x;if (x>=10) wr(x/10);putchar('0'+x%10);}
inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x){wr(x);put;}
inline void wrn(int x,int y){wri(x);wrn(y);}
int n,m,ans,fa[10000012],num,fac[10000012],inv[10000012],siz[10000012];
inline void add(int &x,int k){x+=k;x-=(x>=mod)?mod:0;x+=(x<0)?mod:0;}
int ksm(ll x,int k){
	int sum=1;
	while (k){
		if (k&1) sum=sum*x%mod;
		x=x*x%mod;k>>=1;
	}
	return sum;
}
int C(int n,int m){
	return (n<m||m<0)?0:1LL*fac[n]*inv[m]%mod*inv[n-m]%mod;
}
void init_c(int n){
	fac[0]=1;F(i,1,n) fac[i]=1LL*fac[i-1]*i%mod;
	inv[n]=ksm(fac[n],mod-2);D(i,n-1,0) inv[i]=1LL*inv[i+1]*(i+1)%mod;
}
signed main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();init_c(n+10);
	F(i,1,n) siz[i]=1;
	F(i,2,n) fa[i]=read();
	D(i,n,2) siz[fa[i]]+=siz[i];
	F(i,1,n){
		add(ans,1LL*C(n,siz[i])*fac[siz[i]-1]%mod*fac[n-siz[i]]%mod);
	}
	ans=1LL*ans*inv[n]%mod;
	wrn(ans);
	return 0;
}
/*
题意看错前的做法
 	F(i,2,n) vis[read()]=1;
	F(i,1,n) if (!vis[i]) num++;
	inv[1]=1;
	int fac=1;
	F(i,1,num){
		if (i>1) inv[i]=1LL*(mod-mod/i)*inv[mod%i]%mod;
		fac=1LL*fac*inv[i]%mod;
		fac=1LL*fac*(num-i+1)%mod;
		if (i&1) add(ans,1LL*n*inv[i]%mod*fac%mod);
		else add(ans,-1LL*n*inv[i]%mod*fac%mod);
	}
	wrn(ans);
*/
