#include<bits/stdc++.h>
#define ll long long
using namespace std;
ll fm,fz,i,n,p=998244353,sum[10000001],g[10000001];
int tot,head[10000001],next[10000001],e[10000001];
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
void build(ll t,ll k){
	tot++;
	e[tot]=k;
	next[tot]=head[t];head[t]=tot;
}
void dfs(ll x){
	int i;
	sum[x]=1;
	for(i=head[x];i;i=next[i]){
		dfs(e[i]);
		sum[x]+=sum[e[i]];
	}
	if(!g[sum[x]])fm=fm*sum[x]%p;
	g[sum[x]]++;
//	printf("%lld\n",sum[x]);
}
ll ksm(ll x,ll y){
	ll now=1;
	while(y){
		if(y%2)now=(now*x)%p;
		x=(x*x)%p;
		y/=2;
	}
	return now;
}
ll ny(ll x){
	return ksm(x,p-2);
}
int main(){
freopen("tree.in","r",stdin);
freopen("tree.out","w",stdout);	
	ll i,t;
	scanf("%d",&n);
	for(i=2;i<=n;i++){
		t=read();
		build(t,i);
	}
	fm=1ll;
	dfs(1);
	for(i=1;i<=n;i++)
	 if(g[i]){
	 	//printf("%lld %lld %lld %lld\n",fz,i,sum[i],fm/i*sum[i]);
	  fz=fz+fm*ny(i)%p*g[i]%p;
	  //printf("%lld\n",fz);
}
    //printf("%lld %lld\n",fz,fm);
	printf("%lld",fz*ny(fm)%p); 
}
