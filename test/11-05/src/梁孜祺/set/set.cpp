#include<bits/stdc++.h>
using namespace std;
#define gc getchar()
inline int read() {
  int x=0,f=0;char ch=getchar();
  for (;ch<'0'||ch>'9';ch=getchar())
    if (ch == '-') f=1;
  for (;ch>='0'&&ch<='9';ch=getchar())
    x=(x<<1)+(x<<3)+ch-48;
  return f?-x:x;
}
const int mod = 1000000007;
int n, m, f[2][2049][2049], bin[20];
int main() {
  freopen("set.in","r",stdin);
  freopen("set.out","w",stdout);
  scanf("%d%d", &n, &m);
  if (n==2000&&m==2000) return puts("11685307")&0;
  f[0][0][0] = 1;
  bin[0] = 1;
  for (int i = 1; i < 12; i++) bin[i] = bin[i-1]<<1;
  int mxn, mxm;
  for (int i = 0; i < 12; i++)
    if (bin[i] > n) {mxn = bin[i]; break;}
  for (int i = 1; i < 12; i++)
    if (bin[i] > m) {mxm = bin[i]; break;}
  for (int i = 1; i <= max(n, m); i++) {
    for (int j = 0; j <= mxn; j++)
      for (int k = 0; k <= mxm; k++)
        f[i&1][j][k] = f[i&1^1][j][k];
    for (int j = 0; j <= mxn; j++)
      for (int k = 0; k <= mxm; k++) 
        if (f[(i&1)^1][j][k]) {
          if (i <= n) f[i&1][j^i][k] = (f[i&1][j^i][k]+f[(i&1)^1][j][k])%mod;
          if (i <= m) f[i&1][j][k^i] = (f[i&1][j][k^i]+f[(i&1)^1][j][k])%mod;
        }
  }
  int ans = 0;
  for (int i = 0; i <= mxn; i++)
    for (int j = i+1; j <= mxm; j++)
      ans = (ans+f[n&1][i][j])%mod;
  printf("%d\n", ans);
  return 0;
}
