#include<bits/stdc++.h>
using namespace std;
inline int read() {
  int x=0,f=0;char ch=getchar();
  for (;ch<'0'||ch>'9';ch=getchar())
    if (ch == '-') f=1;
  for (;ch>='0'&&ch<='9';ch=getchar())
    x=(x<<1)+(x<<3)+ch-48;
  return f?-x:x;
}
int a[5];
int main() {
  for (int i = 0; i < 5; i++) a[i] = read();
  sort(a,a+5);
  for (int i = 0; i < 5; i++)
    printf("%d ", a[i]);
  return 0;
}
