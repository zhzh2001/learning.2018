#include<bits/stdc++.h>
using namespace std;
const int mod = 998244353;
int n, m, a[19][19], Pow[19];
bool flag;
int vis[19], tnt, ans, t[19], Vis[19], nxt[19];
inline int solve() {
  memset(Vis, 0, sizeof Vis);int top = 0;
  memset(nxt, 0, sizeof nxt);
  for (int i = 1; i <= n; i++) {
    Vis[t[i]] = 1;
    if (i == 1) {top++;continue;}
    while (top && !a[t[top]][t[i]]) {
      top = nxt[top];
      for (int j = 1; j <= n; j++)
        if(a[t[top]][j] && !Vis[j]) return 0;
    }
    if (!top) return 0;
    nxt[i] = top; 
    top = i; 
  }
  return 1;
}
inline void dfs(int x) {
  if (x == n+1) {
    if (solve()) ans++;
    return;
  }
  for (int i = 1; i <= n; i++)
    if (!vis[i]) {
      t[x] = i;
      vis[i] = 1;
      dfs(x+1);
      vis[i] = 0;
    }
}
int main() {
  freopen("graph.in","r",stdin);
  freopen("graph.out","w",stdout);
  scanf("%d%d", &n, &m);
  for (int i = 1; i <= m; i++) {
    int x, y; scanf("%d%d", &x, &y);
    a[x][y] = a[y][x] = 1;
  }
  dfs(1);
  printf("%d\n", ans);
  return 0;
}
/*
f[i][j] = 

*/
