#include<bits/stdc++.h>
using namespace std;
#define gc getchar()
inline int read() {
  int x=0,f=0;char ch=getchar();
  for (;ch<'0'||ch>'9';ch=getchar())
    if (ch == '-') f=1;
  for (;ch>='0'&&ch<='9';ch=getchar())
    x=(x<<1)+(x<<3)+ch-48;
  return f?-x:x;
}
const int N = 10000005;
int head[N], cnt;
struct edge {
  int nxt, to;
}e[N];
inline void insert(int u, int v) {
  e[++cnt] = (edge){head[u], v}; head[u] = cnt;
}
int n, sz[N];
inline void dfs(int x) {
  sz[x] = 1;
  for (int i = head[x]; i; i = e[i].nxt) {
    dfs(e[i].to);
    sz[x] += sz[e[i].to];
  }
}
const int mod = 998244353;
inline int ksm(int x, int y) {
  int sum = 1;
  while (y) {
    if (y&1) sum = 1ll*sum*x%mod;
    y>>=1;
    x = 1ll*x*x%mod;
  }
  return sum;
}
int ans;
int main() {
  freopen("tree.in","r",stdin);
  freopen("tree.out","w",stdout);
  scanf("%d", &n);
  for (int i = 2; i <= n; i++) {
    int x = read();
    insert(x, i);
  }
  dfs(1);
  for (int i = 1; i <= n; i++)
    ans = (ans+ksm(sz[i], mod-2))%mod;
  printf("%d\n", ans);
  return 0;
}
