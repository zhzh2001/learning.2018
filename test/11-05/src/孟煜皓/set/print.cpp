#include <cstdio>
int n, m, t, a[2005], s1, s2, ans, bo;
int main(){
	freopen("set.txt", "w", stdout);
	for (register int N = 1; N <= 10; ++N){
		for (register int M = 1; M <= 10; ++M){
			n = N, m = M, ans = s1 = s2 = 0;
			if (n > m) t = n, n = m, m = t, bo = 1; else bo = 0;
			for (register int i = 0; i <= m; ++i) a[i] = 0;
			while (!a[0]){
				register int j = m;
				while (a[j] == 2) a[j] = 0, s2 ^= j, --j;
				if (a[j] == 0) if (j > n) a[j] = 2, s2 ^= j; else a[j] = 1, s1 ^= j;
				else a[j] = 2, s1 ^= j, s2 ^= j;
				if (bo) ans += s1 > s2; else ans += s1 < s2;
			}
			printf("%7d", ans);
		}
		putchar('\n');
	}
}
