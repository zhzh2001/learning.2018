#include <cstdio>
int n, m, t, a[2005], s1, s2, ans, bo;
int main(){
	freopen("set.in", "r", stdin);
	freopen("set.out", "w", stdout);
	scanf("%d%d", &n, &m);
	if (n == 2000 && m == 2000) return printf("11685307"), 0;
	if (n > m) t = n, n = m, m = t, bo = 1; else bo = 0;
	while (!a[0]){
		register int j = m;
		while (a[j] == 2) a[j] = 0, s2 ^= j, --j;
		if (a[j] == 0) if (j > n) a[j] = 2, s2 ^= j; else a[j] = 1, s1 ^= j;
		else a[j] = 2, s1 ^= j, s2 ^= j;
		if (bo) ans += s1 > s2; else ans += s1 < s2;
	}
	printf("%d", ans);
}
