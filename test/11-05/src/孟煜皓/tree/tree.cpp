#include <cstdio>
#include <cctype>
#include <algorithm>
#include <cstring>
char buf[80000000], *ps = buf;
void read(int &x){
	for (; !isdigit(*ps); ++ps) ;
	for (; isdigit(*ps); ++ps) x = (x << 1) + (x << 3) + (*ps ^ '0');
}
#define P 998244353
int n, fa[10000005], sz[10000005], inv[10000005];
long long ans;
int main(){
	freopen("tree.in", "r", stdin);
	freopen("tree.out", "w", stdout);
	buf[fread(buf, 1, 80000000, stdin)] = '\0';
	read(n);
	for (register int i = 2; i <= n; ++i) read(fa[i]);
	for (register int i = n; i >= 2; --i) ++sz[i], sz[fa[i]] += sz[i];
	++sz[1];
	inv[1] = 1;
	for (register int i = 2; i <= n; ++i) inv[i] = 1ll * inv[P % i] * (P - P / i) % P;
	for (register int i = 1; i <= n; ++i) ans += inv[sz[i]];
	printf("%lld", ans % P);
}

