#include <cstdio>
#include <cctype>
int read(){
	register int x = 0, f = 1;
	register char ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = 0;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
#define P 998244353
int n, m, a[25][25], d[25], vis[25], p[25], ans;
bool check(int k, int x){
	if (k == 1) return -1;
	if (vis[x]) return 0;
	for (register int i = k - 1; i; --i){
		int flag = 0;
		for (register int j = 1; j <= n; ++j)
			if (a[p[i]][j] && !vis[j]){ flag = 1; break; }
		if (flag) if (a[p[i]][x]) return p[i]; else return 0;
	}
	return 0;
}
void dfs(int k){
	if (k > n) return ++ans, void(0);
	for (register int i = 1, j; i <= n; ++i)
		if (j = check(k, i)){
			p[k] = i, vis[i] = 1, dfs(k + 1), vis[i] = 0;
		}
}
int main(){
	freopen("graph.in", "r", stdin);
	freopen("graph.out", "w", stdout);
	n = read(), m = read();
	for (register int i = 1, u, v; i <= m; ++i)
		u = read(), v = read(), a[u][v] = a[v][u] = 1;
	dfs(1);
	printf("%d\n", ans);
}
