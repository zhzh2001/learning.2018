#include <iostream>
#include <cstdio>
#include <algorithm>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
inline void yyhakking(){
	freopen("set.in","r",stdin);
	freopen("set.out","w",stdout);
}
const ll P=1e9+7;
ll n,m,M,f[2][2048][2048],ans,p;
ll v[13]={0,1,2,4,8,16,32,64,128,256,512,1024,2048};
int main(){
	yyhakking();
	read(n),read(m);
	f[0][0][0]=1;
	M=max(n,m);
	for (ll t=1,k;t<=M;++t){
		k=*upper_bound(v+1,v+13,t);
		p^=1;
		for (ll i=0;i<k;++i)
			for (ll j=0;j<k;++j){
				f[p][i][j]=0;
				if (t<=n) (f[p][i][j]+=f[p^1][i^t][j])%=P;
				if (t<=m) (f[p][i][j]+=f[p^1][i][j^t])%=P;
				(f[p][i][j]+=f[p^1][i][j])%=P;
			}
	}
	for (ll i=0;i<2048;++i)
		for (ll j=i+1;j<2048;++j)
			(ans+=f[p][i][j])%=P;
	wr(ans);
	return 0;
}
//sxdakking
