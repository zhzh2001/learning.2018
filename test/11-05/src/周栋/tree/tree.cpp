#include <iostream>
#include <cstdio>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
inline void yyhakking(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
}
const ll N=1e7+10,P=998244353;
ll n,edge,head[N],fa[N],son[N],f[N],inv[N];
struct edge{ll to,net;}e[N];
inline void addedge(ll x,ll y){
	e[++edge].to=y,e[edge].net=head[x],head[x]=edge;
}
inline void dfs(ll x){
	son[x]=1;
	for (ll i=head[x];i;i=e[i].net){
			dfs(e[i].to);
			(f[x]+=f[e[i].to])%=P;
			son[x]+=son[e[i].to];
		}
	(f[x]+=inv[son[x]])%=P;
}
int main(){
	yyhakking();
	read(n);
	inv[1]=1;
	for (ll i=2;i<=n;++i) inv[i]=(P-P/i)*inv[P%i]%P;
	for (ll i=2;i<=n;++i) read(fa[i]),addedge(fa[i],i);
	dfs(1);
	wr(f[1]);
	return 0;
}
//sxdakking
