#include <iostream>
#include <cstdio>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
inline void yyhakking(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
}
const ll P=998244353;
ll e[20][20],n,m,f[1<<18][20],ans;
inline bool in(ll x,ll y){return y&(1<<(x-1));}
inline ll dfs(ll now,ll k){
	ll &ret=f[now][k];
	if (ret) return ret;
	for (ll i=1;i<=n;++i) if (e[k][i]&&in(i,now))
		ret+=dfs(now^(1<<(k-1)),i);
	bool ok=1;
	for (ll i=1;i<=n;++i) if (e[k][i]&&!in(i,now)) ok=0;
	if (!ok) return ret%=P;
	for (ll i=1;i<k;++i) if (in(i,now)&&i!=k){
		bool flag=0;
		for (ll j=1;j<=n&&!flag;++j) if (e[j][i]&&!in(j,now)) flag=1;
		if (flag) continue;
//		for (ll j=1;j<=n;++j)
//			if (e[j][i]&&in(j,now))
//				ret+=dfs(now^(1<<(i-1)),j);
		ret+=dfs(now,i);
	}
	return ret%=P;
}
int main(){
	yyhakking();
	read(n),read(m);
	if (m==(n-1)*n/2){
		ans=1;
		for (ll i=2;i<=n;++i) ans=ans*i%P;
		return wr(ans),0;
	}
	for (ll i=1,x,y;i<=m;++i){
		read(x),read(y);
		e[x][y]=e[y][x]=1;
	}
/*	for (ll i=1;i<=n;++i) f[1<<(i-1)][i][i]=1;
	for (ll i=1;i<(1<<n);++i){
		if (i==(1<<n)-1) continue;
		for (ll j=1;j<=n;++j) if (in(j,i)){
			for (ll k=1;k<=n;++k) if (in(k,i)){
				if (!f[i][j][k]) continue;
				bool flag=1;
				for (ll l=1;l<=n;++l) if (!in(l,i)&&e[j][l]){
					(f[i|(1<<(l-1))][l][j]+=f[i][j][k])%=P;
					flag=0;
				}
				if (!flag) continue;
				for (ll l=1;l<=n;++l) if (in(l,i))
					(f[i][k][l]+=f[i][j][k])%=P;
			}
		}
	}
	for (ll i=1;i<=n;++i)
		for (ll j=1;j<=n;++j)
			ans=(ans+f[(1<<n)-1][i][j])%P;
*/
	for (ll i=1;i<=n;++i) f[1<<(i-1)][i]=1;
	ans=dfs((1<<n)-1,n);
	wr(ans);
	return 0;
}
//sxdakking
/*
3 2
1 2
2 3
*/
