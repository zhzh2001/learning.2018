#include<iostream>
#include<algorithm>
#include<string.h>
#include<fstream>
#include<stdio.h>
using namespace std;
ifstream fin("set.in");
ofstream fout("set.out");
inline long long read(){
	long long x=0;char ch=getchar();
	while(ch<'0'||ch>'9')ch=getchar();
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+(ch-48),ch=getchar();
	return x;
}
void write(long long x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(long long x){
	write(x),puts("");
}
const long long mod=1000000007;
long long n,m,wh;
int f[520][520][520];
bool swapped;
int ans;
inline void add(int &a,int b){
	a=a+b;
	if(a>=mod) a-=mod;
}
int main(){
	freopen("set.in","r",stdin);
	freopen("set.out","w",stdout);
	n=read(),m=read();
	f[0][0][0]=1;
	if(n>m){
		swap(n,m),swapped=true;
	}
	for(register int i=0;i<n;++i){
		for(register int j=0;j<520;++j){
			for(register int k=0;k<520;++k){
				if(f[i][j][k]){
					add(f[i+1][j^(i+1)][k],f[i][j][k]);
					add(f[i+1][j][k^(i+1)],f[i][j][k]);
					add(f[i+1][j][k],f[i][j][k]);
				}
			}
		}
	}
	for(register int i=n;i<m;++i){
		for(register int j=0;j<520;++j){
			for(register int k=0;k<520;++k){
				if(f[i][j][k]){	
					add(f[i+1][j][k^(i+1)],f[i][j][k]);
					add(f[i+1][j][k],f[i][j][k] );
				}
			}
		}
	}
	for(register int j=0;j<520;++j){
		for(register int k=j+1;k<520;++k){
			if(!swapped){
				add(ans,f[m][j][k]);
			}else{
				add(ans,f[m][k][j]);
			}
		}
	}
	writeln(ans);
	return 0;
}
/*

in:
2 2

out:
4

*/
