#include<iostream>
#include<algorithm>
#include<string.h>
#include<fstream>
#include<stdio.h>
using namespace std;
ifstream fin("tree.in");
ofstream fout("tree.out");
inline long long read(){
	long long x=0;char ch=getchar();
	while(ch<'0'||ch>'9')ch=getchar();
	while(ch>='0'&&ch<='9')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	return x;
}
inline void write(long long x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(long long x){
	write(x),puts("");
}
const long long mod=998244353;
int n;
long long ans,fa[10000003],inv[10000003],sz[10000003];
inline long long qpow(long long a,long long b){
	long long ret=1;
	for(;b;b>>=1){
		if(b&1) ret=ret*a%mod;
		a=a*a%mod;
	}
	return ret;
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	sz[1]=1;
	for(register int i=2;i<=n;++i){
		fa[i]=read();
		sz[i]=1;
	}
	for(register int i=n;i>1;--i){
		sz[fa[i]]+=sz[i];
	}
	long long x=0,y=1;
	for(register int i=1;i<=n;++i){
		x=(x*sz[i]+y);
		if(x>1e9)x=x%mod;
		y=y*sz[i];
		if(y>1e9)y=y%mod;
	}
	writeln(x%mod*qpow(y%mod,mod-2)%mod);
	return 0;
}
/*

in:
3
1 2

out:

*/
