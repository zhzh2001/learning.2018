#include<iostream>
#include<algorithm>
#include<string.h>
#include<fstream>
#include<stdio.h>
#include<map>
#include<vector>
using namespace std;
ifstream fin("graph.in");
ofstream fout("graph.out");
inline long long read(){
	long long x=0;char ch=getchar();
	while(ch<'0'||ch>'9')ch=getchar();
	while(ch>='0'&&ch<='9')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	return x;
}
void write(long long x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(long long x){
	write(x),puts("");
}
using namespace std;
int f[20][(1<<18)-1];
int vis[20];
vector<int> q;
long long n,m;
long long res[(1<<18)-1];
long long head[20],to[403],nxt[403],tot;
inline void add(int x,int y){
	nxt[++tot]=head[x],head[x]=tot,to[tot]=y;
}
long long ans;
map<vector<int>,int> mp;
void dfs(int now,int from,int dep,int len){
	if(len==n){
		if(!mp[q]) ++ans;
		else mp[q]=1;
		return;
	}
	int t=len-1;
	bool b=false;
	q.push_back(now),vis[now]=true;
	while(t>=0&&(!b)){
		int x=q[t];
		for(register int i=head[x];i;i=nxt[i]){
			if(!vis[to[i]]){
				b=true,dfs(to[i],from,dep|(1<<to[i]-1),len+1);
			}
		}
		--t;
	}
	vis[now]=false,q.pop_back();
}
inline void solve(){
	int r=(1<<n)-1;
	for(register int i=0;i<=(1<<n)-1;++i) res[i]=r^i;
	for(register int i=1;i<=n;++i) dfs(i,i,(1<<i-1),1);
}
int main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read(),m=read();
	for(register int i=1;i<=m;++i){
		int u,v;
		u=read(),v=read();
		add(u,v),add(v,u);
	}
	solve();
	writeln(ans);
	return 0;
}
