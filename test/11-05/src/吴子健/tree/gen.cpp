#include <bits/stdc++.h>
typedef long long ll;
typedef double db;
#define pa pair<int,int>
#define mk make_pair
#define LOG(format, args...) fprintf(stderr, format, args)
using namespace std;
inline int rd() {
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) if(ch=='-') f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
int N=1e7;
//main========================
int main() {
	srand((ll)(new char));
	freopen("tree.in","w",stdout);
	printf("%d\n",N);
	for(int i=2;i<=N;++i) printf("%d ",rand()%(i-1)+1);puts("");
	return 0;
}

