#include <bits/stdc++.h>
typedef long long ll;
typedef double db;
#define pa pair<int,int>
#define mk make_pair
using namespace std;
inline char gc() {
    static char buf[1<<12],*p1=buf,*p2=buf;
    return (p1==p2)&&(p2=(p1=buf)+fread(buf,1,1<<12,stdin),p1==p2)?EOF:*p1++;
}
#define dd c = gc()
inline void read(int& x) {
    x = 0;
    int dd;
    bool f = false;
    for(; !isdigit(c); dd)
        if(c == '-')
            f = true;
    for(; isdigit(c); dd)
        x = (x<<1) + (x<<3) + (c^48);
    if(f)
        x = -x;
}
#undef dd
const int MAXN=1e7+100;
const int mod=998244353;
void file() {
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
}
ll inv[MAXN];
int N;
int fa[MAXN];
int sz[MAXN];
ll ans;
//main========================
int main() {
	file();
	read(N);
	inv[0]=inv[1]=1;
	for(int i=2;i<=N;++i) inv[i]=(ll)(mod-mod/i)*inv[mod%i]%mod;
	for(int i=1;i<=N;++i) sz[i]=1;
	for(int i=2;i<=N;++i) {
		//fa[i]=rd();
		read(fa[i]);
	}
	for(int i=N;i>=2;--i) {
		sz[fa[i]]+=sz[i];
	//	printf("sz[%d]=%d\n",i,sz[i]);
	}
	//printf("sz[1]=%d\n",sz[1]);
	for(int i=1;i<=N;++i) {
		ans=(ans+inv[sz[i]])%mod;
	}
	printf("%lld\n",(ans%mod+mod)%mod);
	return 0;
}

