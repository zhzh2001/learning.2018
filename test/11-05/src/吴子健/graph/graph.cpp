#include <bits/stdc++.h>
typedef long long ll;
typedef double db;
#define pa pair<int,int>
#define mk make_pair

using namespace std;
inline int rd() {
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) if(ch=='-') f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
void file() {
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
}
const int mod=998244353;
int f[20][(1<<19)+10];
ll dp[20][(1<<19)+10];
int N,M;
int m[20][20];
int bits;
/*
	f[st][S] 已经走了S,现在在st,从st出发走完一切可以走的点获得的集合
	dp[st][S] 已经走了S,现在在st,剩余点的dfs序列数目 
*/
inline bool in(int x,int S) {
	return ((1<<x)&S)>0;
}
int dfs_f(int st,int S) {
	if(f[st][S]!=-1) return f[st][S];
	f[st][S]=S;
	for(int i=0;i<N;++i) if(m[st][i]==1&&!in(i,S)) {
		//printf("ed=%d\n",i);
		f[st][S]|=dfs_f(i,S|(1<<i));
		//printf("f[%d][%d]|=f[%d][%d]=%d\n",st,S,i,S|(1<<i),f[i][S|(1<<i)]);
	} 
	return f[st][S];
}
ll dfs_dp(int st,int S) {
	if(dp[st][S]!=-1) return dp[st][S];
	//printf("dfs_dp(%d,%d)\n",st,S);
	if(S==f[st][S]) {
	//	printf("dp[%d][%d]=%d\n",st,S,1);
		return 1;
	}
	dp[st][S]=0;
	for(int ed=0;ed<N;++ed) if(m[st][ed]&&!in(ed,S)){
		ll x=dfs_dp(ed,S|(1<<ed));
		ll y=dfs_dp(st,f[ed][S|(1<<ed)]);
		dp[st][S]=(dp[st][S]+x*y%mod)%mod;
	}
	//printf("dp[%d][%d]=%d\n",st,S,dp[st][S]);
	return dp[st][S]%mod;
}
ll ans=0;
//main========================
int main() {
	file();
	memset(f,-1,sizeof f);
	memset(dp,-1,sizeof dp);
	N=rd(),M=rd();bits=(1<<N);
	for(int i=1;i<=M;++i) {
		int x=rd()-1,y=rd()-1;
		m[x][y]=m[y][x]=1;
	} 
	for(int i=1;i<bits;++i) {
		for(int j=0;j<N;++j) if(in(j,i)){
			f[j][i]=dfs_f(j,i);
		//	printf("f[%d][%d]=%d\n",j,i,f[j][i]);
		}
	} 
	for(int i=0;i<N;++i) {
		dp[i][1<<i]=dfs_dp(i,1<<i);
		ans=(ans+dp[i][1<<i])%mod;
	}
	printf("%lld\n",ans);
	return 0;
}

