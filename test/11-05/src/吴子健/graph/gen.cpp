#include <bits/stdc++.h>
typedef long long ll;
typedef double db;
#define pa pair<int,int>
#define mk make_pair
#define LOG(format, args...) fprintf(stderr, format, args)
using namespace std;
inline int rd() {
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) if(ch=='-') f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
//main========================
int main() {
	srand((ll)(new char));
	freopen("graph.in","w",stdout);
	int N=18;
	printf("%d %d\n",N,N*(N-1)/2);
	for(int i=1;i<=N;++i) for(int j=i+1;j<=N;++j) printf("%d %d\n",i,j);
	return 0;
}

