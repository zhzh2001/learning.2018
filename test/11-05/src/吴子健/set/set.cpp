#include <bits/stdc++.h>
typedef long long ll;
typedef double db;
#define pa pair<int,int>
#define mk make_pair

using namespace std;
inline int rd() {
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) if(ch=='-') f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
const int mod=1e9+7;
void file() {
	freopen("set.in","r",stdin);
	freopen("set.out","w",stdout);
}
int N,M;
int ans;
void add(int &a,int b) {
	a=a+b;
	if(a>=mod) a-=mod;
}
int digit(int x,int S) {
	return (S>>x)&1;
}
bool swapped;
//void solve_200();
void solve_2000();
//main========================
int main() {
	file();
	N=rd(),M=rd();
	solve_2000();
	return 0;
}
int g[2053][2053][2]; 
void solve_2000() {
	int MAX_NUM=max(N,M);
	for(int digi=0;digi<11;++digi) {
		memset(g,0,sizeof g);
		g[0][0][0]=1;
		for(int i=0;i<MAX_NUM;++i) {
			for(int j=0;j<2048;++j) {
				for(int k=0;k<=1;++k) if(g[i][j][k]){
					int num=i+1;
					add(g[i+1][j][k],g[i][j][k]);
					if(i+1<=M) add( g[i+1][j^num][k^digit(digi,num)] , g[i][j][k] );
					if(i+1<=N) add( g[i+1][j^num][k] , g[i][j][k] );
				}
			}
		}
		int up_S=0;
		for(int i=digi+1;i<11;++i) up_S|=(1<<i);
		for(int k=0;k<2048;++k) if((k&up_S)==0&&digit(digi,k)==1) {
			add(ans,g[MAX_NUM][k][1]);
		}
	}
	printf("%d\n",(ans%mod+mod)%mod);
}
//int f[520][520][520];
//void solve_200() {
//	f[0][0][0]=1;
//	if(N>M) swap(N,M),swapped=true;
//	for(int i=0;i<N;++i) {
//		for(int j=0;j<520;++j) {
//			for(int k=0;k<520;++k) if(f[i][j][k]){
//				add( f[i+1][j^(i+1)][k] , f[i][j][k] );
//				add( f[i+1][j][k^(i+1)] , f[i][j][k] );
//				add( f[i+1][j][k] , f[i][j][k] );
//			}
//		}
//	}
//	for(int i=N;i<M;++i) {
//		for(int j=0;j<520;++j) {
//			for(int k=0;k<520;++k) if(f[i][j][k]) {	
//				add( f[i+1][j][k^(i+1)] ,f[i][j][k] );
//				add( f[i+1][j][k] , f[i][j][k] );
//			}
//		}
//	}
//	for(int j=0;j<520;++j) {
//		for(int k=j+1;k<520;++k) {
//			if(!swapped) add(ans,f[M][j][k]);
//			else add(ans,f[M][k][j]);
//		}
//	}		
//	printf("%d\n",ans);	
//}
