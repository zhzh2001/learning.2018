//#include<bits/stdc++.h>
#include<cstring>
#include<cstdio>
#include<iostream>
typedef long long ll;
using namespace std;
void setIO() {
	freopen("tree.in", "r", stdin);
	freopen("tree.out", "w", stdout);
}
inline ll read() {ll x = 0; char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}while(ch >= '0' 
&& ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');	}
inline void writeln(ll x) {write(x);puts("");}
const int N = 21;
int n, fa[N];
ll inv[N];
bool c[N];
const ll p = 998244353;
inline ll ksm(ll x, ll y) {
	ll res = 1;
	for(; y; y >>= 1, x = x * x % p)
		if(y & 1) res = res * x % p;
	return res;
}
inline int insert(int x) {
	int res = 0;
	while(x) res += 1 - c[x], c[x] = 1, x = fa[x];
	return res;
}

ll ans;
void dfs(int cnt, ll pi, int d) {
	if(cnt == n) {(ans += d * pi) %= p; return;}
	bool tmp[N];
//	for(int i = 1; i <= n; ++i) tmp[i] = c[i];
	memmove(tmp, c, sizeof c);
	for(register int i = 1; i <= n; ++i)
		if(!tmp[i]) {
			dfs(cnt + insert(i), pi * inv[n - cnt]% p, d + 1);
		//	for(int j = 1; j <= n; ++j) c[j] = tmp[j];
			memmove(c, tmp, sizeof c);
		}
}
int main() {
	setIO();
	n = read();
	if(n > 15) {
		puts("0");
		return 0;
	}
	for(register int i = 2; i <= n; ++i)
		fa[i] = read();
	for(register int i = 1; i <= n; ++i)
		inv[i] = ksm(i, p - 2);
	for(register int i = 1; i <= n; ++i) {
		memset(c, 0, sizeof c);
		dfs(insert(i), inv[n], 1);
	}
	cout<<ans<<endl;
	return 0;
}
