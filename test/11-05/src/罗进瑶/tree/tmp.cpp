#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
void setIO() {
	freopen("tree.in", "r", stdin);
	freopen("tree.out", "w", stdout);
}
inline ll read() {ll x = 0; char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}while(ch >= '0' 
&& ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');	}
inline void writeln(ll x) {write(x);puts("");}
const int N = 25;
int n, fa[N];
bool c[N];
void insert(int x) {
	while(x) c[x] = 1, x = fa[x];
}
inline int get(){
	int res = 0;
	for(int i = 1; i <= n; ++i)
		res += c[i];
	return res;
}
double ans;
void dfs(int cnt, double p, int d) {
	if(cnt == n) {ans += d * p; return;}
	int tmp[N];
	for(int i = 1; i <= n; ++i) tmp[i] = c[i];
	for(int i = 1; i <= n; ++i)
		if(!tmp[i]) {
			insert(i);
			dfs(get(), p * 1 / (n - cnt), d + 1);
			for(int j = 1; j <= n; ++j) c[j] = tmp[j];
		}

}
int main() {
	n = read();
	for(int i = 2; i <= n; ++i)
		fa[i] = read();
	for(int i = 1; i <= n; ++i) {
		memset(c, 0, sizeof c);
		insert(i);
		dfs(get(), 1.0 / n, 1);
	}
	cout<<ans<<endl;
	return 0;
}
