#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
void setIO() {
	freopen("graph.in", "r", stdin);
	freopen("graph.out", "w", stdout);
}
inline ll read() {ll x = 0; char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}while(ch >= '0' 
&& ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');	}
inline void writeln(ll x) {write(x);puts("");}
const int N = 18;
int n, m, g[21][21];
int f[1 << 18];
const int p = 998244353;
int main() {
	setIO();
	int x, y;
	n = read(), m = read();
	for(int i = 1; i <= m; ++i) {
		x = read(), y = read();
		g[x][y] = g[y][x] = 1;
	}
	for(int i = 1; i <= n; ++i) f[1 << (i - 1)] = 1;
	int B = (1 << n);
	for(int S = 0; S < B; ++S)
		for(int i = 1; i <= n; ++i)
			if(S & (1 << (i - 1)))
				for(int j = 1; j <= n; ++j)
					if(g[i][j] && ! (S & 1 << (j - 1)))
						(f[S | (1 << (j - 1))] += f[S]) %= p;
	writeln(f[B-1]);	
	return 0;
}
