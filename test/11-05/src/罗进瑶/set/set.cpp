#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
void setIO() {
	freopen("set.in", "r", stdin);
	freopen("set.out", "w", stdout);
}
inline ll read() {ll x = 0; char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}while(ch >= '0' 
&& ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');	}
inline void writeln(ll x) {write(x);puts("");}
const int N = 15;
const int p = 1000000007; 
int n, m, ans;

int main() {
	setIO();
	n = read(), m = read();
	if(n + m > 27) {
		puts("0");
		return 0; 
	}
	int A = (1 << n);
	int B = (1 << m);
	for(int S = 0; S < A; ++S)
		for(int T = 0; T < B; ++T)
			if(!(S & T)) {
				int t1 = 0, t2 = 0;
				for(int i = 1; i <= n; ++i)
					if(S & (1 << (i - 1))) t1 ^= i;
				for(int j = 1; j <= m; ++j)
					if(T & (1 << (j - 1))) t2 ^= j;
				if(t1 < t2) ++ans;
			}
	writeln(ans);
	return 0;
}
