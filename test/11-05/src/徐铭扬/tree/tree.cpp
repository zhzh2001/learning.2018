#include<bits/stdc++.h>
using namespace std;
const int M=998244353;
int n,i,j,k,x,s[20],f[20][1<<20],y,cnt,ans;
void ex_gcd(int a,int b,int &x,int &y){
	if (!b) x=1,y=0;
	else ex_gcd(b,a%b,y,x),y-=a/b*x;
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	scanf("%d",&n);
	s[0]=1;
	for (i=1;i<n;i++) scanf("%d",&x),s[i]=s[x-1]|(1<<i);
	ex_gcd(n,M,x,y);x=(x+M)%M;
	for (i=0;i<n;i++) f[0][s[i]]=x;
	for (i=0;i<n-1;i++)
		for (j=0;j<1<<n;j++)
			if (f[i][j]){
				cnt=0;
				for (k=0;k<n;k++)
					if (!(j&(1<<k))) cnt++;
				ex_gcd(cnt,M,x,y);x=(x+M)%M;
				for (k=0;k<n;k++)
					if (!(j&(1<<k))) (f[i+1][j|s[k]]+=1ll*x*f[i][j]%M)%=M;
			}
	for (i=0;i<n;i++) (ans+=1ll*(i+1)*f[i][(1<<n)-1])%=M;
	printf("%d",(ans+M)%M);
}
