#include<bits/stdc++.h>
using namespace std;
struct node{
	int to,ne;
}e[22];
int tot,n,m,i,x,y,ans,cnt,h[11],las[11];
bool vis[11];
void add(int x,int y){
	e[++tot]=(node){y,h[x]};
	h[x]=tot;
}
void dfs(int u){
	if (cnt==n){
		ans++;
//		for (;u!=-1;u=las[u]) printf("%d ",u);puts("");
		return;
	}
	memset(vis,0,sizeof(vis));
	int tmp=u;
	for (;u!=-1;u=las[u]) vis[u]=1;
	u=tmp;
	vector<int>q;
	for (int i=h[u],v;i;i=e[i].ne)
		if (!vis[v=e[i].to]) q.push_back(v);
	if (q.size()){
		sort(q.begin(),q.end());
		do{
			las[q[0]]=u,cnt++,dfs(q[0]);
			for (int i=1;i<q.size();i++) las[q[i]]=q[i-1],cnt++,dfs(q[i]);
			for (int i=0;i<q.size();i++) las[q[i]]=-1,cnt--;
		}while (next_permutation(q.begin(),q.end()));
	}
}
int main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (i=1;i<=m;i++) scanf("%d%d",&x,&y),x--,y--,add(x,y),add(y,x);
	for (i=0;i<n;i++) memset(las,-1,sizeof(las)),cnt=1,dfs(i);
	printf("%d",ans);
}
/*4 3
1 2
2 3
3 4
*/
