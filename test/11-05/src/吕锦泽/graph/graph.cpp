#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 20
#define mod 998244353
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=(x<<1)+(x<<3)+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,sum,m,Ti,mp[N][N],vis[N],b[N]; 
bool check(ll v,ll x) {
  	if (x==0) return 1; 
  	if (Ti>=23e7) { printf("%d",sum); exit(0); }
  	per(i,x,1){
    	ll u=b[i]; ++Ti; if (mp[u][v]) return 1; 
    	rep(j,1,n) if (mp[u][j]&&!vis[j]) return 0; 
  	}
  	return 0;
}
void dfs(ll x) {
  	if (x>n) { ++sum; return; }
  	per(i,n,1) if (!vis[i]&&check(i,x-1)){
      vis[i]=1; b[x]=i; 
      dfs(x+1); vis[i]=0;
    }
}

int main() {
	freopen("graph.in","r",stdin); 
  	freopen("graph.out","w",stdout); 
  	n=read(); m=read(); 
  	rep(i,1,m){
    	ll x=read(),y=read(); 
    	mp[x][y]=1; mp[y][x]=1; 
 	}
  	ll flag=0; 
  	rep(i,1,n) rep(j,1,n) 
    	if(i!=j&&!mp[i][j]){
      		flag=1; break; 
    	}
  	if (!flag){
    	ll res=1; 
    	rep(i,1,n) res=1ll*res*i%mod; 
    	printf("%d",res);
    	return 0; 
  	}
  	rep(i,1,n) mp[0][i]=1,mp[i][0]=1;
  	dfs(1);  printf("%d",sum);
}

