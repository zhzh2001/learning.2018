#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 10000005
#define mod 998244353
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=(x<<1)+(x<<3)+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,l,r,tot,ans,head[N],size[N],indeg[N],inv[N],q[N],vis[N];
struct edge{ ll to,next; }e[N<<1];
void add(ll u,ll v){ e[++tot]=(edge){v,head[u]}; ++indeg[v]; head[u]=tot; }
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read(); rep(i,2,n) add(i,read());
	inv[0]=inv[1]=size[1]=1;
	rep(i,2,n){
		inv[i]=inv[mod%i]*(mod-mod/i)%mod;
		size[i]=1; if (!indeg[i]) q[++r]=i;
	}
	while (l<r){
		ll u=q[++l]; vis[u]=1;
		ans=(ans+inv[size[u]])%mod;
		for (ll i=head[u],v=e[i].to;i;i=e[i].next,v=e[i].to){
			size[v]+=size[u]; --indeg[v];
			if (!indeg[v]&&!vis[v]) q[++r]=v;
		}
	}
	printf("%lld",ans);
}
