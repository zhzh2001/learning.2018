#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 2050
#define mod 1000000007
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=(x<<1)+(x<<3)+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,M,ans,f[2][N][N],now;
int main(){
	freopen("set.in","r",stdin);
	freopen("set.out","w",stdout);
	n=read(); m=read(); f[now][0][0]=1;
	M=1; for (;M<=max(n,m);M<<=1);
	rep(i,1,M){
		now^=1; memcpy(f[now],f[now^1],sizeof f[now]);
		rep(j,0,M) rep(k,0,M){
			if (i<=n) f[now][i^j][k]+=f[now^1][j][k];
			if (i<=m) f[now][j][i^k]+=f[now^1][j][k];
		}
	}
	rep(i,0,M) rep(j,i+1,M) (ans+=f[now][i][j])%=mod;
	printf("%lld",ans);
}
