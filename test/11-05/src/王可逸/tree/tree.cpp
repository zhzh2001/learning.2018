#include <bits/stdc++.h>
#define int long long
using namespace std;
const int mod=998244353;
int n,fa[120],fac[120],tot,sxd,a[120];
bool vis[120],vis2[120];
int ksm(int a,int b) {
	int ans=1;
	while(b) {
		if(b&1) ans=(ans*a)%mod;
		a=(a*a)%mod;
		b>>=1;
	}
	return ans;
}
int inv(int x) {	
	return ksm(x,mod-2);
}
void dfs(int at) {
	if(at==n+1) {
		int ww=0;
		memset(vis2,0,sizeof vis2);
		for(int i=1;i<=n;i++) {
			if(vis2[a[i]]) continue;
			ww++;
			int xkx=a[i];
			while(xkx!=1) {
				vis2[xkx]=true;
				xkx=fa[xkx];
			}
			vis2[1]=true;
		}
		tot+=ww;
		return;
	}
	for(int i=1;i<=n;i++) {
		if(!vis[i]) {
			a[at]=i;
			vis[i]=true;
			dfs(at+1);
			vis[i]=false;
			a[at]=0;
		}
	}
}
int fu[23];
vector <int> vv[120000];
void dfs1(int remain,int num,int last,int vvv) {
	cout<<"dfs "<<remain<<" "<<num<<" "<<last<<'\n';
	for(int i=1;i<=n;i++) cout<<vis[i]<<" ";
	cout<<'\n';
	if(remain==0) {
		puts("ok!!");
		int now=1;
		for(int i=1;i<=n;i++) cout<<vis2[i]<<" ";
		puts("");
		for(int i=1;i<=n;i++) {
			printf("fu[%d]=%d\n",i,fu[i]);
			if(!vis2[i]) {cout<<"i="<<i<<'\n';now*=(n-fu[i]);}
		}
		if(now!=1) tot+=now;
		else tot++;
		cout<<"tot="<<tot<<'\n';
		return;
	}
	for(int i=1;i<=n;i++) {
		if(vis[i]) continue;
		int xkx=i;
		vv[vvv].clear();
		while(xkx!=1) {
			if(!vis[xkx]) {vv[vvv].push_back(xkx);fu[xkx]=num;}
			vis[xkx]=true;
			xkx=fa[xkx];
		}
		if(!vis[1]) {vv[vvv].push_back(1);fu[1]=num;}
		vis[1]=true;
		int pfy=0;
		for(int j=1;j<=n;j++) if(!vis[j]) pfy++;
		vis2[i]=true;
		dfs1(pfy,num+1,i,vvv+1);
		vis2[i]=false;
		for(int j=0;j<(int)vv[vvv].size();j++) {
			vis[vv[vvv][j]]=false;
			fu[vv[vvv][j]]=0;
		}
	}	
}
signed main() {
	freopen("tree.in","r",stdin); freopen("tree.out","w",stdout);
	cin>>n;
	fac[0]=1;
	for(int i=1;i<=n;i++) fac[i]=fac[i-1]*i%mod;
	for(int i=2;i<=n;i++) cin>>fa[i];
	dfs(1);
//	dfs1(n,0,0,1);
	int ff=fac[n];
	int gg=__gcd(tot,fac[n]);
	tot/=gg;
	ff/=gg;
	cout<<tot*inv(ff)%mod;	
}
