#include <bits/stdc++.h>
#define int long long
using namespace std;
const int mod=1e9+7;
int n,m,cnt1,cnt2;
int a[1200][12],b[1200][12],aa[12],bb[12],ans;
void dfs1(int x) {
	if(x==n+1) {
		cnt1++;
		int now=0;
		for(int i=1;i<=n;i++) a[cnt1][i]=aa[i];
		for(int i=1;i<=n;i++) if(aa[i]) now^=i;
		a[cnt1][0]=now;
		return;
	}
	aa[x]=1;
	dfs1(x+1);
	aa[x]=0;
	dfs1(x+1);
}
void dfs2(int x) {
	if(x==m+1) {
		cnt2++;
		int now=0;
		for(int i=1;i<=m;i++) b[cnt2][i]=bb[i];
		for(int i=1;i<=m;i++) if(bb[i]) now^=i;
		b[cnt2][0]=now;
		return;
	}
	bb[x]=1;
	dfs2(x+1);
	bb[x]=0;
	dfs2(x+1);
}
signed main() {
	cin>>n>>m;
	dfs1(1);
	dfs2(1);
	for(int i=1;i<=cnt1;i++) 
		for(int j=1;j<=cnt2;j++) {
			bool flag=true;
			for(int k=1;k<=min(n,m);k++)
				if(a[i][k]+b[j][k]==2) {
					flag=false;
					break;
				}
			if(!flag) continue;
			if(a[i][0]<b[j][0]) ans++;
		}
	cout<<ans;
	return 0;
}
