#include <bits/stdc++.h>
#define int long long
#define For(i,a,b) for(register int i=a;i<=b;i++)
using namespace std;
const int mod=998244353;
int n,m,cnt,head[1200],fac[12],ans,now,which[3630000][11],a[12],fin[12];
bool vis[1200];
struct node {int next,to;} sxd[1200];
void add(int u,int v) {
	sxd[++cnt].next=head[u];
	sxd[cnt].to=v;
	head[u]=cnt;
}
void dfs1(int x) {
//	cout<<"x="<<x<<'\n';
	if(x==n+1) {
		cnt++;
		for(int i=1;i<=n;i++) {/*cout<<a[i]<<" ";*/which[cnt][i]=a[i];}
		//cout<<'\n';
		return;
	}
	for(int i=1;i<=n;i++) {
		if(!vis[i]) {
			vis[i]=1;
			a[x]=i;
//			printf("a[%d]=%d\n",x,i);
			dfs1(x+1);
			a[x]=0;
			vis[i]=0;	
		}
	}	
}
void ok(int ww,int x,int num) {
	if(num==n) {ans++; return;}
	for(int i=head[x];i;i=sxd[i].next)  {
		fin[sxd[i].to]=1;
	}
	if(fin[which[ww][num+1]]==1) {
		ok(ww,which[ww][num+1],num+1);	
	}
}
void subtask1() {
	cnt=0;
	dfs1(1);
	for(int i=1;i<=fac[n];i++) {
		memset(fin,0,sizeof fin);
		fin[which[i][1]]=true;
		ok(i,which[i][1],1);
	}
	cout<<ans;
}
signed main() {
	freopen("graph.in","r",stdin); freopen("graph.out","w",stdout);
	cin>>n>>m;
	fac[0]=1;
	For(i,1,n) fac[i]=fac[i-1]*i%mod;
	For(i,1,m) {
		int x,y;
		cin>>x>>y;
		add(x,y);
		add(y,x);
	}
	if(n<=10) {
		subtask1();
	}
	return 0;
}
