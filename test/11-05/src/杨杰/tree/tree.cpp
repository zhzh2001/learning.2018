#include <cstdio>
using namespace std;
#define dd c=getchar()
int read() {int s=0,w=1;char c;while (dd,c>'9' || c<'0') if (c=='-') w=-1;while (c>='0' && c<='9') s=s*10+c-'0',dd;return s*w;}
#undef dd
void write(int x) {if (x<0) putchar('-'),x=-x;if (x>=10) write(x/10);putchar(x%10|'0');}
void wln(int x) {write(x);puts("");}
void wsp(int x) {write(x);putchar(' ');}
const int N=1e7+7,P=998244353;
int n,ans;
int siz[N],fa[N],inv[N];
void Ad(int &x,int y) {x+=y;if (x>=P) x-=P;}
int ksm(int a,int b) {
	int s=1;
	for (;b;b>>=1,a=1ll*a*a%P) if (b&1) s=1ll*s*a%P;
	return s;
}
int main() {
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	for (int i=1;i<=1e6;i++) inv[i]=ksm(i,P-2);
	for (int i=2;i<=n;++i) fa[i]=read();
	for (int i=n;i>=1;i--) {
		siz[i]++;
		siz[fa[i]]+=siz[i];
		if (siz[i]<=1e6) Ad(ans,inv[siz[i]]);
		else Ad(ans,ksm(siz[i],P-2)); 
	}
	wln(ans);
}
