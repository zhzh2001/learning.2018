#include <cstdio>
using namespace std;
const int N=2005,P=1e9+7;
int n,m,ans;
int vis[N];
void dfs2(int x,int B,int A) {
	if (x>m) {
		ans+=B>A;
		if (ans>=P) ans-=P;
		return;
	}
	dfs2(x+1,B,A);
	if (!vis[x]) dfs2(x+1,B^x,A);
}
void dfs1(int x,int A) {
	if (x>n) {
		dfs2(1,0,A);
		return;
	}
	vis[x]=0;
	dfs1(x+1,A);
	vis[x]=1;
	dfs1(x+1,A^x);
}
int main() {
	freopen("set.in","r",stdin);
	freopen("set.out","w",stdout);
	scanf("%d%d",&n,&m);
	dfs1(1,0);
	printf("%d\n",ans);
}
