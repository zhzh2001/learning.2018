#include <cstdio>
#include <cstring>
using namespace std;
#define dd c=getchar()
int read() {int s=0,w=1;char c;while (dd,c>'9' || c<'0') if (c=='-') w=-1;while (c>='0' && c<='9') s=s*10+c-'0',dd;return s*w;}
#undef dd
void write(int x) {if (x<0) putchar('-'),x=-x;if (x>=10) write(x/10);putchar(x%10|'0');}
void wln(int x) {write(x);puts("");}
void wsp(int x) {write(x);putchar(' ');}
const int N=18,Mx=1<<18,P=998244353;
int n,m,ans,now;
int e[N][N],vis[N],dfn[N];
bool check(int x,int v) {
	if (x==0) return 1;
	int u=x-1;
	while (!e[dfn[u]][v] && ~u) {
		for (int j=0;j<n;j++) if (!vis[j] && e[dfn[u]][j]) return 0;
		u--;
	}
	if (u==-1) return 0;
	return 1;
}
void dfs(int x) {
	if (x>=n) {
		ans++;
		return;
	}
	for (int i=0;i<n;i++) if (!vis[i] && check(x,i)) {
		vis[i]=1;dfn[x]=i;
		dfs(x+1);
		vis[i]=0;
	}
}
int main() {
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read();m=read();
	for (int i=1,u,v;i<=m;i++) {
		u=read()-1,v=read()-1;
		e[u][v]=1;e[v][u]=1;
	}
	dfs(0);
	wln(ans%P);
}
