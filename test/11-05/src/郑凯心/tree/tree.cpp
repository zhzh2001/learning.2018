#include <vector>
#include <cstdio>
#include <iostream>

using namespace std;

const int Maxn = 1e7+7;
const int MOD = 998244353;

int n, fa[Maxn];
int vis[Maxn];
long long ans;

void exgcd(int a, int b, int &x, int &y)
{
    if(!b) { x = 1; y = 0; return; }
    exgcd(b, a%b, y, x);
    y -= a/b*x;
}

inline int mul_inverse(int a)
{
    int x, y;
    exgcd(a, MOD, x, y);
    return (x%MOD+MOD)%MOD;
}

void solve(int rest, long long val, int step)
{
    if(!rest)
    {
        ans = (ans+val*step)%MOD;
        return;
    }
    for(int i = 1; i <= n; ++i)
    {
        if(!vis[i])
        {
            int tmp = i;
            vector<int> buf;
            while(tmp && !vis[tmp])
            {
                vis[tmp] = 1;
                buf.push_back(tmp);
                tmp = fa[tmp];
            }
            solve(rest-buf.size(), val*mul_inverse(rest), step+1);
            while(buf.size())
            {
                vis[buf.back()] = 0;
                buf.pop_back();
            }
        }
    }
}

int main()
{
    freopen("tree.in", "r", stdin);
    freopen("tree.out", "w", stdout);
    scanf("%d\n", &n);
    for(int i = 2; i <= n; ++i)
        scanf("%d", fa+i);
    /*
    if(n == 3)
    {
        if(fa[2] == 1)
        {
            if(fa[3] == 1) puts("332748120");
            if(fa[3] == 2) puts("831870296");
            return 0;
        }
    }
    */
    solve(n, 1, 0);
    printf("%d\n", (int)ans);
    return 0;
}
