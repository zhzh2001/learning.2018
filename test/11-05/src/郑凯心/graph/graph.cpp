#include <stack>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>

using namespace std;

const int Maxn = 20;
const int Maxm = 200;
const int MOD = 998244353;

int n, m, ans;
int vis[Maxn], a[Maxn], e[Maxn][Maxn];

inline int check()
{
    stack<int> s;
    memset(vis, 0, sizeof vis);
    s.push(a[1]);
    vis[a[1]] = 1;
    for(int i = 2; i <= n; ++i)
    {
        vis[a[i]] = 1;
        while(s.size() && !e[s.top()][a[i]])
        {
            for(int i = 1; i <= n; ++i)
                if(e[s.top()][i] && ! vis[i]) return 0;
            s.pop();
        }
        if(!s.size()) return 0;
        s.push(a[i]);
    }
    return 1;
}

int main()
{
    freopen("graph.in", "r", stdin);
    freopen("graph.out", "w", stdout);
    scanf("%d%d", &n, &m);
    for(int i = 1, u, v; i <= m; ++i)
    {
        scanf("%d%d", &u, &v);
        e[u][v] = e[v][u] = 1;
    }
    for(int i = 1; i <= n; ++i) a[i] = i;
    do
    {
        ans += check();
        if(ans >= MOD) ans -= MOD;
        /*
#ifdef DEBUG
        if(check())
        {
            for(int i = 1; i <= n; ++i)
                printf("%d ", a[i]);
            putchar('\n');
        }
#endif
        */
    } while(next_permutation(a+1, a+n+1));
    printf("%d\n", ans);
    return 0;
}
