#include <bits/stdc++.h>
#define int long long
using namespace std;
#define gc getchar
inline int read(){
	char ch=gc();int t=0;bool p=0;
	for(;!isdigit(ch);ch=gc())if(ch=='-')p=1;
	for(;isdigit(ch);ch=gc())t=t*10+ch-48;
	return p?-t:t;
}
const int P=998244353;
int n,m,ans,u,v,s[20];
bool f[20][20],vis[20];
inline int add(int x,int y){return x+y>=P?x+y-P:x+y;}
signed main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=m;i++){
		u=read();v=read();
		f[u][v]=f[v][u]=1;
	}
	s[0]=1;
	for(int i=1;i<=n;i++)s[i]=s[i-1]*i%P;
	if(m==n*(n-1)/2)cout<<s[n];else cout<<s[n-1]*2;
	return 0;
}
