#include <bits/stdc++.h>
#define int long long
using namespace std;
char *S,*T,STR[1000000];
inline char gc(){
	if(S==T)T=(S=STR)+fread(STR,1,1000000,stdin);
	return *S++;
}
inline int read(){
	char ch=gc();int t=0;bool p=0;
	for(;!isdigit(ch);ch=gc())if(ch=='-')p=1;
	for(;isdigit(ch);ch=gc())t=t*10+ch-48;
	return p?-t:t;
}
const int N=10000005;
const int P=998244353;
int n,f[N],s[N],ny[N],sum,D;
int ksm(int x,int y){
	int ans=1;
	for(;y;y>>=1,x=x*x%P)if(y&1)ans=ans*x%P;
	return ans;
}
signed main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();sum=1;
	for(int i=2;i<=n;i++)f[i]=read();
	for(int i=n;i;i--)s[f[i]]+=++s[i];
	for(int i=2;i<=n;i++)sum=sum*i%P;
	for(int i=1;i<=n;i++)ny[i]=ksm(i,P-2);
	for(int i=1;i<=n;i++)D=(D+sum*ny[s[i]]%P*(s[i]-1))%P;
	cout<<(sum*n-D+P)%P*ksm(sum,P-2)%P;
	return 0;
}
