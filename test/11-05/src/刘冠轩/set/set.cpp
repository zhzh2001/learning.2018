#include <bits/stdc++.h>
using namespace std;
#define gc getchar
inline int read(){
	char ch=gc();int t=0;bool p=0;
	for(;!isdigit(ch);ch=gc())if(ch=='-')p=1;
	for(;isdigit(ch);ch=gc())t=t*10+ch-48;
	return p?-t:t;
}
const int P=1e9+7;
int n,m,ans,Y,vis[100];
void dfs2(int x,int y){
	if(x>m){if(y>Y)ans++;return;}
	if(!vis[x]){
		vis[x]=1;
		dfs2(x+1,y^x);
		vis[x]=0;
	}dfs2(x+1,y);
}
void dfs(int x,int y){
	if(x>n){Y=y;dfs2(1,0);return;}
	vis[x]=1;
	dfs(x+1,y^x);
	vis[x]=0;
	dfs(x+1,y);
}
int main(){
	freopen("set.in","r",stdin);
	freopen("set.out","w",stdout);
	n=read();m=read();
	dfs(1,0);
	cout<<ans;
	return 0;
}
