#include<bits/stdc++.h>
using namespace std;
int n,m,sum1,sum2,ans1,ans2,ans;
int a[2001],b[2001];
void find(int x){
	if(x>max(n,m)){
		if(sum1)
			ans1=a[1];
		else
			ans1=0;
		if(sum2)
			ans2=b[1];
		else
			ans2=0;
		for(int i=2;i<=sum1;i++)
			ans1^=a[i];
		for(int i=2;i<=sum2;i++)
			ans2^=b[i];
		if(ans1<ans2)
			ans++;
		ans%=1000000007;
		return;
	}
	if(n<m){
		find(x+1);
		sum2++;
		b[sum2]=x;
		find(x+1);
	}
	else{
		find(x+1);
		sum1++;
		a[sum1]=x;
		find(x+1);
	}
	return;
}
void search(int x){
	if(x>min(n,m)){
		find(x);
		return;
	}
	search(x+1);
	sum1++;
	a[sum1]=x;
	search(x+1);
	sum1--;
	sum2++;
	b[sum2]=x;
	search(x+1);
}
int main(){
	freopen("set.in","r",stdin);
	freopen("set.out","w",stdout);
	scanf("%d%d",&n,&m);
	search(1);
	printf("%d",ans);
	return 0;
}
