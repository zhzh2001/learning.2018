#include<bits/stdc++.h>
#define int long long
using namespace std;
const int p=998244353;
int n,m,sum,ans;
int father[10000001],inv[10000001],flag[10000001];
void search(int x){
	for(int I=1;I<=n;I++)
		if(!flag[I]){
			for(int i=1;i<=n;i++)
				if(!flag[i]){
					int num=i;
					while(num!=1){
						flag[num]++;
						num=father[num];
					}
					flag[1]++;
					search(x+1);
					num=i;
					while(num!=1){
						flag[num]--;
						num=father[num];
					}
					flag[1]--;
				}
			return;
		}
	ans+=x-1;
	sum++;
	return;
}
signed main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	scanf("%lld",&n);
	father[1]=1;
	for(int i=2;i<=n;i++)
		scanf("%lld",&father[i]);
	search(1);
	inv[1]=1;
    for(int i=2;i<=sum+2;i++)
        inv[i]=(int)(p-p/i)*inv[p%i]%p;
    printf("%lld\n",(ans%p)*(inv[sum]%p)%p);
	return 0;
}
