#include<cstdio>
#include<algorithm>
using namespace std;
inline int read(){
	int x=0,f=1; char c=getchar();
	while (c<'0'||c>'9') {
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
void write(int x){
	if (x<0){
		putchar('-');
		x=-x;
	}
	if (x>=10) write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x); puts("");
}
const int N=257,mod=1e9+7;
int ans,n,m,dp[N][N][N];
inline void init(){
	n=read(); m=read();
}
inline void update(int &x,int y){
	x+=y;
	if (x>=mod) x-=mod;
}
inline void solve(){
	int t=max(n,m); dp[0][0][0]=1;
	for (int i=1;i<=t;i++){
		for (int j=0;j<256;j++){
			for (int k=0;k<256;k++){
				if (i<=n) update(dp[i][j^i][k],dp[i-1][j][k]);
				if (i<=m) update(dp[i][j][k^i],dp[i-1][j][k]);
				update(dp[i][j][k],dp[i-1][j][k]);
			}
		}
	}
	for (int i=0;i<256;i++){
		for (int j=i+1;j<256;j++){
			update(ans,dp[t][i][j]);
		}
	}
	writeln(ans);
}
int main(){
	freopen("set.in","r",stdin); freopen("set.out","w",stdout);
	init(); solve();
	return 0;
}
