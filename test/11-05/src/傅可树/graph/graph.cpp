#include<cstdio>
using namespace std;
inline int read(){
	int x=0,f=1; char c=getchar();
	while (c<'0'||c>'9') {
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
void write(int x){
	if (x<0){
		putchar('-');
		x=-x;
	}
	if (x>=10) write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x); puts("");
}
const int N=20,mod=998244353;
bool vis[N];
int now,n,ans,m,mat[N][N];
inline void init(){
	n=read(); m=read();
	for (int i=1;i<=m;i++){
		int u=read(),v=read();
		mat[u][v]=mat[v][u]=1;
	}
}
inline void solve(){
	ans=1;
	for (int i=1;i<=n;i++){
		ans=ans*i%mod;
	}
	writeln(ans);
}
int main(){
	freopen("graph.in","r",stdin); freopen("graph.out","w",stdout);
	
	init(); solve();
	return 0;
}
