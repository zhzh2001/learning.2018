#include<cstdio>
#define int long long
using namespace std;
inline int read(){
	int x=0,f=1; char c=getchar();
	while (c<'0'||c>'9') {
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
void write(int x){
	if (x<0){
		putchar('-');
		x=-x;
	}
	if (x>=10) write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x); puts("");
}
const int N=1e7+5,mod=998244353;
int n,du[N],cnt;
inline void init(){
	n=read();
	for (int i=2;i<=n;i++){
		du[read()]++;
	}
	for (int i=1;i<=n;i++){
		if (!du[i]) cnt++;
	}
}
inline int Pow(int x,int k){
	int y=1;
	for (;k;k>>=1,x=x*x%mod) if (k&1) y=y*x%mod;
	return y;
}
int pre,now,ans,fac[N],inv_fac[N];
inline void solve(){
	fac[0]=1;
	for (int i=1;i<=n;i++) fac[i]=fac[i-1]*i%mod;
	inv_fac[n]=Pow(fac[n],mod-2);
	for (int i=n-1;i>=0;i--) inv_fac[i]=inv_fac[i+1]*(i+1)%mod;
	for (int i=cnt;i<=n;i++){
		now=fac[n-cnt]*fac[i]%mod*inv_fac[n]%mod*inv_fac[i-cnt]%mod;
		int tmp=pre; pre=now; now=(now-tmp+mod)%mod;
		ans=(ans+i*now%mod)%mod;
	}
	writeln(ans);
}
signed main(){
	init(); solve();
	return 0;
}
