#include<cstdio>
#define int long long
using namespace std;
inline int read(){
	int x=0,f=1; char c=getchar();
	while (c<'0'||c>'9') {
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
void write(int x){
	if (x<0){
		putchar('-');
		x=-x;
	}
	if (x>=10) write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x); puts("");
}
const int N=1e7+5,mod=998244353;
struct edge{
	int link,next;
}e[N<<1];
int inv[N],tot,g[N],size[N],head[N],n,dp[N];
inline void add_edge(int u,int v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
}
inline void insert(int u,int v){
	add_edge(u,v); add_edge(v,u);
}
inline void init(){
	n=read();
	for (int i=2;i<=n;i++){
		insert(i,read());
	}
}
void dfs(int u,int fa){
	size[u]=1;
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=fa){
			dfs(v,u);
			size[u]+=size[v];
		}
	}
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=fa){
			g[u]=(g[u]+g[v]+dp[v]*(size[u]-size[v])%mod)%mod;
		}
	}
	dp[u]=(g[u]*inv[size[u]]%mod+1);
}
inline void solve(){
	inv[1]=1;
	for (int i=2;i<=n;i++) inv[i]=inv[mod%i]*(mod-mod/i)%mod;
	dfs(1,0);
	writeln(dp[1]);
}
signed main(){
	freopen("tree.in","r",stdin); freopen("tree.out","w",stdout);
	init(); solve();
	return 0;
}
