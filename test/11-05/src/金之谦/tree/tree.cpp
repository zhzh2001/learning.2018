#include <bits/stdc++.h>
#define int long long
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define getchar gc
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=1e7+10,MOD=998244353;
inline int mi(int a,int b){
	int x=1,y=a;
	for(;b;b>>=1,y=y*y%MOD)if(b&1)x=x*y%MOD;
	return x;
}
int n,s[N],fa[N];
signed main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	for(int i=2;i<=n;i++)fa[i]=read();
	int ans=n,sum=1;
	for(int i=n;i;i--){
		s[i]++;
		ans=((ans-sum+MOD)%MOD*s[i]%MOD+sum)%MOD;
		sum=sum*s[i]%MOD;
		s[fa[i]]+=s[i];
	}
	ans=ans*mi(sum,MOD-2)%MOD;
	writeln(ans);
	return 0;
}
/*
1:7/3
2:11/6
*/
