#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=1010,MOD=998244353;
int nedge=0,p[2*N],nex[2*N],head[2*N];
inline void addedge(int a,int b){
	p[++nedge]=b;nex[nedge]=head[a];head[a]=nedge;
}
int d[20][20];
int f[20][1000010],n,m,s[N],top=0;
inline void upd(int &x,int y){x=(x+y)%MOD;}
inline int dfs(int x,int v){
	if(f[x][v])return f[x][v];
	bool flag=1;
	for(int k=head[x];k;k=nex[k])if((v&(1<<(p[k]-1)))==0){
		flag=0;s[++top]=p[k];
		upd(f[x][v],dfs(p[k],v|(1<<(p[k]-1))));
		top--;
	}
	if(flag){
		upd(f[x][v],dfs(s[--top],v));
		s[++top]=x;
	}
}
inline void Main(){
	static int a[20],ans=0,b[20];
	for(int i=1;i<=n;i++)a[i]=i;
	do{
		memset(b,0,sizeof b);
		top=1;bool flag=1;s[1]=a[1];b[a[1]]=1;
		for(int i=2;i<=n;i++){
			while(top>1){
				int flag=0;
				for(int j=1;j<=n;j++)if(d[s[top]][j]&&!b[j])flag=1;
				if(flag)break;
				top--;
			}
			if(!d[s[top]][a[i]]){flag=0;break;}
			s[++top]=a[i];b[a[i]]=1;
		}
		if(flag)ans++;
	}while(next_permutation(a+1,a+n+1));
	writeln(ans);exit(0);
}
signed main()
{
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read();m=read();
	if(m==n*(n-1)/2){
		int ans=1;
		for(int i=2;i<=n;i++)ans=ans*i%MOD;
		writeln(ans);return 0;
	}
	for(int i=1;i<=m;i++){
		int x=read(),y=read();
		d[x][y]=d[y][x]=1;
		addedge(x,y);addedge(y,x);
	}
	if(n<12)Main();
	int ans=0,p=(1<<n)-1;
	for(int i=1;i<=n;i++)f[i][p]=1;
	for(int i=1;i<=n;i++){
		top=1;s[top]=i;
		ans=(ans+dfs(i,1<<(i-1)))%MOD;
	}
	writeln(ans);
	return 0;
}
