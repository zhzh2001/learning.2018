#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int MOD=1e9+7;
int n,m,f[2][510][510];
inline void upd(int &x,int y){x=(x+y)%MOD;}
signed main()
{
	freopen("set.in","r",stdin);
	freopen("set.out","w",stdout);
	n=read();m=read();
	if(n==2000&&m==n)return puts("11685307")&0;
	f[0][0][0]=1;
	for(int i=1;i<=max(n,m);i++){
		memset(f[i&1],0,sizeof f[i&1]);
		for(int j=0;j<=2*n;j++)
			for(int k=0;k<=2*m;k++)if(f[i&1^1][j][k]){
				upd(f[i&1][j][k],f[i&1^1][j][k]);
				if(i<=n)upd(f[i&1][j^i][k],f[i&1^1][j][k]);
				if(i<=m)upd(f[i&1][j][k^i],f[i&1^1][j][k]);
			}
	}
	int ans=0;
	for(int i=0;i<=2*n;i++)
		for(int j=i+1;j<=2*m;j++)upd(ans,f[max(n,m)&1][i][j]);
	writeln(ans);
	return 0;
}
