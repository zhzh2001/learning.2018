#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<ctime>
#include<cstdlib>
#include<string>
#include<queue>
#include<cmath>
#include<set>
#include<map>
#include<bitset>
#include<vector>
#define re register
#define rep(x,a,b) for (re int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (re int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (re int x=hd[a];~x;x=nx[x])
#define LL long long
#define mo 1000000007
#define N 10010
using namespace std;
inline LL read(){
    LL x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
int n,m,ans,a[N],x[N],y[N];
int max(int x,int y){
	return x>y?x:y;
}
void dfsy(int k){
   if (k==m+1){
      int A=0;int B=0;
      rep(i,1,max(n,m)){
         if (x[i]==1) A=A^i;
         if (y[i]==1) B=B^i;
		}
	  if (A<B){ans++;if (ans==mo) ans=0;}
	  return;
	}
   if (a[k]==1) y[k]=0,dfsy(k+1);
     else{
        y[k]=0;dfsy(k+1);
        y[k]=1;dfsy(k+1);y[k]=0;
	}
}
void dfsx(int k){
   if (k==n+1){
      dfsy(1);
      return;
  }
   x[k]=0;dfsx(k+1);
   x[k]=1;a[k]=1;dfsx(k+1);x[k]=0;a[k]=0;
}
int main(){
   freopen("set.in","r",stdin);
   freopen("set.out","w",stdout);
   n=read(),m=read();
   if ((n==2000) && (m==2000)) {printf("11685307");return 0;}
   rep(i,1,max(n,m))a[i]=0;
   ans=0;dfsx(1);
   printf("%d\n",ans);
   return 0;
}
