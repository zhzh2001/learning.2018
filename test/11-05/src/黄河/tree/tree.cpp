#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<ctime>
#include<cstdlib>
#include<string>
#include<queue>
#include<cmath>
#include<set>
#include<map>
#include<bitset>
#include<vector>
#define re register
#define rep(x,a,b) for (re int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (re int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (re int x=hd[a];~x;x=nx[x])
#define LL long long
#define mo 998244353
#define N 20000010
using namespace std;
int n,m,i,j,k,l,r;
int fa[N],son[N],f[N],size[N];
LL ans,inv[N];
inline LL read(){
    LL x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
LL qpow(LL x,LL y){
	LL res=1;
	for (;y;x=x*x%mo,y>>=1)if(y&1) res=res*x%mo;
	return res;
}
inline void pre(int n){
	inv[1]=1;
	rep(i,2,n) inv[i]=inv[mo%i]%mo*(mo-mo/i)%mo;
}
int main(){
	freopen("tree.in","r",stdin);	
	freopen("tree.out","w",stdout);
	n=read(); ans=0;
	pre(n);
	rep(i,2,n){fa[i]=read(); son[fa[i]]++;}
	rep(i,1,n) f[i]=son[i],size[i]=1;
	rep(i,1,n)
		if(son[i]==0){
			j=i;
			while(j){
				size[fa[j]]+=size[j];
				j=fa[j]; f[j]--;
				if(f[j]) break;
			}
		}
	rep(i,1,n) ans=(ans+inv[size[i]])%mo;
	printf("%lld",ans);
	return 0;
}
