#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<ctime>
#include<cstdlib>
#include<string>
#include<queue>
#include<cmath>
#include<set>
#include<map>
#include<bitset>
#include<vector>
#define re register
#define rep(x,a,b) for (re int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (re int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (re int x=hd[a];~x;x=nx[x])
#define LL long long
#define mo 998244353
#define N 1010
using namespace std;
inline LL read(){
    LL x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
int n,m,cnt,to[N<<1],nxt[N<<1],ans,a[N][N],b[N],c[N],H[N],ind[N],fa[N],head[N],id[N];
inline void add(int u,int v){
	to[++cnt]=v;
	nxt[cnt]=head[u];
	head[u]=cnt;
}
bool check(){
   rep(i,1,n)fa[i]=0,ind[i]=0,id[c[i]]=i;
   rep(i,2,n){
      int j=c[i-1];
      while (j!=0){
         if (a[c[i]][j]==1)  {fa[c[i]]=j;break;}
	       else if (ind[j]!=H[j]) return 0;
	     j=fa[j];
	  }
	  if (fa[c[i]]==0) return 0;
	  for (int j=head[c[i]];j;j=nxt[j]){
	     int jj=to[j];
	     if (id[jj]<i)  ind[jj]++,ind[c[i]]++;
	  }
   }
   return 1;
}
inline void Solve(int k){
   if (k==n+1){
      if (check()){
	     ans=ans+1;if (ans==998244353) ans=0;
	  }
	  return;
   }
   rep(i,1,n)
     if (b[i]==1)
        c[k]=i,b[i]=0,Solve(k+1),b[i]=1;
}
int main(){
   freopen("graph.in","r",stdin);
   freopen("graph.out","w",stdout);
   n=read(),m=read();
   rep(i,1,m){
      int x,y;
      x=read(),y=read();
      add(x,y);add(y,x);
      a[x][y]=1;a[y][x]=1;
      H[x]=H[x]+1;H[y]=H[y]+1;
	 }
   rep(i,1,n)b[i]=1;
   ans=0;Solve(1);
   printf("%d",ans);
   return 0;
}
