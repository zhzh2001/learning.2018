//#include<bits/stdc++.h>
//using namespace std;
//inline char gc()
//{
//    static char buf[100000],*p1=buf,*p2=buf;
//    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
//}
//#define getchar gc
//long long read()
//{
//	char ch=getchar();long long x=0,ff=1;
//	while(ch<'0'||ch>'9') {if(ch=='-') ff=-1;ch=getchar();}
//	while(ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
//	return x*ff;
//}
//void write(long long aa)
//{
//	if(aa<0) putchar('-'),aa=-aa;
//	if(aa>9) write(aa/10);
//	putchar(aa%10+'0');
//	return;
//}
//const long long mod=998244353;
//long long n,m,ans;
//long long vis[19];
//long long zhan[19],jl[19][19],qq[19],top,mp[19][19];
//void dfs(long long now)
//{
//	if(now==n) {ans++;if(ans>=mod) ans-=mod;return;}
//	qq[now]=top;
//	for(long long i=1;i<=qq[now];++i) jl[now][i]=zhan[i];
//	long long ff=0;
//	while(!ff&&top)
//	{
//		for(long long i=1;i<=n;++i)
//		if(mp[zhan[top]][i]&&!vis[i])
//		{
//			ff=1;
//			vis[i]=1;
//			zhan[++top]=i;
//			dfs(now+1);
//			top--;
//			vis[i]=0;
//		}
//		if(ff) break;
//		else top--;
//	}
//	top=qq[now];
//	for(long long i=1;i<=qq[now];++i) zhan[i]=jl[now][i];
//	return;
//}
//long long main()
//{
//	freopen("graph.in","r",stdin);
//	freopen("graph.out","w",stdout);
//	n=read(),m=read();
//	for(long long i=1;i<=m;i++)
//	{
//		long long x=read(),y=read();
//		mp[x][y]=1;mp[y][x]=1;
//	}
//	for(long long i=1;i<=n;++i) {zhan[++top]=i;vis[i]=1;dfs(1);vis[i]=0;top--;}
//	write(ans);
//	return 0;
//}
#include<bits/stdc++.h>
using namespace std;
inline char gc()
{
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define getchar gc
long long read()
{
	char ch=getchar();long long x=0,ff=1;
	while(ch<'0'||ch>'9') {if(ch=='-') ff=-1;ch=getchar();}
	while(ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x*ff;
}
void write(long long aa)
{
	if(aa<0) putchar('-'),aa=-aa;
	if(aa>9) write(aa/10);
	putchar(aa%10+'0');
	return;
}
const long long mod=998244353;
long long n,m,ans;
long long mp[19][19];
long long ini[19][530005],dfn[19][530005];
long long tl(long long,long long);
long long dfs(long long,long long);
long long init(long long,long long);
long long init(long long rt,long long zt)
{
	if(ini[rt][zt]!=-1) return ini[rt][zt];
	long long now=(1<<(rt-1)),tmp;
	zt^=now;
	for(long long i=1;i<=n;++i)
	if(zt&(1<<(i-1))&&mp[rt][i])
	{
		tmp=init(i,zt);
		zt^=tmp;
		now|=tmp;
	}
	return now;
}
long long tl(long long zt,long long now)
{
	if(!now) return 1;
	long long sum=0;
	for(long long i=1;i<=n;++i)
	if((zt&(1<<(i-1)))&&(now&(1<<(i-1))))
	{
		long long tmp=init(i,now);
		sum=(sum+dfs(i,tmp)*tl(zt^(1<<(i-1)),now^tmp)%mod)%mod;
	}
	return sum;
}
long long dfs(long long rt,long long zt)
{
	if((1<<(rt-1))==zt) return dfn[rt][zt]=1;
	if(dfn[rt][zt]!=-1) return dfn[rt][zt];
	long long now=0;
	for(long long i=1;i<=n;++i) if(mp[rt][i]&&(zt&(1<<(i-1)))) now|=(1<<(i-1));
	return dfn[rt][zt]=tl(now,zt^(1<<(rt-1)));
}
int main()
{
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	memset(dfn,-1,sizeof(dfn));
	memset(ini,-1,sizeof(ini));
	n=read(),m=read();
	for(long long i=1;i<=m;i++)
	{
		long long x=read(),y=read();
		mp[x][y]=1;mp[y][x]=1;
	}
	for(long long i=1;i<=n;++i) ans=(ans+dfs(i,(1<<n)-1))%mod;
	write(ans);
	return 0;
}
