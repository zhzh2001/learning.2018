#include<bits/stdc++.h>
using namespace std;
inline char gc()
{
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define getchar gc
long long read()
{
	char ch=getchar();long long x=0,ff=1;
	while(ch<'0'||ch>'9') {if(ch=='-') ff=-1;ch=getchar();}
	while(ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x*ff;
}
void write(long long aa)
{
	if(aa<0) putchar('-'),aa=-aa;
	if(aa>9) write(aa/10);
	putchar(aa%10+'0');
	return;
}
const int mod=998244353;
int n,sum;
int fa[10000005],sz[10000005],ny[10000005];
int ksm(long long aa,int bb)
{
	long long ans=1;
	while(bb)
	{
		if(bb&1) ans=(ans*aa)%mod;
		bb>>=1;
		aa=(aa*aa)%mod;
	}
	return ans;
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	for(int i=2;i<=n;++i) fa[i]=read();
	ny[1]=1;
	for(int i=n;i>=1;--i)
	{
		sz[i]+=1;
		sz[fa[i]]+=sz[i];
		if(!ny[sz[i]]) ny[sz[i]]=ksm(sz[i],mod-2);
		sum+=ny[sz[i]];
		if(sum>=mod) sum-=mod;
	}
	write(sum);
	return 0;
}
