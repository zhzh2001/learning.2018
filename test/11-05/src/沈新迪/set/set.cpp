#include<bits/stdc++.h>
using namespace std;
inline char gc()
{
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define getchar gc
long long read()
{
	char ch=getchar();long long x=0,ff=1;
	while(ch<'0'||ch>'9') {if(ch=='-') ff=-1;ch=getchar();}
	while(ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x*ff;
}
void write(long long aa)
{
	if(aa<0) putchar('-'),aa=-aa;
	if(aa>9) write(aa/10);
	putchar(aa%10+'0');
	return;
}
const int mod=1e9+7;
int n,m,now,ans,wss;
long long f[2][2055][2055];
int main()
{
	freopen("set.in","r",stdin);
	freopen("set.out","w",stdout);
	n=read(),m=read();
	f[now][0][0]=1;
	for(int i=0;i<=11;++i) if(max(n,m)<(1<<i)) {wss=i;break;}
	for(int i=1;i<=max(n,m);++i)
	{
		int qq=now;
		now^=1;
		for(int A=0;A<(1<<wss);++A)
		for(int B=0;B<(1<<wss);++B)
		if(f[qq][A][B])
		{
			f[now][A][B]=(f[now][A][B]+f[qq][A][B])%mod;
			if(i<=n) f[now][A^i][B]=(f[now][A^i][B]+f[qq][A][B])%mod;
			if(i<=m) f[now][A][B^i]=(f[now][A][B^i]+f[qq][A][B])%mod;
			f[qq][A][B]=0;
		}
	}
	for(int i=0;i<(1<<wss);++i)
	for(int j=i+1;j<(1<<wss);++j) 
	if(f[now][i][j]) ans=(ans+f[now][i][j])%mod;
	write(ans);
	return 0;
}
