#include<bits/stdc++.h>
using namespace std;
#define mod 998244353
inline void Add(int &x,int y){x=x+y>=mod?x+y-mod:x+y;}
int n;bool edge[21][21];
int cango[(1<<18)+100][21];
int f[(1<<18)+100][19];bool vis[(1<<18)+100][19];
inline int dp(int x,int now)
{
	if((1<<(now-1))==x) return 1;
	if(vis[x][now]) return f[x][now];
	vis[x][now]=1;f[x][now]=0;
	for(int i=1;i<=n;i++) if(edge[now][i]) if(x&(1<<(i-1)))
	{
		int nxt=cango[x^(1<<(now-1))][i]&x,Nxt=x^nxt;
		Add(f[x][now],1ll*dp(nxt,i)*dp(Nxt,now)%mod);
	}
	return f[x][now];
}
bool Vis[21];
inline void dfs(int x,int &to,int lim)
{
	if((lim&(1<<(x-1)))==0) return;if(Vis[x]) return;to|=1<<(x-1);Vis[x]=1;
	for(int i=1;i<=n;i++) if(edge[x][i]) dfs(i,to,lim);
}
inline void getcango()
{
	for(int i=0;i<(1<<n);i++) for(int j=1;j<=n;j++) if(i&(1<<(j-1)))
	{
		memset(Vis,0,sizeof(Vis));
		dfs(j,cango[i][j],i);
	}
}
inline void init()
{
	int m;scanf("%d%d",&n,&m);
	for(int i=1,a,b;i<=m;i++) scanf("%d%d",&a,&b),edge[a][b]=edge[b][a]=1;
}
int main()
{
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	init();getcango();
	int ans=0;
	for(int i=1;i<=n;i++) Add(ans,dp((1<<n)-1,i));
	cout<<ans;return 0;
}
