#include<bits/stdc++.h>
using namespace std;
#define mod 998244353
inline char gc()
{
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline int read()
{
	int x=0;char c=gc();
	for(;!isdigit(c);c=gc());
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);return x;
}
int inv[10000100],n,fa[10000100],size[10000100],f[10000100];
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	inv[1]=1;for(int i=2;i<=10000000;i++) inv[i]=1ll*(mod-mod/i)*inv[mod%i]%mod;
	n=read();for(int i=2;i<=n;i++) fa[i]=read();
	for(int i=n;i;i--)
	{
		size[i]++;
		if(fa[i])size[fa[i]]+=size[i];
	}
	for(int x=n;x;x--)
	{
		f[x]++;
		if(fa[x]) (f[fa[x]]+=(1ll*f[x]-1ll*size[x]*inv[size[fa[x]]]%mod+mod)%mod)%=mod;
	}
	cout<<f[1];
}
