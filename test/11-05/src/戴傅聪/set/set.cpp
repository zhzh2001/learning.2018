#include<bits/stdc++.h>
using namespace std;
#define mod 1000000007
int f[2][2049][2049];
int n,m;
inline void Add(int &x,int y){x=x+y>=mod?x+y-mod:x+y;}
int main()
{
	freopen("set.in","r",stdin);
	freopen("set.out","w",stdout);
	scanf("%d%d",&n,&m);
	int cur=max(n,m),cnt=0;
	for(;cur;cur>>=1,cnt++);
	int T=(1<<cnt)-1;
	f[0][0][0]=1;
	for(int i=1;i<=max(n,m);i++)
	{
		int now=i&1,lst=now^1;
		for(int j=0;j<=T;j++) for(int k=0;k<=T;k++)
		{
			f[now][j][k]=f[lst][j][k];
			if(i<=n) Add(f[now][j][k],f[lst][j^i][k]);
			if(i<=m) Add(f[now][j][k],f[lst][j][k^i]);
		}
	}
	int ans=0;
	for(int i=0;i<=T;i++) for(int j=i+1;j<=T;j++) Add(ans,f[max(n,m)&1][i][j]);
	cout<<ans;return 0;
}
