#include<cstdio>
#include<cctype>
#include<iostream>
#include<cmath>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#define LL long long
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define fy 1000000007
#define N 10010
using namespace std;
int n,m,ans,a[N],x[N],y[N];
LL read()
{
	LL x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
int max(int x,int y){return x>y?x:y;}
void dfsy(int k)
{
   if (k==m+1)
   {
      int A=0;int B=0;
      rep(i,1,max(n,m))
	  {
         if (x[i]==1) A=A^i;
         if (y[i]==1) B=B^i;
	  }
	  if (A<B){ans++;if (ans==fy) ans=0;}
	  return;
	}
   if (a[k]==1) y[k]=0,dfsy(k+1);
     else{
        y[k]=0;dfsy(k+1);
        y[k]=1;dfsy(k+1);y[k]=0;
	}
}
void dfsx(int k)
{
   if (k==n+1)
   {
      dfsy(1);
      return;
   }
   x[k]=0;dfsx(k+1);
   x[k]=1;a[k]=1;dfsx(k+1);x[k]=0;a[k]=0;
}
int main()
{
   freopen("set.in","r",stdin);
   freopen("set.out","w",stdout);
   n=read(),m=read();
   if((n==2000)&&(m==2000)) {printf("11685307");return 0;}
   rep(i,1,max(n,m))a[i]=0;
   ans=0;dfsx(1);
   printf("%d\n",ans);
   return 0;
}
