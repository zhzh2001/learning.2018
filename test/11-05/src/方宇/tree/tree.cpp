#include<cstdio>
#include<cctype>
#include<iostream>
#include<cmath>
#include<cstring>
#include<algorithm>
#include<map>
#include<set>
#define LL long long
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define fy 998244353
#define N 20000010
using namespace std;
int n,m,i,j,k,l,r;
int fa[N],son[N],f[N],size[N];
LL sum[N],ans;
int read()
{
	int x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
int main()
{
	freopen("tree.in","r",stdin);	
	freopen("tree.out","w",stdout);
	n=read(); ans=0;
	rep(i,2,n){fa[i]=read(); son[fa[i]]++;}
	rep(i,1,n) f[i]=son[i],size[i]=1;
	rep(i,1,n)
		if(son[i]==0)
		{
			j=i;
			while(j)
			{
				size[fa[j]]+=size[j];
				j=fa[j]; f[j]--;
				if(f[j]) break;
			}
		}	
	sum[1]=1;
	rep(i,2,n) sum[i]=sum[fy%i]%fy*(fy-fy/i)%fy;
	rep(i,1,n) ans=(ans+sum[size[i]])%fy;
	printf("%lld",ans);
	return 0;
}
