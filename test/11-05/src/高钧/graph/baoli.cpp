#include <bits/stdc++.h>
#define M(a,v) memset(a,v,sizeof a)
#define PB push_back
using namespace std;
#define int long long
const int N=20,B=262149,md=998244353;
int n,m,f[N][B],re=0,up;
vector<int>e[N];
inline int dfs(int x,int no,int la)
{
	int in1=0; if(((1<<(x-1))|(no))==up){return f[x][no]=1;} f[x][no]=0;
	for(vector<int>::iterator it=e[x].begin();it!=e[x].end();it++)if((no&(1<<(*it-1)))==0)
	{
		f[x][no]=(f[x][no]+dfs(*it,no|(1<<(x-1)),x))%md; in1++;
	}if(!in1)return f[x][no]=dfs(la,no,x);else return f[x][no];
}
signed main()
{
	freopen("graph.in","r",stdin);
	int i,j,x,y; scanf("%lld%lld",&n,&m); up=(1<<n)-1;
	for(i=1;i<=m;i++)
	{
		scanf("%lld%lld",&x,&y); e[x].PB(y); e[y].PB(x);
	}for(i=1;i<=n;i++){M(f,-1);dfs(i,0,0);}printf("%lld\n",re);
}
