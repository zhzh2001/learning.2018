#include <bits/stdc++.h>
#define M(a,v) memset(a,v,sizeof a)
#define PB push_back
using namespace std;
#define int long long
const int N=20,B=262149,md=998244353;
int n,m,f[N][B],pre[N],vis[N],jc[N],no[N];
vector<int>e[N];
//inline void bfs_pre(int s)
//{
//	int x; queue<int>q; q.push(s); M(no,-1); no[s]=0;
//	while(!q.empty())
//	{
//		x=q.front(); q.pop();
//		for(vector<int>::iterator it=e[x].begin();it!=e[x].end();it++)if(~no[*it])
//		{
//			no[*it]=no[x]|(1<<(x-1)); q.push(*it);
//		}
//	}
//}
//inline void dfs_pre(int x,int bo)
//{
//	for(vector<int>::iterator it=e[x].begin();it!=e[x].end();it++)if((bo&(1<<(*it-1)))==0)
//	{
//		
//	}
//}
inline int dfs(int x,int no)
{
	int in1=0,re=1LL; if(~f[x][no])return f[x][no];
	for(vector<int>::iterator it=e[x].begin();it!=e[x].end();it++)if((no&(1<<(*it-1)))==0)
	{
		re=re*dfs(*it,no|(1<<(x-1)))%md; in1++;
	}
	re=re*jc[in1]%md;
//	printf("%lld %lld %lld\n",x,no,re);
	return f[x][no]=re;
}
signed main()
{
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	int i,j,x,y,wa=0LL; scanf("%lld%lld",&n,&m); jc[0]=1LL; for(i=1;i<=n;i++)jc[i]=jc[i-1]*i%md;
	for(i=1;i<=m;i++)
	{
		scanf("%lld%lld",&x,&y); e[x].PB(y); e[y].PB(x);
	}
	for(i=1;i<=n;i++)
	{
		M(f,-1); wa=(wa+dfs(i,0))%md;
	}printf("%lld\n",wa);
}
