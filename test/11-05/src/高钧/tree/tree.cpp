#include <bits/stdc++.h>
using namespace std;
#define int long long
const int N=10000005,M=10000005,md=998244353;
int n,tot=0,Next[M],to[M],head[M],re,sz[N],rt;
bool in1[N],ou1[N];
inline int read(){int s=0; char c; while(!isdigit(c))c=getchar(); while(isdigit(c)){s=s*10+c-'0';c=getchar();}return s;}
#define R(x) x=read()
struct node{int fz,fm;}f[N];
inline int gcd(int x,int y){if(!y)return x;else return gcd(y,x%y);}
inline node yf(node re){int gg=gcd(re.fm,re.fz); re.fm/=gg; re.fz/=gg; return re;}
inline node operator*(const node &p,const node &q){node re=p; re.fm*=q.fm; re.fz*=q.fz; return yf(re);}
inline node operator+(const node &p,const node &q){node re=p; if(re.fm==q.fm){re.fz+=q.fz;return re;}int x=re.fm; re.fm*=q.fm; re.fz*=q.fm; re.fz+=q.fz*x; return yf(re);}
inline void Add(int x,int y){Next[++tot]=head[x]; to[tot]=y; head[x]=tot; in1[y]=1; ou1[x]=1;}
inline int ksm(int x,int y){int re=1;for(;y;y>>=1){if(y&1)re=re*x%md; x=x*x%md;}return re;}
inline void dfs(int x)
{
	int i; sz[x]=1; if(!ou1[x]){f[x]=(node){1,1};return;} f[x].fm=1,f[x].fz=0;
	for(i=head[x];i;i=Next[i]){dfs(to[i]); sz[x]+=sz[to[i]]; f[x]=f[x]+f[to[i]];}
	node aa,bb,cc; aa.fm=bb.fm=sz[x]; aa.fz=cc.fm=cc.fz=1; bb.fz=sz[x]-1; f[x]=(aa*(f[x]+cc))+(bb*f[x]);
}
signed main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	int i,x;  R(n); for(i=2;i<=n;i++){R(x); Add(x,i);}
	for(i=1;i<=n;i++)if(!in1[i])rt=i; dfs(rt);
	printf("%lld\n",f[rt].fz*ksm(f[rt].fm,md-2)%md);
}
