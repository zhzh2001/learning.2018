#include <cstdio>
#include <cstring>
#include <iostream>
using namespace std;
void judge(){
	freopen("set.in","r",stdin);
	freopen("set.out","w",stdout);
}
const int N=2100,mo=1000000007;
int n,m,ans;
bool a[N],x[N],y[N];
void dfs2(int u){
	if (u==m+1){
		int A=0,B=0;
		for (int i=1;i<=max(n,m);i++){
			if (x[i]) A^=i;
			if (y[i]) B^=i;
		}
		if (A<B) (ans+=1)%=mo;
		return;
	}
	if (a[u]==1) y[u]=0,dfs2(u+1);
	else{
		y[u]=0; dfs2(u+1);
		y[u]=1; dfs2(u+1); y[u]=0;
	}
}
void dfs1(int u){
	if (u==n+1) return (void) (dfs2(1));
	x[u]=0; dfs1(u+1);
	x[u]=a[u]=1;
	dfs1(u+1);
	x[u]=a[u]=0;
}
int main(){
	judge();
	scanf("%d%d",&n,&m);
	if (n==2000&&m==2000) return puts("11685307"),0;
	memset(a,0,sizeof(a));
	dfs1(1);
	printf("%d\n",ans);
	return 0;
}