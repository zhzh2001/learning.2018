#include <map>
#include <ctime>
#include <cstdio>
#include <vector>
#include <cstdlib>
#include <cstring>
#include <iostream>
using namespace std;
void judge(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
}
const int N=20,mo=998244353;
int n,m,ans,v[N][N],opt[N],cnt;
map<string,bool> M;
bool used[N];
bool check(){
	string s="";
	for (int i=1;i<=n;i++){
		if (opt[i]>9) s=s+(char)(opt[i]/10+'0')+(char)(opt[i]%10+'0');
		else s=s+(char)(opt[i]+'0');
	}
	cerr<<s<<endl;
	if (!M[s]) return M[s]=1,1;
	return 0;
}
void dfs(int u){
	if (cnt==n) return (void) ((ans+=check())%=mo);
	for (int i=1;i<=v[u][0];i++){                                                                                                                                                                                                        
		int k=v[u][i];
		if (!used[k]){
			used[k]=1; opt[++cnt]=k;
			dfs(k);
		}
	}
}
int main(){
	judge();
	srand(time(NULL));
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++){
		int x,y; scanf("%d%d",&x,&y);
		v[x][++v[x][0]]=y;
		v[y][++v[y][0]]=x;
	}
	for (int i=1;i<=n;i++){
		memset(used,0,sizeof(used));
		opt[cnt=1]=i; used[i]=1;
		dfs(i);
	}
	printf("%d\n",ans);
	return 0;
}
