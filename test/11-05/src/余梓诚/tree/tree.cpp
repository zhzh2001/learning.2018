#include <cstdio>
#include <cstring>
#include <iostream>
using namespace std;
void judge(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
}
char buf[80000000],*ps=buf;
void read(int &x){
	for (;!isdigit(*ps);++ps) ;
	for (;isdigit(*ps);++ps) x=(x<<1)+(x<<3)+(*ps^'0');
}
const int N=10000005,mo=998244353;
int n,m,size[N],fa[N],inv[N];
long long ans;
int main(){
	judge();
	buf[fread(buf, 1, 80000000, stdin)] = '\n';
	read(n);
	for (int i=2;i<=n;i++) read(fa[i]);
	for (int i=n;i>=2;i--) size[i]++,size[fa[i]]+=size[i];
	size[1]++;
	inv[1]=1;
	for (int i=2;i<=n;i++) inv[i]=inv[mo%i]*1ll*(mo-mo/i)%mo;
	for (int i=1;i<=n;i++) ans+=inv[size[i]];
	printf("%lld\n",ans%mo);
	return 0;
}
