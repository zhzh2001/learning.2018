#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0){x=-x;putchar('-');}if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln (ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
const int N=20;
int n,m,d[N][N],ans,vis[N],q[N];

inline bool emp(int x)
{
	For(i,1,n)	if(d[x][i])	if(!vis[i])	return 0;
	return 1;
}
inline void Dfs(int now)
{
	if(now==n+1)	{ans++;return;}
	For(i,1,n)	if(!vis[i])
	{
		bool ok=0;
		Dow(j,1,now-1)
		{
			if(d[i][q[j]])	{ok=1;break;}
			if(!emp(q[j]))	break;
		}
		if(ok)	
		{
			q[now]=i;vis[i]=1;
			Dfs(now+1);
			vis[i]=0;q[now]=0;
		}
	}
}
int main()
{
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	n=read();m=read();
	if(m==n*(n-1)/2)
	{
		ll ans=1;
		For(i,1,n)	ans=ans*i%998244353;
		writeln(ans);
		return 0;
	}
	For(i,1,m)
	{
		int x=read(),y=read();
		d[x][y]=d[y][x]=1;
	}
	For(i,1,n)
	{
		vis[i]=1;
		q[1]=i;
		Dfs(2);
		vis[i]=0;
	}
	writeln(ans);
}
