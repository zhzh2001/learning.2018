#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(itn i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0){x=-x;putchar('-');}if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln (ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}

int n,m,mo=1e9+7;
unsigned int dp[301][301],tmp[301][301];
int main()
{
	freopen("set.in","r",stdin);
	freopen("set.out","w",stdout);
	n=read();m=read();
	dp[0][0]=1;
	For(now,1,min(n,m))
	{
		For(i,0,256)
			For(j,0,256)
				tmp[i][j]+=dp[i^now][j]+dp[i][j^now]+dp[i][j],tmp[i][j]%=mo;
		For(i,0,256)	For(j,0,256)	dp[i][j]=tmp[i][j],tmp[i][j]=0;
	}
	if(n>m)	
	{
		For(now,m+1,n)	
			For(i,0,256)
				For(j,0,256)
					tmp[i][j]+=dp[i^now][j]+dp[i][j],tmp[i][j]%=mo;
			For(i,0,256)	For(j,0,256)	dp[i][j]=tmp[i][j],tmp[i][j]=0;
	}else
	if(m>n)
	{	
		For(now,n+1,m)	
			For(i,0,256)
				For(j,0,256)
					tmp[i][j]+=dp[i][j^now]+dp[i][j],tmp[i][j]%=mo;
			For(i,0,256)	For(j,0,256)	dp[i][j]=tmp[i][j],tmp[i][j]=0;
	}
	int ans=0;
	For(i,0,256)	For(j,i+1,256)	ans+=dp[i][j],ans%=mo;
	writeln(ans);
}	
