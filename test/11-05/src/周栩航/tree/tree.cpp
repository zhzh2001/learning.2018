#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0){x=-x;putchar('-');}if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln (ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
int n,k;
const int N=1e7+5;
ll mo=998244353,fa[N],sz[N],rev[N],fac[N];
inline ll ksm(ll x,ll y){ll sum=1;for(;y;y/=2,x=x*x%mo)	if(y&1)	sum=sum*x%mo;return sum;}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	For(i,2,n)	fa[i]=read(),sz[i]=1;
	sz[1]=1;
	Dow(i,1,n)	sz[fa[i]]+=sz[i];
	ll ans=0;
	fac[0]=1;
	For(i,1,n)	fac[i]=fac[i-1]*i%mo;
	rev[n]=ksm(fac[n],mo-2);
	Dow(i,0,n-1)	rev[i]=rev[i+1]*(i+1)%mo;
	
	For(i,1,n)	ans+=(rev[sz[i]]*fac[sz[i]-1]%mo),ans%=mo;
	writeln(ans);
}
