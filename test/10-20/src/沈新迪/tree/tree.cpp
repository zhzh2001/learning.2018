#include<bits/stdc++.h>
using namespace std;
int n,ff=0,S,T;
int vis[100005];
double cs,sumx,sumy,ans,sum;
int tot,head[100005],nx[200005],to[200005];
double X[100005],Y[100005];
int son[100005],fa[100005],dep[100005];
double sz[100005];
long long read()
{
	char ch=getchar();long long x=0,ff=1;
	while(ch<'0'||ch>'9') {if(ch=='-') ff=-1;ch=getchar();}
	while(ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x*ff;
}
void write(long long aa)
{
	if(aa<0) putchar('-'),aa=-aa;
	if(aa>9) write(aa/10);
	putchar(aa%10+'0');
	return;
}
void jia(int aa,int bb)
{
	tot++;
	nx[tot]=head[aa];
	to[tot]=bb;
	head[aa]=tot;
	return;
}
void dfs(int rt,int faa)
{
	fa[rt]=faa;dep[rt]=dep[faa]+1;
	for(int i=head[rt];i;i=nx[i])
	{
		int yy=to[i];
		if(yy==faa) continue;
		dfs(yy,rt);
		son[rt]++;
	}
	double sum=0;
	for(int i=head[rt];i;i=nx[i])
	{
		int yy=to[i];
		if(yy==faa) continue;
		sum+=(sz[yy]+2)/(double)son[rt];
	}
	sz[rt]=sum;
	return;
}
void getfa(int aa,int bb)
{
	int qa=0,qb=0;
	while(aa!=bb)
	{
		sum++;
		if(dep[aa]>=dep[bb])
		{
			for(int i=head[aa];i;i=nx[i])
			{
				int yy=to[i];
				if(yy==fa[aa]||yy==qa) continue;
				sum+=(sz[yy]+2)/(double)son[aa];
			}
			qa=aa;
			aa=fa[aa];
		}
		else
		{
			qb=bb;
			bb=fa[bb];
			for(int i=head[bb];i;i=nx[i])
			{
				int yy=to[i];
				if(yy==aa||yy==fa[bb]||yy==qb) continue;
				sum+=(sz[yy]+2)/(double)son[bb];
			}
		}
	}
	return;
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	for(int i=1;i<n;++i) 
	{
		int x=read(),y=read();
		if(abs(y-x)!=1) ff=0;
		jia(x,y);jia(y,x);
	}
	for(int i=1;i<=n;++i) scanf("%lf%lf",&X[i],&Y[i]),sumx+=X[i],sumy+=Y[i];
	dfs(1,1);
	for(int i=1;i<=n;++i) if(X[i]) 
	for(int j=1;j<=n;++j) if(Y[j])
	{
		S=i,T=j;
		memset(vis,0,sizeof(vis));
		sum=0;
		getfa(S,T);
		ans+=X[i]/sumx*Y[j]/sumy*sum;
	}
	printf("%.10lf",ans);
	return 0;
}
