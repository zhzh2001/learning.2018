#include<bits/stdc++.h>
using namespace std;
const long long mod=1e9+7;
long long T;
long long h,w,m,n,ans,sum,anssum;
long long mp[55][55],jl,tmp;
struct node
{
	long long ax,ay,bx,by,col;
}a[15];
long long read()
{
	char ch=getchar();long long x=0,ff=1;
	while(ch<'0'||ch>'9') {if(ch=='-') ff=-1;ch=getchar();}
	while(ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x*ff;
}
void write(long long aa)
{
	if(aa<0) putchar('-'),aa=-aa;
	if(aa>9) write(aa/10);
	putchar(aa%10+'0');
	return;
}
long long ksm(long long aa,long long bb)
{
	long long ss=1;
	while(bb)
	{
		if(bb&1) ss=(ss*aa)%mod;
		bb>>=1;
		aa=(aa*aa)%mod;
	}
	return ss;
}
bool cmp(node aa,node bb)
{
	return aa.col>bb.col;
}
int main()
{
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	T=read();
	while(T--)
	{
		h=read(),w=read(),m=read(),n=read();ans=1;
		for(long long i=1;i<=n;++i) a[i].ax=read(),a[i].ay=read(),a[i].bx=read(),a[i].by=read(),a[i].col=read();
		sort(a+1,a+n+1,cmp);
		for(long long l=1,r=1;l<=n;l=r+1)
		{
			while(a[r+1].col==a[l].col) r++;
			anssum=1;
			jl=0;tmp=0;
			for(int i=l;i<=r;++i)
			{
				memset(mp,0,sizeof(mp));
				sum=(a[i].bx-a[i].ax+1)*(a[i].by-a[i].ay+1);
				for(long long j=l;j<=n;++j)
				if(i!=j&&a[j].col<=a[i].col)
				{
					for(long long x=max(a[j].ax,a[i].ax);x<=min(a[j].bx,a[i].bx);++x)
					for(long long y=max(a[j].ay,a[i].ay);y<=min(a[j].by,a[i].by);++y) if(!mp[x][y]) sum--,mp[x][y]=a[j].col;else mp[x][y]=a[j].col;
				}
				jl=(jl+sum);
				for(int x=a[i].ax;x<=a[i].bx;++x)
				for(int y=a[i].ay;y<=a[i].by;++y) if(mp[x][y]==a[i].col) tmp++;
				anssum=(anssum*(ksm(a[i].col,sum)-ksm(a[i].col-1,sum)+mod)%mod)%mod;
			}
			tmp/=2;
			ans=ans*((ksm(a[l].col,tmp)-ksm(a[l].col-1,tmp)+mod)%mod*ksm(a[l].col,jl)%mod+ksm(a[l].col-1,tmp)*anssum%mod)%mod;
		}
		memset(mp,0,sizeof(mp));
		sum=h*w;
		for(long long i=1;i<=n;++i)
		for(long long x=a[i].ax;x<=a[i].bx;++x)
		for(long long y=a[i].ay;y<=a[i].by;++y) if(!mp[x][y]) sum--,mp[x][y]=1;
		ans=ksm(m,sum)*ans%mod;
		write(ans);puts("");
	}
	return 0;
}
/*
2
3 3 2 2
1 1 2 2 2
2 2 3 3 1
4 4 4 4
1 1 2 3 3
2 3 4 4 2
2 1 4 3 2
1 2 3 4 4
*/
