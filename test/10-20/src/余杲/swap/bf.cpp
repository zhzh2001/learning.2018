#include<vector>
#include<cstdio>
#include<cstdlib>
#include<iostream>
using namespace std;
int n,a[1000],deg[1000];
vector<pair<int,int>>v,opt;
bool used[1000000];
void dfs(int k){
	if(k>v.size()){
		for(int i=1;i<=n;i++)
			if(a[i]!=i)return;
		cout<<"puts(\"Yes\");"<<endl;
		for(pair<int,int>p:opt)cout<<"puts(\""<<p.first<<' '<<p.second<<"\");"<<endl;
		exit(0);
	}
	for(int i=1;i<=n;i++)
		if(deg[i]==n-1&&a[i]!=i)return;
	for(int i=0;i<v.size();i++)
		if(!used[i]){
			used[i]=1;
			opt.push_back(v[i]);
			swap(a[v[i].first],a[v[i].second]);
			deg[v[i].first]++,deg[v[i].second]++;
			dfs(k+1);
			opt.pop_back();
			deg[v[i].first]--,deg[v[i].second]--;
			swap(a[v[i].first],a[v[i].second]);
			used[i]=0;
		}
}
int main(){
	freopen("out.txt","w",stdout);
	cin>>n;
	for(int i=1;i<=n;i++)a[i]=i;
	for(int i=1;i<=n/2;i++){
		for(int j=i+1;j<n-i+1;j++){
			deg[i]++,deg[j]++;
			opt.push_back(make_pair(i,j));
			swap(a[i],a[j]);
//			cout<<i<<' '<<j<<endl;
		}
		for(int j=n-i+1;j<=n;j++)v.push_back(make_pair(i,j));
	}
	for(int i=n/2+1;i<=n;i++)
		for(int j=i+1;j<=n;j++)
			v.push_back(make_pair(i,j));
	dfs(1);
	cout<<"No"<<endl;
}
