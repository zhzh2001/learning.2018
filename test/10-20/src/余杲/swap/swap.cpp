#include<cstdio>
#include<iostream>
#include<vector>
#include<cstdlib>
using namespace std;
int n,a[1000],deg[1000];
vector<pair<int,int> >v,opt;
bool used[1000000];
void dfs(int k){
	if(k>v.size()){
		for(int i=1;i<=n;i++)
			if(a[i]!=i)return;
		for(int i=0;i<opt.size();i++)cout<<opt[i].first<<' '<<opt[i].second<<endl;
		exit(0);
	}
	for(int i=1;i<=n;i++)
		if(deg[i]==n-1&&a[i]!=i)return;
	for(int i=0;i<v.size();i++)
		if(!used[i]){
			used[i]=1;
			opt.push_back(v[i]);
			swap(a[v[i].first],a[v[i].second]);
			deg[v[i].first]++,deg[v[i].second]++;
			dfs(k+1);
			opt.pop_back();
			deg[v[i].first]--,deg[v[i].second]--;
			swap(a[v[i].first],a[v[i].second]);
			used[i]=0;
		}
}
void bf(){
puts("1 2");
puts("1 3");
puts("1 4");
puts("1 5");
puts("1 6");
puts("1 7");
puts("1 8");
puts("2 3");
puts("2 4");
puts("2 5");
puts("2 6");
puts("2 7");
puts("3 4");
puts("3 5");
puts("3 6");
puts("4 5");
puts("2 8");
puts("3 7");
puts("3 8");
puts("4 6");
puts("4 7");
puts("4 9");
puts("5 6");
puts("5 7");
puts("5 9");
puts("3 9");
puts("5 8");
puts("4 8");
puts("6 7");
puts("7 8");
puts("6 8");
puts("6 9");
puts("1 9");
puts("8 9");
puts("2 9");
puts("7 9");
}
int main(){
	freopen("swap.in","r",stdin);
	freopen("swap.out","w",stdout);
	cin>>n;
	if(n%4>1)return puts("NO"),0;
	puts("YES");
	if(n==9)return bf(),0;
	for(int i=1;i<=n;i++)a[i]=i;
	for(int i=1;i<=n/2;i++){
		for(int j=i+1;j<n-i+1;j++){
			deg[i]++,deg[j]++;
			opt.push_back(make_pair(i,j));
			swap(a[i],a[j]);
		}
		for(int j=n-i+1;j<=n;j++)v.push_back(make_pair(i,j));
	}
	for(int i=n/2+1;i<=n;i++)
		for(int j=i+1;j<=n;j++)
			v.push_back(make_pair(i,j));
	dfs(1);
}
