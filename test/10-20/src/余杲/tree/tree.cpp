#include<cstdio>
#include<cstring>
#include<iostream>
using namespace std;
inline char _gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return (p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2))?EOF:*p1++;
}
#define gc c=_gc()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
const int MAXN=1e5+5;
int nedge,head[MAXN];
struct Edge{
	int to,nxt;
}edge[MAXN<<1];
void add(int x,int y){
	edge[++nedge].to=y;
	edge[nedge].nxt=head[x];
	head[x]=nedge;
}
int n,x[MAXN],y[MAXN],X,Y,deg[MAXN],siz[MAXN];
double dfs(int k,int fa,int step){
	double ans=1.0*step*y[k]/Y;
	for(int i=head[k];i;i=edge[i].nxt){
		int u=edge[i].to;
		if(u==fa)continue;
		ans+=1.0/(deg[k]-1)*dfs(u,k,step+1)+1.0*(deg[k]-2)/(deg[k]-1)*dfs(u,k,n-siz[u]);
	}
	return ans;
}
void dfs(int k,int fa){
	siz[k]=1;
	for(int i=head[k];i;i=edge[i].nxt){
		int u=edge[i].to;
		if(u==fa)continue;
		dfs(u,k);
		siz[k]+=siz[u];
	}
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	for(int i=1;i<n;i++){
		int x=read(),y=read();
		add(x,y),add(y,x);
		deg[x]++,deg[y]++;
	}
	for(int i=1;i<=n;i++){
		x[i]=read(),y[i]=read();
		X+=x[i],Y+=y[i];
	}
	double ans=0;
	for(int i=1;i<=n;i++)
		if(x[i]){
			memset(siz,0,sizeof(siz));
			dfs(i,0);
			ans+=1.0*x[i]/X*dfs(i,0,0);
		}
	printf("%.10lf",ans);
}
