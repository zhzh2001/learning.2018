#include<cstdio>
#include<iostream>
#include<algorithm>
using namespace std;
#define gc c=getchar()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
const int P=1e9+7;
long long pw(int a,int n,int P){
	long long ans=1;
	while(n){
		if(n&1)ans=1LL*ans*a%P;
		a=1LL*a*a%P;
		n>>=1;
	}
	return ans;
}
int h,w,m,n;
long long ans;
int x1[15],y1[15],x2[15],y2[15],v[15];
int a[55][55];
void dfs(int x,int y){
	if(x>h){
		for(int cur=1;cur<=n;cur++){
			int Mx=0;
			for(int i=x1[cur];i<=x2[cur];i++)
				for(int j=y1[cur];j<=y2[cur];j++)
					Mx=max(a[i][j],Mx);
			if(Mx!=v[cur])return;
		}
		ans=(ans+1)%P;
		return;
	}
	if(y>w){
		dfs(x+1,1);
		return;
	}
	for(int i=1;i<=m;i++){
		a[x][y]=i;
		dfs(x,y+1);
	}
}
int num,b[5000],cnt[5000];
void bf(){
	for(int i=1;i<=h;i++)
		for(int j=1;j<=w;j++)
			a[i][j]=m+1;
	for(int cur=1;cur<=n;cur++)
		for(int i=x1[cur];i<=x2[cur];i++)
			for(int j=y1[cur];j<=y2[cur];j++)
				a[i][j]=min(a[i][j],v[cur]);
	num=0;
	for(int i=1;i<=h;i++)
		for(int j=1;j<=w;j++)
			b[++num]=a[i][j];
	sort(b+1,b+num+1);
	num=unique(b+1,b+num+1)-b-1;
	for(int i=1;i<=h;i++)
		for(int j=1;j<=w;j++){
			int pos=lower_bound(b+1,b+num+1,a[i][j])-b;
			cnt[pos]++;
		}
	ans=1;
	for(int i=1;i<num;i++){
		int val=((pw(b[i],cnt[i],P)-pw(b[i]-1,cnt[i],P))%P+P)%P;
		ans=1LL*ans*val%P;
	}
	ans=1LL*ans*pw(m,cnt[num],P)%P;
}
int main(){
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	for(int T=read();T;T--){
		h=read(),w=read(),m=read(),n=read();
		ans=0;
		for(int i=1;i<=n;i++){
			x1[i]=read(),y1[i]=read();
			x2[i]=read(),y2[i]=read();
			v[i]=read();
		}
		if(m<=2)dfs(1,1);
		else bf();
		printf("%lld\n",ans);
	}
}
