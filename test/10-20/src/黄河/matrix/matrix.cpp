#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<ctime>
#include<cstdlib>
#include<string>
#include<queue>
#include<cmath>
#include<set>
#include<map>
#include<bitset>
#include<vector>
#define re register
#define rep(x,a,b) for (re int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (re int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (re int x=hd[a];~x;x=nx[x])
#define LL long long
#define mo 1000000007
#define N 1010
using namespace std;
int T,h,w,m,n,X1[20],X2[20],Y1[20],Y2[20],V[20],a[109][109];
LL ans,Ji;
int read(){
int x=0,fh=0; char c=getchar();
	while(!isdigit(c)) fh=(c=='-')?1:fh,c=getchar();
	while(isdigit(c)) x=(x<<3)+(x<<1)+(c^'0'),c=getchar();
return fh?-x:x;
}
int max(int x,int y){
return (x>y)?x:y;
}
int Findmax(int X_1,int Y_1,int X_2,int Y_2){
int sum=0;
	rep(i,X_1,X_2)
	 rep(j,Y_1,Y_2)
	 sum=max(sum,a[i][j]);
	 
	return sum;
}
bool Judge(){
	rep(i,1,n) if(Findmax(X1[i],Y1[i],X2[i],Y2[i])!=V[i]) 
	 return 0;
	return 1;
}
void dfs(int x,int y){
if(x>h) {if(Judge()) ++ans; return;}
	int xx=x,yy=y+1;
	 if(yy>w) ++xx,yy=1;
	 
	 rep(i,1,m){
	 	a[x][y]=i;
	 	dfs(xx,yy);
	 }
}
inline void task1(){
	if (n==1){
		X1[1]=read(); Y1[1]=read(); X2[1]=read(); Y2[1]=read(); V[1]=read();
		ans=1LL*(1<<((X2[1]-X1[1])*(Y2[1]-Y1[1])*V[1]))%mo;
		ans=(ans*1LL*(1<<((h*w-(X2[1]-X1[1])*(Y2[1]-Y1[1]))*m))%mo)%mo;
		printf("%lld",ans);
		return;
	}else{
		X1[1]=read(); Y1[1]=read(); X2[1]=read(); Y2[1]=read(); V[1]=read();
		X1[2]=read(); Y1[2]=read(); X2[2]=read(); Y2[2]=read(); V[2]=read();
		LL Sum1=(X2[1]-X1[1])*(Y2[1]-Y1[1]);
		LL Sum2=(X2[2]-X1[2])*(Y2[2]-Y1[2]);
		if ((X2[1]>X1[2])||(X1[1]<X2[2])) Ji=(min(Y2[1],Y2[2])-max(Y1[1],Y1[2]))*abs(X2[1]-X1[2]);
		if ((Y1[1]<Y2[2])||(Y2[1]>Y1[2])) Ji=(min(X2[1],X2[2])-max(X1[1],X1[2]))*abs(Y2[1]-Y1[2]);
		if (V[1]>V[2]) Sum1-=Ji;else Sum2-=Ji;
		ans=(1LL*(1<<(Sum1*V[1]))%mo+1LL*(1<<(Sum2*V[2]))%mo)%mo;
		ans=(ans*1LL*(1<<(h*w-Sum1-Sum2*m))%mo)%mo;
		printf("%lld",ans);
	}
	
}
int main(){
   freopen("matrix.in","r",stdin); freopen("matrix.out","w",stdout);
	T=read();
	while(T--){
		h=read(); w=read(); m=read(); n=read();
		if (n<=2&&(h>4||w>4||m>2))task1();else{
		rep(i,1,n){
			X1[i]=read(); Y1[i]=read(); X2[i]=read(); Y2[i]=read(); V[i]=read();
		}	
		ans=0;	
		dfs(1,1);
		printf("%lld\n",ans);
	}
	}
return 0;
}
