#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<ctime>
#include<cstdlib>
#include<string>
#include<queue>
#include<cmath>
#include<set>
#include<map>
#include<bitset>
#include<vector>
#define re register
#define rep(x,a,b) for (re int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (re int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (re int x=hd[a];~x;x=nx[x])
#define LL long long
#define mo 1000000007
#define N 1010
using namespace std;
using namespace std;
struct arr{
   int TO,nx;
}Edge[200010];
int Hd[100010],Fa[100010][21],Depth[100010],x,y,n,Cnt,l,r;
inline void swap(int x,int y){
	      int t=x;x=y;y=t;
}
int maxdeep(int k){
   return ((int)(log((k)*1.0)/log(2.0)));
}
int lowbit(int k){
   return (k&(-k));
}
void add(int x,int y){
   Cnt=Cnt+1;Edge[Cnt].TO=y;Edge[Cnt].nx=Hd[x];Hd[x]=Cnt;
}
void dfs(int u,int k){
   for (int i=Hd[u];i;i=Edge[i].nx){
      int j=Edge[i].TO;
      if (j==k) continue;
      Depth[j]=Depth[u]+1;Fa[j][0]=u;dfs(j,u);
   }
}
int lca(int x,int y){
   if (Depth[x]<Depth[y]) swap(x,y);
   for (int i=Depth[x]-Depth[y];i;i=i-lowbit(i)) x=Fa[x][maxdeep(lowbit(i))];
   if (x==y) return x;
   drp(i,maxdeep(2*n),0)
     if (Fa[x][i]!=Fa[y][i]) x=Fa[x][i],y=Fa[y][i];
   return Fa[x][0];
}
int main(){
  	freopen("tree.in","r",stdin);
  	freopen("tree.out","w",stdout);
   scanf("%d",&n);
   rep(i,1,n-1){
      int x,y;
      scanf("%d%d",&x,&y);
      add(x,y);add(y,x);
   }
   rep(i,1,n){
      int x,y;
      scanf("%d%d",&x,&y);
      if (x==1) l=i;if (y==1) r=i; 
   }
   Depth[1]=1;
   dfs(1,0);
   for (int i=1;i<=maxdeep(2*n);i++)
   rep(i,1,maxdeep(2*n))
     rep(j,1,n)
       Fa[j][i]=Fa[Fa[j][i-1]][i-1];
///   for (int i=1;i<=n;i++)
//     for (int j=0;j<=20;j++) printf("%d %d %d\n",i,j,Fa[i][j]);
 //  for (int i=1;i<=n;i++) printf("%d\n",Depth[i]);
   int sum=lca(l,r);//printf("%d %d %d\n",l,r,sum);
   if (sum==l){
      double s=Depth[r]-Depth[l];
      printf("%0.8lf\n",s);
      return 0;
   }
   if (sum==r){
      double s=Depth[l]-Depth[r];
      printf("%0.8lf\n",s);
      return 0;
   }
   double s=Depth[l]+Depth[r]-2*Depth[sum];
   printf("%0.8lf\n",s);
   return 0;
}
