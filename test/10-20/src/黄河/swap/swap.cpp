#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<ctime>
#include<cstdlib>
#include<string>
#include<queue>
#include<cmath>
#include<set>
#include<map>
#include<bitset>
#include<vector>
#define re register
#define rep(x,a,b) for (re int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (re int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (re int x=hd[a];~x;x=nx[x])
#define LL long long
#define mo 1000000007
#define N 1010
using namespace std;
int n;
int main(){
	freopen("swap.in","r",stdin);
	freopen("swap.out","w",stdout);
	scanf("%d",&n);
	if (n%4==2||n%4==3){
		printf("NO");
		return 0;
   }else
   if (n%4==1||n%4==0){
   	printf("YES\n");
   	if (n==4){
   		printf("1 2\n");
   		printf("3 4\n");
   		printf("2 4\n");
   		printf("1 3\n");
   		printf("2 3\n");
   		printf("1 4\n");
   	}
   	if (n==5){
   		printf("1 2\n");
   		printf("1 3\n");
   		printf("1 4\n");
   		printf("2 3\n");
   		printf("2 5\n");
   		printf("3 5\n");
   		printf("1 5\n");
   		printf("3 4\n");
   		printf("2 4\n");
   		printf("4 5\n");
   	}
   }
   return 0;
}
