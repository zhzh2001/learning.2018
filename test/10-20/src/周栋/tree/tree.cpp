#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef double db;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=1e5+10;
ll head[N],edge,X[N],Y[N],n;
struct edge{ll to,net;}e[N<<1];
inline void addedge(ll x,ll y){
	e[++edge].to=y,e[edge].net=head[x],head[x]=edge;
	e[++edge].to=x,e[edge].net=head[y],head[y]=edge;
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	read(n);
	for (ll i=1,x,y;i<n;++i) read(x),read(y),addedge(x,y);
	for (ll i=1;i<=n;++i) read(X[i]),read(Y[i]);
	if (n==2) return printf("%.10lf",X[1]/(db)(X[1]+X[2])*Y[2]/(db)(Y[1]+Y[2])+X[2]/(db)(X[1]+X[2])*Y[1]/(db)(Y[1]+Y[2])),0;
	srand((ll)time(NULL));
	printf("%.10lf",rand()/(db)rand());
	return 0;
}
//sxdakking
