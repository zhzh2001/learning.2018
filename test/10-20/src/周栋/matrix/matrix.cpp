#include <iostream>
#include <cstdio>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll P=1e9+7;
ll T,ans,h,w,m,n,X[15],Y[15],XX[15],YY[15],V[15],a[10][10];
inline void dfs(ll x,ll y){
	if (y>w) ++x,y=1;
	if (x>h){
		for (ll i=1;i<=n;++i){
			ll t=0;
			for (ll xx=X[i];xx<=XX[i];++xx)
				for (ll yy=Y[i];yy<=YY[i];++yy)
					t=max(a[xx][yy],t);
			if (t==V[i]) return;
		}
		++ans;
		return;
	}
	a[x][y]=1;
	dfs(x,y+1);
	if (m==1) return;
	a[x][y]=2;
	dfs(x,y+1);
}
inline ll pow(ll x,ll y){
	ll ret=1;
	for (;y;y>>=1,x=x*x%P) if (y&1) ret=ret*x%P;
	return ret;
}
int main(){
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	read(T);
	while (T--){
		read(h),read(w),read(m),read(n);
		for (ll i=1;i<=n;++i) read(X[i]),read(Y[i]),read(XX[i]),read(YY[i]),read(V[i]);
		if (h<5&&w<5&&m<3){
			ans=0;dfs(1,1);
			wr(ans);
			puts("");
			continue;
		}
		if (n==0){
			wr(pow(m,h*w));
			puts("");
			continue;
		}
		if (n==1){
			wr((pow(m,h*w)-pow(V[1],(XX[1]-X[1]+1)*(YY[1]-Y[1]+1)-1)+P)%P);
			puts("");
			continue;
		}
	}
	return 0;
}
//sxdakking
