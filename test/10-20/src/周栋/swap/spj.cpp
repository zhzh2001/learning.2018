#include <iostream>
#include <cstdio>
#include <cstdlib>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=1000;
ll n,tot,a[N],id[N],ins[N];
bool vis[N][N];
struct node{
	ll x,y;
	node(){}
	node(ll X,ll Y):x(X),y(Y){}
	inline void print(){
		if (x>y) swap(x,y);
		wr(x),pc(' '),wr(y),puts("");
	}
}ans[N*N];
inline void swap_id(ll x,ll y){
	ans[++tot]=node(x,y);
	swap(a[id[x]],a[id[y]]);
	swap(id[x],id[y]);
	vis[x][y]=vis[y][x]=1;
}
int main(){
	freopen("swap.in","r",stdin);
	read(n);
	freopen("swap.out","r",stdin);
	string s;
	cin>>s;
	freopen("ans.out","w",stdout);
	if (s=="NO"&&(n%4==3||n%4==2)) return puts("YES"),0;
	else if (s=="NO") return puts("WW"),0;
	if (n==4) return puts("YES"),0;
	for (ll i=1;i<=n;++i) id[i]=a[i]=i;
	for (ll i=1,x,y;i<=n*(n-1)/2;++i)
		read(x),read(y),swap_id(x,y);
	for (ll i=1;i<=n;++i) if (a[i]!=i) return puts("WA"),0;
	puts("YES");
	return 0;
}
