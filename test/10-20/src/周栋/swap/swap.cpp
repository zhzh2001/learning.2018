#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=1000;
ll n,tot,a[N],id[N],ins[N];
bool vis[N][N];
struct node{
	ll x,y;
	node(){}
	node(ll X,ll Y):x(X),y(Y){}
	inline void print(){
		if (x>y) swap(x,y);
		wr(x),pc(' '),wr(y),puts("");
	}
}ans[N*N];
inline void swap_id(ll x,ll y){
	if (vis[x][y]) {puts("NO");exit(0);}
	ans[++tot]=node(x,y);
	swap(id[a[x]],id[a[y]]);
	swap(a[x],a[y]);
	vis[x][y]=vis[y][x]=1;
	--ins[x],--ins[y];
//	for (ll i=1;i<=n;++i) cout<<a[i]<<' ';puts("");
}
inline void dfs(ll x){
	while (ins[x]>1){
//		if (x%8<=4){
//			for (ll i=n;i;--i) if (!vis[x][i]){
//				if (a[i]==x) continue;
//				swap_id(x,i);
//				break;
//			}
//		}else
		for (ll i=1;i<=n;++i) if (!vis[x][i]){
			if (a[i]==x) continue;
			swap_id(x,i);
			break;
		}
	}
	if (ins[x]==1){
		for (ll i=1;i<=n;++i) if (!vis[x][i]){
			if (id[x]!=i) swap_id(i,id[x]);
			swap_id(id[x],x);
			break;
		}
	}
	if (a[x]!=x) {puts("NO");exit(0);}
}
int main(){
	freopen("swap.in","r",stdin);
	freopen("swap.out","w",stdout);
	read(n);
	if (n==1) return puts("YES"),0;
	if (n%4==2||n%4==3) return puts("NO"),0;
	puts("YES");
	for (ll i=1;i<=n;++i) vis[i][i]=1;
	for (ll i=1;i<=n;++i) id[i]=a[i]=i,ins[i]=n-1;
	for (ll i=1;i<=n;++i) dfs(i);
	for (ll i=1;i<=tot;++i) ans[i].print();
	return 0;
}
//stdakking
