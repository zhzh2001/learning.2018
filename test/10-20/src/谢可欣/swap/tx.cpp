#include <iostream>
#include <cstdio>
using namespace std;
inline int read(){int tot=1;char c=getchar();while(c!='-'&&(c<'0'||c>'9'))c=getchar();
int sum=0;while(c<='9'&&c>='0')sum=sum*10+c-'0',c=getchar();return sum*tot;}
int n,Yes,tmp[1000],vis[1000][1000],sum[1000];
struct arr{
	int i,j;
}ans[100000];
void dfs(int rt){
	if(Yes)return ;
//	for(int i=1;i<=n;i++)cout<<tmp[i]<<" ";puts("");
	if(rt==n*(n-1)/2+1){
		int flag=1;
		for(int i=1;i<=n;i++){
			if(tmp[i]!=i){
				flag=0;
				break;
			}
		}
		if(flag==1){
			puts("");
			for(int i=1;i<=n*(n-1)/2;i++)cout<<ans[i].i<<" "<<ans[i].j<<endl;
			puts("YES");
			Yes=1;
//			exit(0);
		}
		return ;
	}
	for(int i=1;i<=n;i++){
		for(int j=i+1;j<=n;j++){
			if(!vis[i][j]){
				if(i==tmp[j]){
					if(sum[i]!=n-2)continue;
				}
				sum[i]++;sum[j]++;
				vis[i][j]=1;
				swap(tmp[i],tmp[j]);
				ans[rt].i=i,ans[rt].j=j;
				dfs(rt+1);
				sum[i]--;sum[j]--;
				vis[i][j]=0;
				swap(tmp[i],tmp[j]);
			}
		}
	}
}
int main()
{
	n=read();
	for(int i=1;i<=n;i++)tmp[i]=i;
	dfs(1);
	if(!Yes)puts("NO");
}
