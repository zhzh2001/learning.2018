#include <iostream>
#include <cstdio>
using namespace std;
inline int read(){int tot=1;char c=getchar();while(c!='-'&&(c<'0'||c>'9'))c=getchar();
int sum=0;while(c<='9'&&c>='0')sum=sum*10+c-'0',c=getchar();return sum*tot;}
void wr(int x){if(x<0){putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
void wrn(int x,int y){wr(x);putchar(' ');wr(y);putchar('\n');}
int main()
{
	freopen("swap.in","r",stdin);
	freopen("swap.out","w",stdout);
	int n=read();
	if(n==1){
		puts("YES");return 0;
	}
	if(n==2||n==3){
		puts("NO");
		return 0;
	}
	if(n%4==0){
		puts("YES");
		int k=n/4;
		for(int i=0;i<n;i+=4){
			wrn(1+i,2+i); wrn(3+i,4+i); wrn(2+i,4+i);
			wrn(1+i,3+i); wrn(2+i,3+i); wrn(1+i,4+i);
		}
		return 0;
	}
	if(n==5){
		puts("YES");
		puts("1 2"); puts("1 3"); puts("1 4"); puts("2 3"); puts("2 5"); 
		puts("3 5"); puts("1 5"); puts("3 4"); puts("2 4"); puts("4 5");
		return 0;
	}
	if(n==6){
		puts("YES");
		return 0;
	}
	puts("YES");
}
