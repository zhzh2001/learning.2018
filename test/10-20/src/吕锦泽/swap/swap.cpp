#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n;
int main(){
	freopen("swap.in","r",stdin);
	freopen("swap.out","w",stdout);
	n=read();
  	if (n==1) return puts("YES");
  	else if (n==2) puts("NO");
  	else if (n==3) puts("NO");
  	else if (n==4) {
  		puts("YES");
  		printf("1 2\n3 4\n2 4\n1 3\n2 3\n1 4");
  	}else if (n==5){
  		puts("YES");
  		printf("1 2\n1 3\n1 4\n2 3\n2 5\n3 5\n1 5\n3 4\n2 4\n4 5\n");
  	}else puts("NO");
}
