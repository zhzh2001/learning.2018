#include<cmath>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define mod 1000000007
#define N 105
#define rep(i,j,k) for (int i=j;i<=k;++i)
#define per(i,j,k) for (int i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
struct xxx{ int lx,ly,rx,ry; ll v; }a[N];
ll mi[N][N]; int f[N][N],now,h,w,n,m,s[N];
void check(){
	int now=0;
	rep(k,1,n){
    	bool b=false;
    	bool can=true;
    	rep(i,a[k].lx,a[k].rx) rep(j,a[k].ly,a[k].ry)
        	if (f[i][j]>a[k].v) can=0;
        	else if(f[i][j]==a[k].v) b=1;
    	if(!b) ++now;
  	}
  	++s[now];
}
void dfs(int x,int y){
	if(y==w+1){
		dfs(x+1,1);
    	return;
  	}
  	if(x>h){
    	check();
    	return;
  	}
  	rep(i,1,mi[x][y]){
    	f[x][y]=i;
    	dfs(x,y+1);
    	f[x][y]=0;
  	}
}
int main(){
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	int T=read();
	while(T--){
		memset(s,0,sizeof s);
		h=read();w=read();m=read();n=read();
		rep(i,1,h) rep(j,1,w) mi[i][j]=m;
    	rep(i,1,n){
      		a[i].lx=read(),a[i].ly=read();
     		a[i].rx=read(),a[i].ry=read();
      		a[i].v=read();
      		rep(k,a[i].lx,a[i].rx)rep(j,a[i].ly,a[i].ry)
          		mi[k][j]=min(mi[k][j],a[i].v);
    	}
    	dfs(1,1);
    	printf("%d\n",s[0]);
	}
}
/*
1
3 3 2 2
1 1 2 2 2
2 2 3 3 1

*/
