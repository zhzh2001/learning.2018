#include <bits/stdc++.h>
using namespace std;

int a[800],y,used[800][800],u[800][800],ti[800],n,s;

void dfs(int dep){
	if (dep>=n*(n-1)/2){
		if (s!=0) return;
		y=1;
		cout<<"YES"<<endl;
		return;
	}
	if (s==0&&dep>=1) return;
	for (int i=1;i<n;++i){
		if (ti[i]==n) continue;
		if (ti[i]==n && a[i]!=i) return;
		for (int j=i+1;j<=n;++j){
			if (!used[i][j]&&!u[i][j]){
				used[i][j]=1;
				u[a[i]][a[j]]=1;
				swap(a[i],a[j]);
				ti[i]++;ti[j]++;
				int t=s;
				s=t-abs(a[i]-i)-abs(a[j]-j)+abs(a[i]-j)+abs(a[j]-i);
				dfs(dep+1);
				if (y==1){
					cout<<i<<' '<<j<<endl;
					return;
				}
				s=t;
				ti[i]--;ti[j]--;
				used[i][j]=0;
				swap(a[i],a[j]);
				u[a[i]][a[j]]=0;
				
			}
		}
	}
}

int main(){
    freopen("swap.in","r",stdin);
    freopen("swap.out","w",stdout);
	cin>>n;
	if (n==6||n==0){
		cout<<"NO"<<endl;
		return 0;
	}
	if (n>6&&n%4==0){
		cout<<"YES"<<endl;
		for (int i=1;i<n;++i){
			for (int j=i+1;j<=n;++j){
				printf("%d %d\n",i,j);
			}
		}
		return 0;
	}else if(n>6){
		cout<<"NO"<<endl;
		return 0;
	}
	for (int i=1;i<=n;++i){
		a[i]=i;
	}
	dfs(0);
	if (y==0) cout<<"NO"<<endl;
	return 0;
}
