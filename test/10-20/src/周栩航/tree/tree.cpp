#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0){putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}

const int N=100005;
int poi[N*2],nxt[N*2],head[N],cnt,n;
double dp[N],no[N],x[N],y[N],totx,toty;
inline void add(int x,int y)
{
	poi[++cnt]=y;nxt[cnt]=head[x];head[x]=cnt;
	poi[++cnt]=x;nxt[cnt]=head[y];head[y]=cnt;
}
inline void Dfs(int x,int fa)
{
	int son=0;
	for(int i=head[x];i;i=nxt[i])
	{
		if(poi[i]==fa)	continue;
		Dfs(poi[i],x);
		son++;
		no[x]+=no[poi[i]];
	}
	no[x]+=y[x]/toty;
	for(int i=head[x];i;i=nxt[i])
	{
		if(poi[i]==fa)	continue;
		dp[x]+=(dp[poi[i]]+1)*(1.0-(no[x]-no[poi[i]])/2);
//		cout<<dp[x]<<endl;
		dp[x]+=(1-no[poi[i]])/son;
	}
	dp[x]*=(1-y[x]/toty);
}
double ans;
inline void Solve(int rt)
{
	For(i,1,n)	dp[i]=0,no[i]=0;//dp 为期望，no为 不用走上去的概率 
	toty-=y[rt];
	int tmp=y[rt];
	y[rt]=0;
	if(!toty)	return;
	Dfs(rt,rt);
	y[rt]=tmp;
	toty+=y[rt];
	ans+=(1.0*x[rt]/totx)*dp[rt]*(1.0-y[rt]/toty);
}
int main()
{
//	freopen("tree2.in","r",stdin);
	freopen("tree.in","r",stdin);freopen("tree.out","w",stdout);
	n=read();
	For(i,1,n-1)	add(read(),read());
	For(i,1,n)
	{
		x[i]=read();y[i]=read();
		totx+=x[i];toty+=y[i];
	}
	if(n==2)
	{
		double rt_1=x[1]/totx,no1=y[1]/toty;
		printf("%.20lf",rt_1*(1-no1)+(1-rt_1)*no1);
		return 0;
	}
	For(i,1,n)
		if(x[i])	Solve(i);
	printf("%.20lf",ans);
}
/*
2
1 2 

1 2 
2 1 
*/

