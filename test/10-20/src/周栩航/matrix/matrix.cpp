#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define y1 cqzduliu
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0){putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}

const int N=20;
int n,m,mx,x1[N],y1[N],x2[N],y2[N],v[N],ans,t[20][20],a[400],q;
inline void check()
{
	For(i,1,n*m)	t[(i-1)/m+1][(i-1)%m+1]=a[i];
	For(i,1,q)
	{
		int tmx=0;
		For(tx,x1[i],x2[i])
			For(ty,y1[i],y2[i])		tmx=max(tmx,t[tx][ty]);
		if(tmx!=v[i])	return;
	}
	ans++;
}
inline void Dfs(int x)
{
	if(x==n*m+1)
	{
		check();
		return;
	}
	a[x]=1;Dfs(x+1);a[x]=2;Dfs(x+1);
}
int main()
{
	freopen("matrix.in","r",stdin);freopen("matrix.out","w",stdout);
	int T=read();
	while(T--)
	{
		ans=0;
		n=read();m=read();mx=read();q=read();
		For(i,1,q)	x1[i]=read(),y1[i]=read(),x2[i]=read(),y2[i]=read(),v[i]=read();
		if(mx==2)	{Dfs(1);writeln(ans);}
	}
}
