#include<bits/stdc++.h>
using namespace std;
#define maxn 100100
int head[maxn],nxt[maxn<<1],ver[maxn<<1],tot;
inline void add(int a,int b)
{
	nxt[++tot]=head[a];ver[tot]=b;head[a]=tot;
	nxt[++tot]=head[b];ver[tot]=a;head[b]=tot;
}
int size[maxn],totpos[maxn];int n;
int x[maxn],y[maxn],totx,toty;
inline void getsize(int X,int fat)
{
	size[X]=1;totpos[X]=x[X];
	for(int i=head[X];i;i=nxt[i])
	{
		int Y=ver[i];if(Y==fat) continue;
		getsize(Y,X);size[X]+=size[Y];
		totpos[X]+=totpos[Y];
	}
}
inline void init()
{
	scanf("%d",&n);
	for(int i=1,a,b;i<n;i++) scanf("%d%d",&a,&b),add(a,b);
	for(int i=1;i<=n;i++) scanf("%d%d",x+i,y+i),totx+=x[i],toty+=y[i];
	getsize(1,0);
}
double ans=0;
inline void getans(int X,int fat)
{
	double pos=1.0*y[X]/toty;
	for(int i=head[X];i;i=nxt[i])
	{
		int Y=ver[i];if(Y==fat) continue;
		getans(Y,X);ans+=pos*1.0*totpos[Y]/totx*size[Y];
	}
	ans+=pos*(1.0-1.0*totpos[X]/totx)*(n-size[X]);
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	init();
	getans(1,0);
	printf("%.17f",ans);
	return 0;
}
