#include<cstdio>
#include<cstring>
#include<algorithm>
#include<iostream>
#include<cstdlib>
using namespace std;
#define mod 1000000007
inline int ksm(int a,int b)
{
	int res=1;
	for(;b;b>>=1,a=1ll*a*a%mod) if(b&1) res=1ll*res*a%mod;
	return res;
}
int h,w,m,n;
int x1[11],y1[11],x2[11],y2[11],v[11];
int val[5][5];int ans=0;
inline void check()
{
	for(int i=1;i<=n;i++)
	{
		int vmax=0;
		for(int j=x1[i];j<=x2[i];j++) for(int k=y1[i];k<=y2[i];k++) vmax=max(vmax,val[j][k]);
		if(vmax!=v[i]) return;
	}
	ans++;
}
inline void dfs(int x,int y)
{
	if(x>h){check();return;}
	for(int i=1;i<=m;i++)
	{
		val[x][y]=i;
		if(y==w) dfs(x+1,1);
		else dfs(x,y+1);
	}
}
inline void solve_bf()
{
	ans=0;dfs(1,1);
	printf("%d\n",ans);
}
inline int calc(int p,int v)
{
	int v1=ksm(v,p),v2=ksm(v-1,p);
	return (v1-v2+mod)%mod;
}
inline void solve_2()
{
	ans=0;
	if(n==1)
	{
		int S=(x2[1]-x1[1]+1)*(y2[1]-y1[1]+1);
		ans=1ll*calc(S,v[1])*ksm(m,h*w-S)%mod;
		printf("%d\n",ans);
		return;
	}
	if(((x1[1]>=x1[2]&&x1[1]<=x2[2])||(x2[1]>=x1[2]&&x2[1]<=x2[2]))||((y1[1]>=y1[2]&&y1[1]<=y2[2])||(y2[1]>=y1[2]&&y2[1]<=y2[2])))
	{
		int S1=(x2[1]-x1[1]+1)*(y2[1]-y1[1]+1),S2=(x2[2]-x1[2]+1)*(y2[2]-y1[2]+1);
		int S=(min(x2[1],x2[2])-max(x1[1],x1[2])+1)*(min(y2[1],y2[2])-max(y1[1],y1[2])+1);
		int left=h*w-(S1+S2-S);
		if(v[1]==v[2])
		{
			ans+=1ll*calc(S,v[1])*ksm(v[1],S1+S2-S-S)%mod*ksm(m,left)%mod;
			ans+=1ll*calc(S1-S,v[1])*calc(S2-S,v[1])%mod*ksm(v[1]-1,S)%mod*ksm(m,left)%mod;
			ans%=mod;
		}
		else if(v[1]>v[2]) ans=1ll*calc(S1-S,v[1])*calc(S2,v[2])%mod*ksm(m,left)%mod;
		else if(v[2]>v[1]) ans=1ll*calc(S1,v[1])*calc(S2-S,v[2])%mod*ksm(m,left)%mod;
	}
	else
	{
		int S1=(x2[1]-x1[1]+1)*(y2[1]-y1[1]+1),S2=(x2[2]-x1[2]+1)*(y2[2]-y1[2]+1);
		int left=h*w-S1-S2;
		ans=1ll*calc(S1,v[1])*calc(S2,v[2])%mod*ksm(m,left)%mod;
	}
	printf("%d\n",ans);
}
inline void solve()
{
	scanf("%d%d%d%d",&h,&w,&m,&n);
	for(int i=1;i<=n;i++) scanf("%d%d%d%d%d",x1+i,y1+i,x2+i,y2+i,v+i);
	if(h<=4&&w<=4&&m<=2) solve_bf();
	else if(n<=2) solve_2();
	else printf("%lld\n",1ll*rand()*rand()%mod);
}
int main()
{
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	srand(23333333);
	int T;scanf("%d",&T);
	while(T--) solve();
}
