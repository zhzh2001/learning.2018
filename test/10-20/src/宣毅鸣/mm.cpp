#include<bits/stdc++.h>
using namespace std;
#define y1 ___y1
#define int long long
const int M=1e9+7,N=65;
int x2[15],y2[15],b[N][N],ans,totx,toty,xx[N],yy[N],T,n,m,l,r,a[5][5],x1[15],y1[15],z[15];
int ksm(int x,int y){
	if (!y)return 1;
	int z=ksm(x,y/2);
	z*=z;z%=M;
	if (y&1)z*=x;
	return z%M;
}
signed main(){
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		ans=0;
		scanf("%d%d%d%d",&l,&r,&m,&n);
		xx[1]=1;xx[3]=l;xx[2]=l+1;
		yy[1]=1;yy[3]=r;yy[2]=r+1;
		totx=toty=3;
		for (int i=1;i<=n;i++){
			scanf("%d%d%d%d%d",&x1[i],&y1[i],&x2[i],&y2[i],&z[i]);
			xx[++totx]=x1[i];xx[++totx]=x2[i];
			xx[++totx]=x1[i]-1;xx[++totx]=x2[i]+1;
			yy[++toty]=y1[i];yy[++toty]=y2[i];
			yy[++toty]=y1[i]-1;yy[++toty]=y2[i]+1;
		}
		sort(xx+1,xx+totx+1);sort(yy+1,yy+toty+1);
		int numx=1;
		for (int i=2;i<=totx;i++)
			if (xx[i]!=xx[numx])xx[++numx]=xx[i];
		int numy=1;
		for (int i=2;i<=toty;i++)
			if (yy[i]!=yy[numy])yy[++numy]=yy[i];	
		totx=numx;toty=numy;	
		for (int i=1;i<=n;i++){
			for (int j=1;j<=totx;j++)
				if (xx[j]==x1[i]){
					x1[i]=j;
					break;
				}
			for (int j=1;j<=totx;j++)
				if (xx[j]==x2[i]){
					x2[i]=j;
					break;
				}	
			for (int j=1;j<=toty;j++)
				if (yy[j]==y1[i]){
					y1[i]=j;
					break;
				}		
			for (int j=1;j<=toty;j++)
				if (yy[j]==y2[i]){
					y2[i]=j;
					break;
				}									
		}
		for (int v=0;v<1<<n;v++){
			int flag=1;
			for (int i=1;i<=n;i++)
				if (v&(1<<(i-1)))flag=-flag;
			for (int i=1;i<totx;i++)
				for (int j=1;j<toty;j++)b[i][j]=m;
			for (int i=1;i<=n;i++)
				for (int l1=x1[i];l1<=x2[i];l1++)
					for (int r1=y1[i];r1<=y2[i];r1++)
						if (v&(1<<(i-1)))b[l1][r1]=min(b[l1][r1],z[i]-1);
						else b[l1][r1]=min(b[l1][r1],z[i]);
			for (int i=1;i<totx;i++)
				for (int j=1;j<toty;j++)
					flag=flag*b[i][j]%M*(xx[i+1]-xx[i])%M*(yy[j+1]-yy[j])%M;
			(ans+=flag+M)%=M;				
		}
		printf("%lld\n",ans);
	}
	return 0;
}
