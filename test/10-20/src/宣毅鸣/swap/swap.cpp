#include<bits/stdc++.h>
using namespace std;
const int N=700;
int n,a[N*N],v[N],cnt,x[N*N],y[N*N];
int main(){
	freopen("swap.in","r",stdin);
	freopen("swap.out","w",stdout);
	srand(time(NULL));
	scanf("%d",&n);
	if (n==1){
		puts("YES");
		return 0;
	}
	if (n==4){
		puts("YES");
		puts("1 2");
		puts("3 4");
		puts("2 4");
		puts("1 3");
		puts("2 3");
		puts("1 4");
		return 0;
	}
	if (n==5){
		puts("YES");
		puts("4 5");
		puts("1 3");
		puts("3 4");
		puts("1 5");
		puts("2 4");
		puts("2 5");
		puts("3 5");
		puts("2 3");
		puts("1 4");
		puts("1 2");
		return 0;
	}
	if (n==9){
		puts("YES");
		puts("1 8");
		puts("5 7");
		puts("1 7");
		puts("2 4");
		puts("1 3");
		puts("2 6");
		puts("1 9");
		puts("1 5");
		puts("3 9");
		puts("2 3");
		puts("3 4");
		puts("2 9");
		puts("4 5");
		puts("4 6");
		puts("3 7");
		puts("6 9");
		puts("2 5");
		puts("3 6");
		puts("4 8");
		puts("5 6");
		puts("4 7");
		puts("2 8");
		puts("6 8");
		puts("6 7");
		puts("2 7");
		puts("1 4");
		puts("3 5");
		puts("5 9");
		puts("1 2");
		puts("1 6");
		puts("7 9");
		puts("5 8");
		puts("4 9");
		puts("8 9");
		puts("7 8");
		puts("3 8");
		return 0;
	}
	if (n==12){
		puts("YES");
		puts("2 10");
		puts("3 9");
		puts("3 6");
		puts("2 4");
		puts("5 7");
		puts("2 11");
		puts("4 7");
		puts("1 8");
		puts("1 9");
		puts("6 7");
		puts("3 8");
		puts("6 8");
		puts("1 3");
		puts("1 11");
		puts("5 9");
		puts("8 12");
		puts("1 10");
		puts("5 11");
		puts("3 10");
		puts("2 9");
		puts("3 11");
		puts("6 10");
		puts("2 7");
		puts("2 6");
		puts("5 12");
		puts("3 12");
		puts("7 10");
		puts("4 12");
		puts("7 11");
		puts("9 11");
		puts("5 6");
		puts("1 6");
		puts("3 7");
		puts("11 12");
		puts("8 11");
		puts("1 7");
		puts("4 6");
		puts("4 5");
		puts("9 12");
		puts("7 9");
		puts("8 9");
		puts("7 8");
		puts("2 8");
		puts("4 9");
		puts("10 12");
		puts("2 5");
		puts("5 10");
		puts("3 5");
		puts("6 9");
		puts("4 8");
		puts("6 11");
		puts("4 10");
		puts("1 2");
		puts("10 11");
		puts("2 3");
		puts("7 12");
		puts("1 12");
		puts("9 10");
		puts("1 5");
		puts("5 8");
		puts("8 10");
		puts("4 11");
		puts("3 4");
		puts("1 4");
		puts("2 12");
		puts("6 12");
		return 0;
	}
	if (n%4==0||n%4==1){
		puts("YES");
		for (int i=1;i<=n;i++)
			for (int j=i+1;j<=n;j++)printf("%d %d\n",i,j);
		return 0;	
	}
	puts("NO");
	return 0;
}
