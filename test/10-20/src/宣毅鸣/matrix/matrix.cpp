#include<bits/stdc++.h>
using namespace std;
#define y1 ___y1
#define int long long
const int M=1e9+7,N=55;
int x2[15],y2[15],b[N][N],ans,totx,toty,xx[25],yy[25],T,n,m,l,r,a[5][5],x1[15],y1[15],z[15];
int ksm(int x,int y){
	if (!y)return 1;
	int z=ksm(x,y/2);
	z*=z;z%=M;
	if (y&1)z*=x;
	return z%M;
}
signed main(){
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		ans=0;
		scanf("%d%d%d%d",&l,&r,&m,&n);
		for (int i=1;i<=n;i++)
			scanf("%d%d%d%d%d",&x1[i],&y1[i],&x2[i],&y2[i],&z[i]);
		if (l<=50&&r<=50){
			for (int v=0;v<1<<n;v++){
				int flag=1;
				for (int i=1;i<=n;i++)
					if (v&(1<<(i-1)))flag=-flag;
				for (int i=1;i<=l;i++)
					for (int j=1;j<=r;j++)b[i][j]=m;
				for (int i=1;i<=n;i++)
					for (int l1=x1[i];l1<=x2[i];l1++)
						for (int r1=y1[i];r1<=y2[i];r1++)
							if (v&(1<<(i-1)))b[l1][r1]=min(b[l1][r1],z[i]-1);
							else b[l1][r1]=min(b[l1][r1],z[i]);
				for (int i=1;i<=l;i++)
					for (int j=1;j<=r;j++)
						(flag*=b[i][j])%=M;
				(ans+=flag+M)%=M;				
			}
			printf("%lld\n",ans);
			continue;
		}
		if (!n){
			printf("%lld\n",ksm(m,l*r));
			continue;
		}
		if (n==1){
			int v=(x2[1]-x1[1]+1)*(y1[1]-y2[1]+1);
			printf("%lld\n",ksm(m,l*r-v)*(ksm(z[1],v)-ksm(z[1]-1,v)-+M)%M);
			continue;
		}
		if (n==2){
			int s=0,v1=(x2[1]-x1[1]+1)*(y2[1]-y1[1]+1),v2=(x2[2]-x1[2]+1)*(y2[2]-y1[2]+1);
			if (!(x1[1]>x2[2]||x1[2]>x2[1]||y1[1]>y2[2]||y1[2]>y2[1]))
				s=(min(x2[1],x2[2])-max(x1[1],x1[2])+1)*(min(y2[1],y2[2])-max(y1[1],y1[2])+1);
			if (z[1]==z[2]){
				v1-=s;v2-=s;
				int ans=ksm(z[1]-1,s)*(ksm(z[1],v1)-ksm(z[1]-1,v1)+M)%M
				*(ksm(z[1],v2)-ksm(z[1]-1,v2)+M)%M+(ksm(z[1],s)-ksm(z[1]-1,s)+M)*
				ksm(z[1],v1)*ksm(z[1],v1);
				ans%=M;
				printf("%lld\n",ans*ksm(m,l*r-v1-v2-s)%M);
			}
			else {
				if (z[1]>z[2])v1-=s;
				else v2-=s;
				int ans=(ksm(z[1],v1)-ksm(z[1]-1,v1)+M)%M
				*(ksm(z[2],v2)-ksm(z[2]-1,v2)+M)%M;
				printf("%lld\n",ans*ksm(m,l*r-v1-v2)%M);				
			}
		}
	}
	return 0;
}
