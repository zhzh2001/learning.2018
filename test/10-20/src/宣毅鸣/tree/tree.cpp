#include<bits/stdc++.h>
using namespace std;
#define int long long
const int N=200005;
int ne[N],ans,a[N],b[N],size[N],n,x,y,zz[N],tot,fi[N];
double num[N],sum1,sum2;
void jb(int x,int y){
	ne[++tot]=fi[x];
	fi[x]=tot;
	zz[tot]=y;
}
void dfs(int x,int y){
	num[x]=a[x];
	size[x]=1;
	for (int i=fi[x];i;i=ne[i])
		if (zz[i]!=y){
			dfs(zz[i],x);
			num[x]+=num[zz[i]];
			size[x]+=size[zz[i]];
			ans+=num[zz[i]]*size[zz[i]]*b[x];
		}
	ans+=(sum1-num[x])*(n-size[x])*b[x];
}
signed main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	scanf("%lld",&n);
	for (int i=1;i<n;i++){
		scanf("%lld%lld",&x,&y);
		jb(x,y);jb(y,x);
	}
	for (int i=1;i<=n;i++)scanf("%lld%lld",&a[i],&b[i]),sum1+=a[i],sum2+=b[i];
	dfs(1,0);
	printf("%.10lf",(double)ans/sum1/sum2);
	return 0;
}
