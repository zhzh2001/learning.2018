#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("tree.in");
ofstream fout("tree.out");
const int N=100005;
int n,head[N],v[N*2],nxt[N*2],e,sz[N];
double x[N],y[N],sum[N],ans;
inline void add_edge(int u,int v)
{
	::v[++e]=v;
	nxt[e]=head[u];
	head[u]=e;
}
void dfs(int k,int fat)
{
	sz[k]=1;
	sum[k]=x[k];
	for(int i=head[k];i;i=nxt[i])
		if(v[i]!=fat)
		{
			dfs(v[i],k);
			sz[k]+=sz[v[i]];
			ans+=y[k]*sum[v[i]]*sz[v[i]];
			sum[k]+=sum[v[i]];
		}
	ans+=y[k]*(1-sum[k])*(n-sz[k]);
}
int main()
{
	fin>>n;
	for(int i=1;i<n;i++)
	{
		int u,v;
		fin>>u>>v;
		add_edge(u,v);
		add_edge(v,u);
	}
	int X=0,Y=0;
	for(int i=1;i<=n;i++)
	{
		fin>>x[i]>>y[i];
		X+=x[i];
		Y+=y[i];
	}
	for(int i=1;i<=n;i++)
	{
		x[i]/=X;
		y[i]/=Y;
	}
	dfs(1,0);
	fout.precision(15);
	fout<<fixed<<ans<<endl;
	return 0;
}
