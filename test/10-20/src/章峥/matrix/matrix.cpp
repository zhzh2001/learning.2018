#include<fstream>
#include<algorithm>
#define y1 yy1
using namespace std;
ifstream fin("matrix.in");
ofstream fout("matrix.out");
const int N=25,MOD=1e9+7,M=10005;
int x1[N],y1[N],x2[N],y2[N],v[N],x[N],y[N],mx[N][N],sum[M],id[N];
long long qpow(long long a,int b)
{
	long long ans=1;
	do
	{
		if(b&1)
			ans=ans*a%MOD;
		a=a*a%MOD;
	}while(b/=2);
	return ans;
}
int main()
{
	int t;
	fin>>t;
	while(t--)
	{
		int h,w,m,n;
		fin>>h>>w>>m>>n;
		int xn=0,yn=0;
		for(int i=1;i<=n;i++)
		{
			fin>>x1[i]>>y1[i]>>x2[i]>>y2[i]>>v[i];
			x[++xn]=x1[i];y[++yn]=y1[i];
			x[++xn]=x2[i]+1;y[++yn]=y2[i]+1;
		}
		x[++xn]=1;x[++xn]=h+1;
		sort(x+1,x+xn+1);
		xn=unique(x+1,x+xn+1)-x-1;
		y[++yn]=1;y[++yn]=w+1;
		sort(y+1,y+yn+1);
		yn=unique(y+1,y+yn+1)-y-1;
		int ans=0;
		for(int i=0;i<1<<n;i++)
		{
			for(int p=1;p<xn;p++)
				for(int q=1;q<yn;q++)
					mx[p][q]=m;
			int cnt=0;
			for(int j=1;j<=n;j++)
			{
				id[j]=i&(1<<(j-1))?v[j]-1:v[j];
				int xl=lower_bound(x+1,x+xn+1,x1[j])-x,xr=lower_bound(x+1,x+xn+1,x2[j]+1)-x;
				for(int p=xl;p<xr;p++)
				{
					int yl=lower_bound(y+1,y+yn+1,y1[j])-y,yr=lower_bound(y+1,y+yn+1,y2[j]+1)-y;
					for(int q=yl;q<yr;q++)
						mx[p][q]=min(mx[p][q],id[j]);
				}
				if(i&(1<<(j-1)))
					cnt++;
			}
			id[n+1]=m;
			fill(sum,sum+m+1,0);
			for(int p=1;p<xn;p++)
				for(int q=1;q<yn;q++)
					sum[mx[p][q]]+=(x[p+1]-x[p])*(y[q+1]-y[q]);
			long long now=1;
			if(sum[0])
				now=0;
			else
				for(int j=1;j<=n+1;j++)
				{
					now=now*qpow(id[j],sum[id[j]])%MOD;
					sum[id[j]]=0;
				}
			if(cnt&1)
				ans=(ans-now+MOD)%MOD;
			else
				ans=(ans+now)%MOD;
		}
		fout<<ans<<endl;
	}
	return 0;
}
