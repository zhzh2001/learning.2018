#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("swap.in");
ofstream fout("swap.out");
const int n=8;
pair<int,int> p[100];
int P[10];
int main()
{
	int cc=0;
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++)
			p[++cc]=make_pair(i,j);
	do
	{
		for(int i=1;i<=n;i++)
			P[i]=i;
		for(int i=1;i<=cc;i++)
			swap(P[p[i].first],P[p[i].second]);
		bool flag=true;
		for(int i=1;i<=n;i++)
			if(P[i]!=i)
				flag=false;
		if(flag)
		{
			for(int i=1;i<=cc;i++)
				fout<<p[i].first<<' '<<p[i].second<<endl;
			fout<<endl;
			break;
		}
	}while(next_permutation(p+1,p+cc+1));
	return 0;
}
