#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("swap.in");
ofstream fout("swap.out");
const int N=700;
bool vis[N];
int n;
inline void print(int x,int y)
{
	if(n%4==1&&!vis[x]&&!vis[y])
		fout<<x<<' '<<n<<endl;
	fout<<x<<' '<<y<<endl;
	if(n%4==1&&!vis[x]&&!vis[y])
	{
		fout<<y<<' '<<n<<endl;
		vis[x]=vis[y]=true;
	}
}
int main()
{
	fin>>n;
	if(n==1)
		fout<<"YES\n";
	else if(n%4>=2)
		fout<<"NO\n";
	else
	{
		fout<<"YES\n";
		for(int i=1;i+3<=n;i+=4)
		{
			print(i,i+1);
			print(i+2,i+3);
			print(i+1,i+3);
			print(i,i+2);
			print(i+1,i+2);
			print(i,i+3);
		}
		for(int i=1;i+3<=n;i+=4)
			for(int j=i+4;j+3<=n;j+=4)
			{
				print(i,j);
				print(i+1,j+1);
				print(i+2,j+2);
				print(i+3,j+3);
				print(i+2,j);
				print(i+3,j+1);
				print(i,j+2);
				print(i+1,j+3);
				print(i+2,j+1);
				print(i+3,j+2);
				print(i,j+3);
				print(i+1,j);
				print(i,j+1);
				print(i+1,j+2);
				print(i+2,j+3);
				print(i+3,j);
			}
	}
	return 0;
}
