#include<bits/stdc++.h>
#define int long long
using namespace std;
const int P=1000000007;
int t,h,w,n,m,x,y,X,Y,v,ans;
int Map[10001];
int mp[51][51];
int poww(int x,int y){
	int sum=1;
	while(y){
		if(y&1){
			sum=(sum*x)%P;
			y--;
		}
		y/=2;
		x=x*x%P;
	}
	return sum;
}
signed main(){
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	scanf("%lld",&t);
	while(t--){
		scanf("%lld%lld%lld%lld",&h,&w,&m,&n);
		if(h<=50&&w<=50){
			ans=1;
			for(int i=1;i<=m;i++)
				Map[i]=0;
			for(int i=1;i<=h;i++)
				for(int j=1;j<=w;j++)
					mp[i][j]=m+1;
			for(int i=1;i<=n;i++){
				scanf("%lld%lld%lld%lld%lld",&x,&y,&X,&Y,&v);
				for(int j=x;j<=X;j++)
					for(int k=y;k<=Y;k++)
						mp[j][k]=min(mp[j][k],v);
			}
			printf("mp:\n");
			for(int i=1;i<=h;i++){
				for(int j=1;j<=w;j++){
					printf("%lld",mp[i][j]);
					Map[mp[i][j]]++;
				}
				puts("");
			}
			for(int i=1;i<=m;i++)
				if(Map[i])
						ans=(ans*(poww(i,Map[i])-poww(i-1,Map[i])))%P;
			printf("%lld\n",ans);
			goto finish;
		}
		if(n<=2){
			scanf("%lld%lld%lld%lld%lld",&x,&y,&X,&Y,&v);
			int a,b,c,d,e,ans=1;
			scanf("%lld%lld%lld%lld%lld",&a,&b,&c,&d,&e);
			if(a<x){
				swap(a,x);
				swap(b,y);
				swap(c,X);
				swap(d,Y);
				swap(e,v);
			}
			if(a<X&&b<Y){
				int s1=(min(X,c)-max(x,a))*(min(Y,d)-max(y,b));
				int s2=(X-x)*(Y-y)-s1;
				int s3=(c-a)*(d-b)-s1;
				int s4=max((int)0,((h*w)-s1-s2-s3));
				ans=(ans*(poww(v,s2)-poww(v-1,s2)))%P;
				ans=(ans*(poww(e,s3)-poww(e-1,s3)))%P;
				ans=(ans*(poww(min(v,e),s1)-poww(min(e,v)-1,s1)))%P;
				if(s4)
					ans=(ans*poww(m,s4))%P;
				printf("%lld\n",ans);
				goto finish;
			}
			int s2=(X-x)*(Y-y);
			int s3=(c-a)*(d-b);
			int s1=max((int)0,(h*w)-s2-s3);
			ans=(ans*(poww(v,s2)-poww(v-1,s2)))%P;
			ans=(ans*(poww(e,s3)-poww(e-1,s3)))%P;
			if(s1)
				ans=(ans*poww(m,s1))%P;
			printf("%lld\n",ans);
		}
		finish:;
	}
}

