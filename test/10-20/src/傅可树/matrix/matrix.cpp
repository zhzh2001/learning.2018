#include<cstdio>
#include<algorithm>
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=55;
int h,w,n,m,tot;
struct pp{
	int x1,y1,x2,y2,v;
}b[12];
int a[N][N],ans,mn[N][N];
inline void init(){
	h=read(); w=read(); m=read(); n=read(); tot=0;
	for (int i=1;i<=h;i++){
		for (int j=1;j<=w;j++){
			mn[i][j]=m;
		}
	}
	for (int i=1;i<=n;i++){
		int x1=read(),y1=read(),x2=read(),y2=read(),v=read();
		for (int j=x1;j<=x2;j++){
			for (int k=y1;k<=y2;k++){
					mn[j][k]=min(mn[j][k],v);
				}
			}
		b[++tot]=(pp){x1,y1,x2,y2,v};
	}
}
inline bool judge(){
	for (int i=1;i<=tot;i++){
		int mx=0;
		for (int j=b[i].x1;j<=b[i].x2;j++){
			for (int k=b[i].y1;k<=b[i].y2;k++){
					mx=max(a[j][k],mx);
				}
			}
		if (mx!=b[i].v) return 0;
	}
	return 1;
}
void dfs(int x,int y){
	if (x==h+1) {
		ans+=judge();
		return;
	} 
	for (int i=1;i<=mn[x][y];i++){
		a[x][y]=i;
		if (y==w) dfs(x+1,1);
			else dfs(x,y+1);
	}
}
inline int solve(){
	ans=0;
	dfs(1,1); writeln(ans);
}
int main(){
	freopen("matrix.in","r",stdin); freopen("matrix.out","w",stdout);
	int T=read();
	while (T--){
		init(); solve();
	}
	return 0;
}
