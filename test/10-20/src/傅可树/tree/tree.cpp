#include<cstdio>
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=5005;
struct edge{
	int link,next;
}e[N<<1];
int n,head[N],tot,x[N],y[N],X,Y;
double p1[N],p2[N];
inline void add_edge(int u,int v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
}
inline void insert(int u,int v){
	add_edge(u,v); add_edge(v,u);
}
inline void init(){
	n=read();
	for (int i=1;i<n;i++) {
		insert(read(),read());
	}
	for (int i=1;i<=n;i++) {
		x[i]=read(); y[i]=read();
		X+=x[i]; Y+=y[i];
	}
	for (int i=1;i<=n;i++){
		p1[i]=1.0*x[i]/X;
		p2[i]=1.0*y[i]/Y;
	}
}
double ans,dp[N][2];
void dfs(int u,int fa){
	dp[u][0]=dp[u][1]=0; double tmp1=0,tmp2=0;
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=fa){
			dfs(v,u);
			dp[u][1]+=dp[v][1]+2;
			tmp1+=p2[v]; tmp2+=(dp[v][0]+1)*p2[v];
		}
	}
	dp[u][0]=1.0*(1-p2[u])*(tmp1*(dp[u][1])*0.5+tmp2);
	dp[u][1]=1.0*dp[u][1]*(1-p2[u]);
}
inline void solve(){
	for (int i=1;i<=n;i++){
		dfs(i,0);
		ans+=p1[i]*dp[i][0];
	}
	printf("%.10lf\n",ans);
}
int main(){
	freopen("tree.in","r",stdin); freopen("tree.out","w",stdout);
	init(); solve();
	return 0;
}
