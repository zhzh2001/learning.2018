#include<bits/stdc++.h>
using namespace std;
const int N=10;
int n;
inline void init(){
	scanf("%d",&n);
}
int a[N],flag,vis[N][N],t,q[N][2];
void dfs(int x){
	if (x==(n-1)*n/2+1){
		for (int i=1;i<=n;i++) if (a[i]!=i) return;
		flag=1; puts("YES"); 
		for (int i=1;i<=t;i++) {
			printf("%d %d\n",q[i][0],q[i][1]);
		}
		exit(0);
		return;
	}
	for (int i=1;i<n;i++){
		for (int j=i+1;j<=n;j++){
			if (!vis[i][j]) {
				vis[i][j]=1; swap(a[i],a[j]); q[++t][0]=i; q[t][1]=j;
				dfs(x+1);
				vis[i][j]=0; swap(a[i],a[j]); t--;
			}
		}
	}
}
inline void solve(){
	if (n>=6) {
		if (n%4==0||n%4==1) puts("YES");
			else puts("NO");
		return;
	}
	for (int i=1;i<=n;i++) a[i]=i;
	dfs(1); puts("NO");
}
int main(){
	freopen("swap.in","r",stdin); freopen("swap.out","w",stdout);
	init(); solve();
	return 0;
}
