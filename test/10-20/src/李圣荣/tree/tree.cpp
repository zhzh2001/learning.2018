#include<bits/stdc++.h>
using namespace std;
#define int long long
int sumx,sumy;
struct node{
	int to,next;
}e[500050];
int cnt,head[500050],dep[500050],xx[500050],yy[500050];
inline int read(){
	int x=0;char c=getchar();bool p=1;
	for(;!isdigit(c);c=getchar())if(c=='-')p=0;
	for(;isdigit(c);c=getchar())x=(x<<1)+(x<<3)+(c^48);
	return p?x:-x;
}
void add(int u,int v){
	++cnt; e[cnt].to=v;
	e[cnt].next=head[u];
	head[u]=cnt;
}
int mxdep;
double ans;
void pre(int u,int fa){
	dep[u]=dep[fa]+1; mxdep=max(mxdep,dep[u]);
	for(int i=head[u];i;i=e[i].next){
		int v=e[i].to;
		if(v==fa) continue;
		pre(v,u);
	}
}
double res;
void dfs(int u,int fa,double gv){
	for(int i=head[u];i;i=e[i].next){
		int v=e[i].to;
		if(v==fa) continue;
		dep[v]=dep[u]+1;
		res+=(double)dep[v]*((double)yy[v]/sumy)*((double)1.0/gv);
		dfs(v,u,gv*2.0);
	}
}
signed main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	int n; cin>>n; int flag=0;
	for(int i=1;i<n;i++){
		int x,y; 
		x=read(); y=read();
		if(y!=x+1) flag=1;
		add(x,y); add(y,x);
	} int dx=0; int dy=0; int gs=0; int gs1=0;
	for(int i=1;i<=n;i++){
		xx[i]=read(); yy[i]=read();
		if(xx[i]==1) dx=i,gs++;
		if(yy[i]==1) dy=i,gs1++;
		sumx+=xx[i]; sumy+=yy[i];
	}
	if(n==2){
		ans+=(double)xx[1]/sumx*yy[2]/sumy;
		ans+=(double)yy[1]/sumy*xx[2]/sumx;
		printf("%.9lf\n",ans); return 0;
	}
	int flag1=0;
	if(gs==1&&gs1==1) flag1=1;
	dep[0]=-1;
	pre(1,0);
	 double k=2.0;
	if(flag1){
			if(dep[dx]<dep[dy]){
		for(int i=1;i<=dep[dx]-1;i++) ans+=1.0/k,k*=2.0;
		k=2.0;
		for(int i=1;i<=dep[dy]-dep[dx];i++) ans+=1.0/k,k*=2.0;
	}else{
		for(int i=1;i<=dep[dx]-dep[dy];i++) ans+=1.0/k,k*=2.0;
		k=2.0;
		for(int i=1;i<=mxdep-dep[dx];i++) ans+=1.0/k,k*=2.0;
		}
		printf("%.9lf\n",ans);	
	}else if(flag==0){
		for(int i=1;i<=n;i++) dep[i]=0,res=0,dfs(i,0,1.0),ans+=(double)((double)xx[i]/sumx)*res,printf("%.9lf\n",res);
		printf("%.9lf\n",ans);
	}
}
