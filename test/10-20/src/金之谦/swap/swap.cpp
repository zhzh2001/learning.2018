#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int n;
int main()
{
	freopen("swap.in","r",stdin);
	freopen("swap.out","w",stdout);
	n=read();
	if(n==1)return puts("YES")&0;
	if(n==4)return puts("YES\n1 2\n3 4\n2 4\n1 3\n2 3\n1 4");
	for(int p=4,i=1;p<=n;p*=4,i++)if(n==p){
		puts("YES");
		
		return 0;
	}
	puts("NO");
	return 0;
}
