#include <bits/stdc++.h>
using namespace std;
typedef double D;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=1e5+10;
int nedge=0,p[2*N],nex[2*N],head[2*N];
D sx=0,sy=0,X[N],Y[N];
int s[N],n;
inline void addedge(int a,int b){
	p[++nedge]=b;nex[nedge]=head[a];head[a]=nedge;
}
D f[N],g[N],q[N],ans=0,r[N];
inline void dfs(int x,int fa){
	s[x]=1;
	for(int k=head[x];k;k=nex[k])if(p[k]!=fa){
		dfs(p[k],x);s[x]+=s[p[k]];
	}
	for(int k=head[x];k;k=nex[k])if(p[k]!=fa){
		f[x]+=f[p[k]]+(D)(s[x]-s[p[k]])*q[p[k]];
		ans+=(Y[x]/sy)*(f[p[k]]+q[p[k]]);
//		cout<<x<<" "<<p[k]<<' '<<(Y[x]/sy)*(f[p[k]]+q[p[k]])<<endl;
	}
	f[x]+=(X[x]/sx)*(D)(s[x]-1);
	q[x]+=X[x]/sx;
}
inline void dfss(int x,int fa){
	D sf=0;r[x]+=r[fa]+q[x];
	ans+=g[x]*(Y[x]/sy);
	for(int k=head[x];k;k=nex[k])if(p[k]!=fa){
		r[x]+=q[p[k]];
		sf+=f[p[k]]+q[p[k]]*(s[1]-s[p[k]]+1);
	}
//	cout<<x<<' '<<sf<<endl;
	for(int k=head[x];k;k=nex[k])if(p[k]!=fa){
		r[p[k]]=r[x]-q[p[k]];
		g[p[k]]=(X[x]/sx)*(s[1]-s[p[k]]);
		g[p[k]]+=g[x]+(s[x]-s[p[k]]-1)*r[fa];
//		cout<<p[k]<<" pp "<<g[p[k]]<<endl;
		g[p[k]]+=sf-(f[p[k]]+q[p[k]]*(s[1]-s[p[k]]+1));
//		cout<<p[k]<<" qq "<<g[p[k]]<<endl;
		g[p[k]]-=(r[x]-r[fa]-q[p[k]]-q[x])*s[p[k]];
		dfss(p[k],x);
	}
//	cout<<x<<" "<<f[x]<<' '<<g[x]<<endl;
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	for(int i=1;i<n;i++){
		int x=read(),y=read();
		addedge(x,y);addedge(y,x);
	}
	for(int i=1;i<=n;i++){
		X[i]=read();Y[i]=read();
		sx+=X[i];sy+=Y[i];
	}
	dfs(1,0);dfss(1,0);
	printf("%.10lf",ans);
	return 0;
}
/*
f[x]:���� 
f[x]=sigma(f[p[k]+E(p[k]->x))
g[x]:����
g[x]= 
3
1 2
1 3
1 1
1 1
1 1
*/
