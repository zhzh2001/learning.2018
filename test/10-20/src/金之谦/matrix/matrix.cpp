#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int ans=0,a[20][20],n,m,H,W;
struct ppap{int x1,x2,y1,y2,v;}p[20];
inline void check(){
	for(int i=1;i<=n;i++){
		int pp=0;
		for(int j=p[i].x1;j<=p[i].x2;j++)
			for(int k=p[i].y1;k<=p[i].y2;k++)pp=max(pp,a[j][k]);
//		cout<<i<<' '<<pp<<' '<<p[i].v<<endl;
		if(p[i].v!=pp)return;
	}
	ans++;
}
inline void dfs(int px,int py){
	if(px==H+1){
		check();return;
	}
	for(int i=1;i<=m;i++){
		a[px][py]=i;
		if(py==W)dfs(px+1,1);else dfs(px,py+1);
	}
}
int main()
{
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	for(int T=read();T;T--){
		H=read();W=read();m=read();n=read();
		for(int i=1;i<=n;i++){
			p[i].x1=read();p[i].y1=read();p[i].x2=read();p[i].y2=read();p[i].v=read();
		}
		ans=0;dfs(1,1);writeln(ans);
	}
	return 0;
}
