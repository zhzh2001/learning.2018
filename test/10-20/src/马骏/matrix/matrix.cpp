// #pragma GCC optimize(3)
#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 50
#define MOD 1000000007
#define int long long
using namespace std;
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9) write(x/10);putchar(x%10+'0');}
int read(){int d=0,w=1;char c=getchar();for(;c<'0'||c>'9';c=getchar())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=getchar())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
int ans,xx1[maxn],xx2[maxn],yy1[maxn],yy2[maxn],v[maxn],a[maxn][maxn],n,m,h,w;
void dfs(int x,int y)
{
	if(x>h)
	{
		for(int i=1;i<=n;i++)
		{
			int ma=0;
			for(int j=xx1[i];j<=xx2[i];j++)
				for(int k=yy1[i];k<=yy2[i];k++)
					ma=max(ma,a[j][k]);
			if(ma!=v[i]) return;
		}
		// putchar('\n');
		// for(int i=1;i<=h;i++,putchar('\n'))
		// 	for(int j=1;j<=w;wrs(a[i][j++]));
		// putchar('\n');
		ans++;
		return;
	}
	for(int i=1;i<=m;i++)
	{
		a[x][y]=i;
		dfs(x+y/w,y%w+1);
	}
}
signed main()
{
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	for(int t=read();t--;)
	{
		h=read();
		w=read();
		m=read();
		n=read();
		for(int i=1;i<=n;v[i++]=read())
		{
			xx1[i]=read();
			yy1[i]=read();
			xx2[i]=read();
			yy2[i]=read();
		}
		ans=0;
		dfs(1,1);
		wln(ans);
	}
	return 0;
}
