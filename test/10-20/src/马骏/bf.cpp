// #pragma GCC optimize(3)
#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 100
// #define int long long
using namespace std;
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9) write(x/10);putchar(x%10+'0');}
int read(){int d=0,w=1;char c=getchar();for(;c<'0'||c>'9';c=getchar())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=getchar())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
int n,f,nn,a[maxn],mp[maxn][maxn];
void dfs(int k)
{
	if(f) return;
	if(k>n)
	{
		for(int i=1;i<=nn;i++)
			if(a[i]!=i) return;
		f=1;
		return;
	}
	for(int i=1;i<nn;i++)
		for(int j=i+1;j<=nn;j++)
		{
			if(mp[i][j]) continue;
			mp[i][j]=1;
			swap(a[i],a[j]);
			dfs(k+1);
			swap(a[i],a[j]);
			mp[i][j]=0;
			if(f) {wrs(i),wln(j);return;}
		}
}
signed main()
{
	// freopen(".in","r",stdin);
	// freopen(".out","w",stdout);
	for(int i=1;i<=8;i++)
	{
		nn=i;
		for(int j=1;j<=i;j++)
			a[j]=j;
		n=i*(i-1)>>1;
		f=0;
		dfs(1);
		wrs(i);
		puts(f?"YES":"NO");
	}
	return 0;
}
