// #pragma GCC optimize(3)
#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
// #define maxn
// #define int long long
using namespace std;
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9) write(x/10);putchar(x%10+'0');}
int read(){int d=0,w=1;char c=getchar();for(;c<'0'||c>'9';c=getchar())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=getchar())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
int n;
signed main()
{
	freopen("swap.in","r",stdin);
	freopen("swap.out","w",stdout);
	n=read();
	if(n==1) puts("YES");
	if(n==2) puts("NO");
	if(n==3) puts("NO");
	if(n==4) printf("YES\n1 2\n3 4\n2 4\n1 3\n2 3\n1 4");
	if(n==5) printf("YES\n1 2\n1 3\n1 4\n2 3\n2 5\n3 5\n1 5\n3 4\n2 4\n4 5");
	if(n==6) puts("NO");
	if(n>6) puts("YES");
	return 0;
}
