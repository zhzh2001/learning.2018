#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define PI pair<int,int>
#define mp make_pair
inline ll read(){
	bool pos=1; ll x=0; char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')pos=0;
	for(;isdigit(ch);ch=getchar())x=x*10+ch-'0';
	return pos?x:-x;
}
void write(ll x){
	if(x<0){
		x=-x; putchar('-');
	}
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x); puts("");
}
const int N=10005,mod=1000000007;
int h,w,m,n,cnt[N],q1[N],q2[N],sx[N],sy[N],tx[N],ty[N],v[N],b[N],vis[10005][25];
#define in(a,b,c)  (a>=b&&a<=c)
inline ll ksm(ll a,int b){
	int ans=1;
	for(;b;b>>=1){
		if(b&1)ans=ans*a%mod;
		a=a*a%mod;
	}
	return ans;
}
ll Ksm(int a,int b){
	if(vis[a][b])return vis[a][b];
	else return vis[a][b]=ksm(a,q2[b+1]-q2[b]);
}
int main(){
	freopen("matrix.in","r",stdin); freopen("matrix.out","w",stdout);
	int T=read();
	while(T--){
		h=read(); w=read(); m=read(); n=read();
		int t1=1,t2=1;
		q1[1]=q2[1]=1; 
		for(int i=1;i<=n;i++){
			sx[i]=read(); sy[i]=read(); tx[i]=read(); ty[i]=read(); v[i]=read();
			q1[++t1]=sx[i]; q1[++t1]=tx[i]+1;
			q2[++t2]=sy[i]; q2[++t2]=ty[i]+1;
		}
		q1[++t1]=h+1; q2[++t2]=w+1;
		sort(&q1[1],&q1[t1+1]); sort(&q2[1],&q2[t2+1]);
		ll ans=0;
		memset(vis,0,sizeof(vis));
		for(int i=0;i<(1<<n);i++){
			for(int j=0;j<n;j++)b[j+1]=(i>>j&1)?(v[j+1]-1):(v[j+1]);
			ll sum=1;
			for(int j=1;j<t1;j++){
				ll zs=1;
				for(int k=1;k<t2;k++){
					int t=m;
					for(int o=1;o<=n;o++)if(in(q1[j],sx[o],tx[o])&&in(q2[k],sy[o],ty[o]))t=min(t,b[o]);
					zs=zs*Ksm(t,k)%mod;
				}
				sum=sum*ksm(zs,q1[j+1]-q1[j])%mod;
			}
			if(i)cnt[i]=cnt[i>>1]+(i&1);
			if(cnt[i]&1)ans=(ans-sum+mod)%mod; else ans=(ans+sum)%mod;
		}
		cout<<(ans+mod)%mod<<endl;
	}
}
