#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 1e4+10;
int n,cnt,a[N],b[N],c[N],p[N];
bool vis[N];
inline bool check(){
	For(i,1,n) if (p[i]!=i) return 0;
	return 1;
}
inline void print(){
	For(i,1,cnt) printf("%d %d\n",a[c[i]],b[c[i]]);puts("");
}
inline void dfs(int k){
	if (k>cnt){
		if (check()){print();exit(0);}
		return;
	}
	For(i,1,cnt) if (!vis[i]) c[k]=i,vis[i]=1,swap(p[a[i]],p[b[i]]),dfs(k+1),vis[i]=0,swap(p[a[i]],p[b[i]]);
}
int main(){
	n=3;
	For(i,1,n) p[i]=i;
	For(i,1,n)
		For(j,i+1,n) a[++cnt]=i,b[cnt]=j;
	dfs(1);puts("NO");
}
