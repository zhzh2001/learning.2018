#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
typedef long double ldb;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 1e5+10;
int n,x,y,a[N],b[N],suma,sumb;
bool flag;
int tot,first[N],to[N<<1],last[N<<1];
inline void Add(int x,int y){to[++tot]=y,last[tot]=first[x],first[x]=tot;}
inline double Geta(int x){return 1.0*a[x]/suma;}
inline double Getb(int x){return 1.0*b[x]/sumb;}
namespace Subtask2{
	inline void Main(){
		int s,t;
		For(i,1,n) if (a[i]==1) s=i;
		For(i,1,n) if (b[i]==1) t=i;
		if (s==t) puts("0");
		else if (s<t) printf("%0.10lf",0.5*(t-s)+0.5*(2*(s-1)+(t-s)));
		else printf("%0.10lf",0.5*(s-t)+0.5*(2*(n-s)+(s-t)));
	} 
}
namespace Subtask3{
	double ans,pre[N],suf[N];
	inline void Main(){
		For(i,1,n) pre[i]=pre[i-1]+Getb(i)*(n-i);
		Dow(i,n,1) suf[i]=suf[i+1]+Getb(i)*(i-1);
		For(i,1,n) ans+=Geta(i)*(pre[i-1]+suf[i+1]);
		printf("%0.10lf",ans);
	}
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	if (n==1) return puts("0"),0;
	bool flag=1;
	For(i,1,n-1){
		x=read(),y=read(),Add(x,y),Add(y,x);
		if (x!=i||y!=i+1) flag=0;
	}
	For(i,1,n) a[i]=read(),b[i]=read(),suma+=a[i],sumb+=b[i];
	if (n==2) return printf("%0.10lf",Geta(1)*Getb(2)+Getb(1)*Geta(2)),0;
//	if (flag&&suma==1&&sumb==1) return Subtask2::Main(),0;
	if (flag) return Subtask3::Main(),0;
}
