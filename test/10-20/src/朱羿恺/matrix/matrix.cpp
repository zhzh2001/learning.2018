#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
typedef long double ldb;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 11, mod = 1e9+7;
int T,n,m,c,k;
struct Matrix{
	int a,b,c,d,v;
}a[N];
int ans,b[5][5];
inline bool check(){
	For(i,1,k){
		int Max=0;
		For(j,a[i].a,a[i].c){
			For(k,a[i].b,a[i].d){
				Max=max(Max,b[j][k]);
				if (Max>a[i].v) break;
			}
			if (Max>a[i].v) break;
		}
		if (Max!=a[i].v) return 0; 
	}
	/*For(i,1,n){
		For(j,1,m) printf("%d ",b[i][j]);
		puts("");
	}puts("");*/
	return 1;
}
inline void dfs(int l,int r){
	if (l>n){ans+=check();return;}
	For(i,1,c){
		b[l][r]=i;
		if (r==m) dfs(l+1,1);
			else dfs(l,r+1);
	}
}
inline void solve1(){
	ans=0,dfs(1,1),printf("%d\n",ans);
}
inline int power(int a,int b){
	int ans=1;
	for (;b;a=1ll*a*a%mod,b>>=1) if (b&1) ans=1ll*ans*a%mod;
	return ans;
}
ll C[2501][2501];
inline void init(){
	For(i,1,2500){
		C[i][1]=i,C[i][i]=1;
		For(j,2,i-1) C[i][j]=(C[i-1][j-1]+C[i-1][j])%mod;
	}
}
int Min[51][51],Sum[10010];
inline void Mod(ll &x){
	while (x<0) x+=mod;
	while (x>=mod) x-=mod;
}
inline int calc(int x,int y){
	if (y<=0) return 1;
	ll ans=0,f=1;
	Dow(i,y,0) ans+=f*(1ll*C[y+1][y-i+1]*power(x,i)%mod),Mod(ans),f*=-1;
	return ans;
}
inline void solve2(){
	For(i,1,n)
		For(j,1,m) Min[i][j]=1e9;
	memset(Sum,0,sizeof Sum);
	For(i,1,k)
		For(j,a[i].a,a[i].c)
			For(k,a[i].b,a[i].d) Min[j][k]=min(Min[j][k],a[i].v);
	For(i,1,n)
		For(j,1,m)
			if (Min[i][j]==1e9) Sum[0]++;
				else Sum[Min[i][j]]++;
	ans=power(c,Sum[0]);
	For(i,1,c) ans=1ll*ans*calc(i,Sum[i]-1)%mod;
	printf("%d\n",ans);
}
int main(){
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	T=read();init();
	while (T--){
		n=read(),m=read(),c=read(),k=read();
		For(i,1,k) a[i]=(Matrix){read(),read(),read(),read(),read()};
		if (n<=4&&m<=4&&c<=2){solve1();continue;}
		if (n<=50&&m<=50){solve2();continue;}
	}
}
/*
1
4 4 4 4
1 1 2 3 3
2 3 4 4 2
2 1 4 3 2
1 2 3 4 4
*/
