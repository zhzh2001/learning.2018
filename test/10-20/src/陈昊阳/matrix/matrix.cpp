#include<iostream>
#include<stdio.h>
#include<cmath>
#include<cstring>
#include<string>
#include<algorithm>
#define rep(a,b,c) for(int a=b;a<=c;++a)
#define drp(a,b,c) for(int a=b;a>=c;--a)
#define crs(a,b) for(int a=hd[b];a;a=ne[a])
#define N 10009
#define INF 0x3f3f3f3f
#define XJ "I Love You"
#define LL long long
#define mod  1000000007

int T,h,w,m,n,X1[20],X2[20],Y1[20],Y2[20],V[20],a[109][109],ans;

int read(){
int x=0,fh=0; char c=getchar();
	while(!isdigit(c)) fh=(c=='-')?1:fh,c=getchar();
	while(isdigit(c)) x=(x<<3)+(x<<1)+(c^'0'),c=getchar();
return fh?-x:x;
}

int max(int x,int y){
return (x>y)?x:y;
}

int Findmax(int X_1,int Y_1,int X_2,int Y_2){
int sum=0;
	rep(i,X_1,X_2)
	 rep(j,Y_1,Y_2)
	 sum=max(sum,a[i][j]);
	 
	return sum;
}

bool Judge(){
	rep(i,1,n) if(Findmax(X1[i],Y1[i],X2[i],Y2[i])!=V[i]) 
	 return 0;
	return 1;
}

void dfs(int x,int y){
if(x>h) {if(Judge()) ++ans; return;}
	int xx=x,yy=y+1;
	 if(yy>w) ++xx,yy=1;
	 
	 rep(i,1,m){
	 	a[x][y]=i;
	 	dfs(xx,yy);
	 }
}

int main(){
freopen("matrix.in","r",stdin); freopen("matrix.out","w",stdout);
	T=read();
	
	while(T--){
		h=read(); w=read(); m=read(); n=read();
		rep(i,1,n){
			X1[i]=read(); Y1[i]=read(); X2[i]=read(); Y2[i]=read(); V[i]=read();
		}
			
		ans=0;	
		dfs(1,1);
		printf("%d\n",ans);
		
	}
return 0;
}








