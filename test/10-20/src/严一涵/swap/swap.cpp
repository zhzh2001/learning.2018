#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
int n,v[1003];
int a[1003],b[1003],p;
bool vis[1003];
int ct=0;
bool dfs(int q){
	if(q==p){
		for(int i=1;i<=n;i++)if(v[i]!=i)return 0;
		puts("YES");
		return 1;
	}
	for(int i=1;i<=p;i++)if(!vis[i]){
		bool ans;
		vis[i]=1;
		swap(v[a[i]],v[b[i]]);
		ans=dfs(q+1);
		swap(v[a[i]],v[b[i]]);
		vis[i]=0;
		if(ans){
			printf("%d %d\n",a[i],b[i]);
			return 1;
		}
	}
	return 0;
}
int main(){
	freopen("swap.in","r",stdin);
	freopen("swap.out","w",stdout);
	n=read();
	if(n>5){
		if(n%4==0){
			puts("YES");
			for(int i=1;i<=n;i++){
				for(int j=1;i+j<=n;j++){
					printf("%d %d\n",j,i+j);
				}
			}
		}else{
			puts("NO");
		}
		return 0;
	}
	for(int i=1;i<=n;i++)v[i]=i;
	for(int i=1;i<=n;i++){
		for(int j=1;i+j<=n;j++){
			p++;
			a[p]=j;
			b[p]=i+j;
		}
	}
	if(dfs(0))return 0;
	puts("NO");
	return 0;
}
