#include <iostream>
#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
vector<int>e[100003];
int x[100003],y[100003],sx,sy,n,py;
double ans;
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	for(int i=1;i<n;i++){
		static int xx,yy;
		xx=read();
		yy=read();
		e[xx].push_back(yy);
		e[yy].push_back(xx);
	}
	for(int i=1;i<=n;i++){
		x[i]=read();
		y[i]=read();
		sx+=x[i];
		sy+=y[i];
	}
	ans=0;
	py=0;
	for(int i=1;i<=n;i++)x[i]+=x[i-1];
	for(int j=1;j<=n;j++)if(y[j]!=0){
		ans+=1.0*x[j-1]*y[j]/sx/sy*(j-1);
		ans+=1.0*(x[n]-x[j])*y[j]/sx/sy*(n-j);
	}
	printf("%.15lf\n",ans);
	return 0;
}
