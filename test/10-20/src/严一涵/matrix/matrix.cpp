#include <iostream>
#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
int T;
int h,w,n,m,ans;
struct st{int a,b,c,d,e;}a[13];
inline bool get(int&p,int&x,int&y){
	return p&(1<<((x-1)*w+y-1));
}
int main(){
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	T=read();
	while(T-->0){
		h=read();
		w=read();
		m=read();
		n=read();
		for(int i=1;i<=n;i++){
			a[i].a=read();
			a[i].b=read();
			a[i].c=read();
			a[i].d=read();
			a[i].e=read();
		}
		ans=0;
		for(int p=0;p<(1<<(h*w));p++){
			bool ff=1;
			for(int i=1;i<=n;i++)if(a[i].e==2){
				bool f=0;
				for(int j=a[i].a;j<=a[i].c;j++){
					for(int k=a[i].b;k<=a[i].d;k++){
						f|=get(p,j,k);
					}
				}
				if(!f)ff=0;
			}else{
				bool f=0;
				for(int j=a[i].a;j<=a[i].c;j++){
					for(int k=a[i].b;k<=a[i].d;k++){
						f|=get(p,j,k);
					}
				}
				if(f)ff=0;
			}
			if(ff)ans++;
		}
		printf("%d\n",ans);
	}
	return 0;
}
