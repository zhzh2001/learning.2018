program matrix;
 uses math;
 type
  mat=record
   xc,yc,xp,yp:longint;
  end;
 var
  a:array[0..301] of mat;
  b,d,s:array[0..301] of longint;
  i1,i,m,n,t,h,w,f,mot:longint;
  ssum,o,p:int64;
 function hahabingbing(x,y:mat):mat;
  begin
   hahabingbing.xc:=max(x.xc,y.xc);
   hahabingbing.yc:=max(x.yc,y.yc);
   hahabingbing.xp:=min(x.xp,y.xp);
   hahabingbing.yp:=min(x.yp,y.yp);
  end;
 function hahacals(x:mat):longint;
  begin
   if (x.xc>x.xp) or (x.yc>x.yp) then exit(0);
   exit((x.xp-x.xc+1)*(x.yp-x.yc+1));
  end;
 procedure hahatongchi(k,nc,x,np:longint;mats:mat);
  var
   i:longint;
  begin
   //writeln(k,' ',nc,' ',x);
   //writeln;
   if k=nc then
    begin
     p:=1;
     if f=1 then
      for i:=2 to nc do
       if b[d[i]]<=b[d[p]] then p:=i
                           else p:=p
             else
      for i:=2 to nc do
       if b[d[i]]>=b[d[p]] then p:=i;
     inc(s[d[p]],f*hahacals(mats));
     exit;
    end;
   if n-np+1<nc-k then exit;
   //for i:=x+1 to n do
    //begin
     //writeln(k,' ',nc,' ',x,' ',i);
   d[k+1]:=np;
   hahatongchi(k+1,nc,x,np+1,hahabingbing(mats,a[np]));
   hahatongchi(k,nc,x,np+1,mats);
    //end;
   //writeln;
  end;
 function hahaksm(x,y:int64):int64;
  begin
   hahaksm:=1;
   while y>0 do
    begin
     if y and 1=1 then hahaksm:=hahaksm*x mod mot;
     y:=y>>1;
     x:=x*x mod mot;
    end;
  end;
 function hahacheck:byte;
  var
   i,j:longint;
  begin
   for i:=1 to n-1 do
    write(s[i],' ');
   writeln(s[n]);
  end;
 begin
  assign(input,'matric.in');
  assign(output,'matrix.out');
  reset(input);
  rewrite(output);
  mot:=1000000007;
  readln(t);
  for i1:=1 to t do
   begin
    readln(h,w,m,n);
    for i:=1 to n do
     with a[i] do
      readln(xc,yc,xp,yp,b[i]);
    p:=0;
    a[0].xc:=1;
    a[0].yc:=1;
    a[0].xp:=h;
    a[0].yp:=w;
    b[p]:=1008208820;
    filldword(s,sizeof(s)>>2,0);
    f:=1;
    for i:=1 to n do
     begin
      hahatongchi(0,i,0,1,a[0]);
      f:=0-f;
      //hahacheck;
     end;
    p:=1;
    ssum:=h*w;
    for i:=1 to n do
     dec(ssum,s[i]);
    p:=hahaksm(ssum,m);
    for i:=1 to n do
     begin
      o:=hahaksm(b[i],s[i]);
      //writeln(b[i],' ',s[i]);
      o:=(o-hahaksm(b[i]-1,s[i])+mot) mod mot;
      //writeln(b[i],' ',s[i]);
      //p:=p*hahaksm(s[i],b[i]) mod mot;
      p:=(p*o) mod mot;
     end;
    writeln(p);
   end;
  close(input);
  close(output);
 end.
