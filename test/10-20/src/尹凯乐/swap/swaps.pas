program hahaswap;
 var
  a:array[0..667,0..667] of longint;
  b:array[0..667] of longint;
  i,n,t,x,y,upass:longint;
  s:string;
 begin
  assign(input,'swap.out');
  assign(output,'swaps.out');
  reset(input);
  rewrite(output);
  readln(s);
  n:=0;
  if s='YES' then readln(n);
  upass:=0;
  filldword(a,sizeof(a)>>2,0);
  for i:=1 to n do
   b[i]:=i;
  for i:=1 to n*(n-1) div 2 do
   begin
    readln(x,y);
    if a[x,y]+a[y,x]=2 then upass:=1;
    a[x,y]:=1;
    a[y,x]:=1;
    t:=b[x];
    b[x]:=b[y];
    b[y]:=t;
   end;
  for i:=1 to n do
   if b[i]<>i then upass:=1;
  if upass=1 then writeln('You Loooooooooose.!')
             else writeln('....');
  close(input);
  close(output);
 end.
