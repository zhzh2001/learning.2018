#include <cstdio>
#include <algorithm>
using namespace std;
#define int long long
const int N=15,md=1000000007;
int T,h,w,m,n,re,mp[5][5];
struct node{int x1,y1,x2,y2,v,s,F;}jx[N];
inline int ksm(int x,int y){int re=1; while(y){if(y&1)re=re*x%md;x=x*x%md;y>>=1;}return re%md;}
inline int xj(node p,node q)
{
    return (min(p.x2,q.x2)-max(p.x1,q.x1)+1)*(min(p.y2,q.y2)-max(p.y1,q.y1)+1);
}
inline int solve1()
{
    if(n==2)
    {
        int pp=xj(jx[1],jx[2]),bb=min(jx[1].v,jx[2].v); if(!pp)return ksm(m,w*h-jx[1].s-jx[2].s)*ksm(jx[1].v,jx[1].s)%md*ksm(jx[2].v,jx[2].s)%md;
        return ksm(m,w*h-jx[1].s-jx[2].s+pp)*((ksm(jx[1].v,jx[1].s-pp)-ksm(jx[1].v-1,jx[1].s-pp)+md)%md)%md*(ksm(jx[2].v,jx[2].s-pp)-ksm(jx[2].v-1,jx[2].s-pp)+md)%md*ksm(pp,bb)%md;
    }else return ksm(m,w*h-jx[1].s)*jx[1].s%md*ksm(jx[1].v,jx[1].s-1);
}
inline bool jud(){int i,j,k,bo;for(i=1;i<=n;i++){bo=0;for(j=jx[i].x1;j<=jx[i].x2;j++)for(k=jx[i].y1;k<=jx[i].y2;k++){if(mp[j][k]>jx[i].v)return false;if(mp[j][k]==jx[i].v)bo=1;}if(!bo)return false;}return true;}
inline void dfs(int x,int y){if(y==w+1){x++;y=1;}if(x==h+1){if(jud())re++;return;} mp[x][y]=1; dfs(x,y+1); mp[x][y]=2; dfs(x,y+1);}
inline int solve2(){re=0; dfs(1,1); return re;}
signed main()
{
    freopen("matrix.in","r",stdin);
    freopen("matrix.out","w",stdout);
    int i; scanf("%lld",&T);
    while(T--)
    {
        scanf("%lld%lld%lld%lld",&h,&w,&m,&n); if(!n){printf("%lld\n",ksm(m,w*h)%md);continue;} if(m==1){printf("1\n");continue;}
        for(i=1;i<=n;i++)
        {
            scanf("%lld%lld%lld%lld%lld",&jx[i].x1,&jx[i].y1,&jx[i].x2,&jx[i].y2,&jx[i].v); jx[i].s=(jx[i].x2-jx[i].x1+1)*(jx[i].y2-jx[i].y1+1);
        }if(n<=2)printf("%lld\n",solve1());else if(m<=2)printf("%lld\n",solve2());
    }
}
