#include <queue>
#include <vector>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
const int N=100005;
int n,F[N][23],bo[N]; double x[N],y[N],re=0,d[N],X,Y,f[N];
vector<int>ve[N];
inline void dfs(int x,int pre)
{
    int i; d[x]=d[pre]+1.00;F[x][0]=pre; for(i=1;i<=19;i++)F[x][i]=F[F[x][i-1]][i-1];for(i=0;i<ve[x].size();i++)if(ve[x][i]!=pre) dfs(ve[x][i],x);
}
inline int ask(int x,int y){int i;if(d[x]<d[y])swap(x,y);for(i=19;~i;i--)if(d[F[x][i]]>=d[y])x=F[x][i];if(x==y)return x;for(i=19;~i;i--)if(F[x][i]!=F[y][i])x=F[x][i],y=F[y][i];return F[x][0];}
struct rec{int x;double w;};
inline double bfs(int s)
{
    queue<rec>q; q.push((rec){s,0}); int i; memset(bo,0,sizeof bo); bo[s]=1; double rr=0;
    while(!q.empty())
    {
        rec tmp=q.front(); q.pop();
        for(i=0;i<ve[tmp.x].size();i++)
        {
            if(!bo[ve[tmp.x][i]]){bo[ve[tmp.x][i]]=1;q.push((rec){ve[tmp.x][i],tmp.w+1});rr+=(double)(tmp.w+1)*y[ve[tmp.x][i]];}
        }
    }return rr;
}
int main()
{
    freopen("tree.in","r",stdin);
    freopen("tree.out","w",stdout);
    int i,j,u,v,f1=0,f2=0,s=0,t=0,ll; scanf("%d",&n); if(n==1)return 0*printf("0.000000000000\n");
    for(i=1;i<n;i++)
    {
        scanf("%d%d",&u,&v); ve[u].push_back(v); ve[v].push_back(u); if(u+1!=v)f1=1;
    }for(i=1;i<=n;i++){scanf("%lf%lf",&x[i],&y[i]);X+=x[i];Y+=y[i];if(x[i]==1&&s)f2=1;if(y[i]==1&&t)f2=1;if(x[i])s=i;if(y[i])t=i;}
    if(n==2){re=x[1]/(x[1]+x[2])*y[2]/(y[1]*y[2])+x[2]/(x[1]+x[2])*y[1]/(y[1]+y[2]); return 0*printf("%.12lf\n",re);}
    if(!f1&&!f2)
    {
        for(i=1;i<=n;i++)if(ve[i].size()==1){dfs(i,i);break;} ll=ask(s,t);
        if(ll==s)
        {
            re=0.50000*(double)(d[s])+0.50000*(double)(d[s]+d[t]-2*d[ll]); printf("%.12lf\n",re);
        }else re=0.500000*(double)(n-1.00-d[s])+0.500000*(double)(d[s]+d[t]-2*d[ll]); return 0*printf("%.12lf\n",re);
    }else if(!f1){for(i=1;i<=n;i++)if(ve[i].size()==1){dfs(i,i);break;} for(i=1;i<=n;i++) re+=bfs(i); printf("%.12lf\n",re);}
}
