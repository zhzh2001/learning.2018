#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for(int i=a;i<=b;i++)
#define Rep(i,a,b) for(int i=b;i>=a;i--)
#define fi first
#define sd second
#define sff sd.fi.fi
#define sfs sd.fi.sd
#define ssf sd.sd.fi
#define sss sd.sd.sd
#define p pair<int,int>
#define pp pair<int,pair<p,p> >
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
pp a[15];
ll ans,mo,h,w,n,m,T;
bool f[510][510];
ll b[510][510];
ll mx[510][510];
bool pd(int x)
{
	ll mxx=0;
	For(i,a[x].sff,a[x].ssf)
		For(j,a[x].sfs,a[x].sss)
			mxx=max(mxx,b[i][j]);
	if(mxx!=a[x].fi)return false;
	return true;
}
void dfs(int x,int y)
{
	if(x==h+1)
	{
		For(i,1,n)
		{
			if(!pd(i))return;
		}
		ans++;
		cout<<x<<" "<<y<<endl;
		return;
	}
	if(f[x][y])
	{
		For(i,1,mx[x][y])
		{
			b[x][y]=i;
			if(y==w)dfs(x+1,1);
				else dfs(x,y+1);
		}
	}
	else
	{
		if(y==w)dfs(x+1,1);
				else dfs(x,y+1);
	}
}
int main()
{
	mo=1e9+7;
	T=read();
	while(T--)
	{
		h=read();w=read();
		m=read();n=read();
		For(i,1,h)
			For(j,1,w)
				mx[i][j]=m,f[i][j]=false;
		For(i,1,n)
		{
  			a[i].sff=read();
			a[i].sfs=read();
			a[i].ssf=read();
			a[i].sss=read();
			a[i].fi=read();
		}
		sort(a+1,a+n+1);
		Rep(i,1,n)
		{
			For(j,a[i].sff,a[i].ssf)
				For(k,a[i].sfs,a[i].sss)
					mx[j][k]=a[i].fi,f[j][k]=true;
		}
		ll lzq=1;
		For(i,1,h)
			For(j,1,w)
			{
				if(!f[i][j])
				{
					lzq*=m;
					lzq%=mo;
				}
			}
		dfs(1,1);
		ans*=lzq;
		ans%=mo;
		cout<<lzq<<endl;
		cout<<ans<<endl;
		For(i,1,h)
		{
			For(j,1,w)
			{
				cout<<mx[i][j]<<" ";
			}
			cout<<endl;
		}
	}
	return 0;
}
