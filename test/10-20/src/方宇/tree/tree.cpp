#include<set>
#include<map>
#include<cmath>
#include<cstdio>
#include<cctype>
#include<vector>
#include<cstring>
#include<iostream>
#include<algorithm>
#define LL long long
#define fy 20021218020021218
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define N 100010
using namespace std;
LL a[N],b[N],c[N],size[N],father[N],x[N],y[N];
LL n,m,i,j,k,l,r,X,Y;
long double ans;
LL read()
{
	LL x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
void add(LL k,LL l,LL r){a[k]=r; b[k]=c[l]; c[l]=k;}
void dfs(LL k,LL fa)
{
	for(LL j=c[k];j;j=b[j])
		if(a[j]!=fa)
		{
			dfs(a[j],k);
			father[a[j]]=k;
			size[k]+=size[a[j]]+1;
		}
}
void get_ans(LL begin,LL k,LL fa,LL sum)
{
	ans+=(x[begin]*1.0/X)*(y[k]*1.0/Y)*sum;
	for(LL j=c[k];j;j=b[j])
		if(a[j]!=fa) 
		{
			if(fa==0)
			{
				if(father[k]==a[j]) get_ans(begin,a[j],k,sum+size[k]+1);
				else get_ans(begin,a[j],k,sum+size[1]-size[a[j]]);
			}
			else
			{
				if(father[k]==a[j]) get_ans(begin,a[j],k,sum+size[k]-size[fa]);
			      else 
				    if(father[k]==fa) get_ans(begin,a[j],k,sum+size[k]-size[a[j]]);
				      else get_ans(begin,a[j],k,sum+size[1]-size[a[j]]-size[fa]-1);	
			}
		}
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	rep(i,1,n-1)
	{
		l=read(); r=read();
		add(i*2-1,l,r);
		add(i*2,r,l);
	}
	rep(i,1,n)
	{
		x[i]=read(); X+=x[i];
		y[i]=read(); Y+=y[i];
	}
	dfs(1,0);
	rep(i,1,n)
		if(x[i]!=0) get_ans(i,i,0,0);
	printf("%.10lf",(double)ans);
	return 0;
}
