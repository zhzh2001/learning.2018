#include<set>
#include<map>
#include<cmath>
#include<cstdio>
#include<cctype>
#include<vector>
#include<cstring>
#include<iostream>
#include<algorithm>
#define LL long long
#define fy 20021218020021218
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define N 100010
using namespace std;
LL n,m,i,j,k,sum;
LL read()
{
	LL x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
void swap1(LL k1,LL k2,LL k3,LL k4)
{
	printf("%lld %lld\n",k1,k2);
	printf("%lld %lld\n",k3,k4);
	printf("%lld %lld\n",k2,k4);
	printf("%lld %lld\n",k1,k3);
	printf("%lld %lld\n",k2,k3);
	printf("%lld %lld\n",k1,k4);
}
void swap2(LL k1,LL k2,LL k3,LL k4)
{
	printf("%lld %lld\n",k1,k4);
	printf("%lld %lld\n",k2,k3);
	printf("%lld %lld\n",k1,k3);
	printf("%lld %lld\n",k2,k4);
}
void swap3(LL k1,LL k2,LL k3,LL k4,LL k5)
{
	printf("%lld %lld\n",k1,k5);
	printf("%lld %lld\n",k1,k2);
	printf("%lld %lld\n",k2,k5);
	printf("%lld %lld\n",k3,k5);
	printf("%lld %lld\n",k3,k4);
	printf("%lld %lld\n",k4,k5);
}
int main()
{
	freopen("swap.in","r",stdin);
	freopen("swap.out","w",stdout);
	n=read();
	if(n%4==0)
	{
		printf("YES\n");
		m=n/4;
		rep(i,1,m) swap1(4*i-3,4*i-2,4*i-1,4*i);
		rep(i,1,m-1)
		  rep(j,i+1,m)
		  {
		  	swap2(4*i-3,4*i-2,4*j-3,4*j-2);
		  	swap2(4*i-3,4*i-2,4*j-1,4*j);
		  	swap2(4*i-1,4*i,4*j-3,4*j-2);
		  	swap2(4*i-1,4*i,4*j-1,4*j);
		  }
	}
	if(n%4==1)
	{
		printf("YES\n");
		m=n/4;
		rep(i,1,m) swap3(4*i-3,4*i-2,4*i-1,4*i,n);
		rep(i,1,m) swap2(4*i-3,4*i-2,4*i-1,4*i);
		rep(i,1,m-1)
		  rep(j,i+1,m)
		  {
		  	swap2(4*i-3,4*i-2,4*j-3,4*j-2);
		  	swap2(4*i-3,4*i-2,4*j-1,4*j);
		  	swap2(4*i-1,4*i,4*j-3,4*j-2);
		  	swap2(4*i-1,4*i,4*j-1,4*j);
		  }
	}
	if(n%4==2) printf("NO\n");
	if(n%4==3) printf("NO\n");
	return 0;
}
