#include <bits/stdc++.h>

#define Max 100005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

struct Edge{
	int v,to;
}e[Max*2];

int n,l,r,s1,s2,size,B,E,X,Y,x[Max],y[Max],son[Max],head[Max];
bool vis[Max];
double ans,now,fact[Max];

inline void add(int u,int v){
	e[++size].v=v;e[size].to=head[u];head[u]=size;
}

inline void dfs(int u,int fa,int t){
	if(u==t){
		vis[u]=true;
	}
	son[u]=1;
	for(int i=head[u];i;i=e[i].to){
		int v=e[i].v;
		if(v==fa)continue;
		dfs(v,u,t);
		if(vis[v])vis[u]=true;
		son[u]+=son[v];
	}
	return;
}

inline void dfs2(int u,int fa,int t){
	int sum=0,num=0;
	if(u==t)return;
	for(int i=head[u];i;i=e[i].to){
		int v=e[i].v;
		if(v==fa)continue;
		num++;
		if(vis[v]){
			now=now+1;
			dfs2(v,u,t);
		}else{
			sum+=son[v];
		}
	}
	now=now+fact[num]/2.0*sum;
	return;
}

inline double calc(int s,int t){
	memset(vis,0,sizeof vis);
	dfs(s,s,t);
	now=0;
	dfs2(s,s,t);
	return now;
}

int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	s1=true;
	s2=3;
	for(int i=1;i<n;i++){
		l=read();r=read();
		add(l,r);
		add(r,l);
		if(l+1!=r)s1=false;
	}
	fact[0]=1;
	for(int i=1;i<=n;i++){
		fact[i]=fact[i-1]*i;
		x[i]=read();
		X+=x[i];
		if(x[i])s2--;
		y[i]=read(); 
		Y+=y[i]; 
		if(y[i])s2--;
	}
	ans=0;
	if(s2==1){
		for(int i=1;i<=n;i++){
			if(X==x[i])B=i;
			if(Y==y[i])E=i;
		}
		ans=1.0*calc(B,E);
		goto loop;
	}
	if(s1){
		for(int i=1;i<=n;i++){
			for(int j=1;j<=n;j++){
				if(i==j)continue;
//				printf("%d %d %.10lf\n",i,j,1.0*x[i]*y[j]/X/Y);
				ans=ans+1.0*x[i]*y[j]/X/Y*abs(i-j);
				if(i<j){
					ans=ans+1.0*x[i]*y[j]/X/Y*(i-1);
				}else{
					ans=ans+1.0*x[i]*y[j]/X/Y*(n-i);
				}
			}
		}
		goto loop;
	}
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++){
			if(i==j)continue;
			if(x[i]==0||y[j]==0)continue;
//			printf("%d %d %.2lf %.2lf\n",i,j,1.0*x[i]*y[j]/X/Y,calc(i,j));
			ans+=1.0*x[i]*y[j]/X/Y*calc(i,j);
		}
	}
	loop:
	printf("%.10lf\n",ans);
	return 0;
}

