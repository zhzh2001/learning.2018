#include <bits/stdc++.h>

#define ll long long

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(ll x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(ll x){
	write(x);puts("");
}

const ll wzp=1e9+7;

struct Matrix{
	ll x1,y1,x2,y2,v;
	inline void init(){
		x1=read();
		y1=read();
		x2=read();
		y2=read();
		v=read();
		return;
	}
}a[11];

ll ans,t,h,w,m,n,b[10][10];

inline bool check(){
	for(ll i=1;i<=n;i++){
		ll maxn=-1e18;
		for(ll j=a[i].x1;j<=a[i].x2;j++){
			for(ll k=a[i].y1;k<=a[i].y2;k++){
				maxn=max(b[j][k],maxn);
			}
		}
		if(maxn==a[i].v)return false;
	}
	return true;
}

inline void dfs(ll x,ll y){
	if(x==h+1){
		if(check())ans=(ans+1)%wzp;
		return;
	}
	for(ll i=1;i<=m;i++){
		b[x][y]=i;
		if(y==w){
			dfs(x+1,1);
		}else{
			dfs(x,y+1);
		}
	}
	return;
}

inline ll pow(ll x,ll y){
	ll ans=1;
	while(y){
		if(y&1)ans=ans*x%wzp;
		x=x*x%wzp;
		y>>=1;
	}
	return ans;
}

int main(){
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	t=read();
	while(t--){
		h=read();
		w=read();
		m=read();
		n=read();
		for(ll i=1;i<=n;i++){
			a[i].init();
		}
		ans=0;
		if(h<=4&&w<=4&&m<=2){
			dfs(1,1);
			writeln(ans%wzp);
		}else{
			writeln(pow(m,h*w));
		}
	}
	return 0;
}

