#include <bits/stdc++.h>

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

int n,size,ans,a[100],b[100],f[100],num[100];
bool flag,vis[100];

inline bool check(){
	for(int i=1;i<=n;i++)num[i]=i;
	for(int i=1;i<=size;i++){
		swap(num[a[f[i]]],num[b[f[i]]]);
	}
	for(int i=1;i<=n;i++){
		if(num[i]!=i)return false;
	}
	return true;
}

inline void dfs(int x){
	if(x==size+1){
		if(check()){
			puts("YES");
			flag=true;
			for(int i=1;i<=n*(n-1)/2;i++){
				cout<<a[f[i]]<<" "<<b[f[i]]<<endl;
			}
		}
		return;
	}
	for(int i=1;i<=size;i++){
		if(!vis[i]){
			f[x]=i;
			vis[i]=true;
			dfs(x+1);
			vis[i]=false;
			if(flag)return;
		}
	}
}

int main(){
	freopen("swap.in","r",stdin);
	freopen("swap.out","w",stdout);
	n=read();
	if(n>5){
		size=n*(n-1)/2;
		if(size&1){
			puts("NO");
		}else{
			puts("YES");
		}
		return 0;
	}
	size=0;
	for(int i=1;i<n;i++){
		for(int j=i+1;j<=n;j++){
			a[++size]=i;b[size]=j;
		}
	}
	dfs(1);
	if(!flag){
		puts("NO");
	}
	return 0;
}

