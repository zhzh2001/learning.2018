#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef double D;
const int N=100001;
ll x[N],y[N],X,Y,Z,sum[N],s[N],tmp;
D ans;
int a[N],b[N],n,i;
bool fl;
inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int rd(){
	int x=0,fl=1;char ch=gc();
	for (;ch<48||ch>57;ch=gc())if(ch=='-')fl=-1;
	for (;48<=ch&&ch<=57;ch=gc())x=(x<<3)+(x<<1)+(ch^48);
	return x*fl;
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=rd();
	for (i=1;i<n;i++) a[i]=rd(),b[i]=rd();
	puts("1");
	return 0;
	fl=1;
	for (i=1;i<n && fl;i++)
		if (a[i]+1!=b[i]) fl=0;
	if (fl){
		for (i=1;i<=n;i++) x[i]=rd(),y[i]=rd(),X+=x[i],Y+=y[i],sum[i]=sum[i-1]+y[i]*i,s[i]=s[i-1]+y[i];
		Z=1ll*X*Y;
		for (i=1;i<=n;i++){
			tmp=s[i-1]*i-sum[i-1]+(sum[n]-sum[i]-(s[n]-s[i])*i);
			ans+=tmp*x[i]*1.0/Z;
		}
		printf("%.9lf",ans+1);
//		for (i=1;i<=n;i++)
//			for (int j=1;j<=n;j++) ans+=abs(i-j+1)*x[i]*y[j];
//		printf("%.9lf",ans*1.0/Z);
	}
}
