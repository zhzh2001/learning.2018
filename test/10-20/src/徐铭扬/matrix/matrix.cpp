#include<cstdio>
#include<cstring>
using namespace std;
int a[6][6],b[6][6],x1[12],y1[12],x2[12],y2[12],n,m,h,w,i,j,k,v[12],ans,T;
inline int min(int x,int y){return x<y?x:y;}
void dfs(int x,int y){
	if (x>h){
		for (int k=1;k<=n;k++){
			int fl=0;
			for (int i=x1[k];i<=x2[k] && !fl;i++)
				for (int j=y1[k];j<=y2[k] && !fl;j++)
					if (b[i][j]==v[k]) fl=1;
			if (!fl) return;
		}
		ans++;
		return;
	}
	if (y>w){
		dfs(x+1,1);
		return;
	}
	for (int i=1;i<=a[x][y];i++) b[x][y]=i,dfs(x,y+1);
}
int main(){
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d%d%d%d",&h,&w,&m,&n);ans=0;
		for (i=1;i<=h;i++)
			for (j=1;j<=w;j++) a[i][j]=m;
		for (k=1;k<=n;k++){
			scanf("%d%d%d%d%d",&x1[k],&y1[k],&x2[k],&y2[k],&v[k]);
			for (i=x1[k];i<=x2[k];i++)
				for (j=y1[k];j<=y2[k];j++) a[i][j]=min(a[i][j],v[k]);
		}
		dfs(1,1);
		printf("%d\n",ans);
	}
}
