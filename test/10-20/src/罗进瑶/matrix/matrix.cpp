#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = 
getchar();}while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) x = -x, putchar('-');if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
}
int T, h, w, m, n;
int a[100][110];
struct T {
	int x1, x2, y1, y2, v;
}b[12];
bool check(int id) {
	int res = 0;
	for(int i = b[id].x1; i <= b[id].x2; ++i)
		for(int j = b[id].y1; j <= b[id].y2; ++j)
			res = max(res, a[i][j]);
	return res == b[id].v - 1;
}
int main() {
	setIO();
	T = read();
	while(T--) {
		int res = 0;
		h = read(), w = read(), m = read(), n = read();
		if(!(h <= 4 && w <= 4 && m <= 2)) {
			puts("0");
			continue;
		}
		for(int i = 1; i <= n; ++i)
			b[i].x1 = read(), b[i].y1 = read(), b[i].x2 = read(), b[i].y2 = read(), b[i].v = read();
		int B = (1 << (h * w));
		for(int S = 0; S < B; ++S) {
			for(int i = 1; i <= h; ++i)
				for(int j = 1; j <= w; ++j)
					a[i][j] = (S >> ((i - 1) * w + j - 1)) & 1;
			bool fg = 1;
			for(int i = 1; i <= n; ++i)
				if(!check(i)) {fg = 0;break;}
			res += fg;
		}
		writeln(res);
	}
	return 0;
}
