#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = 
getchar();}while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) x = -x, putchar('-');if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
}
const int N = 230000;
int n;
int ver[N], nxt[N], en, head[N], d[N];
void add(int x, int y) {
	ver[++en] = y, nxt[en] = head[x], head[x] = en;
}
double t1[N], t2[N];
double s1, s2;
int s, t, vis[N], cnt;
vector<int>b[N];
bool done;
void dfs(int x) {
	if(done) return;
	if(x == t) {done = 1;return;};
	vis[x] = 1;
	random_shuffle(b[x].begin(),b[x].end());
	for(int i = 0; i < b[x].size(); ++i)
		if(!vis[b[x][i]]) {
			++cnt;
			dfs(b[x][i]);
			if(done) return;
		}
	if(done) return;
	++cnt;
}
int main() {
	setIO();
	srand(time(NULL));
	n = read();
	if(n <= 2) {
		for(int i = 1; i <= n; ++i) 
			scanf("%lf%lf", &t1[i], &t2[i]), s1 += t1[i], s2 += t2[i];
		for(int i = 1; i <= n; ++i) {
			if(s1 != 0)
				t1[i] /= s1;
			if(s2 != 0)
				t2[i] /= s2;
		}
		if(n == 1) 
			printf("%.10lf\n", t1[1] * (1 - t2[1]));
		else
			printf("%.10lf\n", t1[1] * t2[2] + t2[1] * t1[2]);
		return 0;		
	}
	if(n <= 10) {
		for(int i = 1; i < n; ++i) {
			int x = read(), y = read();
			b[x].push_back(y);
			b[y].push_back(x);
		}	
		for(int i = 1; i <= n; ++i) {
			scanf("%lf%lf", &t1[i], &t2[i]);
			s1 += t1[i];
			s2 += t2[i];
		}
		for(int i = 1; i <= n; ++i)
			t1[i] /= s1, t2[i] /= s2;
		double ans = 0;
		for(int i = 1; i <= n; ++i)
			for(int j = 1; j <= n; ++j) {
				s = i, t = j;
				double sum = 0;
				for(int T = 1; T <= 20000; ++T) {
					done = 0;
					for(int k = 1; k <= n; ++k) vis[k] = 0;
					cnt = 0;				
					dfs(i);	
					sum += cnt;
				}
				ans += t1[i] * t2[j] * sum / 20000;
			}
		printf("%.10lf\n", ans);
		return 0;
	}	
	bool fg1 = 1;
	for(int i = 1; i < n; ++i) {
		int x = read(), y = read();
		fg1 &= (x + 1 == y);
		add(x, y);
		add(y, x);
		++d[x], ++d[y];
	}
	int p1 = 0, p2 = 0;
	for(int i = 1; i <= n; ++i) {
		scanf("%lf%lf", &t1[i], &t2[i]);
		s1 += t1[i];
		s2 += t2[i];
		if(t1[i] > t1[p1]) p1 = i;
		if(t2[i] > t2[p2]) p2 = i;
	}
	for(int i = 1; i <= n; ++i)
		t1[i] /= s1, t2[i] /= s2;
	double ans = 0;
	if(t1[p1] == s1 && t2[p2] == s2) {
			ans = t1[p1] * t2[p2] * (0.5 * ((p1 - 1) * 2 + p2 - p1) + 0.5 * (p2 - p1));
			printf("%.10lf\n", ans);
			return 0;
	} else if(n <= 5000) {
		for(int i = 1; i <= n; ++i)
			for(int j = i + 1; j <= n; ++j) {
				ans += t1[i] * t2[j] * (0.5 * ((i - 1) * 2 + j - i) + 0.5 * (j - i));
				ans += t2[i] * t1[j] * (0.5 * ((n - j) * 2 + j - i) + 0.5 * (j - i));
			}
		printf("%.10lf\n", ans);		
	} else puts("0");

	return 0;
}
