#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = 
getchar();}while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) x = -x, putchar('-');if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
}
const int N = 230000;
int n;
int ver[N], nxt[N], en, head[N], d[N];
void add(int x, int y) {
	ver[++en] = y, nxt[en] = head[x], head[x] = en;
}
double t1[N], t2[N];
double s1, s2;
int s, t, dis[N];
double f[N];
void dfs(int x, int F) {
	if(x == t) return;
	for(int i = head[x]; i;i = nxt[i]) {
		int y = ver[i];
		if(y == F) continue;
		f[y] = f[x] / (d[x] - (F > 0));
		dis[y] = dis[x] + 1;
		dfs(y, x);
	}
}
int main() {
	n = read();
	for(int i = 1; i < n; ++i) {
		int x = read(), y = read();
		add(x, y);
		add(y, x);
		++d[x], ++d[y];
	}
	for(int i = 1; i <= n; ++i) {
		scanf("%lf%lf", &t1[i], &t2[i]);
		s1 += t1[i];
		s2 += t2[i];
	}
	for(int i = 1; i <= n; ++i)
		t1[i] /= s1, t2[i] /= s2;
	double ans = 0;
	for(int i = 1; i <= n; ++i)
		for(int j = 1; j <= n; ++j)if(t1[i] > 0 && t2[j] > 0){
			s=i,t=j;
			f[i] = 1;
			dis[i] = 0;
			dfs(i, 0);
			double sum = 0;
			for(int k = 1; k <= n; ++k)
				if(d[k] == 1) sum += f[k] * dis[k] * 2;
				else if(k == j) sum += f[k] * dis[k];
			ans += t1[i] * t2[j] * sum;
		}
	printf("%.10lf\n", ans);
	return 0;
}
