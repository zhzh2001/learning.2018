#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = 
getchar();}while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) x = -x, putchar('-');if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("swap.in","r",stdin);
	freopen("swap.out","w",stdout);
}
const int N = 800;
int n, a[N], x[N * N], y[N * N], c, r[N * N];
int main() {
	setIO();
	n = read();
	if(n <= 3) {
		puts("NO");
		return 0;
	}
	if(n == 4) {
		puts("YES");
		puts("1 2\n1 3\n2 4\n1 4\n2 3\n3 4");
		return 0;
	}
	if(n == 5) {
		puts("YES");
		puts("1 2\n1 3\n1 4\n2 3\n2 5\n3 5\n1 5\n3 4\n2 4\n4 5");
		return 0;
	}
	if(n % 4 == 0) {
		puts("YES");
		return 0;
	} else puts("NO");
	return 0;
}
