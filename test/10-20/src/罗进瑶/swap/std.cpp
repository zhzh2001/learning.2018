
#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = 
getchar();}while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) x = -x, putchar('-');if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("swap.in","r",stdin);
	freopen("swap.out","w",stdout);
}
const int N = 800;
int n, a[N], x[N * N], y[N * N], c, r[N * N];
bool solve() {
	for(int i = 1;  i<= n; ++i) a[i] = i;
	for(int i = 1; i <= c; ++i)
		swap(a[x[r[i]]], a[y[r[i]]]);
	for(int i = 1; i <= n; ++i) if(a[i] ^ i) return 0;
	return 1;
}
int main() {
	//freopen("a.txt","w",stdout);
	n = read();
	for(int k = 1; k <= n; ++k)
		for(int i = 1; i + k <= n; ++i) {
			x[++c] = i, y[c] = i + k;
		} 
	for(int i = 1;  i<= c; ++i) r[i] = i;
	int t = 0;
	do {
		if(solve()) {
			printf("CASE #%d:\n", ++t);
			for(int i = 1;  i <= c; ++i)
				printf("%d %d\n", x[r[i]], y[r[i]]);
			return 0; 
		}
	}while(next_permutation(r + 1, r + 1 + c));
	return 0;
}
