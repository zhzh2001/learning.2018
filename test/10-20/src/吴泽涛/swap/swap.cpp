#include<bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
using namespace std;
inline GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
inline void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
inline void writeln(GG x) {
    write(x); puts("");
}

int n, sum; 
void dfs(int l, int r) {
	if(l == r) return; 
	int cnt = (r-l+1)/4; 
	For(i, l, l+cnt-1) { write(i); putchar(' '); writeln(i+cnt); ++sum; } 
	For(i, l+2*cnt, l+3*cnt-1) { write(i); putchar(' '); writeln(i+cnt); ++sum; } 
	For(i, l, l+2*cnt-1) { write(i); putchar(' '); writeln(i+2*cnt); ++sum; } 
	For(i, l, l+cnt-1) { write(i); putchar(' '); writeln(i+3*cnt); ++sum; } 
	For(i, l+cnt, l+2*cnt-1) { write(i); putchar(' '); writeln(i+cnt); ++sum; } 
	dfs(l, l+cnt-1); dfs(l+cnt, l+2*cnt-1); dfs(l+2*cnt, l+3*cnt-1); dfs(l+3*cnt, l+4*cnt-1); 
}

int main() {
	freopen("swap.in", "r", stdin); 
	freopen("swap.out", "w", stdout); 
	n = read(); 
	if(n%4==0 || n==1) 
		puts("YES"); 
	else 
		puts("NO"); 
}




