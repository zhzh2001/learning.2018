#include<bits/stdc++.h>
#define LL long long
#define GG long long 
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
using namespace std;
inline GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
inline void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
inline void writeln(GG x) {
    write(x); puts("");
}

const int N = 40, Mod = 1e9+7; 
int T, n, m, h, w; 
LL ans;  
struct node{
	int xs, ys, xt, yt, mx;  
}a[N];
int num[20], c[5][5], cnt, wzt; 

inline bool cmp_mx(node a, node b) {
	return a.mx < b.mx; 
}

inline LL ksm(int x, int y) {
	LL res = 1; 
	int b[40], tot = 0; 
	while(y) {
		b[++tot] = y%2; 
		y/=2; 
	}
	Dow(i, tot, 1) {
		res = res*res % Mod; 
		if(b[i]) res = res*x % Mod; 
	}
	return res; 
}

inline void work_2() {
	sort(a+1, a+n+1, cmp_mx); 
	int x, y, z;  
	a[3].xs = max(a[1].xs, a[2].xs); 
	a[3].ys = max(a[1].ys, a[2].ys); 
	a[3].xt = min(a[1].xt, a[2].xt); 
	a[3].yt = min(a[1].yt, a[2].yt); 
	z = max(0, (a[3].xt-a[3].xs+1)*(a[3].yt-a[3].ys+1)); 
	x = (a[1].xt-a[1].xs+1)*(a[1].yt-a[1].ys+1);
	y = (a[2].xt-a[2].xs+1)*(a[2].yt-a[2].ys+1)-z; 
	ans = ksm(m, h*w-x-y) * (ksm(a[1].mx, x)-ksm(a[1].mx-1, x)+Mod) % Mod; 
	ans = ans * ( ksm(a[2].mx, y) - ksm(a[2].mx-1, y) + Mod ) % Mod; 
	writeln(ans); 
} 

void dfs(int x) {  
	if(x > h*w) {
		cnt = 0; 
		For(i, 1, h) 
			For(j, 1, w) c[i][j] = num[++cnt]; 
		For(k, 1, n) {
			int mx = 0; 
			For(i, a[k].xs, a[k].xt) 
				For(j, a[k].ys, a[k].yt) 
					mx = max(mx, c[i][j]); 
			if(mx != a[k].mx) return; 
		}
		++wzt; 
		return; 
	}
	num[x] = 1; dfs(x+1); 
	num[x] = 2; dfs(x+1); 
}

inline void work_1() {
	wzt = 0; 
	dfs(1); 
	writeln(wzt); 
}

int main() {
	freopen("matrix.in", "r", stdin); 
	freopen("matrix.out", "w", stdout); 
	T = read(); 
	while(T--) {
		h = read(); w = read(); m = read(); n = read(); 
		For(i, 1, n) {
			a[i].xs = read(); a[i].ys = read(); a[i].xt = read(); 
			a[i].yt = read(); a[i].mx = read(); 
		}
		if(h<=4 && w<=4 && m<=2) {
			work_1(); 
			continue; 
		}
		if(n==2) {
			work_2(); 
			continue; 
		}
	}
}


/*

2
3 3 2 2
1 1 2 2 2
2 2 3 3 1


*/



