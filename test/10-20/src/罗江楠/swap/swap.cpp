#include <bits/stdc++.h>
using namespace std;

inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}

namespace baoli {

void fail() {
  puts("NO");
  exit(0);
}

vector<pair<int, int> > ans;
void add_ans(int a, int b) {
  ans.push_back(make_pair(a, b));
}

void double_swap(int _1, int _2, int _3, int _4) {
  add_ans(_2, _4);
  add_ans(_1, _3);
  add_ans(_2, _3);
  add_ans(_1, _4);
}
void four_swap(int _1, int _2, int _3, int _4) {
  add_ans(_1, _2);
  add_ans(_3, _4);
  double_swap(_1, _2, _3, _4);
}
void eight_swap(int _1, int _2, int _3, int _4, int _5, int _6, int _7, int _8) {
  four_swap(_1, _2, _3, _4);
  four_swap(_5, _6, _7, _8);
  double_swap(_1, _2, _5, _6);
  double_swap(_1, _2, _7, _8);
  double_swap(_3, _4, _5, _6);
  double_swap(_3, _4, _7, _8);
}

void works(int l, int r) {
  int n = r - l + 1;
  if (n == 4) {
    return four_swap(l, l + 1, l + 2, l + 3);
  }
  if (n < 4) fail();
  if (n & 1) fail();
  int mid = l + n / 2 - 1;
  works(l, mid);
  works(mid + 1, r);
  for (int i = l; i <= mid; i += 2) {
    for (int j = mid + 1; j <= r; j += 2) {
      double_swap(i, i + 1, j, j + 1);
    }
  }
}

void work(int n) {
  works(1, n);
  puts("YES");
  for (size_t i = 0; i < ans.size(); ++ i) {
    printf("%d %d\n", ans[i].first, ans[i].second);
  }
}
}

int main(int argc, char const *argv[]) {

  freopen("swap.in", "r", stdin);
  freopen("swap.out", "w", stdout);

  int n = read();
  baoli::work(n);

  return 0;
}