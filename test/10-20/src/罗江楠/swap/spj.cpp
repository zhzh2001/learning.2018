#include <bits/stdc++.h>

using namespace std;

ifstream fin("swap.in");
ifstream fans("swap.ans");
ifstream fout("swap.out");

int p[1000];
int main(int argc, char const *argv[]) {
  
  int n;
  fin >> n;

  string avalible, output;
  fans >> avalible;
  fout >> output;

  if (avalible == "YES") {
    if (output != avalible) {
      puts("It should be YES but NO found");
      return 1;
    }
    puts("YES = YES");
    bool ans = true;
    for (int i = 1; i <= n; ++ i) {
      p[i] = i;
    }
    for (int i = 1; i <= (n - 1) * n / 2; ++ i) {
      int x, y;
      fout >> x >> y;
      swap(p[x], p[y]);
    }
    for (int i = 1; i <= n; ++ i) {
      ans &= (p[i] == i);
    }
    if (!ans) {
      puts("WA");
      return 1;
    } else {
      puts("AC");
      return 0;
    }
  } else {
    if (output != avalible) {
      puts("It should be NO but YES found");
      return 1;
    }
    puts("NO = NO");
    return 0;
  }

  return 0;
}


