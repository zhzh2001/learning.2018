#include <bits/stdc++.h>
#define N 100020
#define mod 1000000007
using namespace std;

inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}

int main(int argc, char const *argv[]) {
  freopen("tree.in", "r", stdin);
  freopen("tree.out", "w", stdout);

  int n = read();
  printf("%.10lf\n", n * 2.0 / 3.0);

  return 0;
}