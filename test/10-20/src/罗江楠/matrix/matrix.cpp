#include <bits/stdc++.h>
#define N 100020
#define mod 1000000007
using namespace std;

inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}

typedef long long ll;

ll fast_pow(ll x, ll y) {
  ll z = 1;
  for (; y; y >>= 1, x = x * x % mod) {
    if (y & 1) {
      z = z * x % mod;
    }
  }
  return z;
}

inline ll notHasN(ll n, ll a) {
  return fast_pow(n - 1, a);
}
inline ll hasN(ll n, ll a) {
  return (fast_pow(n, a) - notHasN(n, a) + mod) % mod;
}
inline int popcnt(ll x) {
  int res = 0;
  while (x) {
    x &= x - 1;
    ++ res;
  }
  return res;
}

int xs[N], ys[N], xcnt, ycnt;
int sz[50][50];
struct ggwp {
  int x1, y1, x2, y2, v;
}xws[20];

ll ans;
bool vis[50][50];
inline void clear() {
  xcnt = ycnt = 0;
  ans = 1;
  memset(vis, 0, sizeof vis);
}

bool cmp(const ggwp &a, const ggwp &b) {
  return a.v < b.v;
}


void work() {

  int h = read(), w = read(), m = read(), n = read();

  for (int i = 1; i <= n; ++ i) {
    xws[i].x1 = read();
    xws[i].y1 = read();
    xws[i].x2 = read() + 1;
    xws[i].y2 = read() + 1;
    xws[i].v = read();
    xs[++ xcnt] = xws[i].x1;
    xs[++ xcnt] = xws[i].x2;
    ys[++ ycnt] = xws[i].y1;
    ys[++ ycnt] = xws[i].y2;
  }
  xs[++ xcnt] = 1;
  xs[++ xcnt] = h + 1;
  ys[++ ycnt] = 1;
  ys[++ ycnt] = w + 1;

  sort(xs + 1, xs + xcnt + 1);
  sort(ys + 1, ys + ycnt + 1);
  xcnt = unique(xs + 1, xs + xcnt + 1) - xs - 1;
  ycnt = unique(ys + 1, ys + ycnt + 1) - ys - 1;

  sort(xws + 1, xws + n + 1, cmp);
  for (int i = 1; i <= n; ++ i) {
    xws[i].x1 = lower_bound(xs + 1, xs + xcnt + 1, xws[i].x1) - xs;
    xws[i].x2 = lower_bound(xs + 1, xs + xcnt + 1, xws[i].x2) - xs - 1;
    xws[i].y1 = lower_bound(ys + 1, ys + ycnt + 1, xws[i].y1) - ys;
    xws[i].y2 = lower_bound(ys + 1, ys + ycnt + 1, xws[i].y2) - ys - 1;
    // printf("(%d %d %d %d)\n", xws[i].x1, xws[i].y1, xws[i].x2, xws[i].y2);
  }

  for (int i = 1; i < xcnt; ++ i) {
    for (int j = 1; j < ycnt; ++ j) {
      sz[i][j] = (xs[i + 1] - xs[i]) * (ys[j + 1] - ys[j]);
    }
  }

  for (int i = 1, j = 1; i <= n; i = j + 1) {
    while (j < n && xws[j + 1].v == xws[i].v) {
      ++ j;
    }
    const int max_value = xws[i].v;

    int a[20], b[50][50], c[2500];
    memset(a, 0, sizeof a);
    memset(b, 0, sizeof b);
    memset(c, 0, sizeof c);

    for (int k = i; k <= j; ++ k) {
      for (int xx = xws[k].x1; xx <= xws[k].x2; ++ xx) {
        for (int yy = xws[k].y1; yy <= xws[k].y2; ++ yy) {
          if (!vis[xx][yy]) {
            a[k] += sz[xx][yy];
            b[xx][yy] |= (1 << (k - i));
          }
        }
      }
    }

    ll total_size = 0;
    for (int x = 1; x <= xcnt; ++ x) {
      for (int y = 1; y <= ycnt; ++ y) {
        if (b[x][y]) {
          total_size += sz[x][y];
          // printf("get %d, %d\n", x, y);
        }
      }
    }

    int max_popcnt = 0;
    for (int x = 1; x <= xcnt; ++ x) {
      for (int y = 1; y <= ycnt; ++ y) {
        c[b[x][y]] += sz[x][y];
        max_popcnt = max(max_popcnt, popcnt(b[x][y]));
      }
    }

    ll res = fast_pow(max_value, total_size);
    if (max_popcnt > 1) {
      res = (res + fast_pow(max_value - 1, total_size)) % mod;
      for (int k = 0; k < j - i + 1; ++ k) {
        res = (res - fast_pow(max_value - 1, total_size - c[1 << k])
          * fast_pow(max_value, c[1 << k]) % mod + mod) % mod;
      }
    } else {
      res = (res - fast_pow(max_value - 1, total_size) + mod) % mod;
    }

    ans = ans * res % mod;
    // printf("res = %d\n", res);
    // printf("total_size = %d\n", total_size);

    for (int k = i; k <= j; ++ k) {
      for (int xx = xws[k].x1; xx <= xws[k].x2; ++ xx) {
        for (int yy = xws[k].y1; yy <= xws[k].y2; ++ yy) {
          vis[xx][yy] = 1;
        }
      }
    }

  }

  // printf("%d\n", ans);

  ll remaid_size = 0;
  for (int x = 1; x < xcnt; ++ x) {
    for (int y = 1; y < ycnt; ++ y) {
      if (!vis[x][y]) {
        remaid_size += sz[x][y];
      }
    }
  }
  ans = ans * fast_pow(m, remaid_size) % mod;

}

int main(int argc, char const *argv[]) {

  freopen("matrix.in", "r", stdin);
  freopen("matrix.out", "w", stdout);

  int T = read();
  while (T --) {
    clear();
    work();
    printf("%lld\n", ans);
  }

  return 0;
}