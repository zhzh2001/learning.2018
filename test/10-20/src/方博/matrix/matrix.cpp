#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')ch=getchar();
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=105;
struct xxx{
	int lx,ly,rx,ry;
	int v;
}a[N];
const int mod=1e9+7;
int mmph[10005],mmpw[10005];
int b[N],c[N];
int hh[N],ww[N];
int n;
int f[N][N];
ll t[N][N];
int s[11];
int u[N];
int h,w,m;
int ht,wt;
inline ll ksm(ll x,ll y)
{
	ll ret=1;
	while(y){
		if(y&1)ret=ret*x%mod;
		x=x*x%mod;
		y=y/2;
	}
	return ret;
}
inline ll check(int now)
{
	ll ret=1;
	for(register int i=2;i<=ht;i++)
		for(register int j=2;j<=wt;j++){
			int mi=f[i][j];
			for(register int k=1;k<=n;k++)
				if(!u[k])continue;
				else if(a[k].lx<=hh[i]&&a[k].rx>=hh[i]&&a[k].ly<=ww[j]&&a[k].ry>=ww[j])
					mi=min(mi,a[k].v-1);
			if(mi==f[i][j])ret=ret*t[i][j]%mod;
			else ret=ret*ksm(mi,(hh[i]-hh[i-1])*(ww[j]-ww[j-1]))%mod;
		}
	return ret;
}
inline void dfs(int k,int t)
{
	if(k>n){
		s[t]=(s[t]+check(t))%mod;
		return;
	}
	u[k]=0;
	dfs(k+1,t);
	u[k]=1;
	dfs(k+1,t+1);
	u[k]=0;
}
int main()
{
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	int T=read();
	while(T--){
		h=read();w=read();m=read();n=read();
		for(int i=1;i<=n;i++){
			a[i].lx=read(),a[i].ly=read();
			a[i].rx=read(),a[i].ry=read();
			a[i].v=read();
		}
		for(int i=1;i<=b[0];i++)
			mmph[b[i]]=0;
		for(int i=1;i<=c[0];i++)
			mmpw[c[i]]=0;
		b[0]=c[0]=0;
		b[++b[0]]=0,c[++c[0]]=0;b[++b[0]]=h,c[++c[0]]=w;
		for(int i=1;i<=n;i++)
			b[++b[0]]=a[i].lx,b[++b[0]]=a[i].lx-1,b[++b[0]]=a[i].lx+1;
		for(int i=1;i<=n;i++)
			b[++b[0]]=a[i].rx,b[++b[0]]=a[i].rx-1,b[++b[0]]=a[i].rx+1;
		for(int i=1;i<=n;i++)
			c[++c[0]]=a[i].ly,c[++c[0]]=a[i].ly-1,c[++c[0]]=a[i].ly+1;
		for(int i=1;i<=n;i++)
			c[++c[0]]=a[i].ry,c[++c[0]]=a[i].ry-1,c[++c[0]]=a[i].ry+1;
		sort(b+1,b+b[0]+1);
		sort(c+1,c+c[0]+1);
		int th=0;
		for(int i=1;i<=b[0];i++)
			if(!mmph[b[i]]&&b[i]<=h)mmph[b[i]]=++th,hh[th]=b[i];
		int tw=0;
		for(int i=1;i<=c[0];i++)
			if(!mmpw[c[i]]&&c[i]<=w)mmpw[c[i]]=++tw,ww[tw]=c[i];
		for(int i=1;i<=th;i++)
			for(int j=1;j<=tw;j++)
				f[i][j]=m;
		for(int k=1;k<=n;k++){
			int tx=mmph[a[k].lx],ty=mmpw[a[k].ly];
			int ex=mmph[a[k].rx],ey=mmpw[a[k].ry];
			for(int i=tx;i<=ex;i++)
				for(int j=ty;j<=ey;j++)
					f[i][j]=min(f[i][j],a[k].v);
		}
		bool bb=true;
		for(int k=1;k<=n;k++){
			int tx=mmph[a[k].lx],ty=mmpw[a[k].ly];
			int ex=mmph[a[k].rx],ey=mmpw[a[k].ry];
			bool can=false;
			for(int i=tx;i<=ex;i++)
				for(int j=ty;j<=ey;j++)
					if(f[i][j]>=a[k].v)can=true;
			if(can==false)bb=false;
		}
		if(!bb)puts("0");
		else {
			ll ans=0;
			ht=mmph[h];
			wt=mmpw[w];
			for(int i=2;i<=ht;i++)
				for(int j=2;j<=wt;j++)
					t[i][j]=ksm(f[i][j],(hh[i]-hh[i-1])*(ww[j]-ww[j-1]))%mod;
			for(int i=0;i<=n;i++)
				s[i]=0;
			dfs(1,0);
			for(int i=0;i<=n;i++)
				if(i%2==0)ans=(ans+s[i])%mod;
				else ans=(ans-s[i]+mod)%mod;
			writeln(ans);
		}
	}
}
