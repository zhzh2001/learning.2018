#include<bits/stdc++.h>
using namespace std;
int n;
bool vis[705][705];
void out(int l,int r)
{
	if(r-l+1==4){
		cout<<l<<" "<<l+1<<endl;
		cout<<l+2<<" "<<l+3<<endl;
		cout<<l+1<<" "<<l+3<<endl;
		cout<<l<<" "<<l+2<<endl;
		cout<<l+1<<" "<<l+2<<endl;
		cout<<l<<" "<<l+3<<endl;
		return;
	}
	int dis=(r-l+1)/4;
	for(int i=l;i<=l+dis-1;i++){
		for(int j=l+dis;j<=l+2*dis-1;j++){
			if(!vis[i][j])
			for(int k=l+2*dis;k<=l+3*dis-1;k++){
				if(!vis[j][k]&&!vis[i][k])
				for(int s=l+3*dis;s<=r;s++){
					if(!vis[i][s]&&!vis[j][s]&&!vis[k][s]){
						vis[i][j]=vis[i][s]=vis[i][k]=vis[j][k]=vis[j][s]=vis[k][s]=1;
						cout<<i<<" "<<j<<endl;
						cout<<k<<" "<<s<<endl;
						cout<<j<<" "<<s<<endl;
						cout<<i<<" "<<k<<endl;
						cout<<j<<" "<<k<<endl;
						cout<<i<<" "<<s<<endl;
					}
				}
			}
		}
	}
	out(l,l+dis-1),out(l+dis,l+2*dis-1),out(l+2*dis,l+3*dis-1),out(l+3*dis,r);
}
inline void solve()
{
	int num=n;
	while(num%4==0)num=num/4;
	if(num!=1){
		cout<<"NO"<<endl;
		return;
	}
	cout<<"YES"<<endl;
	out(1,n);
}
int main()
{
	freopen("swap.in","r",stdin);
	freopen("swap.out","w",stdout);
	cin>>n;
	if(n==1){
		cout<<"YES"<<endl;
	}else solve();
	return 0;
}
