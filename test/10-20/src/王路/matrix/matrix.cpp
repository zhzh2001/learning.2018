#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
using namespace std;

namespace IO {
static const int kMegaBytes = 1 << 20;
static char buf[kMegaBytes], *S = buf, *T = buf;
inline bool Fetch() { return T = (S = buf) + fread(buf, sizeof(char), sizeof(buf), stdin), S != T; }
inline char NC() { return S == T ? (Fetch() ? *S++ : EOF) : *S++; }
template <typename Int> inline void RI(Int &x) {
  x = 0;
  bool fl = 0;
  char ch;
  for (ch = NC(); isspace(ch); ch = NC())
    ;
  if (ch == '-')
    fl = 1, ch = NC();
  for (; isdigit(ch); ch = NC())
    x = x * 10 + ch - '0';
  if (fl) x = -x;
}
inline char RC() {
  char ch;
  for (ch = NC(); isspace(ch); ch = NC())
    ;
  return ch;
}
} // namespace IO
using IO::RI;

typedef long long ll;
typedef pair<int, int> pii;

const int kMaxN = 10000, kMod = 1e9 + 7;

int T, h, w, m, n;
struct Rect {
  int x1, y1, x2, y2, v;
  ll s;
} a[kMaxN];

inline ll QuickPow(ll x, ll y) {
  ll ret = 1;
  for (; y; y >>= 1, x = x * x % kMod)
    if (y & 1) ret = ret * x % kMod;
  return ret;
}

int main() {
  freopen("matrix.in", "r", stdin);
  freopen("matrix.out", "w", stdout);
  RI(T);
  while (T--) {
    RI(h), RI(w), RI(m), RI(n);
    for (int i = 1; i <= n; ++i) {
      RI(a[i].x1), RI(a[i].y1);
      RI(a[i].x2), RI(a[i].y2);
      RI(a[i].v);
      a[i].s = 1LL * (a[i].y2 - a[i].y1 + 1) * (a[i].x2 - a[i].x1 + 1) % kMod;
    }
    /*if (n == 2) {
      if (a[1].v < a[2].v)
        swap(a[1], a[2]);
      ll cnt = 0;
      for (int i = a[1].x1; i <= a[1].x2; ++i)
        for (int j = a[1].y1; j <= a[1].y2; ++j) {
          if (i >= a[2].x1 && i <= a[2].x2 && j >= a[2].y1 && j <= a[2].y2) {
            ++cnt;
          }
        }
      ll ans = 1;
      fprintf(stderr, "%d * %d * %d * %d * %d\n", a[2].s, QuickPow(a[2].v, (a[2].s + kMod - 1) % kMod), (a[1].s - cnt), QuickPow(a[1].v, (a[1].s - cnt - 1 + kMod)), QuickPow(m, (h * w - a[1].s - a[2].s + cnt + kMod) % kMod));
      ans = a[2].s * QuickPow(a[2].v, (a[2].s + kMod - 1) % kMod) % kMod 
        * (a[1].s - cnt) % kMod * QuickPow(a[1].v, (a[1].s - cnt - 1 + kMod)) % kMod * QuickPow(m, (h * w - a[1].s - a[2].s + cnt + kMod) % kMod);
      printf("%lld\n", ans);
    } else */if (h <= 4 && w <= 4 && m <= 2) {
      int pos = h * w;
      int b[h][w], ans = 0;
      for (int i = 0; i < (1 << pos); ++i) {
        memset(b, 0x00, sizeof b);
        for (int j = 0; j < pos; ++j) {
          b[j / w + 1][j % w + 1] = ((i >> j) & 1) + 1;
        }
        bool valid = true;
        for (int j = 1; j <= n; ++j) {
          for (int k = a[j].x1; k <= a[j].x2; ++k)
            for (int p = a[j].y1; p <= a[j].y2; ++p) 
              if (b[k][p] > a[j].v)
                valid = false;
        }
        if (!valid)
          continue;
        for (int j = 1; j <= n; ++j) {
          bool can = false;
          for (int k = a[j].x1; k <= a[j].x2; ++k)
            for (int p = a[j].y1; p <= a[j].y2; ++p) 
              if (b[k][p] == a[j].v) can = true;
          valid &= can;
        }
        ans += valid;
      }
      printf("%d\n", ans);
    }
  }
  return 0;
}