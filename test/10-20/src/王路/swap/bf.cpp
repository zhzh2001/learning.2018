#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <vector>
using namespace std;

namespace IO {
static const int kMegaBytes = 1 << 20;
static char buf[kMegaBytes], *S = buf, *T = buf;
inline bool Fetch() { return T = (S = buf) + fread(buf, sizeof(char), sizeof(buf), stdin), S != T; }
inline char NC() { return S == T ? (Fetch() ? *S++ : EOF) : *S++; }
template <typename Int> inline void RI(Int &x) {
  x = 0;
  bool fl = 0;
  char ch;
  for (ch = NC(); isspace(ch); ch = NC())
    ;
  if (ch == '-')
    fl = 1, ch = NC();
  for (; isdigit(ch); ch = NC())
    x = x * 10 + ch - '0';
  if (fl) x = -x;
}
inline char RC() {
  char ch;
  for (ch = NC(); isspace(ch); ch = NC())
    ;
  return ch;
}
} // namespace IO
using IO::RI;

typedef long long ll;
typedef pair<int, int> pii;

const int kMaxN = 20;
int n, a[kMaxN];

int main() {
  freopen("swap.in", "r", stdin);
  freopen("bf.out", "w", stdout);
  RI(n);
  vector<pii> v;
  for (int i = 1; i <= n; ++i) {
    for (int j = i + 1; j <= n; ++j)
      v.push_back(make_pair(i, j));
  }
  reverse(v.begin(), v.end());
  do {
    for (int i = 1; i <= n; ++i)
      a[i] = i;
    for (int i = 0; i < (int)v.size(); ++i)
      swap(a[v[i].first], a[v[i].second]);
    bool can = true;
    for (int i = 1; i <= n; ++i)
      if (a[i] != i) can = false;
    if (can) {
      puts("YES");
      for (int i = 0; i < (int)v.size(); ++i)
        printf("%d %d\n", a[v[i].first], a[v[i].second]);
      return 0;
    }
  } while (prev_permutation(v.begin(), v.end()));
  puts("NO");
  return 0;
}