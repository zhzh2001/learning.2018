#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int, int> pii;
int a[1000];
int test(int n) {
  printf(":: N = %d\n", n);
  vector<pii> v;
  for (int i = 1; i <= n; ++i) {
    for (int j = i + 1; j <= n; ++j)
      v.push_back(make_pair(i, j));
  }
  reverse(v.begin(), v.end());
  clock_t st = clock();
  do {
    for (int i = 1; i <= n; ++i)
      a[i] = i;
    for (int i = 0; i < (int)v.size(); ++i)
      swap(a[v[i].first], a[v[i].second]);
    bool can = true;
    for (int i = 1; i <= n; ++i)
      if (a[i] != i) can = false;
    if (clock() - st > CLOCKS_PER_SEC * 300)
      break;
    if (can) {
      puts("YES\\n");
      for (int i = 0; i < (int)v.size(); ++i)
        printf("%d %d\\n", a[v[i].first], a[v[i].second]);
      return 0;
    }
  } while (prev_permutation(v.begin(), v.end()));
  puts("NO");
  return 1;
}

int main() {
  freopen("table.out", "w", stdout);
  for (int i = 8; i <= 9; ++i) {
    test(i);
    fflush(stdout);
  }
  return 0;
}