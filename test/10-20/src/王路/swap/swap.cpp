#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
using namespace std;

namespace IO {
static const int kMegaBytes = 1 << 20;
static char buf[kMegaBytes], *S = buf, *T = buf;
inline bool Fetch() { return T = (S = buf) + fread(buf, sizeof(char), sizeof(buf), stdin), S != T; }
inline char NC() { return S == T ? (Fetch() ? *S++ : EOF) : *S++; }
template <typename Int> inline void RI(Int &x) {
  x = 0;
  bool fl = 0;
  char ch;
  for (ch = NC(); isspace(ch); ch = NC())
    ;
  if (ch == '-')
    fl = 1, ch = NC();
  for (; isdigit(ch); ch = NC())
    x = x * 10 + ch - '0';
  if (fl) x = -x;
}
inline char RC() {
  char ch;
  for (ch = NC(); isspace(ch); ch = NC())
    ;
  return ch;
}
} // namespace IO
using IO::RI;

typedef long long ll;
typedef pair<int, int> pii;

int n;

int main() {
  freopen("swap.in", "r", stdin);
  freopen("swap.out", "w", stdout);
  RI(n);
  if (n == 4) {
    puts("YES\n1 2\n3 4\n2 4\n1 3\n2 3\n1 4");
    return 0;
  }
  if (n == 5) {
    puts("YES\n4 5\n3 5\n3 4\n2 5\n1 4\n1 3\n2 3\n2 4\n1 5");
    return 0;
  }
  if (n == 8) {
    puts("YES\n7 8\n6 8\n6 7\n5 8\n5 7\n5 6\n4 8\n4 7\n4 6\n4 5\n3 8\n3 7\n3 6\n3 5\n3 4\n2 8\n2 7\n2 6\n1 5\n2 5\n2 3\n2 4\n1 6\n1 4\n1 8\n1 2\n1 7\n1 3");
    return 0;
  }
  if (n == 9) {
    puts("YES\n8 9\n7 9\n7 8\n6 9\n6 8\n6 7\n5 9\n5 8\n5 7\n5 6\n4 9\n4 8\n4 7\n4 6\n4 5\n3 9\n3 8\n3 7\n3 6\n3 5\n3 4\n2 9\n2 8\n2 7\n1 6\n1 5\n2 5\n2 6\n2 3\n2 4\n1 7\n1 4\n1 9\n1 2\n1 8\n1 3");
    return 0;
  }
  if (n <= 15)
    puts("NO");
  return puts("YES"), 0;
}