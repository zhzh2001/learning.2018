#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <vector>
using namespace std;

namespace IO {
static const int kMegaBytes = 1 << 20;
static char buf[kMegaBytes], *S = buf, *T = buf;
inline bool Fetch() { return T = (S = buf) + fread(buf, sizeof(char), sizeof(buf), stdin), S != T; }
inline char NC() { return S == T ? (Fetch() ? *S++ : EOF) : *S++; }
template <typename Int> inline void RI(Int &x) {
  x = 0;
  bool fl = 0;
  char ch;
  for (ch = NC(); isspace(ch); ch = NC())
    ;
  if (ch == '-')
    fl = 1, ch = NC();
  for (; isdigit(ch); ch = NC())
    x = x * 10 + ch - '0';
  if (fl) x = -x;
}
inline char RC() {
  char ch;
  for (ch = NC(); isspace(ch); ch = NC())
    ;
  return ch;
}
} // namespace IO
using IO::RI;

typedef long long ll;
typedef pair<int, int> pii;

const int kMaxN = 1e5 + 5;
int n, x[kMaxN], y[kMaxN], sz[kMaxN], sumx, sumy;

int head[kMaxN], ecnt;
struct Edge { int to, nxt; } e[kMaxN << 1];
inline void Insert(int x, int y) {
  e[++ecnt] = (Edge) { y, head[x] };
  head[x] = ecnt;
}

inline double Fact(int n) {
  double ret = 1;
  for (int i = 2; i <= n; ++i)
    ret = ret * i;
  return ret;
}

namespace SubTask1 {
int st, ed;
double tmp;
pair<double, bool> DFS(int x, double len, double p, int f = 0) {
  if (x == ed) {
    tmp += p * len;
    return make_pair(0, true);
  }
  vector<int> v;
  for (int i = head[x]; i; i = e[i].nxt) {
    if (e[i].to == f)
      continue;
    v.push_back(e[i].to);
  }
  sort(v.begin(), v.end());
  double pp = 1.0 / Fact(v.size());
  bool exists = false;
  double ll = 0;
  do {
    bool f = false;
    double prefix = len;
    for (int i = 0; i < (int)v.size(); ++i) {
      pair<double, bool> ret = DFS(v[i], prefix + 1, p * pp, x);
      f |= ret.second;
      if (f) {
        ll += pp * (prefix + 1);
        break;
      }
      prefix = prefix + ret.first;
    }
    if (f)
      exists = true;
  } while (next_permutation(v.begin(), v.end()));
  return make_pair(ll + 1, exists);
}
int Solve() {
  double ans = 0;
  for (int i = 1; i <= n; ++i) {
    if (x[i]) {
      memset(sz, 0x00, sizeof sz);
      for (int j = 1; j <= n; ++j) {
        if (j != i && y[j]) {
          tmp = 0;
          st = i, ed = j;
          DFS(st, 1, 1, 0);
          ans = ans + 1.0 * x[i] / sumx * y[j] / sumy * tmp;
        }
      }
    }
  }
  printf("%.10lf\n", ans);
  return 0;
}
}

namespace SubTask2 {
int Solve() {
  return 0;
}
}

namespace SubTask3 {
int Solve() {
  return 0;
}
}

int main() {
  freopen("tree.in", "r", stdin);
  freopen("tree.out", "w", stdout);
  RI(n);
  bool islist = true;
  for (int i = 1, a, b; i < n; ++i) {
    RI(a), RI(b);
    if (a != i || b != i + 1)
      islist = false;
    Insert(a, b);
    Insert(b, a);
  }
  for (int i = 1; i <= n; ++i) {
    RI(x[i]), RI(y[i]);
    sumx += x[i], sumy += y[i];
  }
  if (n == 1 && x[1] && y[1])
    return puts("0"), 0;
  if (n == 2) {
    double ans = 0;
    for (int i = 1; i <= n; ++i)
      for (int j = 1; j <= n; ++j) {
        if (x[i] && y[j]) {
          ans = ans + 1.0 * x[i] / sumx * y[i] / sumy * abs(i - j);
        }
      }
    printf("%.10lf\n", ans);
    return 0;
  }
  if (n <= 10)
    return SubTask1::Solve(); // force
  if (islist)
    return SubTask2::Solve(); // a list
  return SubTask3::Solve();
}