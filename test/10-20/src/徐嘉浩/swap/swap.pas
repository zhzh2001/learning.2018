var n,i:longint;
    xx:array[1..16]of longint=(0,1,4,5,16,20,25,64,80,100,125,256
    ,320,400,500,625);
    p:boolean;
procedure swap(a,b,c,d:longint);
var i,j:longint;
begin
 for i:=b downto a do
  for j:=c to d do writeln(i,' ',j);
end;
procedure solve(l,r:longint);
var k,a1,a2,a3,a4,a5,b1,b2,b3,b4,b5:longint;
begin
 if l=r then exit;
 if (r-l+1)mod 5=0 then
 begin
  k:=(r-l+1)div 5;
  a1:=l; a2:=l+k; a3:=l+2*k; a4:=l+3*k; a5:=l+4*k;
  b1:=a2-1; b2:=a3-1; b3:=a4-1; b4:=a5-1; b5:=r;
  solve(a1,b1); solve(a2,b2); solve(a3,b3);
  solve(a4,b4); solve(a5,b5);
  swap(a1,b1,a2,b2); swap(a4,b4,a5,b5);
  swap(a2,b2,a3,b3); swap(a2,b2,a5,b5); swap(a3,b3,a5,b5);
  swap(a1,b1,a3,b3); swap(a1,b1,a4,b4); swap(a3,b3,a4,b4);
  swap(a1,b1,a5,b5); swap(a2,b2,a4,b4);
 end
 else begin
  k:=(r-l+1)div 4;
  a1:=l; a2:=l+k; a3:=l+2*k; a4:=l+3*k;
  b1:=a2-1; b2:=a3-1; b3:=a4-1; b4:=r;
  solve(a1,b1); solve(a2,b2); solve(a3,b3); solve(a4,b4);
  swap(a1,b1,a2,b2); swap(a3,b3,a4,b4);
  swap(a2,b2,a4,b4); swap(a1,b1,a3,b3);
  swap(a1,b1,a4,b4); swap(a2,b2,a3,b3);
 end;
end;
begin
 assign(input,'swap.in');
 assign(output,'swap.out');
 reset(input);
 rewrite(output);
 read(n);
 for i:=1 to 16 do
  if xx[i]=n then p:=true;
 if not p then writeln('NO')
 else begin
  writeln('YES');
  if (n<>0)and(n<>1) then solve(1,n);
 end;
 close(input);
 close(output);
end.
