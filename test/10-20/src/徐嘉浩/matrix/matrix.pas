uses math;
var t,h,w,n,m,i,j,k,p,ans:longint;
    a,l1,r1,l2,r2,zh:array[0..1000000]of longint;
    b:array[0..1000,0..1000]of longint;
function pd():boolean;
var i,j,k,ans:longint;
begin
 for i:=1 to h do
  for j:=1 to w do b[i,j]:=a[(i-1)*h+j];
 for k:=1 to n do
 begin
   ans:=0;
  for i:=l1[k] to l2[k] do
   for j:=r1[k] to r2[k] do ans:=max(ans,b[i,j]);
  if ans<>zh[k] then exit(false);
 end;
 exit(true);
end;
procedure dfs(k:longint);
var i:longint;
begin
 if k=h*w+1 then
 begin
  if pd then inc(ans);
  exit;
 end;
 for i:=1 to m do
 begin
  a[k]:=i;
  dfs(k+1);
 end;
end;
begin
 assign(input,'matrix.in');
 assign(output,'matrix.out');
 reset(input);
 rewrite(output);
 read(t);
 for k:=1 to t do
 begin
  read(h,w,m,n);
  for i:=1 to n do read(l1[i],r1[i],l2[i],r2[i],zh[i]);
  dfs(1);
  writeln(ans);
 end;
 close(input);
 close(output); 
end.