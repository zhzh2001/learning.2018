/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
//#define N 500055
//#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int n,m,ans,a[10];
int g(int x,int l,int r){
	while (x>r) x=l+(x-r)-1;
	return x;
}
int f[12]={1,2,3,4,2,4,1,3,2,3,1,4};
void pri(int a1,int a2,int a3,int a4){
	a[1]=a1;a[2]=a2;a[3]=a3;a[4]=a4;
	F(i,0,11){
		wri(a[f[i]]);if (i&1) put;
	}
}
int f2[4]={0,1,3,2};
void solve(int x){
	F(i,1,x/4){
		pri(i*4-3,i*4-2,i*4-1,i*4);
	}
	F(i,1,x/4){
		F(j,i+1,x/4){
			int l1=i*4-3,r1=i*4,l2=j*4-3,r2=j*4;
			F(k,0,3){
				F(p,l1,r1){
					wrn(p,g(l2+(p-l1),l2,r2));
				}
			}
		}
	}
}
signed main(){
	freopen("swap.in","r",stdin);
	freopen("swap.out","w",stdout);
	n=read();
	if (n%4==0){
		puts("YES");
		solve(n);
	}
	else puts("NO");
	return 0;
}
