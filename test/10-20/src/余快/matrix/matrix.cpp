/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
#define N 500055
//#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int n,m,ans,h,w,a[N],_x[N],_y[N],totx,toty,tx,ty,gp[N],x[N],y[N],num[50][50],p[50][50],g[50][50];
int f[2][5005];
struct xx{
	int x1,y1,x2,y2;
}z[N];
pa ax[N],ay[N];
int ksm(ll x,ll k){
	ll sum=1;
	while (k){if (k&1) sum=(ll)sum*x%mod;x=(ll)x*x%mod;k=k>>1;}
	return sum;
}
ll le[5000];
inline void add(int &x,int k){
	x+=k;x-=(x>=mod)?mod:0;
}
signed main(){
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	for (int ci=read();ci;ci--){
		h=read();w=read();m=read();n=read();
		F(i,1,n){
			z[i].x1=read();z[i].y1=read();z[i].x2=read();z[i].y2=read();a[i]=read();
			_x[i*2-1]=z[i].x1;_x[i*2]=z[i].x2;
			_y[i*2-1]=z[i].y1;_y[i*2]=z[i].y2;
		}
		sort(_x+1,_x+n*2+1);sort(_y+1,_y+n*2+1);
		totx=toty=tx=ty=0;
		F(i,1,n*2){
			if (_x[i]!=_x[i-1]) totx++;
			x[totx]=_x[i];
			if (_y[i]!=_y[i-1]) toty++;
			y[toty]=_y[i];
		}
		F(i,1,totx){
			ax[++tx]=mp(x[i],x[i]);
			if (i<totx&&x[i]+1<x[i+1]) ax[++tx]=mp(x[i]+1,x[i+1]-1);
		}
		F(i,1,toty){
			ay[++ty]=mp(y[i],y[i]);
			if (i<toty&&y[i]+1<y[i+1]) ay[++ty]=mp(y[i]+1,y[i+1]-1);
		}
		F(i,1,tx) F(j,1,ty) g[i][j]=(ax[i].se-ax[i].fi+1)*(ay[j].se-ay[j].fi+1),p[i][j]=m,num[i][j]=0;
		F(i,1,n){
			F(j,1,tx){
				if (ax[j].fi<=z[i].x1&&z[i].x1<=ax[j].se) z[i].x1=j;
				if (ax[j].fi<=z[i].x2&&z[i].x2<=ax[j].se) z[i].x2=j;
			}
			F(j,1,ty){
				if (ay[j].fi<=z[i].y1&&z[i].y1<=ay[j].se) z[i].y1=j;
				if (ay[j].fi<=z[i].y2&&z[i].y2<=ay[j].se) z[i].y2=j;				
			}
			F(j,z[i].x1,z[i].x2){
				F(k,z[i].y1,z[i].y2){
					num[j][k]+=(1<<(i-1));p[j][k]=min(p[j][k],a[i]);
				}
			}
		}
		//f[i]表示每个矩形的要求有没有达到
		int tpp=(1<<n)-1;
		F(i,0,tpp) le[i]=0;		
		ll cqz=1LL*h*w;
		F(i,1,tx) F(j,1,ty){
			if (num[i][j]) le[num[i][j]]+=g[i][j],cqz-=g[i][j],gp[num[i][j]]=p[i][j]; 
		}
		ans=ksm(m,cqz);
		F(i,0,tpp) f[0][i]=f[1][i]=0;
		f[0][0]=1;
		F(i,1,tpp){
			if (!le[i]) continue;
			int wzp=0,v1=ksm(gp[i]-1,le[i]);
			int v2=(ksm(gp[i],le[i])-v1+mod)%mod;
			F(j,1,n){
				if (gp[i]==a[j]&&(i&(1<<(j-1)))) wzp|=(1<<(j-1));
			}
			F(j,0,tpp){
				add(f[1][j],1LL*f[0][j]*v1%mod);
				add(f[1][j|wzp],1LL*f[0][j]*v2%mod);
			}
			F(j,0,tpp){
				f[0][j]=f[1][j];f[1][j]=0;
			}
		}
		ans=1LL*ans*f[0][tpp]%mod;
		wrn(ans);
	}
	return 0;
}
