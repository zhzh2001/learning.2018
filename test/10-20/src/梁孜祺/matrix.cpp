#include<bits/stdc++.h>
#define int long long
#define m(x,y) (x-1)*n+y-1
using namespace std;
int read() {
  int x=0,f=0; char ch = getchar();
  for (;ch<'0'||ch>'9';ch=getchar()) if (ch=='-') f=1;
  for (;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
  return f?-x:x;
}
int a[55][55], h, w, n, m;
struct data {
  int lx, ly, rx, ry, v;
}b[15];
const int mod = 1e9+7;
int ksm(int x, int y) {
  int sum = 1;
  while(y) {
  	if (y&1) sum = 1ll *sum*x%mod;
	y>>=1;
	x = 1ll*x*x%mod; 
  }
  return sum;
}
void solve() {
  h = read(); w = read();
  n = read(); m = read();
  if (n == 1) {
  	int lx = read(), ly = read();
  	int rx = read(), ry = read();
  	int v = read();
    int ans = ksm(m,h*w-(rx-lx+1)*(ry-ly+1));
    ans = (ans+(rx-lx+1)*(ry-ly+1)*ksm(v-1, (rx-lx+1)*(ry-ly+1)-1)%mod)%mod;
    printf("%lld\n", ans);
  }else if (m <= 2) {
  	int ans = 0;
  	for (int i = 1; i <= n; i++)
  	  b[i] = (data){read(), read(), read(),read(),read()};
  	for (int i = 1; i < 1<<(h*w); i++) {
  	  for (int j = 1; j <= h; j++)
  	    for (int k = 1; k <= w; k++)
  	      if ((1<<m(j,k))&i) a[j][k] = 1;
  	      else a[j][k] = 2;
  	  bool flag = 0, F = 0;
  	  for (int j = 1; j <= n; j++) {
  	    int lx = b[j].lx, ly = b[j].ly;
		int rx = b[j].rx, ry = b[j].ry;
		int v = b[j].v;
		for (int k = lx; k <= rx; k++)
		  for (int l = ly; l <= ry; l++) {
		    if (a[k][l] == v) flag = 1;
		    if (a[k][l] > v) F = 1;
		  }
	  }
	  if(flag &&(!F)) {
	    ans++;
	    for (int i = 1; i <= h; i++) {
	      for (int j = 1; j <= w; j++) cout << a[i][j] <<" ";
	      cout <<"\n";
		}
		puts("");
	  }
	}
	printf("%lld\n", ans);
	return;
  }
}
signed main() {
  freopen("matrix.in","r",stdin);
  freopen("matrix.out","w",stdout);
  int T = read();
  while (T--) solve();
  return 0;
}
