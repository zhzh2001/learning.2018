#include <bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
using namespace std;
inline GG read(){
	GG k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(GG x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(GG x){ write(x);puts("");}
int tot, n, a[10], t[30][2], g[30][2], vis[30], b[10];
void doit() {
  memcpy(b,a,sizeof a);
  for (int i = 1; i <= tot; i++)
    swap(b[g[i][0]], b[g[i][1]]);
/*  for (int i = 1; i <= tot; i++)
    cout << g[i][0] <<" "<< g[i][1] <<"\n";*/
  for (int i = 1; i <= n; i++)
    if (b[i] != i) return;
  for (int i = 1; i <= tot; i++)
    cout << g[i][0] <<" "<< g[i][1] <<"\n";
  puts("YES");
  exit(0);
}
void dfs(int x) {
  if (x == tot+1) {
  	doit();
  	return;
  }
  for (int i = 1; i <= tot; i++)
    if (!vis[i]) {
      vis[i] = 1;
      g[x][0] = t[i][0];
      g[x][1] = t[i][1];
      dfs(x+1);
      vis[i] = 0;
	}
}
int main() {
  n = read();
  for (int i = 1; i <= n; i++)
    a[i] = i;
  for (int i = 1; i <= n; i++)
    for (int j = i+1; j <= n; j++)
      t[++tot][0] = i, t[tot][1] = j;
  cout << tot << endl;
  dfs(1);
  return 0;
}
