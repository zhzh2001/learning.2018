#include<bits/stdc++.h>
using namespace std;
int read() {
  int x=0,f=0; char ch = getchar();
  for (;ch<'0'||ch>'9';ch=getchar()) if (ch=='-') f=1;
  for (;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
  return f?-x:x;
}
int n;
int main() {
  freopen("swap.in","r",stdin);
  freopen("swap.out","w",stdout);
  n = read();
  if (n == 1) return puts("YES");
  if (n == 2) puts("NO");
  else if (n == 3) puts("NO");
  else if (n == 4) {
  	puts("YES");
  	printf("1 2\n3 4\n2 4\n1 3\n2 3\n1 4");
  }else if (n == 5){
  	puts("YES");
  	printf("1 2\n1 3\n1 4\n2 3\n2 5\n3 5\n1 5\n3 4\n2 4\n4 5\n");
  }else puts("NO");
  return 0;
}
