#include<bits/stdc++.h>
using namespace std;
int read() {
  int x=0,f=0; char ch = getchar();
  for (;ch<'0'||ch>'9';ch=getchar()) if (ch=='-') f=1;
  for (;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
  return f?-x:x;
}
int n, ax[N], ay[N], X, Y;
int head[N], cnt;
void insert(int u, int v) {
  e[++cnt] = (edge){head[u], v}; head[u] = cnt;
  e[++cnt] = (edge){head[v], u}; head[v] = cnt;
}
double dis[N];
void dfs(int x, int fa) {
  for (int y, i = head[x]; i; i = e[i].nxt)
    if ((y = e[i].to) != fa) {
      dis[y] = dis[x]*(Y-ay[x])/Y+1;
      dfs(y, x);
	}
}
int main() {
  n = read(); 
  for (int i = 1; i < n; i++) {
    int x = read(), y = read();
    insert(x, y);
  }
  for (int i = 1; i <= n; i++)
    ax[i] = read(), ay[i] = read(), X += ax[i], Y += ay[i];
  dfs(1, 0);  
  return 0;
}
