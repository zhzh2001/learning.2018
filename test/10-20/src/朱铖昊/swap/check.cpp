#include<bits/stdc++.h>
using namespace std;
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
const int N=1005;
inline void gg1()
{
	puts("GG1");
	exit(0);
}
inline void gg2()
{
	puts("GG2");
	exit(0);
}
int a[N],f[N][N],n,x,y;
int main()
{
	freopen("swap.out","r",stdin);
	freopen("check.out","w",stdout);
	read(n);
	for (int i=1;i<=n;++i)
		a[i]=i;
	for (int i=1;i<=n*(n-1)/2;++i)
	{
		read(x);
		read(y);
		if (f[x][y])
		{
			cout<<x<<' '<<y<<'\n';
			gg1();
		}
		f[x][y]=1;
		swap(a[x],a[y]);
	}
	for (int i=1;i<=n;++i)
		if (a[i]!=i)
			gg2();
	puts("AK");
	return 0;
}