#include<bits/stdc++.h>
using namespace std;
int n;
// const int f13[52]={1,2,3,4,1,5,6,7,1,8,9,10,1,11,12,13,0,0,0,0,2,5,8,11,2,6,9,12,2,7,10,13,3,5,9,13,3,6,10,11,3,7,8,12,4,5,10,12,4,6,8,13};
const int f[80]={1,2,3,4,1,5,6,7,1,8,9,10,1,11,12,13,1,14,15,16,2,5,8,11,2,6,9,14,2,7,12,15,2,10,13,16,3,5,10,15,3,6,11,16,3,7,9,13,3,8,12,14,4,5,13,14,4,6,10,12,4,7,8,16,4,9,11,15,5,9,12,16,6,8,13,15,7,10,11,14};
inline void sc(int a,int b,int c,int d)
{
	printf("%d %d\n",a,b);
	printf("%d %d\n",c,d);
	printf("%d %d\n",b,d);
	printf("%d %d\n",a,c);
	printf("%d %d\n",b,c);
	printf("%d %d\n",a,d);
}
inline void work(int l,int r)
{
	int len=r-l+1,bc=len/4;
	if (bc==1)
	{
		sc(l,l+1,r-1,r);
		return;
	}
	for (int i=0;i<bc;++i)
		for (int j=0;j<bc;++j)
			sc(l+i,l+bc+j,l+bc*2+(i+j)%bc,l+bc*3+(i*2+j)%bc);
	work(l,l+bc-1);
	work(l+bc,l+2*bc-1);
	work(l+bc*2,l+bc*3-1);
	work(l+bc*3,r);
}
int main()
{
	freopen("swap.in","r",stdin);
	freopen("swap.out","w",stdout);
	cin>>n;
	// cout<<n<<'\n';
	if (n==13)
	{
		puts("YES");
		for (int i=0;i<13;++i)
			sc(f13[i*4],f13[i*4+1],f13[i*4+2],f13[i*4+3]);
	}
	int m=n;
	while (m!=1)
	{
		if (m%4!=0)
		{
			puts("NO");
			return 0;
		}
		m/=4;
	}
	puts("YES");
	if (n==16)
	{
		for (int i=0;i<20;++i)
			sc(f[i*4],f[i*4+1],f[i*4+2],f[i*4+3]);
	}
	else
		work(1,n);
	return 0;
}