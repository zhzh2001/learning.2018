#include<bits/stdc++.h>
using namespace std;
const int n=13;
int m;
int f[n*2][n*2],g[n*n][10];
inline void add(int a,int b,int c,int d,int p)
{
	int e=1;
	if (f[a][b])
		e=-1;
	f[a][b]+=e;
	f[a][c]+=e;
	f[a][d]+=e;
	f[b][c]+=e;
	f[b][d]+=e;
	f[c][d]+=e;
	if (e==-1)
	{
		g[p][0]=a;
		g[p][1]=b;
		g[p][2]=c;
		g[p][3]=d;
	}
}
inline void sc()
{
	for (int i=1;i<=m;++i)
		for (int j=0;j<4;++j)
			printf("%d,",g[i][j]);
	exit(0);
}
inline void dfs(int x,int y)
{
	if (y==m+1)
		sc();
	bool b=true;
	for (int i=x+1;i<=n;++i)
		if (f[x][i])
			b=false;
	if (b)
		dfs(x+1,y);
	else
	{
		for (int i=x+1;i<=n;++i)
			if (f[x][i])
				for (int j=i+1;j<=n;++j)
					if (f[x][j]&&f[i][j])
						for (int k=j+1;k<=n;++k)
							if (f[x][k]&&f[i][k]&&f[j][k])
							{
								add(x,i,j,k,y);
								dfs(x,y+1);
								add(x,i,j,k,y);
							}
	}
}
int main()
{
	freopen("bl.out","w",stdout);
	m=n*(n-1)/2/6;
	for (int i=1;i<=n;++i)
		for (int j=i+1;j<=n;++j)
			f[i][j]=1;
	for (int i=1;i<=(n-1)/3;++i)
		add(1,i*3-1,i*3,i*3+1,i);	
	dfs(2,6);
}