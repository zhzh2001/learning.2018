#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define pi pair<int,int> 
#define fi first
#define se second
// #define int long long
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
const int N=15;
const int mod=1e9+7;
struct data
{
	pi x,y;
	int v;
	bool operator<(const data &a)const
	{
		return v<a.v;
	}
}a[N],f[N];
int n,m,h,w,ans,t;
int si[N];
inline int ksm(int x,int y)
{
	int ans=1;
	for (;y;y/=2,x=(ll)x*x%mod)
		if (y&1)
			ans=(ll)ans*x%mod;
	return ans;
}
inline pi cross(pi a,pi b)
{
	if (a.fi>b.fi)
		swap(a,b);
	if (a.se<b.fi)
		return (pi){-1,-1};
	return (pi){b.fi,min(b.se,a.se)};
}
inline void dfs(int p,data a,int x,int opt)
{
	if (x==0)
	{
		si[p]+=(a.x.se-a.x.fi+1)*(a.y.se-a.y.fi+1)*opt;
		return;
	}
	dfs(p,a,x-1,opt);
	data b={cross(a.x,f[x].x),cross(a.y,f[x].y),a.v};
	if (b.x.first!=-1&&b.y.first!=-1)
		dfs(p,b,x-1,-opt);
}
inline int work(int x)
{
	int opt=0;
	for (int i=1;i<=n;++i)
	{
		f[i]=a[i];
		if ((x>>(i-1))&1)
		{
			--f[i].v;
			opt=!opt;
		}
	}
	for (int i=1;i<=n;++i)
		si[i]=0;
	sort(f+1,f+1+n);
	for (int i=1;i<=n;++i)
		dfs(i,f[i],i-1,1);
	int ans=1;
	for (int i=1;i<=n;++i)
		ans=(ll)ans*ksm(f[i].v,si[i])%mod;
	int clear=h*w;
	for (int i=1;i<=n;++i)
		clear-=si[i];
	ans=(ll)ans*ksm(m,clear)%mod;
	return opt?mod-ans:ans;
}
signed main()
{
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	read(t);
	while (t--)
	{	
		bool b=false;
		read(h);
		read(w);
		read(m);
		read(n);
		for (int i=1;i<=n;++i)
		{
			read(a[i].x.fi);
			read(a[i].y.fi);
			read(a[i].x.se);
			read(a[i].y.se);
			read(a[i].v);
			if (a[i].v>m)
				b=true;
		}
		if (b)
			puts("0");
		else
		{
			ans=0;
			for (int i=0;i<(1<<n);++i)
				ans=(ans+work(i))%mod;
			cout<<ans<<'\n';
		}
	}
	return 0;
}