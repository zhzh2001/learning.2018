#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int N=100005;
int la[N],to[N*2],pr[N*2],cnt;
int x[N],y[N],px[N],py[N],sx,sy;
int si[N],ch[N];
int n,u,v;
ll ans,S;
inline void add(int x,int y)
{
	to[++cnt]=y;
	pr[cnt]=la[x];
	la[x]=cnt;
}
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void work()
{
	for (int i=1;i<=n;++i)
	{
		px[i]=px[i-1]+x[i];
		py[i]=py[i-1]+y[i];
	}
	ans=0;
	// ans=(ll)sx*sy*(n-1);
	for (int i=1;i<n;++i)
		ans+=(ll)px[i]*(sy-py[i])+(ll)py[i]*(sx-px[i]);
	for (int i=2;i<n;++i)
		ans+=(ll)x[i]*((ll)(n-i)*py[i-1]+(ll)(i-1)*(sy-py[i]));
	// for (int i=1;i<n;++i)
	// 	ans-=(ll)px[i]*py[i]+(ll)(sx-px[i])*(sy-py[i]);
	// ans=(ll)sx*sy*(n-1)-ans;
	printf("%.10lf",(double)ans/sx/sy);
}
inline void getsi(int x,int p)
{
	si[x]=1;
	for (int i=la[x];i;i=pr[i])
		if (to[i]!=p)
		{
			getsi(to[i],x);
			si[x]+=si[to[i]];
		}
	S-=(ll)si[x]*y[x];
}
// inline void dfs(int x,int p,int sum)
// {
// 	S+=(ll)y[x]*sum;
// 	for (int i=la[x];i;i=pr[i])
// 		if (to[i]!=p)
// 			dfs(to[i],x,sum+si[x]-si[to[i]]);
// }
inline void bl()
{
	for (int i=1;i<=n;++i)
		if (x[i])
		{
			S=(ll)sy*n;
			for (int j=1;j<=n;++j)
				si[j]=0;
			getsi(i,0);
			ans+=(ll)S*x[i];
		}
	printf("%.10lf",(double)ans/sx/sy);
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	read(n);
	bool b=true;
	for (int i=1;i<n;++i)
	{
		read(u);
		read(v);
		add(u,v);
		add(v,u);
		if (u!=i||v!=i+1)
			b=false;
	}
	for (int i=1;i<=n;++i)
	{
		read(x[i]);
		read(y[i]);
		sx+=x[i];
		sy+=y[i];
	}
	if (b)
		work();
	else
		bl();
	return 0;
}