#include <cstdio>
#include <cstring>
#include <iostream>
using namespace std;

const int N=5100,mo=1e9+7;


void judge(){
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
}

namespace sub2{
	int h,w,m,n,ans,a[N][N];
	struct info{
		int x,y,_x,_y,v;
	}b[20];
	bool check(){
		for (int t=1;t<=n;t++){
			int Max=0;
			for (int i=b[t].x;i<=b[t]._x;i++){
				for (int j=b[t].y;j<=b[t]._y;j++) Max=max(Max,a[i][j]);
			}
			if (Max==b[t].v) return 0;
		}
		return 1;
	}
	void dfs(int x,int y){
		if (x==h+1) return (void) (ans=(ans+check())%mo);
		if (y==w+1) return (void) (dfs(x+1,1));
		a[x][y]=1; dfs(x,y+1); a[x][y]=0;
		a[x][y]=2; dfs(x,y+1); a[x][y]=0;
	}
	int main(){
		for (int i=1;i<=n;i++) scanf("%d%d%d%d%d",&b[i].x,&b[i].y,&b[i]._x,&b[i]._y,&b[i].v);
		dfs(1,1);
		printf("%d\n",ans);
		return 0;
	}
}
using namespace sub2;

int main(){
	judge();
	int t; scanf("%d",&t);
	while (t--){
		ans=0;
		scanf("%d%d%d%d",&h,&w,&m,&n);
		if (m<=2) return sub2::main(),0;
	}
	return 0;
}
