#include <iomanip>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

const int N=11000;
int n,m,size[N];
long double ans,sumx,sumy,x[N],y[N];
struct info {
	int to,nxt;
} e[N<<1];
int head[N],opt;
void add(int x,int y) {
	e[++opt].to=y;
	e[opt].nxt=head[x];
	head[x]=opt;
}

void judge() {
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
}

void dfs(int u,int fa) {
	for (int i=head[u]; i; i=e[i].nxt) {
		int k=e[i].to;
		if (k==fa) continue;
		dfs(k,u);
		size[u]+=size[k]+1;
	}
}

void fun(int s,int u,int fa,long double sum) {
	ans+=(x[s]*1.00000000/sumx*1.00000000)*(y[u]*1.00000000/sumy*1.0000000)*sum;
	for (int i=head[u]; i; i=e[i].nxt) {
		int k=e[i].to;
		if (k==fa) continue;
		fun(s,k,u,sum+size[u]-size[k]);
	}
}

int main() {
	judge();
	scanf("%d",&n);
	for (int i=1; i<n; i++) {
		int x,y;
		scanf("%d%d",&x,&y);
		add(x,y);
		add(y,x);
	}
	for (int i=1; i<=n; i++) {
		cin>>x[i]>>y[i];
		sumx+=x[i];
		sumy+=y[i];
	}
	for (int i=1; i<=n; i++) {
		if (x[i]!=0) {
			memset(size,0,sizeof(size));
			dfs(i,0);
			fun(i,i,0,0);
		}
	}
	cout<<fixed<<setprecision(10)<<ans<<endl;
	//cout<<ans<<endl;
	return 0;
}
