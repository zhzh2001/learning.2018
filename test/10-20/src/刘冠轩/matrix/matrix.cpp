#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int t=0;bool p=0;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
	for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
	return p?-t:t;
}
const int N=15;
const int M=55;
int n,m,h,w,x[N],xx[N],y[N],yy[N],v[N],mp[M][M],ans,o;
int solve(){
	int mx;
	for(int i=1;i<=n;i++){
		mx=0;
		for(int j=x[i];j<=xx[i];j++)
			for(int k=y[i];k<=yy[i];k++)
				mx=max(mx,mp[j][k]);
		if(mx!=v[i])return 0;
	}
	return 1;
}
void dfs(int x,int y){
	if(y>w)y=1,x++;
	if(x>h){
		ans+=solve();
		return;
	}
	for(int i=1;i<=m;i++)mp[x][y]=i,dfs(x,y+1);
}
int main(){
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	for(int T=read();T;T--){
		h=read();w=read();
		n=read();m=read();
		for(int i=1;i<=n;i++){
			x[i]=read();y[i]=read();
			xx[i]=read();yy[i]=read();
			v[i]=read();
		}
		ans=0;
		dfs(1,1);
		cout<<ans<<endl;
	}
	return 0;
}
