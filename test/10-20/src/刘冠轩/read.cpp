#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int t=0;bool p=0;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
	for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
	return p?-t:t;
}
int main(){
	int n=read();
	for(int i=1;i<=n;i++){
		int x=read();
		cout<<x<<endl;
	}
	return 0;
}
