#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int t=0;bool p=0;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
	for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
	return p?-t:t;
}
const int N=100005;
int n,x[N],y[N],X,Y,sz[N],fa[N],u,v,head[N],cnt;
struct edge{int to,nt;}e[N*2];
void add(int u,int v){
	e[++cnt]=(edge){v,head[u]};
	head[u]=cnt;
}
long double ans;
void dfs1(int u){
	sz[u]=1;
	for(int i=head[u];i;i=e[i].nt)
	if(e[i].to!=fa[u]){
		fa[e[i].to]=u;
		dfs1(e[i].to);
		sz[u]+=sz[e[i].to];
	}
}
void dfs2(int u,int son,double s){
	ans+=s;
	for(int i=head[u];i;i=e[i].nt)
	if(e[i].to!=son&&e[i].to!=fa[u])
		ans+=s*sz[e[i].to];
	if(fa[u]!=0)dfs2(fa[u],u,s);
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	for(int i=1;i<n;i++){
		u=read();v=read();
		add(u,v);add(v,u);
	}
	for(int i=1;i<=n;i++){
		X+=x[i]=read();
		Y+=y[i]=read();
	}
	if(n==2){
		ans=X*Y;
		ans-=x[1]*y[1];
		ans-=x[2]*y[2];
		ans/=X*Y;
	}else if(n<=10||X+Y==2){
		for(int i=1;i<=n;i++)if(x[i])
			for(int j=1;j<=n;j++)if(y[j]){
				if(i==j)continue;else{
					dfs1(i);
					dfs2(fa[j],j,1.0*x[i]*y[j]/X/Y);
				}
			}
	}else{
		for(int i=1;i<n;i++)
			for(int j=i+1;j<=n;j++)
				ans+=1.0*((j-1)*x[i]*y[j]+(n-i)*y[i]*x[j])/X/Y;
	}
	printf("%.10lf",(double)ans);
	return 0;
}
