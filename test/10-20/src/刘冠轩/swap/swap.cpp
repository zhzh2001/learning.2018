#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int t=0;bool p=0;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
	for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
	return p?-t:t;
}
const int N=670;
int n,cnt,a[N*N],x1[N*N],x2[N*N],b[N];
bool vis[N*N],flag;
bool solve(){
	for(int i=1;i<=n;i++)b[i]=i;
	for(int i=1;i<=cnt;i++)
		swap(b[x1[a[i]]],b[x2[a[i]]]);
	for(int i=1;i<=n;i++)
		if(b[i]!=i)return 0;
	return 1;
}
void print(){
	puts("YES");
	for(int i=1;i<=cnt;i++)
		cout<<x1[a[i]]<<' '<<x2[a[i]]<<'\n';
}
void dfs(int x){
	if(flag)return;
	if(x>cnt){
		if(solve())print(),flag=1;
		return;
	}
	for(int i=1;i<=cnt;i++)
		if(!vis[i]){
			a[x]=i;
			vis[i]=1;
			dfs(x+1);
			vis[i]=0;
		}
}
int main(){
	freopen("swap.in","r",stdin);
	freopen("swap.out","w",stdout);
	n=read();
	if(n<=5){
		for(int i=1;i<n;i++)
			for(int j=i+1;j<=n;j++)
				x1[++cnt]=i,x2[cnt]=j;
		dfs(1);
		if(!flag)puts("NO");
	}else{
		if(n%4==0)puts("YES");
		if(n%4==1)puts("YES");
		if(n%4==2)puts("NO");
		if(n%4==3)puts("NO");
	}
	return 0;
}
