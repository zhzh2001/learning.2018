//#include<iostream>
#include<algorithm>
#include<fstream>
#include<math.h>
#include<string.h>
//#define cout fout
//#define cin fin
using namespace std;
ifstream fin("swap.in");
ofstream fout("swap.out");
long long n;
bool vis[1003][1003];
void print(int l,int r){
	if(r-l+1==4){
		fout<<l<<" "<<l+1<<endl;
		fout<<l+2<<" "<<l+3<<endl;
		fout<<l+1<<" "<<l+3<<endl;
		fout<<l<<" "<<l+2<<endl;
		fout<<l+1<<" "<<l+2<<endl;
		fout<<l<<" "<<l+3<<endl;
		return;
	}
	int dis=(r-l+1)/4;
	for(int i=l;i<=l+dis-1;i++){
		for(int j=l+dis;j<=l+2*dis-1;j++){
			if(!vis[i][j])
			for(int k=l+2*dis;k<=l+3*dis-1;k++){
				if(!vis[j][k]&&!vis[i][k])
				for(int s=l+3*dis;s<=r;s++){
					if(!vis[i][s]&&!vis[j][s]&&!vis[k][s]){
						vis[i][j]=vis[i][s]=vis[i][k]=vis[j][k]=vis[j][s]=vis[k][s]=1;
						fout<<i<<" "<<j<<endl;
						fout<<k<<" "<<s<<endl;
						fout<<j<<" "<<s<<endl;
						fout<<i<<" "<<k<<endl;
						fout<<j<<" "<<k<<endl;
						fout<<i<<" "<<s<<endl;
					}
				}
			}
		}
	}
	print(l,l+dis-1),print(l+dis,l+2*dis-1),print(l+2*dis,l+3*dis-1),print(l+3*dis,r);
}
inline void solve(){
	long long num=n;
	while(num%4==0){
		num=num/4;
	}
	if(num!=1){
		fout<<"NO"<<endl;
		return;
	}
	fout<<"YES"<<endl;
	print(1,n);
}
int main(){
	fin>>n;
	if(n==1){
		fout<<"YES"<<endl;
	}else if(n==5){
		fout<<"YES"<<endl;
	}if((n*(n-1)/2)%6!=0){
		fout<<"NO"<<endl;
	}else if(n<=3){
		fout<<"NO"<<endl;
		return 0;
	}else{
		solve();
	}
	return 0;
}
