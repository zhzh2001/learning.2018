//#include<iostream>
#include<algorithm>
#include<fstream>
#include<iomanip>
#include<math.h>
#include<string.h>
using namespace std;
ifstream fin("tree.in");
ofstream fout("tree.out");
long long n;
long long head[200003],nxt[200003],to[200003],tot;
inline void add(int u,int v){
	to[++tot]=v,nxt[tot]=head[u],head[u]=tot;
}
double x[400003],y[400003];
double f[400003],g[400003],sumx[400003],sumy[400003],X,Y,ans;
long long ff[400003],deg[400003],sz[400003];
void dfs1(int now,int fa){
	f[now]=1;
	sz[now]=1;
	ff[now]=fa;
	sumx[now]=x[now]/X,sumy[now]=y[now]/Y;
	for(int i=head[now];i;i=nxt[i]){
		if(to[i]!=fa){
			dfs1(to[i],now);
			sz[now]+=sz[to[i]];
			sumx[now]+=sumx[to[i]],sumy[now]+=sumy[to[i]];
			f[now]+=f[to[i]];
		}
	}
}
void dfs2(int x,int fa){
	double sum=0;
	sum=sum+g[fa]+1;
	for(int i=head[x];i;i=nxt[i]){
		if(to[i]!=fa){
			sum=sum+f[to[i]];
		}
	}
	for(int i=head[x];i;i=nxt[i]){
		if(to[i]!=fa){
			g[to[i]]=sum-f[to[i]];
			dfs2(to[i],x);
		}
	}
}
void calc(int x,int fa){
	for(int i=head[x];i;i=nxt[i]){
		if(to[i]!=fa){
			calc(to[i],x);
			ans+=sumx[to[i]]*f[to[i]]*(1.00-sumy[to[i]]);
			ans+=(1.00-sumx[to[i]])*g[to[i]]*sumy[to[i]];
		}
	}
}
int main(){
	fin>>n;
	for(int i=1;i<=n-1;i++){
		int u,v;
		fin>>u>>v;
		deg[u]++,deg[v]++;
		add(u,v),add(v,u);
	}
	for(int i=1;i<=n;i++){
		fin>>x[i]>>y[i];
		X+=x[i],Y+=y[i];
	}
	dfs1(1,0),dfs2(1,0);
	calc(1,0);
	fout<<fixed<<setprecision(10)<<ans<<endl;
	return 0;
}
/*
f[i]=1/degree+sigma((f[son]+f[i])/degree)
g[i]=1/degree[fa[i]]+sigma(f[fa'son]+g[i])/degree[fa[i]]+(g[fa[i]]+g[i])/degree[fa[i]]
*/
