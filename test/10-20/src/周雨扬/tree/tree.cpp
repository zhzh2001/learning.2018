#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
#define pii pair<int,int>
#define fi first
#define se second
#define y1 wzptxdy
using namespace std;
inline int read(){
	#define gc getchar
	int x=0,f=1; char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc())
		if (ch=='-') f=0;
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return f?x:-x;
}
const int N=100005;
struct edge{
	int to,next;
}e[N*2];
int head[N],tot;
int n,SX,SY;
long long ans;
int X[N],Y[N],sz[N];
void add(int x,int y){
	e[++tot]=(edge){y,head[x]};
	head[x]=tot;
}
void dfs(int x,int fa){
	sz[x]=1;
	for (int i=head[x];i;i=e[i].next)
		if (e[i].to!=fa){
			dfs(e[i].to,x);
			sz[x]+=sz[e[i].to];
			X[x]+=X[e[i].to];
			ans+=1ll*Y[x]*X[e[i].to]*sz[e[i].to];
		}
	ans+=1ll*Y[x]*(SX-X[x])*(n-sz[x]);
}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	For(i,1,n-1){
		int x=read(),y=read();
		add(x,y); add(y,x);
	}
	For(i,1,n){
		SX+=(X[i]=read());
		SY+=(Y[i]=read());
	}
	dfs(1,0); 
	printf("%.15lf\n",1.0*ans/SX/SY);
	//如果爆精度了我就A了出题人 
}
