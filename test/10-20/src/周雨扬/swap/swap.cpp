#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
#define pii pair<int,int>
#define fi first
#define se second
#define y1 wzptxdy
using namespace std;
inline int read(){
	#define gc getchar
	int x=0,f=1; char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc())
		if (ch=='-') f=0;
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return f?x:-x;
}
const int N=700;
int cnt;
pii ans[N*N];
void solve(int n){
	if (n<=1) return;
	For(i,1,n-4)
		ans[++cnt]=pii(i,n-3);
	ans[++cnt]=pii(n-3,n-2);
	Rep(i,n-4,1)
		ans[++cnt]=pii(i,n-2);
	For(i,1,n-4)
		ans[++cnt]=pii(i,n-1);
	ans[++cnt]=pii(n-1,n);
	Rep(i,n-4,1)
		ans[++cnt]=pii(i,n);
	ans[++cnt]=pii(n-3,n-1);
	ans[++cnt]=pii(n-2,n);
	ans[++cnt]=pii(n-3,n);
	ans[++cnt]=pii(n-2,n-1);
	solve(n-4);
}
/*
2 1 4 3
swap(1,3),swap(2,4) 4,3,2,1
swap(1,4),swap(2,3) 1,2,3,4

2,1,4,3,6,5

swap(1,3),swap(2,4) 4,3,2,1,6,5
swap(1,4),swap(2,3) 1,2,3,4,6,5

//1,5,1,6,2,5,2,6,3,5,3,6,4,5,4,6
//1
*/
int n,id[N],s[N][N];
inline void write(int x){
	static int a[20],top;
	for (top=0;x;a[++top]=x%10,x/=10);
	for (;top;putchar(a[top--]+'0'));
}
int main(){
	freopen("swap.in","r",stdin);
	freopen("swap.out","w",stdout);
	scanf("%d",&n);
	if (n%4>=2) return puts("NO"),0;
	solve(n);
	puts("YES");
	For(i,1,n) id[i]=i;
	For(i,1,cnt){
		swap(id[ans[i].fi],id[ans[i].se]);
		s[ans[i].fi][ans[i].se]++;
		write(ans[i].fi); putchar(' ');
		write(ans[i].se); puts("");
		//printf("%d %d\n",ans[i].fi,ans[i].se);
	}
	//For(i,1,n) assert(id[i]==i);
	//For(i,1,n) For(j,i+1,n) assert(s[i][j]==1);
}
/*
1 2
1 3
1 4
2 3
2 5
3 5
1 5
3 4
2 4
4 5

1 2
1 3
2 4
1 4
2 3
3 4
*/
