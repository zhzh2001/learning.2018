#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
#define pii pair<int,int>
#define fi first
#define se second
#define y1 wzptxdy
using namespace std;
inline int read(){
	#define gc getchar
	int x=0,f=1; char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc())
		if (ch=='-') f=0;
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return f?x:-x;
}
const int mo=1000000007;
const int N=30;
int a[N][N],X[N],Y[N],V[N];
int x1[N],x2[N],y1[N],y2[N],v[N];
int h,w,n,mx,ans;
int power(int x,int y){
	int s=1;
	for (;y;y/=2,x=1ll*x*x%mo)
		if (y&1) s=1ll*s*x%mo;
	return s;
}
int id[N],sum[N];
bool cmp(int x,int y){
	return V[x]<V[y];
}
int calc(){
	V[n+1]=mx;
	For(i,1,n+1) id[i]=i,sum[i]=0;
	sort(id+1,id+n+1,cmp);
	For(i,1,*X-1) For(j,1,*Y-1) a[i][j]=n+1;
	Rep(i,n,1) For(j,x1[id[i]],x2[id[i]]) For(k,y1[id[i]],y2[id[i]]) a[j][k]=i;
	int ans=1;
	For(i,1,*X-1) For(j,1,*Y-1)
		sum[a[i][j]]+=(X[i+1]-X[i])*(Y[j+1]-Y[j]);
	For(i,1,n+1) if (V[id[i]]<=0&&sum[i]) return 0;
		else ans=1ll*ans*power(V[id[i]],sum[i])%mo;
	return ans;
}
void dfs(int x,int cnt){
	if (x==n+1){
		if (cnt) ans=(ans+mo-calc())%mo;
		else ans=(ans+calc())%mo;
		return;
	}
	V[x]=v[x];
	dfs(x+1,cnt);
	V[x]=v[x]-1;
	dfs(x+1,cnt^1);
}
void solve(){
	X[2]=h=read(); Y[2]=w=read();
	X[0]=Y[0]=2; X[1]=Y[1]=1; X[2]++; Y[2]++;
	mx=read(); n=read(); ans=0;
	For(i,1,n){
		X[++*X]=x1[i]=read();
		Y[++*Y]=y1[i]=read();
		X[++*X]=x2[i]=read();
		Y[++*Y]=y2[i]=read();
		X[*X]++; Y[*Y]++;
		v[i]=read();
	}
	sort(X+1,X+*X+1);
	sort(Y+1,Y+*Y+1);
	*X=unique(X+1,X+*X+1)-X-1;
	*Y=unique(Y+1,Y+*Y+1)-Y-1;
	For(i,1,n){
		x1[i]=lower_bound(X+1,X+*X+1,x1[i])-X;
		x2[i]=lower_bound(X+1,X+*X+1,x2[i]+1)-X-1;
		y1[i]=lower_bound(Y+1,Y+*Y+1,y1[i])-Y;
		y2[i]=lower_bound(Y+1,Y+*Y+1,y2[i]+1)-Y-1;
	}
	ans=0;
	dfs(1,0);
	printf("%d\n",ans);
}
int main(){
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	int T=read();
	while (T--) solve();
}
/*
2
3 3 2 2
1 1 2 2 2
2 2 3 3 1
4 4 4 4
1 1 2 3 3
2 3 4 4 2
2 1 4 3 2
1 2 3 4 4
*/
