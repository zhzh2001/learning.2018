#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
#define pii pair<int,int>
#define fi first
#define se second
#define y1 wzptxdy
using namespace std;
inline int read(){
	#define gc getchar
	int x=0,f=1; char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc())
		if (ch=='-') f=0;
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return f?x:-x;
}
int rnd(){
	int x=0;
	For(i,1,4) x=x*10+rand()%10;
	return x;
}
void solve(){
	printf("10000 10000 10000 10\n");
	For(i,1,10){
		int x1=rnd()+1,x2=rnd()+1,y1=rnd()+1,y2=rnd()+1;
		if (x1>x2) swap(x1,x2);
		if (y1>y2) swap(y1,y2);
		printf("%d %d %d %d %d\n",x1,y1,x2,y2,rnd()+1);
	}
}
int main(){
	freopen("matrix.in","w",stdout);
	printf("%d\n",5);
	For(i,1,5) solve();
}
/*
2
3 3 2 2
1 1 2 2 2
2 2 3 3 1
4 4 4 4
1 1 2 3 3
2 3 4 4 2
2 1 4 3 2
1 2 3 4 4
*/
