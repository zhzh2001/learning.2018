#include <bits/stdc++.h>
using namespace std;
typedef long double db;
typedef long long ll;
#define file "tree"
int read(){
	char c=getchar();
	while(c<'0' || c>'9')
		c=getchar();
	int x=0;
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x;
}
const int maxn=100002;
struct graph{
	int n,m;
	struct edge{
		int to,next;
	}e[maxn*2];
	int first[maxn];
	void addedge(int from,int to){
		e[++m]=(edge){to,first[from]};
		first[from]=m;
	}
	int sz[maxn],fa[maxn];
	ll px[maxn],py[maxn];
	void dfs(int u){
		sz[u]=1;
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (v==fa[u])
				continue;
			fa[v]=u;
			dfs(v);
			sz[u]+=sz[v];
		}
	}
	ll s[maxn],s2[maxn],tot;
	ll ans;
	void dfs2(int u){
		ans+=px[u]*(tot-s[u]+s2[u]+n*py[u]);
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (v==fa[u])
				continue;
			s[v]=s[u]+sz[v]*py[v];
			s2[v]=s2[u]+py[u]*(n-sz[v]);
			dfs2(v);
		}
	}
	ll work(){
		dfs(1);
		ans=0;
		tot=0;
		for(int i=1;i<=n;i++)
			tot+=sz[i]*py[i];
		s[1]=sz[1]*py[1];
		dfs2(1);
		return ans;
	}
}g;
int main(){
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	int n=g.n=read();
	for(int i=1;i<n;i++){
		int u=read(),v=read();
		g.addedge(u,v);
		g.addedge(v,u);
	}
	ll sx=0,sy=0;
	for(int i=1;i<=n;i++){
		g.px[i]=read(),g.py[i]=read();
		sx+=g.px[i];
		sy+=g.py[i];
	}
	db ans=g.work();
	ans/=sx;
	ans/=sy;
	ans=n-ans;
	printf("%.10f",(double)ans);
}