#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
#define file "matrix"
int read(){
	char c=getchar();
	while(c<'0' || c>'9')
		c=getchar();
	int x=0;
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x;
}
const int mod=1000000007;
ll power(ll x,ll y){
	ll ans=1;
	while(y){
		if (y&1)
			ans=ans*x%mod;
		x=x*x%mod;
		y>>=1;
	}
	return ans;
}
struct rect{
	int x1,y1,x2,y2,v;
}r[233];
int cnt[(1<<10)+2],msk[(1<<10)+2];
ll dp[(1<<10)+2][(1<<10)+2];
int mn[(1<<10)+2];
int main(){
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	int T=read();
	while(T--){
		memset(dp,0,sizeof(dp));
		memset(cnt,0,sizeof(cnt));
		memset(msk,0,sizeof(msk));
		int h=read(),w=read(),m=read(),n=read();
		for(int i=0;i<n;i++)
			r[i].x1=read(),r[i].y1=read(),r[i].x2=read(),r[i].y2=read(),r[i].v=read();
		for(int i=0;i<(1<<n);i++){
			int lx=1,ly=1,rx=h,ry=w;
			for(int j=0;j<n;j++)
				if (i>>j&1){
					lx=max(lx,r[j].x1);
					ly=max(ly,r[j].y1);
					rx=min(rx,r[j].x2);
					ry=min(ry,r[j].y2);
				}
			if (lx>rx || ly>ry)
				cnt[i]=0;
			else cnt[i]=(rx-lx+1)*(ry-ly+1);
		}
		for(int i=(1<<n)-1;i>=0;i--){
			// fprintf(stderr,"cnt[%d]=%d\n",i,cnt[i]);
			for(int j=0;j<(1<<n);j++)
				if ((j!=i) && ((j|i)==j))
					cnt[i]-=cnt[j];
			// fprintf(stderr,"cnt[%d]=%d\n",i,cnt[i]);
		}
		for(int i=0;i<(1<<n);i++)
			mn[i]=m;
		for(int i=0;i<n;i++)
			for(int j=0;j<(1<<n);j++)
				if (j>>i&1)
					mn[j]=min(mn[j],r[i].v);
		for(int i=0;i<n;i++)
			for(int j=0;j<(1<<n);j++)
				if (j>>i&1){
					if (mn[j]==r[i].v)
						msk[j]|=1<<i;
				}
		dp[0][0]=power(m,cnt[0]);
		for(int i=0;i<(1<<n)-1;i++){
			ll x=power(mn[i+1]-1,cnt[i+1]),y=power(mn[i+1],cnt[i+1]);
			y=(y-x+mod)%mod;
			for(int j=0;j<(1<<n);j++){
				dp[i+1][j|msk[i+1]]=(dp[i+1][j|msk[i+1]]+dp[i][j]*y)%mod;
				dp[i+1][j]=(dp[i+1][j]+dp[i][j]*x)%mod;
			}
		}
		printf("%lld\n",(dp[(1<<n)-1][(1<<n)-1]%mod+mod)%mod);
	}
}