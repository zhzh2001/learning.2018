#include <bits/stdc++.h>
using namespace std;
#define file "swap"
int read(){
	char c=getchar();
	while(c<'0' || c>'9')
		c=getchar();
	int x=0;
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x;
}
int a[1007],n;
void swp(int i,int j){
	if (i>j)
		swap(i,j);
	printf("%d %d\n",i,j);
	swap(a[i],a[j]);
}
void qaq(int p1,int p2,int k){
	for(int i=p1+k-1;i>=p1;i--)
		for(int j=p2+k-1;j>=p2;j--)
			swp(i,j);
}
void rev(int l,int r){
	for(int i=l;i<=r;i++)
		for(int j=i+1;j<=r;j++)
			swp(i,j);
}
void tat(int x,int y){
	int w=n/4;
	qaq((x-1)*w+1,(y-1)*w+1,w);
}
int main(){
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	n=read();
	if (n==1)
		return puts("YES"),0;
	if (n%4)
		return puts("NO"),0;
	puts("YES");
	for(int i=1;i<=n;i++)
		a[i]=i;
	tat(1,2);
	tat(3,4);
	tat(2,4);
	tat(1,3);
	tat(2,3);
	tat(1,4);
	for(int i=1;i<=4;i++)
		rev((i-1)*(n/4)+1,i*(n/4));
	// for(int i=1;i<=n;i++)
		// fprintf(stderr,"%d ",a[i]);
	// fprintf(stderr,"%.5f",1.0*clock()/CLOCKS_PER_SEC);
}