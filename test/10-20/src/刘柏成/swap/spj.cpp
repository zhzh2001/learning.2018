#include <bits/stdc++.h>
using namespace std;
char s[103];
bool used[1003][1003];
int a[1003];
int main(){
	freopen("swap.in","r",stdin);
	int n;
	scanf("%d",&n);
	fclose(stdin);
	freopen("swap.out","r",stdin);
	scanf("%s",s);
	if (n%4 && n!=1){
		if (s[0]=='N')
			puts("OK");
		else puts("WA");
		return 0;
	}
	if (s[0]=='N')
		return puts("WA"),0;
	for(int i=1;i<=n;i++)
		a[i]=i;
	int x,y;
	while(scanf("%d%d",&x,&y)!=EOF){
		if (x>=y || x<1 || x>n || y<1 || y>n)
			return puts("WA"),0;
		if (used[x][y])
			return puts("WA"),0;
		used[x][y]=1;
		swap(a[x],a[y]);
	}
	for(int i=1;i<=n;i++)
		if (a[i]!=i)
			return puts("WA"),0;
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++)
			if (!used[i][j])
				return puts("WA"),0;
	puts("OK");
}