#include <cstdio>
#include <cctype>
#include <algorithm>
#define P 1000000007
int T, n, m, d, q, cx, X[35], cy, Y[35], b[35][35], id[35][35], S[15];
struct matrix{
	int x1, y1, x2, y2, v;
}a[15];
long long qpow(long long a, long long b){
	long long s = 1;
	for (; b; b >>= 1, (a *= a) %= P) if (b & 1) (s *= a) %= P;
	return s;
}
long long calc(){
	long long ans = 1;
	for (register int i = 1; i < cx; ++i)
		for (register int j = 1; j < cy; ++j)
			b[i][j] = d;
	for (register int i = 1; i <= q; ++i)
		for (register int r = a[i].x1 + 1; r <= a[i].x2; ++r)
			for (register int c = a[i].y1 + 1; c <= a[i].y2; ++c)
				if (a[i].v >= b[r][c]) S[i] -= (X[r] - X[r - 1]) * (Y[c] - Y[c - 1]);
				else b[r][c] = a[i].v, S[id[r][c]] -= (X[r] - X[r - 1]) * (Y[c] - Y[c - 1]), id[r][c] = i;
	for (register int i = 1; i < cx; ++i)
		for (register int j = 1; j < cy; ++j)
			ans *= qpow(b[i][j], (X[i] - X[i - 1]) * (Y[j] - Y[j - 1]));
	return ans;
}
int main(){
	freopen("matrix.in", "r", stdin);
	freopen("matrix.out", "w", stdout);
	for (scanf("%d", &T); T; --T){
		scanf("%d%d%d%d", &n, &m, &d, &q);
		for (register int i = 1; i <= q; ++i)
			scanf("%d%d%d%d%d", &a[i].x1, &a[i].y1, &a[i].x2, &a[i].y2, &a[i].v), --a[i].x1, --a[i].y1;
		X[0] = 0, X[1] = n, cx = 2, Y[0] = 0, Y[1] = m, cy = 2;
		for (register int i = 1; i <= q; ++i)
			X[cx++] = a[i].x1, Y[cy++] = a[i].y1, X[cx++] = a[i].x2, Y[cy++] = a[i].y2;
		std :: sort(X, X + cx), std :: sort(Y, Y + cy);
		cx = std :: unique(X, X + cx) - X, cy = std :: unique(Y, Y + cy) - Y;
		for (register int i = 1; i <= q; ++i)
			a[i].x1 = std :: lower_bound(X, X + cx, a[i].x1) - X,
			a[i].y1 = std :: lower_bound(Y, Y + cy, a[i].y1) - Y,
			a[i].x2 = std :: lower_bound(X, X + cx, a[i].x2) - X,
			a[i].y2 = std :: lower_bound(Y, Y + cy, a[i].y2) - Y;
		long long ans = calc();
		for (register int i = 1; i <= q; ++i) --a[i].v;
		ans -= calc();
		printf("%lld\n", ans);
	}
}
