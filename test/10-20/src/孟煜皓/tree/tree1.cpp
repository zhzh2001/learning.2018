#include <cstdio>
#define N 100005
int n, a[N], b[N], sa, sb;
long double x[N], y[N], p1, p2, s1, s2, ans;
int main(){
	freopen("tree.in", "r", stdin);
	freopen("tree.out", "w", stdout);
	if (n == 1) return printf("0"), 0;
	scanf("%d", &n);
	for (register int i = 1; i < n; ++i) scanf("%*d%*d");
	for (register int i = 1; i <= n; ++i) scanf("%d%d", a + i, b + i), sa += a[i], sb += b[i];
	for (register int i = 1; i <= n; ++i) x[i] = 1.0 * a[i] / sa, y[i] = 1.0 * b[i] / sb, s1 += y[i] * i, s2 += y[i];
	for (register int i = 1; i <= n; ++i){
		p1 += y[i] * i, s1 -= y[i] * i, p2 += y[i], s2 -= y[i];
		ans += x[i] * (p2 * i - p1 - s1 + s2 * i);
	}
	printf("%.15lf", (double)ans);
} 
