var
  n, sa, sb, i : longint;
  a, b : array[0..100005] of longint;
  x, y : array[0..100005] of extended;
  p1, p2, s1, s2, ans : extended;
begin
  assign(input, 'tree.in');
  assign(output, 'tree.out');
  reset(input);
  rewrite(output);
  readln(n);
  for i := 1 to n - 1 do readln;
  for i := 1 to n do 
    begin
      readln(a[i], b[i]);
      inc(sa, a[i]); inc(sb, b[i]);
    end;
  for i := 1 to n do 
    begin
      x[i] := a[i] / sa; y[i] := b[i] / sb;
      s1 := s1 + y[i] * i; s2 := s2 + y[i];
    end;
  for i := 1 to n do 
    begin
      p1 := p1 + y[i] * i; p2 := p2 + y[i];
      s1 := s1 - y[i] * i; s2 := s2 - y[i];
      ans := ans + x[i] * (p2 * i - p1 - s1 + s2 * i);
    end;
  writeln(ans : 0 : 15);
  close(input);
  close(output);
end.
