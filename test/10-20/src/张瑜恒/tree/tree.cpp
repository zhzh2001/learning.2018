#include<cstdio>
#include<cmath>
using namespace std;
struct arr
  {
   int aa,bb;
  }na[200010];
int head[100010],deep[100010],pa[100010][25],n,nn,l,r;
int maxdeep(int k)
  {
   return ((int)(log((k)*1.0)/log(2.0)));
  }
int lowbit(int k)
  {
   return (k&(-k));
  }
void add(int x,int y)
  {
   nn=nn+1;na[nn].aa=y;na[nn].bb=head[x];head[x]=nn;
  }
void dfs(int u,int k)
  {
   for (int i=head[u];i;i=na[i].bb)
     {
      int j=na[i].aa;
      if (j==k) continue;
      deep[j]=deep[u]+1;pa[j][0]=u;dfs(j,u);
     }
  }
int lca(int x,int y)
  {
   if (deep[x]<deep[y])
     {
      int t=x;x=y;y=t;
     }
   for (int i=deep[x]-deep[y];i;i=i-lowbit(i))
     x=pa[x][maxdeep(lowbit(i))];
   if (x==y) return x;
   for (int i=maxdeep(2*n);i>=0;i--)
     if (pa[x][i]!=pa[y][i])
       {
        x=pa[x][i];y=pa[y][i];
       }
   return pa[x][0];
  }
int main()
  {
   freopen("tree.in","r",stdin);
   freopen("tree.out","w",stdout);
   
   scanf("%d",&n);
   for (int i=1;i<=n-1;i++)
     {
      int x,y;
      scanf("%d%d",&x,&y);
      add(x,y);add(y,x);
     }
   for (int i=1;i<=n;i++)
     {
      int x,y;
      scanf("%d%d",&x,&y);
      if (x==1) l=i;
	  if (y==1) r=i; 
     }
   deep[1]=1;
   dfs(1,0);
   for (int i=1;i<=maxdeep(2*n);i++)
     for (int j=1;j<=n;j++)
       pa[j][i]=pa[pa[j][i-1]][i-1];
   int sum=lca(l,r);
   if (sum==l)
     {
      double s=deep[r]-deep[l];
      printf("%0.8lf\n",s);
      return 0;
     }
   if (sum==r)
     {
      double s=deep[l]-deep[r];
      printf("%0.8lf\n",s);
      return 0;
     }
   double s=deep[l]+deep[r]-2*deep[sum];
   printf("%0.8lf\n",s);
   return 0;
  }
