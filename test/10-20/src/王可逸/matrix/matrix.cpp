#include <bits/stdc++.h>
#define int long long
using namespace std;
namespace mcl {
	const int mod=1e9+7;
	int ans, t, h, w, m, n,a[12][12];
	int mx(int use,int x1,int y1,int x2,int y2) {
		int MX=0;
		for(int x=x1; x<=x2; x++)
			for(int y=y1; y<=y2; y++) {
//				cout<<(use%(1<<((x-1)*w+y+1)))/(1<<((x-1)*w+y))<<'\n';
				MX=max((use%(1<<((x-1)*w+y)))/(1<<((x-1)*w+y-1))+1,MX);	
			}
		return MX;
	}
	void subtask1() {
//		cout<<"subtask1\n";
		int x1[120],y1[120],x2[120],y2[120],v[120];
		if(m==1) {ans=1;return;}
		for(int i=1;i<=n;i++) cin>>x1[i]>>y1[i]>>x2[i]>>y2[i]>>v[i];
		for(int i=0; i<(1<<(h*w)) ; i++) {
			for(int j=1; j<=n; j++) {
//				cout<<mx(i,x1[j],y1[j],x2[j],y2[j])<<" "<<v[j]<<'\n';
				if(mx(i,x1[j],y1[j],x2[j],y2[j])!=v[j]) {
					goto L1;
				}
			}
//			cout<<i<<'\n';
			ans++;
L1:
			;
		}
	}
	signed main() {
		// freopen("matrix.in","r",stdin);
		// freopen("matrix.out","w",stdout);
		cin >> t;
		while (t--) {
			cin >> h >> w >> m >> n;
			if (h <= 4 && w <= 4 && m <= 2) {
				ans=0;
				subtask1();
				cout<<ans<<'\n';
			}
		}
		return 0;
	}
}
signed main() {
	mcl::main();
}
