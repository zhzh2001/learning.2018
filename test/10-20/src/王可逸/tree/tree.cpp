#include <bits/stdc++.h>
using namespace std;
int n,m,s,cnt,head[520000],fa[520000][25],depth[520000];
struct node {
	int next,to;
} sxd[1040000];
inline int read() {
	int X=0,w=0;
	char ch=0;
	while(!isdigit(ch)) {
		w|=ch=='-';
		ch=getchar();
	}
	while(isdigit(ch)) X=(X<<3)+(X<<1)+(ch^48),ch=getchar();
	return w?-X:X;
}
inline void write(int x) {
	if(x<0) putchar('-'),x=-x;
	if(x>9) write(x/10);
	putchar(x%10+'0');
}
void add(int u,int v) {
	sxd[++cnt].next=head[u];
	sxd[cnt].to=v;
	head[u]=cnt;
}
void dfs1(int at,int f) {
	depth[at]=depth[f]+1;
	fa[at][0]=f;
	for(int i=1; (1<<i)<=depth[at]; i++)
		fa[at][i]=fa[fa[at][i-1]][i-1];
	for(int i=head[at]; i; i=sxd[i].next) {
		if(sxd[i].to==f) continue;
		dfs1(sxd[i].to,at);
	}
}
double ans=0;
int sum=0;
void dfs(int at,int zd,int road,int faa) {
	if(at==zd) {
		ans+=road;
		sum++;
		return;	
	}
	for(int i=head[at];i;i=sxd[i].next) {
		if(sxd[i].to==faa) continue;
		dfs(sxd[i].to,zd,road+1,at);	
	}	
}
int lca(int x,int y) {
	if(depth[x]>depth[y]) swap(x,y);
	for(int i=20; i>=0; i--)
		if(depth[x]<=depth[y]-(1<<i))
			y=fa[y][i];
	if(x==y) return x;
	for(int i=20; i>=0; i--)
		if(fa[x][i]!=fa[y][i]) {
			x=fa[x][i];
			y=fa[y][i];
		}
	return fa[x][0];
}
int aa[120000],bb[120000];
int main() {
//	freopen("tree.in","r",stdin);
//	freopen("tree.out","w",stdout);
	cin>>n;
	s=1;
	for(int i=1; i<n; i++) {
		int x,y;
		cin>>x>>y;
		add(x,y);
		add(y,x);
	}
	dfs1(1,0);
	int tota=0,totb=0,wa,wb;
	for(int i=1; i<=n; i++) {
		cin>>aa[i]>>bb[i];
		tota+=aa[i];
		totb+=bb[i];
		if(aa[i]) wa=i;
		if(bb[i]) wb=i;
	}
	if(n<=2) {
		double ans = 0;
		ans = ans + aa[1] * bb[2] + aa[2] * bb[1];
		ans /= ((aa[1] + aa[2]) * (bb[1] + bb[2]));
		printf("%.12lf", ans);

	} else {
		dfs1(1,0);
		dfs(wa,wb,0,0);
		double aaa=ans/sum;
		printf("%.12lf",aaa);
	}
	return 0;
}
