#include <bits/stdc++.h>
using namespace std;
int n;
void subtask1() {
	if(n==1) {
		puts("YES");
	} else if(n==2) {
		puts("NO");
	} else if(n==3) {
		puts("NO");
	} else if(n==4) {
		puts("YES");
		puts("1 2");
		puts("3 4");
		puts("2 4");
		puts("1 3");
		puts("2 3");
		puts("1 4");
	} else if(n==5) {
		puts("YES");
		puts("1 2");
		puts("1 3");
		puts("1 4");
		puts("2 4");
		puts("4 5");
		puts("1 5");
		puts("3 5");
		puts("3 4");
		puts("2 3");
		puts("2 5");
	} else if(n==6) {
		puts("NO");
	} else if(n==7) {
		puts("NO");
	} else if(n==8) {
		puts("NO");
	}
	//以下没有验证过
	else if(n==16 || n==64 || n==256){
		puts("YES");
	}
	else puts("NO");
}
signed main() {
	freopen("swap.in","r",stdin);
	freopen("swap.out","w",stdout);
	cin>>n;
	subtask1();
	return 0;
}