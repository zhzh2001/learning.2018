#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}
typedef long double ld;
typedef long long ll;

const int N = 675;
int n;

signed main(){
	freopen("swap.in","r",stdin);
	freopen("swap.in","w",stdout);
	n = read();
	if(n == 1){
		puts("YES");
		return 0;
	}
	if(n == 2){
		puts("NO");
		return 0;
	}
	if(n == 3){
		puts("NO");
		return 0;
	}
	if(n == 4){
		puts("YES");
		printf("1 2\n3 4\n2 4\n1 3\n2 3\n1 4");
		return 0;
	}
	if(n == 5){
		puts("NO");
		return 0;
	}
	if(n == 6){
		puts("NO");
		return 0;
	}
	if(n == 7){
		puts("NO");
		return 0;
	}
	if(n == 8){
		puts("YES");
		for(int i = 1;i < 8;++i)
			for(int j = i+1;j < 8;++j)
				printf("%d %d\n",i,j);
		return 0;
	}
	if(n == 9){
		puts("NO");
		return 0;
	}
	if(n == 10){
		puts("NO");
		return 0;
	}
	return 0;
}
