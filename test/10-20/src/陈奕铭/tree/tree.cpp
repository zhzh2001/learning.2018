#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}
typedef long double ld;
typedef long long ll;

const int N = 100005;
int nxt[N*2],head[N],to[N*2],cnt;
int n;
int a[N],b[N];
ll size[N];
ld A[N],B[N],ans;
ll tota,totb,tot;

inline void insert(int x,int y){
	to[++cnt] = y; nxt[cnt] = head[x]; head[x] = cnt;
}

void dfs1(int x,int fa){
	size[x] = 1;
	B[x] = (ld)b[x]/tot;
//	A[x] = (ld)b[x]/tot;
	for(int i = head[x];i;i = nxt[i])
		if(to[i] != fa){
			int y = to[i];
			dfs1(y,x);
			size[x] += size[y];
			B[x] += B[y];
		}
	for(int i = head[x];i;i = nxt[i])
		if(to[i] != fa){
			int y = to[i];
			A[x] += (size[x]-size[y])*B[y]+A[y];
		}
}

void dfs2(int x,int fa){
	size[x] = 1;
	B[x] = (ld)b[x]/tot;
	ld totB = 0;
	A[x] = 0;
	for(int i = head[x];i;i = nxt[i]){
		int y = to[i];
		size[x] += size[y];
		B[x] += B[y];
		totB += B[y];
	}
	for(int i = head[x];i;i = nxt[i])
		A[x] += ((size[x]-size[to[i]])*B[to[i]]+A[to[i]]);	//当前点为起点的总贡献 
	ans += A[x]*a[x];
//	A[x] += (ld)b[x]/tot;
	
	ll Size = size[x]; ld BB = B[x],AA = A[x];
	for(int i = head[x];i;i = nxt[i])
		if(to[i] != fa){
			int y = to[i];
			size[x] = Size-size[y];
			A[x] = AA-(Size-size[y])*B[y]-A[y];
			A[x] -= size[y]*(totB-B[y]);
			B[x] = BB-B[y];
			
			ll size1 = size[y];
			ld B1 = B[y];
			ld A1 = A[y];
			dfs2(y,x);
			size[y] = size1;
			B[y] = B1;
			A[y] = A1;
		}
}

signed main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n = read();
	for(int i = 1;i < n;++i){
		int x = read(),y = read();
		insert(x,y); insert(y,x);
	}
	for(int i = 1;i <= n;++i){
		a[i] = read(); b[i] = read();
		tota += a[i];
		totb += b[i];
	}
	tot = tota*totb;
	dfs1(1,1);
	dfs2(1,1);
	printf("%.12LF",ans);
	return 0;
}
