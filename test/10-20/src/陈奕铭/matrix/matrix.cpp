#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}
typedef long double ld;
typedef long long ll;

const int N = 10005;
const ll mod = 1e9+7;
ll h,w,m,n;
ll ans;
struct node{
	ll x1,y1,x2,y2,v;
	friend bool operator <(node a,node b){
		return a.v < b.v;
	}
}mix[15];
ll d1[75],cnt1,num1;
ll d2[75],cnt2,num2;
ll dp[1025][2];
ll val[25][25];
ll cove[25][25];
ll Bin[11];

inline ll erfen1(ll x){
	ll l = 1,r = cnt1,mid;
	while(l <= r){
		mid = (l+r)>>1;
		if(d1[mid] == x) return mid;
		if(d1[mid] > x) r = mid-1;
		else l = mid+1;
	}
	return l;
}
inline ll erfen2(ll x){
	ll l = 1,r = cnt2,mid;
	while(l <= r){
		mid = (l+r)>>1;
		if(d2[mid] == x) return mid;
		if(d2[mid] > x) r = mid-1;
		else l = mid+1;
	}
	return l;
}

inline ll qpow(ll x,ll n){
	ll ans = 1;
	while(n){
		if(n&1) ans = ans*x%mod;
		x = x*x%mod;
		n >>= 1;
	}
	return ans;
}

inline ll bok(ll a,ll b){
	return (d1[a+1]-d1[a])*(d2[b+1]-d2[b])%mod;
}

signed main(){
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	Bin[0] = 1; for(ll i = 1;i <= 10;++i) Bin[i] = Bin[i-1]<<1;
	ll T = read();
	while(T--){
		memset(val,0,sizeof val);
		memset(dp,0,sizeof dp);
		memset(cove,0,sizeof cove);
		num1 = num2 = cnt1 = cnt2 = 0;
		h = read(),w = read(),m = read(),n = read();
		bool flag = false;
		for(ll i = 1;i <= n;++i){
			mix[i].x1 = read(); mix[i].y1 = read();
			mix[i].x2 = read(); mix[i].y2 = read();
			d1[++num1] = mix[i].x1; d1[++num1] = mix[i].x2+1;
			d2[++num2] = mix[i].y1; d2[++num2] = mix[i].y2+1;
			mix[i].v = read();
			if(mix[i].v > m) flag = true;
		}
		if(flag){
			puts("0");
			continue;
		}
		d1[++num1] = 1; d1[++num1] = h+1;
		d2[++num2] = 1; d2[++num2] = w+1;
		sort(mix+1,mix+n+1);
		sort(d1+1,d1+num1+1);
		sort(d2+1,d2+num2+1);
		for(ll i = 1;i <= num1;++i) if(d1[i] != d1[i-1]) d1[++cnt1] = d1[i];
		for(ll i = 1;i <= num2;++i) if(d2[i] != d2[i-1]) d2[++cnt2] = d2[i];
		ans = 1;
		for(ll i = 1;i <= n;++i){
			ll x1 = erfen1(mix[i].x1),x2 = erfen1(mix[i].x2+1)-1;
			ll y1 = erfen2(mix[i].y1),y2 = erfen2(mix[i].y2+1)-1;
			ll tot = 0;
			for(ll a = x1;a <= x2;++a)
				for(ll b = y1;b <= y2;++b)
					if(val[a][b] == 0 || val[a][b] == mix[i].v){
						cove[a][b] |= Bin[i-1];
						val[a][b] = mix[i].v;
					}
		}
		ll p = 1; dp[0][0] = 1;
		for(ll a = 1;a < cnt1;++a)
			for(ll b = 1;b < cnt2;++b){
				for(ll i = 0;i < Bin[n];++i) dp[i][p] = 0;
				for(ll i = 0;i < Bin[n];++i){
					if(dp[i][p^1] != 0){
						if(val[a][b] == 0){	//��Լ�� 
							(dp[i][p] += dp[i][p^1]*qpow(m,bok(a,b))) %= mod;
						}else if(val[a][b] == 1) (dp[i|cove[a][b]][p] += dp[i][p^1]) %= mod;
						else{
							(dp[i][p] += dp[i][p^1]*qpow(val[a][b]-1,bok(a,b))%mod) %= mod;
							(dp[i|cove[a][b]][p] += dp[i][p^1]*(((qpow(val[a][b],bok(a,b))-qpow(val[a][b]-1,bok(a,b)))%mod+mod)%mod)%mod) %= mod;
						}
					}
				}
				p ^= 1;
			}
		printf("%lld\n",(dp[Bin[n]-1][p^1]%mod+mod)%mod);
//		printf("%lld\n",dp[Bin[n]-1][p^1]);
	}
	return 0;
}
