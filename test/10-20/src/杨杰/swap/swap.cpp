#include <cstdio>
#include <algorithm>
using namespace std;
int n,cnt;
#define pii pair<int,int>
#define mk make_pair
#define fi first
#define se second
pii p[30],kk[30];
int a[30],vis[30];
void check() {
	for (int i=1;i<=n;i++) a[i]=i;
	for (int i=1;i<=cnt;i++) {
		swap(a[kk[i].fi],a[kk[i].se]);
	}
	for (int i=1;i<=n;++i) if (a[i]!=i) return;
	puts("YES");
	for (int i=1;i<=cnt;i++) {
		printf("%d %d\n",kk[i].fi,kk[i].se);
	}
	exit(0);
}
void dfs(int x) {
	if (x==cnt+1) {
		check();
		return;
	}
	for (int i=1;i<=cnt;i++) if (!vis[i]){
		vis[i]=1;
		kk[x]=p[i];
		dfs(x+1);
		vis[i]=0;
	}
}
int main() {
	freopen("swap.in","r",stdin);
	freopen("swap.out","w",stdout);
	scanf("%d",&n);
	if (n>5) return puts("YES"),0;
	for (int i=1;i<=n;i++) 
		for (int j=i+1;j<=n;j++) p[++cnt]=mk(i,j);
	printf("%d\n",cnt);
	dfs(1);
	puts("NO");
	return 0;
}

