#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
const int N=10005,P=1e9+7;
struct node {
	int x1,x2,y1,y2,v;
	bool check(int x,int y) {
		if (x>=x1 && x<=x2 && y>=y1 && y<=y2) return 1;
		else return 0;
	}
}p[15];
int T,h,w,n,m;
int f[1024],g[1024];
int solve() {
	memset(f,0,sizeof f);
	f[0]=1;
	for (int i=1;i<=h;i++)
		for (int j=1;j<=w;j++) {
			int mx=m,ad=0;
			for (int k=1;k<=n;k++) if (p[k].check(i,j)){
				if (p[k].v<mx) mx=p[k].v,ad=1<<(k-1);
				else if (p[k].v==mx) ad|=1<<(k-1);
			}
			for (int k=0;k<(1<<n);k++) {
				g[k]=f[k];f[k]=1ll*f[k]*(mx-1)%P;	
			}
			for (int k=0;k<(1<<n);k++) f[k|ad]=(f[k|ad]+g[k])%P;
		}
	return f[(1<<n)-1];
}
int main() {
	freopen("matrix.in","r",stdin);
	freopen("matrix.out","w",stdout);
	scanf("%d",&T);
	while (T--) {
		scanf("%d%d%d%d",&h,&w,&m,&n);
		for (int i=1;i<=n;i++) {
			scanf("%d%d%d%d%d",&p[i].x1,&p[i].y1,&p[i].x2,&p[i].y2,&p[i].v);
		}
		printf("%d\n",solve());
	}
}
