
#include <cstdio>
using namespace std;
const int N=1e5+7;
struct edge {
	int t,nxt;
}e[N<<1];
int n,cnt,sy,sx;
int head[N],siz[N],chu[N],ru[N];
double ans,now;
double f[N];
void add(int u,int t) {
	e[++cnt].t=t;e[cnt].nxt=head[u];head[u]=cnt;
}
void dfs(int u,int fa) {
	siz[u]=1;
	for (int i=head[u];i;i=e[i].nxt) {
		int t=e[i].t;
		if (t==fa) continue;
		dfs(t,u);
		siz[u]+=siz[t];
	}
} 
void dfs1(int u,int fa) {
	now+=f[u]*chu[u];
	for (int i=head[u];i;i=e[i].nxt) {
		int t=e[i].t;
		if (t==fa) continue;
		f[t]=f[u]+1+(siz[u]-siz[t]-1);
		dfs1(t,u);
	}
}
void dfs2(int u,int fa) {
	ans+=1.0*now/sy*ru[u];
	for (int i=head[u];i;i=e[i].nxt) {
		int t=e[i].t;
		if (t==fa) continue;
		now-=f[t]*chu[t];
		now+=siz[t]*chu[u];
		dfs2(t,u);
		now+=f[t]*chu[t];
		now-=siz[t]*chu[u];
	}
}
int main() {
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	scanf("%d",&n);
	for (int i=1,u,v;i<n;i++) {
		scanf("%d%d",&u,&v);
		add(u,v);add(v,u);
	}
	for (int i=1;i<=n;i++) {
		scanf("%d%d",&ru[i],&chu[i]);
		sx+=ru[i];sy+=chu[i];
	}
	dfs(1,0);
	dfs1(1,0);
	dfs2(1,0);
	printf("%.10lf\n",ans/sx);
}
