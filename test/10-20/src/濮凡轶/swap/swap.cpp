#include <cstdio>
#include <cstdlib>
#include <utility>
#include <fstream>
#include <iostream>

using namespace std;

typedef long long LL;
typedef pair<int, int> pii;

const int maxn = 700;

int n;

ifstream fin("swap.in");
ofstream fout("swap.out");

namespace sxdaknoip
{

	pii ans[maxn * maxn];
	int cnt;

	inline void solve_zheng(int, int);
	inline void solve_dao(int, int);

	inline void swap(int ll, int lr, int rl, int rr)
	{
		if(lr - ll != rr - rl)
		{
			fout << "NO";
			exit(0);
		}
		for(; ll <= lr && rl <= rr; ++ll, ++rl)
			ans[++cnt] = make_pair(ll, rl);
	}

	inline void swap_fan(int l, int r)
	{
		for(; l < r; ++l, --r)
			ans[++cnt] = make_pair(l, r);
	}

	inline void solve_dao(int l, int r)
	{
//		ans[++cnt] = make_pair(23333333, 333333333);
//		cout << l << ' ' << r << endl;
		int len = r - l + 1;
		if(len & 1)
		{
			fout << "NO";
			exit(0);
		}
		if(len == 2)
		{
			ans[++cnt] = make_pair(l, r);
			return;
		}
		int mid = (l + r) >> 1;
		solve_zheng(l, mid);
		solve_zheng(mid + 1, r);
		int midl = (l + mid) >> 1;
		int midr = (mid + 1 + r) >> 1;
		swap(l, midl, mid + 1, midr);
		swap(midl + 1, mid, midr + 1, r);
		swap_fan(l, r);
//		ans[++cnt] = make_pair(233333333, 333333333);
	}

	inline void solve_zheng(int l, int r)
	{
		int len = r - l + 1;
		if(len == 1)
			return;
		if(len & 1)
		{
			fout << "NO";
			exit(0);
		}
		int mid = (l + r) >> 1;
		solve_dao(l, mid);
		solve_dao(mid + 1, r);
		int midl = (l + mid) >> 1;
		int midr = (mid + 1 + r) >> 1;
//		cout << l << ' ' << midl << endl;
//		cout << mid + 1 << ' ' << midr << endl;
		swap(l, midl, mid + 1, midr);
		swap(midl + 1, mid, midr + 1, r);
		swap_fan(l, r);
	}

	int main(int NN)
	{
		LL n = NN;
		solve_zheng(1, n);
		fout << "YES" << '\n';
		for(int i = 1; i <= cnt; ++i)
			fout << ans[i].first << ' ' << ans[i].second << '\n';
		return 0;
	}
}

#include <ctime>
#include <vector>
#include <utility>
#include <iostream>
#include <algorithm>

typedef pair<int, int> pii;

pii a[maxn];
int N;

int aa[maxn];

inline bool pan()
{
	for(int i = 1; i <= n; ++i)
		aa[i] = i;
	for(int i = 0; i < N; ++i)
		swap(aa[a[i].first], aa[a[i].second]);
	for(int i = 1; i <= n; ++i)
		if(aa[i] != i)
			return false;
	return true;
}

inline void get()
{
	for(int i = 1; i <= n; ++i)
		aa[i] = i;
	for(int i = 0; i < N; ++i)
	{
		swap(aa[a[i].first], aa[a[i].second]);
		for(int j = 1; j <= n; ++j)
			fout << aa[j] << ' ';
		fout << endl;
	}
	return;
}

int main()
{
	int tim = clock();
	fin >> n;
	if(n == 0)
	{
		fout << "YES";
		return 0;
	}
	int tn = n;
	while(!(tn % 4))
		tn /= 4;
	if(tn == 1)
	{
		sxdaknoip::main(n);
		return 0;
	}
	for(int i = 1; i <= n; ++i)
		for(int j = i + 1; j <= n; ++j)
			a[N++] = make_pair(i, j);
	do
	{
		if(pan())
		{
			fout << ("YES") << '\n';
			for(int i = 0; i < N; ++i)
				fout << a[i].first << ' ' << a[i].second << endl;
//			get();
			return 0;
		}
	}
	while(clock() - tim <= 900 && (random_shuffle(a + 1, a + N + 1), 1));
	fout << ("NO");
	return 0;
}
