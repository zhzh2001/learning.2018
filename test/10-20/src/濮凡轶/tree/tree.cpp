#include <cstdio>
#include <iostream>
#include <algorithm>

using namespace std;

typedef long long LL;

namespace sxdaknoip
{
	static const int maxn = 100005;

	int fa[maxn];
	int ffa[maxn];
	double f[maxn]; // ��
	double jl_ff[maxn];
	double jl_tt[maxn];
	int siz[maxn];
	double sumjlt[maxn];
	int du[maxn];

	struct Edge
	{
		int to, nxt;
	} e[maxn << 1];

	int first[maxn];

	inline void add_edge(int from, int to)
	{
		static int cnt = 0;
		e[++cnt].nxt = first[from];
		first[from] = cnt;
		e[cnt].to = to;
		e[++cnt].nxt = first[to];
		first[to] = cnt;
		e[cnt].to = from;
	}

	inline void gx(int now)
	{
		int son = 0;
		for(int i = first[now]; i; i = e[i].nxt)
		{
			int to = e[i].to;
			if(to != fa[now])
			{
				son++;
				if(sumjlt[now] > 0)
				{
					f[now] += .5 * siz[to] * (sumjlt[now] - sumjlt[to]) / sumjlt[now]; // ����to
					f[now] += f[to] * sumjlt[to] / sumjlt[now]; // ��to
				}
			}
		}
		if(son)
			f[now] /= son;
	}

	inline void dfs1(int now)
	{
		sumjlt[now] = jl_tt[now];
		siz[now] = 1;
		for(int i = first[now]; i; i = e[i].nxt)
		{
			int to = e[i].to;
			if(to != fa[now])
			{
				ffa[to] = fa[to] = now;
				dfs1(to);
				sumjlt[now] += sumjlt[to];
				siz[now] += siz[to];
			}
		}
		gx(now);
	}

	double ans = 0.;

	inline void dfs2(int now)
	{
		double fnow = f[now];
		int siznow = siz[now];
		double sumjltnow = sumjlt[now];
		fa[now] = 0;
		for(int i = first[now]; i; i = e[i].nxt)
		{
			int to = e[i].to;
			if(to != fa[now])
			{
				siz[now] += siz[to];
				sumjlt[now] += sumjlt[to];
			}
		}
		gx(now);
		ans += f[now] * jl_ff[now];
		for(int i = first[now]; i; i = e[i].nxt)
		{
			int to = e[i].to;
			if(to != ffa[now])
			{
				fa[now] = to;
				sumjlt[now] -= sumjlt[to];
				siz[now] -= siz[to];
				gx(now);
				dfs2(to);
			}
			fa[now] = ffa[now];
			f[now] = fnow;
			siz[now] = siznow;
			sumjlt[now] = sumjltnow;
		}
	}

	int main()
	{
		int n;
		scanf("%d", &n);
		for(int i = 1, F, T; i < n; ++i)
		{
			scanf("%d%d", &F, &T);
			add_edge(F, T);
		}
		for(int i = 1; i <= n; ++i)
			scanf("%lf%lf", &jl_ff[i], &jl_tt[i]);
		dfs1(1);
//		for(int i = 1; i <= n; ++i)
//			cout << f[i] << ' ';
		dfs2(1);
//		cout << endl;
		printf("%.16f", ans * (n + 1));
		return 0;
	}
}

int main()
{
	freopen("tree.in", "r", stdin);
	freopen("tree.out", "w", stdout);
	sxdaknoip::main();
	fclose(stdin);
	fclose(stdout);
	return 0;
}
