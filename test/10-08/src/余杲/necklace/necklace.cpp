#include<bits/stdc++.h>
using namespace std;
#define gc c=getchar()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
const int MAXN=50005;
struct Edge{
	int x,y,w,c;
	bool operator<(const Edge &b)const{
		return w<b.w;
	}
}edge[MAXN<<1];
int f[MAXN];
bool used[MAXN<<1];
int Find(int x){
	return f[x]==x?x:f[x]=Find(f[x]);
}
int main(){
	freopen("necklace.in","r",stdin);
	freopen("necklace.out","w",stdout);
	int n=read(),m=read(),k=read();
	for(int i=1;i<=m;i++){
		int x=read()+1,y=read()+1,z=read(),c=read();
		if(x>y)swap(x,y);
		edge[i].x=x;edge[i].y=y;
		edge[i].w=z;edge[i].c=c;
	}
	sort(edge+1,edge+m+1);
	for(int i=1;i<=n;i++)f[i]=i;
	int ans=0;
	int pb;
	for(pb=1;pb<=m&&n>k+1;pb++){
		if(!edge[pb].c)continue;
		int x=Find(edge[pb].x),y=Find(edge[pb].y);
		if(x==y)continue;
		n--;
		ans+=edge[pb].w;
		f[x]=y;
	}
	pb--;
	for(int i=1;i<=m;i++){
		if(edge[i].c)continue;
		int x=Find(edge[i].x),y=Find(edge[i].y);
		if(x==y)continue;
		k--;n--;
		used[i]=1;
		ans+=edge[i].w;
		f[x]=y;
	}
	for(int i=1;i<=m&&k;i++){
		if(edge[i].c)continue;
		if(used[i])continue;
		int x=Find(edge[i].x),y=Find(edge[i].y);
		if(x^y)continue;
		n++;
		if(x==edge[i].x)f[edge[i].y]=edge[i].y;
		else f[edge[i].x]=edge[i].x;
	}
	for(int i=1;i<=pb&&k;i++){
		if(!edge[i].c)continue;
		int x=Find(edge[i].x),y=Find(edge[i].y);
		if(x^y)ans-=edge[i].w;
	}
	for(int i=1;i<=m&&k;i++){
		if(edge[i].c)continue;
		int x=Find(edge[i].x),y=Find(edge[i].y);
		if(x==y)continue;
		k--;n--;
		used[i]=1;
		ans+=edge[i].w;
		f[x]=y;
	}
	for(int i=1;i<=m&&n>1;i++){
		if(!edge[i].c)continue;
		int x=Find(edge[i].x),y=Find(edge[i].y);
		if(x==y)continue;
		n--;
		ans+=edge[i].w;
		f[x]=y;
	}
	printf("%d",ans);
}
