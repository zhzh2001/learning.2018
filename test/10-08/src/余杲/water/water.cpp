#include<bits/stdc++.h>
using namespace std;
#define gc c=getchar()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
double d[5005],a[5005][5005];
long long x[5005],y[5005];
bool vis[5005];
long long sqr(long long x){
	return x*x;
}
int main(){
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
	int n=read();
	for(int i=1;i<=n+1;i++)
		for(int j=1;j<=n+1;j++)
			a[i][j]=1e18;
	for(int i=1;i<=n;i++)a[n+1][i]=a[i][n+1]=read();
	for(int i=1;i<=n;i++)x[i]=read(),y[i]=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			a[i][j]=sqrt(sqr(x[i]-x[j])+sqr(y[i]-y[j]));
	for(int i=0;i<=n+1;i++)d[i]=1e18;
	d[n+1]=0;
	double ans=0;
	for(int i=1;i<=n+1;i++){
		int k=0;
		for(int j=1;j<=n+1;j++)
			if(d[j]<d[k]&&!vis[j])k=j;
		vis[k]=1;
		ans+=d[k];
		for(int j=1;j<=n+1;j++)
			if(a[k][j]<d[j]&&!vis[j])d[j]=a[k][j];
	}
	printf("%.2lf",ans);
}
