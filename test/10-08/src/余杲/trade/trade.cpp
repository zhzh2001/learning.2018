#pragma GCC optimize(2)
#include<cstdio>
#include<vector>
#include<iostream>
#include<algorithm>
using namespace std;
#define gc c=getchar()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
const int MAXN=40005;
int nedge,head[MAXN];
struct Edge{
	int to,nxt;
}edge[MAXN<<1];
void add(int x,int y){
	edge[++nedge].to=y;
	edge[nedge].nxt=head[x];
	head[x]=nedge;
}
struct Path{
	int x,y,w;
}a[MAXN];
vector<int>v;
bool vis[MAXN];
int fa[MAXN],d[MAXN];
void dfs(int k,int f){
	fa[k]=f;
	d[k]=d[f]+1;
	for(int i=head[k];i;i=edge[i].nxt){
		int u=edge[i].to;
		if(u==f)continue;
		dfs(u,k);
	}
}
int main(){
	freopen("trade.in","r",stdin);
	freopen("trade.out","w",stdout);
	int n=read(),p=read(),q=read();
	for(int i=1;i<n;i++){
		int x=read(),y=read();
		add(x,y),add(y,x);
	}
	dfs(1,0);
	for(int i=1;i<=p;i++)a[i].x=read(),a[i].y=read(),a[i].w=read();
	while(q--){
		int x=read(),y=read(),k=read();
		for(int i=1;i<=n;i++)vis[i]=0;
		while(x!=y){
			if(d[x]<d[y])swap(x,y);
			vis[x]=1;
			x=fa[x];
		}
		vis[x]=1;
		v.clear();
		for(int i=1;i<=p;i++)
			if(vis[a[i].x]&&vis[a[i].y])v.push_back(a[i].w);
		sort(v.begin(),v.end());
		printf("%d\n",v[k-1]);
	}
}
