#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 50005
#define M 100005
#define inf (1e9)
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
struct edge{ ll u,v,w,c; }e[M];
ll n,m,k,ans=inf,num,cnt,tmp,fa[N];
bool cmp(edge x,edge y){
	if (x.w==y.w) return x.c<y.c;
	return x.w<y.w;
}
ll find(ll x) { return fa[x]==x?x:fa[x]=find(fa[x]); }
void solve(ll t){
	rep(i,1,m) if (!e[i].c) e[i].w+=t; cnt=tmp=num=0;
	rep(i,0,n) fa[i]=i; sort(e+1,e+1+m,cmp);
	rep(i,1,m){
		ll p=find(e[i].u),q=find(e[i].v);
		if (p!=q) {
			fa[p]=q; tmp+=e[i].w; ++num;
			if (!e[i].c) tmp-=t,cnt++;
		}
		if (num==n-1) break;
	}
	rep(i,1,m) if (!e[i].c) e[i].w-=t;
}
int main(){
	freopen("necklace.in","r",stdin);
	freopen("necklace.out","w",stdout);
	n=read(); m=read(); k=read();
	rep(i,1,m){
		e[i].u=read(); e[i].v=read();
		e[i].w=read(); e[i].c=read();
	}
	ll l=-200,r=200;
	while (l<=r){
		ll mid=l+r>>1; solve(mid);
		if (cnt>=k) l=mid+1,ans=min(ans,tmp);
		else r=mid-1;
	}
	printf("%d",ans);
}
