#include<cmath>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 5005
#define inf (1e10)
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
struct data{ ll x,y,w; }a[N];
ll n,flag[N]; double ans,dis[N];
bool cmp(data x,data y) { return x.w<y.w; }
double calc(ll x,ll y){ return sqrt((a[x].x-a[y].x)*(a[x].x-a[y].x)+(a[x].y-a[y].y)*(a[x].y-a[y].y)); }
int main(){
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
	n=read();
	rep(i,1,n) dis[i]=a[i].w=read();
	rep(i,1,n){
		a[i].x=read();
		a[i].y=read();
	}
	dis[0]=inf;
	rep(i,1,n){
		ll pos=0;
		rep(j,1,n) if (!flag[j]&&(dis[j]<dis[pos]||dis[j]==dis[pos]&&a[j].w<a[pos].w)) pos=j;
		flag[pos]=1;
		rep(j,1,n) if (!flag[j]&&dis[j]>calc(pos,j)) dis[j]=calc(pos,j);
	}
	rep(i,1,n) ans+=dis[i];
	printf("%.2f",ans);
}
