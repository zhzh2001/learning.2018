#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-') f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 5005;
int x[N],y[N];
int n;
double dist[N],ans;
bool vis[N];

inline double dis(int a,int b){
	double sum = sqrt(1ll*(x[a]-x[b])*(x[a]-x[b])+1LL*(y[a]-y[b])*(y[a]-y[b]));
	return sum;
}

signed main(){
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
	n = read();
	for(int i = 1;i <= n;++i) dist[i] = read();
	for(int i = 1;i <= n;++i) x[i] = read(),y[i] = read();
	for(int i = 1;i <= n;++i){
		int a = 0;double Mi = 1e12;
		for(int j = 1;j <= n;++j)
			if(!vis[j] && dist[j] < Mi){
				Mi = dist[j];
				a = j;
			}
		ans += dist[a];
		vis[a] = true;
		// printf("@ %d %.2lf\n", a,dist[a]);
		for(int j = 1;j <= n;++j)
			if(!vis[j]){
				// if(dist[a] + dis(a,j) < dist[j]){
				// 	dist[j] = dist[a] + dis(a,j);
				// 	printf("# %d %.2lf\n",j,dist[j]);
				// }
				dist[j] = min(dist[j],dis(a,j));
			}
	}
	printf("%.2lf\n",ans);
	return 0;
}