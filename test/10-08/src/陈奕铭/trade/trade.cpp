#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-') f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 40005;
int n,p,q;
int head[N],nxt[N*2],to[N*2],cnt;
struct node{
	int a,b,c;
	friend bool operator <(node a,node b){
		return a.c < b.c;
	}
}poc[N],mon[N];
int ans[N];
int son[N],dfn[N],DFN,f[N],size[N],top[N],dep[N];
vector<int> Seg[N<<2];
int sum[N],k[N];
int now;

inline void insert(int x,int y){
	to[++cnt] = y; nxt[cnt] = head[x]; head[x] = cnt;
}

void dfs1(int x,int fa){
	size[x] = 1;
	for(int i = head[x];i;i = nxt[i])
		if(to[i] != fa){
			f[to[i]] = x;
			dep[to[i]] = dep[x]+1;
			dfs1(to[i],x);
			size[x] += size[to[i]];
			if(size[to[i]] > size[son[x]])
				son[x] = to[i];
		}
}

void dfs2(int x,int fa){
	dfn[x] = ++DFN; top[x] = fa;
	if(son[x]) dfs2(son[x],fa);
	for(int i = head[x];i;i = nxt[i])
		if(to[i] != son[x] && to[i] != f[x])
			dfs2(to[i],to[i]);
}

void add(int num,int l,int r,int x,int y){
	Seg[num].push_back(y);
	if(l == r) return;
	int mid = (l+r)>>1;
	if(x <= mid) add(num<<1,l,mid,x,y);
	else add(num<<1|1,mid+1,r,x,y);
}

void Query(int num,int l,int r,int x,int y){
	if(x <= l && r <= y){
		for(register int i = 0;i < Seg[num].size();++i){
			int j = Seg[num][i];
			++sum[j];
			if(sum[j] == 2){
				++k[j];
				if(k[j] == mon[j].c) ans[j] = poc[now].c;
			}
		}
		return;
	}
	int mid = (l+r)>>1;
	if(x <= mid) Query(num<<1,l,mid,x,y);
	if(y > mid) Query(num<<1|1,mid+1,r,x,y);
}

inline void getlca(int x,int y){
	while(top[x] != top[y]){
		if(dep[top[x]] < dep[top[y]]) swap(x,y);
		Query(1,1,n,dfn[top[x]],dfn[x]);
		x = f[top[x]];
	}
	if(dep[x] < dep[y]) swap(x,y);
	Query(1,1,n,dfn[y],dfn[x]);
}

signed main(){
	n = read(); p = read(); q = read();
	for(register int i = 1;i < n;++i){
		int x = read(),y = read();
		insert(x,y);
		insert(y,x);
	}
	for(register int  i = 1;i <= p;++i){
		poc[i].a = read(); poc[i].b = read(); poc[i].c = read();
	}
	for(register int i = 1;i <= q;++i){
		mon[i].a = read(); mon[i].b = read(); mon[i].c = read();
	}
	sort(poc+1,poc+p+1);
	dfs1(1,1);
	dfs2(1,1);
	for(register int i = 1;i <= q;++i){
		add(1,1,n,dfn[mon[i].a],i);
		add(1,1,n,dfn[mon[i].b],i);
	}
	for(register int i = 1;i <= p;++i){
		memset(sum,0,sizeof sum);
		now = i;
		getlca(poc[i].a,poc[i].b);
	}
	for(register int i = 1;i <= q;++i) printf("%d\n", ans[i]);
	return 0;
}
