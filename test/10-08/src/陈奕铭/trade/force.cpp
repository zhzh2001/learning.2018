#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-') f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 40005;

map<pair<int,int>,int> mp;
vector<int> st[N];
int n,p,q,Cnt;
bool vis[N];
int sum[N],ans[N];
int head[N],nxt[N*2],to[N*2],cnt;
vector<int> pp[N];
int Seg[N<<2];
struct node{
	int a,b,c;
	friend bool operator <(node a,node b){
		return a.c < b.c;
	}
}poc[N],mon[N];

inline void insert(int x,int y){
	to[++cnt] = y; nxt[cnt] = head[x]; head[x] = cnt;
}

void add(int num,int l,int r,int x,int k){
	Seg[num] += k;
	if(l == r) return;
	int mid = (l+r)>>1;
	if(x <= mid) add(num<<1,l,mid,x,k);
	else add(num<<1|1,mid+1,r,x,k);
}

int Query(int num,int l,int r,int k){
	if(l == r) return l;
	int mid = (l+r)>>1;
	if(Seg[num<<1] >= k) return Query(num<<1,l,mid,k);
	else return Query(num<<1|1,mid+1,r,k-Seg[num<<1]);
}

void dfs(int x,int y,int fax,int fay){
	if(mp[make_pair(x,y)] != 0){
		int pos = mp[make_pair(x,y)];
		for(int i = 0;i < st[pos].size();++i)
			ans[st[pos][i]] = Query(1,1,n,mon[st[pos][i]].c);
	}
	for(int i = head[x];i;i = nxt[i])
		if(to[i] != fax){
			if(!vis[to[i]]){
				vis[to[i]] = true;
				for(int z = 0;z < pp[to[i]].size();++z){
					int j = pp[to[i]][z];
					++sum[j];
					if(sum[j] == 2){
						add(1,1,n,poc[j].c,1);
					}
				}
				if(to[i] < y) 	dfs(to[i],y,x,fay);
				else dfs(y,to[i],fay,x);
				vis[to[i]] = false;
				for(int z = 0;z < pp[to[i]].size();++z){
					int j = pp[to[i]][z];
					--sum[j];
					if(sum[j] == 1){
						add(1,1,n,poc[j].c,-1);
					}
				}
			}else{				
				vis[x] = false;
				for(int z = 0;z < pp[x].size();++z){
					int j = pp[x][z];
					--sum[j];
					if(sum[j] == 1){
						add(1,1,n,poc[j].c,-1);
					}
				}
				if(to[i] < y) 	dfs(to[i],y,x,fay);
				else dfs(y,to[i],fay,x);
				vis[x] = true;
				for(int z = 0;z < pp[x].size();++z){
					int j = pp[x][z];
					++sum[j];
					if(sum[j] == 2){
						add(1,1,n,poc[j].c,1);
					}
				}
			}
		}
	for(int i = head[y];i;i = nxt[i])
		if(to[i] != fay){
			if(!vis[to[i]]){
				vis[to[i]] = true;
				for(int z = 0;z < pp[to[i]].size();++z){
					int j = pp[to[i]][z];
					++sum[j];
					if(sum[j] == 2){
						add(1,1,n,poc[j].c,1);
					}
				}
				if(x < to[i])	dfs(x,to[i],fax,y);
				else dfs(to[i],x,y,fax);
				vis[to[i]] = false;
				for(int z = 0;z < pp[to[i]].size();++z){
					int j = pp[to[i]][z];
					--sum[j];
					if(sum[j] == 1){
						add(1,1,n,poc[j].c,-1);
					}
				}
			}else{		
				vis[y] = false;
				for(int z = 0;z < pp[y].size();++z){
					int j = pp[y][z];
					--sum[j];
					if(sum[j] == 1){
						add(1,1,n,poc[j].c,-1);
					}
				}
				if(x < to[i])	dfs(x,to[i],fax,y);
				else dfs(to[i],x,y,fax);
				vis[y] = true;
				for(int z = 0;z < pp[y].size();++z){
					int j = pp[y][z];
					++sum[j];
					if(sum[j] == 2){
						add(1,1,n,poc[j].c,1);
					}
				}
			}
		}
}

signed main(){
	n = read(); p = read(); q = read();
	for(register int i = 1;i < n;++i){
		int x = read(),y = read();
		insert(x,y);
		insert(y,x);
	}
	for(register int  i = 1;i <= p;++i){
		poc[i].a = read(); poc[i].b = read(); poc[i].c = read();
		pp[poc[i].a].push_back(i);
		pp[poc[i].b].push_back(i);
	}
	for(register int i = 1;i <= q;++i){
		mon[i].a = read(); mon[i].b = read(); mon[i].c = read();
		int x = mon[i].a,y = mon[i].b;
		if(x > y) swap(x,y);
		pair<int,int> a = make_pair(x,y);
		if(mp[a] == 0) mp[a] = ++Cnt;
		st[mp[a]].push_back(i);
	}
	vis[1] = true;
	for(int i = 0;i < pp[1].size();++i){
		int j = pp[1][i];
		++sum[j];
	}
	dfs(1,1,1,1);
	for(int i = 1;i <= q;++i) printf("%d\n", ans[i]);
	return 0;
}
