#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-') f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 50005,M = 100005;
int n,k,m;
int f[N];
int ans,edg;
int sum0;
struct node{
	int a,b,w,c;
}e[M];

inline bool cmp1(node a,node b){
	return a.w < b.w;
}
inline bool cmp2(node a,node b){
	if(a.c == b.c) return a.w < b.w;
	return a.c < b.c;
}

int getf(int x){
	if(x == f[x]) return x;
	return f[x] = getf(f[x]);
}

signed main(){
	freopen("necklace.in","r",stdin);
	freopen("necklace.out","w",stdout);
	n = read(); m = read(); k = read();
	for(int i = 1;i <= m;++i){
		e[i].a = read(); e[i].b = read(); e[i].w = read(); e[i].c = read();
	}
	sort(e+1,e+m+1,cmp1);
	for(int i = 0;i < n;++i) f[i] = i;
	for(int i = 1;i <= m;++i){
		if(sum0 == k && e[i].c == 0) continue;
		int fa = getf(e[i].a),fb = getf(e[i].b);
		if(fa == fb) continue;
		ans += e[i].w;
		f[fa] = fb;
		++edg;
		if(edg == n-1) break;
	}
	if(sum0 == k){
		printf("%d\n", ans);
		return 0;
	}
	sum0 = ans = edg = 0;
	for(int i = 0;i < n;++i) f[i] = i;
	sort(e+1,e+m+1,cmp2);
	for(int i = 1;i <= m;++i){
		if(sum0 == k && e[i].c == 0) continue;
		int fa = getf(e[i].a),fb = getf(e[i].b);
		if(fa == fb) continue;
		ans += e[i].w;
		f[fa] = fb;
		++edg;
		if(edg == n-1) break;
	}
	printf("%d\n",ans
		);
	return 0;
}