#include<cstdio>
#include<algorithm>
using namespace std;
int n,m,k,l,x,nu,fa[51000],us[51000],a[110000],b[110000],c[110000],na[110000],nb[110000],nc[110000],d[110000];
long long s;
inline int read(){
	int ret=0,p=1;
	char c=getchar();
	while((c>'9')||(c<'0')){
		if(c=='-')p=-1;
		c=getchar();
	}
	while((c>='0')&&(c<='9')){
		ret=(ret<<1)+(ret<<3)+c-'0';
		c=getchar();
	}
	return ret*p;
}
void sor(int l,int r){
	int i=l,j=r;
	long long x=c[(l+r)>>1];
	while(i<=j){
		while(c[i]<x)i++;
		while(x<c[j])j--;
		if(i<=j){
			swap(a[i],a[j]);
			swap(b[i],b[j]);
			swap(c[i],c[j]);
			swap(d[i],d[j]);
			i++;
			j--;
		}
	}
	if(l<j)sor(l,j);
	if(i<r)sor(i,r);
}
int getf(int x){
	if(x==fa[x])return x;
	fa[x]=getf(fa[x]);
	return fa[x];
}
int main(){
	freopen("necklace.in","r",stdin);
	freopen("necklace.out","w",stdout);
	n=read();
	m=read();
	k=read();
	k=n-k;
	for(int i=1;i<=m;i++){
		a[i]=read();
		b[i]=read();
		c[i]=read();
		d[i]=read();
	}
	for(int i=1;i<=n;i++)
		fa[i]=i;
	sor(1,m);
	l=1;
	s=0;
	while(n>1){
		while((d[l]==1||getf(a[l])==getf(b[l]))&&l<=m)l++;
		if(l>m)break;
		fa[getf(a[l])]=getf(b[l]);
		nu++;
		na[nu]=a[l];
		nb[nu]=b[l];
		nc[nu]=c[l];
		s+=c[l];
		l++;
		n--;
	}
	l=1;
	while(n>1||k>1){
		while((d[l]==0||(getf(a[l])==getf(b[l])&&k<=n))&&l<=m)l++;
		if(getf(a[l])==getf(b[l])){
			x=0;
			for(int i=1;i<=nu;i++)
				if(us[i]==0&&(getf(na[i])==getf(a[l])&&getf(nb[i])==getf(b[l]))||(getf(na[i])==getf(b[l])&&getf(nb[i])==getf(a[l])))x=max(x,nc[i]);
			s-=x;
			s+=c[l];
			k--;
		}
		else{
			fa[getf(a[l])]=getf(b[l]);
			s+=c[l];
			l++;
			n--;
			k--;
		}
	}
	printf("%lld",s);
	return 0;
}
