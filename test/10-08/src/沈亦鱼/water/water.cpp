#include<cmath>
#include<cstdio>
using namespace std;
int n,j,x[5100],y[5100],us[5100];
double lx,s,w[5100];
inline int read(){
	int ret=0,p=1;
	char c=getchar();
	while((c>'9')||(c<'0')){
		if(c=='-')p=-1;
		c=getchar();
	}
	while((c>='0')&&(c<='9')){
		ret=(ret<<1)+(ret<<3)+c-'0';
		c=getchar();
	}
	return ret*p;
}
int main(){
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
	n=read();
	s=0;
	for(int i=1;i<=n;i++)
		w[i]=read();
	for(int i=1;i<=n;i++){
		x[i]=read();
		y[i]=read();
	}
	s=0;
	while(true){
		j=0;
		for(int i=1;i<=n;i++)
			if((us[i]==0)&&((j==0)||(w[j]>w[i])))j=i;
		if(!j)break;
		us[j]=1;
		s+=w[j];
//		printf("%d %d %d %0.2lf\n",j,x[j],y[j],w[j]);
		for(int i=1;i<=n;i++){
			lx=(long long)(x[i]-x[j])*(x[i]-x[j]);
			lx+=(long long)(y[i]-y[j])*(y[i]-y[j]);
			lx=sqrt(lx);
//			if(i==5)printf("%lf %lf\n",lx,w[i]);
			if(lx<w[i])w[i]=lx;
		}
	}
	printf("%0.2lf",s);
	return 0;
}
