#include<cstdio>
#include<algorithm>
using namespace std;
int n,pp,q,tim,tot,totq,k,lu,lv,j,vis[41000],fath[41000],a[41000],b[41000],c[41000],u[41000],v[41000],w[41000],bf[41000],he[41000],heq[41000],p[81000],pq[81000],ne[81000],neq[81000],idq[81000],ru[41000],chu[41000];
inline int read(){
	int ret=0,p=1;
	char c=getchar();
	while((c>'9')||(c<'0')){
		if(c=='-')p=-1;
		c=getchar();
	}
	while((c>='0')&&(c<='9')){
		ret=(ret<<1)+(ret<<3)+c-'0';
		c=getchar();
	}
	return ret*p;
}
void adg(int u,int v){
	tot++;
	p[tot]=v;
	ne[tot]=he[u];
	he[u]=tot;
}
void dfs(int k,int fa){
	tim++;
	ru[k]=tim;
	for(int i=he[k];i;i=ne[i])
		if(p[i]!=fa)dfs(p[i],k);
	tim++;
	chu[k]=tim;
}
void sor(int l,int r){
	int i=l,j=r,x=c[(l+r)>>1];
	while(i<=j){
		while(c[i]<x)i++;
		while(x<c[j])j--;
		if(i<=j){
			swap(a[i],a[j]);
			swap(b[i],b[j]);
			swap(c[i],c[j]);
			i++;
			j--;
		}
	}
	if(l<j)sor(l,j);
	if(i<r)sor(i,r);
}
bool bh(int u,int v){
	if(ru[u]<=ru[v]&&chu[v]<=chu[u])return true;
	else return false;
}
void adgq(int u,int v,int w){
	totq++;
	pq[totq]=v;
	idq[totq]=w;
	neq[totq]=heq[u];
	heq[u]=totq;
}
int getf(int x){
	if(x==fath[x])return x;
	fath[x]=getf(fath[x]);
	return fath[x];
}
void lca(int u,int fa){
	for(int e=he[u];e;e=ne[e]){
		int v=p[e];
		if(v!=fa){
			lca(v,u);
			fath[v]=u;
		}
	}
	vis[u]=1;//printf("%d %d\n",u,heq[u]);
	for(int e=heq[u];e;e=neq[e]){
		int v=pq[e];//printf("%d %d %d %d\n",u,v,vis[v],getf(v));
		if(vis[v])bf[idq[e]]=getf(v);
	}
}
int main(){
	freopen("trade.in","r",stdin);
	freopen("trade.out","w",stdout);
	n=read();
	pp=read();
	q=read();
	for(int i=1;i<n;i++){
		lu=read();
		lv=read();
		adg(lu,lv);
		adg(lv,lu);
		fath[i]=i;
	}
	fath[n]=n;
	tim=0;
	dfs(1,1);
	for(int i=1;i<=pp;i++){
		a[i]=read();
		b[i]=read();
		if(bh(a[i],b[i]))swap(a[i],b[i]);
		c[i]=read();
	}
	sor(1,pp);
	for(int i=1;i<=q;i++){
		u[i]=read();
		v[i]=read();
		if(bh(v[i],u[i]))swap(u[i],v[i]);
		adgq(u[i],v[i],i);
		adgq(v[i],u[i],i);
		w[i]=read();
	}
	lca(1,1);
	for(int i=1;i<=q;i++){
		k=w[i];//printf("%d %d\n",a[3],bh(a[3],u[i]));
//		printf("%d !\n",w[i]);
		if(bh(u[i],v[i])){
			for(j=1;k&&j<=pp;j++){
				if(bh(b[j],a[j])&&bh(u[i],b[j])&&bh(a[j],v[i]))k--;//printf("%d ",k);
			}
		}
		else{
			for(j=1;k&&j<=pp;j++){
				if(bh(bf[i],a[j])&&bh(bf[i],b[j])&&((bh(a[j],u[i])||bh(a[j],v[i]))&&(bh(b[j],u[i])||bh(b[j],v[i]))))k--;//printf("%d %d ",k,j);
			}
		}
		printf("%d\n",c[j-1]);
	}
	return 0;
}/*
6 5 8
6 1
6 2
6 3
2 5
3 4
3 1 8
4 2 6
6 3 9
3 2 9
5 3 10
3 2 2
3 6 1
1 4 2
3 5 3
1 4 1
3 5 1
4 6 1
4 2 1
*/
