#pragma GCC optimize 2
#include <bits/stdc++.h>
#define N 5020
#define ll long long
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}
// int __a;

int x[N], y[N], n;
double w[N];

inline double dist(int i, int j) {
  return sqrt((ll) (x[i] - x[j]) * (x[i] - x[j])
            + (ll) (y[i] - y[j]) * (y[i] - y[j]));
}

int cnt;

struct edge {
  int x, y;
  double len;
} e[N*N/2],ass[N];
// int __b;

inline bool cmp(const edge &a, const edge &b) {
  return a.len < b.len;
}

struct bcj {
  int fa[N];
  int find(int x) {
    return fa[x] == x ? x : fa[x] = find(fa[x]);
  }
  void merge(int x, int y) {
    fa[find(x)] = find(y);
  }
  void init(int n) {
    for (int i = 1; i <= n; ++ i) {
      fa[i] = i;
    }
  }
}b1, b2;

int to[N<<1], nxt[N<<1], head[N], _cnt;
double val[N<<1];
inline void insert(int x, int y, double len) {
  printf("inserted %d %d %.3lf\n", x, y, len);
  to[++ _cnt] = y; nxt[_cnt] = head[x];
  head[x] = _cnt;  val[_cnt] = len;
  to[++ _cnt] = x; nxt[_cnt] = head[y];
  head[y] = _cnt;  val[_cnt] = len;
}

double dfs(int x, int f) {
  double ans = 0;
  for (int i = head[x]; i; i = nxt[i]) {
    if (to[i] == f) continue;
    ans += dfs(to[i], x);
    if (val[i] > w[to[i]]) {
      printf("%d is worth choose\n", to[i]);
    }
    ans += min(val[i], (double) w[to[i]]);
  }
  return ans;
}

double tmp[N];
bool isroot[N];

int main(int argc, char const *argv[]) {
  freopen("water.in", "r", stdin);
  freopen("water.out", "w", stdout);
  // printf("%.3lf\n", 1.0*(&__a-&__b)/1024/1024);

  n = read();
  b1.init(n);
  b2.init(n);
  for (int i = 1; i <= n; ++ i) {
    w[i] = read();
  }
  for (int i = 1; i <= n; ++ i) {
    x[i] = read();
    y[i] = read();
  }

  for (int i = 1; i <= n; ++ i) {
    for (int j = i + 1; j <= n; ++ j) {
      e[++ cnt].x = i;
      e[cnt].y = j;
      e[cnt].len = dist(i, j);
    }
  }

  sort(e + 1, e + cnt + 1, cmp);

  int tot = 1, ant = 0;
  for (int i = 1; i <= cnt; ++ i) {
    int x = e[i].x, y = e[i].y;
    double len = e[i].len;
    int fx = b1.find(x), fy = b1.find(y);
    if (fx == fy) continue;
    b1.fa[fx] = fy;

    if (len < w[x] && len < w[y]) {
      b2.merge(x, y);
    }
    ass[++ ant] = e[i];

    if (++ tot == n) break;
  }

  double ans = 0;

  for (int i = 1; i <= n; ++ i) {
    tmp[i] = 1e60;
  }
  int size = 0;
  for (int i = 1; i <= n; ++ i) {
    int f = b2.find(i);
    tmp[f] = min(tmp[f], w[i]);
    printf("%d <- %d\n", f, i);
    printf("%.3lf %.3lf\n", tmp[f], w[i]);
    isroot[i] = f == i;
  }

  for (int i = 1; i <= size; ++ i)
    printf("tmp = %.2lf\n", tmp[i]);

  for (int i = 1; i <= ant; ++ i) {
    int x = b2.find(ass[i].x), y = b2.find(ass[i].y);
    if (x == y) {
      printf("%d %d %.2lf\n", x, y, ass[i].len);
      ans += ass[i].len;
    } else {
      tmp[x] = min(tmp[x], ass[i].len);
      tmp[y] = min(tmp[y], ass[i].len);
    }
  }

  for (int i = 1; i <= n; ++ i) {
    if (isroot[i]) {
      printf("%.2lf\n", tmp[i]);
      ans += tmp[i];
    }
  }

  printf("%.2lf\n", ans);

  return 0;
}