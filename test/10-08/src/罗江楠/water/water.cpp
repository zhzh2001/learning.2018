#pragma GCC optimize 2
#include <bits/stdc++.h>
#define N 100020
#define ll long long
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}

int w[N], x[N], y[N];
int need[N];
double cost[N];
bool sol[N];

inline double dist(int i, int j) {
  return sqrt((ll) (x[i] - x[j]) * (x[i] - x[j])
            + (ll) (y[i] - y[j]) * (y[i] - y[j]));
}

int main(int argc, char const *argv[]) {
  freopen("water.in", "r", stdin);
  freopen("water.out", "w", stdout);

  int n = read();
  for (int i = 1; i <= n; ++ i) {
    w[i] = read();
  }
  for (int i = 1; i <= n; ++ i) {
    x[i] = read();
    y[i] = read();
  }

  for (int i = 1; i <= n; ++ i) {
    int minp = i;
    double minc = w[i];

    for (int j = 1; j <= n; ++ j) {
      if (j == i) continue;
      double cst = dist(i, j);
      if (cst < minc) {
        minc = cst;
        minp = j;
      }
    }

    need[i] = minp;
    cost[i] = minc;
    // printf("%d %.2lf\n", need[i], cost[i]);
  }

  double ans = 0;
  for (int i = 1; i <= n; ++ i) {
    if (i == need[i]) {
      sol[i] = true;
      ans += w[i];
    } else {
      if (i == need[need[i]]) {
        if (w[need[i]] < w[i]) {
          sol[need[i]] = true;
          ans += w[need[i]];
        } else {
          sol[i] = true;
          ans += w[i];
        }
      }
    }
  }

  for (int i = 1; i <= n; ++ i) {
    if (!sol[i]) {
      ans += cost[i];
    }
  }

  printf("%.2lf\n", ans);

  return 0;
}