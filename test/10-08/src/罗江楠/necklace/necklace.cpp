#pragma GCC optimize 2
#include <bits/stdc++.h>
#define N 100020
#define ll long long
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}
struct node {
  int x, y, c, t;
  friend bool operator < (const node &a, const node &b){
    return a.c < b.c;
  }
}a[N];
int vis[N], fa[N], tot;
int find(int x) {
  return fa[x] == x ? x : fa[x] = find(fa[x]);
}
int main(int argc, char const *argv[]) {
  freopen("necklace.in", "r", stdin);
  freopen("necklace.out", "w", stdout);

  int n = read(), m = read(), k = read();

  for(int i = 1; i <= m; ++ i) {
    a[i].x = read(); a[i].y = read();
    a[i].c = read(); a[i].t = read();
  }

  int ans = 0;

  for(int i = 1; i <= n; ++ i) {
    fa[i] = i;
  }

  sort(a + 1, a + m + 1);

  for(int i = 1; i <= m; i++){
    if (a[i].t) continue;
    vis[i] = 1;
    int x = find(a[i].x), y = find(a[i].y);
    if (x == y) continue;
    fa[x] = y;
    ans += a[i].c; tot++;
    if (!-- k) break;
  }

  for (int i = 1; i <= m; i++) {
    if (!vis[i]) {
      int x = find(a[i].x), y = find(a[i].y);
      if (x == y) continue;
      fa[x] = y;
      ans += a[i].c;
      if (++ tot == n - 1) break;
    }
  }

  printf("%d\n", ans);

  return 0;
}