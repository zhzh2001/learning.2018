#pragma GCC optimize 2
#include <bits/stdc++.h>
#define N 40020
#define ll long long
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}

int to[N<<1], nxt[N<<1], head[N], cnt;
inline void insert(int x, int y) {
  to[++ cnt] = y; nxt[cnt] = head[x]; head[x] = cnt;
  to[++ cnt] = x; nxt[cnt] = head[y]; head[y] = cnt;
}

int fa[N][18], dep[N];

int lca(int x, int y){
  if (dep[x] < dep[y]) swap(x, y);
  int t = dep[x] - dep[y];
  for (int i = 0; i < 18; ++ i)
    if (t >> i & 1) x = fa[x][i];
  for (int i = 18-1; ~i; -- i)
    if (fa[x][i] != fa[y][i])
      x = fa[x][i], y = fa[y][i];
  return x == y ? x : fa[x][0];
}

void dfs(int x, int f) {
  fa[x][0] = f;
  dep[x] = dep[f] + 1;
  for (int i = head[x]; i; i = nxt[i]) {
    if (to[i] == f) continue;
    dfs(to[i], x);
  }
}

struct timber {
  int x, y, k;
} ps[N], qs[N];

inline bool cmp(const timber &a, const timber &b) {
  return a.k < b.k;
}

inline bool isInline(int x, int b, int t) {
  return lca(x, t) == t && lca(x, b) == x;
}

inline bool check(int p, int q) {
  int x1 = ps[p].x;
  int y1 = ps[p].y;
  int x2 = qs[q].x;
  int y2 = qs[q].y;

  int z1 = lca(x1, y1);
  int z2 = lca(x2, y2);

  return (isInline(x1, x2, z2) || isInline(x1, y2, z2)) &&
         (isInline(y1, x2, z2) || isInline(y1, y2, z2));

}

int main(int argc, char const *argv[]) {
  freopen("trade.in", "r", stdin);
  freopen("trade.out", "w", stdout);

  int n = read(), p = read(), q = read();

  for (int i = 1; i < n; ++ i) {
    insert(read(), read());
  }
  for (int i = 1; i <= p; ++ i) {
    ps[i].x = read();
    ps[i].y = read();
    ps[i].k = read();
  }
  for (int i = 1; i <= q; ++ i) {
    qs[i].x = read();
    qs[i].y = read();
    qs[i].k = read();
  }

  sort(ps + 1, ps + p + 1, cmp);

  dfs(1, 0);
  for (int i = 1; i < 18; ++ i) {
    for (int j = 1; j <= n; ++ j) {
      fa[j][i] = fa[fa[j][i - 1]][i - 1];
    }
  }

  for (int i = 1; i <= q; ++ i) {

    int tot = 0;
    for (int j = 1; j <= p; ++ j) {
      if (check(j, i) && ++ tot == qs[i].k) {
        printf("%d\n", ps[j].k);
        break;
      }
    }

  }
  
  return 0;
}