/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
#define N 50055
//#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int dde[N],n,m,ans[N],ti,p,q,dis[N],fa[N],ls,cg[N],dg[N],de[N],tot,l[N],r[N],id[N],fc[N];
//矩形加一个数，单点查第k大
struct tree1{
	int le,ri,root;
}tree[N*4];
struct t{
	int l,r,num;
}_t[N*16*16];
int _find(int x,int le,int ri,int l,int r){
	if (x==0||le<1||ri>ls||le>ri) return 0;
	if (le==l&&ri==r) return _t[x].num;
	int mid=(le+ri)>>1;
	if (r<=mid) return _find(_t[x].l,le,mid,l,r);
	if (l>mid) return _find(_t[x].r,mid+1,ri,l,r);
	return _find(_t[x].l,le,mid,l,mid)+_find(_t[x].r,mid+1,ri,mid+1,r);
}
int query(int x,int k,int p){
	if (tree[x].le==tree[x].ri){
		return (tree[x].root)?_find(tree[x].root,1,ls,1,p):0;
	}
	int mid=(tree[x].le+tree[x].ri)>>1;
	int sum=(tree[x].root)?_find(tree[x].root,1,ls,1,p):0;
	if (k<=mid) return query(x*2,k,p)+sum;
	else return query(x*2+1,k,p)+sum;
}
void _change(int x,int le,int ri,int k,int p){
	_t[x].num+=p;
	if (le==ri) return;
	int mid=(le+ri)>>1;
	if (k<=mid){
		if (_t[x].l==0) _t[x].l=++tot;
		_change(_t[x].l,le,mid,k,p);
	}
	else{
		if (_t[x].r==0) _t[x].r=++tot;
		_change(_t[x].r,mid+1,ri,k,p);
	}
}
void t_ch(int x,int l,int r,int k,int p){
	if (tree[x].le==l&&tree[x].ri==r){
		if (!tree[x].root) tree[x].root=++tot;
		_change(tree[x].root,1,ls,k,p);
		return;
	}
	int mid=(tree[x].le+tree[x].ri)>>1;
	if (r<=mid) t_ch(x*2,l,r,k,p);
	else if (l>mid) t_ch(x*2+1,l,r,k,p);
	else t_ch(x*2,l,mid,k,p),t_ch(x*2+1,mid+1,r,k,p);
}
void build(int x,int l,int r){
	tree[x].le=l;tree[x].ri=r;
	if (l==r){return;}
	int mid=(l+r)>>1;
	build(x*2,l,mid);build(x*2+1,mid+1,r);
}
int Next[N*2],head[N],to[N*2],nedge;
#define V to[i]
void add(int a,int b){Next[++nedge]=head[a];head[a]=nedge;to[nedge]=b;}
void add_ne(int a,int b){add(a,b);add(b,a);}
void dfs(int x){
	l[x]=++ti;
	go(i,x){
		if (fa[x]==V) continue;
		fa[V]=x;dis[V]=dis[x]+1;dfs(V);
	}
	r[x]=ti;
}
struct xx{
	int i,l,r,e;
}z[N*4];
bool cmp(xx a,xx b){
	return a.i<b.i;
}
void add_j(int a,int b,int c,int d,int e){
	z[++tot].i=a;z[tot].l=c;z[tot].r=d;z[tot].e=e;
	z[++tot].i=b+1;z[tot].l=c;z[tot].r=d;z[tot].e=-e;
}
void add_jz(int a,int b,int c,int d,int e){
	if (a>b||c>d) return;
	add_j(a,b,c,d,e);add_j(c,d,a,b,e);
}
bool cmp3(int a,int b){return de[a]<de[b];}
void init(){
	int c=0,d=0,e=0;
	F(i,1,p){
		cg[i]=read();dg[i]=read();de[i]=read();id[i]=i;
	}
	sort(id+1,id+p+1,cmp3);
	F(i,1,p){
		if (de[id[i]]!=de[id[i-1]]||i==1) fc[++ls]=de[id[i]];
		dde[id[i]]=ls;
	}
	F(i,1,p){
		c=cg[i];d=dg[i];e=dde[i];
		if (dis[c]>dis[d]) swap(c,d);
		if (l[c]<=l[d]&&r[c]>=r[d]){
			go(j,c){
				if (to[j]==fa[c]) continue;
				if (l[to[j]]<=l[d]&&r[to[j]]>=r[d]){
					add_jz(1,l[to[j]]-1,l[d],r[d],e);
					add_jz(r[to[j]]+1,n,l[d],r[d],e);
				}
			}
		}
		else{
			add_jz(l[c],r[c],l[d],r[d],e);
		}
	}
}
struct xx2{
	int a,b,k,i;
}c[N];
bool cmp2(xx2 a,xx2 b){return a.a<b.a;}
void change(int l,int r,int k){
	if (k<0) t_ch(1,l,r,-k,-1);
	else t_ch(1,l,r,k,1);
}
bool check(int x,int k,int p){
	return query(1,x,p)>=k;
}
int ask1(int x,int k){
	int l=1,r=ls;
	while (l<r){
		int mid=(l+r)>>1;
		if (check(x,k,mid)) r=mid;
		else l=mid+1;
	}
	return fc[l];
}
signed main(){
	freopen("trade.in","r",stdin);
	freopen("trade.out","w",stdout);
	n=read();p=read();q=read();
	F(i,1,n-1) add_ne(read(),read());
	dfs(1);init();sort(z+1,z+tot+1,cmp);
	F(i,1,q){
		c[i].i=i;c[i].a=l[read()];c[i].b=l[read()];c[i].k=read();
	}
	sort(c+1,c+q+1,cmp2);int k=1;
	build(1,1,n);tot=0;
	F(i,1,q){
		for (;z[k].i<=c[i].a;k++) change(z[k].l,z[k].r,z[k].e);
		ans[c[i].i]=ask1(c[i].b,c[i].k);
	}
	F(i,1,q) wrn(ans[i]);
	return 0;
}
