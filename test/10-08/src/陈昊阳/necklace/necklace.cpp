#include<iostream>
#include<stdio.h>
#include<cstring>
#include<string>
#include<cmath>
#include<ctime>
#include<algorithm>
#include<cstdlib>
#define rep(a,b,c) for(int a=b;a<=c;++a)
#define drp(a,b,c) for(int a=b;a>=c;--a)
#define N 50009
#define M 100009
#define LL long long
#define ULL unsigned long long
#define XJ Iloveyou
#define INF 0x3f3f3f3f
using namespace std;

int n,m,k,fa[N],sum,ans=INF,cnt,T,l,r,mid;
struct xj{
 int u,v,w,c;
}hy[M];

int read(){
int x=0,fh=1; char c=getchar();
	while(!isdigit(c)) {fh=(c=='-')?0:fh; c=getchar();}
	while(isdigit(c)) {x=(x<<3)+(x<<1)+(c^'0'); c=getchar();}
return fh?x:-x;
}

bool cmp(xj x,xj y){
int a=x.w,b=y.w;
 if(!x.c) a+=mid;
 if(!y.c) b+=mid;
 
 if(a==b) return x.c<y.c;
 else return a<b;
}

int find(int x){
 if(fa[x]!=x) fa[x]=find(fa[x]);
 return fa[x];
}

void solve(){
	  mid=(l+r)>>1;
	   sort(hy+1,hy+m+1,cmp);
	   
	   rep(i,0,n) fa[i]=i;
	   T=1; cnt=sum=0;
	   
	   rep(i,1,m){
		 if(find(hy[i].u)==find(hy[i].v)) continue;
		  ++T; if(!hy[i].c) ++cnt;
		  sum+=hy[i].w;
		  fa[find(hy[i].u)]=find(hy[i].v);
		  
		  if(T==n) break;
		}
		
		if(cnt<k) r=mid;
		 else l=mid;
		 
	if(cnt==k) ans=min(ans,sum);
}

int main(){
freopen("necklace.in","r",stdin); freopen("necklace.out","w",stdout);
	n=read(); m=read(); k=read(); T=n-1;
	 rep(i,0,n) fa[i]=i;
	 rep(i,1,m){
	  hy[i].u=read();
	  hy[i].v=read();
	  hy[i].w=read();
	  hy[i].c=read();
	 }
	 
	 l=-105; r=105;
	 
	 while(l+1<r) 
	  solve();
	 
	 cout<<ans;
return 0;
}




