#include <cstdio>
#include <algorithm>
#include <cctype>
int read(){
	register int x = 0;
	register char f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = !f;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
int n, m, k;
int l = -1001, r = 1001, mid, ans, sum;
struct edge{
	int u, v, w, c;
	bool operator < (const edge &res) const {
		return w < res.w || w == res.w && c < res.c;
	}
}a[100005];
int fa[100005];
int find(int x){
	return fa[x] == x ? x : (fa[x] = find(fa[x]));
}
int merge(int x, int y){
	int fx = find(x), fy = find(y);
	if (fx == fy) return 0;
	return fa[fy] = fx, 1;
}
int check(int x){
	int num = 0, num0 = 0; sum = 0;
	for (register int i = 1; i <= m; ++i)
		if (!a[i].c) a[i].w += x;
	std :: sort(a + 1, a + 1 + m);
	for (register int i = 1; i <= n; ++i) fa[i] = i;
	for (register int i = 1; i <= m; ++i)
		if (merge(a[i].u, a[i].v)){
			sum += a[i].w, ++num;
			if (!a[i].c) ++num0;
			if (num == n - 1) break;
		}
	for (register int i = 1; i <= m; ++i)
		if (!a[i].c) a[i].w -= x;
	return num0 >= k;
}
int main(){
	freopen("necklace.in", "r", stdin);
	freopen("necklace.out", "w", stdout);
	n = read(), m = read(), k = read();
	for (register int i = 1; i <= m; ++i)
		a[i].u = read() + 1, a[i].v = read() + 1, a[i].w = read(), a[i].c = read();
	while (l <= r)
		if (check(mid = l + r >> 1)) l = mid + 1, ans = sum - mid * k;
		else r = mid - 1;
	printf("%d", ans);
}