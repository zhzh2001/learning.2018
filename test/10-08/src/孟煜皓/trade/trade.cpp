#include <cstdio>
#include <cctype>
#include <algorithm>
int read(){
	register int x = 0;
	register char f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = !f;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
#define N 100005
int n, m, q;
struct path{
	int x, y, w;
	bool operator < (const path &res) const {
		return w < res.w;
	}
}a[N];
int edge, to[N], pr[N], hd[N];
int fa[N], b1[N], b2[N];
void addedge(int u, int v){
	to[++edge] = v, pr[edge] = hd[u], hd[u] = edge;
}
void dfs(int u){
	for (register int i = hd[u]; i; i = pr[i])
		if (to[i] != fa[u]) fa[to[i]] = u, dfs(to[i]);
}
int query(int u, int v, int k){
	int lca = 0;
	for (register int i = u; i; i = fa[i]) b1[i] = 1;
	for (register int i = v; i; i = fa[i])
		if (b1[i]) { lca = i; break; } else b2[i] = 1;
	for (register int i = fa[lca]; i; i = fa[i]) b1[i] = 0;
	register int cnt = 0, ans = 0;
	for (register int i = 1; i <= m; ++i)
		if ((b1[a[i].x] || b2[a[i].x]) && (b1[a[i].y] || b2[a[i].y])){
			++cnt;
			if (cnt == k) { ans = a[i].w; break; }
		}
	for (register int i = u; i != lca; i = fa[i]) b1[i] = 0;
	for (register int i = v; i != lca; i = fa[i]) b2[i] = 0;
	b1[lca] = b2[lca] = 0;
	return ans;
}
int main(){
	freopen("trade.in", "r", stdin);
	freopen("trade.out", "w", stdout);
	n = read(), m = read(), q = read();
	for (register int i = 1; i < n; ++i){
		int x = read(), y = read();
		addedge(x, y), addedge(y, x);
	}
	dfs(1);
	for (register int i = 1; i <= m; ++i)
		a[i].x = read(), a[i].y = read(), a[i].w = read();
	std :: sort(a + 1, a + 1 + m);
	while (q--){
		int x = read(), y = read(), k = read();
		printf("%d\n", query(x, y, k));
	}
}