#include <cstdio>
#include <algorithm>
#include <cctype>
#include <cmath>
int read(){
	register int x = 0;
	register char f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = !f;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
#define N 5005
int n, x[N], y[N], w[N], vis[N];
double dis[N], ans;
double calc(int i, int j){
	if (i == 0) return w[j];
	if (j == 0) return w[i];
	return sqrt(1ll * (x[i] - x[j]) * (x[i] - x[j]) + 1ll * (y[i] - y[j]) * (y[i] - y[j]));
}
int main(){
	freopen("water.in", "r", stdin);
	freopen("water.out", "w", stdout);
	n = read();
	for (register int i = 1; i <= n; ++i) w[i] = read();
	for (register int i = 1; i <= n; ++i) x[i] = read(), y[i] = read();
	for (register int i = 1; i <= n; ++i) vis[i] = 0, dis[i] = 1e100;
	dis[0] = 0;
	for (register int i = 0, u; i <= n; ++i){
		double mn = 1e100;
		for (register int j = 0; j <= n; ++j)
			if (!vis[j] && dis[j] < mn) mn = dis[j], u = j;
		vis[u] = 1, ans += mn;
		for (register int j = 0; j <= n; ++j)
			if (!vis[j]) dis[j] = std :: min(dis[j], calc(u, j));
	}
	printf("%.2lf", ans);
}