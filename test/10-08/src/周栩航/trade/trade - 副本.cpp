#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define pb push_back


using namespace std;

inline ll read()
{
	ll t=0,dp=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	dp=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-48,c=getchar();
	return t*dp;
}
inline void write(ll x){if(x<0)	{putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+48);}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
/*------------------------------------------------------------------------------------------------------*/

const int N=3005;
int in[N],out[N],Fa[N],tim;
int n,p,q,px[N],py[N],pv[N],qx[N],qy[N],qk[N],cnt,poi[N*2],nxt[N*2],head[N],dep[N],lca[N],Q[N],top;
inline void add(int x,int y)
{
	poi[++cnt]=y;nxt[cnt]=head[x];head[x]=cnt;
	poi[++cnt]=x;nxt[cnt]=head[y];head[y]=cnt;
}
inline void Dfs(int x,int fa)
{
	in[x]=++tim;Fa[x]=fa;
	for(int i=head[x];i;i=nxt[i])
	{
		if(poi[i]==fa)	continue;
		dep[poi[i]]=dep[x]+1;
		Dfs(poi[i],x);
	}
	out[x]=tim;
}
inline bool bh(int x,int y){return in[x]<=in[y]&&out[y]<=out[x];}
int main()
{
	freopen("trade1.in","r",stdin);freopen("trade.out","w",stdout);
	n=read();p=read();q=read();
	For(i,1,n-1)	add(read(),read());
	Dfs(1,1);
	For(i,1,p)	px[i]=read(),py[i]=read(),pv[i]=read();
	For(i,1,q)	qx[i]=read(),qy[i]=read(),qk[i]=read();
	For(i,1,q)
	{
		int tx=qx[i],ty=qy[i];
		while(tx!=ty)
		{
			if(dep[tx]>dep[ty])	swap(tx,ty);
			ty=Fa[ty];
		}
		lca[i]=ty;
	}
	For(i,1,q)	if(dep[px[i]]>dep[py[i]])	swap(px[i],py[i]);
	For(i,1,q)
	{
		top=0;
		For(j,1,p)	
		{
			if(!bh(px[j],py[j]))
			{
				if((bh(px[j],qx[i])&&bh(py[j],qy[i]))||(bh(px[j],qy[i])&&bh(py[j],qx[i])))
					Q[++top]=pv[j];
			}
			else
				if(bh(lca[i],px[j])&&bh(lca[i],py[j])&&((bh(py[j],qx[i])&&bh(px[j],qx[i]))||(bh(py[j],qy[i])&&bh(px[j],qy[i]))))	Q[++top]=pv[j];
		}
		sort(Q+1,Q+top+1);
		writeln(Q[qk[i]]);
	}
}
