#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#include<bits/stdc++.h>
#define For(i,j,k)	for(register int i=j;i<=k;++i)
#define Dow(i,j,k)	for(register int i=k;i>=j;--i)
#define ll long long
#define pb push_back


using namespace std;

inline ll read()
{
	ll t=0,dp=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	dp=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-48,c=getchar();
	return t*dp;
}
inline void write(ll x){if(x<0)	{putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+48);}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
/*------------------------------------------------------------------------------------------------------*/

const int N=40005;
int in[N],out[N],Fa[N],tim,tp[N],q_tp[N],t_top;
int n,p,q,px[N],py[N],pv[N],qx[N],qy[N],qk[N],cnt,poi[N*2],nxt[N*2],head[N],dep[N],lca[N],Q[N],top;
vector<int> vec[N];
inline void add(int x,int y)
{
	poi[++cnt]=y;nxt[cnt]=head[x];head[x]=cnt;
	poi[++cnt]=x;nxt[cnt]=head[y];head[y]=cnt;
}
inline void Dfs(int x,int fa)
{
	in[x]=++tim;Fa[x]=fa;
	for(int i=head[x];i;i=nxt[i])
	{
		if(poi[i]==fa)	continue;
		dep[poi[i]]=dep[x]+1;
		Dfs(poi[i],x);
	}
	out[x]=tim;
}
int f[N][21];
inline void Pre()
{
	For(i,1,n)	f[i][0]=Fa[i];
	For(i,1,20)	For(j,1,n)	f[j][i]=f[f[j][i-1]][i-1];
}
inline int LCA(int x,int y)
{
	if(dep[x]>dep[y])	swap(x,y);
	int delta=dep[y]-dep[x];
	For(i,0,20)	if(delta>>i&1)	y=f[y][i];
	if(x==y)	return x;
	Dow(i,0,20)	if(f[x][i]!=f[y][i])	x=f[x][i],y=f[y][i];
	return f[x][0];
}
inline bool bh(int x,int y){return in[x]<=in[y]&&out[y]<=out[x];}
int main()
{
	freopen("trade.in","r",stdin);freopen("trade.out","w",stdout);
	n=read();p=read();q=read();
	For(i,1,n-1)	add(read(),read());
	Dfs((n+1)/2,(n+1)/2);
	Pre();
	For(i,1,p)	px[i]=read(),py[i]=read(),pv[i]=read();
	For(i,1,q)	qx[i]=read(),qy[i]=read(),qk[i]=read();
	For(i,1,q)
	{
		int ty=LCA(qx[i],qy[i]);
		lca[i]=ty;
	}
	For(i,1,p)	if(dep[px[i]]>dep[py[i]])	swap(px[i],py[i]);
	For(i,1,p)	if(bh(px[i],py[i]))	tp[i]=1,q_tp[++t_top]=i;
	For(i,1,p)
	if(!tp[i])
	{
		int ty=LCA(px[i],py[i]);
		vec[ty].pb(i);
	}
	For(i,1,q)
	{
		top=0;
		int jdb=0;
		for(jdb=0;jdb<vec[lca[i]].size();jdb+=2)	
		{
			int j=vec[lca[i]][jdb];
			if((bh(px[j],qx[i])&&bh(py[j],qy[i]))||(bh(px[j],qy[i])&&bh(py[j],qx[i])))
				Q[++top]=pv[j];
			if(!(jdb+1<vec[lca[i]].size()))	break;	
			int jj=vec[lca[i]][jdb+1];
			if((bh(px[jj],qx[i])&&bh(py[jj],qy[i]))||(bh(px[jj],qy[i])&&bh(py[jj],qx[i])))
				Q[++top]=pv[jj];
		}
		for(jdb+=2;jdb<vec[lca[i]].size();jdb++)	
		{
			int j=vec[lca[i]][jdb];
			if((bh(px[j],qx[i])&&bh(py[j],qy[i]))||(bh(px[j],qy[i])&&bh(py[j],qx[i])))
				Q[++top]=pv[j];
		}
		
		for(jdb=1;jdb<=t_top;jdb+=2)
		{
			int j=q_tp[jdb];
			if(bh(lca[i],px[j])&&bh(lca[i],py[j])&&((bh(py[j],qx[i])&&bh(px[j],qx[i]))||(bh(py[j],qy[i])&&bh(px[j],qy[i]))))	Q[++top]=pv[j];
			if(jdb+1>t_top)	break;
			int jj=q_tp[jdb+1];
			if(bh(lca[i],px[jj])&&bh(lca[i],py[jj])&&((bh(py[jj],qx[i])&&bh(px[jj],qx[i]))||(bh(py[jj],qy[i])&&bh(px[jj],qy[i]))))	Q[++top]=pv[jj];
		}
		for(jdb+=2;jdb<=t_top;jdb++)
		{
			int j=q_tp[jdb];
			if(bh(lca[i],px[j])&&bh(lca[i],py[j])&&((bh(py[j],qx[i])&&bh(px[j],qx[i]))||(bh(py[j],qy[i])&&bh(px[j],qy[i]))))	Q[++top]=pv[j];
		}
		nth_element(Q+1,Q+qk[i],Q+top+1);
		writeln(Q[qk[i]]);
	}
}
