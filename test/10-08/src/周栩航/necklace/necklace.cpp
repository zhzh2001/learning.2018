#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define pb push_back


using namespace std;

inline ll read()
{
	ll t=0,dp=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	dp=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-48,c=getchar();
	return t*dp;
}
inline void write(ll x){if(x<0)	{putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+48);}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}

const int N=200005;
int n,m,k,Fa[N];
struct node{int x,y,c;int v;}	e[N],t[N];
inline bool cmp(node x,node y){return x.v<y.v;}

inline int Get(int x){return x==Fa[x]?x:Fa[x]=Get(Fa[x]);}
inline void Merge(int x,int y){if(Get(x)!=Get(y))	Fa[Get(x)]=Get(y);}
inline bool check(int ex,bool ou)
{
	For(i,1,m)	if(e[i].c==0)	e[i].v+=ex;
	For(i,1,m)	t[i]=e[i];
	For(i,1,n)	Fa[i]=i;
	sort(t+1,t+m+1,cmp);
	double tmp=0;
	int edge=0,whi=0;
	For(i,1,m)
	{
		if(Get(t[i].x)==Get(t[i].y))	continue;
		Merge(t[i].x,t[i].y);
		tmp+=t[i].v;	
		edge++;
		if(t[i].c==0)	whi++;
		if(edge==n-1)	break; 
	}
	For(i,1,m)	if(e[i].c==0)	e[i].v-=ex; 	
	if(ou)	writeln(tmp-whi*ex);
	else	return whi>=k;
}
int main()
{
	freopen("necklace.in","r",stdin);
	freopen("necklace.out","w",stdout);
	n=read();m=read();k=read();
	For(i,1,m)
	{
		e[i].x=read()+1;e[i].y=read()+1;
		e[i].v=read();e[i].c=read();
	}
	int l=-101,r=101,ans=0;
	while(l<=r)
	{
		int mid=(l+r)/2;
		if(check(mid,0))	l=mid+1,ans=mid;else	r=mid-1;//�ױ�>=k 
	}
	check(ans,1);
}
