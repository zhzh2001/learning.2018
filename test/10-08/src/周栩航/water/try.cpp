#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define pb push_back


using namespace std;

inline ll read()
{
	ll t=0,dp=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	dp=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-48,c=getchar();
	return t*dp;
}
inline void write(ll x){if(x<0)	{putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+48);}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
/*------------------------------------------------------------------------------------------------------*/

const int N=5005;
int n,x[N],y[N],vis[N];
double dis[N],v[N][N];

inline double sqr(double x){return x*x;}
inline void Prim()
{
	For(i,1,n)	dis[i]=1e15;
	dis[0]=1e15;
	double ans=0;
	For(i,1,n+1)	
	{
		int k=0;
		For(j,1,n+1)	if(!vis[j]&&dis[j]<dis[k])	k=j;
		vis[k]=1;if(k)	ans+=dis[k];
		For(j,1,n+1)
			if(!vis[j])	
				dis[j]=min(dis[j],dis[k]+v[k][j]);
	}
	printf("%.2lf",ans);
}
int main()
{
	n=read();
	For(i,1,n)	v[n+1][i]=v[i][n+1]=read();
	For(i,1,n)	x[i]=read(),y[i]=read();
	For(i,1,n)	For(j,i+1,n)
		v[j][i]=v[i][j]=sqrt(1.0*sqr(x[i]-x[j])+1.0*sqr(y[i]-y[j]));
	Prim();
}
