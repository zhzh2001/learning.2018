#include <cmath>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>

using namespace std;

typedef long long LL;

const double double_inf = 1e9;
const int maxn = 5005;

struct Point
{
	int x, y;
} p[maxn];

bool vis[maxn];
LL D[maxn];
double dist[maxn];
int n;

inline double sqr(int x)
{
	return (double) x * x;
}

inline double getdist(Point a, Point b)
{
	return sqrt(sqr(a.x - b.x) + sqr(a.y - b.y));
}

inline double prim(int from)
{
	for(int i = 1; i <= n; ++i)
		dist[i] = double_inf;
	dist[from] = (double) D[from];
	memset(vis, 0, sizeof(vis));
	for(int i = 1; i <= n; ++i)
	{
		int now = 0;
		for(int j = 1; j <= n; ++j)
			if(!vis[j] && (!now || dist[j] < dist[now]))
				now = j;
//		cout << now << endl;
		vis[now] = true;
		if(!now)
			break;
		for(int j = 1; j <= n; ++j)
		{
			if(!vis[j])
			{
				double dd = min(getdist(p[now], p[j]), (double) D[j]);
//				cout << dd << endl;
				if(dd < dist[j])
					dist[j] = dd;
			}
		}
	}
	double ans = 0.;
	for(int i = 1; i <= n; ++i)
		ans += dist[i];
	return ans;
}

int main()
{
	freopen("water.in", "r", stdin);
	freopen("water.out", "w", stdout);
	scanf("%d", &n);
	int mini = 0;
	for(int i = 1; i <= n; ++i)
	{
		scanf("%lld", &D[i]);
		if(!mini || D[i] < D[mini])
			mini = i;
	}
	for(int i = 1; i <= n; ++i)
		scanf("%d%d", &p[i].x, &p[i].y);
	printf("%.2f", prim(mini));
	fclose(stdin);
	fclose(stdout);
	return 0;
}
