#include <ctime>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <algorithm>

using namespace std;

const int maxn = 50005;
const int maxm = 100005;

int n, m, k;

struct Edge
{
	int from, to, dist, cc;

	inline bool operator < (const Edge& other) const
	{
		return this->dist < other.dist;
	}
} e[maxm << 1];

int fa[maxn];
inline int getfa(int x)
{
	return fa[x] == x ? x : fa[x] = getfa(fa[x]);
}

inline int Kruskal()
{
	int K[] = {k, n - 1 - k};
	for(int i = 0; i < n; ++i)
		fa[i] = i;
	int ans = 0;
	for(int i = 1; i <= m; ++i)
	{
		if(K[e[i].cc])
		{
			int fax = getfa(e[i].from);
			int fay = getfa(e[i].to);
			if(fax != fay)
			{
				fa[fax] = fay;
				ans += e[i].dist;
				K[e[i].cc]--;
			}
		}
	}
	return ans;
}

int dist[1000][1000][2];

int main()
{
	freopen("necklace.in", "r", stdin);
	freopen("necklace.out", "w", stdout);
	int tim = clock();
	srand((unsigned) time(NULL));
	scanf("%d%d%d", &n, &m, &k);
	if(n <= 1000)
	{
		memset(dist, 0xff, sizeof(dist));
		for(int i = 1, f, t, d, c; i <= m; ++i)
		{
			scanf("%d%d%d%d", &f, &t, &d, &c);
			if(~dist[f][t][c])
				dist[f][t][c] = min(dist[f][t][c], d);
			else
				dist[f][t][c] = d;
		}
		m = 0;
		for(int i = 0; i < n; ++i)
		{
			for(int j = 0; j < n; ++j)
			{
				if(~dist[i][j][0])
				{
					e[++m] = (Edge)
					{
						i, j, dist[i][j][0], 0
					};
				}
				if(~dist[i][j][1])
				{
					e[++m] = (Edge)
					{
						i, j, dist[i][j][1], 1
					};
				}
			}
		}
	}
	else
	{
		for(int i = 1; i <= m; ++i)
			scanf("%d%d%d%d", &e[i].from, &e[i].to, &e[i].dist, &e[i].cc);
	}
	sort(e + 1, e + m + 1);
	int ans = Kruskal();
	while(clock() - tim < 800)
	{
		random_shuffle(e + 1, e + m + 1);
		ans = min(ans, Kruskal());
	}
	printf("%d\n", ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
