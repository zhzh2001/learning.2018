#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=1e5+10;
int nedge=0,p[2*N],nex[2*N],head[2*N];
struct ppap{int x,y,v;}a[N];
inline bool cmp(ppap a,ppap b){
	return a.v<b.v;
}
int n,m,Q;
inline void addedge(int a,int b){
	p[++nedge]=b;nex[nedge]=head[a];head[a]=nedge;
}
vector<int>e[N];
int rt[N],t[200*N],mt[200*N],ls[200*N],rs[200*N],Cnt=0,cnt;
int Fa[N][20],dep[N];
inline void xg(int l,int r,int &nod,int la,int x){
	nod=++Cnt;ls[nod]=ls[la];rs[nod]=rs[la];
	t[nod]=t[la];mt[nod]=mt[la];
	if(l==r){
		t[nod]++;
		if(t[nod]==2)mt[nod]=1;
		return;
	}
	int mid=l+r>>1;
	if(x<=mid)xg(l,mid,ls[nod],ls[la],x);
	else xg(mid+1,r,rs[nod],rs[la],x);
	mt[nod]=mt[ls[nod]]+mt[rs[nod]];
}
inline void dfs(int x,int fa){
	rt[x]=rt[fa];dep[x]=dep[fa]+1;
	for(int i=0;i<e[x].size();i++){
		xg(1,n,rt[x],rt[x],e[x][i]);
	}
	Fa[x][0]=fa;
	for(int i=1;i<20;i++)Fa[x][i]=Fa[Fa[x][i-1]][i-1];
	for(int k=head[x];k;k=nex[k])if(p[k]!=fa){
		dfs(p[k],x);
	}
}
inline int lca(int x,int y){
	if(dep[x]<dep[y])swap(x,y);
	for(int i=19;i>=0;i--)if(dep[Fa[x][i]]>=dep[y])x=Fa[x][i];
	for(int i=19;i>=0;i--)if(Fa[x][i]!=Fa[y][i])x=Fa[x][i],y=Fa[y][i];
	if(x!=y)x=Fa[x][0];return x;
}
inline void merge(int l,int r,int &nod,int xn,int yn,int la,int lla){
	if(xn*yn==0){nod=xn+yn;return;}
	nod=++cnt;mt[nod]=0;
	if(l==r){
		t[nod]=t[xn]+t[yn]-t[la]-t[lla];
		if(t[nod]==2)mt[nod]=1;
		return;
	}
	int mid=l+r>>1;
	merge(l,mid,ls[nod],ls[xn],ls[yn],ls[la],ls[lla]);
	merge(mid+1,r,rs[nod],rs[xn],rs[yn],rs[la],rs[lla]);
	mt[nod]=mt[ls[nod]]+mt[rs[nod]];
}
inline int sk(int l,int r,int K,int nod){
	if(!nod)return 0;
	if(l==r)return l;
	int mid=l+r>>1;
	if(K<=mt[ls[nod]])return sk(l,mid,K,ls[nod]);
	else return sk(mid+1,r,K-mt[ls[nod]],rs[nod]);
}
int main()
{
	freopen("trade.in","r",stdin);
	freopen("trade.out","w",stdout);
	n=read();m=read();Q=read();
	for(int i=1;i<n;i++){
		int x=read(),y=read();
		addedge(x,y);addedge(y,x);
	}
	for(int i=1;i<=m;i++){
		a[i].x=read();a[i].y=read();
		a[i].v=read();
	}
	sort(a+1,a+m+1,cmp);
	for(int i=1;i<=m;i++){
		e[a[i].x].push_back(i);
		e[a[i].y].push_back(i);
	}
	dfs(1,0);
	while(Q--){
		cnt=Cnt;
		int x=read(),y=read(),K=read();
		int LCA=lca(x,y);
//		cout<<LCA<<endl;
		merge(1,n,rt[n+1],rt[x],rt[y],rt[LCA],rt[Fa[LCA][0]]);
		int ans=sk(1,n,K,rt[n+1]);
		writeln(a[ans].v);
	}
	return 0;
}
/*
6 5 8
6 1
6 2
6 3
2 5
3 4
3 1 8
4 2 6
6 3 9
3 2 9
5 3 10
3 2 2
3 6 1
1 4 2
3 5 3
1 4 1
3 5 1
4 6 1
4 2 1
*/
