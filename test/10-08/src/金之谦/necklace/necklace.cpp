#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=1e5+10;
int n,m,K,ans=1e18;
struct ppap{int x,y,w,c;}a[N];
inline bool cmp(ppap a,ppap b){return a.w==b.w?a.c<b.c:a.w<b.w;}
int fa[N];
inline int find(int x){
	if(fa[x]==x)return x;
	return fa[x]=find(fa[x]);
}
inline bool check(int x){
	for(int i=1;i<=n;i++)fa[i]=i;
	for(int i=1;i<=m;i++){
		if(a[i].c)a[i].w+=x;
	}
	sort(a+1,a+m+1,cmp);
	int sum=0,cnt=n,ss=0;
	for(int i=1;i<=m;i++){
		int fx=find(a[i].x),fy=find(a[i].y);
		if(fx!=fy){
			fa[fx]=fy;cnt--;sum+=a[i].w;
			if(a[i].c==0)ss++;
		}
	}
	for(int i=1;i<=m;i++){
		if(a[i].c)a[i].w-=x;
	}
	if(ss>=K){
		ans=min(ans,sum);
		return 1;
	}
	return 0;
}
signed main()
{
	freopen("necklace.in","r",stdin);
	freopen("necklace.out","w",stdout);
	n=read();m=read();K=read();
	for(int i=1;i<=m;i++){
		a[i].x=read();a[i].y=read();
		a[i].w=read();a[i].c=read();
	}
	int l=0,r=200;
	while(l<=r){
		int mid=l+r>>1;
		if(check(mid))r=mid-1;
		else l=mid+1;
	}
	writeln(ans);
	return 0;
}