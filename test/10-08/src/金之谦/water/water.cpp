#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
#define int long long
using namespace std;
typedef double D;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=5010;
struct ppap{int x,y,w;}a[N];
int n,vis[N];
D dist[N],ans=0;
inline bool cmp(ppap a,ppap b){return a.w<b.w;}
inline D sqr(D x){return x*x;}
inline void prim(){
	for(int i=1;i<=n;i++)dist[i]=a[i].w;
	for(int i=1;i<=n;i++){
		int k=-1;
		for(int j=1;j<=n;j++)if(!vis[j]&&(k==-1||dist[k]>dist[j]))k=j;
		if(k==-1)break;vis[k]=1;
		for(int j=1;j<=n;j++)if(!vis[j]){
			D dis=sqrt(sqr(a[k].x-a[j].x)+sqr(a[k].y-a[j].y));
			if(dist[j]>dis)dist[j]=dis;
		}
	}
}
signed main()
{
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)a[i].w=read();
	for(int i=1;i<=n;i++){
		a[i].x=read();a[i].y=read();
	}
	prim();
	for(int i=1;i<=n;i++)ans+=dist[i];
	printf("%.2lf",ans);
	return 0;
}