#include <iostream>
#include <cstdio>
#include <algorithm>
using namespace std;
typedef int ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=1e5+10,TT=15;
struct edge{ll to,net;}e[N<<1];
struct node{ll x,y,v;}a[N];
ll head[N],edge,n,p,q,fa[N][21],dep[N],u[N],num;
inline void addedge(ll x,ll y){
	e[++edge].to=y,e[edge].net=head[x],head[x]=edge;
	e[++edge].to=x,e[edge].net=head[y],head[y]=edge;
}
inline void dfs(ll x){
	for (ll i=head[x];i;i=e[i].net)
		if (e[i].to!=fa[x][0]){
			fa[e[i].to][0]=x;
			dep[e[i].to]=dep[x]+1;
			dfs(e[i].to);
		}
}
inline ll isfa(ll x,ll y){
	if (dep[x]<=dep[y]) swap(x,y);
	else return 0;
	for (ll i=TT;~i;--i)
		if (dep[fa[x][i]]>=dep[y]) x=fa[x][i];
	if (x==y) return 1;
	return 0;
}
inline ll LCA(ll x,ll y){
	if (dep[x]<dep[y]) swap(x,y);
	for (ll i=TT;~i;--i)
		if (dep[fa[x][i]]>=dep[y]) x=fa[x][i];
	if (x==y) return x;
	for (ll i=TT;~i;--i)
		if (fa[x][i]!=fa[y][i]) x=fa[x][i],y=fa[y][i];
	return fa[x][0];
}
int main(){
	freopen("trade.in","r",stdin);
	freopen("trade.out","w",stdout);
	read(n),read(p),read(q);
	for (ll i=1,x,y;i<n;++i) read(x),read(y),addedge(x,y);
	dep[1]=1,dfs(1);
	for (ll i=1;i<=TT;++i)
		for (ll j=1;j<=n;++j)
			fa[j][i]=fa[fa[j][i-1]][i-1];
	for (ll i=1;i<=p;++i){
		read(a[i].x),read(a[i].y),read(a[i].v);
		if (dep[a[i].x]>dep[a[i].y]) swap(a[i].x,a[i].y);
	}
	for (ll i=1,x,y,k;i<=q;++i){
		read(x),read(y),read(k);
		num=0;
		if (dep[x]<dep[y]) swap(x,y);
		for (ll j=1;j<=p;++j){
			bool flag=0;
			if (isfa(a[j].x,a[j].y)){
				if (isfa(y,x)){
					if (isfa(a[j].x,x)&&isfa(a[j].y,x)&&isfa(y,a[j].x)&&isfa(y,a[j].y)) flag=1;
				}else{
					ll t=LCA(x,y);
					if (!isfa(t,a[j].x)||!isfa(t,a[j].y)) continue;
					if (isfa(a[j].x,x)&&isfa(a[j].y,x)) flag=1;else
					if (isfa(a[j].x,y)&&isfa(a[j].y,y)) flag=1;
				}
			}else{
				if (isfa(y,x)) continue;
				if (isfa(a[j].x,x)&&isfa(a[j].y,y)) flag=1;else
				if (isfa(a[j].x,y)&&isfa(a[j].y,x)) flag=1;
			}
			if (flag) u[++num]=a[j].v;
		}
		sort(u+1,u+1+num);
//	cout<<num<<' ';for(ll i=1;i<=num;++i) cout<<u[i]<<' ';puts("");
		wr(u[k]),puts("");
	}
	return 0;
}
/*
6 5 8
6 1
6 2
6 3
2 5
3 4
3 1 8
4 2 6
6 3 9
3 2 9
5 3 10
3 2 2
3 6 1
1 4 2
3 5 3
1 4 1
3 5 1
4 6 1
4 2 1
*/
