#include <iostream>
#include <cstdio>
#include <cmath>
using namespace std;
typedef long long ll;
typedef double db;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=5010;
ll n,x[N],y[N];
db D[N][N],dist[N],ans;
bool vis[N];
inline ll sqr(ll x){return x*x;}
int main(){
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
	read(n);
	for (ll i=1,v;i<=n;++i) read(v),D[0][i]=D[i][0]=v;
	for (ll i=1;i<=n;++i) read(x[i]),read(y[i]);
	for (ll i=1;i<=n;++i) for (ll j=1;j<=n;++j) D[i][j]=D[j][i]=sqrt(sqr(x[i]-x[j])+sqr(y[i]-y[j]));
	for (ll i=1;i<=n;++i) dist[i]=1e12;
	do{
		ll k=-1;
		for (ll i=0;i<=n;++i) if (!vis[i]&&(k==-1||dist[k]>dist[i])) k=i;
		if (k==-1) break;
		ans+=dist[k];
		vis[k]=1;
		for (ll i=0;i<=n;++i) if (!vis[i]&&dist[i]>D[i][k]) dist[i]=D[i][k];
	}while (1);
	printf("%.2lf",ans);
	return 0;
}
