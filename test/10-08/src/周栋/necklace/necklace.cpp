#include <iostream>
#include <algorithm>
#include <cstdio>
#include <ctime>
using namespace std;
typedef int ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=1e5+10;
ll n,m,k,fa[N],ans,g[N];
struct node{
	ll x,y,v,c;
	bool operator <(const node&res)const{
		return v<res.v;
	}
}a[N];
inline ll fid(ll x){return x==fa[x]?x:fa[x]=fid(fa[x]);}
inline void work(){
	for (ll i=0;i<n;++i) fa[i]=i;
	ll num=0,j=1;
	for (ll i=1;i<=m;++i) if (a[i].c==g[j]){
		ll t1=fid(a[i].x),t2=fid(a[i].y);
		if (t1==t2) continue;
		num+=a[i].v;
		fa[t1]=t2;
		++j;
		if (j==n) break;
	}
	if (j<n) return;
	ans=min(ans,num);
}
int main(){
	freopen("necklace.in","r",stdin);
	freopen("necklace.out","w",stdout);
	srand(998244353);
	ll T=clock();
	read(n),read(m),read(k);
	for (ll i=1;i<=m;++i) read(a[i].x),read(a[i].y),read(a[i].v),read(a[i].c);
	sort(a+1,a+1+m);
	for (ll i=k+1;i<n;++i) g[i]=1;
	ans=100*n;
	for (;;){
		if (clock()-T>850) break;
		work();
		random_shuffle(g+1,g+n);
	}
	wr(ans);
	return 0;
}
