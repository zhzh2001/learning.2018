#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

inline char read(){
    static const int IN_LEN=1000000;
    static char buf[IN_LEN],*s,*t;
    return (s==t?t=(s=buf)+fread(buf,1,IN_LEN,stdin),(s==t?-1:*s++):*s++);
}
template<class T>
inline void read(T &x){
    static bool iosig;
    static char c;
    for (iosig=false,c=read();!isdigit(c);c=read()){
        if (c=='-') iosig=true;
        if (c==-1) return;
    }
    for (x=0;isdigit(c);c=read()) x=((x+(x<<2))<<1)+(c^'0');
    if (iosig) x=-x;
}
const int OUT_LEN=10000000;
char obuf[OUT_LEN],*ooh=obuf;
inline void print(char c){
    if (ooh==obuf+OUT_LEN) fwrite(obuf,1,OUT_LEN,stdout),ooh=obuf;
    *ooh++=c;
}
template<class T>
inline void print(T x){
    static int buf[30],cnt;
    if (x==0) print('0');
    else{
        if (x<0) print('-'),x=-x;
        for (cnt=0;x;x/=10) buf[++cnt]=x%10+48;
        while (cnt) print((char)buf[cnt--]);
    }
}
inline void flush(){
	fwrite(obuf,1,ooh-obuf,stdout);
}

void judge(){
    freopen("necklace.in","r",stdin);
    freopen("necklace.out","w",stdout);
}

const int maxn=51000;
int n,k,m,ans,tot,f[maxn];
struct info{
	int x,y,w,c;
}e[maxn<<1];

int find(int x){
	return f[x]==x?x:f[x]=find(f[x]);
}

bool cmp(info a,info b){
	return a.w==b.w?a.c<b.c:a.w<b.w;
}

void change(int num){
	for (int i=1;i<=m;i++){
		if (e[i].c==0) e[i].w+=num;
	}
}

bool check(int x){
	int cnt=0,sum=0; tot=0;
	change(x);
	for (int i=1;i<=n;i++) f[i]=i;
	sort(e+1,e+m+1,cmp);
	for (int i=1;i<=m;i++){
		int u=find(e[i].x),v=find(e[i].y),w=e[i].w;
		if (u!=v){
			if (e[i].c==0) sum++;
			cnt++; f[u]=v; tot+=w;
		}
		if (cnt==n-1) break;
	}
	change(-x);
	return sum>=k;
}
			
int main(){
	judge();
	read(n),read(m),read(k);
	for (int i=1;i<=m;i++) read(e[i].x),read(e[i].y),read(e[i].w),read(e[i].c),e[i].x++,e[i].y++;
	int l=-101,r=101;
	while (l<=r){
		int mid=l+r>>1;
		if (check(mid)) l=mid+1,ans=tot-k*mid;
		else r=mid-1;
	}
	print(ans),print('\n');
	return flush(),0;
}
