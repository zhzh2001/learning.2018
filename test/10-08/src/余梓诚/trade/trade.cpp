#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

inline char read(){
    static const int IN_LEN=1000000;
    static char buf[IN_LEN],*s,*t;
    return (s==t?t=(s=buf)+fread(buf,1,IN_LEN,stdin),(s==t?-1:*s++):*s++);
}
template<class T>
inline void read(T &x){
    static bool iosig;
    static char c;
    for (iosig=false,c=read();!isdigit(c);c=read()){
        if (c=='-') iosig=true;
        if (c==-1) return;
    }
    for (x=0;isdigit(c);c=read()) x=((x+(x<<2))<<1)+(c^'0');
    if (iosig) x=-x;
}
const int OUT_LEN=10000000;
char obuf[OUT_LEN],*ooh=obuf;
inline void print(char c){
    if (ooh==obuf+OUT_LEN) fwrite(obuf,1,OUT_LEN,stdout),ooh=obuf;
    *ooh++=c;
}
template<class T>
inline void print(T x){
    static int buf[30],cnt;
    if (x==0) print('0');
    else{
        if (x<0) print('-'),x=-x;
        for (cnt=0;x;x/=10) buf[++cnt]=x%10+48;
        while (cnt) print((char)buf[cnt--]);
    }
}
inline void flush(){
	fwrite(obuf,1,ooh-obuf,stdout);
}

void judge(){
    freopen("trade.in","r",stdin);
    freopen("trade.out","w",stdout);
}

const int maxn=5100;

int n,m,q,p,opt,fa[maxn],dep[maxn];
struct info{
	int x,y,k;
}a[maxn],b[maxn];
struct edge{
	int to,nxt;
}e[maxn<<1];
int head[maxn],cnt;
void add(int x,int y){
	e[++cnt].to=y; e[cnt].nxt=head[x]; head[x]=cnt;
}

int size[maxn],top[maxn],son[maxn],dfn[maxn];
void dfs(int u){
	size[u]=1; dep[u]=dep[fa[u]]+1;
	for (int i=head[u];i;i=e[i].nxt){
		int k=e[i].to;
		if (k==fa[u]) continue;
		fa[k]=u; dfs(k); size[u]+=size[k];
		if (!son[u]||size[k]>size[son[u]]) son[u]=k;
	}
}
void dfs(int u,int Top){
	top[u]=Top; dfn[u]=++opt;
	if (!son[u]) return;
	dfs(son[u],Top);
	for (int i=head[u];i;i=e[i].nxt){
		int k=e[i].to;
		if (k==son[u]||k==fa[u]) continue;
		dfs(k,k);
	}
}
int lca(int x,int y){
	while (top[x]!=top[y]){
		if (dep[top[x]]>=dep[top[y]]) x=fa[top[x]];
		else y=fa[top[y]];
	}
	return dep[x]<dep[y]?x:y;
}

bool cmp(info a,info b){
	return a.k<b.k;
}

int main(){
	judge();
	read(n),read(p),read(q);
	for (int i=1;i<n;i++){
		int x,y; read(x),read(y);
		add(x,y); add(y,x);
	}
	dfs(1); dfs(1,1);
	for (int i=1;i<=p;i++) read(a[i].x),read(a[i].y),read(a[i].k);
	sort(a+1,a+p+1,cmp);
	for (int i=1;i<=q;i++){
		read(b[i].x),read(b[i].y),read(b[i].k);
		int sum=0;
		for (int j=1;j<=p;j++){
			if (lca(b[i].x,a[j].x)==a[j].x||lca(b[i].x,a[j].y)==a[j].y){
				if (lca(b[i].y,a[j].x)==a[j].x||lca(b[i].y,a[j].y)==a[j].y){
					int t1=lca(a[j].x,a[j].y),t2=lca(b[i].x,b[i].y);
					if (lca(t1,t2)!=t2) continue;
					sum++;
					if (sum==b[i].k){ 
						print(a[j].k),print('\n');
						break;
					}
				}
			}
		}
	}
	return flush(),0;
}
