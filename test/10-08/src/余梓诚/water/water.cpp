#include <cmath>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

void judge(){
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
}

const int maxn=5100;
const double inf=1e100;
int n,m;
double ans,v[maxn],dis[maxn],f[maxn][maxn];
bool used[maxn];
struct info{
	long long x,y;
}a[maxn];

double calc(int i,int j){
	long long t=1ll*(a[i].x-a[j].x)*(a[i].x-a[j].x)+1ll*(a[i].y-a[j].y)*(a[i].y-a[j].y);
	return sqrt(t);
}

void run(){
	for (int i=1;i<=n;i++){
		for (int j=1;j<=n;j++) f[i][j]=calc(i,j);
		f[0][i]=f[i][0]=v[i];
	}
	for (int i=1;i<=n;i++) dis[i]=inf,used[i]=0;
	dis[0]=0;
	for (int i=0;i<=n;i++){
		int u;
		double Min=inf;
		for (int j=0;j<=n;j++){
			if (!used[j]&&Min>dis[j]) u=j,Min=dis[j];
		}
		ans+=Min;
		used[u]=1;
		for (int j=0;j<=n;j++){
			if (!used[j]&&u!=j) dis[j]=min(dis[j],f[u][j]);
		}
	}
}

int main(){
	judge();
	scanf("%d",&n);
	for (int i=1;i<=n;i++) scanf("%lf",&v[i]);
	for (int i=1;i<=n;i++) scanf("%lld%lld",&a[i].x,&a[i].y);
	run();
	printf("%.2lf\n",ans);
	return 0;
}
