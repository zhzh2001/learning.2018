#include<bits/stdc++.h>
using namespace std;
#define int long long
struct node{
	int u,v,w,c;
}a[500050],b[500050];
int fa[500050];
bool cmp(node a,node b){
	return a.w==b.w?a.c<b.c:a.w<b.w;
}
int n,m,k,ans;
int find(int x){return fa[x]==x?x:fa[x]=find(fa[x]);}
inline int read(){
	int k=0; int f=1; char ch=getchar();
	while(!isdigit(ch)){if(ch=='-') f=-1; ch=getchar();}
	while(isdigit(ch)){k=(k<<1)+(k<<3)+(ch^48);ch=getchar();}
	return k*f;
}
bool pd(int x){
	for(int i=0;i<n;i++) fa[i]=i;
	for(int i=1;i<=m;i++){
		b[i].u=a[i].u;  b[i].v=a[i].v;
		b[i].w=a[i].w+(a[i].c==0)*x; b[i].c=a[i].c;
	}
	sort(b+1,b+m+1,cmp); int cnt=0; int gs=0; ans=0;
	for(int i=1;i<=m;i++){
		int fx=find(b[i].u); int fy=find(b[i].v);
		if(fx!=fy){
			fa[fx]=fy;
			ans+=b[i].w;
			if(b[i].c==0) gs++; cnt++;
			if(cnt==n-1) break;
		}
	}
	return gs==k;
}
signed main(){
	freopen("necklace.in","r",stdin);
	freopen("necklace.out","w",stdout);
	 cin>>n>>m>>k;
	 for(int i=0;i<n;i++) fa[i]=i;
	for(int i=1;i<=m;i++){
		a[i].u=read(); a[i].v=read(); a[i].w=read(); a[i].c=read();
	}
	int l=-100; int r=100; int res=0;
	while(l<=r){
		int mid=(l+r)>>1;
		if(pd(mid)) l=mid+1,res=mid;
		else r=mid-1;
	}
	pd(res);
	cout<<ans-k*res;
}
