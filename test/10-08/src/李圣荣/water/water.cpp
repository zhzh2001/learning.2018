#include<bits/stdc++.h>
using namespace std;
#define int long long
inline int read(){
	int k=0; int f=1; char ch=getchar();
	while(!isdigit(ch)){if(ch=='-') f=-1; ch=getchar();}
	while(isdigit(ch)){k=(k<<1)+(k<<3)+(ch^48);ch=getchar();}
	return k*f;
}
int f[100050],w[100050];
int find(int x){return f[x]==x?x:f[x]=find(f[x]);}
struct node{
	int fr,to;
	double w;
}q[13000050];
struct sdaw{
	int x,y;
}a[100050];
int cnt;
double js(int p,int q){
	return sqrt((a[p].x-a[q].x)*(a[p].x-a[q].x)*1.00+(a[p].y-a[q].y)*(a[p].y-a[q].y)*1.00);
}
bool cmp(node a,node b){
	return a.w<b.w;
}
signed main(){
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
	int n; cin>>n;
	for(int i=1;i<=n;i++) w[i]=read();
	for(int i=1;i<=n+50;i++) f[i]=i;
	for(int i=1;i<=n;i++) a[i].x=read(),a[i].y=read();
	for(int i=1;i<=n;i++){
		for(int j=i+1;j<=n;j++){
			if(i==j) continue;
			q[++cnt].fr=i; q[cnt].to=j; double kk=js(i,j);
			q[cnt].w=kk;
		//	printf("%.2f\n",kk);
		}
	}
	for(int i=1;i<=n;i++){
	  q[++cnt].fr=i,q[cnt].to=n+10,q[cnt].w=w[i]*1.00;
 	// q[++cnt].fr=n+10,q[cnt].to=i,q[cnt].w=w[i]*1.00;	
	} 
	sort(q+1,q+cnt+1,cmp); int gs=0; double ans=0.00;
	for(int i=1;i<=cnt;i++){
		int x=find(q[i].fr); int y=find(q[i].to);
		if(x!=y){
			f[x]=y;
			ans+=q[i].w;
			gs++;
			if(gs==n) break;
		}
	}
	printf("%.2lf",ans);
}
