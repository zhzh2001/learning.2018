#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
const int inf=5000000;
const int N=50005,M=100005;
int l,r,mid,n,m,k,sum,fa[N],jsq;
inline int get_fa(int x)
{
	return x==fa[x]?x:fa[x]=get_fa(fa[x]);
}
struct edge
{
	int x,y,d,p;
	bool operator<(const edge &a)const
	{
		return d+p*mid<a.d+a.p*mid;
	}
}a[M];
int main()
{
	freopen("necklace.in","r",stdin);
	freopen("necklace.out","w",stdout);
	read(n);
	read(m);
	read(k);
	k=n-1-k;
	for (int i=1;i<=m;++i)
	{
		read(a[i].x);
		read(a[i].y);
		++a[i].x;
		++a[i].y;
		read(a[i].d);
		read(a[i].p);
	}
	l=-inf,r=inf;
	while (l<r)
	{
		mid=l+r;
		if (mid>=0)
			mid/=2;
		else
			mid=-((-mid)/2);
		sort(a+1,a+1+m);
		int tot=0;
		sum=0;
		for (int i=1;i<=n;++i)
			fa[i]=i;
		for (int i=1;i<=m;++i)
		{
			int p=get_fa(a[i].x),q=get_fa(a[i].y);
			if (p!=q)
			{
				sum+=a[i].d;
				tot+=a[i].p;
				fa[p]=q;
			}
		}
		//cout<<l<<' '<<r<<' '<<mid<<' '<<tot<<'\n';
		if (tot==k)
		{
			cout<<sum;
			return 0;
		}
		if (tot<k)
			r=mid;
		else
			l=mid+1;
		++jsq;
		if (jsq>=25)
			break;
	}
	//cout<<k<<' '<<sum;
	cout<<sum;
	return 0;
}