#include<bits/stdc++.h>
using namespace std;
#define pi pair<int,int>
#define fi first
#define se second
const int N=40005;
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
struct data
{
	int x,y,z;
	bool operator<(const data &a)const
	{
		return z<a.z;
	}
}a[N];
vector<data> v[N],b;
int n,x,y,z,k,t,m;
int fa[17][N];
int to[N*2],pr[N*2],la[N],l[N];
int cnt;
inline void add(int x,int y)
{
	to[++cnt]=y;
	pr[cnt]=la[x];
	la[x]=cnt;
}
inline void dfs(int x)
{
	for (int i=1;i<=15;++i)
		fa[i][x]=fa[i-1][fa[i-1][x]];
	for (int i=la[x];i;i=pr[i])
		if (!l[to[i]])
		{
			l[to[i]]=l[x]+1;
			fa[0][to[i]]=x;
			dfs(to[i]);
		}
}
inline int ktha(int x,int y)
{
	for (int i=0;i<=15;++i)
		if ((y>>i)&1)
			x=fa[i][x];
	return x;
}
inline int lca(int x,int y)
{
	if (l[x]<l[y])
		swap(x,y);
	int p=l[x]-l[y];
	for (int i=0;i<=15;++i)
		if ((p>>i)&1)
			x=fa[i][x];
	for (int i=15;i>=0;--i)
		if (fa[i][x]!=fa[i][y])
		{
			x=fa[i][x];
			y=fa[i][y];
		}
	return x==y?x:fa[0][x];
}
inline bool pda(int x,int y)
{
	return (l[x]>=l[y])&&(ktha(x,l[x]-l[y])==y);
}
inline bool check1(int x,int y,data a)
{
	if (l[x]>l[y])
		swap(x,y);
	if (l[a.x]>l[a.y])
		swap(a.x,a.y);
	// return ((l[x]>l[a.x])&&(ktha(x,l[x]-l[a.x])==a.x))
	//      &&((l[y]<l[a.y])&&(ktha(a.y,l[a.y]-l[y])==y));
	return pda(x,a.x)&&pda(a.y,y);
}
inline int check2(int x,int y,data a)
{
	return (pda(x,a.x)&&pda(y,a.y))||(pda(x,a.y)&&pda(y,a.x));
}
inline int check3(int x,int y,int p,data a)
{
	return pda(a.x,p)&&(pda(x,a.y)||pda(y,a.y));
}
int main()
{
	freopen("trade.in","r",stdin);
	freopen("trade.out","w",stdout);
	read(n);
	read(m);
	read(t);
	for (int i=1;i<n;++i)
	{
		read(x);
		read(y);
		add(x,y);
		add(y,x);
	}
	l[1]=1;
	dfs(1);
	for (int i=1;i<=m;++i)
	{
		read(a[i].x);
		read(a[i].y);
		read(a[i].z);
		int p=lca(a[i].x,a[i].y);
		if (p==a[i].x||p==a[i].y)
		{
			if (l[a[i].x]>l[a[i].y])
				swap(a[i].x,a[i].y);
			b.push_back(a[i]);
		}
		else
			v[p].push_back(a[i]);
	}
	sort(b.begin(),b.end());
	for (int i=1;i<=n;++i)
		if (v[i].size())
			sort(v[i].begin(),v[i].end());
	// sort(a+1,a+1+m);
	while (t--)
	{
		read(x);
		read(y);
		read(k);
		int p=lca(x,y);
		if (p==x||p==y)
		{
			for (unsigned i=0;i<b.size();++i)
				if (check1(x,y,b[i]))
				{
					--k;
					if (k==0)
					{
						printf("%d\n",b[i].z);
						break;
					}
				}
		}
		else
		{
			int l=0,r=0;
			while (1)
			{
				if ((r==b.size())||(l!=v[p].size()&&v[p][l].z<=b[r].z))
				{
					if (check2(x,y,v[p][l]))
						--k;
					if (k==0)
					{
						printf("%d\n",v[p][l].z);
						break;
					}
					++l;
				}
				else
				{
					if (check3(x,y,p,b[r]))
						--k;
					if (k==0)
					{
						printf("%d\n",b[r].z);
						break;
					}
					++r;
				}
			}
		}
	}
	return 0;
}