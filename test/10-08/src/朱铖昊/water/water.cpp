#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define pi pair<int,int>
const int N=5005;
int a[N],fa[N];
int n,cnt;
double ans;
pi p[N];
inline void read(int &x)
{
	char c=getchar();
	int y=1;
	while (c>'9'||c<'0')
	{
		if (c=='-')
			y=-1;
		c=getchar();
	}
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
	x*=y;
}
inline int get_fa(int x)
{
	return x==fa[x]?x:fa[x]=get_fa(fa[x]);
}
inline ll dist(pi a,pi b)
{
	return (ll)(a.first-b.first)*(a.first-b.first)+(ll)(a.second-b.second)*(a.second-b.second);
}
struct edge
{
	int x,y;
	ll d;
	bool operator<(const edge &a)const
	{
		return d<a.d;
	}
}e[N*N/2];
int main()
{
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
	read(n);
	for (int i=1;i<=n;++i)
	{
		read(a[i]);
		ans+=a[i];
	}
	for (int i=1;i<=n;++i)
	{
		read(p[i].first);
		read(p[i].second);
	}
	for (int i=1;i<=n;++i)
		for (int j=i+1;j<=n;++j)
			e[++cnt]=(edge){i,j,dist(p[i],p[j])};
	sort(e+1,e+1+cnt);
	for (int i=1;i<=n;++i)
		fa[i]=i;
	for (int i=1;i<=cnt;++i)
	{
		int p=get_fa(e[i].x),q=get_fa(e[i].y);
		if (p==q)
			continue;
		if (sqrt(e[i].d)<(double)(max(a[p],a[q])))
		{
			ans+=sqrt(e[i].d);
			ans-=max(a[p],a[q]);
			if (a[p]>a[q])
				fa[p]=q;
			else
				fa[q]=p;
		}
	}
	printf("%.2lf",ans);
	return 0;
}