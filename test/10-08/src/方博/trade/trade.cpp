#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=40005;
int n,t;
int head[N],nxt[N*2],tail[N*2];
int dep[N],f[N];
inline void addto(int x,int y)
{
	nxt[++t]=head[x];
	head[x]=t;
	tail[t]=y;
}
int dfs(int k,int fa,int d)
{
	dep[k]=d;f[k]=fa;
	for(int i=head[k];i;i=nxt[i]){
		int v=tail[i];
		if(v==fa)continue;
		dfs(v,k,d+1);	
	}
}
struct xxx{
	int x,y,e;
}a[N];
int vis[N];
int ans[N];
int p,q;
int main()
{
	freopen("trade.in","r",stdin);
	freopen("trade.out","w",stdout);
	n=read();p=read();q=read();
	for(int i=1;i<n;i++){
		int x=read(),y=read();
		addto(x,y);
		addto(y,x);
	}
	dfs(1,1,1);
	for(int i=1;i<=p;i++)
		a[i].x=read(),a[i].y=read(),a[i].e=read();
	for(int i=1;i<=q;i++){
		int x=read(),y=read(),k=read();
		if(dep[x]<dep[y])swap(x,y);
		while(dep[x]>dep[y])vis[x]=i,x=f[x];
		while(x!=y){
			vis[x]=i;vis[y]=i;
			x=f[x],y=f[y];
		}
		vis[x]=i;
		int cnt=0;
		for(int j=1;j<=p;j++)
			if(vis[a[j].x]==i&&vis[a[j].y]==i)
				ans[++cnt]=a[j].e;
//		cout<<'!'<<cnt<<endl;
		sort(ans+1,ans+cnt+1);
		writeln(ans[k]);
	}
}
