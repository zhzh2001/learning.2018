#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=5005;
struct xxx{
	ll x,y,w;
}a[N];
double f[N];
int n;
inline double get(int x,int y)
{
	return sqrt((a[x].x-a[y].x)*(a[x].x-a[y].x)+(a[x].y-a[y].y)*(a[x].y-a[y].y));
}
bool vis[N];
int main()
{
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)
		a[i].w=read();
	for(int i=1;i<=n;i++)
		a[i].x=read(),a[i].y=read();
	for(int i=1;i<=n;i++)
		f[i]=a[i].w;
/*	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++)
			printf("	%lf",get(i,j));
		cout<<endl;
	}*/
	memset(vis,false,sizeof(vis));
	for(int i=1;i<=n;i++){
		int k=-1;
		for(int j=1;j<=n;j++)
			if(!vis[j]&&(k==-1||f[j]<f[k]))k=j;
		vis[k]=true;
		for(int j=1;j<=n;j++)
			if(!vis[j])f[j]=min(f[j],get(k,j));
	}
/*	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++)
			if(f[i]-f[j]==get(i,j))cout<<i<<"->"<<j<<endl;*/
	double ans=0;
	for(int i=1;i<=n;i++)
		ans+=f[i];
	printf("%.2lf",ans);
}
