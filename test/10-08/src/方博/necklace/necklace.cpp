#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=50005;
const int M=200005;
int f[N],n,m,k,tmp,now,cnt,ans;
struct xxx{
	int u,v,c,w;
}e[M];
inline int gf(int k){
	if(k!=f[k])f[k]=gf(f[k]);
	return f[k];
}
inline bool cmp(xxx a,xxx b){
	if(a.w==b.w)return a.c<b.c;
	else return a.w<b.w;
}
int main()
{
	freopen("necklace.in","r",stdin);
	freopen("necklace.out","w",stdout);
	n=read();m=read();k=read();
	for(register int i=1;i<=m;i++){
		e[i].u=read();e[i].v=read();e[i].w=read();e[i].c=read();
		e[i].u++,e[i].v++;
	}
	ll l=-101,r=101;
	while(l<=r){
		int mid=(l+r)>>1;
		now=0,tmp=0,cnt=0;
		for(register int i=1;i<=n+1;++i)f[i]=i;
		for(register int i=1;i<=m;++i)
			if(e[i].c==0)e[i].w+=mid;
		sort(e+1,e+m+1,cmp);
		for(register int i=1;cnt!=n-1;++i){
			int x=gf(e[i].u),y=gf(e[i].v);
			if(x!=y){
				f[x]=y;
				cnt++;
				if(e[i].c==0)tmp++;
				now+=e[i].w;
			}
		}
		for(register int i=1;i<=m;++i)
			if(e[i].c==0)e[i].w-=mid;
		if(tmp>=k){
			l=mid+1;
			ans=now-k*mid;
		}else r=mid-1;
	}
	writeln(ans);
	return 0;
}
