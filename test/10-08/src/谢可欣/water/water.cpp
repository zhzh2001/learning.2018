#include <iostream>
#include <cstdio>
#include <queue>
#include <cmath>
#define int long long
#define put putchar('\n')
using namespace std;
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}
inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}
double ans;
int n,m,s,sum,dis[200000],x[200005],y[200005],vis[200005],w[200005];
struct arr{
	double d; int rt;
    bool operator <(const arr &b) const{
        return d > b.d;
    }
};
priority_queue<arr>q;
double js(int i,int j){return sqrt((x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j]));}
void calc(int s)
{
	for(int i=1;i<=n;i++) {
		if(!vis[i]){
			q.push((arr){js(i,s),i});
		}
	}
	while(!q.empty()){
		arr u=q.top();q.pop();
		if(vis[u.rt])continue;
		if(u.d>w[u.rt]){
			ans+=w[u.rt];
//			cout<<"zzz";
		}
		else ans+=u.d;
//		printf("%.2lf\n",ans);
		vis[u.rt]=1;sum++;
//		if(sum==n)break;
		for(int i=1;i<=n;i++){
			if(!vis[i]){
				q.push((arr){js(i,u.rt),i});
			}
		}
	}
}
signed main(){
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
	int mn=100000000000,k;n=read();
	for(int i=1;i<=n;i++)w[i]=read();
	for(int i=1;i<=n;i++)x[i]=read(),y[i]=read();
	/*for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++)cout<<js(i,j)<<" ";puts("");
	}*/
	for(int i=1;i<=n;i++){
		if(w[i]<mn){
			mn=w[i]; k=i;
		}
	}
//	cout<<mn<<" "<<k<<endl;
//	sum=1;
	ans+=mn; vis[k]=1;
	calc(k);
	printf("%.2lf",ans);
//	for(int i=1;i<=n;i++)wri(dis[i]);
	return 0;
}
