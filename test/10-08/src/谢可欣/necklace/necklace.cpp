#include <algorithm>
#include <iostream>
#include <cstdio>
using namespace std;
inline int read(){int tot=1;char c=getchar();while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
int n,m,k,cnt,ans,sum,tot;
int black[30][30];
int father[100005],tmp[100005],use[100005],as[100005],dele[100005];
struct arr
{
    int u,w,y,c;
}a[600005];
bool cmp(arr a,arr b){ return a.w<b.w;}
int find(int x)
{
    if(father[x]!=x)father[x]=find(father[x]);
    return father[x];
}
void kruskal()
{
    for(int i=1;i<=n;i++) father[i]=i;
    sort(a+1,a+1+m,cmp);
    for(int i=1;i<=m;i++)
    {
        int u=a[i].u,y=a[i].y,w=a[i].w,c=a[i].c;
        if(sum==k&&!c)continue;
        int uu=find(u),yy=find(y);
        if(uu!=yy)
        {
        	if(!c)sum++;
        	if(c){
        		tmp[++tot]=i;
//				black[u][y]=i;
//				black[y][u]=i;
			}
            cnt++;
            ans+=w;
            father[yy]=uu;
            use[i]=1;
        }
        if(cnt==n-1)break;
    }
//	if(cnt<n-1) return -1;
//    return ans;
}
int main()
{
	freopen("necklace.in","r",stdin);
	freopen("necklace.out","w",stdout);
	n=read(),m=read(),k=read();
	for(int i=1;i<=m;i++){
		a[i].u=read(),a[i].y=read(),a[i].w=read(),a[i].c=read();
	}
	kruskal();
//	cout<<ans<<endl;
	if(sum<k){
//		cout<<sum<<" "<<k<<endl;
		int sz=0;
		for(int i=1;i<=m;i++){
			if(use[i]==0&&!a[i].c){
				if(black[a[i].u][a[i].y]&&!dele[black[a[i].u][a[i].y]])as[++sz]=a[i].w-a[black[a[i].u][a[i].y]].w,dele[black[a[i].u][a[i].y]]=1;
				else {
					while(dele[tmp[tot]])tot--;
					as[++sz]=a[i].w-a[tot].w;
					tot--;
				}
			}
		}
		sort(as+1,as+1+sz);
//		for(int i=1;i<=sz;i++)cout<<as[i]<<" ";puts("");
		for(int i=1;i<=k-sum;i++)ans+=as[i];
	}
	cout<<ans<<endl;
}
