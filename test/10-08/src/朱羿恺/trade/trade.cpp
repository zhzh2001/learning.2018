#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
int n,p,q;
namespace Subtask1{
	const int N = 3010;
	int x,y,k,val[N];
	int tot,first[N],last[N<<1],to[N<<1];
	inline void Add(int x,int y){to[++tot]=y,last[tot]=first[x],first[x]=tot;}
	int dep[N],fa[N];
	inline void dfs(int u){
		dep[u]=dep[fa[u]]+1;
		cross(i,u) if (to[i]!=fa[u]) fa[to[i]]=u,dfs(to[i]);
	}
	vector<int>id[N];
	int flag[N];
	inline void Add(int u){if (!id[u].empty()) For(i,0,id[u].size()-1) flag[id[u][i]]++;}
	priority_queue<int>h;
	inline void solve(){
		For(i,1,p) flag[i]=0;
		if (dep[x]<dep[y]) swap(x,y);
		if (dep[x]>dep[y]) while (dep[x]>dep[y]) Add(x),x=fa[x];
		if (x!=y) while (x!=y) Add(x),Add(y),x=fa[x],y=fa[y];
		Add(x);
		while (!h.empty()) h.pop();
		For(i,1,p) 
			if (flag[i]>=2)
				if (h.size()<k) h.push(val[i]);
				else {
					int X=h.top();
					if (val[i]<X) h.pop(),h.push(val[i]);
				}
		printf("%d\n",h.top());
	}
	inline void Main(){
		For(i,1,n-1) x=read(),y=read(),Add(x,y),Add(y,x);
		dfs(1);
		For(i,1,p) x=read(),y=read(),val[i]=read(),id[x].push_back(i),id[y].push_back(i);
		while (q--) x=read(),y=read(),k=read(),solve();
	}
};
int main(){
	freopen("trade.in","r",stdin);
	freopen("trade.out","w",stdout);
	n=read(),p=read(),q=read();
	if (n<=3000) Subtask1::Main();
}
/*
6 5 8
6 1
6 2
6 3
2 5
3 4
3 1 8
4 2 6
6 3 9
3 2 9
5 3 10
3 2 2
3 6 1
1 4 2
3 5 3
1 4 1
3 5 1
4 6 1
4 2 1
*/
