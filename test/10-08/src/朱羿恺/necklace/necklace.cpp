#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int M = 1e5+10;
int n,m,k;
struct edge{int x,y,val,c;}E[M],e[M];
const int N = 5e5+10;
int f[N],sum,cnt,cnt0,x,y;
inline int Find(int x){return f[x]==x?x:f[x]=Find(f[x]);}
inline bool cmp(edge x,edge y){return x.val<y.val;}
inline bool check(int x){
	For(i,1,m) e[i]=E[i];
	For(i,1,n) f[i]=i;
	For(i,1,m) if (!e[i].c) e[i].val+=x;
	sort(e+1,e+1+m,cmp);
	sum=cnt=cnt0=0;
	For(i,1,m){
		x=Find(e[i].x),y=Find(e[i].y);
		if (x!=y){
			sum+=e[i].val,f[x]=y,cnt++,cnt0+=(e[i].c==0);
			if (cnt==n-1) break;
		}
	}
	return cnt0>=k;
}
int l,r,mid,ans;
inline void solve(){
	l=-100,r=100;
	while (l<=r) mid=l+r>>1,(check(mid))?(l=mid+1,ans=sum-mid*k):r=mid-1;
	printf("%d",ans);
}
int main(){
	freopen("necklace.in","r",stdin);
	freopen("necklace.out","w",stdout);
	n=read(),m=read(),k=read();
	For(i,1,m) E[i]=(edge){read()+1,read()+1,read(),read()};
	solve();
}
