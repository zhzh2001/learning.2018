#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 5010;
int n,cnt;
ll val[N],x[N],y[N];
struct edge{int x,y;double dis;}e[13000001];
inline bool cmp(edge a,edge b){return a.dis<b.dis;}
inline ll sqr(ll x){return x*x;}
inline double dis(ll a,ll b,ll c,ll d){return sqrt(sqr(a-c)+sqr(b-d));}
int tot,f[N];
double ans;
inline int Find(int x){return f[x]==x?x:f[x]=Find(f[x]);}
inline void calc(){
	For(i,1,n+1) f[i]=i;
	int x,y;
	For(i,1,cnt){
		x=Find(e[i].x),y=Find(e[i].y);
		if (x!=y){
			f[x]=y,ans+=e[i].dis,tot++;
			if (tot==n) break;
			//printf("%d %d\n",e[i].x,e[i].y);
		}
	}
}
int main(){
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
	n=read();
	For(i,1,n) val[i]=read(),e[++cnt]=(edge){n+1,i,val[i]};
	For(i,1,n) x[i]=read(),y[i]=read();
	For(i,1,n)
		For(j,i+1,n) e[++cnt]=(edge){i,j,dis(x[i],y[i],x[j],y[j])};
	sort(e+1,e+1+cnt,cmp);
	calc();
	printf("%0.2lf",ans);
}
