#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 5010;
int n;
double ans,dst[N],cst[N][N];
bool vis[N];
ll x[N],y[N];
inline ll sqr(ll x){return x*x;}
inline double dis(ll a,ll b,ll c,ll d){return sqrt(sqr(a-c)+sqr(b-d));}
int main(){
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
    n=read();
    for(int i=1;i<=n;i++) dst[i]=read();
    for(int i=1;i<=n;i++) x[i]=read(),y[i]=read();
    for(int i=1;i<=n;i++)
        for(int j=1;j<=n;j++) cst[i][j]=dis(x[i],y[i],x[j],y[j]);
    int k;double min_x;
    for(int i=1;i<=n;i++){
        min_x=1ll<<35;
        for(int j=1;j<=n;j++)if(!vis[j]&&dst[j]<min_x) min_x=dst[j],k=j;
        ans+=min_x,vis[k]=1;
        for(int j=1;j<=n;j++)if(!vis[j]&&cst[k][j]<dst[j]) dst[j]=cst[k][j];
    }
    printf("%0.2lf\n",ans);
}
