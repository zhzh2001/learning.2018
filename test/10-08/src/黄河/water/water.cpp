#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<ctime>
#include<cstdlib>
#include<string>
#include<queue>
#include<cmath>
#include<set>
#include<map>
#include<bitset>
#include<vector>
#define re register
#define rep(x,a,b) for (re int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (re int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (re int x=hd[a];~x;x=nx[x])
#define LL long long
#define mo 1000000007
using namespace std;
int read(){
int x=0,fh=1; char c=getchar();
  for(;!isdigit(c); c=getchar()) fh=(c=='-')?-1:fh;
  for(;isdigit(c); c=getchar()) x=(x<<3)+(x<<1)+(c^'0');
  return x*fh;
}
const int M=5010;
const double inf=999999999.0;
int n,e1,e,x[M],y[M];
double w[M][M],minc[M];
bool vis[M];
double  sqr(int x){
	return 1.0*x*x;
}
void solve(int s){
	int k;
    double min,count=0.0;
    rep(i,1,n)minc[i]=w[s][i];
    minc[s]=inf;
    rep(i,1,n-1){
        min=inf;
    //    rep(i,1,n) printf("%.2lf ",minc[i]);
      //  printf("\n");
        rep(j,1,n){
            if(minc[j]!=inf && minc[j]<min){
                min=minc[j];
                k=j;
            }
        }
        minc[k]=inf;
        vis[k]=1;
        count+=min;
        rep(j,1,n)
            if(w[k][j]<minc[j]&&!vis[j])minc[j]=w[k][j];
      //      printf("%.2lf %d\n",min,k);
    }
    printf("%.2lf\n",count);
}

int main(){
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
    n=read();
    rep(i,1,n+1)
		rep(j,1,n+1) w[i][j]=inf;
	rep(i,1,n){
		w[i][n+1]=read();
		w[n+1][i]=w[i][n+1];
	}
	rep(i,1,n) x[i]=read(),y[i]=read();
	rep(i,1,n-1){
		rep(j,i+1,n){
			w[i][j]=sqrt(sqr(x[i]-x[j])+sqr(y[i]-y[j]));
			w[j][i]=w[i][j];
		}
	}
	n++;
	memset(vis,0,sizeof(vis));
	vis[n]=1;
    solve(n);
    return 0;
}
