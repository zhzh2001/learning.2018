#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<ctime>
#include<cstdlib>
#include<string>
#include<queue>
#include<cmath>
#include<set>
#include<map>
#include<bitset>
#include<vector>
#define re register
#define rep(x,a,b) for (re int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (re int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (re int x=hd[a];~x;x=nx[x])
#define LL long long
#define mo 1000000007
#define N 100010
using namespace std;
int read(){
int x=0,fh=1; char c=getchar();
  for(;!isdigit(c); c=getchar()) fh=(c=='-')?-1:fh;
  for(;isdigit(c); c=getchar()) x=(x<<3)+(x<<1)+(c^'0');
  return x*fh;
}
int fa[N*100],n,m,k,l(-105),r(105),mid,num,ans,r1,r2,cnt,res;
struct node{
int from,to,c,v;
}t[N];

int find(int x){
	if (x!=fa[x]) fa[x]=find(fa[x]);
	return fa[x];
}
bool cmp(node a,node b){
	if (a.v==b.v) return a.c<b.c;
		else return a.v<b.v;
}
void solve(){
  sort(t+1,t+m+1,cmp);
  for (int i=1;cnt!=n-1;i++){
     r1=find(t[i].from);
	 r2=find(t[i].to);
     if (r1!=r2){
	 	fa[r1]=r2;
		cnt++;
		if (t[i].c==0) num++;
		ans+=t[i].v;
		}
   }
}
int main(){
	freopen("necklace.in","r",stdin);
	freopen("necklace.out","w",stdout);
    n=read(),m=read(),k=read();
    rep(i,1,m){
       t[i].from=read(),t[i].to=read(),t[i].v=read(),t[i].c=read();
       t[i].from++;t[i].to++;
     }
    while (l<=r){
      mid=(l+r)>>1;
      rep(i,1,m) {if (t[i].c==0) t[i].v+=mid;}
      rep(i,1,n+1) fa[i]=i;
   	  ans=0;num=0;cnt=0;
      solve();
      if (num>=k) l=mid+1,res=ans-k*mid; else r=mid-1;
      rep(i,1,m) if (t[i].c==0) t[i].v-=mid;
    }
    printf("%d\n",res);
}

