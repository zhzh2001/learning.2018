#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cmath>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline LL read(){
	LL a=0,b=1;int dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
const double inf=1e18;
LL n,x[5013],y[5013];
double dis[5013],ans;
bool vis[5013];
inline double gd(int i,int j){
	return sqrt((x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j]));
}
int main(){
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		dis[i]=read();
		vis[i]=0;
	}
	for(int i=1;i<=n;i++){
		x[i]=read();
		y[i]=read();
	}
	ans=0;
	for(int i=1;i<=n;i++){
		int k=-1;
		for(int j=1;j<=n;j++)if(!vis[j]&&(k==-1||dis[j]<dis[k]))k=j;
		vis[k]=1;
		ans+=dis[k];
		for(int j=1;j<=n;j++)if(!vis[j]&&dis[j]>gd(k,j)){
			dis[j]=gd(k,j);
		}
	}
	ans=(LL)(100*ans+0.5);
	printf("%.2lf\n",ans/100);
	return 0;
}
