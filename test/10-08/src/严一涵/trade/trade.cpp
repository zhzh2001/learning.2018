#include <iostream>
#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
int iti[40003],oti[40003],cnt=0,ans;
int n,p,q;
int x,y,z,tc;
int f[23][40003],deep[40003];
struct st{int a,b,c;}a[40003];
bool operator<(st a,st b){
	return a.c<b.c;
}
vector<int>e[40003];
void dfs(int u,int fa){
	iti[u]=++cnt;
	f[0][u]=fa;
	for(vector<int>::iterator i=e[u].begin();i!=e[u].end();i++){
		if(*i!=fa){
			deep[*i]=deep[u]+1;
			dfs(*i,u);
		}
	}
	oti[u]=cnt;
}
void lcainit(){
	for(int i=1;i<=20;i++){
		for(int j=1;j<=n;j++){
			f[i][j]=f[i-1][f[i-1][j]];
		}
	}
}
int lca(int a,int b){
	if(deep[a]<deep[b])return lca(b,a);
	for(int i=20;i>=0;i--){
		if(deep[f[i][a]]>=deep[b])a=f[i][a];
	}
	if(a==b)return a;
	for(int i=20;i>=0;i--)if(f[i][a]!=f[i][b]){
		a=f[i][a];
		b=f[i][b];
	}
	return f[0][a];
}
inline bool isup(int&x,int&y){
	return iti[x]<=iti[y]&&oti[x]>=oti[y];
}
int main(){
	freopen("trade.in","r",stdin);
	freopen("trade.out","w",stdout);
	n=read();
	p=read();
	q=read();
	for(int i=1;i<n;i++){
		x=read();
		y=read();
		e[x].push_back(y);
		e[y].push_back(x);
	}
	deep[1]=1;
	dfs(1,1);
	lcainit();
	for(int i=1;i<=p;i++){
		a[i].a=read();
		a[i].b=read();
		a[i].c=read();
	}
	sort(a+1,a+p+1);
	for(int i=1;i<=q;i++){
		x=read();
		y=read();
		z=read();
		ans=0;
		tc=lca(x,y);
		for(int j=1;j<=p;j++){
			int&aa=a[j].a,&bb=a[j].b;
			if(isup(tc,aa)&&isup(tc,bb)&&(isup(aa,x)||isup(aa,y))&&(isup(bb,x)||isup(bb,y))){
				z--;
				ans=a[j].c;
			}
			if(z==0)break;
		}
		printf("%d\n",ans);
	}
	return 0;
}
