#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
int n,m,k0,k1,ans;
struct edge{
	int a,b,w,v;
	void get(){
		a=read()+1;
		b=read()+1;
		w=read();
		v=read();
	}
}e[100003];
bool operator<(edge a,edge b){
	if(a.w!=b.w)return a.w<b.w;
	if(a.a!=b.a)return a.a<b.a;
	if(a.b!=b.b)return a.b<b.b;
	return a.v<b.v;
}
class bcj{
	private:int f[50003];
	public:void init(){
		for(int i=1;i<=n;i++)f[i]=i;
	}
	private:int getf(int v){
		return f[v]==v?v:f[v]=getf(f[v]);
	}
	public:void add(edge x){
		f[getf(x.a)]=getf(x.b);
	}
	public:bool test(edge x){
		return getf(x.a)==getf(x.b);
	}
}b1,b2,b3;
int main(){
	freopen("necklace.in","r",stdin);
	freopen("necklace.out","w",stdout);
	n=read();
	m=read();
	k0=read();
	k1=n-1-k0;
	b1.init();
	b2.init();
	for(int i=1;i<=m;i++){
		e[i].get();
		if(e[i].v){
			b1.add(e[i]);
		}else{
			b2.add(e[i]);
		}
	}
	sort(e+1,e+m+1);
	ans=0;
	b3.init();
	for(int i=1;i<=n;i++){
		if(e[i].v){
			if(!b2.test(e[i])){
				b2.add(e[i]);
				b3.add(e[i]);
				ans+=e[i].w;
				k1--;
			}
		}else{
			if(!b1.test(e[i])){
				b1.add(e[i]);
				b3.add(e[i]);
				ans+=e[i].w;
				k0--;
			}
		}
	}
	for(int i=1;i<=m;i++){
		if(!b3.test(e[i])){
			if(e[i].v){
				if(k1!=0){
					b3.add(e[i]);
					ans+=e[i].w;
					k1--;
				}
			}else{
				if(k0!=0){
					b3.add(e[i]);
					ans+=e[i].w;
					k0--;
				}
			}
		}
	}
	printf("%d\n",ans);
	return 0;
}
