//#include<iostream>
#include<stdio.h>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
using namespace std;
ifstream fin("necklace.in");
ofstream fout("necklace.out");
int fa[100003],n,m,k,temp,ans,cnt,anss;
struct edge{
	int u,v,c,w;
}e[200003];
int find(int x){
	if(x!=fa[x]) fa[x]=find(fa[x]);
	return fa[x];
}
inline bool cmp(edge a,edge b){
	if(a.w==b.w) return a.c<b.c;
	return a.w<b.w;
}
int main(){
	fin>>n>>m>>k;
	for(register int i=1;i<=m;i++){
		fin>>e[i].u>>e[i].v>>e[i].w>>e[i].c;
		e[i].u++,e[i].v++;
	}
	long long l=-110,r=110,mid;
	while(l<=r){
		mid=(l+r)>>1;
		ans=0,temp=0,cnt=0;
		for(register int i=1;i<=n+1;i++){
			fa[i]=i;
		}
		for(register int i=1;i<=m;i++){
			if(e[i].c==0){
				e[i].w+=mid;
			}
		}
		sort(e+1,e+m+1,cmp);
		for(register int i=1;cnt!=n-1;i++){
			int x=find(e[i].u),y=find(e[i].v);
			if(x!=y){
				fa[x]=y;
				cnt++;
				if(e[i].c==0){
					temp++;
				}
				ans+=e[i].w;
			}
		}
		for(register int i=1;i<=m;i++){
			if(e[i].c==0){
				e[i].w-=mid;
			}
		}
		if(temp>=k){
			l=mid+1;
			anss=ans-k*mid;
		}else{
			r=mid-1;
		}
	}
	fout<<anss<<endl;
	return 0;
}
/*

in:
2 2 1
0 1 1 1
0 1 2 0

out:
2

*/
