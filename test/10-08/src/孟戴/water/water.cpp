//#include<iostream>
#include<algorithm>
#include<math.h>
#include<fstream>
#include<iomanip>
#include<stdio.h>
using namespace std;
ifstream fin("water.in");
ofstream fout("water.out");
struct md{
	long long x,y,w;
}a[5003];
double f[5003];
int n;
bool vis[5003];
inline long long sqr(long long x){
	return x*x;
}
inline double get(int x,int y){
	return sqrt(sqr(a[x].x-a[y].x)+sqr(a[x].y-a[y].y));
}
int main(){
	fin>>n;
	for(int i=1;i<=n;i++){
		fin>>a[i].w;
	}
	for(int i=1;i<=n;i++){
		fin>>a[i].x>>a[i].y;
	}
	for(int i=1;i<=n;i++){
		f[i]=a[i].w;
	}
	for(int i=1;i<=n;i++){
		int k=-1;
		for(int j=1;j<=n;j++){
			if(!vis[j]&&(k==-1||f[j]<f[k])){
				k=j;
			}
		}
		vis[k]=true;
		for(int j=1;j<=n;j++){
			if(!vis[j]){
				f[j]=min(f[j],get(k,j));
			}
		}
	}
	double ans=0;
	for(int i=1;i<=n;i++){
		ans+=f[i];
	}
	fout<<fixed<<setprecision(2)<<ans<<endl;
	return 0;
}
/*

in:
5
16
3
15
11
0
9 -1
0 -11
1 -4
-6 1
0 0

out:
21.75

*/
