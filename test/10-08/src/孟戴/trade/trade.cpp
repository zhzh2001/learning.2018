//#include<iostream>
#include<stdio.h>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
using namespace std;
ifstream fin("trade.in");
ofstream fout("trade.out");
struct xxx{
	int x,y,e;
}a[40003];
int n,p,q;
int head[40003],nxt[80003],to[80003],tot;
int dep[40003],f[40003],vis[40003],ans[40003];
inline void add(int u,int v){
	nxt[++tot]=head[u],head[u]=tot,to[tot]=v;
}
void dfs(int k,int fa,int d){
	dep[k]=d;
	f[k]=fa;
	for(int i=head[k];i;i=nxt[i]){
		if(to[i]!=fa){
			dfs(to[i],k,d+1);
		}
	}
}
int main(){
	fin>>n>>p>>q;
	for(int i=1;i<n;i++){
		int x,y;
		fin>>x>>y;
		add(x,y),add(y,x);
	}
	dfs(1,1,1);
	for(int i=1;i<=p;i++){
		fin>>a[i].x>>a[i].y>>a[i].e;
	}
	for(int i=1;i<=q;i++){
		int x,y,k;
		fin>>x>>y>>k;
		if(dep[x]<dep[y]){
			swap(x,y);
		}
		while(dep[x]>dep[y]){
			vis[x]=i,x=f[x];
		}
		while(x!=y){
			vis[x]=i,vis[y]=i,x=f[x],y=f[y];
		}
		vis[x]=i;
		int cnt=0;
		for(int j=1;j<=p;j++){
			if(vis[a[j].x]==i&&vis[a[j].y]==i){
				ans[++cnt]=a[j].e;
			}
		}
		sort(ans+1,ans+cnt+1);
		fout<<ans[k]<<endl;
	}
	return 0;
}
/*

in:
6 5 8
6 1
6 2
6 3
2 5
3 4
3 1 8
4 2 6
6 3 9
3 2 9
5 3 10
3 2 2
3 6 1
1 4 2
3 5 3
1 4 1
3 5 1
4 6 1
4 2 1

out:
9
9
9
10
8
9
9
6

*/
