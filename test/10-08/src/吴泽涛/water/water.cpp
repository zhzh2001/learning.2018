#include<bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
using namespace std;
inline GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
inline void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
inline void writeln(GG x) {
    write(x); puts("");
}

const int N = 5011; 
const double inf = 1e10;  
struct node{
	int x, y; 
}a[N];
int n, S, Mn; 
double dist[N];
bool visit[N]; 

inline double sqr(double x) {
	return x*x; 
}

inline double calc(int s, int t) {
	return sqrt(sqr(a[s].x-a[t].x)+sqr(a[s].y-a[t].y)); 
}

inline void prim(int S) {
	For(i, 1, n-1) {
		int u = 0; 
		For(j, 1, n) 
			if(!visit[j] && dist[j]<dist[u]) u = j; 
		visit[u] = 1; 
		For(j, 1, n) 
			if(!visit[j] && calc(u, j)<dist[j] ) dist[j] = calc(u, j); 
 	}
 	double ans = 0; 
 	For(i, 1, n) ans = ans+dist[i]; 
	printf("%.2lf", ans); 
}

int main() {
	freopen("water.in","r",stdin); 
	freopen("water.out","w",stdout); 
	n = read(); 
	dist[0] = inf; 
	For(i, 1, n) {
		dist[i] = read(); 
		if(dist[i] < dist[S])  
			S = i; 
	}
	For(i, 1, n) {
		a[i].x = read(); 
		a[i].y = read(); 
	}
	prim(S); 
}


/*

5
16
3
15
11
0
9 -1
0 -11
1 -4
-6 1
0 0


*/

