#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int x=0;char c=getchar();bool p=1;
	for(;!isdigit(c);c=getchar())if(c=='-')p=0;
	for(;isdigit(c);c=getchar())x=(x<<1)+(x<<3)+(c^48);
	return p?x:-x;
}
const int N=100005;
int n,m,k,fa[N],cnt,fu,fv,ans,K;
struct node{int u,v,l,c;}z[N];
bool cmp(node a,node b){return a.l<b.l;}
int find(int x){return fa[x]==x?x:fa[x]=find(fa[x]);}
int main(){
	freopen("necklace.in","r",stdin);
	freopen("necklace.out","w",stdout);
	n=read();m=read();k=read();
	for(int i=1;i<=n;i++)fa[i]=i;
	for(int i=1;i<=m;i++)
		z[i]=(node){read()+1,read()+1,read(),read()};
	sort(z+1,z+m+1,cmp);
	cnt=1;
	for(int i=1;i<=m&&cnt<n;i++){
		if(K==k&&z[i].c==0)continue;
		if(cnt-K==n-k&&z[i].c==1)continue;
		fu=find(z[i].u);
		fv=find(z[i].v);
		if(fu!=fv){
			fa[fu]=fv;
			ans+=z[i].l;
			K+=1-z[i].c;
			cnt++;
		}
	}
	cout<<ans;
	return 0;
}
