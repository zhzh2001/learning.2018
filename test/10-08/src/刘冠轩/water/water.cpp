#include <bits/stdc++.h>
#define ll long long
#define sqr(x) (((ll)x)*((ll)x))
#define calc(x,y) sqrt(sqr(x)+sqr(y))
using namespace std;
inline int read(){
	int x=0;char c=getchar();bool p=1;
	for(;!isdigit(c);c=getchar())if(c=='-')p=0;
	for(;isdigit(c);c=getchar())x=(x<<1)+(x<<3)+(c^48);
	return p?x:-x;
}
const int N=5005;
int n,f;
double x[N],y[N],dis[N][N],dist[N],ans;
bool vis[N];
int main(){
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)dis[0][i]=read();
	for(int i=1;i<=n;i++)x[i]=read(),y[i]=read(),dist[i]=10000000;
	dist[n+1]=10000000;
	for(int i=1;i<n;i++)
		for(int j=i+1;j<=n;j++)
			dis[i][j]=dis[j][i]=calc(x[i]-x[j],y[i]-y[j]);
	for(int i=0;i<=n;i++){
		f=n+1;
		for(int j=0;j<=n;j++)
			if(!vis[j]&&dist[j]<dist[f])f=j;
		ans+=dist[f];
		vis[f]=1;
		for(int j=0;j<=n;j++)
			if(!vis[j])dist[j]=min(dist[j],dis[f][j]);
	}
	printf("%.2lf",ans);
	return 0;
}
