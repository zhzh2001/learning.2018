#include <bits/stdc++.h>
using namespace std;
int n,m,k,fa[240000];
inline int read() {
	int X=0,w=0;
	char ch=0;
	while(!isdigit(ch)) {
		w|=ch=='-';
		ch=getchar();
	}
	while(isdigit(ch)) X=(X<<3)+(X<<1)+(ch^48),ch=getchar();
	return w?-X:X;
}
int find(int x) {
	if(fa[x]==x) return x;
	else return fa[x]=find(fa[x]);
}
struct node {
	int u,v,w,c;
} sxd[240000];
bool cmp(node a,node b) {
	return a.w<b.w;
}
signed main() {
	freopen("necklace.in","r",stdin);
	freopen("necklace.out","w",stdout);
	cin>>n>>m>>k;
	for(int i=1;i<=m;i++) {
		sxd[i].u=read();
		sxd[i].v=read();
		sxd[i].w=read();
		sxd[i].c=read();
	}
	for(int i=1;i<=120000;i++) fa[i]=i;
	sort(sxd+1,sxd+m+1,cmp);
	int white=0,sum=0,qwq=0,at=1,ans=0;
	while(1) {
		if(sum==n-1) break;
		if(n-1-sum==k-white) {
			qwq=1;
			break;
		}//剩下必须全部白色
		if(white==k && sum<n-1) {
			qwq=2;
			break;
		}//剩下不许全部黑色
		node mcl=sxd[at];
		at++;
		int xx=mcl.u,yy=mcl.v;
		xx=find(xx);
		yy=find(yy);
		if(xx==yy) continue;
		sum++;
		if(mcl.c==0) white++;
		fa[xx]=yy;
		ans+=mcl.w;
	}
	if(qwq==1) {
		while(1) {
			if(sum==n-1) break;
			node mcl=sxd[at];
			at++;
			if(mcl.c==1) continue;
			int xx=mcl.u,yy=mcl.v;
			xx=find(xx);
			yy=find(yy);
			if(xx==yy) continue;
			sum++;
			fa[xx]=yy;
			ans+=mcl.w;
		}
	} else if(qwq==2) {
		while(1) {
			if(sum==n-1) break;
			node mcl=sxd[at];
			at++;
			if(mcl.c==0) continue;
			int xx=mcl.u,yy=mcl.v;
			xx=find(xx);
			yy=find(yy);
			if(xx==yy) continue;  
			sum++;
			fa[xx]=yy;
			ans+=mcl.w;
		}
	} 
	cout<<ans;
	return 0;
}