#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read() {
	int X=0,w=0;
	char ch=0;
	while(!isdigit(ch)) {
		w|=ch=='-';
		ch=getchar();
	}
	while(isdigit(ch)) X=(X<<3)+(X<<1)+(ch^48),ch=getchar();
	return w?-X:X;
}
int n,fa[1200000],cnt=-1,mn_from[1200000],head[1200000],mnat=0,aaa,num;
bool vis[1200000],not_ok[2400000];
double mn[1200000],mm,ans,xiao=0x3f3f3f3f,w[1200000];
struct node {
	int next,to;
	double w;
} sxd[2400000];
int find(int x) {
	if(fa[x]==x) return x;
	return fa[x]=find(fa[x]);
}
struct p {
	int x,y,id,w;
} point[1200000];
double count(int a,int b) {
	return sqrt((point[a].x-point[b].x) * (point[a].x-point[b].x) + (point[a].y-point[b].y) * (point[a].y-point[b].y));
}
void add(int u,int v,double w) {
	sxd[++cnt].next=head[u];
	sxd[cnt].to=v;
	sxd[cnt].w=w;
	head[u]=cnt;
}
void dfs(int at,double mx,int from,int mx_at,int fa) {
//	cout<<"DFS-> "<<at<<" "<<mx<<" "<<from<<" "<<mx_at<<" "<<fa<<'\n';
	if(vis[at]) {
		double real=point[from].w;
		if(mx>real) {
//			cout<<"FOUND!\n";
//			printf("%lf %lf\n",mx,real);
			not_ok[mx_at]=true;
			not_ok[mx_at^1]=true;
			vis[from]=1;
			ans-=mx;
			ans+=real;
			return;
		}
	}
	for(int i=head[at]; i!=-1; i=sxd[i].next) {
		if(sxd[i].to==fa) continue;
		if(not_ok[i]) continue;
		double old1=mx,old2=mx_at;
		if(sxd[i].w>mx) {
			mx=sxd[i].w;
			mx_at=i;
		}
		dfs(sxd[i].to,mx,from,mx_at,at);
		mx=old1;
		mx_at=old2;
	}
}
signed main() {
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
	n=read();
	mm=0x3f3f3f3f;
	memset(head,-1,sizeof head);
	for(int i=1; i<=n; i++) {
		point[i].w=read();
		if(point[i].w<mm) {
			mm=point[i].w;
			mnat=i;
		}
	}
	for(int i=1; i<=n; i++) {
		point[i].x=read();
		point[i].y=read();
	}
	for(int i=1; i<=n; i++) mn[i]=0x3f3f3f3f;
//	cout<<"mnat="<<mnat<<'\n';
	mn[mnat]=0;
	aaa=mnat;
	num=0;
	ans=mm;
	while(num<n) {
		xiao=1e9,mnat=0;
		for(int i=1; i<=n; i++) {
			if(mn[i]<xiao && vis[i]==0) {
				xiao=mn[i];
				mnat=i;
			}
		}
//		cout<<"xiao && mnat ="<<xiao<<" "<<mnat<<'\n';
		ans+=xiao;
		if(num) {
			add(mnat,mn_from[mnat],xiao);
			add(mn_from[mnat],mnat,xiao);
//			cout<<"ADD->"<<mnat<<" "<<mn_from[mnat]<<'\n';
		}
		num++;
		vis[mnat]=1;
		for(int i=1; i<=n; i++)
			if(vis[i]==0 && mn[i]>count(i,mnat)) {
				mn[i]=count(i,mnat);
				mn_from[i]=mnat;
			}
//		cout<<"mn->";
//		for(int i=1;i<=n;i++) cout<<mn[i]<<" ";
//		cout<<"mn<-\n";
	}
//	cout<<"ans="<<ans<<'\n';
	memset(vis,0,sizeof vis);
	vis[aaa]=1;
	for(int i=1; i<=n; i++) {
		if(vis[i]==0) {
//			cout<<"WKY->QQQ "<<i<<'\n';
			dfs(i,0.0,i,0,0);
		}
	}
	printf("%.2lf",ans);
	return 0;
}
