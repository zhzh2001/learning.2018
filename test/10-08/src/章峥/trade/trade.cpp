#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("trade.in");
ofstream fout("trade.out");
const int N=40005,M=80005,L=18;
int head[N],v[M],nxt[M],e;
inline void add_edge(int u,int v)
{
	::v[++e]=v;
	nxt[e]=head[u];
	head[u]=e;
}
int into[N],outo[N],t,f[N][L],dep[N];
void dfs(int k)
{
	into[k]=++t;
	for(int i=head[k];i;i=nxt[i])
		if(v[i]!=f[k][0])
		{
			f[v[i]][0]=k;
			dep[v[i]]=dep[k]+1;
			dfs(v[i]);
		}
	outo[k]=t;
}
int lca(int x,int y)
{
	if(dep[x]<dep[y])
		swap(x,y);
	int delta=dep[x]-dep[y];
	for(int i=0;delta;delta/=2,i++)
		if(delta&1)
			x=f[x][i];
	if(x==y)
		return x;
	for(int i=L-1;i>=0;i--)
		if(f[x][i]!=f[y][i])
		{
			x=f[x][i];
			y=f[y][i];
		}
	return f[x][0];
}
struct BIT
{
	int tree[N];
	void modify(int x,int val)
	{
		for(;x<=p;x+=x&-x)
			tree[x]+=val;
	}
	int query(int x)
	{
		int ans=0;
		for(;x;x-=x&-x)
			ans+=tree[x];
		return ans;
	}
}T;
int main()
{
	int n,p,q;
	fin>>n>>p>>q;
	for(int i=1;i<n;i++)
	{
		int u,v;
		fin>>u>>v;
		add_edge(u,v);
		add_edge(v,u);
	}
	dfs(1);
	for(int j=1;j<L;j++)
		for(int i=1;i<=n;i++)
			f[i][j]=f[f[i][j-1]][j-1];
	while(p--)
	{
		int u,v,c;
		fin>>u>>v>>c;
		
	}
	return 0;
}
