#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("necklace.in");
ofstream fout("necklace.ans");
const int N=20,M=100005,INF=1e9;
int n,m,k,f[N][N],ans;
bool cho[M],tmp[M],best[M];
struct edge
{
	int u,v,w,c;
	bool operator<(const edge& rhs)const
	{
		return w<rhs.w;
	}
}e[M];
int getf(int now,int x)
{
	return f[now][x]==x?x:f[now][x]=getf(now,f[now][x]);
}
void dfs(int now,int start,int ans)
{
	if(ans>::ans)
		return;
	if(now==k+1)
	{
		for(int i=1;i<=n;i++)
			f[now][i]=f[now-1][i];
		copy(cho+1,cho+m+1,tmp+1);
		for(int i=1;i<=m;i++)
			if(e[i].c&&getf(now,e[i].u)!=getf(now,e[i].v))
			{
				f[now][getf(now,e[i].u)]=getf(now,e[i].v);
				ans+=e[i].w;
				tmp[i]=true;
			}
		if(ans<(::ans))
		{
			::ans=ans;
			copy(tmp+1,tmp+m+1,best+1);
		}
	}
	else
		for(int i=start;i<=m;i++)
			if(!e[i].c&&getf(now-1,e[i].u)!=getf(now-1,e[i].v))
			{
				for(int i=1;i<=n;i++)
					f[now][i]=f[now-1][i];
				f[now][getf(now,e[i].u)]=getf(now,e[i].v);
				cho[i]=true;
				dfs(now+1,i+1,ans+e[i].w);
				cho[i]=false;
			}
}
int main()
{
	fin>>n>>m>>k;
	for(int i=1;i<=m;i++)
	{
		fin>>e[i].u>>e[i].v>>e[i].w>>e[i].c;
		e[i].u++;
		e[i].v++;
	}
	for(int i=1;i<=n;i++)
		f[0][i]=i;
	sort(e+1,e+m+1);
	ans=INF;
	dfs(1,1,0);
	fout<<ans<<endl;
	for(int i=1;i<=m;i++)
	{
		fout<<e[i].u<<' '<<e[i].v<<' '<<e[i].w<<' '<<e[i].c;
		if(best[i])
			fout<<"!\n";
		else
			fout<<endl;
	}
	return 0;
}
