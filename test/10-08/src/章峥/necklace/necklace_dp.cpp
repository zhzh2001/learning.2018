#include<fstream>
#include<algorithm>
#include<cstring>
using namespace std;
ifstream fin("necklace.in");
ofstream fout("necklace.out");
const int N=20,INF=0x3f3f3f3f;
int dp[N][N],mat0[N][N],mat1[N][N];
bool vis[N];
int main()
{
	int n,m,k;
	fin>>n>>m>>k;
	memset(mat0,0x3f,sizeof(mat0));
	memset(mat1,0x3f,sizeof(mat1));
	memset(dp,0x3f,sizeof(dp));
	while(m--)
	{
		int u,v,w,c;
		fin>>u>>v>>w>>c;
		u++;v++;
		if(c)
		{
			if(w<mat1[u][v])
				mat1[u][v]=mat1[v][u]=w;
		}
		else
		{
			if(w<mat0[u][v])
				mat0[u][v]=mat0[v][u]=w;
		}
	}
	dp[1][0]=0;
	for(int i=0;i<=k;i++)
	{
		memset(vis,0,sizeof(vis));
		for(;;)
		{
			int t=0;
			for(int j=1;j<=n;j++)
				if(!vis[j]&&dp[j][i]<dp[t][i])
					t=j;
			if(!t)
				break;
			vis[t]=true;
			for(int j=1;j<=n;j++)
				if(!vis[j])
				{
					dp[j][i]=min(dp[j][i],mat1[t][j]);
					dp[j][i+1]=min(dp[j][i+1],mat0[t][j]);
				}
		}
	}
	int ans=0;
	for(int i=1;i<=n;i++)
		ans+=dp[i][k];
	fout<<ans<<endl;
	return 0;
}
