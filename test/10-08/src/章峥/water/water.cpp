#include<fstream>
#include<cmath>
#include<algorithm>
using namespace std;
ifstream fin("water.in");
ofstream fout("water.out");
const int N=5005;
const double INF=1e100;
double w[N];
int x[N],y[N];
bool vis[N];
inline double dis(double x,double y)
{
	return sqrt(x*x+y*y);
}
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
		fin>>w[i];
	for(int i=1;i<=n;i++)
		fin>>x[i]>>y[i];
	double ans=.0;
	w[0]=INF;
	for(int i=1;i<=n;i++)
	{
		int j=0;
		for(int k=1;k<=n;k++)
			if(!vis[k]&&w[k]<w[j])
				j=k;
		vis[j]=true;
		ans+=w[j];
		for(int k=1;k<=n;k++)
			if(!vis[k])
				w[k]=min(w[k],dis(x[j]-x[k],y[j]-y[k]));
	}
	fout.precision(2);
	fout<<fixed<<ans<<endl;
	return 0;
}
