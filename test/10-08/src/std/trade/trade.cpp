#include<cstdio>
#include<algorithm>
#include<cstring>
#define ps puts("")
#define fi first
#define nd second
#define mset(x) memset((x), 0, sizeof (x))
#define mk make_pair
#define sqr(x) ((x)*(x))
#define pii pair<int,int>
using namespace std;
typedef long long ll;
inline ll read() {
    ll x = 0; char f = 1, ch = getchar(); while (ch < '0' || ch > '9') {
        if (ch == '-')f = -1;
        ch = getchar();
    }while (ch >= '0' && ch <= '9') { x = x * 10 + ch - '0'; ch = getchar(); }return x * f;
}
void write(ll x) { if (x < 0) x = -x, putchar('-'); if (x > 9) write(x / 10); putchar(x % 10 + '0'); }
inline void writeln(ll x) { write(x); puts(""); }
const int N = 90000;
int n, P, Q;
int head[N], nxt[N], en, ver[N];
void add(int x, int y) {
    ver[++en] = y, nxt[en] = head[x], head[x] = en;
    ver[++en] = x, nxt[en] = head[y], head[y] = en;
}
int ls[N], rs[N], num, dep[N], f[N][21];
void dfs(int x, int F) {
    ls[x] = ++num;
    dep[x] = dep[F] + 1;
    for(int i = head[x]; i; i = nxt[i]) {
        int y = ver[i];
        if(y == F) continue;
        f[y][0] = x;
        for(int j = 1; j <= 17; ++j)
            f[y][j] = f[f[y][j - 1]][j - 1];
        dfs(y, x);
    }
    rs[x] = num;
}
int LCA(int x, int y) {
    if(dep[x] > dep[y]) swap(x, y);
    for(int i = 17; i >= 0; --i)
        if(dep[f[y][i]] > dep[x])
            y = f[y][i];
    if(x == f[y][0]) return y;
    for(int i = 17; i >= 0; --i)
        if(f[x][i] != f[y][i])
            x = f[x][i], y = f[y][i];
    return x;
}
struct Node {
    int l1, l2, r1, r2, val;
}p[N * 4];
struct Query{
    int x, y, k, i;
}a[N * 2], lt[N * 2], rt[N * 2];
bool cmpv(Node x, Node y) {
    return x.val < y.val;
}
struct BIT {
    int c[N];
    void add(int x,  int z) {
        for(; x <= n; x += x & (-x))
            c[x] += z; 
    }
    int ask(int x) {
        int res = 0;
        for(; x; x -= x & (-x))
            res += c[x];
        return res;
    }
}t;
int ans[N];
struct E {
    int x, l,r, v, i;
}b[N * 4];
bool cmpx(E x, E y) {
    if(x.x != y.x) return x.x < y.x;
    return x.v > y.v;
} 
int sum[N * 4];
void solve(int l, int r, int L, int R){
    if(L == R) {
        for(int i = l; i <= r; ++i)
            ans[a[i].i] = p[L].val;
        return; 
    }   
    if(l > r) return;
    int mid = (L + R) >> 1;
    int tot = 0;
    for(int i = L; i <= mid; ++i) {
        b[++tot] = (E){p[i].l1, p[i].r1, p[i].r2, 1, 0};
        b[++tot] = (E){p[i].l2, p[i].r1, p[i].r2, -1, 0};
    }
    for(int i = l; i <= r; ++i) b[++tot] = (E){a[i].x, a[i].y, 0, 0, i};
    sort(b + 1, b + 1 + tot, cmpx);
    for(int i = 1; i <= tot; ++i)
        if(b[i].v) t.add(b[i].l, b[i].v), t.add(b[i].r + 1, -b[i].v);
        else sum[b[i].i] = t.ask(b[i].l);
    int t1 = 0, t2 = 0;
    for(int i = l;i <= r;++i)
        if(sum[i] >= a[i].k) lt[++t1] = a[i];
        else a[i].k -= sum[i], rt[++t2] = a[i];
    int tt = l;
    for(int i = 1; i <= t1; ++i)
        a[tt++] = lt[i];
    for(int i = 1; i <= t2; ++i)
        a[tt++] = rt[i];
    solve(l, l + t1 - 1, L, mid);
    solve(l + t1, r, mid + 1, R);
}
int main() {
	freopen("trade.in","r",stdin);
	freopen("trade.out","w",stdout); 
    n = read(), P = read(), Q = read();
    for(int i = 1; i < n; ++i)
        add(read(), read());
    dfs(1, 0);
    int cnt = 0; 
    for(int i = 1; i <= P; ++i) {
        int x = read(), y = read(), z = read();
        if(dep[x] > dep[y]) swap(x, y);
        int t = LCA(x, y);
        if(f[t][0] == x) {
            p[++cnt] = (Node){ls[y], rs[y], 1, ls[t] - 1, z};
            p[++cnt] = (Node){ls[y], rs[y], rs[t] + 1, n, z};
            p[++cnt] = (Node){1, ls[t] - 1, ls[y], rs[y], z};
            p[++cnt] = (Node){rs[t] + 1, n,ls[y], rs[y],  z};           
        } else {
            p[++cnt] = (Node){ls[x], rs[x], ls[y], rs[y], z};
            p[++cnt] = (Node){ls[y], rs[y], ls[x], rs[x], z};
        }
    }
    for(int i = 1; i <= Q; ++i) {
        int x = read(), y = read(), z = read();
        a[i] = (Query){ls[x], ls[y], z, i};
    }
    sort(p + 1, p + 1 + cnt, cmpv);
    solve(1, Q, 1, cnt);
    for(int i = 1; i <= Q; ++i)
        writeln(ans[i]);
    return 0;
}
