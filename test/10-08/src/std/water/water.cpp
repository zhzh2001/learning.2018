#include<bits/stdc++.h>
#define ps puts("")
#define fi first
#define nd second
#define mset(x) memset((x), 0, sizeof (x))
#define mk make_pair
#define sqr(x) ((x)*(x))
#define pii pair<int,int>
#define exp 1e-10
using namespace std;
typedef long long ll;
ll read() {ll x = 0;char f = 1, ch = getchar();while(ch < '0' || ch > '9') {if(ch == '-')f = -1;
ch = getchar();}while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * f;}
void write(ll x) {if(x < 0) x = -x, putchar('-');if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
char IN[10],OUT[10];
const int N = 5100;
ll w[N];
ll x[N], y[N];
double mincost[N];
bool vis[N];
double dst(ll x1, ll y1, ll x2, ll y2) {
	return sqrt(sqr(x1 - x2) + sqr(y1 - y2));
}
void solve() {
	memset(vis, 0, sizeof vis);
	int n = read();
	for(int i = 1; i <= n; ++i)
		w[i] = read();
	for(int i = 1; i <= n; ++i)
		x[i] = read(), y[i] = read();
	mincost[0] = 1e17;
	for(int i = 1; i <= n; ++i) mincost[i] = w[i];
	double ans = 0;
	for(int i = 1; i <= n; ++i) {
		int p = 0;
		for(int j = 1; j <= n; ++j) if(!vis[j] && mincost[j] <= mincost[p]) p = j;
		vis[p] = 1;
		ans += mincost[p];
		for(int j = 1; j <= n; ++j) mincost[j] = min(mincost[j], dst(x[p], y[p], x[j], y[j]));
	}
	printf("%.2lf\n", ans);
}
int main() {
	freopen("water.in", "r", stdin);
	freopen("water.out", "w", stdout);
	solve();
	return 0;
}
