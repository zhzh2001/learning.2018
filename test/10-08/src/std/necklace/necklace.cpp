#include<bits/stdc++.h>
#define ll long long
ll read() {ll x = 0;char f = 1, ch = getchar();while(ch < '0' || ch > '9') {if(ch == '-')f = -1;
ch = getchar();}while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * f;}
void write(ll x) {if(x < 0) x = -x, putchar('-');if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
using namespace std;
const int N = 50100, M = 110000;
int n, m, ned;
 
struct Edge{
    int u , v, w, id;
    bool operator < (const Edge & rhs) const {
        return w < rhs.w || w == rhs.w && id < rhs.id;
    }
}e[M];
int fa[N],res;
int get(int x) {
    return fa[x] == x ? x : fa[x] = get(fa[x]);
}
bool check(int mid ) {
    for(int i = 1; i <= m; ++i)
        if(e[i].id == 0)
            e[i].w += mid;
    sort(e + 1, e + 1 + m);
    for(int i = 1; i <= n; ++i) fa[i] = i;
    int cnt = 1, tot = 0;
    res = 0;
    for(int i = 1;  i<= m && cnt < n; ++i) {
        if(get(e[i].u) != get(e[i].v)) {
            res += e[i].w;
            ++cnt;
            fa[get(e[i].u)] = get(e[i].v);
            if(e[i].id == 0)++tot;
        }
    }
    for(int i = 1; i <= m; ++i)
        if(e[i].id == 0)
            e[i].w -= mid;
    return tot >= ned;
}
int main() {
	freopen("necklace.in","r", stdin);
	freopen("necklace.out", "w", stdout);
    n = read(), m = read(), ned = read();
    for(int i = 1; i <= m; ++i)
        e[i].u = read() + 1,e[i].v = read() + 1, e[i].w = read(), e[i].id = read();
    int l = -100,r = 101;
	while(r - l > 1) {
        int mid = (l + r) >> 1;
        if(check(mid))l = mid;
        else r = mid;
    }
    check(l);
    writeln(res - l * ned);
    return 0;
}
