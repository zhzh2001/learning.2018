#include <algorithm>
#include <iostream>
#include <cstring>
#include <cstdio>

#define Max 100005

using namespace std;

inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
    int x=0;char ch=gc();bool positive=1;
    for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
    for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
    return positive?x:-x;
}

inline void write(int x){
    if(x<0)x=-x,putchar('-');
    if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
    write(x);puts("");
}

struct Edge{
    int v,to,w,c;
}e[Max*2];

struct Node{
    int l,r,w;
    inline bool operator<(const Node&x)const{
        return w<x.w;
    }
}E[Max];

int l,r,w,c,n,m,k,ans,size,fa[Max],use[Max],dis[Max],head[Max];
bool vis[Max];

inline void add(int u,int v,int w,int c){
    e[++size].v=v;e[size].to=head[u];e[size].w=w;e[size].c=c;head[u]=size;
}

inline int calc(){
    memset(dis,0x7f,sizeof dis);
    memset(vis,0,sizeof vis);
    dis[0]=0;
    int sum=0;
    for(int i=1;i<=n;i++){
        int minn=1e9,u;
        for(int j=0;j<n;j++){
            if(!vis[j]&&dis[j]<minn){
                minn=dis[j];
                u=j;
            }
        }
        if(minn==1e9)return 1e9;
        sum+=dis[u];
        vis[u]=true;
        for(int j=head[u];j!=-1;j=e[j].to){
            int v=e[j].v;
            if(use[v]){
                if(!e[j].c){
                    dis[v]=min(dis[v],e[j].w);
                }
            }else{
                dis[v]=min(dis[v],e[j].w);
            }
        }
    }
    return sum;
}

inline void dfs(int x){
    if(x==n){
        int sum=0;
        for(int i=1;i<n;i++)sum+=use[i];
        if(sum==k){
            // for(int i=1;i<n;i++)cout<<use[i]<<" ";cout<<endl;
            ans=min(ans,calc());
        }
        return;
    }
    use[x]=1;
    dfs(x+1);
    use[x]=0;
    dfs(x+1);
    return;
}

inline int find(int x){
    return fa[x]==x?x:fa[x]=find(fa[x]);
}

int main(){
    freopen("necklace.in","r",stdin);
    freopen("necklace.out","w",stdout);
    n=read();
    m=read();
    k=read();
    memset(head,-1,sizeof head);
    for(int i=1;i<=m;i++){
        l=read();
        r=read();
        w=read();
        c=read();
        add(l,r,w,c);
        add(r,l,w,c);
        E[i].l=l;
        E[i].r=r;
        E[i].w=w;
    }
    if(n<=20){
        ans=1e9;
        dfs(0);
    }else{
        ans=0;
        sort(E+1,E+m+1);
        for(int i=0;i<n;i++)fa[i]=i;
        for(int i=1;i<=m;i++){
            l=E[i].l;r=E[i].r;
            if(find(l)!=find(r)){
                fa[find(l)]=find(r);
                ans+=E[i].w;
            }
        }
    }
    writeln(ans);
    return 0;
}
