#include <iostream>
#include <cstdio>
#include <queue>

#define Max 500005

using namespace std;

inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
    int x=0;char ch=gc();bool positive=1;
    for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
    for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
    return positive?x:-x;
}

inline void write(int x){
    if(x<0)x=-x,putchar('-');
    if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
    write(x);puts("");
}

struct Node{
    int l,r,k;
}a[Max],b[Max];

struct Edge{
    int v,to;
}e[Max*2];

int n,p,q,l,r,k,ans,size,dep[Max],f[Max][20],head[Max];

priority_queue<int>que;

inline void add(int u,int v){
    e[++size].v=v;e[size].to=head[u];head[u]=size;
}

inline void dfs(int u,int fa){
    f[u][0]=fa;
    dep[u]=dep[fa]+1;
    for(int i=head[u];i;i=e[i].to){
        int v=e[i].v;
        if(v==fa)continue;
        dfs(v,u);
    }
    return;
}

inline int LCA(int x,int y){
    if(dep[x]<dep[y])swap(x,y);
    for(int i=19;i>=0;i--){
        if(dep[f[x][i]]>=dep[y]){
            x=f[x][i];
        }
    }
    for(int i=19;i<=0;i--){
        if(f[x][i]!=f[y][i]){
            x=f[x][i];
            y=f[y][i];
        }
    }
    return x==y?x:f[x][0];
}

inline bool check(int l,int r,int L,int R){
    int x,y,a,b,z;
    x=LCA(l,r);
    a=dep[l]+dep[r]-dep[x]+1;
    y=LCA(L,R);
    b=dep[L]+dep[R]-dep[y]+1;
    if(b>a)return false;
    if(max(dep[L],dep[R])>max(dep[l],dep[r]))return false;
    if(min(dep[L],dep[R])<dep[x])return false;
    // return true;
    /*if(y==L){
        x=LCA(L,l);
        y=LCA(L,r);
        if(x!=L&&y!=L)return false;
    }else{
        if(y==R){
            x=LCA(R,l);
            y=LCA(R,r);
            if(x!=R&&y!=R)return false;
        }else{
            if(dep[y]<dep[x])return false;
        }
    }*/
    y=LCA(l,r);
    z=LCA(R,L);
    x=LCA(l,L);
    if(x!=l&&x!=L&&x!=y&&x!=z)return false;
    x=LCA(l,R);
    if(x!=l&&x!=R&&x!=y&&x!=z)return false;
    x=LCA(r,L);
    if(x!=r&&x!=L&&x!=y&&x!=z)return false;
    x=LCA(r,R);
    if(x!=r&&x!=R&&x!=y&&x!=z)return false;
    return true;
}

int main(){
    freopen("trade.in","r",stdin);
    freopen("trade.out","w",stdout);
    n=read();p=read();q=read();
    for(int i=1;i<n;i++){
        l=read();
        r=read();
        add(l,r);
        add(r,l);
    }
    dfs(1,0);
    f[1][0]=1;
    for(int i=1;i<=19;i++){
        for(int j=1;j<=n;j++){
            f[j][i]=f[f[j][i-1]][i-1];
        }
    }
    for(int i=1;i<=p;i++){
        a[i].l=read();
        a[i].r=read();
        a[i].k=read();
    }
    for(int i=1;i<=q;i++){
        l=read();r=read();k=read();
        for(int j=1;j<=p;j++){
            if(check(l,r,a[j].l,a[j].r)){
                que.push(a[j].k);
                // cout<<j<<" ";
            }
        }
        // cout<<endl;
        while(k--){
            if(que.empty())break;
            ans=que.top();
            que.pop();
        }
        while(!que.empty())que.pop();
        writeln(ans);
    }
    return 0;
}
