#include <iostream>
#include <cstring>
#include <cstdio>
#include <cmath>

#define ll long long
#define Max 5005

using namespace std;

inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline ll read(){
    ll x=0;char ch=gc();bool positive=1;
    for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
    for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
    return positive?x:-x;
}

inline void write(ll x){
    if(x<0)x=-x,putchar('-');
    if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(ll x){
    write(x);puts("");
}

ll n,w[Max],vis[Max],x[Max],y[Max];
double ans,dis[Max];

inline double calc(ll a,ll b){
    return sqrt((x[a]-x[b])*(x[a]-x[b])+(y[a]-y[b])*(y[a]-y[b]));
}

int main(){
    freopen("water.in","r",stdin);
    freopen("water.out","w",stdout);
    n=read();
    for(ll i=1;i<=n;i++)w[i]=read();
    for(ll i=1;i<=n;i++){
        x[i]=read();
        y[i]=read();
    }
    for(ll i=1;i<=n;i++)dis[i]=w[i];
    for(ll i=1;i<=n;i++){
        ll minn=1e9,now;
        for(ll j=1;j<=n;j++){
            if(!vis[j]&&dis[j]<minn){
                minn=dis[j];
                now=j;
            }
        }
        vis[now]=true;
        // prllf("%d %.2lf\n",now,dis[now]);
        for(ll j=1;j<=n;j++){
            if(!vis[j]){
                dis[j]=min(dis[j],calc(now,j));
            }
        }
    }
    for(ll i=1;i<=n;i++)ans+=dis[i];
    printf("%.2lf\n",ans);
    return 0;
}
