#pragma GCC optimize 2
#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <queue>
#include <set>
#include <map>
#include <vector>
using namespace std;

namespace IO {
static const int kMegaBytes = 1 << 20;
static char buf[kMegaBytes], *S = buf, *T = buf;
inline char NextChar() {
  if (S == T) {
    T = (S = buf) + fread(buf, sizeof(char), sizeof(buf), stdin);
    if (S == T)
      return EOF;
  }
  return *S++;
}
template <typename Int> inline void Read(Int &x) {
  x = 0;
  Int flag = 1;
  char ch;
  for (ch = NextChar(); isspace(ch); ch = NextChar())
    ;
  if (ch == '-')
    flag = -1, ch = NextChar();
  for (; isdigit(ch); ch = NextChar())
    x = x * 10 + ch - '0';
  x *= flag;
}
} // namespace IO
using IO::Read;

typedef long long ll;
typedef pair<int, int> pii;

const int kMaxN = 40005;
int n, p, q;
struct Edge { int to, nxt; } e[kMaxN << 1];
int ecnt, head[kMaxN], dep[kMaxN], fa[kMaxN];

namespace SubTask1 {
inline void AddEdge(int u, int v) {
  e[++ecnt] = (Edge) { v, head[u] };
  head[u] = ecnt;
}
void BFS(int x) {
  queue<int> Q;
  Q.push(x);
  dep[x] = 1;
  fa[x] = 0;
  while (!Q.empty()) {
    int u = Q.front();
    for (int i = head[u]; i; i = e[i].nxt) {
      if (!dep[e[i].to]) {
        dep[e[i].to] = dep[u] + 1;
        fa[e[i].to] = u;
        Q.push(e[i].to);
      }
    }
    Q.pop();
  }
}
bool ban[kMaxN];
vector<pii> pp[kMaxN];
int Solve() {
  for (int i = 1, x, y; i < n; ++i) {
    Read(x), Read(y);
    AddEdge(x, y);
    AddEdge(y, x);
  }
  BFS(1);
  for (int i = 1, c, d, e; i <= p; ++i) {
    Read(c), Read(d), Read(e);
    pp[c].push_back(make_pair(e, i));
    pp[d].push_back(make_pair(e, i));
  }
  for (int i = 1, c, d, k; i <= q; ++i) {
    map<pii, int> S;
    Read(c), Read(d), Read(k);
    if (dep[c] < dep[d])
      swap(c, d);
    while (dep[c] > dep[d]) {
      for (int j = 0; j < pp[c].size(); ++j)
        ++S[pp[c][j]];
      c = fa[c];
    }
    while (c != d) {
      for (int j = 0; j < pp[c].size(); ++j)
        ++S[pp[c][j]];
      for (int j = 0; j < pp[d].size(); ++j)
        ++S[pp[d][j]];
      c = fa[c];
      d = fa[d];
    }
    if (c == d)
      for (int j = 0; j < pp[c].size(); ++j)
        ++S[pp[c][j]];
    int cntk = k, cnt = 0;
    for (map<pii, int>::iterator it = S.begin(); it != S.end(); ++it) {
      if (ban[it->first.second] || it->second < 2)
        continue;
      if (--cntk == 0) {
        printf("%d\n", it->first.first);
        // ban[it->first.second] = true;
      }
    }
    // fprintf(stderr, "%d\n", cnt);
  }
  return 0;
}
}

int main() {
  freopen("trade.in", "r", stdin);
  freopen("trade.out", "w", stdout);
  Read(n), Read(p), Read(q);
  if (n <= 3000)
    return SubTask1::Solve(); // 40pts
  return 0;
}