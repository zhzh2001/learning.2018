#pragma GCC optimize 2
#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <queue>
#include <cmath>
using namespace std;

namespace IO {
static const int kMegaBytes = 1 << 20;
static char buf[kMegaBytes], *S = buf, *T = buf;
inline char NextChar() {
  if (S == T) {
    T = (S = buf) + fread(buf, sizeof(char), sizeof(buf), stdin);
    if (S == T)
      return EOF;
  }
  return *S++;
}
template <typename Int> inline void Read(Int &x) {
  x = 0;
  Int flag = 1;
  char ch;
  for (ch = NextChar(); isspace(ch); ch = NextChar())
    ;
  if (ch == '-')
    flag = -1, ch = NextChar();
  for (; isdigit(ch); ch = NextChar())
    x = x * 10 + ch - '0';
  x *= flag;
}
} // namespace IO
using IO::Read;

typedef long long ll;
typedef pair<int, int> pii;

const int kMaxN = 5005;
int n, w[kMaxN];
struct Node { int x, y; } c[kMaxN];
double ans, d[kMaxN];

double QueryDist(int a, int b) {
  if (a > b)
    swap(a, b);
  if (a == 0) {
    return w[b];
  } else {
    return sqrt((double)(c[a].x - c[b].x) * (c[a].x - c[b].x)
      + (double)(c[a].y - c[b].y) * (c[a].y - c[b].y));
  }
}

bool vis[kMaxN];

int Prim() {
  for (int i = 1; i <= n; ++i)
    d[i] = w[i];
  for (int i = 1; i <= n; ++i) {
    int v = -1;
    for (int j = 1; j <= n; ++j)
      if (!vis[j] && (v == -1 || d[j] < d[v]))
        v = j;
    vis[v] = true;
    ans += d[v];
    for (int j = 1; j <= n; ++j)
      if (!vis[j]) {
        d[j] = min(d[j], QueryDist(v, j));
      }
  }
}

int main() {
  freopen("water.in", "r", stdin);
  freopen("water.out", "w", stdout);
  Read(n);
  for (int i = 1; i <= n; ++i) {
    Read(w[i]);
  }
  for (int i = 1; i <= n; ++i) {
    Read(c[i].x), Read(c[i].y);
  }
  Prim();
  printf("%.2lf\n", ans);
  return 0;
}