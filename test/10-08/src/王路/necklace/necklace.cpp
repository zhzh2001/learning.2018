#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
using namespace std;

namespace IO {
static const int kMegaBytes = 1 << 20;
static char buf[kMegaBytes], *S = buf, *T = buf;
inline char NextChar() {
  if (S == T) {
    T = (S = buf) + fread(buf, sizeof(char), sizeof(buf), stdin);
    if (S == T)
      return EOF;
  }
  return *S++;
}
template <typename Int> inline void Read(Int &x) {
  x = 0;
  Int flag = 1;
  char ch;
  for (ch = NextChar(); isspace(ch); ch = NextChar())
    ;
  if (ch == '-')
    flag = -1, ch = NextChar();
  for (; isdigit(ch); ch = NextChar())
    x = x * 10 + ch - '0';
  x *= flag;
}
} // namespace IO
using IO::Read;

typedef long long ll;
typedef pair<int, int> pii;

const int kMaxN = 1e5 + 5, kMaxM = 1e5 + 5;

int n, m, k;
struct Edge { 
  int u, v, w, c;
  inline bool operator<(const Edge &other) const {
    return w < other.w || (w == other.w && c < other.c);
  }
} a[kMaxM];

int fa[kMaxN];
inline void Init(int n = kMaxN) {
  for (int i = 0; i < kMaxN; ++i)
    fa[i] = i;
}
int Find(int x) { return x == fa[x] ? x : fa[x] = Find(fa[x]); }
inline bool Same(int x, int y) { return Find(x) == Find(y); }
inline bool Merge(int x, int y) {
  int fx = Find(x), fy = Find(y);
  if (fx == fy)
    return false;
  return fa[fx] = fy, true;
}
const int kInf = 0x3f3f3f3f;
namespace SubTask2 {
int d[2][16][16];
bool cho[kMaxN];
int Solve() {
  memset(d, 0x3f, sizeof d);
  for (int i = 1; i <= m; ++i) {
    d[a[i].c][a[i].u][a[i].v] = min(d[a[i].c][a[i].u][a[i].v], a[i].w);
    d[a[i].c][a[i].v][a[i].u] = min(d[a[i].c][a[i].v][a[i].u], a[i].w);
  }
  m = 0;
  for (int i = 0; i < n; ++i)
    for (int j = i + 1; j < n; ++j) {
      if (d[0][i][j] < kInf)
        a[++m] = (Edge) { i, j, d[0][i][j], 0};
      if (d[1][i][j] < kInf)
        a[++m] = (Edge) { i, j, d[1][i][j], 1};
    }
  Init();
  sort(a + 1, a + m + 1);
  int ans = 0;
  for (int i = 1, cnt = 0; i <= m; ++i) {
    if (!Same(a[i].u, a[i].v)) {
      ans += a[i].w;
      k -= a[i].c == 0;
      if (k < 0) {
        ++k;
        continue;
      }
      Merge(a[i].u, a[i].v);
      cho[i] = true;
    }
  }
  while (k > 0) {
    for (int i = m; i >= 1; --i) {
      if (cho[i] && a[i].c == 1) {
        bool fl = false;
        int u = a[i].u, v = a[i].v;
        for (int k = 1; k <= m; ++k)
          if (!cho[k] && a[k].c == 0)
            if (a[k].u == u && a[k].v == v) {
              ans -= a[i].w;
              ans += a[k].w;
              fl = true;
              break;
            }
        if (fl) {
          --k;
        }
        if (k == 0)
          break;
      }
    }
  }
  for (int ppp = m; ppp >= max(m - 100, 0); --ppp) {
    Init(16);
    int tmp = 0;
    random_shuffle(a + 1, a + m + 1);
    int cnt = 0;
    for (int i = 1; i <= m; ++i) {
      if (!Same(a[i].u, a[i].v)) {
        tmp += a[i].w;
        k -= a[i].c == 0;
        if (k < 0) {
          ++k;
          continue;
        }
        Merge(a[i].u, a[i].v);
        cho[i] = true;
        ++cnt;
      }
    }
    if (cnt != n - 1)
      continue;
    if (k > 0) {
      for (int i = m; i >= 1; --i) {
        if (cho[i] && a[i].c == 1) {
          bool fl = false;
          int u = a[i].u, v = a[i].v;
          for (int k = 1; k <= m; ++k)
            if (!cho[k] && a[k].c == 0)
              if (a[k].u == u && a[k].v == v) {
                tmp -= a[i].w;
                tmp += a[k].w;
                fl = true;
                break;
              }
          if (fl) {
            --k;
          }
          if (k == 0)
            break;
        }
      }
    }
    ans = min(ans, tmp);
  }
  printf("%d\n", ans);
  return 0;
}
}
namespace SubTask1 {
const int kMaxM = 1e5 + 5;
int Solve() {
  if (k != n - 1)
    puts("-1");
  int ans = 0;
  for (int i = 1, cnt = 0; i <= m; ++i) {
    if (Merge(a[i].u, a[i].v)) {
      ans += a[i].w;
      if (++cnt == n - 1)
        break;
    }
  }
  printf("%d\n", ans);
  return 0;
}
}
namespace Solution {
bool cho[kMaxM];
int Solve() {
  Init();
  int ans = 0;
  for (int i = 1, cnt = 0; i <= m; ++i) {
    if (!Same(a[i].u, a[i].v)) {
      ans += a[i].w;
      k -= a[i].c == 0;
      if (k < 0) {
        ++k;
        continue;
      }
      Merge(a[i].u, a[i].v);
      cho[i] = true;
    }
  }
  if (k > 0) {

  }
  printf("%d\n", ans);
  return 0;
}
}

int main() {
  freopen("necklace.in", "r", stdin);
  freopen("necklace.out", "w", stdout);
  Read(n), Read(m), Read(k);
  for (int i = 1; i <= m; ++i) {
    Read(a[i].u), Read(a[i].v), Read(a[i].w), Read(a[i].c);
  }
  sort(a + 1, a + m + 1);
  if (n <= 15)
    return SubTask2::Solve(); // 40 pts, fake
  bool same = a[1].c == 0;
  for (int i = 2; i <= m; ++i)
    same &= (a[i].c == 0);
  if (same)
    return SubTask1::Solve(); // 20 pts
  return Solution::Solve(); // fake
}
