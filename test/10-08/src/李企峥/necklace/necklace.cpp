#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for(int i=a;i<=b;i++)
#define Rep(i,a,b) for(int i=b;i>=a;i--)
#define p pair<int,int>
#define pp pair<p,p>
#define fi first
#define sd second
#define ff fi.fi
#define fs fi.sd
#define sf sd.fi
#define ss sd.sd
using namespace std;
ll n,m,k,kk,ans;
ll fa[50010];
pp a[100100];
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
int get(int x)
{
	if(fa[x]==x)return x;
	fa[x]=get(fa[x]);
	return fa[x];
}
int main()
{
	freopen("necklace.in","r",stdin);
	freopen("necklace.out","w",stdout);
	n=read();m=read();k=read();kk=n-1-k;
	For(i,1,m)
	{
		a[i].sf=read();a[i].ss=read();
		a[i].ff=read();a[i].fs=read();
	}
	sort(a+1,a+m+1);
	For(i,1,n)fa[i]=i;
	For(i,1,m)
	{
		int x=get(a[i].sf),y=get(a[i].ss);
		if(x==y)continue;
		if(a[i].fs==0&&k==0)continue;
		if(a[i].fs==1&&kk==0)continue;
		ans+=a[i].ff;
		fa[x]=y;
		if(a[i].fs==0)k--;
		if(a[i].fs==1)kk--;
	}
	cout<<ans<<endl;
	return 0;
}

