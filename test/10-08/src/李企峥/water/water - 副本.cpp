#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for(int i=a;i<=b;i++)
#define Rep(i,a,b) for(int i=b;i>=a;i--)
using namespace std;
struct D{
	double x,y,z;
}a[5010];
int c[5010],d[5010];
bool f[5010];
double ans;
int n,sz;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
void down(int x)
{
	int xx=0;
	while(x*2<=sz)
	{
		if(x*2+1>sz)xx=x*2;
		else
		{
			if(a[c[x*2]].z<a[c[x*2+1]].z)xx=x*2;
									else xx=x*2+1;
		}
		if(a[c[xx]].z>a[c[x]].z)break;
		swap(d[c[xx]],d[c[x]]);
		swap(c[xx],c[x]);
		x=xx;
	}
}
void up(int x)
{
	while(x!=1)
	{
		if(a[c[x/2]].z<a[c[x]].z)break;
		swap(d[c[x]],d[c[x/2]]);
		swap(c[x],c[x/2]);
		x/=2;
	}
}
void ins(int x)
{
	sz++;
	c[sz]=x;
	d[x]=sz;
	up(sz);
}
double sqr(double x)
{
	return x*x;
}
int main()
{
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
	n=read();
	For(i,1,n)cin>>a[i].z;
	For(i,1,n)cin>>a[i].x>>a[i].y;
	For(i,1,n)ins(i);
	memset(f,false,sizeof(f));
	For(i,1,n)
	{
		cout<<c[1]<<endl;
		ans+=a[c[1]].z;
		f[c[1]]=true;
		For(j,1,n)
		{
			if(f[j])continue;
			a[j].z=min(sqrt(sqr(a[c[1]].x-a[j].x)+sqr(a[c[1]].y-a[j].y)),a[j].z);
			up(d[j]);
		}
		c[1]=c[sz];
		sz--;
		down(1);
	}
	printf("%.2lf",ans);
	//cout<<ans<<endl;
	return 0;
}

