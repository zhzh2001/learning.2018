#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int inf = 0x3f3f3f3f;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define gc getchar
#define pc putchar
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
#define pp(x,y) cout << "pp: " << x << " " << y << endl;
inline int read(){
	register int x=0,f=1;register char c=gc();
	for(;!isdigit(c);c=gc())if(c=='-')f=-1;
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}
#define rd read
void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);puts("");}
const int maxn = 5005;
int n,x[maxn],y[maxn],w[maxn];
double f[maxn];
bool vis[maxn];
double query(int a,int b){
	if(a==0) return w[b];
	return sqrt(1ll * (x[a] - x[b]) * (x[a] - x[b]) + 1ll * (y[a] - y[b]) * (y[a] - y[b]));
}
int main(){
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
	n = rd();
	Rep(i,1,n) w[i] = rd();
	Rep(i,1,n) x[i] = rd(),y[i] = rd();
	Rep(i,0,n) f[i] = 1e100,vis[i] = false;
	f[0] = 0;
	double ans = 0;
	Rep(t,1,n+1){
		int k = -1;
		Rep(i,0,n) if(!vis[i] && (k==-1 || f[i] < f[k])) k = i;
		if(k==-1) break;
		ans += f[k];
		vis[k] = true;
		Rep(i,0,n) if(!vis[i]) if(f[i] > query(k,i)) f[i] = query(k,i);
	}printf("%.2lf\n",ans);
}



