#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int inf = 0x3f3f3f3f;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define gc getchar
#define pc putchar
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
#define pp(x,y) cout << "pp: " << x << " " << y << endl;
inline int read(){
	register int x=0,f=1;register char c=gc();
	for(;!isdigit(c);c=gc())if(c=='-')f=-1;
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}
#define rd read
void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);puts("");}
#define queue QuEuE
const int maxn = 4e4+233;
struct Edge{
	int to,nxt;
	Edge(){}
	Edge(int to,int nxt):
		to(to),nxt(nxt){}
}edge[maxn * 2];
int first[maxn],nume;
void Addedge(int a,int b){
	edge[nume] = Edge(b,first[a]);
	first[a] = nume++;
}
int in[maxn],out[maxn],dfsclk=0;
int fa[maxn][17],dep[maxn];
void dfs(int u){
	in[u] = ++dfsclk;
	for(int e=first[u];~e;e=edge[e].nxt){
		int v = edge[e].to;
		if(v == fa[u][0]) continue;
		fa[v][0] = u;
		dep[v] = dep[u] + 1;
		dfs(v);
	}
	out[u] = dfsclk;
}

int h[maxn];
struct Queue{
	int l,r,L,R;
	Queue(){}
	Queue(int l,int r,int L,int R):
		l(l),r(r),L(L),R(R){}
}queue[maxn*30];int front,rear;

struct modify{
	int pos1,pos2,l,r,opt,v;
	modify(){}
	modify(int pos1,int pos2,int l,int r,int opt,int v):
		pos1(pos1),pos2(pos2),l(l),r(r),opt(opt),v(v){}
}M[maxn * 4],TM[maxn * 4];int tot,cur;
int qx[maxn],qy[maxn],qk[maxn],res[maxn],ans[maxn],a[maxn];
int tmp[maxn];
inline bool cmpv(modify a,modify b){
	return a.v < b.v;
}
inline bool cmp3(int x,int y){
	return in[qy[x]] < in[qy[y]];
}
int c[maxn],n,m,q;
#define lowbit(x) ((x) & (-x))
struct Tree{
	int tot;
	void clear(){tot = 0;}
	int lc[maxn*200],rc[maxn*200],tag[maxn*200],T[maxn*200];
	int root[maxn*200]; 
	#define mid ((l+r)>>1)
	inline void insert(int &o,int l,int r,int x,int y){
		if(!o){
			o=++tot;
			T[o]=tag[o]=lc[o]=rc[o]=root[o]=0;
		}
		if(l==x&&r==y){
			T[o]++;tag[o]++;
			return ;
		}
		if(y<=mid) insert(lc[o],l,mid,x,y); else
		if(mid+1<=x) insert(rc[o],mid+1,r,x,y); else{
			insert(lc[o],l,mid,x,mid);
			insert(rc[o],mid+1,r,mid+1,y);
		}
		T[o] = (T[lc[o]] + T[rc[o]]) + tag[o];
	}
	inline void Insert(int &o,int l,int r,int x,int y,int L,int R){
		if(!o){
			o=++tot;
			T[o]=tag[o]=lc[o]=rc[o]=root[o]=0;
		}
		if(l==x&&r==y){
			insert(root[o],1,n,L,R);
			return ;
		}
		if(y<=mid) Insert(lc[o],l,mid,x,y,L,R); else
		if(mid+1<=x) Insert(rc[o],mid+1,r,x,y,L,R); else{
			Insert(lc[o],l,mid,x,mid,L,R);
			Insert(rc[o],mid+1,r,mid+1,y,L,R);
		}
	}
	inline int query(int o,int l,int r,int x){
		if(!o) return 0;
		if(l==r) return T[o];
		if(x<=mid) return query(lc[o],l,mid,x) + tag[o]; else
				   return query(rc[o],mid+1,r,x) + tag[o];
	}
	inline int query(int o,int l,int r,int x,int y){
		if(!o) return 0;
		if(l==r) return query(root[o],1,n,y);
		int res = query(root[o],1,n,y);
		if(x<=mid) return query(lc[o],l,mid,x,y)+res; else
				   return query(rc[o],mid+1,r,x,y)+res;
	}
	#undef mid
}T;
int ROOT;
inline void solve(int l,int r,int L,int R){
	//L,R表示二分范围 
	if(l>r) return ;
	if(L==R){
		Rep(i,l,r) ans[a[i]] = L;
		return ;
	}
	int mid = (L+R)>>1;
	while(cur <= tot && M[cur].v <= mid){
		T.Insert(ROOT,1,n,M[cur].pos1,M[cur].pos2,M[cur].l,M[cur].r);
		cur++;
	}
	int ll = l,rr = r;
	Rep(i,l,r){
		if(T.query(ROOT,1,n,in[qy[a[i]]],in[qx[a[i]]]) >= qk[a[i]]) tmp[ll++] = a[i]; else tmp[rr--] = a[i];
	}
	Rep(i,l,r) a[i] = tmp[i];
	queue[rear++] = Queue(l,ll-1,L,mid);
	queue[rear++] = Queue(rr+1,r,mid+1,R);
	//res[i]表示小于等于它的有多少个
	//如果res比原来的小 ，那么还需要变多
	//如果res比原来的大于等于，那么可以不变或者变少 
}
void main_solve(){
	Rep(i,1,q) a[i] = i;
	front = rear = 0;
	cur = 1;T.clear();ROOT = 0;
	queue[rear++] = Queue(1,q,1,*h);
	while(front < rear){
		if(front && (!(queue[front-1].r < queue[front].l))){
			cur = 1;
			T.clear();
			ROOT = 0;
		}
		Queue tmp = queue[front++];
		solve(tmp.l,tmp.r,tmp.L,tmp.R);
//		if(rear==3) exit(0);
	}
}
void Add(int pos1,int pos2,int l,int r,int opt,int v){
//	printf("%d %d %d %d %d\n",pos,l,r,opt,v);
	if(pos1 >= n+1) return ;
	if(l >= n+1) return ;
	if(pos1 > pos2) return ;
	if(l > r) return ;
	M[++tot] = modify(pos1,pos2,l,r,opt,v);
}
int tc[maxn],td[maxn],te[maxn];
int main(){
	freopen("trade.in","r",stdin);
	freopen("trade.out","w",stdout);
	n = rd(),m = rd(),q = rd();
	mem(first,-1);nume = 0;
	rep(i,1,n){
		int a = rd(),b = rd();
		Addedge(a,b);
		Addedge(b,a);
	}
	dep[0] = 0;fa[1][0] = 0;dfsclk = 0;dfs(1);
	Rep(j,1,16)
		Rep(i,1,n)
			fa[i][j] = fa[fa[i][j-1]][j-1];
			
	*h = 0;
	Rep(i,1,m){
		tc[i]=rd(),td[i]=rd(),te[i]=rd();
		h[++*h] = te[i];
	}sort(h+1,h+1+*h);
	*h = unique(h+1,h+1+*h) - h - 1;
	Rep(i,1,m) te[i]=lower_bound(h+1,h+1+*h,te[i])-h;
	Rep(i,1,m){
		int x = tc[i],y = td[i],v = te[i];
		if(in[x] > in[y]) swap(x,y);
		if(x==y){
			Add(in[x],out[x],1,in[x],1,v);
			Add(out[x]+1,n,in[x],out[x],1,v);
		} else
		if(in[x] <= in[y] && in[y] <= out[x]){
			int z = y;
			Dep(i,16,0) if(dep[fa[z][i]] > dep[x]) z = fa[z][i];
			Add(in[y],out[y],1,in[z]-1,1,v);
			Add(out[z]+1,n,in[y],out[y],1,v);
		} else{
			Add(in[y],out[y],in[x],out[x],1,v);
		}
	}sort(M+1,M+1+tot,cmpv);
	Rep(i,1,q){
		qx[i] = rd(),qy[i] = rd(),qk[i] = rd(),res[i] = 0;
		if(in[qx[i]] > in[qy[i]]) swap(qx[i],qy[i]);
	}
	main_solve();
	Rep(i,1,q){
		writeln(h[ans[i]]);
	}
	return 0;
}
