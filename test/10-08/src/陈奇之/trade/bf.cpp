#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int inf = 0x3f3f3f3f;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define gc getchar
#define pc putchar
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
#define pp(x,y) cout << "pp: " << x << " " << y << endl;
inline int read(){
	register int x=0,f=1;register char c=gc();
	for(;!isdigit(c);c=gc())if(c=='-')f=-1;
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}
#define rd read
void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);puts("");}
#define queue QuEuE
const int maxn = 4e4+233;
struct Edge{
	int to,nxt;
	Edge(){}
	Edge(int to,int nxt):
		to(to),nxt(nxt){}
}edge[maxn * 2];
int first[maxn],nume;
void Addedge(int a,int b){
	edge[nume] = Edge(b,first[a]);
	first[a] = nume++;
}
int in[maxn],out[maxn],dfsclk=0;
int fa[maxn][17],dep[maxn];
void dfs(int u){
	in[u] = ++dfsclk;
	for(int e=first[u];~e;e=edge[e].nxt){
		int v = edge[e].to;
		if(v == fa[u][0]) continue;
		fa[v][0] = u;
		dep[v] = dep[u] + 1;
		dfs(v);
	}
	out[u] = dfsclk;
}

int h[maxn];
struct Queue{
	int l,r,L,R;
	Queue(){}
	Queue(int l,int r,int L,int R):
		l(l),r(r),L(L),R(R){}
}queue[maxn*20];int front,rear;

struct modify{
	int pos,l,r,opt,v;
	modify(){}
	modify(int pos,int l,int r,int opt,int v):
		pos(pos),l(l),r(r),opt(opt),v(v){}
}M[maxn * 4],TM[maxn * 4];int tot,cur;
int qx[maxn],qy[maxn],qk[maxn],res[maxn],ans[maxn],a[maxn];
int tmp[maxn];
bool cmpv(modify a,modify b){
	return a.v < b.v;
}
bool cmp2(modify a,modify b){
	return a.pos < b.pos || (a.pos == b.pos && a.opt<b.opt);
}
bool cmp3(int x,int y){
	return in[qy[x]] < in[qy[y]];
}
int c[maxn],n,m,q;
#define lowbit(x) ((x) & (-x))
void change(int l,int r,int v){
	for(int x=l;x<=n;x+=lowbit(x)) c[x] += v;
	for(int x=r+1;x<=n;x+=lowbit(x)) c[x] -= v;
}
int get(int x){
	int res = 0;
	for(;x;x-=lowbit(x)) res += c[x];
	return res;
}
void solve(int l,int r,int L,int R){
	//L,R表示二分范围 
	if(L==R){
		Rep(i,l,r) ans[a[i]] = L;
		return ;
	}
	int mid = (L+R)>>1;
//	printf("solve(%d)\n",h[mid]);
	int last = cur;
	while(last <= tot && M[last].v <= mid){
		TM[last] = M[last];
		last++;
	}
	sort(TM + cur,TM + last,cmp2);//按照dfs序排序 
	if(l>r){
		while(cur < last){
			change(TM[cur].l,TM[cur].r,TM[cur].opt);
			cur++;
		}
		return ;
	}
	sort(a+l,a+r+1,cmp3);//按照dfs序排序 
	for(int i=l;i<=r;++i){
		while(cur < last && TM[cur] . pos <= in[qy[a[i]]]){
			change(TM[cur].l,TM[cur].r,TM[cur].opt);
			cur++;
		}
		res[a[i]] = get(in[qx[a[i]]]);
	}
	while(cur < last){
		change(TM[cur].l,TM[cur].r,TM[cur].opt);
		cur++;
	}
	int ll = l,rr = r;
	Rep(i,l,r){
		if(res[a[i]] >= qk[a[i]]) tmp[ll++] = a[i]; else tmp[rr--] = a[i];
	}
	Rep(i,l,r) a[i] = tmp[i];
	
	queue[rear++] = Queue(l,ll-1,L,mid);
	queue[rear++] = Queue(rr+1,r,mid+1,R);
	//res[i]表示小于等于它的有多少个
	//如果res比原来的小 ，那么还需要变多
	//如果res比原来的大于等于，那么可以不变或者变少 
}
void main_solve(){
	front = rear = 0;
	cur = 1;
	Rep(i,1,n) res[i] = 0;
	Rep(i,1,n) c[i] = 0;
	Rep(i,1,q) a[i] = i;
	queue[rear++] = Queue(1,q,1,*h);
	while(front <rear){
		if(front && (!(queue[front-1].r < queue[front].l))){
			Rep(i,1,n) res[i] = 0;
			cur = 1;
			Rep(i,1,n) c[i] = 0;
		}
		Queue tmp = queue[front++];
		solve(tmp.l,tmp.r,tmp.L,tmp.R);
		//if(rear>4) exit(0);
	}
}
void Add(int pos,int l,int r,int opt,int v){
//	printf("%d %d %d %d %d\n",pos,l,r,opt,v);
	if(pos==n+1) return ;
	M[++tot] = modify(pos,l,r,opt,v);
}
int tc[maxn],td[maxn],te[maxn];
int main(){
	freopen("trade.in","r",stdin);
//	freopen("trade.out","w",stdout);
	n = rd(),m = rd(),q = rd();
	mem(first,-1);nume = 0;
	rep(i,1,n){
		int a = rd(),b = rd();
		Addedge(a,b);
		Addedge(b,a);
	}
	dep[0] = 0;fa[1][0] = 0;dfsclk = 0;dfs(1);
	Rep(j,1,16)
		Rep(i,1,n)
			fa[i][j] = fa[fa[i][j-1]][j-1];
			
	*h = 0;
	Rep(i,1,m){
		tc[i]=rd(),td[i]=rd(),te[i]=rd();
		h[++*h] = te[i];
	}sort(h+1,h+1+*h);
	*h = unique(h+1,h+1+*h) - h - 1;
//	puts("H is ");
//	Rep(i,870,890){
//		printf("%d : %d\n",i,h[i]);
//	}
	Rep(i,1,m) te[i]=lower_bound(h+1,h+1+*h,te[i])-h;
	Rep(i,1,m){
		int x = tc[i],y = td[i],v = te[i];
		if(in[x] > in[y]) swap(x,y);
		if(x==y){
			Add(in[x],1,in[x],1,v);
			Add(out[x]+1,1,in[x],-1,v);
			Add(out[x]+1,in[x],out[x],1,v);
			Add(n+1,in[x],out[x],-1,v);
/*			Rep(i,in[x],out[x]){
				Rep(j,1,in[x]){
					printf("{%d %d}\n",i,j);
				}
			}
			Rep(i,out[x]+1,n){
				Rep(j,in[x],out[x]){
					printf("{%d %d}\n",i,j);
				}
			}*/
		} else
		if(in[x] <= in[y] && in[y] <= out[x]){
			int z = y;
			Dep(i,16,0) if(dep[fa[z][i]] > dep[x]) z = fa[z][i];
			Add(in[y],1,in[z]-1,1,v);
			Add(out[y]+1,1,in[z]-1,-1,v);
			
			Add(out[z]+1,in[y],out[y],1,v);
			Add(n+1,in[y],out[y],-1,v);
			
/*			Rep(i,in[y],out[y]){
				Rep(j,1,in[z]-1){
					printf("{%d %d}\n",i,j);
				}
			}
			Rep(i,out[x]+1,n){
				Rep(j,in[y],out[y]){
					printf("{%d %d}\n",i,j);
				} 
			}*/
		} else{
			Add(in[y],in[x],out[x],1,v);
			Add(out[y]+1,in[x],out[x],-1,v);
		}
	}sort(M+1,M+1+tot,cmpv);
	Rep(i,1,q){
		qx[i] = rd(),qy[i] = rd(),qk[i] = rd(),res[i] = 0;
		if(in[qx[i]] > in[qy[i]]) swap(qx[i],qy[i]);
//		printf("{%d %d}\n",in[qx[i]],in[qy[i]]);
	}
	main_solve();
	Rep(i,1,q){
		writeln(h[ans[i]]);
	}
	return 0;
}
