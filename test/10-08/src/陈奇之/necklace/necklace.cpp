#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int inf = 0x3f3f3f3f;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define gc getchar
#define pc putchar
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
#define pp(x,y) cout << "pp: " << x << " " << y << endl;
inline int read(){
	register int x=0,f=1;register char c=gc();
	for(;!isdigit(c);c=gc())if(c=='-')f=-1;
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}
#define rd read
void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);puts("");}
const int maxn = 50050;
const int maxm = 105050;
struct E{
	int s,t,c,col;
	bool operator < (const E &w) const{
		if(c != w.c) return c < w.c;
		return col<w.col;
	}
}e[maxm];
int fa[maxn];
int n,m,need,ans;
inline int find(int x){
	return fa[x] == x ? x : fa[x] = find(fa[x]); 
}
pair<int,int> check(int cost){
	pair<int,int> tmp = make_pair(0,0);
	Rep(i,1,m) if(!e[i].col) e[i].c+=cost;
	sort(e+1,e+1+m);
	rep(i,0,n) fa[i] = i;
	int j = 0;
	Rep(i,1,m){
		if(find(e[i].s)!=find(e[i].t)){
			if(!e[i].col) tmp.se++;
			fa[find(e[i].s)] = find(e[i].t);
			tmp.fi += e[i].c;
			if(++j==n-1) break;
		}
	}
	Rep(i,1,m) if(!e[i].col) e[i].c-=cost;
	return tmp;
}
int main(){
	freopen("necklace.in","r",stdin);
	freopen("necklace.out","w",stdout);
	n = rd(),m = rd(),need = rd();
	Rep(i,1,m){
		e[i].s = rd();
		e[i].t = rd();
		e[i].c = rd();
		e[i].col = rd();
	}
	int l = -100,r = 100,ans = 233;
	while(l <= r){
		int mid = (l + r) >> 1;
		pair<int,int> tmp = check(mid);
		if(tmp.se >= need){
			ans = tmp.fi - need * mid;
			l = mid+1;
		} else{
			r = mid-1;
		}
	}writeln(ans);
	return 0;
}


