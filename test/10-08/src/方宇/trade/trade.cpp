#include<cstdio>
#include<cctype>
#include<cmath>
#include<cstring>
#include<iostream>
#include<algorithm>
#define LL long long 
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define fy 20021218020021218
#define N 100010
using namespace std;
LL a[N],b[N],c[N],f[N][21],deep[N],q[N][5],p[N][5],ans[N];
LL n,m,i,j,k,l,r,x,y,num;
LL read()
{
	LL x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
void add(LL k,LL l,LL r){a[k]=r; b[k]=c[l]; c[l]=k;}
void swap(LL &a,LL &b){LL t=a; a=b; b=t;}
void build(LL k,LL fa)
{
	deep[k]=deep[fa]+1;
	f[k][0]=fa;
	for(LL j=c[k];j;j=b[j])
	{
		if(a[j]==fa) continue;
		build(a[j],k);
	}
}
LL lca(LL l,LL r)
{
	if(deep[l]<deep[r]) swap(l,r);
	for(LL k=20;k>=0;k--)
	  if(deep[f[l][k]]>=deep[r]) l=f[l][k];
	if(l==r) return l;
	for(LL k=20;k>=0;k--)
	  if(f[l][k]!=f[r][k]) l=f[l][k],r=f[r][k];
	return f[l][0];
}
bool cmp(LL a,LL b){return a<b;}
int main()
{
	freopen("trade.in","r",stdin);
 	freopen("trade.out","w",stdout);
	n=read(); l=read(); r=read();
	rep(i,1,n-1)
	{
		x=read(); y=read();
		add(i*2-1,x,y);
		add(i*2,y,x);
	}
	build(1,0);
	rep(i,1,20)
	  rep(j,1,n) f[j][i]=f[f[j][i-1]][i-1];
	rep(i,1,l){q[i][1]=read(); q[i][2]=read(); q[i][3]=read();}
	rep(i,1,r){p[i][1]=read(); p[i][2]=read(); p[i][3]=read();}
	rep(j,1,r)
	{
		num=0;
		rep(i,1,l)
		  if(lca(lca(q[i][1],q[i][2]),lca(p[j][1],p[j][2]))==lca(p[j][1],p[j][2]))
		    if(lca(q[i][1],p[j][1])==q[i][1]&&lca(q[i][2],p[j][2])==q[i][2]||lca(q[i][1],p[j][2])==q[i][1]&&lca(q[i][2],p[j][1])==q[i][2])
		      ans[++num]=q[i][3];
		sort(ans+1,ans+num+1,cmp);
		printf("%lld\n",ans[p[j][3]]);
		rep(i,1,num) ans[i]=0;
	}
	return 0;
}
