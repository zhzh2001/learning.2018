#include<cstdio>
#include<cctype>
#include<cmath>
#include<cstring>
#include<iostream>
#include<algorithm>
#define LL long long 
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define fy 20021218020021218
#define N 5010
using namespace std;
double x[N],y[N];
double ans,minn,pre[N];
int boo[N];
int n,m,i,j,k,l,r;
int read()
{
	int x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
double sum(double x1,double y1,double x2,double y2)
{
	return sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
}
int main()
{
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
	n=read();
	rep(i,1,n) scanf("%lf",&pre[i]);
	rep(i,1,n) scanf("%lf%lf",&x[i],&y[i]);
	boo[0]=1; ans=0;
	rep(i,1,n)
	{
		minn=fy; k=0;
		rep(j,1,n)
		  if(minn>pre[j]&&!boo[j]) minn=pre[j],k=j;
		ans+=pre[k]; boo[k]=1;
		rep(j,1,n)
		  if(pre[j]>sum(x[k],y[k],x[j],y[j])&&!boo[j]) pre[j]=sum(x[k],y[k],x[j],y[j]);
	}
	printf("%.2lf",ans);
	return 0;
}
