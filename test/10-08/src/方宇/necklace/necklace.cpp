#include<cstdio>
#include<cctype>
#include<cstring>
#include<iostream>
#include<algorithm>
#define LL long long 
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define fy 20021218020021218
#define N 200010
using namespace std;
struct node
{
	int l,r,v,col;
}white[N],black[N];
LL num0,num1,color;
LL n,m,i,j,k,l,r,h,mid,ans,s,sum,Min,fal,far;
LL a[N];
LL read()
{
	LL x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
LL find(LL k)
{
	if(k==a[k]) return k;
	a[k]=find(a[k]);
	return a[k];
}
bool cmp(node a,node b){return a.v<b.v;}
int main()
{
	freopen("necklace.in","r",stdin);
	freopen("necklace.out","w",stdout);
	n=read(); m=read(); k=read();
	rep(i,1,m) 
	{
		l=read(); r=read(); h=read(); color=read();
		if(!color) 
		{
			num0++;
			white[num0].l=l;
			white[num0].r=r;
			white[num0].v=h;
		}
		else
		{
			num1++;
			black[num1].l=l;
			black[num1].r=r;
			black[num1].v=h;
		}
	}
	sort(white+1,white+num0+1,cmp);
	sort(black+1,black+num1+1,cmp);
	l=-101; r=101; Min=fy;
	while(l<=r)
	{
		mid=(l+r)>>1;
		s=0; sum=0; ans=0;
		rep(i,0,n) a[i]=i;
		i=1; j=1;
		while(i<=num0&&j<=num1)
		{
			if(s==n-1) break;
			if(white[i].v+mid<=black[j].v)
			{
				fal=find(white[i].l);
				far=find(white[i].r);
				if(fal!=far)
				{
					s++; sum++;
					ans+=white[i].v;
					a[fal]=far;
				}
				i++;
			}
			else
			{
				fal=find(black[j].l);
				far=find(black[j].r);
				if(fal!=far)
				{
					s++; 
					ans+=black[j].v;
					a[fal]=far;
				}
				j++;
			}
		}
		while(i<=num0)
		{
			if(s==n-1) break;
			fal=find(white[i].l);
			far=find(white[i].r);
			if(fal!=far)
			{
				s++; sum++;
				ans+=white[i].v;
				a[fal]=far;
			}
			i++;
		}
		while(j<=num1)
		{
			if(s==n-1) break;
			fal=find(black[j].l);
			far=find(black[j].r);
			if(fal!=far)
			{
				s++; 
				ans+=black[j].v;
				a[fal]=far;
			}
			j++;
		}
		if(sum<k) r=mid-1;
		else
		{
			if(ans<Min) Min=ans;
			l=mid+1;
		}
	}
	printf("%lld",Min);
	return 0;
}
