#include <cstdio>
#include <algorithm>
using namespace std;
const int N=5e4+7;
struct edge {
	int u,t,w,c,id;
}e[N<<1];
int n,m,K,cnt,ans;
int fa[N],vis[N<<1];
void add(int u,int t,int w,int c) {
	e[++cnt].t=t;e[cnt].u=u;e[cnt].w=w;e[cnt].c=c;e[cnt].id=cnt;
}
int find(int x) {return x==fa[x]?x:fa[x]=find(fa[x]);}
bool cmp1(edge a,edge b) {
	if (a.c!=b.c) return a.c<b.c;
	return a.w<b.w;
}
bool cmp2(edge a,edge b) {
	return a.w<b.w;
}
int main() {
	freopen("necklace.in","r",stdin);
	freopen("necklace.out","w",stdout);
	scanf("%d%d%d",&n,&m,&K);
	for (int i=1,u,v,w,c;i<=m;i++) {
		scanf("%d%d%d%d",&u,&v,&w,&c);
		add(u,v,w,c);
	}
	for (int i=0;i<n;i++) fa[i]=i;
	sort(e+1,e+m+1,cmp1);
	for (int i=1;i<=m;i++) {
		int u=find(e[i].u),v=find(e[i].t);
		if (u==v) continue;
		fa[u]=v;vis[e[i].id]=1;
		ans+=e[i].w;
		K--;
		if (!K) break;
	}
	sort(e+1,e+m+1,cmp2);
	for (int i=1;i<=m;i++) {
		int u=find(e[i].u),v=find(e[i].t);
		if (u==v || vis[e[i].id]) continue;
		fa[u]=v;ans+=e[i].w;
	}
	printf("%d\n",ans);
}
