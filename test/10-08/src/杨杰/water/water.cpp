#include <cstdio>
#include <cmath>
#include <algorithm>
using namespace std;
const int N=5005,M=12497507;
#define ll long long
int n,cnt;
double ans;
int fa[N];
double w[N],x[N],y[N];
double sma[N],val[N];
struct edge {
	int u,t;
	double w;
	bool operator <(edge a) const {
		return w<a.w;
	}
}e[M];
double sqr(double x) {return x*x;}
void add(int u,int t,double w) {
	e[++cnt].u=u;e[cnt].t=t;e[cnt].w=w;
}
double dis(int i,int j) {
	return sqrt(sqr(x[i]-x[j])+sqr(y[i]-y[j]));
}
int find(int x) {return x==fa[x]?x:fa[x]=find(fa[x]);}
int main() {
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdut);
	scanf("%d",&n);
	for (int i=1;i<=n;i++) scanf("%lf",&w[i]);
	for (int i=1;i<=n;i++) scanf("%lf%lf",&x[i],&y[i]);
	for (int i=1;i<=n;i++)
		for (int j=i+1;j<=n;j++) add(i,j,dis(i,j));
	for (int i=1;i<=n;i++) fa[i]=i,sma[i]=val[i]=w[i];
	sort(e+1,e+cnt+1);
	for (int i=1;i<=cnt;i++) {
		int u=find(e[i].u),v=find(e[i].t);
		if (u==v) continue;
		if (max(sma[u],sma[v])>e[i].w) {
			fa[v]=u;val[u]+=val[v]-max(sma[u],sma[v])+e[i].w;
			sma[u]=min(sma[u],sma[v]);
		}
	}
	for (int i=1;i<=n;i++) if (find(i)==i) ans+=val[i];
	printf("%.2lf\n",ans);
}
