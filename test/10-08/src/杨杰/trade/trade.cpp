#include <cmath>
#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;
const int N=40005;
struct edge {
	int t,nxt;
}e[N<<1];
int n,p,q,cnt,sqr,tp,now;
int head[N],w[N],b[N],fa[N][18],dep[N],st[N],bl[N],vis[N],ci[N],ans[N],T[N<<2];
vector<int>flg[N];
struct node {
	int u,v,k,id;
	bool operator <(node a) const {
		return bl[u]==bl[a.u]?bl[v]<bl[a.v]:bl[u]<bl[a.u];
	}
}ask[N];
void add(int u,int t) {
	e[++cnt].t=t;e[cnt].nxt=head[u];head[u]=cnt;
}
void dfs(int u,int F) {
	int la=tp;
	fa[u][0]=F;dep[u]=dep[F]+1;
	for (int i=1;i<=17;i++) fa[u][i]=fa[fa[u][i-1]][i-1];
	for (int i=head[u];i;i=e[i].nxt) {
		int t=e[i].t;
		if (t==F) continue;
		dfs(t,u);
		if (tp-la>=sqr) {
			now++;
			while (tp-la>0) bl[st[tp--]]=now;
		}
	}
	st[++tp]=u;
}
int Lca(int a,int b) {
	if (dep[a]>dep[b]) swap(a,b);
	for (int i=17;~i;i--) if (dep[fa[b][i]]>=dep[a]) b=fa[b][i];
	if (a==b) return a;
	for (int i=17;~i;i--) if (fa[b][i]!=fa[a][i]) b=fa[b][i],a=fa[a][i];
	return fa[a][0];
}
#define ls ts<<1
#define rs ts<<1|1
void update(int ts,int l,int r,int pos,int v) {
	T[ts]+=v;
	if (l==r) return;
	int mid=(l+r)>>1;
	if (pos<=mid) update(ls,l,mid,pos,v);
	else update(rs,mid+1,r,pos,v);
}
int query(int ts,int l,int r,int K) {
	if (l==r) return b[l];
	int mid=(l+r)>>1;
	if (T[ls]>=K) return query(ls,l,mid,K);
	else return query(rs,mid+1,r,K-T[ls]);
}
void Add(int x) {
	for (int i=0;i<(int)flg[x].size();i++) {
		int u=flg[x][i];
		vis[u]++;
		if (vis[u]==2) update(1,1,b[0],w[u],1);
	}
}
void del(int x) {
	for (int i=0;i<(int)flg[x].size();i++) {
		int u=flg[x][i];
		vis[u]--;
		if (vis[u]==1) update(1,1,b[0],w[u],-1);
	}
}
void HH(int x) {
	if (!ci[x]) Add(x),ci[x]++;
	else del(x),ci[x]--;
}
void change(int lu,int u) {
	int lca=Lca(lu,u);
	while (lu!=lca) HH(lu),lu=fa[lu][0];
	while (u!=lca) HH(u),u=fa[u][0];
}
int main() {
	freopen("trade.in","r",stdin);
	freopen("trade.out","w",stdout);
	scanf("%d%d%d",&n,&p,&q);
	for (int i=1,u,v;i<n;i++) scanf("%d%d",&u,&v),add(u,v),add(v,u);
	sqr=sqrt(n);
	dfs(1,0);
	now++;
	while (tp) bl[st[tp--]]=now;
	for (int i=1,u,v;i<=p;i++) {
		scanf("%d%d%d",&u,&v,&w[i]);b[++b[0]]=w[i];
		flg[u].push_back(i);flg[v].push_back(i);
	}
	sort(b+1,b+b[0]+1);
	b[0]=unique(b+1,b+b[0]+1)-b-1;
	for (int i=1;i<=p;i++) w[i]=lower_bound(b+1,b+b[0]+1,w[i])-b;
	for (int i=1;i<=q;i++) scanf("%d%d%d",&ask[i].u,&ask[i].v,&ask[i].k),ask[i].id=i;
	sort(ask+1,ask+q+1);
	int lu=1,lv=1;
	for (int i=1;i<=q;i++) {
		int u=ask[i].u,v=ask[i].v;
		change(lu,u);change(lv,v);
		int lca=Lca(u,v);
		Add(lca);
		ans[ask[i].id]=query(1,1,b[0],ask[i].k);
		del(lca);
		lu=u;lv=v;
	}
	for (int i=1;i<=q;i++) printf("%d\n",ans[i]);
}
