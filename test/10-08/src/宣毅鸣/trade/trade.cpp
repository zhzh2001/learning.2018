#include<bits/stdc++.h>
using namespace std;
const int N=200005;
int ne[N],tot,fi[N],zz[N],cnt,l,sd[N],fa[20][N],f[N],in[N],out[N],n,m,q,x,y,c[N],a[N],b[N];
void jb(int x,int y){
	ne[++tot]=fi[x];
	fi[x]=tot;
	zz[tot]=y;
}
void dfs(int x,int y,int z){
	fa[0][x]=y;
	sd[x]=z;
	in[x]=++l;
	for (int i=fi[x];i;i=ne[i])
		if (zz[i]!=y)dfs(zz[i],x,z+1);
	out[x]=++l;
}
int pd(int x,int y){
	return in[x]<=in[y]&&out[x]>=out[y];
}
int cmp(int x,int y){
	return c[x]<c[y];
}
int lca(int x,int y){
	if (x==y)return x;
	if (sd[x]<sd[y])swap(x,y);
	for (int i=19;i>=0;i--)
		if (!pd(fa[i][x],y))x=fa[i][x];
	return fa[0][x];	
}
int main(){
	freopen("trade.in","r",stdin);
	freopen("trade.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	for (int i=1;i<n;i++){
		scanf("%d%d",&x,&y);
		jb(x,y);jb(y,x);
	}
	for (int i=1;i<=m;i++)scanf("%d%d%d",&a[i],&b[i],&c[i]),f[i]=i;
	sort(f+1,f+m+1,cmp);
	dfs(1,0,0);
	out[0]=++l;
	for (int i=1;i<=19;i++)
		for (int j=1;j<=n;j++)fa[i][j]=fa[i-1][fa[i-1][j]];
	while (q--){
		scanf("%d%d%d",&x,&y,&cnt);
		int z=lca(x,y);
		for (int i=1;i<=m;i++){
			if (((pd(a[f[i]],x)&&pd(z,a[f[i]]))||(pd(a[f[i]],y)&&pd(z,a[f[i]])))&&
			((pd(b[f[i]],x)&&pd(z,b[f[i]]))||(pd(b[f[i]],y)&&pd(z,b[f[i]]))))cnt--;
			if (!cnt){
				printf("%d\n",c[f[i]]);
				break;
			}
		}
	}
	return 0;
}
