#include<bits/stdc++.h>
using namespace std;
const int N=5005;
int n,f[N];
double dis[N],x[N],y[N],ans,k[N];
int main(){
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++)scanf("%lf",&dis[i]),k[i]=dis[i];
	for (int i=1;i<=n;i++)scanf("%lf%lf",&x[i],&y[i]);
	for (int i=1;i<=n;i++){
		int l=-1;
		for (int j=1;j<=n;j++)
			if (!f[j]&&(l==-1||dis[l]>dis[j]))l=j;
		f[l]=1;ans+=dis[l];
//		printf("%lf\n",ans);
		for (int j=1;j<=n;j++)
			if (!f[j])
				dis[j]=min(dis[j],sqrt((x[l]-x[j])*
				(x[l]-x[j])+(y[l]-y[j])*(y[l]-y[j])));
	}
	printf("%.2lf",ans);
	return 0;
}
