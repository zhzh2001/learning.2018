#include<bits/stdc++.h>
using namespace std;
const int N1=17,N2=1e5+5;
int f[1<<N1][N1],k,p[1<<N1][N1],fa[N2],xx[N2],yy[N2],z[N2],ff[N2],n,m,x,y,w,s,a[N1][N1][2];
int dfs(int x,int y){
	if (x==(1<<n)-1){
		if (y==k)return 0;
		return 1e8;
	}
	if (y>k)return 1e8;
	if (p[x][y])return f[x][y];
	f[x][y]=1e8;p[x][y]=1;
	for (int l=0;l<=1;l++)
		for (int i=0;i<n;i++)
			if (((1<<i)&x)==0){
				int s=1e5;
				for (int j=0;j<n;j++)
					if ((1<<j)&x)s=min(s,a[i][j][l]);
				if (s!=1e5)f[x][y]=min(f[x][y],s+dfs(x|(1<<i),y+1-l));	
			}
	return f[x][y];		
}
int cmp(int x,int y){
	return z[x]<z[y];
}
int find(int x){
	if (x==fa[x])return x;
	return fa[x]=find(fa[x]);
}
void doit(){
	for (int i=1;i<=m;i++){
		scanf("%d%d%d%d",&xx[i],&yy[i],&z[i],&ff[i]);
		if (ff[i]==1)z[i]=1e9;
		ff[i]=i;
	}
	sort(ff+1,ff+m+1,cmp);
	int ans=0;
	for (int i=0;i<n;i++)fa[i]=i;
	for (int i=1;i<=m;i++){
		x=find(xx[ff[i]]),y=find(yy[ff[i]]);
		if (x!=y){
			fa[x]=y;
			ans+=z[ff[i]];
		}
	}
	printf("%d",ans);
}
int main(){
	freopen("necklace.in","r",stdin);
	freopen("necklace.out","w",stdout);
	scanf("%d%d%d",&n,&m,&k);
	if (n>15){
		doit();
		return 0;
	}
	for (int i=0;i<n;i++)
		for (int j=0;j<n;j++)a[i][j][0]=a[i][j][1]=1e5;
	for (int i=1;i<=m;i++){
		scanf("%d%d%d%d",&x,&y,&w,&s);
		a[x][y][s]=min(a[x][y][s],w);
		a[y][x][s]=min(a[y][x][s],w);
	}
	printf("%d",dfs(1,0));
	return 0;
}
