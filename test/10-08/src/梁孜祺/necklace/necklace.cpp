#include<bits/stdc++.h>
#define N 100005
#define ll long long
using namespace std;
inline int read(){
  int x = 0, f = 0;char ch = getchar();
  for (; ch <'0' || ch>'9'; ch = getchar()) if (ch == '-') f = 1;
  for (; ch >= '0'&& ch<='9'; ch = getchar()) x = (x<<1) + (x<<3) + ch - 48;
  return f ?-x :x;
}
const int oo = 1e9;
int n, m, k, fa[N];
struct data{
  int u, v, w, c, tnt;
  bool operator < (const data & a) {
    return (w == a.w)?(c < a.c):(w < a.w);
  }
}e[N];
int cnt, tmp;
inline int find(int x) {
  return (fa[x] == x)?x:fa[x] = find(fa[x]);
}
inline void check(int z) {
  cnt = tmp = 0;
  for (int i = 1; i <= m; i++)
    if (!e[i].c) e[i].w += z;
  sort(e+1,e+1+m);
  for (int i = 0; i < n; i++) fa[i] = i;
  for (int i = 1; i <= m; i++) {
    int u = find(e[i].u), v = find(e[i].v);
    if (u!=v) {
      fa[u] = v; tmp += e[i].w;
      if (!e[i].c) tmp -= z, cnt++;
    }
  }
  for (int i = 1; i <= m; i++)
    if (!e[i].c) e[i].w -= z;
}
int main() {
  freopen("necklace.in","r",stdin);
  freopen("necklace.out","w",stdout);
  n = read(); m = read(); k = read();
  for (int i = 1; i <= m; i++)
    e[i] = (data){read(),read(),read(),read()};
  int l = -250, r = 250, ans = oo;
  while (l <= r) {
    int mid = l+r>>1;
    check(mid);
    if (cnt >= k) ans = tmp, l = mid+1;
    else r = mid-1;
  }
  printf("%d\n", ans);
  return 0;
}