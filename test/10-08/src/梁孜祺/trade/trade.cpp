#include<bits/stdc++.h>
#define N 40005
#define ll long long
using namespace std;
inline int read(){
  int x = 0, f = 0;char ch = getchar();
  for (; ch <'0' || ch>'9'; ch = getchar()) if (ch == '-') f = 1;
  for (; ch >= '0'&& ch<='9'; ch = getchar()) x = (x<<1) + (x<<3) + ch - 48;
  return f ?-x :x;
}
const int oo = 1e9;
int n, m1, m2, head[N], cnt, p[3005][3005], tot[3005];
struct edge {
  int nxt, to;
}e[N<<1];
inline void insert(int u, int v){
  e[++cnt] = (edge){head[u], v}; head[u] = cnt;
  e[++cnt] = (edge){head[v], u}; head[v] = cnt;
}
int fa[N][17], bin[17], dep[N];
inline void dfs(int x){
  for (int i = 1; i < 17; i++) {
    if (dep[x] < bin[i]) break;
    fa[x][i] = fa[fa[x][i-1]][i-1];
  }
  for (int y, i = head[x]; i; i = e[i].nxt)
    if ((y = e[i].to)!=fa[x][0]){
      dep[y] = dep[x]+1;
      fa[y][0] = x;
      dfs(y);
    }
}
struct data1{
  int x, y, z;
}a[N];
struct data2{
  int x, y, k;
}b[N];
inline bool pdfa(int x, int y) {
  if (dep[x] < dep[y]) return 0;
  int del = dep[x] - dep[y];
  for (int i = 0; i < 17; i++)
    if (del & bin[i]) x = fa[x][i];
  if (x == y) return 1;
  return 0;
}
inline int lca(int x, int y){
  if (dep[x] < dep[y]) swap(x, y);
  int del = dep[x] - dep[y];
  for (int i = 0; i < 17; i++)
    if (del & bin[i]) x = fa[x][i];
  for (int i = 16; i >= 0; i--)
    if (fa[x][i]!=fa[y][i])
      x = fa[x][i], y = fa[y][i];
  if (x == y) return x;
  return fa[x][0];
}
inline bool check(int tf, int bd) {
  int u1 = b[tf].x, v1 = b[tf].y, u2 = a[bd].x, v2 = a[bd].y;
  int LCA1 = lca(u1, v1), LCA2 = lca(u2, v2);
  if (pdfa(u1, u2)&&pdfa(v1, v2)) {
    if (LCA1 == LCA2) return 1;
    return 0;
  }
  if (pdfa(u1, v2)&&pdfa(v1, u2)) {
    if (LCA1 == LCA2) return 1;
    return 0;
  }
  if (pdfa(u1, u2)&&pdfa(u1, v2)) {
    if (pdfa(u2, LCA1) && pdfa(v2, LCA1)) return 1;
    return 0;
  }
  if (pdfa(v1, u2)&&pdfa(v1, v2)) {
    if (pdfa(u2, LCA1) && pdfa(v2, LCA1)) return 1;
    return 0;
  }
  return 0;
}
inline void solve(int i, int k) {
  sort(p[i]+1,p[i]+1+tot[i]);
  printf("%d\n", p[i][k]);
}
int main() {
  freopen("trade.in","r",stdin);
  freopen("trade.out","w",stdout);
  n = read();
  m1 = read();
  m2 = read();
  bin[0] = 1;
  for (int i = 1; i < 17; i++) bin[i] = bin[i-1]<<1;
  for (int i = 1; i < n; i++) {
    int u = read(), v = read();
    insert(u, v);
  }
  dfs(1);
  for (int i = 1; i <= m1; i++) a[i] = (data1){read(), read(), read()};
  for (int i = 1; i <= m2; i++) b[i] = (data2){read(), read(), read()};
  for (int i = 1; i <= m1; i++)
    for (int j = 1; j <= m2; j++)
      if (check(j, i)) {
        p[j][++tot[j]] = a[i].z;
      }
  for (int i = 1; i <= m2; i++)
    solve(i, b[i].k);
  return 0;
}