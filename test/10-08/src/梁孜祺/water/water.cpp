#include<bits/stdc++.h>
#define N 5005
#define ll long long
using namespace std;
inline int read(){
  int x = 0, f = 0;char ch = getchar();
  for (; ch <'0' || ch>'9'; ch = getchar()) if (ch == '-') f = 1;
  for (; ch >= '0'&& ch<='9'; ch = getchar()) x = (x<<1) + (x<<3) + ch - 48;
  return f ?-x :x;
}
const int oo = 1e9;
int n, vis[N];
double dis[N];
struct data{
  int w, x, y;
}a[N];
inline ll sqr(int x) {
  return 1ll*x*x;
}
inline double D(int i, int j) {
  return sqrt(sqr(a[i].x - a[j].x)+sqr(a[i].y - a[j].y));
}
int main() {
  freopen("water.in","r",stdin);
  freopen("water.out","w",stdout);
  n = read();
  for (int i = 1; i <= n; i++) 
    a[i].w = read();
  for (int i = 1; i <= n; i++)
    a[i].x = read(), a[i].y = read();
  for (int i = 1; i <= n; i++)
    dis[i] = a[i].w;
  for (int i = 1; i < n; i++) {
    double mn = 1.0*oo; int mnx = 0;
    for (int j = 1; j <= n; j++) 
      if ((mn > dis[j] || mn == dis[j] && a[j].w < a[mnx].w) && !vis[j]) {
        mn = dis[j];
        mnx = j;
      }
    vis[mnx] = 1;
    for (int j = 1; j <= n; j++) 
      if (!vis[j]&&dis[j] > D(j, mnx))
        dis[j] = D(j, mnx);
  }
  double ans = 0;
  for (int i = 1; i <= n; i++)
    ans += dis[i];
  printf("%.2lf\n", ans);
  return 0;
}