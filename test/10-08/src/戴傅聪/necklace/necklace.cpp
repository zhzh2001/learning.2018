#include<bits/stdc++.h>
using namespace std;
#define maxn 50010
template<int SIZE>class union_set
{
	protected:
		int fa[SIZE+1];
	public:
		inline void init(){for(int i=1;i<=SIZE;i++) fa[i]=i;}union_set(){init();}
		inline int find(int x){return x==fa[x]?x:fa[x]=find(fa[x]);}
		inline void Union(int x,int y){fa[find(x)]=find(y);}
};
int ans=2000000000,exval;int n,m,k;
struct Edge
{
	int a,b;int val,col;
	inline void read(){scanf("%d%d%d%d",&a,&b,&val,&col);a++;b++;val<<=1;}
}e[maxn<<1];
inline bool cmp(Edge a,Edge b)
{
	int vala=a.val,valb=b.val;
	if(!a.col) vala+=exval;if(!b.col) valb+=exval;
	return vala<valb;
}
union_set<maxn> s;
inline int kruscal()
{
	s.init();sort(e+1,e+m+1,cmp);int totval=0,totcnt=0;
	for(int i=1;i<=m;i++) if(s.find(e[i].a)!=s.find(e[i].b))
	{
		s.Union(e[i].a,e[i].b);
		if(!e[i].col) totcnt++;
		totval+=e[i].val;
	}
	totval>>=1;if(totcnt==k) ans=min(ans,totval);
	return totcnt;
}
inline void init()
{
	scanf("%d%d%d",&n,&m,&k);
	for(int i=1;i<=m;i++) e[i].read();
}
int main()
{
	freopen("necklace.in","r",stdin);
	freopen("necklace.out","w",stdout);
	init();int l=-10000,r=10000;
	while(l<=r)
	{
		int mid=(l+r)>>1;exval=mid;
		int res=kruscal();if(res>=k) l=mid+1;else r=mid-1;
	}
	cout<<ans;return 0;
}
/*
2 2 1
0 1 1 1
0 1 2 0
*/
/*
3 4 1
1 2 5 0
1 2 7 1
2 3 2 0
2 3 9 1
*/
