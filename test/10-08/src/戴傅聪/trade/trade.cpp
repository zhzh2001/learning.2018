#include<bits/stdc++.h>
using namespace std;
#define maxn 40100
int head[maxn],nxt[maxn<<1],ver[maxn<<1],tot;
inline void addedge(int a,int b)
{
	nxt[++tot]=head[a];ver[tot]=b;head[a]=tot;
	nxt[++tot]=head[b];ver[tot]=a;head[b]=tot;
}
int n,p,q;
int fa[maxn],size[maxn],hson[maxn],dep[maxn];
inline void dfs1(int x,int fat)
{
	dep[x]=dep[fat]+1;size[x]=1;fa[x]=fat;int maxsize=0;
	for(int i=head[x];i;i=nxt[i])
	{
		int y=ver[i];if(y==fat) continue;
		dfs1(y,x);size[x]+=size[y];
		if(size[y]>maxsize) maxsize=size[y],hson[x]=y;
	}
}
int id[maxn],id_cnt,top[maxn];
inline void dfs2(int x,int Top)
{
	id[x]=++id_cnt;top[x]=Top;if(hson[x]) dfs2(hson[x],Top);
	for(int i=head[x];i;i=nxt[i])
	{
		int y=ver[i];if(y==fa[x]||y==hson[x]) continue;
		dfs2(y,y);
	}
}
inline void init()
{
	scanf("%d%d%d",&n,&p,&q);
	for(int i=1,a,b;i<n;i++) scanf("%d%d",&a,&b),addedge(a,b);
	dfs1(1,0);dfs2(1,1);
}
inline int lca(int a,int b)
{
	while(top[a]!=top[b])
	{
		if(dep[top[a]]<dep[top[b]]) swap(a,b);
		a=fa[top[a]];
	}
	return dep[a]<dep[b]?a:b;
}
int ux[maxn],vx[maxn],wx[maxn];
int tmp[maxn],Tot;
inline int query(int fx,int fy,int fk)
{
	int l=lca(fx,fy);Tot=0;
	for(int i=1;i<=p;i++)
	{
		int x=ux[i],y=vx[i];
		if(dep[x]<dep[l]||dep[y]<dep[l]) continue;
		bool flag1=id[fx]>=id[x]&&id[fx]<=id[x]+size[x]-1,flag2=id[fy]>=id[x]&&id[fy]<=id[x]+size[x]-1;
		if(flag1==0&&flag2==0) continue;
		flag1=id[fx]>=id[y]&&id[fx]<=id[y]+size[y]-1,flag2=id[fy]>=id[y]&&id[fy]<=id[y]+size[y]-1;
		if(flag1==0&&flag2==0) continue;
		tmp[++Tot]=wx[i];
	}
	sort(tmp+1,tmp+Tot+1);
	return tmp[fk];
}
int main()
{
	freopen("trade.in","r",stdin);
	freopen("trade.out","w",stdout);
	init();
	for(int i=1;i<=p;i++) scanf("%d%d%d",ux+i,vx+i,wx+i);
	for(int i=1;i<=q;i++)
	{
		int a,b,c;scanf("%d%d%d",&a,&b,&c);
		printf("%d\n",query(a,b,c));
	}
}
/*
6 5 8
6 1
6 2
6 3
2 5
3 4
3 1 8
4 2 6
6 3 9
3 2 9
5 3 10
3 2 2
3 6 1
1 4 2
3 5 3
1 4 1
3 5 1
4 6 1
4 2 1
*/
