#include<bits/stdc++.h>
using namespace std;
#define maxn 5100
double dis[maxn][maxn];
int x[maxn],y[maxn];int n;bool vis[maxn];double mdis[maxn];
inline void init()
{
	scanf("%d",&n);n++;
	for(int i=1;i<=n;i++) for(int j=1;j<=n;j++) dis[i][j]=1000000000000000.0;
	for(int i=2,t;i<=n;i++)
	{
		scanf("%d",&t);
		dis[1][i]=dis[i][1]=t;
	}
	for(int i=2;i<=n;i++) scanf("%d%d",x+i,y+i);
	for(int i=2;i<=n;i++)
	{
		dis[i][i]=0;double cal;
		for(int j=i+1;j<=n;j++)
		{
			cal=sqrt(1ll*(x[i]-x[j])*(x[i]-x[j])+1ll*(y[i]-y[j])*(y[i]-y[j]));
			dis[i][j]=dis[j][i]=cal;
		}
	}
}
inline void prim()
{
	for(int i=2;i<=n;i++) mdis[i]=1000000000000000.0;
	mdis[1]=0;double ans=0;memset(vis,0,sizeof(vis));
	for(int i=1;i<=n;i++)
	{
		int x=0;for(int j=1;j<=n;j++) if((!vis[j])&&(x==0||mdis[j]<mdis[x])) x=j;
		ans+=mdis[x];vis[x]=1;
		for(int j=1;j<=n;j++) if(!vis[j]) mdis[j]=min(mdis[j],dis[x][j]);
	}
	printf("%.2f",ans);
}
int main()
{
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
	init();
	prim();
}
