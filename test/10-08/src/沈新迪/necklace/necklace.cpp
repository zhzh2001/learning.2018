#include<bits/stdc++.h>
using namespace std;
long long n,m,k,ans,cnt[2],anss;
long long fa[50005];
long long f[33005][16],mp[16][16][2];
struct node
{
	long long fro,to,w;
}a[2][100005];
long long read()
{
	char ch=getchar();long long x=0,ff=1;
	while(ch<'0'||ch>'9') {if(ch=='-') ff=-1;ch=getchar();}
	while(ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x*ff;
}
void write(long long aa)
{
	if(aa<0) {putchar('-');aa=-aa;}
	if(aa>9) write(aa/10);
	putchar(aa%10+'0');
	return;
}
bool cmp(node aa,node bb)
{
	return aa.w<bb.w;
}
long long getfa(long long aa)
{
	return fa[aa]==aa?aa:fa[aa]=getfa(fa[aa]);
}
int main()
{
	freopen("necklace.in","r",stdin);
	freopen("necklace.out","w",stdout);
	n=read(),m=read();k=read();
	anss=0x3f3f3f3f;
	if(n<=15)
	{
		memset(mp,0x3f3f3f3f,sizeof(mp));
		for(long long i=1;i<=m;++i)
		{
			long long x=read(),y=read(),z=read(),c=read();
			mp[x][y][c]=min(mp[x][y][c],z);
			mp[y][x][c]=min(mp[y][x][c],z);
		}
		memset(f,0x3f3f3f3f,sizeof(f));
		for(long long i=0;i<n;++i) f[1<<i][0]=0;
		for(long long i=1;i<(1<<n);++i)
		{
			for(long long j=0;j<n;++j)
			if(i&(1<<j))
			{
				for(long long k=0;k<n;++k)
				if(!(i&(1<<k)))
				{
					for(long long qwq=0;qwq<=k;++qwq)
					{
						f[i|(1<<k)][qwq+1]=min(f[i|(1<<k)][qwq+1],f[i][qwq]+mp[j][k][0]);
						f[i|(1<<k)][qwq]=min(f[i|(1<<k)][qwq],f[i][qwq]+mp[j][k][1]);
					}
				}
			}
		}
		write(f[(1<<n)-1][k]);puts("");
		return 0;
	}
	for(long long i=1;i<=n;++i) fa[i]=i;
	for(long long i=1;i<=m;++i)
	{
		long long x=read(),y=read(),z=read(),c=read();
		a[c][++cnt[c]].fro=x;a[c][cnt[c]].to=y;
		a[c][cnt[c]].w=z;
	}
	sort(a[0]+1,a[0]+cnt[0]+1,cmp);
	sort(a[1]+1,a[1]+cnt[1]+1,cmp);
	long long oo=0,sum0=0,sum1=0;
	while(sum0<k&&oo<cnt[0])
	{
		oo++;
		long long X=getfa(a[0][oo].fro),Y=getfa(a[0][oo].to);
		if(X==Y) continue;
		fa[X]=Y;
		ans+=a[0][oo].w;
		sum0++;
	}
	oo=0;
	while(sum1<n-k-1&&oo<cnt[1])
	{
		oo++;
		long long X=getfa(a[1][oo].fro),Y=getfa(a[1][oo].to);
		if(X==Y) continue;
		fa[X]=Y;
		ans+=a[1][oo].w;
		sum1++;
	}
	if(sum0==k&&sum1==n-k-1) anss=min(anss,ans);
	for(long long i=0;i<=n;++i) fa[i]=i;
	ans=0;oo=0;sum0=0;sum1=0;
	while(sum1<n-k-1&&oo<cnt[1])
	{
		oo++;
		long long X=getfa(a[1][oo].fro),Y=getfa(a[1][oo].to);
		if(X==Y) continue;
		fa[X]=Y;
		ans+=a[1][oo].w;
		sum1++;
	}
	oo=0;
	while(sum0<k&&oo<cnt[0])
	{
		oo++;
		long long X=getfa(a[0][oo].fro),Y=getfa(a[0][oo].to);
		if(X==Y) continue;
		fa[X]=Y;
		ans+=a[0][oo].w;
		sum0++;
	}
	if(sum0==k&&sum1==n-k-1) anss=min(anss,ans);
	for(long long i=0;i<=n;++i) fa[i]=i;
	ans=0;sum0=0;sum1=0;
	long long oo0=1,oo1=1;
	while(sum1<n-k-1&&sum0<k&&(oo0<=cnt[0]||oo1<=cnt[1]))
	{
		if(oo0<=cnt[0]&&a[0][oo0].w<=a[1][oo1].w)
		{
			long long X=getfa(a[0][oo0].fro),Y=getfa(a[0][oo0].to);
			if(X==Y) continue;
			fa[X]=Y;
			ans+=a[0][oo0].w;
			sum0++;
			oo0++;
		}
		else
		{
			long long X=getfa(a[1][oo1].fro),Y=getfa(a[1][oo1].to);
			if(X==Y) continue;
			fa[X]=Y;
			ans+=a[1][oo1].w;
			sum1++;
			oo1++;
		}
	}
	if(sum0==k&&sum1==n-k-1) anss=min(anss,ans);
	write(anss);
	return 0;
}
