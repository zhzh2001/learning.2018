#include<bits/stdc++.h>
using namespace std;
long long n,p,q,cnt;
long long tot,head[400005],nx[800005],to[800005];
long long top[400005],fa[400005],id[400005],dep[400005],sz[400005],son[400005];
struct node
{
	long long fro,to,w,lc;
}a[40005];
priority_queue<long long>qq;
long long read()
{
	char ch=getchar();long long x=0,ff=1;
	while(ch<'0'||ch>'9') {if(ch=='-') ff=-1;ch=getchar();}
	while(ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x*ff;
}
void write(long long aa)
{
	if(aa<0) {putchar('-');aa=-aa;}
	if(aa>9) write(aa/10);
	putchar(aa%10+'0');
	return;
}
void jia(long long aa,long long bb)
{
	tot++;
	nx[tot]=head[aa];
	to[tot]=bb;
	head[aa]=tot;
	return;
}
void dfs(long long rt,long long faa)
{
	sz[rt]=1;
	fa[rt]=faa;
	dep[rt]=dep[faa]+1;
	long long mx=0;
	for(long long i=head[rt];i;i=nx[i])
	{
		long long yy=to[i];
		if(yy==faa) continue;
		dfs(yy,rt);
		sz[rt]+=sz[yy];
		if(sz[yy]>mx) mx=sz[yy],son[rt]=yy;
	}
	return;
}
void dfs1(long long rt)
{
	id[rt]=++cnt;
	if(son[rt]) top[son[rt]]=top[rt],dfs1(son[rt]);
	for(long long i=head[rt];i;i=nx[i])
	{
		long long yy=to[i];
		if(yy==fa[rt]||yy==son[rt]) continue;
		top[yy]=yy;
		dfs1(yy);
	}
	return;
}
long long lca(long long x,long long y)
{
	long long X=top[x],Y=top[y];
	while(X!=Y)
	if(dep[X]>dep[Y]) x=fa[X],X=top[x];
	else y=fa[Y],Y=top[y];
	if(dep[x]>dep[y]) swap(x,y);
	return x;
}
int main()
{
	freopen("trade.in","r",stdin);
	freopen("trade.out","w",stdout);
	n=read(),q=read(),p=read();
	for(long long i=1;i<n;++i)
	{
		long long x=read(),y=read();
		jia(x,y);
		jia(y,x);
	}
	dfs(1,1);
	top[1]=1;
	dfs1(1);
	for(long long i=1;i<=q;++i)
	{
		a[i].fro=read();
		a[i].to=read();
		if(dep[a[i].fro]>dep[a[i].to]) swap(a[i].fro,a[i].to);
		a[i].lc=lca(a[i].fro,a[i].to);
		a[i].w=read();
	}
	for(long long i=1;i<=p;++i)
	{
		while(!qq.empty()) qq.pop();
		long long A=read(),B=read(),k=read(),lc=lca(A,B);
		for(long long j=1;j<=q;++j)
		{
			if(a[j].fro==a[j].lc)
			{
				if(lca(a[j].fro,lc)==lc&&(lca(A,a[j].to)==a[j].to||lca(B,a[j].to)==a[j].to))
				{
					if(k) k--,qq.push(a[j].w);
					else if(qq.top()>a[j].w) qq.pop(),qq.push(a[j].w);
				}
			}
			else if(lc==a[j].lc)
			{
				if((lca(a[j].to,A)==a[j].to&&lca(a[j].fro,B)==a[j].fro)||(lca(a[j].to,B)==a[j].to&&lca(a[j].fro,A)==a[j].fro))
				{
					if(k) k--,qq.push(a[j].w);
					else if(qq.top()>a[j].w) qq.pop(),qq.push(a[j].w);
				}
			}
		}
		write(qq.top());puts("");
	}
	return 0;
}
