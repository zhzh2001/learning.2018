#include<bits/stdc++.h>
using namespace std;
long long n,S;
double ans;
long long vis[5005];
double w[5005],x[5005],y[5005];
struct node
{
	long long to;
	double w;
};
bool operator <(node aa,node bb)
{
	return aa.w>bb.w;
}
priority_queue<node>q;
long long read()
{
	char ch=getchar();long long x=0,ff=1;
	while(ch<'0'||ch>'9') {if(ch=='-') ff=-1;ch=getchar();}
	while(ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x*ff;
}
void write(long long aa)
{
	if(aa<0) {putchar('-');aa=-aa;}
	if(aa>9) write(aa/10);
	putchar(aa%10+'0');
	return;
}
node bt(long long bb,double cc)
{
	node tmp;
	tmp.to=bb;tmp.w=cc;
	return tmp;
}
double js(long long aa,long long bb)
{
	return sqrt((x[aa]-x[bb])*(x[aa]-x[bb])+(y[aa]-y[bb])*(y[aa]-y[bb]));
}
int main()
{
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
	n=read();
	S=n+1;
	vis[S]=1;
	for(long long i=1;i<=n;++i) 
	{
		scanf("%lf",&w[i]);
		q.push(bt(i,w[i]));
	}
	for(long long i=1;i<=n;++i) scanf("%lf%lf",&x[i],&y[i]);
	long long bian=0;
	while(bian<n)
	{
		node kk=q.top();
		q.pop();
		if(vis[kk.to]) continue;
		bian++;
		ans+=kk.w;
		vis[kk.to]=1;
		for(long long i=1;i<=n;++i)
		if(!vis[i]) q.push(bt(i,js(kk.to,i)));
	}
	printf("%.2lf",ans);
	return 0;
}
