#include <vector>
#include <cstdio>
#include <algorithm>
using namespace std;
const int N=50005,M=100005,inf=0x3f3f3f3f;
int n,m,k,fa[N];
struct rec{int x,w;};
vector<rec>in1[N],in2[N];
struct node{int x,y,w,c;}a[M];
inline bool cmp(node a,node b){return a.w<b.w;}
inline int Find(int x){while(x!=fa[x])x=fa[x]=fa[fa[x]];return x;}
int main()
{
	freopen("necklace.in","r",stdin);
	freopen("necklace.out","w",stdout);
	int i,j,x,y,z,c,up,cnt=0,num=0,mi; rec tmp; long long re=0; scanf("%d%d%d",&n,&m,&k); up=n-1-k; for(i=1;i<=n;i++)fa[i]=i;
	for(i=1;i<=m;i++)
	{
		scanf("%d%d%d%d",&x,&y,&z,&c); x++; y++; a[i]=(node){x,y,z,c}; if(c) in2[x].push_back((rec){y,z}),in2[y].push_back((rec){x,z});else in1[x].push_back((rec){y,z}),in1[y].push_back((rec){x,z});
	}for(i=1;i<=n;i++)if(!in1[i].size()){mi=inf; for(j=0;j<in2[i].size();j++){if(in2[i][j].w>mi)tmp=in2[i][j];} fa[Find(i)]=Find(tmp.x); re+=tmp.w;} sort(a+1,a+m+1,cmp);
	for(i=1;i<=m;i++)
	{
		x=Find(a[i].x); y=Find(a[i].y); if(x!=y&&num+a[i].c<=up){fa[x]=y;re+=1LL*a[i].w;if(++cnt==n-1)break;}
	}printf("%lld\n",re);
}
