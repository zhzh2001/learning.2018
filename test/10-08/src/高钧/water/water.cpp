#include <cmath>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
const int N=5005; const double inf=9999999999.99999999;
int n; double dis[N],w[N][N],pp[N]; bool bo[N]; struct Pot{double x,y;}p[N];
inline double js(double x,double y,double xx,double yy){return (double)sqrt((x-xx)*(x-xx)+(y-yy)*(y-yy));}
int main()
{
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
	int i,j,cnt=0; double re=0,mi; scanf("%d",&n); for(i=1;i<=n;i++)scanf("%lf",&pp[i]); memset(bo,0,sizeof bo);
	for(i=1;i<=n;i++)scanf("%lf%lf",&p[i].x,&p[i].y); memset(dis,127,sizeof dis); for(i=1;i<=n;i++)dis[i]=pp[i];
	for(i=1;i<=n;i++)for(j=1;j<=n;j++)if(i!=j)w[i][j]=js(p[i].x,p[i].y,p[j].x,p[j].y); bo[0]=1;
	while(cnt<n)
	{
		mi=inf; for(i=1;i<=n;i++)if(mi>dis[i]&&!bo[i]){mi=dis[i];j=i;} cnt++; re+=dis[j]; bo[j]=1; for(i=1;i<=n;i++)if(!bo[i])dis[i]=min(dis[i],w[j][i]);
	}printf("%.2lf\n",re);
}
