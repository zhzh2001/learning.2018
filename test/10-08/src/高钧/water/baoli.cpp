#include <cmath>
#include <cstdio>
#include <algorithm>
using namespace std;
const int N=5005; const double eps=1e-9;
struct node{int x,y;double w;}a[N*N+N*2];
inline bool cmp(node a,node b){return a.w<b.w;}
struct Pot{double x,y;}p[N];
int n,fa[N];double w[N];
inline double js(double x,double y,double xx,double yy){return (double)sqrt((x-xx)*(x-xx)+(y-yy)*(y-yy));}
inline int Find(int x){while(x!=fa[x])x=fa[x]=fa[fa[x]];return x;}
int main()
{
	freopen("water1.in","r",stdin);
//	freopen("pp.in","r",stdin);
	int i,j,cnt=0,x,y,po=0; double re=0.00; scanf("%d",&n); for(i=1;i<=n;i++)scanf("%lf",&w[i]),fa[i]=i; fa[0]=0;
	for(i=1;i<=n;i++)
	{
		scanf("%lf%lf",&p[i].x,&p[i].y); a[++cnt].x=0; a[cnt].y=i; a[cnt].w=w[i];
	}for(i=1;i<=n;i++)for(j=1;j<=n;j++)if(i!=j)a[++cnt].x=i,a[cnt].y=j,a[cnt].w=js(p[i].x,p[i].y,p[j].x,p[j].y); sort(a+1,a+cnt+1,cmp);
	for(i=1;i<=cnt;i++)
	{
		x=Find(a[i].x); y=Find(a[i].y); if(x!=y){fa[x]=y; re+=(double)a[i].w; if(++po==n)break;}
	}printf("%.2lf\n",re);
}
