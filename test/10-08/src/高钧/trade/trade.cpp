#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
const int N=3005;
int n,pp,qq,tot=0,Next[N*2],to[N*2],head[N*2],q[N],depth[N],fa[N][23],px[N],py[N],pe[N],a[N],bo[N];
inline void add(int x,int y){Next[++tot]=head[x];to[tot]=y;head[x]=tot;}
inline void bfs(int s)
{
	int i,j,x,tou=1,wei=2; q[tou]=s; depth[s]=1; for(i=19;~i;i--)fa[s][i]=s; fa[s][0]=s;
	while(tou<wei){x=q[tou++];for(i=head[x];i;i=Next[i]){if(to[i]!=fa[x][0]){depth[to[i]]=depth[x]+1; fa[to[i]][0]=x; for(j=1;j<=19;j++)fa[to[i]][j]=fa[fa[to[i]][j-1]][j-1]; q[wei++]=to[i];}}}
}
inline int ask(int x,int y){int i; if(depth[x]<depth[y])swap(x,y);for(i=19;~i;i--)if(depth[fa[x][i]]>=depth[y])x=fa[x][i];if(x==y)return x;for(i=19;~i;i--)if(fa[x][i]!=fa[y][i])x=fa[x][i],y=fa[y][i]; return fa[x][0];}
int main()
{
	freopen("trade.in","r",stdin);
	freopen("trade.out","w",stdout);
	int ll,dd,oo,i,j,x,y,k,cnt=0; scanf("%d%d%d",&n,&pp,&qq); memset(head,0,sizeof head);
	for(i=1; i<n; i++)
	{
		scanf("%d%d",&x,&y); add(x,y); add(y,x);
	}bfs(1); for(i=1;i<=pp;i++)scanf("%d%d%d",&px[i],&py[i],&pe[i]);
	for(i=1;i<=qq;i++)
	{
		scanf("%d%d%d",&x,&y,&k); ll=ask(x,y); memset(bo,0,sizeof bo); cnt=0; bo[ll]=bo[x]=bo[y]=1;
		while(x!=ll){bo[x]=1;x=fa[x][0];}while(y!=ll){bo[y]=1;y=fa[y][0];}
		for(j=1;j<=pp;j++)if(bo[px[j]]&&bo[py[j]])a[++cnt]=pe[j]; sort(a+1,a+cnt+1); printf("%d\n",a[k]);
	}
}
