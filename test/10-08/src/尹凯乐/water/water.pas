program water;
 var
  f:Array[0..5001] of int64;
  d,k:array[0..5001] of real;
  x,y:Array[0..5001] of longint;
  i,j,n,p:longint;
  o,ssum:real;
 begin
  assign(input,'water.in');
  assign(output,'water.out');
  reset(input);
  rewrite(output);
  readln(n);
  for i:=1 to n do
   readln(d[i]);
  {for i:=0 to n do
   d[i]:=10082088200000;}
  d[0]:=10082088200000;
  for i:=1 to n do
   readln(x[i],y[i]);
  fillqword(f,sizeof(f)>>3,0);
  for i:=1 to n do
   begin
    p:=0;
    for j:=1 to n do
     if (f[j]=0) and (d[p]>=d[j]) then p:=j;
    f[p]:=1;
    for j:=1 to n do
     if f[j]=0 then
      begin
       o:=sqrt(sqr(int64(x[p]-x[j]))+sqr(int64(y[p]-y[j])));
       if d[j]>=o then d[j]:=o;
      end;
   end;
  ssum:=0;
  for i:=1 to n do
   ssum:=ssum+d[i];
  writeln(ssum:0:2);
  close(input);
  close(output);
 end.
