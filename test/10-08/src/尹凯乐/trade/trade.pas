program trade;
 type
  wb=record
   n:^wb;
   t:longint;
  end;
  wwb=^wb;
  wcg=record
   x,y,z,h,s,l,r:longint;
  end;
  wtree=record
   l,r:^wtree;
   s:longint;
  end;
  wwtree=^wtree;
  wh=record
   x,y,z,t,w:longint;
  end;
 var
  b:array[0..80001] of wb;
  cg,cgx,cgy:array[0..40001] of wcg;
  hc:array[0..40001] of wh;
  f:array[0..40001,0..17] of longint;
  h,d,w,wg,num,cgh,nx,ny:array[0..40001] of longint;
  zkw:array[0..323456] of longint;
  i,j,n,p,q,pc,x,y,root,ssum,nmax,tsum,qn:longint;
  //root:longint;
 procedure hahainc(x,y:longint);
  begin
   inc(pc);
   b[pc].n:=@b[h[x]];
   b[pc].t:=y;
   h[x]:=pc;
  end;
 procedure hahaDFS(k,x,s:longint);
  var
   i:longint;
   j:wwb;
  begin
   inc(ssum);
   num[k]:=ssum;
   f[k,0]:=x;
   d[k]:=s;
   j:=@b[h[k]];
   while j<>@b[0] do
    begin
     i:=j^.t;
     j:=j^.n;
     if i=x then continue;
     hahaDFS(i,k,s+1);
    end;
  end;
 procedure haha(l,r,cgh:longint);
  var
   o,p:longint;
   m,t:wcg;
  function hahacmp(x,y:wcg):boolean;
   begin
    if cgh=1 then exit(x.z<y.z);
    if cgh=2 then exit(x.x<y.x);
    if cgh=3 then exit(x.y<y.y);
   end;
  procedure hahaswap(var x,y:wcg);
   begin
    t:=x;
    x:=y;
    y:=t;
   end;
  begin
   o:=l;
   p:=r;
   m:=cg[(l+r)>>1];
   repeat
    while hahacmp(cg[o],m) do inc(o);
    while hahacmp(m,cg[p]) do dec(p);
    if o>p then break;
    //hahaswap(cgx[o],cgx[p]);
    //hahaswap(cgy[o],cgy[p]);
    hahaswap(cg[o],cg[p]);
    inc(o);
    dec(p);
   until o>p;
   if o<r then haha(o,r,cgh);
   if l<p then haha(l,p,cgh);
  end;
 procedure hahabuild;
  begin
   tsum:=1;
   while tsum<=nmax+1 do
    tsum:=tsum<<1;
  end;
 procedure hahadid(k,x:longint);
  //var
  // t:longint;
  begin
   k:=k+tsum;
   while k>0 do
    begin
     inc(zkw[k],x);
     k:=k>>1;
    end;
  end;
 function hahaask(s,k:longint):longint;
  begin
   if s>tsum then exit(s-tsum);
   if zkw[s<<1]>=k then exit(hahaask(s<<1,k));
   exit(hahaask(s<<1+1,k-zkw[s<<1]));
  end;
 procedure hahaha(l,r:longint);
  var
   o,p:longint;
   m,t:wh;
  function hahacmp(x,y:wh):boolean;
   begin
    //if cgh=1 then exit(x.z<y.z);
    //if cgh=2 then exit(x.x<y.x);
    //if cgh=3 then exit(x.y<y.y);
    if x.t<>y.t then exit(x.t<y.t);
    if y.y and 1=1 then exit(x.y>y.y)
                   else exit(x.y<y.y);
   end;
  procedure hahaswap(var x,y:wh);
   begin
    t:=x;
    x:=y;
    y:=t;
   end;
  begin
   o:=l;
   p:=r;
   m:=hc[(l+r)>>1];
   repeat
    while hahacmp(hc[o],m) do inc(o);
    while hahacmp(m,hc[p]) do dec(p);
    if o>p then break;
    //hahaswap(cgx[o],cgx[p]);
    //hahaswap(cgy[o],cgy[p]);
    hahaswap(hc[o],hc[p]);
    inc(o);
    dec(p);
   until o>p;
   if o<r then hahaha(o,r);
   if l<p then hahaha(l,p);
  end;
 procedure hahaswap(var x,y:longint);
  var
   t:longint;
  begin
   t:=x;
   x:=y;
   y:=t;
  end;
 function hahalca(x,y:longint):longint;
  var
   i,j:longint;
  begin
   if d[x]<=d[y] then hahaswap(x,y);
   for i:=16 downto 0 do
    if d[f[y,i]]>=d[x] then y:=f[y,i];
   for i:=16 downto 0 do
    if f[x,i]<>f[y,i] then
     begin
      x:=f[x,i];
      y:=f[y,i];
     end;
   if x=y then exit(x);
   exit(f[y,0]);
  end;
 procedure hahabilix(x,k:longint);
  var
   i,j:longint;
  begin
   if k=1 then
    for i:=nx[x] to nx[x+1]-1 do
     begin
      //writeln(i);
      j:=cgx[i].h;
      writeln(j);
      cg[j].l:=1;
      if cg[j].r=1 then hahadid(cg[j].s,1);
     end;
   if k=2 then
    for i:=ny[y] to ny[y+1]-1 do
     begin
      //writeln(i);
      j:=cgy[i].h;
      cg[j].r:=1;
      if cg[j].l=1 then hahadid(cg[j].s,1);
     end;
   if k=3 then
    for i:=nx[x] to nx[x+1]-1 do
     begin
      j:=cgx[i].h;
      if cg[j].l+cg[j].r=2 then hahadid(cgx[j].s,-1);
      cgx[j].l:=0;
     end;
   if k=4 then
    for i:=ny[y] to ny[y+1]-1 do
     begin
      j:=cgy[i].h;
      if cg[j].r+cg[j].l=2 then hahadid(cgy[j].s,-1);
      cg[j].r:=0;
     end;
  end;
 procedure hahacheck;
  var
   i,j,k:longint;
  begin
   //for i:=1 to n-1 do
    //write(f[i],' ');
   //writeln(f[n]);
   //for i:=1 to p do
    //writeln(nx[i],' ',ny[i]);
   writeln(x,' ',y);
  end;
 begin
  randomize;
  ssum:=0;
  readln(n,p,q);
  pc:=0;
  filldword(h,sizeof(H)>>2,0);
  for i:=1 to n-1 do
   begin
    readln(x,y);
    hahainc(x,y);
    hahainc(y,x);
   end;
  ///root:=random(n)+1;
  root:=1;
  hahaDFS(root,0,0);
  //hahacheck;
  for i:=1 to p do
   readln(cg[i].x,cg[i].y,cg[i].z);
  for i:=1 to p do
   begin
    if cg[i].x>cg[i].y then hahaswap(cg[i].x,cg[i].y);
    cg[i].h:=i;
   end;
  haha(1,p,2);
  cgx:=cg;
  for i:=1 to p do
   if cgx[i].x<>cgx[i-1].x then
    for j:=cgx[i-1].x+1 to cgx[i].x do
     nx[j]:=i;
  for j:=cgx[p].x+1 to n+1 do
   nx[j]:=p+1;
  haha(1,p,3);
  cgy:=cg;
  for i:=1 to p do
   if cgy[i].y<>cgy[i-1].y then
    for j:=cgy[i-1].y+1 to cgy[i].y do
     ny[j]:=i;
  for j:=cgy[p].y+1 to n+1 do
   ny[j]:=p+1;
  haha(1,p,1);
  //filldword(cgh,sizeof(cgh)>>2,0);
  //hahacheck;
  cg[1].s:=1;
  for i:=2 to p do
   if cg[i].z=cg[i-1].z then cg[i].s:=cg[i-1].s
                        else cg[i].s:=cg[i-1].s+1;
  for i:=1 to p do
   wg[cg[i].s]:=cg[i].z;
  nmax:=cg[p].s;
  hahabuild;
  for i:=1 to q do
   begin
    readln(hc[i].x,hc[i].y,hc[i].z);
    if hc[i].x>hc[i].y then hahaswap(hc[i].x,hc[i].y);
   end;
  qn:=trunc(sqrt(q));
  for i:=1 to q do
   begin
    hc[i].t:=hc[i].x div qn;
    hc[i].w:=i;
   end;
  hahaha(1,q);
  x:=hc[1].x;
  y:=hc[1].y;
  hahacheck;
  while x>0 do
   begin
    hahabilix(x,1);
    x:=f[x,0];
   end;
  while y>0 do
   begin
    hahabilix(y,2);
    y:=f[y,0];
   end;
  //hahacheck;
  qn:=hahaask(1,hc[i].z);
  writeln(wg[qn]);
 end.
