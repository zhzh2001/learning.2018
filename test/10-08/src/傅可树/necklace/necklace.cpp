#include<cstdio>
#include<algorithm>
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=50005,M=100005,INF=1e9;
struct edge{
	int u,v,w,col;
}e[M];
int n,m,k;
inline bool cmp(edge A,edge B){
	return A.w<B.w;
}
inline void init(){
	n=read(); m=read(); k=read();
	for (int i=1;i<=m;i++){
		e[i].u=read()+1; e[i].v=read()+1; e[i].w=read(); e[i].col=read();
	}
	sort(e+1,e+1+m,cmp);
}
int fa[N];
int getfa(int x){
	return (fa[x]==x)?x:fa[x]=getfa(fa[x]);
}
int ans,cnt1,cnt2;
inline void solve1(){
	int sum=0;
	for (int i=1;i<=n;i++) fa[i]=i;
	for (int i=1;i<=m;i++){
		if (cnt1+(e[i].col==0)>k) continue; 
		if (cnt2+(e[i].col==1)>n-1-k) continue; 
		int p=getfa(e[i].u),q=getfa(e[i].v);
		if (p!=q){
			fa[p]=q; sum+=e[i].w;
			cnt1+=(e[i].col==0);
			cnt2+=(e[i].col==1);
		}
	}
	ans=sum;
}
inline void solve(){
	solve1();
	writeln(ans);
}
int main(){
	freopen("necklace.in","r",stdin); freopen("necklace.out","w",stdout);
	init(); solve(); return 0;
}
