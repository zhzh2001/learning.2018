#pragma GCC optimize ("O2")
#include<cstdio>
#include<algorithm>
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=4e4+5;
struct edge{
	int link,next;
}e[N<<1];
int n,p,q,head[N],tot;
inline void add_edge(int u,int v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
}
inline void insert(int u,int v){
	add_edge(u,v); add_edge(v,u);
}
inline void init(){
	n=read(); p=read(); q=read();
	for (int i=1;i<n;i++){
		int u=read(),v=read(); insert(u,v);
	}
}
int dep[N],Fa[N][17];
void dfs(int u,int fa){
	Fa[u][0]=fa; dep[u]=dep[fa]+1;
	for (int i=1;i<17;i++){
		Fa[u][i]=Fa[Fa[u][i-1]][i-1];
	}
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=fa){
			dfs(v,u);
		}
	}
}
inline int LCA(int u,int v){
	if (dep[u]<dep[v]) swap(u,v);
	int delta=dep[u]-dep[v];
	for (int i=0;i<17;i++){
		if (delta&(1<<i)){
			u=Fa[u][i];
		}
	}
	for (int i=16;i>=0;i--){
		if (Fa[u][i]!=Fa[v][i]){
			u=Fa[u][i]; v=Fa[v][i];
		}
	}
	if (u!=v) return Fa[u][0]; else return u;
}
inline bool is_anc(int u,int v){
	if (dep[u]<dep[v]) return 0;	
	int delta=dep[u]-dep[v];
	for (int i=0;i<17;i++){
		if (delta&(1<<i)){
			u=Fa[u][i];
		}
	}
	if (u!=v) return 0; else return 1;
}
struct pp{
	int u,v,w;
}a[N];
int cnt;
inline bool cmp(pp A,pp B){
	return A.w<B.w;
}
inline void solve(){
	dfs(1,0);
	for (int i=1;i<=p;i++){
		a[i]=(pp){read(),read(),read()};
	}
	sort(a+1,a+1+p,cmp);
	for (int i=1;i<=q;i++){
		int u=read(),v=read(),k=read(); cnt=0; int lca=LCA(u,v);
		for (int j=1;j<=p;j++){
			if (is_anc(a[j].u,lca)&&is_anc(a[j].v,lca)){
				if (is_anc(u,a[j].u)||is_anc(v,a[j].u)){
					if (is_anc(u,a[j].v)||is_anc(v,a[j].v)){
						++cnt;
						if (cnt==k) {
							writeln(a[j].w);
							break;
						}
					}
				}
			}
		}
	}
}
int main(){
	freopen("trade.in","r",stdin); freopen("trade.out","w",stdout);
	init(); solve();
	return 0;
}
