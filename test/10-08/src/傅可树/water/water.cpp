#include<cstdio>
#include<algorithm>
#include<cmath>
using namespace std;
typedef long long ll;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=5005;
struct node{
	int x,y;
}a[N];
int n,w[N];
inline void init(){
	n=read();
	for (int i=1;i<=n;i++){
		w[i]=read();
	}
	for (int i=1;i<=n;i++){
		a[i].x=read(); a[i].y=read();
	}
}
inline double sqr(double x) {
	return x*x;
} 
inline double Dis(int i,int j){
	return sqrt(sqr(a[i].x-a[j].x)+sqr(a[i].y-a[j].y));
}
double INF,ans,d[N][N],dis[N];
inline double prim(){
	INF=1e9;
	for (int i=0;i<=n;i++){
		dis[i]=d[0][i];
	}
	dis[0]=-INF;
	for (int i=0;i<n;i++){
		int id=0; double mn=INF;
		for (int j=0;j<=n;j++){
			if(dis[j]!=-INF&&mn>dis[j]) mn=dis[j],id=j;
		}
		ans+=dis[id]; dis[id]=-INF;
		for (int j=0;j<=n;j++){
			if (d[id][j]<dis[j]) dis[j]=d[id][j];
		}
	}
	return ans;
}
inline void solve(){
	int S=0;
	for (int i=1;i<n;i++){
		for (int j=i+1;j<=n;j++){
			d[i][j]=d[j][i]=Dis(i,j);
		}
	}
	for (int i=1;i<=n;i++) d[i][S]=d[S][i]=w[i];
	printf("%.2lf\n",prim());
}
int main(){
	freopen("water.in","r",stdin); freopen("water.out","w",stdout);
	init(); solve();
	return 0;
}
