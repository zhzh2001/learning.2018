//#include<iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <fstream>
#include <queue>
//#define cin fin
//#define cout fout
using namespace std;
ifstream fin("prime.in");
ofstream fout("prime.out");
const int inf = 0x3f3f3f3f;
struct Edge
{
	int v, f, next;
} edge[9100003];
int n, m, cnt;
int first[30003], level[30003];
int q[30003];
int prime[200003];
inline bool pd(int x)
{
	for (int i = 2; i * i <= x; i++)
	{
		if (x % i == 0)
		{
			return 1;
		}
	}
	return 0;
}
inline void add(int u, int v, int f)
{
	edge[cnt].v = v, edge[cnt].f = f;
	edge[cnt].next = first[u], first[u] = cnt++;
	edge[cnt].v = u, edge[cnt].f = 0;
	edge[cnt].next = first[v], first[v] = cnt++;
}
int bfs(int s, int t)
{
	memset(level, 0, sizeof(level));
	level[s] = 1;
	int front = 0, rear = 1;
	q[front] = s;
	while (front < rear)
	{
		int x = q[front++];
		if (x == t)
			return 1;
		for (int e = first[x]; e != -1; e = edge[e].next)
		{
			int v = edge[e].v, f = edge[e].f;
			if (!level[v] && f)
			{
				level[v] = level[x] + 1;
				q[rear++] = v;
			}
		}
	}
	return 0;
}
int dfs(int u, int maxf, int t)
{
	if (u == t)
		return maxf;
	int ret = 0;
	for (int e = first[u]; e != -1; e = edge[e].next)
	{
		int v = edge[e].v, f = edge[e].f;
		if (level[u] + 1 == level[v] && f)
		{
			int Min = min(maxf - ret, f);
			f = dfs(v, Min, t);
			edge[e].f -= f;
			edge[e ^ 1].f += f;
			ret += f;
			if (ret == maxf)
				return ret;
		}
	}
	return ret;
}
inline int dinic(int s, int t)
{
	int ans = 0;
	while (bfs(s, t))
		ans += dfs(s, inf, t);
	return ans;
}
int S, T, sum, num[3003];
int main()
{
	memset(first, -1, sizeof(first));
	fin >> n;
	sum = n;
	for (int i = 1; i <= n; i++)
	{
		fin >> num[i];
	}
	for (int i = 1; i <= 200000; i++)
	{
		prime[i] = 1;
		for (int j = 2; j * j <= i; j++)
		{
			if (i % j == 0)
			{
				prime[i] = 0;
				break;
			}
		}
	}
	S = n + 1, T = n + 2;
	bool num1 = false;
	for (int i = 1; i <= n; i++)
	{
		if (num1 && num[i] == 1)
		{
			sum--;
			continue;
		}
		if (num[i] == 1)
		{
			num1 = true;
		}
		if (num[i] % 2 == 1)
		{
			add(S, i, 1);
		}
		if (num[i] % 2 == 0)
		{
			add(i, T, 1);
		}
		for (int j = 1; j <= n; j++)
		{
			if (num[i] % 2 == 1 && num[j] % 2 == 0 && prime[num[i] + num[j]])
			{
				add(i, j, inf);
			}
		}
	}
	int ans = dinic(S, T);
	fout << sum - ans << endl;
	return 0;
}
/*

in:
10
1 1 1 1 1 2 3 4 6 10

out:
2 4 6 10

*/
