//#include<iostream>
#include<stdio.h>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
#include<set>
#define fin cin
#define fout cout
using namespace std;
ifstream fin("list.in");
ofstream fout("list.out");
struct tree{
	int mx,sum;
}t[400003];
int n,num[100003];
void build(int rt,int l,int r){
	if(l==r){
		t[rt].mx=0,t[rt].sum=0;
		return;
	}
	int mid=(l+r)/2;
	build(rt*2,l,mid),build(rt*2+1,mid+1,r);
	t[rt].mx=max(t[rt*2].mx,t[rt*2+1].mx);
	t[rt].sum=t[rt*2].sum+t[rt*2+1].sum;
}
int querymx(int rt,int l,int r,int x,int y){
	if(l==x&&r==y){
		return t[rt].mx;
	}
	int mid=(l+r)/2;
	if(y<=mid){
		return querymx(rt*2,l,mid,x,y);
	}else if(x>mid){
		return querymx(rt*2+1,mid+1,r,x,y);
	}else{
		return max(querymx(rt*2,l,mid,x,mid),querymx(rt*2+1,mid+1,r,mid+1,y));
	}
}
int querysum(int rt,int l,int r,int x,int y){
	if(l==x&&r==y){
		return t[rt].sum;
	}
	int mid=(l+r)/2;
	if(y<=mid){
		return querysum(rt*2,l,mid,x,y);
	}else if(x>mid){
		return querysum(rt*2+1,mid+1,r,x,y);
	}else{
		return querysum(rt*2,l,mid,x,mid)+querysum(rt*2+1,mid+1,r,mid+1,y);
	}
}
void update(int rt,int l,int r,int x,int y){
	if(l==r){
		t[rt].mx=y,t[rt].sum++;
		return;
	}
	int mid=(l+r)/2;
	if(x<=mid){
		update(rt*2,l,mid,x,y);
	}else{
		update(rt*2+1,mid+1,r,x,y);
	}
	t[rt].mx=max(t[rt*2].mx,t[rt*2+1].mx);
	t[rt].sum=t[rt*2].sum+t[rt*2+1].sum;
}
void add(int rt,int l,int r,int x,int y){
	if(l==r){
		t[rt].sum+=y;
		return;
	}
	int mid=(l+r)/2;
	if(x<=mid){
		add(rt*2,l,mid,x,y);
	}else{
		add(rt*2+1,mid+1,r,x,y);
	}
	t[rt].sum=t[rt*2].sum+t[rt*2+1].sum;
}
int last[200003];
struct qqq{
	int opt,pos,nxt;
}qq[400003];
inline bool cmp(qqq a,qqq b){
	if(a.opt==b.opt){
		return a.pos>b.pos;
	}else{
		return a.opt<b.opt;
	}
}
struct wtf{
	int now,nxt;
	bool operator <(const wtf &a)const{
		return qq[now].opt<qq[a.now].opt;
		if(a.nxt==0){
			if(nxt==0){
				return qq[now].opt<qq[a.now].opt;
			}else{
				return nxt>a.nxt;
			}
		}else{
			if(nxt==0){
				return nxt>a.nxt;
			}else{
				return qq[now].opt<qq[a.now].opt;
			}
		}
	}
};
wtf sx;
set<wtf> s;
int main(){
	fin>>n;
	for(int i=1;i<=n;i++){
		fin>>num[i];
		qq[i].pos=i;
		qq[i].opt=num[i];
	}
	for(int i=n;i>=1;i--){
		qq[i].nxt=last[num[i]];
		last[num[i]]=i;
	}
	memset(last,0,sizeof(last));
	for(int i=1;i<=n;i++){
		if(!last[num[i]]){
			sx.now=i,sx.nxt=qq[i].nxt;
			if(sx.nxt!=0){
				s.insert(sx);
			}
			last[num[i]]=1;
		}
	}
	for(int i=n;i>=1;i--){
		if(s.empty()){
			break;
		}
		wtf x=*(s.begin());
		num[x.now]=i;
		s.erase(s.find(x));
		x.now=x.nxt,x.nxt=qq[x.nxt].nxt;
		if(x.nxt!=0){
			s.insert(x);
		}
	}
	long long ans=0;
	build(1,1,n);
	for(int i=n;i>=1;i--){
		int now=querymx(1,1,n,1,num[i]);
		ans+=(now+1);
		update(1,1,n,num[i],now+1);
	}
	fout<<ans<<endl;
	return 0;
}
/*

in:
4
1 2 2 3

out:
5

*/
