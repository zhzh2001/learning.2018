//#include<iostream>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
#include<map>
#include<set>
#define fin cin
#define fout cout
using namespace std;
ifstream fin("seat.in");
ofstream fout("seat.out");
struct my{
	int l,r;
	bool operator <(const my &a)const{
		if(a.r-a.l==r-l){
			return l>a.l;
		}else{
			return r-l>a.r-a.l;
		}
	}
};
set<my> s;
set<int>s2;
map<int,int> mp;
map<int,pair<int,int> >mp2;
struct qq{
	int opt,l,r;
}qq[200003];
int n,q;
my sx;
int pos[400003];
inline int calc(int x,int y){
	if((y-x+1)%2==1){
		return (x+y)/2;
	}else{
		return (x+y)/2+1;
	}
}
struct tree{
	int sum;
}t[1600003];
int ans[200003];
void build(int rt,int l,int r){
	if(l==r){
		t[rt].sum=0;
		return;
	}
	int mid=(l+r)/2;
	build(rt*2,l,mid),build(rt*2+1,mid+1,r);
}
int querysum(int rt,int l,int r,int x,int y){
	if(l==x&&r==y){
		return t[rt].sum;
	}
	int mid=(l+r)/2;
	if(y<=mid){
		return querysum(rt*2,l,mid,x,y);
	}else if(x>mid){
		return querysum(rt*2+1,mid+1,r,x,y);
	}else{
		return querysum(rt*2,l,mid,x,mid)+querysum(rt*2+1,mid+1,r,mid+1,y);
	}
}
void update(int rt,int l,int r,int x,int y){
	if(l==r){
		t[rt].sum+=y;
		return;
	}
	int mid=(l+r)/2;
	if(x<=mid){
		update(rt*2,l,mid,x,y);
	}else{
		update(rt*2+1,mid+1,r,x,y);
	}
	t[rt].sum=t[rt*2].sum+t[rt*2+1].sum;
}
int main(){
	fin>>n>>q;
	for(int i=1;i<=q;i++){
		fin>>qq[i].opt;
		if(qq[i].opt==0){
			fin>>qq[i].l>>qq[i].r;
		}
	}
	sx.l=1,sx.r=n;
	s.insert(sx);
	for(int i=1;i<=q;i++){
		if(qq[i].opt!=0){
			if(mp[qq[i].opt]==1){
				int ll=mp2[qq[i].opt].first,rr=mp2[qq[i].opt].second,mid;
				mid=calc(ll,rr);
				ans[i]=mid;
				if(ll<=mid-1){
					sx.l=ll,sx.r=mid-1;
					s.erase(s.find(sx));
				}
				if(mid+1<=rr){
					sx.l=mid+1,sx.r=rr;
					s.erase(s.find(sx));
				}
				sx.l=ll,sx.r=rr;
				s.insert(sx);
			}else{
				int ll,rr,mid;
				sx=*s.begin();
				ll=sx.l,rr=sx.r;
				mp[qq[i].opt]=1;
				mp2[qq[i].opt].first=ll,mp2[qq[i].opt].second=rr;
				mid=calc(ll,rr);
				s.erase(s.find(sx));
				if(ll<=mid-1){
					sx.l=ll,sx.r=mid-1;
					s.insert(sx);
				}
				if(mid+1<=rr){
					sx.l=mid+1,sx.r=rr;
					s.insert(sx);
				}
				pos[++pos[0]]=mid;
				ans[i]=mid;
			}
		}else{
			pos[++pos[0]]=qq[i].l;
			pos[++pos[0]]=qq[i].r;
		}
	}
	sort(pos+1,pos+pos[0]+1);
	int len=unique(pos+1,pos+pos[0]+1)-pos-1;
	mp.clear();
	for(int i=1;i<=len;i++){
		mp2[pos[i]].first=i;
		s2.insert(pos[i]);
	}
	for(int i=1;i<=q;i++){
		if(qq[i].opt!=0){
			if(mp[qq[i].opt]){
				int x=*s2.lower_bound(ans[i]);
				update(1,1,len,mp2[x].first,-1);
				mp[qq[i].opt]=0;
			}else{
				int x=*s2.lower_bound(ans[i]);
				update(1,1,len,mp2[x].first,1);
				mp[qq[i].opt]=1;
			}
		}else{
			int l=*s2.lower_bound(qq[i].l),r=*s2.lower_bound(qq[i].r);
			fout<<querysum(1,1,len,mp2[l].first,mp2[r].first)<<endl;
		}
	}
	return 0;
}
/*
7 5
1 
2 
3
20
99

in:
7 10
1 
2 
3
0 1 2
0 4 7
0 2 5
20
0 6 6
99
0 4 6

out:
1
2
2
1
3

*/
