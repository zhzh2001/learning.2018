#include<bits/stdc++.h>
#define N 3005
using namespace std;
struct edge{
	int to,next,v;
}e[N*2000];
int head[N],tot=1;
int dis[N],q[N],cur[N];
int a[N],n,S,T,fl[233333];
void add(int x,int y,int v){
	e[++tot]=(edge){y,head[x],v};
	head[x]=tot;
	e[++tot]=(edge){x,head[y],0};
	head[y]=tot;
}
bool spfa(){
	for (int i=1;i<=T;i++)
		dis[i]=-1;
	int h=0,t=1;
	dis[S]=0; q[1]=S;
	while (h!=t){
		int x=q[++h];
		for (int i=head[x];i;i=e[i].next)
			if (e[i].v&&dis[e[i].to]==-1){
				dis[e[i].to]=dis[x]+1;
				if (e[i].to==T) return 1;
				q[++t]=e[i].to;
			}
	}
	return 0;
}
int dinic(int x,int flow){
	if (x==T) return flow;
	int k,rest=flow;
	for (int i=cur[x];i&&rest;i=e[i].next){
		cur[x]=i;
		if (dis[e[i].to]==dis[x]+1&&e[i].v){
			k=dinic(e[i].to,min(rest,e[i].v));
			e[i].v-=k; e[i^1].v+=k; rest-=k;
		}
	}
	if (rest) dis[x]=-1;
	return flow-rest;
}
int main(){
	freopen("prime.in","r",stdin);
	freopen("prime.out","w",stdout);
	int M=200000;
	fl[1]=fl[0]=1;
	for (int i=2;i<=M/2;i++)
		if (!fl[i])
			for (int j=2*i;j<=M;j+=i)
				fl[j]=1;
	scanf("%d",&n);
	for (int i=1;i<=n;i++)
		scanf("%d",&a[i]);
	S=n+1; T=S+1;
	for (int i=1;i<=n;i++)
		if (a[i]&1){
			add(S,i,1);
			for (int j=1;j<=n;j++)
				if (!(a[j]&1)&&!fl[a[i]+a[j]])
					add(i,j,1);
		}
		else add(i,T,1);
	int ans=n;
	while (spfa()){
		for (int i=1;i<=T;i++) cur[i]=head[i];
		ans-=dinic(S,1e9);
	}
	printf("%d",ans);
}
