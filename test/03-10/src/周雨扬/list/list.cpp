#include<bits/stdc++.h>
#define N 100005
using namespace std;
int a[N],deg[N];
int h[N],f[N];
int n,tot;
int head[N];
struct edge{
	int to,next;
}e[N*2];
void add(int x,int y){
	e[++tot]=(edge){y,head[x]};
	head[x]=tot; deg[y]++;
}
int main(){
	freopen("list.in","r",stdin);
	freopen("list.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++){
		int x;
		scanf("%d",&x);
		if (a[x]) add(a[x],i);
		if (a[x-1]) add(i,a[x-1]);
		a[x]=i;
	}
	priority_queue<int> q;
	for (int i=1;i<=n;i++)
		if (!deg[i]) q.push(-i);
	int now=n;
	while (!q.empty()){
		int x=-q.top(); q.pop();
		a[x]=now--;
		for (int i=head[x];i;i=e[i].next){
			deg[e[i].to]--;
			if (!deg[e[i].to]) q.push(-e[i].to);
		}
	}
	long long ans=0;
	h[1]=1e9; int tp=1;
	reverse(a+1,a+n+1);
	for (int i=1;i<=n;i++){
		f[i]=lower_bound(h+1,h+tp+1,a[i])-h;
		h[f[i]]=a[i]; ans+=f[i];
		if (f[i]==tp) h[++tp]=1e9;
	}
	printf("%lld",ans);
}
