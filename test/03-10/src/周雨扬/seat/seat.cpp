#include<bits/stdc++.h>
#define N 5000000
using namespace std;
struct node{
	int l,r,len;
	friend bool operator <(const node &a,const node &b){
		if (a.len!=b.len)
			return a.len<b.len;
		return a.l<b.l;
	}
	friend node operator +(const node &a,const node &b){
		return (node){a.l,b.r,a.len+b.len};
	}
};
struct Nd{
	node mx,l,r;
	int sz,sum;
	Nd(){}
	Nd(int _l,int _r){
		mx.len=sz=_r-_l+1;
		mx.l=_l; mx.r=_r;
		l=r=mx; sum=0;
	}
	friend Nd operator +(const Nd &a,const Nd &b){
		static Nd c;
		c.mx=max(a.r+b.l,max(a.mx,b.mx));
		c.sz=a.sz+b.sz;
		c.sum=a.sum+b.sum;
		if (a.l.len==a.sz)
			c.l=a.l+b.l;
		else c.l=a.l;
		if (b.r.len==b.sz)
			c.r=a.r+b.r;
		else c.r=b.r;
		return c;
	}
	void clear(int _l,int _r){
		mx.len=l.len=r.len=0;
		sz=sum=_r-_l+1;
		mx.l=r.l=_r+1;
		mx.r=r.r=_r;
		l.l=_l; l.r=_l-1; 
	}
}t[N];
int ls[N],rs[N];
int n,Q,sz,rt;
void pushup(int k,int l,int r){
	Nd x,y;
	int mid=(l+r)/2;
	x=(ls[k]?t[ls[k]]:Nd(l,mid));
	y=(rs[k]?t[rs[k]]:Nd(mid+1,r));
	t[k]=x+y;
}
void change(int &k,int l,int r,int p,int v){
	if (!k) k=++sz;
	if (l==r){
		if (v==0)
			t[k]=Nd(l,r);
		else t[k].clear(l,r);
		//printf("change %d %d %d\n",l,r,t[k].sum);
		return;
	}
	int mid=(l+r)/2;
	if (p<=mid) change(ls[k],l,mid,p,v);
	else change(rs[k],mid+1,r,p,v);
	pushup(k,l,r);
}
int ask(int k,int l,int r,int x,int y){
	if (!k||(l==x&&r==y)){
		//printf("ask %d %d %d\n",x,y,t[k].sum);
		return t[k].sum;
	}
	int mid=(l+r)/2;
	if (y<=mid) return ask(ls[k],l,mid,x,y);
	if (x>mid) return ask(rs[k],mid+1,r,x,y);
	return ask(ls[k],l,mid,x,mid)+ask(rs[k],mid+1,r,mid+1,y);
}
map<int,int> mp;
int main(){
	freopen("seat.in","r",stdin);
	freopen("seat.out","w",stdout);
	scanf("%d%d",&n,&Q);
	while (Q--){
		int x; scanf("%d",&x);
		if (x){
			if (mp[x]){
				change(rt,1,n,mp[x],0);
				mp[x]=0;
			}
			else{
				node tmp;
				if (!rt) tmp=(node){1,n,n};
				else tmp=t[rt].mx;
				int mid=(tmp.l+tmp.r+1)/2;
				mp[x]=mid;
				//printf("%d %d %d\n",tmp.l,tmp.r,mid);
				change(rt,1,n,mid,1);
			}
		}
		else{
			int l,r;
			scanf("%d%d",&l,&r);
			printf("%d\n",ask(rt,1,n,l,r));
		}
	}
}
