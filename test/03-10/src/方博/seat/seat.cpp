#include<bits/stdc++.h>
using namespace std;
#define LL long long
inline LL read()
{
	LL x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(LL x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(LL x)
{
	write(x);
	puts("");
}
#define pa pair<int,int>
#define mk make_pair
#define fi first
#define se second
const int N=200005;
int l[N],r[N],fa[N],ls[N],rs[N];
int ll[N],rr[N],tl[N];
int size[N],cnt;
bool lr[N];
set<pa>s;
set<pa>tmp[N];
map<int,int>a;
int n,q;
set<pa>::iterator it;
int solve(int k,int now)
{
//	cout<<':'<<k<<' '<<now<<endl;
	int mid=(l[now]+r[now]+1)/2;
//	cout<<':'<<mid<<'~'<<size[now]<<endl;
	if(k==mid){
		if(ll[now])return size[ll[now]]+1;
		else return 1;
	}
//	puts("!1");
	if(ll[now]&&k<mid)return solve(k,ll[now]);
//	puts("!2");
	if(!ll[now]&&k<mid)return 0;
//	puts("!3");
	if(k<ls[now])return solve(k,ll[now]);
//	puts("!4");
	if(k>mid&&!rr[now])return size[now];
//	puts("!5");
	if(k>mid&&rr[now])return size[ll[now]]+1+solve(k,rr[now]);
//	puts("!6");
	if(k>rs[now])return size[ll[now]]+1+solve(k,rr[now]);
}
int can;
int abc;
int xxx[N];
int main()
{
	freopen("seat.in","r",stdin);
	freopen("seat.out","w",stdout);
	n=read();q=read();
	if(n<10000){		
		for(int i=1;i<=q;i++){
			int op=read(),x,y;
			if(op==0){
				int ans=0;
				x=read(),y=read();
				for(int i=x;i<=y;i++)
					if(xxx[i]!=0)ans++;
				writeln(ans);
			}
			else{
				if(a[op]){
					xxx[a[op]]=0;
					continue;
				}
				if(can>=n)continue;
				can++;
				int k=0,l=1,ma=0,r=1;
				for(int i=1;i<=n;i++){
					if(xxx[i]==0)k++;
					if(xxx[i]!=0&&k>=ma)l=i-k,r=i-1,ma=k,k=0;
						else if(i==n&&k>=ma){
							l=i-k+1;
							r=i;
							ma=k;
							k=0;
							}
					if(xxx[i]!=0)k=0;
				}
				xxx[(l+r+1)/2]=1;
				a[op]=(l+r+1)/2;
			}
	//		for(int i=1;i<=n;i++)
	//			cout<<xxx[i]<<' ';
	//		cout<<endl;
		}
		return 0;
	}
	l[1]=1;r[1]=n;tl[1]=r[1]-l[1]+1;
	s.insert(mk(tl[1],r[1]));
	cnt=1;
	a[tl[1]]=++abc;
	int x=a[tl[1]];
	tmp[x].insert(mk(n-r[1],cnt));
	for(int i=1;i<=q;i++){
		int op=read();
		if(op!=0){
			if(can>=n)continue;
			it=s.end();
			it--;
			int fir=it -> fi;
			int sec=it -> se;
//			cout<<fir<<' '<<sec<<endl;
			s.erase(it);
			int k=tmp[a[fir]].begin() -> se;
			tmp[a[fir]].erase(tmp[a[fir]].begin());
			int mid=(l[k]+r[k]+1)/2;
			if(lr[k])ls[fa[k]]=mid,ll[fa[k]]=k;
			else rs[fa[k]]=mid,rr[fa[k]]=k;
			size[k]=1;
			int t=k;
			while(t!=1){
				size[fa[t]]++;
				t=fa[t];
			}
			// 加入左儿子 
			fa[++cnt]=k;
			lr[cnt]=1;
			l[cnt]=l[k],r[cnt]=mid-1,tl[cnt]=r[cnt]-l[cnt]+1;
			if(!a[tl[cnt]])a[tl[cnt]]=++abc;
			x=a[tl[cnt]];
			tmp[x].insert(mk(n-r[cnt],cnt));
			s.insert(mk(tl[cnt],r[cnt]));
			//加入右儿子 
			fa[++cnt]=k;
			lr[cnt]=0;
			l[cnt]=mid+1,r[cnt]=r[k],tl[cnt]=r[cnt]-l[cnt]+1;
			if(!a[tl[cnt]])a[tl[cnt]]=++abc;
			x=a[tl[cnt]];
			tmp[x].insert(mk(n-r[cnt],cnt));
			s.insert(mk(tl[cnt],r[cnt]));
			can++;
		}
		else if(op==0){
			int x=read(),y=read();
			if(can)writeln(solve(y,1)-solve(x-1,1));
			else writeln(0);
		}
/*		for(int i=1;i<=cnt;i++)
			cout<<l[i]<<' ';
		cout<<endl;
		for(int i=1;i<=cnt;i++)
			cout<<r[i]<<' ';
		cout<<endl;
		for(it=s.begin();it!=s.end();it++)
			cout<<(it->fi)<<' ';
		cout<<endl;
		for(it=s.begin();it!=s.end();it++)
			cout<<(it->se)<<' ';
		cout<<endl;
//		for(int i=1;i<=n;i++)
//			cout<<solve(i,1)-solve(i-1,1)<<'!';
//		cout<<endl;
		cout<<solve(11,1)<<'-'<<solve(10,1)<<endl;*/
	}
	return 0;
}
