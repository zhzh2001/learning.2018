#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=100005;
bool bb[N];
int p[N];
int n,cnt;
int a[N],b[N],l,ans;
bool com(int a,int b)
{
	return a>b;
}
int main()
{
	freopen("prime.in","r",stdin);
	freopen("prime.out","w",stdout);
	for(int i=2;i<=100000;i++){
		if(!bb[i])p[++cnt]=i;
		for(int j=1;j<=cnt;j++){
			if(i*p[j]>100000)break;
			if(bb[i*p[j]])continue;
			else bb[i*p[j]]=true;
		}
	}
	n=read();
	for(int i=1;i<=n;i++)
		a[i]=read();
	sort(a+1,a+n+1);
	b[1]=a[1];
	l=1;
	for(int j=2;j<=n;j++){
		bool now=true;
		for(int k=1;k<=l;k++)
			if(!bb[a[j]+b[k]]){
				now=false;
				break;
			}
		if(now)b[++l]=a[j];
	}
	ans=max(ans,l);
	b[1]=a[n];
	l=1;
	for(int j=n-1;j>=1;j--){
		bool now=true;
		for(int k=1;k<=l;k++)
			if(!bb[a[j]+b[k]]){
				now=false;
				break;
			}
		if(now)b[++l]=a[j];
	}
	ans=max(ans,l);
	for(int i=1;i<=2000;i++){
		random_shuffle(a+1,a+n+1);
		int x=rand()%n+1;
		b[1]=a[x];
		l=1;
		for(int j=1;j<=n;j++){
			if(j==x)continue;
			bool now=true;
			for(int k=1;k<=l;k++)
				if(!bb[a[j]+b[k]]){
					now=false;
					break;
				}
			if(now)b[++l]=a[j];
		}
		ans=max(ans,l);
	}
	writeln(ans);
}
