#include <bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read(){
	ll k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(ll x){
	if(x<0)putchar('-'),x=-x;if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x){
	write(x);puts("");
}
#define pa pair<int,int>
#define mk make_pair
#define fi first
#define se second
const int N=100005;
int a[N],b[N];
int n;
set<pa>s;
set<int>t[N];
int d[N];
int main()
{
	freopen("list.in","r",stdin);
	freopen("list.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)
		a[i]=read();
	for(int i=1;i<=n;i++)
		s.insert(mk(a[i],i));
//	bool bbb=false;
	s.insert(mk(0,0));
	for(int i=2;i<=n;i++){
//		if(a[i]==1)bbb=true;
		b[i]=s.lower_bound(mk(a[i]-1,0))->fi;
	}
//	cout<<bbb<<endl;
	int ma=0;
	for(int i=1;i<=n;i++)
		t[b[i]].insert(i),ma=max(ma,b[i]);
	int tt=n;
	for(int i=ma;i>=0;i--)
		for(set<int>::iterator it=t[i].begin();it!=t[i].end();it++){
			a[*it]=tt;
			tt--;
		}
//	for(int i=1;i<=n;i++)
//		cout<<a[i]<<' ';
//	cout<<endl;
	for(int i=1;i<=n/2;i++)
		swap(a[i],a[n-i+1]);
	tt=1;
	for(int i=1;i<=n;i++)
		d[i]=n+1;
	ll ans=0;
	for(int i=1;i<=n;i++){
		int k=lower_bound(d+1,d+tt+1,a[i])-d-1;
		d[k+1]=min(d[k+1],a[i]);
		ans+=k+1;
		tt=max(tt,k+1);
//		for(int i=1;i<=n;i++)
//			cout<<d[i]<<' ';
//		cout<<endl;
	}
	writeln(ans);
}
