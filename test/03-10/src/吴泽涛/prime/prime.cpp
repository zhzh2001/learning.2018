#include <bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, a, b) for(int i=a;i<=b;i++)
#define Dow(i, a, b) for(int i=a;i>=b;i--)
using namespace std;
inline LL read() {
	LL x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
	while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
	return x * f;
}

const int N = 3011, Val = 200011; 
int n; 
int a[N], rk[N], poi[N], num, alpha; 
int f[Val+11]; 

void pre(){
	f[1] = 1; 
	For(i, 2, Val) 
		if(!f[i]) {
			int x = i+i; 
			while(x <= Val) {
				f[x] = 1; 
				x += i; 
			}
		}
}
int main() {
	freopen("prime.in","r",stdin); 
	freopen("prime.out","w",stdout); 
	srand(time(0)); 
	pre(); 
	n = read(); 
	For(i, 1, n) a[i] = read(), rk[i] = i;  
	int Mx = 0; 
	alpha = 50000000/n/n; 
	For(T, 1, alpha) {
		random_shuffle(rk+1, rk+n+1); 
		num = 1; 
		poi[num] = rk[1]; 
		For(i, 2, n) {
			bool flag = 0; 
			For(j, 1, num)
				if(!f[a[rk[i]] + a[poi[j]]]) { flag = 1; break; } 
			if(!flag) poi[++num] = rk[i];  
		}
		if(num > Mx) Mx = num; 
	}
	printf("%d\n", Mx); 
}



