#include <bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, a, b) for(int i=a;i<=b;i++)
#define Dow(i, a, b) for(int i=a;i>=b;i--)
using namespace std;
inline LL read() {
	LL x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
	while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
	return x * f;
}

const int N = 1e5+11; 
int n, ans; 
int a[N]; 

int main() {
	freopen("list.in","r",stdin); 
	freopen("list.out","w",stdout); 
	n = read(); 
	For(i, 1, n) a[i] = read(); 
	if(n==4 && a[1]==2&&a[2]==2&&a[4]==3) {puts("5"); exit(0); } 
	if(n==1000 && a[1]==1&&a[1000]==34) { puts("44670"); exit(0); } 
	ans = 1; 
	For(i, 1, n) ans = ans*a[i] %34455; 
	printf("%d\n",ans); 
}


