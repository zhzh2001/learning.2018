#include <bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, a, b) for(int i=a;i<=b;i++)
#define Dow(i, a, b) for(int i=a;i>=b;i--)
using namespace std;
inline LL read() {
	LL x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
	while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
	return x * f;
}

struct poi{
	int pos, id; 
	friend bool operator <(poi a, poi b) {
		return a.pos < b.pos; 
	}
};
int n, Ques, sum; 
set<poi> s; 
set<poi> ::iterator it;  
map<int, int> mp; 
struct node{
	int len, l; 
	friend bool operator <(node a, node b) {
		if(a.len != b.len) return a.len < b.len; 
		return a.l < b.l; 
	}
};
priority_queue<node> Q; 

int main() {
	freopen("seat.in","r",stdin); 
	freopen("seat.out","w",stdout); 
	n = read(); Ques = read(); 
	mp[-1] = 1; 
	s.insert((poi){0, -1}); s.insert((poi){n+1, -1} );  
	Q.push((node){ n, 1}); 
	while(Ques--) {
		sum = 0; 
		int id = read(); 
		if(!id) {
			int l = read(), r = read(); 
			set<poi> ::iterator it1; 
			set<poi> ::iterator it2; 
			it1 = s.lower_bound((poi){l, 0}); 
			it2 = s.upper_bound((poi){r, 0}); 
			for(it = it1; it!=it2; it++) sum++; 
			continue; 
		}
		if(!mp[id]) { 
			while(!Q.empty()) {
				node p = Q.top(); Q.pop(); 
				it = s.lower_bound((poi) { p.l-1, 0}); 
				poi q = *it; 
				if(!mp[q.id]) continue; 
				int l = p.l; int r = p.l+1+p.len-1; 
				it = s.lower_bound((poi) { r+1, 0}); 
				q = *it; 
				if(!mp[q.id]) continue; 
				
				 
				int mid = (l+r+1)/2; 
				s.insert((poi){ mid, id}); 
				Q.push( (node){ mid-l, l} ); 
				Q.push( (node){ r-mid, mid+1 }); 
				mp[id] = mid; 
				break; 
 			}
		}
		else {
			int pos = mp[id]; 
			it = s.lower_bound((poi){ pos, 0}); 
			--it; int l = (*it).pos; l++; ++it; 
			++it; int r = (*it).pos; r--; --it; 
			Q.push( (node){r-l+1, l} ); 
			mp[id] = 0; 
		}
		printf("%d\n", sum); 
 	}
}


