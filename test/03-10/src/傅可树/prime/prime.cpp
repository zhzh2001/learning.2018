#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=3005,maxm=9000005;
int n,a[maxn];
inline void init(){
	scanf("%d",&n);
	for (int i=1;i<=n;i++){
		scanf("%d",&a[i]);
	}
}
struct edge{
	int link,next;
}e[maxm];
int pri[200005],tot,cnt,head[maxn*2];
bool flag[200005];
inline void prepare(){
	for (int i=2;i<=200000;i++){
		if (!flag[i]) {
			pri[++cnt]=i; 
		}
		for (int j=1;j<=cnt&&pri[j]*i<200000;j++){
			flag[pri[j]*i]=1;
			if (i%pri[j]==0) break;
		}
	}
}
inline void add_Edge(int u,int v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;	
}
inline void insert(int u,int v){
	add_Edge(u,v);// add_Edge(v,u);
}
int link[maxn*2];
bool vis[maxn*2];
int find(int u){
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (!vis[v]){
			vis[v]=1;
			if (!link[v]||find(link[v])){
				link[v]=u; return 1;
			}
		}
	}
	return 0;
}
inline void solve(){
	prepare();
	for (int i=1;i<n;i++){
		for (int j=i+1;j<=n;j++){
			if (!flag[a[i]+a[j]]){
				insert(i,j+n);
				insert(j,i+n);
			}
		}
	}
	for (int i=1;i<=n;i++){
		insert(i,i+n);
	}
	int ans=0;
	for (int i=1;i<=n;i++){
		memset(vis,0,sizeof(vis));
		ans+=find(i);
	}
	printf("%d\n",ans);
}
int main(){
	freopen("prime.in","r",stdin);
	freopen("prime.out","w",stdout);
	init();
	solve();
	return 0;
}
