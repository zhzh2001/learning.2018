#include<cstdio>
#include<algorithm>
#include<queue>
#include<cstring>
#include<vector>
#define int long long
#define pa pair<int ,int >
using namespace std;
const int maxn=100005,maxm=300005;
inline int read(){int x=0,f=1; int ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
int n,a[maxn];
inline void init(){
	n=read();
	for (int i=1;i<=n;i++){
		a[i]=read();
	}
}
struct edge{
	int link,next;
}e[maxm];
int head[maxn],tot;
inline void insert(int u,int v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
}
bool vis[maxn];
int pre[maxn],num,du[maxn];
priority_queue<int , vector<int >, greater<int > >heap;
inline void topsort(){
	for (int i=1;i<=n;i++){
		if (!du[i]){
			heap.push(i);
			vis[i]=1;
		}
	}
	num=100000;
	while (!heap.empty()){
		int u=heap.top();
		heap.pop();
		a[u]=num--;
		for (int i=head[u];i;i=e[i].next){
			int v=e[i].link;
			du[v]--;
			if (du[v]<=0&&!vis[v]) heap.push(v),vis[v]=1;
		}
	}
}
int dp[maxn],ans,g[maxn];
inline void LCS(){
	memset(g,127/3,sizeof g); g[0]=0;
	for (int i=n;i;i--){
		int x=lower_bound(g,g+1+n,a[i])-g-1;
		dp[i]=x+1;
		g[dp[i]]=min(g[dp[i]],a[i]);
		ans+=dp[i];
	}
	printf("%lld\n",ans);
}
inline void solve(){
	for (int i=1;i<=n;i++){
		if (pre[a[i]-1]){
			insert(i,pre[a[i]-1]);
			du[pre[a[i]-1]]++;
		}
		if (pre[a[i]]){
			insert(pre[a[i]],i);
			du[i]++;
		}
		if (pre[a[i]+1]){
			insert(pre[a[i]+1],i);
			du[i]++;
		}
		pre[a[i]]=i;
	}
	topsort();
	LCS();
}
signed main(){
	freopen("list.in","r",stdin);
	freopen("list.out","w",stdout);
	init();
	solve();
	return 0;
}
