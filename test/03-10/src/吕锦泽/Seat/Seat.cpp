#include<bits/stdc++.h>
#define ll int
#define N 4000005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
map<ll,ll> mp;
ll tot=1,n,q,l[N],r[N],lson[N],rson[N],sum[N],pos[N],len[N];
void pushup(ll tl,ll mid,ll tr,ll p){
	if (lson[p]&&rson[p]){
		l[p]=l[lson[p]]; r[p]=r[rson[p]];
		if (l[lson[p]]==mid-tl+1) l[p]+=l[rson[p]];
		if (r[rson[p]]==tr-mid) r[p]+=r[lson[p]];
		sum[p]=sum[lson[p]]+sum[rson[p]];
		if (len[lson[p]]>len[rson[p]]){
			len[p]=len[lson[p]];
			pos[p]=pos[lson[p]];
		}else{
			len[p]=len[rson[p]];
			pos[p]=pos[rson[p]];
		}
		ll t1=r[lson[p]]+l[rson[p]],t2=(t1+2)/2+mid-r[lson[p]];
		if (t1>len[p]||t1==len[p]&&t2>pos[p]){
			len[p]=t1; pos[p]=t2;
		}
	}else
	if (lson[p]){
		l[p]=l[lson[p]]; r[p]=r[lson[p]]+tr-mid;
		if (l[p]==mid-tl+1) l[p]=tr-tl+1;
		sum[p]=sum[lson[p]];
		len[p]=len[lson[p]];
		pos[p]=pos[lson[p]];
		ll t1=r[lson[p]]+tr-mid,t2=(t1+2)/2+mid-r[lson[p]];
		if (t1>len[p]||t1==len[p]&&t2>pos[p]){
			len[p]=t1; pos[p]=t2;
		}
	}else
	if (rson[p]){
		l[p]=mid-tl+1+l[rson[p]]; r[p]=r[rson[p]];
		if (r[p]==tr-mid) r[p]=tr-tl+1;
		sum[p]=sum[rson[p]];
		len[p]=len[rson[p]];
		pos[p]=pos[rson[p]];
		ll t1=mid-tl+1+l[rson[p]],t2=(t1+2)/2+tl-1;
		if (t1>len[p]||t1==len[p]&&t2>pos[p]){
			len[p]=t1; pos[p]=t2;
		}
	}
}
void modfiy(ll tl,ll tr,ll po,ll tmp,ll p){
	if (tl==tr){
		l[p]=r[p]=len[p]=tmp^1; sum[p]=tmp;
		pos[p]=po*tmp;
		return;
	}
	ll mid=tl+tr>>1;
	if (po<=mid){
		if (!lson[p]) lson[p]=++tot;
		modfiy(tl,mid,po,tmp,lson[p]);
	}else{
		if (!rson[p]) rson[p]=++tot;
		modfiy(mid+1,tr,po,tmp,rson[p]);
	}
	pushup(tl,mid,tr,p);
}
ll query(ll l,ll r,ll s,ll t,ll p){
	if (l==s&&r==t) return sum[p];
	ll mid=l+r>>1;
	if (t<=mid){
		if (lson[p]) return query(l,mid,s,t,lson[p]);
		else return 0;
	}else
	if (s>mid){
		if (rson[p]) return query(mid+1,r,s,t,rson[p]);
		else return 0;
	}else{
		ll temp=0;
		if (lson[p]) temp+=query(l,mid,s,mid,lson[p]);
		if (rson[p]) temp+=query(mid+1,r,mid+1,t,rson[p]);
		return temp;
	}
}
int main(){
	freopen("Seat.in","r",stdin);
	freopen("Seat.out","w",stdout);
	n=read(); q=read(); len[1]=n; pos[1]=(n+2)/2;
	while (q--){
		ll opt=read();
		if (!opt){
			ll l=read(),r=read();
			printf("%d\n",query(1,n,l,r,1));
		}else
		if (mp[opt]){
			modfiy(1,n,mp[opt],0,1);
			mp[opt]=0;
		}else{
			mp[opt]=pos[1];
			modfiy(1,n,pos[1],1,1);
		}
	}
}
