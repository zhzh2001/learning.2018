#include<bits/stdc++.h>
#define ll int
#define N 100005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,num,now,ans,a[N],b[N],c[N],nxt[N],last[N];
int main(){
	freopen("list.in","r",stdin);
	freopen("list.out","w",stdout);
	n=read();
	rep(i,1,n) a[i]=read();
	rep(i,1,n){
		nxt[i]=last[a[i]];
		last[a[i]]=i;
	}
	rep(i,1,n) if (!nxt[i]) nxt[i]=last[a[i]+1];
	num=0; now=last[1];
	while (now){
		b[now]=++num;
		now=nxt[now];
	}
	rep(i,1,n/2) swap(b[i],b[n-i+1]);
	rep(i,1,n){
		ll pos=upper_bound(c+1,c+1+now,b[i])-c;
		now=max(now,pos); c[pos]=b[i]; ans+=pos;
	}
	printf("%d",ans);
}
