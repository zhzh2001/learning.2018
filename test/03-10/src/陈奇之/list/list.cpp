#include<bits/stdc++.h>
using namespace std;
#define gc getchar
#define RG register
#define pc putchar
#define rd read
#define mem(x,v) memset(x,v,sizeof(x))
#define Rep(i,a,b) for(RG int i=(a);i<=(b);i++)
#define Down(i,a,b) for(RG int i=(a);i>=(b);i--)
const int inf = 0x3f3f3f3f;
typedef long long ll;
inline ll read(){
	ll T = 0;int f = 0;char c = gc();
	for(;c<'0'||c>'9';c=gc()) f|=(c=='-');
	for(;c>='0'&&c<='9';c=gc()) T=T*10+c-'0';
	return f?-T:T;
}
inline void write(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>=10) write(x/10);pc(x%10+'0');
}
inline void writeln(ll x){
	write(x);puts("");
}
#define fin(x) (freopen(#x".in","r",stdin))
#define fout(x) (freopen(#x".out","w",stdout))
const int maxn = 1e5+2333;
struct Edge{
	int to,nxt;
	Edge(){}
	Edge(int to,int nxt):to(to),nxt(nxt){}
}edge[10000009];
int first[maxn],nume;
void Addedge(int a,int b){
	edge[nume] = Edge(b,first[a]);
	first[a] = nume++;
//	printf("%d->%d\n",a,b);
}
int last[maxn],pre[maxn];
ll ans;
int dp[maxn];
int dfs(int u){
	if(~dp[u]) return dp[u];
	dp[u] = 1;
	for(int e=first[u];~e;e=edge[e].nxt)
		dp[u]=max(dp[u],dfs(edge[e].to)+1);
	return dp[u];
}
int n,a[maxn],from[maxn];
int main(){
	fin(list);fout(list);
	n = rd();
	Rep(i,1,n)
		a[i] = rd();
	mem(first,-1);nume = 0;
	Rep(i,1,n){
//		if(a[i]!=1)
//			from[i] = pre[last[a[i]-1]];
		int w = last[a[i]-1];//从这个转移过来的
		from[i] = last[a[i]-1];
		while(w){
//			printf("w=%d;%d\n",w,pre[w]);
			if(pre[w])Addedge(pre[w],i);
			w = from[w];
		}
		for(int j=a[i];j<=n;j++){
			if(!last[j]) break;
//			printf("last(%d) = %d\n",j,last[j]);
			Addedge(last[j],i);
		}
//		Addedge(pre[last[a[i]-1]],i);
		pre[i] = last[a[i]];
//		printf("pre(%d) = %d\n",i,pre[i]);
		last[a[i]] = i;
	}
	mem(dp,-1);
	ans = 0;
	Rep(i,1,n) if(dp[i]==-1) dfs(i);
	Rep(i,1,n) ans += dp[i];
//	Rep(i,1,n){
//		printf("%d %d\n",i,dp[i]);
//	}
	printf("%lld\n",ans);
	return 0;
}

/*
6
1 2 1 3 4 1
*/


