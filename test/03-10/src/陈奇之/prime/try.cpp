#include<bits/stdc++.h>
using namespace std;
#define gc getchar
#define RG register
#define pc putchar
#define rd read
#define mem(x,v) memset(x,v,sizeof(x))
#define Rep(i,a,b) for(RG int i=(a);i<=(b);i++)
#define Down(i,a,b) for(RG int i=(a);i>=(b);i--)
const int inf = 0x3f3f3f3f;
typedef long long ll;
inline ll read(){
	ll T = 0;int f = 0;char c = gc();
	for(;c<'0'||c>'9';c=gc()) f|=(c=='-');
	for(;c>='0'&&c<='9';c=gc()) T=T*10+c-'0';
	return f?-T:T;
}
inline void write(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>=10) write(x/10);pc(x%10+'0');
}
inline void writeln(ll x){
	write(x);puts("");
}
#define fin(x) (freopen(#x".in","r",stdin))
#define fout(x) (freopen(#x".out","w",stdout))
const int maxn = 3005;
const int maxai = 100005;
bool Isprime[2*maxai];int prime[maxai];
void init(){
	mem(Isprime,true);
	Isprime[0] = false;
	Rep(i,2,200000){
		if(Isprime[i]){
			prime[++*prime] = i;
		}
		for(int j=1;j<=*prime&&i*prime[j]<=200000;j++){
			Isprime[i*prime[j]] = false;
			if(i%prime[j]==0) break;
		}
	}
}
int S,T;
struct Dinic{
	struct Edge{
		int to,nxt,cap;
		Edge(){}
		Edge(int to,int nxt,int cap):
			to(to),nxt(nxt),cap(cap){}
	}edge[1506*1506*2];
	int first[3009],nume,cur[3009];
/*
6
1 2 3 4 6 10

*/
	void Addedge(int a,int b,int c){
//		printf("%d %d %d\n",a,b,c);
		edge[nume] = Edge(b,first[a],c);first[a] = nume++;
		edge[nume] = Edge(a,first[b],0);first[b] = nume++;
	}
	int q[3009],dis[3009];
	bool bfs(){
		Rep(i,S,T) dis[i] = -1;
		int front,rear;
		front = rear = 0;
		q[rear++] = S;
		dis[S] = 0;
		while(front<rear){
			int u = q[front++];
			for(int e=first[u];~e;e=edge[e].nxt){
				int v = edge[e].to;
				if(edge[e].cap && dis[v]==-1){
					dis[v] = dis[u] + 1;
					q[rear++] = v;
				}
			}
		}
//		Rep(i,S,T) printf("%d ",dis[i]);puts("");
		return dis[T]!=-1;
	}
	int dfs(int u,int flow){
		if(u==T) return flow;
		int used = 0,d;
		for(int &e=cur[u];~e;e=edge[e].nxt){
			int v = edge[e].to;
			if(edge[e].cap && dis[v]==dis[u]+1 && (d = dfs(v,min(edge[e].cap,flow-used))))
				edge[e].cap-=d,edge[e^1].cap+=d,used+=d;
			if(used==flow) break;
		}
		return used;
	}
	int solve(){
		int ans = 0;
		while(bfs()){
			Rep(i,S,T) cur[i] = first[i];
			ans += dfs(S,inf);
		}
		return ans;
	}
}solver;
int n,ans,a[3009];
int main(){
	srand(time(NULL));
	freopen("prime.in","w",stdout);
	puts("3000");
	Rep(i,1,3000){
		printf("%d\n",rand()+1);
	}
}
/*
1 + 偶数
如果有2个1，只能选一个1 
1和1之间连边 
*/
