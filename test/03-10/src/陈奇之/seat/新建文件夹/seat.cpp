#include<bits/stdc++.h>
using namespace std;
#define gc getchar
#define RG register
#define pc putchar
#define rd read
#define mem(x,v) memset(x,v,sizeof(x))
#define Rep(i,a,b) for(RG int i=(a);i<=(b);i++)
#define Down(i,a,b) for(RG int i=(a);i>=(b);i--)
typedef long long ll;
inline ll read(){
	ll T = 0;int f = 0;char c = gc();
	for(;c<'0'||c>'9';c=gc()) f|=(c=='-');
	for(;c>='0'&&c<='9';c=gc()) T=T*10+c-'0';
	return f?-T:T;
}
inline void write(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>=10) write(x/10);pc(x%10+'0');
}
inline void writeln(ll x){
	write(x);puts("");
}
#define hash ____hash
#define fin(x) (freopen(#x".in","r",stdin))
#define fout(x) (freopen(#x".out","w",stdout))
const int maxn = 100005;
int maxlen;
struct SegmentTree{
	#define mid ((l+r)>>1)
	struct node{
		int lmax,rmax,max,sum,lc,rc;
	}T[maxn*50];
	int tot = 0;
//void out(int o,int l,int r){
//	printf("%d %d %d %d %d\n",o,!o?(r-l+1):T[o].lmax,!o?(r-l+1):T[o].max,!o?(r-l+1):T[o].rmax,T[o].sum);
//}
	void pushup(int o,int l,int r){
		int lson = T[o].lc,rson = T[o].rc;
//		if(lson) pushup(lson,l,mid); if(rson) pushup(rson,mid+1,r);
		if(lson && rson){
//			puts("W");
			T[o].lmax=T[lson].lmax;
			if(!T[lson].sum) T[o].lmax=max(T[o].lmax,(mid-l+1)+T[rson].lmax);
			T[o].rmax=T[rson].rmax;
			if(!T[rson].sum) T[o].rmax=max(T[o].rmax,T[lson].rmax+(r-mid));
			T[o].max = max(T[lson].max,T[rson].max);
			T[o].max = max(T[o].max,T[lson].rmax + T[rson].lmax);
			T[o].max = max(T[o].max,max(T[o].lmax,T[o].rmax));
			T[o].sum = T[lson].sum + T[rson].sum; 
		} else
		if(lson){
			T[o].lmax=T[lson].lmax;
			if(!T[lson].sum) T[o].lmax=r-l+1;
			T[o].rmax=T[lson].rmax+(r-mid);
//			T[o].max = max(T[lson].lmax,T[lson].rmax + (r-mid));
			T[o].max = max(T[lson].max,T[lson].rmax + (r-mid));
			T[o].max = max(T[o].max,max(T[o].lmax,T[o].rmax));
			T[o].sum = T[lson].sum; 
		} else
		if(rson){
			T[o].rmax=T[rson].rmax;
			if(!T[rson].sum) T[o].rmax=r-l+1;
			T[o].lmax = (mid-l+1) + T[rson].lmax;
//			T[o].max = max(T[lson].rmax,(mid-l+1)+T[rson].lmax);
			T[o].max = max(T[rson].lmax + (mid-l+1),T[rson].max);
			T[o].max = max(T[o].max,max(T[o].lmax,T[o].rmax));
			T[o].sum = T[rson].sum;
		} else{
//			puts("Wzp Ak ZJOI2018!");
		}
//		out(lson,l,mid);
//		out(rson,mid+1,r);
//		out(o,l,r);
//		puts("");
//		printf("pushup(%d):%d\n",o,T[o].sum);
	}
	int query(int o,int l,int r,int x,int y){
		if(!o) return 0;
		if(l==x&&r==y) return T[o].sum;
		if(y<=mid) return query(T[o].lc,l,mid,x,y); else
		if(mid+1<=x) return query(T[o].rc,mid+1,r,x,y); else
		return query(T[o].lc,l,mid,x,mid)+query(T[o].rc,mid+1,r,mid+1,y); 
	} //OK
	int newnode(int l,int r){
		int x = ++tot;
		T[x].lmax=T[x].rmax=T[x].max=r-l+1;
		T[x].sum=0;
		T[x].lc=T[x].rc=0;
		return x;
	}
	void add(int &o,int l,int r,int x){
		if(!o) o = newnode(l,r);
//		printf("add(%d %d %d)\n",o,l,r,x);
		if(l==r){
			T[o].sum = T[o].sum^1;
			T[o].lmax= T[o].lmax^1;
			T[o].rmax= T[o].rmax^1;
			T[o].max = T[o].max^1;
			return ;
		}
		if(x<=mid) add(T[o].lc,l,mid,x); else
				   add(T[o].rc,mid+1,r,x);
		pushup(o,l,r);
	} //OK
	int getpos(int o,int l,int r){
		if(!o){
			int L = r-maxlen+1,R = r;
			return (L+R+1)/2;
		}
		if(l==r) return l;
		if((!T[o].rc && (r-mid==maxlen)) || T[T[o].rc].max == maxlen) return getpos(T[o].rc,mid+1,r); else
		if((!T[o].lc && (mid-l+1==maxlen)) || T[T[o].lc].max == maxlen) return getpos(T[o].lc,l,mid); else{
			int L = (!T[o].lc) ? (l) : mid - T[T[o].lc].rmax + 1,
				R = (!T[o].rc) ? (r) : mid + T[T[o].rc].lmax;
			return (L+R+1)/2;
		}
	} //OK
}seg;
int n,Q;
int op[maxn],have[maxn],L[maxn],R[maxn],hash[maxn];
int main(){
	fin(seat);fout(seat);
	n = rd(),Q = rd();
	int Qcnt = 0;
	*hash=0;
	Rep(i,1,Q){
		op[i] = rd();
		if(op[i] == 0){
			L[i] = rd();R[i] = rd();
		} else{
			hash[++*hash] = op[i];
			//����ѧ���ı��
		}
	}
	sort(hash+1,hash+1+*hash);*hash = unique(hash+1,hash+1+*hash) - hash - 1;
	Rep(i,1,Q){
		if(op[i]) op[i] = lower_bound(hash+1,hash+1+*hash,op[i])-hash;
		//have[i] = false;
	}
	Rep(i,1,*hash) have[i] = false;
	int root = 0;
	Rep(i,1,Q){
//		printf("tot = %d\n",seg.tot);
//			printf("%d %d\n",i,op[i]);
		if(op[i] == 0){
			printf("%d\n",seg.query(root,1,n,L[i],R[i]));
		} else{
			if(!have[op[i]]){
				maxlen = !root?n:seg.T[root].max;
//				printf("maxlen = %d\n",maxlen);
				have[op[i]] = seg.getpos(root,1,n);
//				printf("  %d\n",have[op[i]]);
				seg.add(root,1,n,have[op[i]]);
			} else{
//				printf("Reverse %d\n",have[op[i]]);
				seg.add(root,1,n,have[op[i]]);
				have[op[i]] = 0;
			}
		}
	}
	return 0;
}
/*
6 10
4
2
6
5
3
1
2
3
4
1000
*/
