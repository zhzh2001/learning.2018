#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int N=100007;
struct Seg_Tree{
	int ls,rs;
	int l,r,len,res,max,ans;
}Tr[N*40];
int Tr_size;

void News(int &pos,int l,int r){
	if (!pos) pos=++Tr_size;
	int len=r-l+1;
	Tr[pos].l=Tr[pos].r=Tr[pos].max=len;
	Tr[pos].len=len;
	Tr[pos].res=(l+r)/2;
	if (len%2==0) Tr[pos].res++;
	Tr[pos].ans=0;
}
void Pushup(int pos,int l,int r){
	int mid=(l+r)>>1;
/*	if (!Tr[pos].ls&&!Tr[pos].rs) return;
	if (!Tr[pos].ls){
		Tr[pos].l=(mid-l+1)+Tr[Tr[pos].rs].l;
		Tr[pos].r=Tr[Tr[pos].rs].r;
		Tr[pos].max=Tr[pos].l;
		Tr[pos].res=l+Tr[pos].max/2;
		Tr[pos].ans=Tr[Tr[pos].rs].ans;
	}
	else if (!Tr[pos].rs){
		Tr[pos].l=Tr[Tr[pos].ls].l;
		Tr[pos].r=(r-mid)+Tr[Tr[pos].ls].r;
		Tr[pos].max=Tr[pos].r;
		Tr[pos].res=r-Tr[pos].max/2;
		if (Tr[pos].max%2==0) Tr[pos].res++;
		Tr[pos].ans=Tr[Tr[pos].ls].ans;
	}
	else*/ 
	if (!Tr[pos].ls) News(Tr[pos].ls,l,mid);
	if (!Tr[pos].rs) News(Tr[pos].rs,mid+1,r);
		Tr[pos].l=Tr[Tr[pos].ls].l;
		if (Tr[pos].l==mid-l+1) Tr[pos].l+=Tr[Tr[pos].rs].l;
		Tr[pos].r=Tr[Tr[pos].rs].r;
		if (Tr[pos].r==r-mid) Tr[pos].r+=Tr[Tr[pos].ls].r;
		Tr[pos].max=max(Tr[Tr[pos].ls].max,Tr[Tr[pos].rs].max);
		Tr[pos].max=max(Tr[pos].max,Tr[Tr[pos].ls].r+Tr[Tr[pos].rs].l);
		if (Tr[Tr[pos].rs].max==Tr[pos].max) Tr[pos].res=Tr[Tr[pos].rs].res;
		else if (Tr[Tr[pos].ls].r+Tr[Tr[pos].rs].l==Tr[pos].max){
			int lx=mid-Tr[Tr[pos].ls].r+1,rx=mid+Tr[Tr[pos].rs].l;
			Tr[pos].res=(lx+rx)/2;
			if ((rx-lx+1)%2==0) Tr[pos].res++;
//			printf("%d %d  %d %d  %d\n",l,r,lx,rx,Tr[pos].res);
		}
		else Tr[pos].res=Tr[Tr[pos].ls].res;
		Tr[pos].ans=Tr[Tr[pos].ls].ans+Tr[Tr[pos].rs].ans;
	
}
void Update(int pos,int l,int r,int x,int y,int num){
	int mid=(l+r)>>1;
	if (l==r) {
		Tr[pos].l=Tr[pos].r=Tr[pos].max=num;
		Tr[pos].ans=num^1;
//			printf("POS=%d   L=%d R=%d   L=%d R=%d Num=%d   Ans=%d\n",pos,l,r,x,y,num,Tr[pos].ans);
		return;
	}
	int flag=1;
	if (!Tr[pos].ls) News(Tr[pos].ls,l,mid),flag=0;
	if (!Tr[pos].rs) News(Tr[pos].rs,mid+1,r),flag=0;
	if (!flag) Pushup(pos,l,r);
	int now=x;
//	if (!num) now=Tr[pos].res; else now=x;
	if (now<=mid){
		
		Update(Tr[pos].ls,l,mid,x,y,num);
	}
	else{
		
		Update(Tr[pos].rs,mid+1,r,x,y,num);		
	}
	Pushup(pos,l,r);
//	printf("POS=%d   L=%d R=%d   L=%d R=%d Num=%d   Ans=%d\n",pos,l,r,x,y,num,Tr[pos].ans);
//	printf("Pos[3]=%d\n",Tr[3].ans);
}

int Query(int pos,int l,int r,int x,int y){
//	printf("Pos[3]=%d\n",Tr[3].ans);
//	printf("Q   POS=%d  L=%d R=%d   X=%d Y=%d  Ans=%d\n",pos,l,r,x,y,Tr[pos].ans);
	if (l==x&&r==y) return Tr[pos].ans;
	int mid=(l+r)>>1;
	if (y<=mid){
		if (!Tr[pos].ls) News(Tr[pos].ls,l,mid);
		return Query(Tr[pos].ls,l,mid,x,y);
	}
	else if (x>mid){
		if (!Tr[pos].rs) News(Tr[pos].rs,mid+1,r);
		return Query(Tr[pos].rs,mid+1,r,x,y);
	}
	else {
		if (!Tr[pos].ls) News(Tr[pos].ls,l,mid);
		if (!Tr[pos].rs) News(Tr[pos].rs,mid+1,r);
		return (Query(Tr[pos].ls,l,mid,x,mid)+Query(Tr[pos].rs,mid+1,r,mid+1,y));
	}
}

int n,m;
struct node{
	int opt,l,r;
}q[N];
#include<map>
map<int,int> dow;
int rt=0;

int main(){

	//	say hello


	freopen("seat.in","r",stdin);
	freopen("seat.out","w",stdout);


	n=read(),m=read();
	For(i,1,m){
		q[i].opt=read();
		if (!q[i].opt)q[i].l=read(),q[i].r=read();
	}

	News(rt,1,n);
	For(i,1,m){
		if (q[i].opt){
			if (dow[q[i].opt]) 
				Update(1,1,n,dow[q[i].opt],dow[q[i].opt],1),dow[q[i].opt]=0;
			else 
				dow[q[i].opt]=Tr[1].res,Update(1,1,n,Tr[1].res,Tr[1].res,0);
		}
		else printf("%d\n",Query(1,1,n,q[i].l,q[i].r));
	}



	// say goodbye

}

