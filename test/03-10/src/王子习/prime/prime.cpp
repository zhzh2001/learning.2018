#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

int Check_P(int x,int y){
	int k=x+y,flag=0;
	For(i,2,k/2) if (k%i==0) flag=1;
	return flag;
}

const int N=3007;
int n;
int a[N],b[N][N],vis[N];
const double eps=1e-9;

int Check(int x){
	int ret=0;
	For(i,1,n) if (b[x][i]==0&&vis[i]) ret++;
	if (ret>1) return 0;
	return 1;
}

int main(){

	//	say hello


	freopen("prime.in","r",stdin);
	freopen("prime.out","w",stdout);

	n=read();
	For(i,1,n) a[i]=read();
	For(i,1,n) For(j,i+1,n){
		if (Check_P(a[i],a[j])) b[i][j]=b[j][i]=1;//,printf("%d %d\n",i,j);
	}
	For(i,1,n) b[i][i]=1;
	double det=1;
	srand(19260817);
	int res=0,nowres=0;
	while (det>eps){
	//	printf("%.6lf\n",det);
		int now=rand()%n+1;
		if (vis[now]) continue;
		if (Check(now) || (det*rand())>5000){
			vis[now]=1;
			nowres++;
			For(i,1,n) if (b[now][i]==0&&vis[i]){
				vis[i]=0;
				nowres--;
			} 
			res=max(res,nowres);
		}
		
		det*=0.999;
	}
	printf("%d\n",res);




	// say goodbye

}

