#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int N=10007;
int ans=0;
int n;

int val[N],f[N],a[N];
int lef[N],head[N],tot;

void dfs(int x){
//	printf("%d %d\n",x,a[x]);
	if (!x||val[x]) return;
	while (head[a[x]-1]>x) dfs(head[a[x]-1]),head[a[x]-1]=lef[head[a[x]-1]];
	dfs(head[a[x]-1]);
	
	if (!val[x]) val[x]=++tot; 
}

int h[N];

int main(){

	//	say hello


	freopen("list.in","r",stdin);
	freopen("list.out","w",stdout);

	n=read();
	For(i,1,n) a[i]=read();
	For(i,1,n){
		lef[i]=head[a[i]];
		head[a[i]]=i;
	}
	Rep(i,1,n) dfs(i);
//	For(i,1,n) printf("%d\n",val[i]);
/*	For(i,1,n) {
		printf("Val[%d]=%d  A=%d \n",i,val[i],a[i]);
	}*/
	
/*	Rep(i,1,n){
		f[i]=1;
		For(j,i+1,n){
			if (val[j]<val[i]) f[i]=max(f[i],f[j]+1);
		}
		ans+=f[i];
	}
*/

	memset(h,67,sizeof(h));
	Rep(i,1,n){
		f[i]=1;
		int l=1,r=n;
		while (l<=r){
			int mid=(l+r)>>1;
		//	printf("L%d R%d  MID%d  %d %d\n",l,r,mid,h[mid],val[i] );
			if (h[mid]<val[i]) f[i]=mid+1,l=mid+1;
			else r=mid-1;
		}
		h[f[i]]=min(h[f[i]],val[i]);
		ans+=f[i];
	}	
	printf("%d\n",ans);



	// say goodbye

}

