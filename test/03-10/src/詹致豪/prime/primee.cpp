#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

int edge[5000000],next[5000000],flow[5000000],first[5000];
int g[5000],h[5000],v[5000];
bool b[500000];
int i,j,n,t,head,tail,sum_edge;

inline bool isprime(int x)
{
	for (int i=2;i*i<=x;i++)
		if (! (x%i))
			return false;
	return true;
}

inline void addedge(int x,int y,int f)
{
	sum_edge++,edge[sum_edge]=y,next[sum_edge]=first[x],flow[sum_edge]=f,first[x]=sum_edge;
	return;
}

inline int oppedge(int x)
{
	return ((x-1)^1)+1;
}

inline void buildgraph()
{
	freopen("primee.in","r",stdin);
	scanf("%d",&n);
	for (i=1;i<=n;i++)
		scanf("%d",&v[i]);
	for (i=2;i<=200000;i++)
		b[i]=isprime(i);
	for (i=1;i<=n;i++)
		if (v[i]&1)
			for (j=1;j<=n;j++)
				if (! (v[j]&1))
					if (b[v[i]+v[j]])
						addedge(i,j,1),addedge(j,i,0);
	for (i=1;i<=n;i++)
		if (v[i]&1)
			addedge(0,i,1),addedge(i,0,0);
		else
			addedge(i,n+1,1),addedge(n+1,i,0);
	return;
}

inline int dinic_bfs()
{
	memset(h,0,sizeof(h));
	tail=1,g[tail]=0,h[g[tail]]=1;
	for (head=1;head<=tail;head++)
		for (i=first[g[head]];i!=0;i=next[i])
			if ((flow[i]) && (! h[edge[i]]))
				tail++,g[tail]=edge[i],h[g[tail]]=h[g[head]]+1;
	return h[n+1];
}

inline int dinic_dfs(int now,int cap)
{
	if (now==n+1)
		return cap;
	int s=0,t=0;
	for (int i=first[now];i!=0;i=next[i])
		if ((flow[i]) && (h[edge[i]]==h[now]+1))
		{
			t=dinic_dfs(edge[i],min(cap,flow[i]));
			s=s+t,cap=cap-t;
			flow[i]=flow[i]-t,flow[oppedge(i)]=flow[oppedge(i)]+t;
			if (! cap)
				break;
		}
	if (! s)
		h[now]=0;
	return s;
}

inline void maxflow()
{
	freopen("primee.out","w",stdout);
	while (dinic_bfs())
		t=t+dinic_dfs(0,n);
	printf("%d",n-t);
	return;
}

int main()
{
	buildgraph();
	maxflow();
	return 0;
}
