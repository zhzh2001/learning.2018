#include <cstdio>
#include <algorithm>

using namespace std;

bool b[150][150];
int g[150];
int i,j,k,n,s;
bool c;

inline bool isprime(int x)
{
	for (int i=2;i*i<=x;i++)
		if (! (x%i))
			return false;
	return true;
}

int main()
{
	freopen("prime.in","r",stdin);
	freopen("prime_.out","w",stdout);
	scanf("%d",&n);
	for (i=1;i<=n;i++)
		scanf("%d",&g[i]);
	for (i=1;i<=n;i++)
		for (j=1;j<=n;j++)
			if (i!=j)
				b[i][j]=isprime(g[i]+g[j]);
	for (i=0;i<(1<<n);i++)
	{
		c=true;
		for (j=1;j<=n;j++)
			if (i&(1<<(j-1)))
				for (k=1;k<=n;k++)
					if (i&(1<<(k-1)))
						if (b[j][k])
							c=false;
		if (c)
		{
			for (j=1,k=0;j<=n;j++)
				if (i&(1<<(j-1)))
					k++;
			s=max(s,k);
		}
	}
	printf("%d",s);
	return 0;
}
