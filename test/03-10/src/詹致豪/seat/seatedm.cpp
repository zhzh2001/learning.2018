#include <cstdio>
#include <ctime>
#include <algorithm>

using namespace std;

int main()
{
	freopen("seate.in","w",stdout);
	srand((int) time(0));
	int n=10000,m=100000;
	printf("%d %d\n",n,m);
	for (int i=1;i<=m;i++)
		if (rand()%2)
			printf("%d\n",(rand()<<15|rand())%n+1);
		else
		{
			int x=(rand()<<15|rand())%n+1,y=(rand()<<15|rand())%n+1;
			if (x>y) swap(x,y);
			printf("0 %d %d\n",x,y);
		}
	return 0;
}
