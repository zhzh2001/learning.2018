#include <cstdio>
#include <algorithm>
#include <map>

using namespace std;

map <int,int> M;

struct treenode
{
	int left,right,max0,left0,right0,sum1;
};

treenode tree[5000000];
int root,sum_node;
int i,m,n,t,x,y;

inline int left0(int l,int r,int node)
{
	if (! node)
		return r-l+1;
	else
		return tree[node].left0;
}

inline int right0(int l,int r,int node)
{
	if (! node)
		return r-l+1;
	else
		return tree[node].right0;
}

inline int max0(int l,int r,int node)
{
	if (! node)
		return r-l+1;
	else
		return tree[node].max0;
}

inline void pushup(int l,int r,int node)
{
	int mid=(l+r)>>1;
	tree[node].left0=left0(l,mid,tree[node].left);
	if (tree[node].left0==mid-l+1)
		tree[node].left0=tree[node].left0+left0(mid+1,r,tree[node].right);
	tree[node].right0=right0(mid+1,r,tree[node].right);
	if (tree[node].right0==r-mid)
		tree[node].right0=tree[node].right0+right0(l,mid,tree[node].left);
	tree[node].max0=max(max0(l,mid,tree[node].left),max0(mid+1,r,tree[node].right));
	tree[node].max0=max(tree[node].max0,right0(l,mid,tree[node].left)+left0(mid+1,r,tree[node].right));
	tree[node].sum1=tree[tree[node].left].sum1+tree[tree[node].right].sum1;
	return;
}

inline void inserttree(int l,int r,int x,int y,int& node)
{
	if (! node)
		sum_node++,node=sum_node;
	if (l==r)
		if (! y)
			tree[node].left0=tree[node].right0=tree[node].max0=1,tree[node].sum1=0;
		else
			tree[node].left0=tree[node].right0=tree[node].max0=0,tree[node].sum1=1;
	else
	{
		int mid=(l+r)>>1;
		if (x<=mid)
			inserttree(l,mid,x,y,tree[node].left);
		else
			inserttree(mid+1,r,x,y,tree[node].right);
		pushup(l,r,node);
	}
	return;
}

inline int asktree(int l,int r,int x,int y,int node)
{
	if ((l==x) && (r==y))
		return tree[node].sum1;
	int mid=(l+r)>>1;
	if (y<=mid)
		return asktree(l,mid,x,y,tree[node].left);
	if (x>mid)
		return asktree(mid+1,r,x,y,tree[node].right);
	return asktree(l,mid,x,mid,tree[node].left)+asktree(mid+1,r,mid+1,y,tree[node].right);
}

inline void getinv(int l,int r,int node)
{
	int mid=(l+r)>>1;
	if (max0(mid+1,r,tree[node].right)==max0(l,r,node))
		getinv(mid+1,r,tree[node].right);
	else
		if (right0(l,mid,tree[node].left)+left0(mid+1,r,tree[node].right)==max0(l,r,node))
			x=mid-right0(l,mid,tree[node].left)+1,y=mid+left0(mid+1,r,tree[node].right);
		else
			getinv(l,mid,tree[node].left);
	return;
}

int main()
{
	freopen("seat.in","r",stdin);
	freopen("seat.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (i=1;i<=m;i++)
	{
		scanf("%d",&t);
		if (! t)
			scanf("%d%d",&x,&y),printf("%d\n",asktree(1,n,x,y,root));
		else
			if (M[t])
				inserttree(1,n,M[t],0,root),M[t]=0;
			else
				getinv(1,n,root),M[t]=(x+y+1)/2,inserttree(1,n,M[t],1,root);
	}
	return 0;
}
