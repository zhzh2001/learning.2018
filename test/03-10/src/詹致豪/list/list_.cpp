#include <cstdio>
#include <algorithm>
#include <vector>

using namespace std;

int g[120000],f[120000],h[120000];
int i,j,k,n;
long long s;

inline void merge(int l,int x)
{
	if (l>n)
		return;
	vector <int> V;
	V.clear();
	for (int i=l;i<=n;i++)
		if ((g[i]==x) && (! h[i]))
			V.push_back(i);
	for (int i=V.size()-1;i>=0;i--)
		k++,h[V[i]]=k,merge(V[i]+1,x+1);
	return;
}

int main()
{
	freopen("list.in","r",stdin);
	freopen("list_.out","w",stdout);
	scanf("%d",&n);
	for (i=1;i<=n;i++)
		scanf("%d",&g[i]);
	merge(1,1);
	for (i=1;i<=n/2;i++)
		swap(h[i],h[n-i+1]);
	for (i=1;i<=n;i++)
	{
		for (j=1;j<i;j++)
			if (h[j]<h[i])
				f[i]=max(f[i],f[j]);
		f[i]++,s=s+f[i];
	}
	printf("%lld",s);
	return 0;
}
