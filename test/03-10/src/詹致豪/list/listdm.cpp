#include <cstdio>
#include <cstring>
#include <ctime>
#include <algorithm>

using namespace std;

int main()
{
	freopen("list.in","w",stdout);
	srand((int) time(0));
	int n=1000;
	int g[120000];
	memset(g,0,sizeof(g));
	g[1]=1;
	for (int i=2;i<=n;i++)
		g[i]=rand()%2;
	for (int i=1;i<=n;i++)
		g[i]=g[i-1]+g[i];
	for (int i=n;i>0;i--)
		if (g[i]==g[i-1])
			g[i]=rand()%g[i]+1;
	printf("%d\n",n);
	for (int i=1;i<=n;i++)
		printf("%d ",g[i]);
	return 0;
}
