#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=1e6;
int b[N],pri[N],a[N],cnt=0,ans=0,n;
inline void get(){
	for(int i=2;i<=N;i++){
		if(!b[i])pri[++cnt]=i;
		for(int j=1;j<=cnt;j++){
			if(i*pri[j]>N)break;
			b[i*pri[j]]=1;
			if(i%pri[j]==0)break;
		}
	}
}
int p[100010];
inline bool check(int n){
	for(int i=1;i<=n;i++)
		for(int j=1;j<i;j++)if(!b[p[i]+p[j]])return 0;
	return 1;
}
inline void dfs(int x,int s){
	if(x==n+1){
		if(check(s))ans=max(ans,s);
		return;
	}
	dfs(x+1,s);
	p[s+1]=a[x];dfs(x+1,s+1);
}
int main()
{
	freopen("prime.in","r",stdin);
	freopen("baoli.out","w",stdout);
	get();n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	dfs(1,0);writeln(ans);
	return 0;
}
