#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
using namespace std;
inline int read()
{
	int k = 0, f = 1;
	char ch = getchar();
	while (ch < '0' || ch > '9')
	{
		if (ch == '-')
			f = -1;
		ch = getchar();
	}
	while (ch >= '0' && ch <= '9')
	{
		k = k * 10 + ch - '0';
		ch = getchar();
	}
	return k * f;
}
inline void write(int x)
{
	if (x < 0)
		putchar('-'), x = -x;
	if (x > 9)
		write(x / 10);
	putchar(x % 10 + '0');
}
inline void writeln(int x)
{
	write(x);
	puts("");
}
const int N = 1e6;
int vk[N], pri[N], cnt = 0;
inline void get()
{
	for (int i = 2; i <= N; i++)
	{
		if (!vk[i])
			pri[++cnt] = i;
		for (int j = 1; j <= cnt; j++)
		{
			if (i * pri[j] > N)
				break;
			vk[i * pri[j]] = 1;
			if (i % pri[j] == 0)
				break;
		}
	}
}
int a[3010], b[3010], dist[2500010], cur[2500010], vis[2500010];
int n, m, s, t, ans = 0, nedge = -1, p[2500010], c[2500010], nex[2500010], head[2500010];
inline void addedge(int a, int b, int v)
{
	p[++nedge] = b;
	nex[nedge] = head[a];
	head[a] = nedge;
	c[nedge] = v;
}
inline bool bfs()
{
	memset(dist, -1, sizeof dist);
	dist[s] = 1;
	queue<int> q;
	q.push(s);
	while (!q.empty())
	{
		int now = q.front();
		q.pop();
		for (int k = head[now]; k > -1; k = nex[k])
			if (c[k] && dist[p[k]] == -1)
			{
				dist[p[k]] = dist[now] + 1;
				if (!vis[p[k]])
				{
					vis[p[k]] = 1;
					q.push(p[k]);
				}
			}
		vis[now] = 0;
	}
	return dist[t] > -1;
}
inline int dfs(int x, int low)
{
	if (x == t)
		return low;
	int used = 0, a;
	for (int k = cur[x]; k > -1; k = nex[k])
		if (c[k] && dist[x] + 1 == dist[p[k]])
		{
			cur[x] = k;
			a = dfs(p[k], min(c[k], low - used));
			if (a)
				c[k] -= a, c[k ^ 1] += a, used += a;
			if (used == low)
				break;
		}
	if (used == 0)
		dist[x] = -1;
	return used;
}
inline int dinic()
{
	int flow = 0;
	while (bfs())
	{
		for (int i = s; i <= t; i++)
			cur[i] = head[i];
		flow += dfs(s, 1e9);
	}
	return flow;
}
int main()
{
	freopen("prime.in", "r", stdin);
	freopen("prime.out", "w", stdout);
	memset(nex, -1, sizeof nex);
	memset(head, -1, sizeof head);
	get();
	n = read();
	s = 0;
	t = n + 1;
	int k[2] = {0};
	for (int i = 1; i <= n; i++)
	{
		int x = read();
		if (x & 1)
			a[++k[0]] = x;
		else
			b[++k[1]] = x;
	}
	for (int i = 1; i <= k[0]; i++)
		addedge(s, i, 1), addedge(i, s, 0);
	for (int i = 1; i <= k[1]; i++)
		addedge(i + k[0], t, 1), addedge(t, i + k[0], 0);
	for (int i = 1; i <= k[0]; i++)
		for (int j = 1; j <= k[1]; j++)
			if (!vk[a[i] + b[j]])
			{
				addedge(i, j + k[0], 1);
				addedge(j + k[0], i, 0);
			}
	ans = n - dinic();
	writeln(ans);
	return 0;
}
