#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int n,a[100010],b[100010],f[100010],ans=0,cnt=0;
vector<int>v[100010],k[100010];
inline void dfs(int x){
	b[x]=++cnt;int s=v[x].size();
	for(int i=0;i<s;i++)dfs(v[x][i]);
}
int main()
{
	freopen("list.in","r",stdin);
	freopen("list.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=n;i;i--){
		k[a[i]].push_back(i);
		for(int j=i-1;j>=0;j--)if(a[j]==a[i]-1){
			f[i]=j;v[j].push_back(i);
			break;
		}
	}
	b[0]=1;
	for(int i=1;i<=n;i++){
		int s=k[i].size();
		for(int j=0;j<s;j++)if(!b[k[i][j]]){
			int now=k[i][j];dfs(now);
		}
	}
	cnt=0;memset(f,0,sizeof f);
	for(int i=n;i;i--){
		int p=lower_bound(f+1,f+cnt+1,b[i])-f;
		if(p>cnt)cnt++;f[p]=b[i];
		ans+=p;
	}
	writeln(ans);
	return 0;
}
