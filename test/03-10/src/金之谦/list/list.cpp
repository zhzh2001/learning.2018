#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
using namespace std;
typedef long long ll;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(ll x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(ll x){
	write(x);puts("");
}
int n,a[100010],b[100010],f[100010],cnt=0;
vector<int>v[100010],k[100010],q[100010];
ll ans=0;
inline void dfs(int x){
	b[x]=++cnt;int s=v[x].size();
	for(int i=0;i<s;i++)dfs(v[x][i]);
}
int main()
{
	freopen("list.in","r",stdin);
	freopen("list.out","w",stdout);
	n=read();
	q[0].push_back(0);
	for(int i=1;i<=n;i++)a[i]=read(),q[a[i]].push_back(i);
	for(int i=n;i;i--){
		k[a[i]].push_back(i);
		int now=q[a[i]-1][q[a[i]-1].size()-1];
		v[now].push_back(i);
		q[a[i]].pop_back();
	}
	b[0]=1;
	for(int i=1;i<=n;i++){
		int s=k[i].size();
		for(int j=0;j<s;j++)if(!b[k[i][j]]){
			int now=k[i][j];dfs(now);
		}
	}
	cnt=0;
	for(int i=n;i;i--){
		int p=lower_bound(f+1,f+cnt+1,b[i])-f;
		if(p>cnt)cnt++;f[p]=b[i];
		ans+=p;
	}
	writeln(ans);
	return 0;
}
