#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int n,a[10010],b[10010],f[10010];
int main()
{
	freopen("list.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();int ans=0;
	for(int i=1;i<=n;i++)a[i]=read(),b[i]=i;
	do{
		bool flag=1;
		for(int i=1;i<=n;i++){
			f[i]=0;
			for(int j=0;j<i;j++)if(b[i]>b[j])f[i]=max(f[i],f[j]+1);
			if(f[i]!=a[i]){flag=0;break;}
		}
		if(flag){
			int sum=0;
			for(int i=n;i;i--){
				f[i]=0;
				for(int j=i+1;j<=n+1;j++)if(b[i]>b[j])f[i]=max(f[i],f[j]+1);
				sum+=f[i];
			}
//			if(sum==40){
//				for(int i=1;i<=n;i++)cout<<b[i]<<' ';puts("");
//			}
			ans=max(ans,sum);
		}
	}while(next_permutation(b+1,b+n+1));
	writeln(ans);
	return 0;
}
