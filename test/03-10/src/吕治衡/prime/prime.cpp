#include<bits/stdc++.h>
#define ll long long
#define N 200010
#define eps 1e-15
#define ld long double
inline ll read(){
	char c=getchar();while (c!='-'&&(c<'0'||c>'9'))c=getchar();
	ll k=0,kk=1;if (c=='-')c=getchar(),kk=-1;
	while (c>='0'&&c<='9')k=k*10+c-'0',c=getchar();return kk*k;
}using namespace std;
void write(ll x){if (x<0)x=-x,putchar('-');if (x/10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);puts("");}
int n,ff[3010],f[3010][3010],flag[1000010],sum,ans,a[3010],dd;
ld t;
inline int calc(int x){int dd=1;for (int i=1;i<=n;i++)dd+=ff[i]&f[x][i];return dd;}
signed main(){
	freopen("prime.in","r",stdin);freopen("prime.out","w",stdout);
	srand(time(0));n=read();for (int i=1;i<=n;i++)a[i]=read();
	for (int i=2;i<=N;i++)if (!flag[i])
		for (int j=i+i;j<=N;j+=i)flag[j]=1;
	for (int i=1;i<=n;i++)for (int j=1;j<=n;j++)f[i][j]=flag[a[i]+a[j]];
	for (int i=1;i<=n;i++)if (a[i]%2==0)ff[i]=1,sum++;else ff[i]=0;
	t=1;while (t>eps){
		ans=max(ans,sum);int d=rand()%n+1;while (ff[d])d=rand()%n+1;
		int dd=calc(d);if (dd>sum||exp(1.0*(dd-sum)/t)*RAND_MAX>rand()){
			sum=dd;ff[d]=1;for (int i=1;i<=n;i++)ff[i]&=f[d][i];
		}t*=0.9999;
	}sum=0;
	for (int i=1;i<=n;i++)if (a[i]%3==0)ff[i]=1,sum++;else ff[i]=0;
	t=1;while (t>eps){
		ans=max(ans,sum);int d=rand()%n+1;while (ff[d])d=rand()%n+1;
		int dd=calc(d);if (dd>sum||exp(1.0*(dd-sum)/t)*RAND_MAX>rand()){
			sum=dd;ff[d]=1;for (int i=1;i<=n;i++)ff[i]&=f[d][i];
		}t*=0.9999;
	}sum=0;
	for (int i=1;i<=n;i++)if (a[i]%2==1&&a[i]!=dd)ff[i]=1,sum++,dd+=a[i]==1;else ff[i]=0;
	t=1;while (t>eps){
		ans=max(ans,sum);int d=rand()%n+1;while (ff[d])d=rand()%n+1;
		int dd=calc(d);if (dd>sum||exp(1.0*(dd-sum)/t)*RAND_MAX>rand()){
			sum=dd;ff[d]=1;for (int i=1;i<=n;i++)ff[i]&=f[d][i];
		}t*=0.9999;
	}writeln(ans);
}


