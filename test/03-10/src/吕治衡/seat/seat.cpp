#include<bits/stdc++.h>
#define LL long long
inline LL read(){
	char c=getchar();while (c!='-'&&(c<'0'||c>'9'))c=getchar();
	LL k=0,kk=1;if (c=='-')c=getchar(),kk=-1;
	while (c>='0'&&c<='9')k=k*10+c-'0',c=getchar();return kk*k;
}using namespace std;
void write(LL x){if (x<0)x=-x,putchar('-');if (x/10)write(x/10);putchar(x%10+'0');}
void writeln(LL x){write(x);puts("");}
int n,q,l[8000010],r[8000010],ll[8000010],rr[8000010],lll[8000010],rrr[8000010],sum[8000010],kk,c;
int h[10000010],f[10000010];
bool flag[8000010];
struct lsg{int x,y,z;}a[8000010];
int hash(int x){
	int k=x%10000007;while (h[k]&&h[k]!=x)k=k==10000007?0:k+1;
	h[k]=x;return k;
}void pushup(int d){
	lsg x;x.x=rr[l[d]]-rrr[l[d]]+1;x.y=ll[r[d]]+lll[r[d]]-1;x.z=x.y-x.x+1;
	if (!sum[l[d]])lll[d]=lll[l[d]]+lll[r[d]];else lll[d]=lll[l[d]];
	if (!sum[r[d]])rrr[d]=rrr[r[d]]+rrr[l[d]];else rrr[d]=rrr[r[d]];
	sum[d]=sum[l[d]]+sum[r[d]];
	a[d]=a[r[d]];if (x.z>a[d].z)a[d]=x;if (a[l[d]].z>a[d].z)a[d]=a[l[d]];
}inline void doit(int d,int l,int r){ll[d]=l;rr[d]=r;lll[d]=rrr[d]=a[d].z=r-l+1;sum[d]=0;a[d].x=l;a[d].y=r;}
inline void pushdown(int d){int m=(ll[d]+rr[d])>>1;l[d]=++kk;r[d]=++kk;doit(l[d],ll[d],m);doit(r[d],m+1,rr[d]);}
void putit(int x,int y,int d){
	if (ll[d]==rr[d]){
		if (y)lll[d]=rrr[d]=0,sum[d]=1,a[d].x=a[d].y=a[d].z=0;
			else lll[d]=rrr[d]=a[d].z=1,sum[d]=0,a[d].x=a[d].y=ll[d];
		return;
	}if (!flag[d])flag[d]=1,pushdown(d);
	int m=(ll[d]+rr[d])>>1;
	if (x<=m)putit(x,y,l[d]);else putit(x,y,r[d]);
	pushup(d);
}int findit(int x,int d){
	if (rr[d]<=x)return sum[d];
	if (!x||ll[d]>x||!sum[d])return 0;
	return findit(x,l[d])+findit(x,r[d]);
}
signed main(){
	freopen("seat.in","r",stdin);freopen("seat.out","w",stdout);
	n=read();q=read();a[1].x=1;a[1].y=n;a[1].z=n;ll[1]=1;rr[1]=n;kk=1;
	while (q--){
		c=read();if (c==0){
			int l=read(),r=read();writeln(findit(r,1)-findit(l-1,1));
		}else{
			int dd=hash(c);if (f[dd]){
				putit(f[dd],0,1);f[dd]=0;
			}else{
				f[dd]=(a[1].x+a[1].y+1)>>1;putit(f[dd],1,1);
			}
		}
	}
}
/*
19 10
9
8
14
8
18
8
4
11
3
0 0 7
*/ 

