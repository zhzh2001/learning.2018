#include<bits/stdc++.h>
using namespace std;
#define ll int
#define llu unsigned long long
#define ld double
#define llu unsigned long long
#define rep(i,x,y)  for(ll i=x;i<y;++i)
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
#define pi acos(-1)
#define mk make_pair<ll,ll>
#define pa pair<ll,ll>
#define lf else if
#define max(x,y)    ((x)<(y)?(y):(x))
#define min(x,y)    ((x)<(y)?(x):(y))
#define sqr(x)      ((x)*(x))
#define abs(x)      ((x)>0?(x):-(x))
#define Mul(x,y)    ((x)=1LL*(x)*(y)%mod)
#define Add(x,y)    ((x)=((x)+(y))%mod)
#define Max(x,y)    ((x)=((x)<(y)?(y):(x)))
#define Min(x,y)    ((x)=((x)>(y)?(y):(x)))
#define E(x)        return writeln(x),0
#define p(x)        printf("~%lld~\n",x)
#define pp(x,y)     printf("~~%lld %lld~~\n",x,y)
#define ppp(x,y,z)  printf("~~~%lld %lld %lld~~~\n",x,y,z)
#define pppp(a,b,c,d)	printf("~~~%lld %lld %lld %lld\n",a,b,c,d)
#define f_in(x)     freopen(x,"r",stdin)
#define f_out(x)    freopen(x,"w",stdout)
#define open(x)     f_in(x".in"),f_out(x".out")
#define fi first
#define se second
typedef complex<double> E;
namespace SHENZHEBEI{
#define NEG 1
#if NEG
    inline ll read(){    ll x=0,f=1; char ch=getchar();   for (;!isdigit(ch);ch=getchar()) if (ch=='-') f=-1;  for (;isdigit(ch);ch=getchar())  x=x*10-48+ch;   return x*f; }
    inline void write(ll x){    if (x<0) putchar('-'),x=-x;  if (x>=10)   write(x/10);    putchar(x%10+'0');  }
#else
    inline ll read(){    ll x=0; char ch=getchar();   for (;!isdigit(ch);ch=getchar());    for (;isdigit(ch);ch=getchar())  x=x*10-48+ch;   return x;   }
    inline void write(ll x){    if (x>=10)   write(x/10);    putchar(x%10+'0');  }
#endif
    inline char readchar(){ char ch=getchar();   for(;isspace(ch);ch=getchar());  return ch;  }
    inline ll readstr(char *s){ char ch=getchar();   int cur=0;  for(;isspace(ch);ch=getchar());      for(;!isspace(ch);ch=getchar())  s[cur++]=ch;    s[cur]='\0';    return cur; }
    inline void writeln(ll x){  write(x);   puts("");   }
    inline ld getreal(){    static ld lbc;  scanf("%lf",&lbc);  return lbc; }
}using namespace SHENZHEBEI;
struct dt{
	ll ls,rs,sum,L_kong,R_kong,pos_L,pos_R;
}tr[10000000],tmp;
ll cnt,n,rt;map<ll,ll>mp;
dt New(ll l,ll r){tmp.sum=0;tmp.L_kong=tmp.R_kong=r-l+1;tmp.pos_L=l;tmp.pos_R=r;return tmp;}
void Change(ll l,ll r,ll &p,ll pos){
	if (!p)tr[p=++cnt]=New(l,r);
	if (l==r){
		if (tr[p].sum)tr[p]=New(l,r);
		else tr[p].sum=1,tr[p].L_kong=tr[p].R_kong=0,tr[p].pos_L=l+1,tr[p].pos_R=r;
		return;
	}ll mid=(l+r)>>1;
	pos<=mid?Change(l,mid,tr[p].ls,pos):Change(mid+1,r,tr[p].rs,pos);
	if (!tr[p].ls)tr[tr[p].ls=++cnt]=New(l,mid);
	if (!tr[p].rs)tr[tr[p].rs=++cnt]=New(mid+1,r);
	dt ls=tr[tr[p].ls],rs=tr[tr[p].rs];
	tr[p].sum=ls.sum+rs.sum;
	tr[p].R_kong=rs.sum?rs.R_kong:(ls.R_kong+r-mid);
	tr[p].L_kong=ls.sum?ls.L_kong:(rs.L_kong+mid-l+1);
	ll	sz1=ls.pos_R-ls.pos_L+1,
		sz2=ls.R_kong+rs.L_kong,
		sz3=rs.pos_R-rs.pos_L+1;
	if (sz1>sz2&&sz1>sz3)	tr[p].pos_L=ls.pos_L,tr[p].pos_R=ls.pos_R;
	lf (sz2>sz3)			tr[p].pos_L=mid-ls.R_kong+1,tr[p].pos_R=mid+rs.L_kong;
	else					tr[p].pos_L=rs.pos_L,tr[p].pos_R=rs.pos_R;
}
ll Query_sum(ll l,ll r,ll p,ll s,ll t){
	if (!p)return 0;
	if (l==s&&r==t)return tr[p].sum;
	ll mid=(l+r)>>1;
	if (t<=mid)return Query_sum(l,mid,tr[p].ls,s,t);
	lf (s>mid)return Query_sum(mid+1,r,tr[p].rs,s,t);
	else return Query_sum(l,mid,tr[p].ls,s,mid)+Query_sum(mid+1,r,tr[p].rs,mid+1,t);
}
int main(){
	freopen("seat.in","r",stdin);
	freopen("seat.out","w",stdout);	
	n=read();tr[rt=cnt=1]=New(1,n);
	for(ll Q=read(),opt;Q--;){
		if ((opt=read())==0){
			ll l=read(),r=read();
			writeln(Query_sum(1,n,rt,l,r));
		}else{
			if (mp[opt]){
				Change(1,n,rt,mp[opt]);
				mp[opt]=0;
			}else{
				mp[opt]=(tr[1].pos_R+tr[1].pos_L+1)/2;
				Change(1,n,rt,(tr[1].pos_R+tr[1].pos_L+1)/2);
			}
		}
	}
}
