#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 100005;
int a[N],n,b[N];
ll ans;
int mx[N*4];
int nxt[N];

inline int Max(int x,int y){ if(x > y) return x; return y; }

void change(int num,int l,int r,int x,int v){
	if(l == r){ mx[num] = v; return; }
	int mid = (l+r)>>1;
	if(x <= mid) change(num<<1,l,mid,x,v);
	else change(num<<1|1,mid+1,r,x,v);
	mx[num] = Max(mx[num<<1],mx[num<<1|1]);
}
int query(int num,int l,int r,int x){
	if(l == r) return mx[num];
	int mid = (l+r)>>1;
	if(x <= mid) return query(num<<1,l,mid,x);
	else return Max(query(num<<1|1,mid+1,r,x),mx[num<<1]);
}


int main(){
	freopen("list.in","r",stdin);
	freopen("list.out","w",stdout);
	n = read();
	for(int i = 1;i <= n;++i) a[i] = read();
	for(int i = n;i >= 1;--i){
		b[i] = query(1,1,n,a[i])+1;
		nxt[a[i]] = i;
		ans += b[i];
		if(nxt[a[i]+1] != 0) b[i] = Max(b[i],b[nxt[a[i]+1]]);
		change(1,1,n,a[i],b[i]);
	}
	printf("%lld\n", ans);
	return 0;
}