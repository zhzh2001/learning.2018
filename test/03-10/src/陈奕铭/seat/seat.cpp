#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}
#define pii pair<int,int>
#define mk(x,y) make_pair(x,y)

int n,q,x,opt,W1,W2,y,rt;
map<int,int> mp;
map<int,int> weight;
set<int> seat;
set<pii > block;
set<int>:: iterator it,it2;
set<pii >:: reverse_iterator rit;
struct node{ int ls,rs,sum; }tr[13600005];int cnt;

void add(int &num,int l,int r,int x,int v){
	if(num == 0) num = ++cnt;
	tr[num].sum += v;
	if(l == r) return;
	int mid = (l+r)>>1;
	if(x <= mid) add(tr[num].ls,l,mid,x,v);
	else add(tr[num].rs,mid+1,r,x,v);
}
int query(int num,int l,int r,int x){
	if(num == 0) return 0;
	if(l == r) return tr[num].sum;
	int mid = (l+r)>>1;
	if(x <= mid) return query(tr[num].ls,l,mid,x);
	else return tr[tr[num].ls].sum+query(tr[num].rs,mid+1,r,x);
}

int main(){
	freopen("seat.in","r",stdin);
	freopen("seat.out","w",stdout);
	n = read(); q = read();
	weight[0] = n;
	seat.insert(0);
	block.insert(mk(n,0));
	while(q--){
		opt = read();
		if(opt != 0){
			if(mp.count(opt)){
				x = mp[opt]; add(rt,1,n,x,-1);
				it = seat.lower_bound(x);it2 = it; --it;
				seat.erase(it2); y = *it;		//seat down
				W1 = weight[y]; block.erase(mk(W1,y));
				W2 = weight[x]; block.erase(mk(W2,x));
				block.insert(mk(W1+W2+1,y));	//block down
				mp.erase(opt); //mp down
				weight.erase(x); weight[y] = W1+W2+1; // weight down
			}else{
				rit = block.rbegin();
				W2 = rit->first; y = rit->second;
				x = y+(W2/2)+1; add(rt,1,n,x,1);
				block.erase(mk(W2,y));
				mp[opt] = x; seat.insert(x);	// mp down // set down
				W1 = x-y-1; W2 = W2-W1-1;
				block.insert(mk(W1,y)); block.insert(mk(W2,x));	// block down
				weight[y] = W1; weight[x] = W2;	// weight down
			}
		}
		else{
			int l = read(),r = read();
			int lsum = query(rt,1,n,l-1),rsum = query(rt,1,n,r);
			printf("%d\n", rsum-lsum);
		}
	}
	return 0;
}