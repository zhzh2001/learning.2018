#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int M = 200000,N = 3005;
bool vis[M+5];
int pri[20005],cntp;
int a[N],n,ans;
int s[N],cnts;
int t[N],cntt,sum;
bool mp[15][15];

void getpri(){
	for(int i = 2;i <= M;++i){
		if(!vis[i]) pri[++cntp] = i;
		for(int j = 1;j <= cntp && pri[j]*i <= M;++j){
			vis[pri[j]*i] = true;
			if(i%pri[j] == 0) break;
		}
	}
}

inline bool pds(int x){
	for(int i = 1;i <= cntt;++i)
		if(!vis[t[i]+x]) return false;
	// printf("%d ",x);
	return true;
}

inline bool pdt(int x){
	for(int i = 1;i <= cnts;++i)
		if(!vis[s[i]+x]) return false;
	// printf("%d ",x);
	return true;
}

inline bool pd(int i){
	for(int j = 0;j < n;++j)
		if((i&(1<<j)) != 0)
			for(int k = j+1;k < n;++k)
				if(((i&(1<<k)) != 0))
					if(mp[j+1][k+1]) return false;
	return true;
}

inline int getsum(int x){
	int sum = 0;
	while(x){
		if(x&1) ++sum;
		x >>= 1;
	}
	return sum;
}

void solve1(){
	ans = 0;
	// for(int i = 1;i <= n;++i) printf("%d ",a[i]);
	// puts("");
	for(int i = 1;i <= n;++i)
		for(int j = 1;j <= n;++j)
			if(i != j)
				if(!vis[a[i]+a[j]])
					mp[i][j] = mp[j][i] = true;
	// for(int i = 1;i <= n;++i,puts(""))
	// 	for(int j = 1;j <= n;++j)
	// 		printf("%d ",mp[i][j]);
	// puts("");
	int num = (1<<n);
	for(int i = 0;i < num;++i)
		if(pd(i)) {
			if(getsum(i) > ans){
				ans = getsum(i);
				// printf("%d\n", i);
			}
		}
	printf("%d\n", ans);
}

int main(){
	freopen("prime.in","r",stdin);
	freopen("prime.out","w",stdout);
	getpri();n = read();
	for(int i = 1;i <= n;++i) a[i] = read();
	sort(a+1,a+n+1);
	if(n <= 10){
		solve1();
		return 0;
	}
	for(int i = 1;i <= n;++i){
		if(a[i]%2){
			if(a[i] == a[i-1] && a[i] == 1) continue;
			t[++cntt] = a[i];
		}else s[++cnts] = a[i];
	}
	sum = 0;
	for(int i = 1;i <= cnts;++i)
		if(pds(s[i])) ++sum;
	// puts("");
	ans = sum+cntt; sum = 0;
	for(int i = 1;i <= cntt;++i)
		if(pdt(t[i])) ++sum;
	// puts("");
	ans = max(ans,sum+cnts);
	printf("%d\n", ans);
	return 0;
}