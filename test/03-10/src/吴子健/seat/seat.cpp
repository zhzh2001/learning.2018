#include <bits/stdc++.h>
#define LL long long

using namespace std;
typedef map<int,int>::iterator Iter;
//data
const int INF=0x3f3f3f3f;
int N,Q;
map<int ,int> M;
struct node
{
	int lson,rson;
	int lmax,rmax,totmax;//max value
	int maxp;//pointer to the left pos of the range
	int vv;//total number of people
} T[int(1e7+5e6)];
int Tcnt;
int Root;
/**
reverse the sequence
choose the left range when multiple ranges are available.
*/
//code
void debug_node(int id)
{
	puts("************************");
	printf("id=%d\n",id);
	printf("lson=%d rson=%d\n",T[id].lson,T[id].rson);
	printf("lmax=%d rmax=%d totmax=%d maxp=%d\n vv=%d\n",T[id].lmax,T[id].rmax,T[id].totmax,T[id].maxp,T[id].vv);
	puts("************************");
}
void debug_map()
{
	Iter it = M.begin();
	while(it!=M.end())
	{
		printf("id=%d pos=%d\n",it->first,it->second);
		++it;
	}
}
void create_node(int &id,int L,int R)
{
	id=++Tcnt;
	T[id].lmax=T[id].rmax=T[id].totmax=(R-L+1);
	T[id].maxp=L;
	T[id].vv=0;
}
void push_up(int id,int L,int R)
{
	if(L!=R)
	{
		int &lson=T[id].lson,&rson=T[id].rson,mid=(R+L)>>1;
		if(!lson) create_node(lson,L,mid);
		if(!rson) create_node(rson,mid+1,R);

		if(T[lson].vv==0)
			T[id].lmax=T[lson].totmax+T[rson].lmax;
		else
			T[id].lmax=T[lson].lmax;

		if(T[rson].vv==0)
			T[id].rmax=T[rson].totmax+T[lson].rmax;
		else
			T[id].rmax=T[rson].rmax;

		T[id].totmax=T[lson].totmax,T[id].maxp=T[lson].maxp;
		if(T[lson].rmax+T[rson].lmax>T[id].totmax)
			T[id].totmax=T[lson].rmax+T[rson].lmax,T[id].maxp=mid-T[lson].rmax+1;
		if(T[rson].totmax>T[id].totmax)
			T[id].totmax=T[rson].totmax,T[id].maxp=T[rson].maxp;

		T[id].vv=T[lson].vv+T[rson].vv;
	}
	else
	{
		if(T[id].vv)
			T[id].lmax=T[id].rmax=T[id].totmax=0,T[id].maxp=-INF;
		else
			T[id].lmax=T[id].rmax=T[id].totmax=1,T[id].maxp=L;
	}
}
void modify(int x,int &id,int L,int R)
{
	if(!id)	create_node(id,L,R);
	//debug_node(id);
	if(L==x&&x==R)
	{
		T[id].vv=1-T[id].vv;
		push_up(id,L,R);
		return;
	}
	int mid=(L+R)>>1;
	if(x<=mid)
		modify(x,T[id].lson,L,mid);
	else
		modify(x,T[id].rson,mid+1,R);
	push_up(id,L,R);
	//debug_node(id);
}
int query(int B,int E,int &id,int L,int R)
{
	//printf("query[%d,%d] in %d [%d,%d]\n",B,E,id,L,R);
	//debug_node(id);
	//system("pause");
	if(L>E||R<B)	return 0;
	if(!id)	create_node(id,L,R);
	if(L>=B&&R<=E)
	{
		//printf("[%d,%d] vv=%d\n",L,R,T[id].vv);
		return T[id].vv;
	}	
	int mid=(L+R)>>1;
	int t=query(B,E,T[id].lson,L,mid)+query(B,E,T[id].rson,mid+1,R);
	return t;
}
int find_pos()
{
	int l=T[Root].maxp,len=T[Root].totmax,r=l+len-1;
	return (l+r)>>1;
}

// main
int main()
{
	freopen("seat.in","r",stdin);
	freopen("seat.out","w",stdout);
	scanf("%d%d",&N,&Q);
	//printf("N=%d Q=%d\n",N,Q);
	create_node(Root,1,N);
	int opt,l,r;
	for(int i=1;i<=Q;++i)
	{
		scanf("%d",&opt);
		//puts("\n\n");
		if(opt)
		{

			Iter it=M.find(opt);
			if(it!=M.end())
			{
				int pos=it->second;
				M.erase(it);
				//exit(0);
				//printf("now query %d,which we already have in %d.\n",opt,pos);
				//debug_map();
				modify(pos,Root,1,N);
			}
			else
			{
				int pos=find_pos();
				M[opt]=pos;
				//printf("now query %d,which we will have in %d.\n",opt,pos);
				//debug_map();
				modify(pos,Root,1,N);
				//debug_node(5);
			}
		}
		else
		{
			scanf("%d%d",&l,&r);
			int lreal=N-r+1,rreal=N-l+1;
			//printf("query in [%d,%d]\n",lreal,rreal);
			int t=query(lreal,rreal,Root,1,N);
			printf("%d\n",t);
		}
	}
	return 0;
}
