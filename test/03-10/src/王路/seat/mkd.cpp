#include <bits/stdc++.h>
#include <Windows.h>
using namespace std;

inline int Rand() { return (rand() << 15) + rand(); }

inline int Uniform(int l, int r) { return Rand() % (r - l + 1) + l; }

int main() {
  srand(GetTickCount());
  freopen("seat.in", "w", stdout);
  int n = 1e3, q = 1e3;
  ios::sync_with_stdio(false);
  cout << n << ' ' << q << endl;
  for (int i = 1; i <= q; ++i) {
    int opt = rand() & 1;
    if (opt == 0) {
      int l = Uniform(1, n), r = Uniform(l, n);
      cout << opt << ' ' << l << ' ' << r << endl;
    } else {
      cout << Uniform(1, n) << endl;
    }
  }
  return 0;
}