#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <map>
using namespace std;

const int kMaxN = 1e5 + 5;

struct SegNode {
  int ls, rs, left_len, right_len, max_len, max_pos, peo;
  inline SegNode() {}
  inline SegNode(int _ls, int _rs, int _left_len, int _right_len, int _max_len, int _max_pos)
    : ls(_ls), rs(_rs), left_len(_left_len), right_len(_right_len), max_len(_max_len), max_pos(_max_pos), peo(0) {}
} mem[kMaxN * 35 * 2];

char buf[1 << 20];
map<int, int> mp;
int root, n, q, ncnt;

inline char NextChar() {
  static char *S = buf, *T = buf;
  if (S == T) {
    T = (S = buf) + fread(buf, sizeof(char), 1 << 20, stdin);
    if (S == T)
      return EOF;
  }
  return *S++;
}
template <typename T>
inline void Read(T &x) {
  x = 0;
  T flag = 1;
  char ch;
  for (ch = NextChar(); isspace(ch); ch = NextChar())
    ;
  if (ch == '-') {
    ch = getchar();
    flag = -1;
  }
  for (; isdigit(ch); ch = NextChar())
    x = x * 10 + ch - '0';
  x *= flag;
}

int SegQuery(int x, int l, int r, int ql, int qr) {
  if (!x)
    return 0;
  if (ql <= l && r <= qr)
    return mem[x].peo;
  int mid = (l + r) >> 1;
  if (qr <= mid)
    return SegQuery(mem[x].ls, l, mid, ql, qr);
  if (ql > mid)
    return SegQuery(mem[x].rs, mid + 1, r, ql, qr);
  return SegQuery(mem[x].ls, l, mid, ql, qr) + SegQuery(mem[x].rs, mid + 1, r, ql, qr);
}
inline void PushUp(int x, int l, int r, int mid) {
  if (mem[mem[x].ls].max_len > mem[mem[x].rs].max_len) {
    mem[x].max_len = mem[mem[x].ls].max_len;
    mem[x].max_pos = mem[mem[x].ls].max_pos;
  } else {
    mem[x].max_len = mem[mem[x].rs].max_len;
    mem[x].max_pos = mem[mem[x].rs].max_pos;
  }
  if (mem[mem[x].ls].right_len + mem[mem[x].rs].left_len > mem[x].max_len
    || (mem[mem[x].ls].right_len + mem[mem[x].rs].left_len == mem[x].max_len && mid - mem[mem[x].ls].right_len + 1 > mem[x].max_pos)) {
    mem[x].max_len = mem[mem[x].ls].right_len + mem[mem[x].rs].left_len;
    mem[x].max_pos = (mid - mem[mem[x].ls].right_len + 1);
  }
  mem[x].right_len = mem[mem[x].rs].right_len;
  if (mem[mem[x].rs].right_len == r - mid)
    mem[x].right_len += mem[mem[x].ls].right_len;
  mem[x].left_len = mem[mem[x].ls].left_len;
  if (mem[mem[x].ls].left_len == mid - l + 1)
    mem[x].left_len += mem[mem[x].rs].left_len;
  mem[x].peo = mem[mem[x].ls].peo + mem[mem[x].rs].peo;
}
void SegInsert(int x, int l, int r, int pos) {
  if (l == r) {
    mem[x].peo = 1;
    mem[x].left_len = mem[x].right_len = mem[x].max_len = 0;
    return;
  }
  int mid = (l + r) >> 1;
  if (!mem[x].ls) {
    mem[x].ls = ++ncnt;
    mem[mem[x].ls] = SegNode(0, 0, mid - l + 1, mid - l + 1, mid - l + 1, l);
  }
  if (!mem[x].rs) {
    mem[x].rs = ++ncnt;
    mem[mem[x].rs] = SegNode(0, 0, r - mid, r - mid, r - mid, mid + 1);
  }
  if (pos <= mid)
    SegInsert(mem[x].ls, l, mid, pos);
  else
    SegInsert(mem[x].rs, mid + 1, r, pos);
  PushUp(x, l, r, mid);
}
void SegRemove(int x, int l, int r, int pos) {
  if (l == r) {
    mem[x].peo = 0;
    mem[x].left_len = mem[x].right_len = mem[x].max_len = 1;
    mem[x].max_pos = l;
    return;
  }
  int mid = (l + r) >> 1;
  // if (!mem[x].ls) {
  //   mem[x].ls = ++ncnt;
  //   mem[mem[x].ls] = SegNode(0, 0, mid - l + 1, mid - l + 1, mid - l + 1, l);
  // }
  // if (!mem[x].rs) {
  //   mem[x].rs = ++ncnt;
  //   mem[mem[x].rs] = SegNode(0, 0, r - mid, r - mid, r - mid, mid + 1);
  // }
  if (pos <= mid)
    SegRemove(mem[x].ls, l, mid, pos);
  else
    SegRemove(mem[x].rs, mid + 1, r, pos);
  PushUp(x, l, r, mid);
}

int main() {
  freopen("seat.in", "r", stdin);
  freopen("seat.out", "w", stdout);  
  Read(n), Read(q);
  int testcnt = 0;
  mem[root = ncnt = 1] = SegNode(0, 0, n, n, n, 1);
  while (q--) {
    int opt;
    Read(opt);
    if (opt == 0) {
      int l, r;
      Read(l), Read(r);
      printf("%d\n", SegQuery(root, 1, n, l, r));
    } else {
      int sta = mp[opt];
      if (sta) {
        SegRemove(root, 1, n, sta);
        mp[opt] = 0; 
      } else {
        int pos = mem[root].max_pos + (mem[root].max_len + 1) / 2 + (mem[root].max_len % 2 == 0) - 1;
        // cout << pos << endl;
        SegInsert(root, 1, n, pos);
        mp[opt] = pos;
      }
    }
  }
  // cerr << ncnt << endl;
  return 0;
}