#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <map>
using namespace std;

char buf[1 << 20];
inline char NextChar() {
  static char *S = buf, *T = buf;
  if (S == T) {
    T = (S = buf) + fread(buf, sizeof(char), 1 << 20, stdin);
    if (S == T)
      return EOF;
  }
  return *S++;
}
template <typename T>
inline void Read(T &x) {
  x = 0;
  T flag = 1;
  char ch;
  for (ch = NextChar(); isspace(ch); ch = NextChar())
    ;
  if (ch == '-') {
    ch = getchar();
    flag = -1;
  }
  for (; isdigit(ch); ch = NextChar())
    x = x * 10 + ch - '0';
  x *= flag;
}

map<int, int> mp;
int root, n, q, ncnt, val[10000000];
int p[123], pc;

int main() {
  freopen("seat.in", "r", stdin);
  freopen("bf.out", "w", stdout);
    Read(n), Read(q);
  int testcnt = 0;
  while (q--) {
    int opt;
    Read(opt);
    if (opt == 0) {
      int l, r;
      Read(l), Read(r);
      int ans = 0;
      pc = 0;
      for (int i = l; i <= r; ++i) {
        if (val[i]) {
          ++ans;
        }
      }

      printf("%d\n", ans);
    } else {
      int sta = mp[opt];
      if (sta) {
        val[sta] = 0;
        mp[opt] = 0;
      } else {
        int len = 0, pos = 1, max_len = 0, max_pos;
        for (int i = 1; i <= n; ++i) {
          if (val[i]) {
            len = 0;
            pos = i + 1;
          } else {
            ++len;
            if (len >= max_len) {
              max_pos = pos;
              max_len = len;
            } 
          }
        }
        pos = max_pos + (max_len + 1) / 2 + (max_len % 2 == 0) - 1;
        // cerr << pos << ' ' << max_pos << ' ' << max_len << endl;
        // cout << pos << endl;
        mp[opt] = pos;
        val[pos] = opt;
      }
    }
  }
  return 0;
}