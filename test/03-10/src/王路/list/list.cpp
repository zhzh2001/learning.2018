#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
using namespace std;

char buf[1 << 20];
inline char NextChar() {
  static char *S = buf, *T = buf;
  if (S == T) {
    T = (S = buf) + fread(buf, sizeof(char), 1 << 20, stdin);
    if (S == T)
      return EOF;
  }
  return *S++;
}
template <typename T>
inline void Read(T &x) {
  x = 0;
  T flag = 1;
  char ch;
  for (ch = NextChar(); isspace(ch); ch = NextChar())
    ;
  if (ch == '-') {
    ch = getchar();
    flag = -1;
  }
  for (; isdigit(ch); ch = NextChar())
    x = x * 10 + ch - '0';
  x *= flag;
}

const int kMaxN = 1e5 + 5;
int n, a[kMaxN], f[kMaxN], g[kMaxN];

int main() {
  freopen("list.in", "r", stdin);
  freopen("list.out", "w", stdout);
  Read(n);
  for (int i = 1; i <= n; ++i)
    Read(a[i]);
  int ans = 0;
  int pcnt = 0;
  for (int i = 1; i <= n; ++i) {
    for (int j = n; j >= 1; --j)
      if (a[j] == i)
        f[j] = ++pcnt;
  }
  // for (int i = 1; i <= n; ++i)
  //   fprintf(stderr, "%d%c", f[i], " \n"[i == n]);
  for (int i = 1; i <= n; ++i) {
    g[i] = 1;
    for (int j = 1; j < i; ++j) {
      if (f[j] > f[i])
        g[i] = max(g[i], g[j] + 1);
    }
    ans += g[i];
  }
  // for (int i = 1; i <= n; ++i)
  //   fprintf(stderr, "%d%c", g[i], " \n"[i == n]);
  printf("%d\n", ans);
  return 0;
}