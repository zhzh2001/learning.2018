#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
using namespace std;

char buf[1 << 20];
inline char NextChar() {
	static char *S = buf, *T = buf;
	if (S == T) {
		T = (S = buf) + fread(buf, sizeof(char), 1 << 20, stdin);
		if (S == T)
			return EOF;
	}
	return *S++;
}
template <typename T>
inline void Read(T &x) {
	x = 0;
	T flag = 1;
	char ch;
	for (ch = NextChar(); isspace(ch); ch = NextChar())
		;
	if (ch == '-') {
		ch = getchar();
		flag = -1;
	}
	for (; isdigit(ch); ch = NextChar())
		x = x * 10 + ch - '0';
	x *= flag;
}

const int kMaxN = 3005, kVal = 1e5 + 5;
int a[kMaxN], n, prime[kVal], not_prime[kVal];
int pri[kMaxN][10], f[kMaxN][kMaxN];

int main() {
	freopen("prime.in", "r", stdin);
	freopen("prime.out", "w", stdout);
	Read(n);
	for (int i = 1; i <= n; ++i)
		Read(a[i]);
  int tot = 0;
  for(int i = 2; i < kVal; i++) {
    if(!not_prime[i]) {
      prime[++tot] = i;
    }
    for(int j = 1; prime[j]*i < kVal; j++) {
      not_prime[prime[j]*i] = 1;
      if(i % prime[j] == 0) {
        break;
      }
    }
  }
	if (n <= 10) {
		int ans = 0;
		for (int i = 0; i < (1 << n); ++i) {
      bool can = true;
			for (int j = 0; j < n; ++j) {
				for (int k = 0; k < n; ++k) {
					if (j == k)
						continue;
					if (i & (1 << j))
						if (i & (1 << k))
							if (!not_prime[a[j + 1] + a[k + 1]]) {
								can = false;
								break;
							}
				}
			}
      if (!can)
        continue;
      ans = max(ans, __builtin_popcount(i));
		}
    printf("%d\n", ans);
    return 0;
	}
	int ans = 0, prip = 0;
	for (int i = 1; i <= tot; ++i) {
		int cnt = 0;
		for (int j = 1; j <= n; ++j) {
			if (j % prime[i] == 0)
				++cnt;
		}
		if (cnt > ans) {
			ans = cnt;
			prip = prime[i];
		}
	}
	printf("%d\n", ans);
	cerr << prip << endl;
	return 0;
}
