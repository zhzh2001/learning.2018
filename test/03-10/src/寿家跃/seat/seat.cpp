#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<cmath>
#include<map>

#define pi acos(-1)

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}

typedef long long ll;

const int N=100005;
const int Ln=32;

bool ST;

#define mid (L+R>>1)

struct node{
	int ld,rd,respos,resval;
	node(){
		ld=rd=respos=resval=0;
	}
	void set(int pos,int fab){
		fab^=1;
		ld=rd=fab;
		if (fab) respos=pos,resval=1;
			else respos=0,resval=0;
	}
};
inline node Merge(int L,int R,node A,node B){
	node res;
	res.resval=res.respos=-1;
	res.ld=((A.ld==mid-L+1)?A.ld+B.ld:A.ld);
	res.rd=((B.rd==R-mid)?B.rd+A.rd:B.rd);
	if (B.resval>res.resval || (B.resval==res.resval && B.respos>res.respos)){
		res.resval=B.resval;
		res.respos=B.respos;
	}
	if (A.rd+B.ld>res.resval || (A.rd+B.ld==res.resval && mid-A.rd+1>res.respos)){
		res.resval=A.rd+B.ld;
		res.respos=mid-A.rd+1;
	}
	if (A.resval>res.resval || (A.resval==res.resval && A.respos>res.respos)){
		res.resval=A.resval;
		res.respos=A.respos;
	}
	return res;
}
const int Limp=N*Ln*4;

int n,q,Root;

struct Segment_Tree{
	node v[Limp]; 
	int ls[Limp],rs[Limp],size[Limp];
	int tree_nodes;
	void Build(int L,int R,int &p){
		p=++tree_nodes;
		v[p].ld=v[p].rd=R-L+1;
		v[p].respos=L;
		v[p].resval=R-L+1;
	}
	void Modify(int L,int R,int &p,int pos,int fab){
		if (L==R){
			size[p]=fab;
			v[p].set(pos,fab);return;
		}
		if (!ls[p]) Build(L,mid,ls[p]);
		if (!rs[p]) Build(mid+1,R,rs[p]);
		
		if (pos<=mid) Modify(L,mid,ls[p],pos,fab);
			else Modify(mid+1,R,rs[p],pos,fab);
		v[p]=Merge(L,R,v[ls[p]],v[rs[p]]);
//		printf("L=%d R=%d ld=%d rd=%d pos=%d val=%d\n",L,R,v[p].ld,v[p].rd,v[p].respos,v[p].resval);
		size[p]=size[ls[p]]+size[rs[p]];
	}
	int Query(int L,int R,int p,int x,int y){
		if (!p) return 0;
		if (L==x && R==y) return size[p];
		if (y<=mid) return Query(L,mid,ls[p],x,y);else
		if (x>mid) return Query(mid+1,R,rs[p],x,y);else{
			return Query(L,mid,ls[p],x,mid)+Query(mid+1,R,rs[p],mid+1,y);
		}
	}
	void Dfs(int L,int R,int p){
		if (p==0) return;
		if (L==R){
			printf("pos=%d size=%d\n",L,size[p]);return;
		}
		Dfs(L,mid,ls[p]);
		Dfs(mid+1,R,rs[p]);
	}
}T;

map<int,int>Map;

bool ED;
int main(){
	freopen("seat.in","r",stdin);
	freopen("seat.out","w",stdout);
//	printf("%lf\n",1.0*(&ED-&ST)/1024/1024);
	n=read(),q=read();
	T.Build(1,n,Root);
	Rep(i,1,q){
		int val=read();
		if (val!=0){
			if (!Map[val]){
				node res=T.v[Root];
				int pos=res.respos+res.resval/2;
				T.Modify(1,n,Root,pos,1);
				Map[val]=pos;
			}
			else{
				T.Modify(1,n,Root,Map[val],0);
				Map[val]=0;
			}
		}
		else{
			int x=read(),y=read();
			printf("%d\n",T.Query(1,n,1,x,y));
		}
	}
}
