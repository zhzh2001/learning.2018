#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<cmath>

#define pi acos(-1)

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}

typedef long long ll;

const int N=3005;
const int L=100005;

int n,A[N],C[N],p[N],Max;
int B[L],P[L];

int main(){
	freopen("prime.in","r",stdin);
	freopen("prime.out","w",stdout);
	n=read(),Max=0;
	Rep(i,1,n) A[i]=read(),Max=max(Max,A[i]);
	Rep(i,2,2*Max){
		if (!B[i]) P[++*P]=i;
		for (int j=1,k;j<=*P && (k=P[j]*i)<=2*Max;j++){
			B[k]=true;
			if (i%P[j]==0) break;
		}
	}
//	Rep(i,1,2*Max) if (!B[i]) printf("%d\n",i);
	Rep(i,1,n) p[i]=i;
	int Ans=0;
	Rep(t,1,n*10){
		random_shuffle(p+1,p+n+1);
		Rep(i,1,n) C[i]=false;
		int res=0;
		Rep(i,1,n) if (!C[p[i]]){
			res++;
			Rep(j,1,n) if (!B[A[p[i]]+A[p[j]]]) C[p[j]]=true;
		}
		Ans=max(Ans,res);
	}
	printf("%d\n",Ans);
}
