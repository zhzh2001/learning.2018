#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<cmath>

#define pi acos(-1)

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}

typedef long long ll;

const int N=100005;

int n,A[N],f[N];

int v[N*4];
#define mid (L+R>>1)
void Modify(int L,int R,int p,int pos,int val){
	if (L==R){
		v[p]=max(v[p],val);return;
	}
	if (pos<=mid) Modify(L,mid,p<<1,pos,val);
		else Modify(mid+1,R,p<<1|1,pos,val);
	v[p]=max(v[p<<1],v[p<<1|1]);
}
int Query(int L,int R,int p,int x,int y){
	if (L==x && R==y) return v[p];
	if (y<=mid) return Query(L,mid,p<<1,x,y);
	if (x>mid) return Query(mid+1,R,p<<1|1,x,y);
	return max(Query(L,mid,p<<1,x,mid),Query(mid+1,R,p<<1|1,mid+1,y));
}
int main(){
	freopen("list.in","r",stdin);
	freopen("list.out","w",stdout);
	n=read();
	Rep(i,1,n) A[i]=read();
	ll Ans=0;
	Dep(i,n,1){
		f[i]=Query(1,n,1,1,A[i])+1;
		Modify(1,n,1,A[i],f[i]);
		Modify(1,n,1,A[i],Query(1,n,1,1,min(n,A[i]+1)));
		Ans+=f[i];
	}
	printf("%lld\n",Ans);
}
/*
val[i]<=val[j] (j<i && A[j]>=A[i])
*/
