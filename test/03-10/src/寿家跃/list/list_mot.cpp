#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<cmath>

#define pi acos(-1)

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}

typedef long long ll;

const int N=100005;

int n,A[N],f[N],size[N],res[N];
ll Ans;
int main(){
	freopen("list.in","r",stdin);
//	freopen("list.out","w",stdout);
	n=read();
	Rep(i,1,n) A[i]=read();
	Rep(i,1,2*n) size[i]=1;
	Dep(i,n,1){
		Rep(j,1,A[i]) f[i]=max(f[i],res[j]+1);
		res[A[i]]=max(res[A[i]],res[A[i]+1]);
		res[A[i]]=max(res[A[i]],f[i]);
		Ans+=f[i];
	}
	printf("%lld\n",Ans);
}
/*
val[i]<=val[j] (j<i && A[j]>=A[i])
*/
