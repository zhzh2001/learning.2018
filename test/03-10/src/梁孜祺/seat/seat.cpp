#include<bits/stdc++.h>
#define N 200005
#define pa pair<int,int>
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define It set<int>::iterator
using namespace std;
int n,Q,h[N],tot;
set<pa> s,S;
set<pa>::iterator it,it2;
map<int,int> mp,mmp;
struct que{int opt,x,y;}q[N];
int t[N];
inline void add(int x,int val){
	for(;x<=tot;x+=(x&-x)) t[x]+=val;
}
inline int query(int x){
	int sum=0;
	for(;x;x-=(x&-x)) sum+=t[x];
	return sum;
}
int main(){
	freopen("seat.in","r",stdin);
	freopen("seat.out","w",stdout);
	scanf("%d%d",&n,&Q);
	For(i,1,Q){
		scanf("%d",&q[i].opt);
		if(!q[i].opt) scanf("%d%d",&q[i].x,&q[i].y),h[++tot]=q[i].x,h[++tot]=q[i].y;
	}
	sort(h+1,h+1+tot);tot=unique(h+1,h+1+tot)-h-1;
	For(i,1,Q) if(!q[i].opt) q[i].x=lower_bound(h+1,h+1+tot,q[i].x)-h,q[i].y=lower_bound(h+1,h+1+tot,q[i].y)-h;
	s.insert(make_pair(n,1));
	S.insert(make_pair(1,n));
	For(i,1,Q){
		if(!q[i].opt){
			int r=query(q[i].y),l=query(q[i].x);
			if(mmp[h[q[i].x]]) r++;
			printf("%d\n",r-l);
		}else{
			if(!mp[q[i].opt]){
				it=s.end();it--;
				int l=(*it).second,r=(*it).second+(*it).first-1;
				mp[q[i].opt]=(l+r+1)/2;int P=(l+r+1)/2;mmp[P]=1;
				int ttt=lower_bound(h+1,h+1+tot,P)-h;add(ttt,1);
				s.erase(it);
				if(P-l!=0) s.insert(make_pair(P-l,l));
				if(r-P!=0) s.insert(make_pair(r-P,P+1));
				it=S.lower_bound(make_pair(l,r));
				S.erase(it);
				if(l<=P-1) S.insert(make_pair(l,P-1));
				if(P+1<=r) S.insert(make_pair(P+1,r));
			}else{
				int P=mp[q[i].opt];
				it=S.lower_bound(make_pair(P,0));
				int l=P,r=P,ttt=lower_bound(h+1,h+1+tot,P)-h;
				add(ttt,-1);
				mmp.erase(P);mp.erase(q[i].opt);
				if((*it).first==P+1){
					r=(*it).second;
					S.erase(it);
					it2=s.lower_bound(make_pair(r-P,P+1));
					s.erase(it2);
					it=S.lower_bound(make_pair(P,0));
				}
				--it;
				if((*it).second==P-1){
					l=(*it).first;
					S.erase(it);
					it2=s.lower_bound(make_pair(P-l,l));
					s.erase(it2);
				}
				S.insert(make_pair(l,r));
				s.insert(make_pair(r-l+1,l));
			}
		}
	}
	return 0;
}
