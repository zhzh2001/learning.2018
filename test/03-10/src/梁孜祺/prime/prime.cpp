#include<bits/stdc++.h>
#define N 200000
#define pa pair<int,int>
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
int n,a[N+5],vis[3005][3005],f[N+5],phi[N+5];
inline void init(){
	For(i,2,N){
		if(!f[i]) phi[++phi[0]]=i;
		for(int j=1;j<=phi[0]&&phi[j]*i<=N;j++){
			f[i*phi[j]]=1;
			if(i%phi[j]==0) break;
		}
	}
}
int b[N+5],p[N+5],tot;
int main(){
	freopen("prime.in","r",stdin);
	freopen("prime.out","w",stdout);
	init();scanf("%d",&n);
	For(i,1,n) scanf("%d",&a[i]);
	For(i,1,n) For(j,1,n) if(i!=j) vis[i][j]=(f[a[i]+a[j]]);
	For(i,1,n) b[i]=i;
	srand(19280817);int mx=0;
	int m=50000000/n/n;
	For(T,1,m){
		random_shuffle(b+1,b+1+n);
		p[tot=1]=b[1];
		For(i,2,n){
			bool flag=0;
			For(j,1,tot) if(!vis[b[i]][p[j]]){flag=1;break;}
			if(!flag) p[++tot]=b[i];
		}
		mx=max(mx,tot);
	}
	printf("%d\n",mx);
	return 0;
}
