#include<bits/stdc++.h>
#define ll long long
#define N 100005
#define pa pair<int,int>
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
int n,a[N],f[N],pre[N],tag[N],ind,b[N];
vector <int> t[N];
inline void dfs(int x){
	int end=t[x].size();end--;
	For(i,0,end) dfs(t[x][i]);
	b[x]=++ind;
}
int p[N],tot;
int main(){
	freopen("list.in","r",stdin);
	freopen("list.out","w",stdout);
	scanf("%d",&n);
	For(i,1,n) scanf("%d",&a[i]);
	tag[0]=0;
	For(i,1,n){
		tag[a[i]]=i;
		pre[i]=tag[a[i]-1];
		t[tag[a[i]-1]].push_back(i);
	}
	dfs(0);
	For(i,1,n) b[i]=n-b[i]+1;
	For(i,1,n/2) swap(b[i],b[n-i+1]);
/*	For(i,1,n) cout<<b[i]<<" ";
	Rep(i,n,1){
		f[i]=1;
		For(j,i+1,n) if(b[i]>b[j]) f[i]=max(f[i],f[j]+1);
	}*/
	f[1]=1;p[tot=1]=b[1];
//	For(i,1,n) cout<<b[i]<<" ";cout<<endl;
	For(i,2,n){
		if(p[tot]<b[i]) p[++tot]=b[i],f[i]=tot;
		else{
			int x=lower_bound(p+1,p+1+tot,b[i])-p;
			p[x]=b[i];f[i]=x;
		}
	}
	ll ans=0;
	For(i,1,n) ans+=f[i];
	printf("%lld\n",ans);
	return 0;
}
