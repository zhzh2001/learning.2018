#include <bits/stdc++.h>
#define N 100020
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}
int c[N], a[N];
void update(int x) {
  // printf("added+++ %d\n", x);
  for (; x < N; x += x & -x)
    c[x] ++;
}
int query(int x) {
  // printf("asked %d\n", x);
  int ans = 0;
  for (; x; x ^= x & -x)
    ans += c[x];
  return ans;
}
int b[N], d[N];
int main(int argc, char const *argv[]) {
  freopen("list.in", "r", stdin);
  freopen("list.out", "w", stdout);

  int n = read(), ans = 0;
  for (int i = 1; i <= n; i++) {
    a[i] = read();
    // b[i] = d[a[i] - 1];
    // d[a[i]] = i;
  }
  for (int i = n; i; i--) {
    ans += query(a[i]) + 1;
    // printf("%d\n", query(i + 1));
    update(a[i]);
  }
  cout << ans << endl;

  return 0;
}