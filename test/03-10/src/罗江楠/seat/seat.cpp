#include <bits/stdc++.h>
#define N 100020
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}
struct node {
  int val, pos;
  friend bool operator < (const node &a, const node &b) {
    return a.val == b.val ? a.pos > b.pos : a.val > b.val;
  }
};
map<int, int> mp, sp, id, pre, nxt; // 每个人的坐标
set<node> st;
set<int> poss;
int main(int argc, char const *argv[]) {
  freopen("seat.in", "r", stdin);
  freopen("seat.out", "w", stdout);
  int n = read(), q = read();
  st.insert((node){n, 0});
  sp[0] = n;
  mp[0] = 0;
  id[0] = 0;
  pre[int(1e9+1)] = 0;
  nxt[0] = int(1e9+1);
  while (q --) {
    int op = read();
    if (op) {
      if (mp[op]) { // delete a man

        // printf("Deleted %d which between %d and %d\n", op, pre[op], nxt[op]);
        int len = sp[op];
        int pos = mp[op];
        if (len) st.erase(st.find((node){len, pos}));
        int pre_id = pre[op];
        if (sp[pre_id]) st.erase(st.find((node){sp[pre_id], mp[pre_id]}));
        sp[pre_id] += len + 1;
        st.insert((node){sp[pre_id], mp[pre_id]});
        // printf("pre_id = %d\n", pre_id); // debug

        nxt[pre_id] = nxt[op];
        pre[nxt[op]] = pre_id;

        // printf("Now nxt[%d] = %d\n", pre[op], nxt[pre[op]]);
        // printf("Now pre[%d] = %d\n", nxt[op], pre[nxt[op]]);
        poss.erase(pos);
        // printf("Deleted pos %d\n", pos); // debug
        // for (auto s : st) // debug
          // cout << "{" << s.val << ", " << s.pos << "}" << endl; // debug

        mp[op] = 0;

      } else { // add a men

        node x = *st.begin();
        st.erase(st.begin());
        int len = x.val, xid = id[x.pos];
        // printf("len = %d, xid = %d, pos = %d\n", len, xid, x.pos); // debug
        int llen = (len - 1) / 2, rlen = len - llen - 1;
        if (!(len&1)) swap(llen, rlen);
        int pos = x.pos + llen + 1; // new position
        mp[op] = pos;
        id[pos] = op;
        sp[xid] = llen;
        sp[op] = rlen;
        if (llen)
          st.insert((node){llen, x.pos});
        if (rlen)
          st.insert((node){rlen, pos});
        pre[op] = xid;
        nxt[op] = nxt[xid];
        pre[nxt[xid]] = op;
        nxt[xid] = op;

        // printf("Added %d between %d and %d\n", op, pre[op], nxt[op]);

        poss.insert(pos);
        // printf("Added pos %d\n", pos); // debug
        // for (auto s : st) // debug
          // cout << "{" << s.val << ", " << s.pos << "}" << endl; // debug
        // for (auto s : pre) // debug
          // cout << "pre[" << s.first << "] = " << s.second << endl; // debug
        // for (auto s : nxt) // debug
          // cout << "nxt[" << s.first << "] = " << s.second << endl; // debug
        

      }
    } else { // 询问
      int l = read(), r = read();
      int ans = 0;
      set<int>::iterator it = poss.lower_bound(l);
      while (it != poss.end() && *it <= r) ++ it, ++ ans;
      printf("%d\n", ans);
    }
  }
  // 我稳了
  return 0;
}