#include<map>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define pa pair<int,int>
using namespace std;
const int N=100100;
inline int read(){
	int x=0,c=getchar(),f=0;
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
map<int,int>pos;
struct cqz{
	int mx,ps,lm,rm,sm;
	void nw(int l,int r){
		mx=r-l+1;sm=0;
		ps=l;lm=r;rm=l;
	}
}tr[N*40];
int ct,n,m,rt,ls[N*40],rs[N*40];
void Mx(int&ps,int&mx,int Ps,int Mx){
	if(Mx>mx||(Mx==mx&&Ps>ps))
	ps=Ps,mx=Mx;
}
void merge(cqz&x,cqz&y,cqz&z,int l,int md,int r){
	z.sm=x.sm+y.sm;
	z.mx=x.mx;z.ps=x.ps;
	Mx(z.ps,z.mx,y.ps,y.mx);
	Mx(z.ps,z.mx,x.rm,y.lm-x.rm+1);
	z.lm=x.lm==md?y.lm:x.lm;
	Mx(z.ps,z.mx,l,z.lm-l+1);
	z.rm=y.rm==md+1?x.rm:y.rm;
	Mx(z.ps,z.mx,z.rm,r-z.rm+1);
}
int get(int i,int l,int r,int x,int y){
	if(l>=x&&r<=y)return tr[i].sm;
	int re=0,md=(l+r)>>1;
	if(x<=md)re+=get(ls[i],l,md,x,y);
	if(y>md)re+=get(rs[i],md+1,r,x,y);
	return re;
}
void add(int&i,int l,int r,int x,int k){
	if(!i){i=++ct;tr[i].nw(l,r);}
	tr[i].sm+=k;if(l==r){
		tr[i].mx=1-tr[i].sm;
		tr[i].lm=l-tr[i].sm;
		tr[i].rm=r+tr[i].sm;
		return;
	}int md=(l+r)>>1;
	if(x<=md)add(ls[i],l,md,x,k);
	else add(rs[i],md+1,r,x,k);
	if(!ls[i]){ls[i]=++ct;tr[ct].nw(l,md);}
	if(!rs[i]){rs[i]=++ct;tr[ct].nw(md+1,r);}
	merge(tr[ls[i]],tr[rs[i]],tr[i],l,md,r);
}
int main(){
	freopen("seat.in","r",stdin);
	freopen("seat.out","w",stdout);
	n=read();m=read();
	rt=ct=1;tr[1].nw(1,n);
	for(int o,x,y,i=1;i<=m;i++){
		o=read();if(!o){
			x=read();y=read();
			printf("%d\n",get(rt,1,n,x,y));
		}else{
			x=pos[o];
			if(x){
				add(rt,1,n,x,-1);
				pos[o]=0;
			}else{
				x=tr[rt].ps+tr[rt].mx/2;
				add(rt,1,n,x,1);
				pos[o]=x;
			}
		}
	}return 0;
}
