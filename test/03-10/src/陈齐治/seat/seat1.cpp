#include<map>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define pa pair<int,int>
using namespace std;
const int N=10000100;
inline int read(){
	int x=0,c=getchar(),f=0;
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
map<int,int>pos;
int sm[N];
int ct,n,m,rt;
void Mx(int&ps,int&mx,int Ps,int Mx){
	if(Mx>mx||(Mx==mx&&Ps>ps))
	ps=Ps,mx=Mx;
}
int ans,pp,f[N];
int get(){
	ans=0;pp=1;
	for(int i=n;i;i--){
		f[i]=sm[i]?0:f[i+1]+1;
		Mx(pp,ans,i,f[i]);
	}return pp+ans/2;
}
int main(){
	freopen("seat.in","r",stdin);
	freopen("seat1.out","w",stdout);
	n=read();m=read();
	for(int o,x,y,i=1;i<=m;i++){
		o=read();if(!o){
			x=read();y=read();
			ans=0;
			for(o=x;o<=y;o++)ans+=sm[o];
			printf("%d\n",ans);
		}else{
			x=pos[o];
			if(x){
				sm[x]--;
				pos[o]=0;
			}else{
				x=get();
				sm[x]++;
				pos[o]=x;
			}
		}
	}return 0;
}
