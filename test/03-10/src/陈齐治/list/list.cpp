#include<queue>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=500100;
inline int read(){
	int x=0,c=getchar(),f=0;
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
priority_queue<int>q;ll ans;
int n,f[N],la[N],tr[N],du[N],L,h[N],to[N],ne[N];
inline void addl(int x,int y){du[y]++;ne[++L]=h[x];h[x]=L;to[L]=y;}
inline void Mx(int&x,int y){if(y>x)x=y;}
inline void add(int x,int k){for(;x;x-=x&-x)Mx(tr[x],k);}
inline int get(int x){int r=0;for(;x<=n;x+=x&-x)Mx(r,tr[x]);return r;}
int main(){
	freopen("list.in","r",stdin);
	freopen("list.out","w",stdout);
	int i,x,k;n=read();
	for(i=1;i<=n;i++){
		x=read();
		addl(i,la[x]);la[x]=i;
		if(x>1)addl(la[x-1],i);
	}for(i=1;i<=n;i++)if(!du[i])q.push(i);
	for(i=1;i<=n;i++){
		x=q.top();q.pop();
		ans+=f[x]=get(x)+1;
		add(x,f[x]);
		for(k=h[x];k;k=ne[k])
		if(!(--du[to[k]]))q.push(to[k]);
	}write(ans);putchar('\n');
	return 0;
}
