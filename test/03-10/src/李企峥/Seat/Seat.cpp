#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
#define fi first
#define sd second
#define sf sd.fi
#define ss sd.sd
using namespace std;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
ll n,Q,t,opt,ans,l,r,x;
map<int,bool>f;
pair<int,pair<int,int> >a[100100];
int main()
{
	freopen("Seat.in","r",stdin);
	freopen("Seat.out","w",stdout);
	n=read();Q=read();
	t=1;
	a[1].fi=n;a[1].sf=1;a[1].ss=n;
	while(Q--)
	{
		opt=read();
		if(opt==0)
		{
			ans=0;
			l=read();r=read();
			For(i,l,r)if(f[i])ans++;
			cout<<ans<<endl;
		}
		else
		{
			sort(a+1,a+t+1);
			l=a[t].sf;
			r=a[t].ss;
			x=(l+r+1)/2;
			f[x]=true;
			a[t].fi=x-l;
			a[t].sf=l;
			a[t].ss=x-1;
			t++;
			a[t].fi=r-x;
			a[t].sf=x+1;
			a[t].ss=r;
		}
	}
	return 0;
}

