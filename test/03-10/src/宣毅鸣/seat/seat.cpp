#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=100005;
int l,r,op,a[N],k,n,Q,q[N];
struct Tree
{
	int l,r;
}T[N];
int get(int x)
{
	for (int i=1;i<=k;i++)
	 if (a[i]==x)return i;
	a[++k]=x;q[k]=-1;
	return k;	
}
int main()
{
	freopen("seat.in","r",stdin);
	freopen("seat.out","w",stdout);
	scanf("%d%d",&n,&Q);
	T[1].l=1,T[1].r=n+1;n=1;
	while (Q--)
	 {
	 	scanf("%d",&op);
	 	if (op==0)
	 	 {
	 	 	scanf("%d%d",&l,&r);
	 	 	int ans=0;
	 	 	for (int i=1;i<=k;i++)
	 	 	 if (q[i]>=l&&q[i]<=r)ans++;
	 	 	printf("%d\n",ans); 
	 	 }
	 	else 
		 {
		 	op=get(op);
		 	if (q[op]!=-1)
		 	 {
		 	 	int l=0,r=0;
		 	 	for (int i=1;i<=n;i++)
		 	 	 if (T[i].r==q[op])r=i;
		 	 	for (int i=1;i<=n;i++)
				 if (T[i].l-1==q[op])l=i;
				T[r].r=T[l].r;
				for (int i=l;i<n;i++)T[i]=T[i+1]; 
				n--;
				q[op]=-1;
		 	 }
		 	else
			 {
			 	int l=1;
			 	for (int i=2;i<=n;i++)
			 	 if (T[i].r-T[i].l>T[l].r-T[l].l||
				 (T[i].r-T[i].l==T[l].r-T[l].l)&&T[i].r>T[l].r)l=i;
			 	int mid=(T[l].l+T[l].r)/2; 
				n++;
				T[n].l=mid+1;T[n].r=T[l].r; 
			 	T[l].r=mid;	
				q[op]=mid;			
			 } 
		 } 
	 }
	return 0; 
}
