#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=3005,M=200005;
int flag[M],match[N],f[N],ne[N*N],fi[N],num,zz[N*N],x,k,n,a[N],b[N],n1,n2;
void jb(int x,int y)
{
	ne[++num]=fi[x];
	fi[x]=num;
	zz[num]=y;
}
int dfs(int x,int y)
{
	for (int i=fi[x];i;i=ne[i])
	 if (f[zz[i]]!=y)
	  {
	  	f[zz[i]]=y;
	  	if (!match[zz[i]]||dfs(match[zz[i]],y))
	  	 {
	  	 	match[zz[i]]=x;
	  	 	return 1;
	  	 }
	  }
	return 0;  
}
int main()
{
	freopen("prime.in","r",stdin);
	freopen("prime.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++)
	 {
	 	scanf("%d",&x);
	 	if (x==1)
	 	 {
	 	 	k++;
	 	 	if (k==1)a[++n1]=x;
	 	 }
	 	else
		 {
		 	if (x%2!=0)a[++n1]=x;
		 	else b[++n2]=x;
		 } 
	 }
	sort(a+1,a+n1+1);
	sort(b+1,b+n2+1);
	int P=a[n1]+b[n2];  
	flag[1]=1;
	int ans=n1+n2; 
	for (int i=2;i<P;i++)
	 if (!f[i])for (int j=2*i;j<P;j+=i)flag[j]=1;
	for (int i=1;i<=n1;i++)
	 for (int j=1;j<=n2;j++)
	  if (!flag[a[i]+b[(j+i)%n2+1]])jb(i,(j+i)%n2+1);
	for (int i=1;i<=n1;i++)ans-=dfs(i,i);
	printf("%d",ans); 
	return 0;
}
