#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstring>
using namespace std;
const int N=200005;
int ne[N],num,fi[N],zz[N],last[N],a[N],d[N],l,b[N],n,k,f[N],c[N];
long long ans;
void down(int x)
{
	int i=x;
	if (x*2<=l&&d[i]<d[x*2])i=x*2;
	if (x*2<l&&d[i]<d[x*2+1])i=x*2+1;
	if (i!=x)
	 {
	 	swap(d[i],d[x]);
	 	down(i);
	 }
}
void up(int x)
{
	if (x==1)return;
	if (d[x]>d[x/2])
	 {
	 	swap(d[x],d[x/2]);
	 	up(x/2);
	 }
}
void jb(int x,int y)
{
	ne[++num]=fi[x];
	fi[x]=num;
	zz[num]=y;
}
void push(int x)
{
	d[++l]=x;
	up(l);
}
int main()
{
	freopen("list.in","r",stdin);
	freopen("list.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++)scanf("%d",&a[i]);
	for (int i=1;i<=n;i++)
	 if (a[i]==1)push(i);
	for (int i=1;i<=n;i++)jb(a[i],i);
	for (int i=1;i<=n;i++)
	 {
	 	int ll=d[1];
	 	d[1]=d[l--];
	 	down(1);
	 	b[ll]=i; 
	 	for (int &j=fi[a[ll]+1];j;j=ne[j])
		 if (zz[j]<ll)break;
		 else push(zz[j]);
	 }	  
	for (int i=n;i;i--)
	 {
	  	if (b[i]>=c[k])
		 {
		 	c[++k]=b[i];
		 	ans+=k;
		 	continue;
		 }
	  	int l=1,r=k;
	  	while (l<r)
	  	 {
	  	 	int mid=(l+r)/2;
	  	 	if (c[mid]>b[i])r=mid;
	  	 	else l=mid+1;
	  	 }
	  	c[l]=b[i];
		ans+=l; 
	 } 
	printf("%lld",ans); 
	return 0;
}
