#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstring>
using std::max;
using std::min;
typedef long long LL;
#define dd c=getchar()
inline LL read(){LL a=0,b=1;char dd;while(!isdigit(c)&&c!='-')dd;
	if(c=='-'){b=-b;dd;}while(isdigit(c)){a=a*10+c-'0';dd;}return a*b;}
#undef dd
#define ls (nod<<1)
#define rs (nod<<1|1)
LL tree[400003];
void pushup(LL nod,LL l,LL r){
	tree[nod]=max(tree[ls],tree[rs]);
}
void build(LL nod,LL l,LL r){
	if(l==r){
		tree[nod]=0;
		return;
	}
	LL mid=(l+r)>>1;
	build(ls,l,mid);
	build(rs,mid+1,r);
	pushup(nod,l,r);
}
void wrt(LL nod,LL l,LL r,LL x,LL v){
	if(l==r){
		tree[nod]=max(tree[nod],v);
		return;
	}
	LL mid=(l+r)>>1;
	if(x<=mid)wrt(ls,l,mid,x,v);
	else wrt(rs,mid+1,r,x,v);
	pushup(nod,l,r);
}
LL query(LL nod,LL l,LL r,LL x,LL y){
	if(l==x&&r==y)return tree[nod];
	LL mid=(l+r)>>1,ans=0;
	if(x<=mid)ans=max(ans,query(ls,l,mid,x,min(y,mid)));
	if(y>mid)ans=max(ans,query(rs,mid+1,r,max(x,mid+1),y));
	return ans;
}
#undef ls
#undef rs
LL n,a[100003],ans=0,v[100003],b[100003],lt[100003],nx[100003],cct[100003],x,y,z;
int main(){
	freopen("list.in","r",stdin);
	freopen("list.out","w",stdout);
	n=read();
	for(LL i=1;i<=n;i++){
		a[i]=read();
		lt[i]=b[a[i]];
		b[a[i]]=i;
	}
	memset(b,0,sizeof(b));
	for(LL i=n;i>0;i--){
		nx[i]=b[a[i]+1];
		b[a[i]]=i;
	}
	build(1,1,n);
	for(LL i=n;i>0;i--){
		v[i]=max(v[i],query(1,1,n,1,a[i]))+1;
		ans+=v[i];
		cct[i]=max(cct[nx[i]],v[i]);
		v[lt[i]]=cct[i];
		wrt(1,1,n,a[i],v[i]);
	}
	printf("%lld\n",ans);
	return 0;
}
