#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstring>
using std::max;
using std::min;
typedef long long LL;
#define dd c=getchar()
inline LL read(){LL a=0,b=1;char dd;while(!isdigit(c)&&c!='-')dd;
	if(c=='-'){b=-b;dd;}while(isdigit(c)){a=a*10+c-'0';dd;}return a*b;}
#undef dd
LL n,a[153],b[153],ans=0;
bool f[200003];
void init(){
	f[1]=1;
	for(LL i=2;i<=200000;i++)if(!f[i]){
		for(LL j=i+i;j<=200000;j+=i)f[j]=1;
	}
}
void dfs(LL p,LL s){
	ans=max(ans,p);
	if(s>n)return;
	bool fla;
	for(LL i=s;i<=n;i++){
		fla=1;
		for(LL j=1;j<=p&&fla;j++)if(!f[b[j]+a[i]])fla=0;
		if(fla){
			b[p+1]=a[i];
			dfs(p+1,i+1);
		}
	}
}
int main(){
	freopen("prime.in","r",stdin);
	freopen("prime.out","w",stdout);
	init();
	n=read();
	for(LL i=1;i<=n;i++)a[i]=read();
	dfs(0,1);
	printf("%lld\n",ans);
	return 0;
}
