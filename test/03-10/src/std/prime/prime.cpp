#include <bits/stdc++.h>
using namespace std;
const int inf = 1e9 + 7, N = 3005, M = 9000005;
int la[N], pr[M], to[M], v[M], vis[N], dis[N], n, s, t, cnt, q[N], x, a[N], b[N], cnt1, cnt2, ans;
inline void read(int &x)
{
	char c = getchar();
	x = 0;
	while (c > '9' || c < '0')
		c = getchar();
	while (c >= '0' && c <= '9')
	{
		x = x * 10 + c - '0';
		c = getchar();
	}
}
inline void add(int x, int y, int z)
{
	to[++cnt] = y;
	pr[cnt] = la[x];
	la[x] = cnt;
	v[cnt] = z;
}
bool bb;
inline bool bfs()
{
	memset(dis, -1, sizeof(dis));
	dis[s] = 0;
	int l = 0, r = 1;
	q[1] = s;
	while (l != r)
	{
		int now = q[++l];
		for (int i = la[now]; i; i = pr[i])
			if (dis[to[i]] == -1 && v[i])
			{
				dis[to[i]] = dis[now] + 1;
				q[++r] = to[i];
			}
	}
	return dis[t] != -1;
}
int dfs(int x, int flow)
{
	if (x == t)
		return flow;
	int res = flow;
	for (int i = la[x]; i; i = pr[i])
		if (dis[x] + 1 == dis[to[i]] && v[i])
		{
			int f = min(v[i], res), k = dfs(to[i], f);
			res -= k;
			v[i] -= k;
			v[i ^ 1] += k;
			if (res == 0)
				break;
		}
	if (res == flow)
		dis[x] = -233;
	return flow - res;
}
int f[200005], zs[200005], tot, kkk;
inline void make_prime()
{
	int K = 200000;
	f[1] = 1;
	for (int i = 2; i <= K; ++i)
	{
		if (!f[i])
			zs[++tot] = i;
		for (int j = 1; j <= tot && i * zs[j] <= K; ++j)
		{
			f[zs[j] * i] = 1;
			if (i % zs[j] == 0)
				break;
		}
	}
	// for (int i=1;i<=tot;++i)
	// 	cout<<zs[i]<<' ';
}
int main()
{
	freopen("prime.in", "r", stdin);
	freopen("prime.out", "w", stdout);
	make_prime();
	read(n);
	for (int i = 1; i <= n; ++i)
	{
		read(x);
		if (x & 1)
		{
			if (x != 1)
				a[++cnt1] = x;
			else
			{
				if (!bb)
				{
					bb = 1;
					a[++cnt1] = 1;
				}
				else
					++kkk;
			}
		}
		else
			b[++cnt2] = x;
	}
	cnt = 1;
	s = n + 1;
	t = n + 2;
	for (int i = 1; i <= cnt1; ++i)
	{
		add(s, i, 1);
		add(i, s, 0);
	}
	for (int i = 1; i <= cnt2; ++i)
	{
		add(i + cnt1, t, 1);
		add(t, i + cnt1, 0);
	}
	for (int i = 1; i <= cnt1; ++i)
		for (int j = 1; j <= cnt2; ++j)
			if (!f[a[i] + b[j]])
			{
				add(i, j + cnt1, 1);
				add(j + cnt1, i, 0);
			}
	while (bfs())
		ans += dfs(s, inf);
	cout << n - ans - kkk;
	// cout<<max(cnt1,cnt2);
	return 0;
}
