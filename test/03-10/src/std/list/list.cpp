#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int N=100005;
int la[N],to[N],pr[N],a[N],pos[N],n,x,y,f[N],cnt;
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void add(int x,int y)
{
	to[++cnt]=y;
	pr[cnt]=la[x];
	la[x]=cnt;
}
inline int lowbit(int x)
{
	return x&(-x);
}
void dfs(int x)
{
	pos[x]=++cnt;
	for (int i=la[x];i;i=pr[i])
		dfs(to[i]);
}
inline void insert(int x,int y)
{
	for (int i=x;i<=n;i+=lowbit(i))
		f[i]=max(f[i],y);
}
inline int ask(int x)
{
	int ans=0;
	for (int i=x;i;i-=lowbit(i))
		ans=max(ans,f[i]);
	return ans;
}
int main()
{
	freopen("list.in","r",stdin);
	freopen("list.out","w",stdout);
	read(n);
	a[0]=n+1;
	for (int i=1;i<=n;++i)
	{
		read(x);
		add(a[x-1],i);
		a[x]=i;
	}
	cnt=-1;
	dfs(n+1);
	ll ans=0;
	for (int i=n;i;--i)
	{
		x=ask(pos[i])+1;
		ans+=x;
		insert(pos[i],x);
	}
	cout<<ans;
}