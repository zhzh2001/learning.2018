#include<bits/stdc++.h>
using namespace std;
const int N=100005,M=5000005;
int a[N],f[N],g[M],n,m,ls[M],rs[M],rt,cnt;
inline unsigned sjs()
{
	return (rand())+(rand()<<8)+(rand()<<16)+(rand()<<24);
}
inline int lowbit(int x)
{
	return x&(-x);
}
void add(int l,int r,int &k,int x,int y)
{
	if (!k)
		k=++cnt;
	g[k]=max(g[k],y);
	if (l==r)
		return;
	int mid=(l+r)/2;
	if (x<=mid)
		add(l,mid,ls[k],x,y);
	else
		add(mid+1,r,rs[k],x,y);
}
int ask(int l,int r,int L,int R,int k)
{
	if (!k)
		return 0;
	if (l==L&&r==R)
		return g[k];
	int mid=(l+r)/2;
	if (R<=mid)
		return ask(l,mid,L,R,ls[k]);
	if (L>mid)
		return ask(mid+1,r,L,R,rs[k]);
	return max(ask(l,mid,L,mid,ls[k]),ask(mid+1,r,mid+1,R,rs[k]));
}
// inline int ask(int x)
// {
// 	int ans=0;
// 	for (int i=x;i;i-=lowbit(i))
// 		ans=max(ans,g[i]);
// 	return ans;
// }
// inline void add(int x,int y)
// {
// 	for (int i=x;i<=n;i+=lowbit(i))
// 		g[i]=max(g[i],y);
// }
int main()
{
	rt=++cnt;
	freopen("list.in","w",stdout);
	srand(time(0));
	n=1000;
	m=1000;
	cout<<n<<'\n';
	for (int i=1;i<=n;++i)
		a[i]=sjs()%m+1;
	for (int i=1;i<=n;++i)
	{
		f[i]=ask(1,m,1,a[i]-1,rt)+1;
		add(1,m,rt,a[i],f[i]);
		// cout<<a[i]<<' '<<f[i]<<'\n';
		cout<<f[i]<<' ';
	}
	return 0;
}