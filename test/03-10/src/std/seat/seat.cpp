#include<bits/stdc++.h>
using namespace std;
const int N=100005;
#define auto set<int>::iterator
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
int n,m,l,r,opt,cnt,root;
struct segment_tree
{
	int lson,rson,sum;
}t[N*30];
inline void segement_tree_insert(int l,int r,int pos,int sum,int &x)
{
	if (!x)
		x=++cnt;
	t[x].sum+=sum;
	if (l==r)
		return;
	int mid=(l+r)/2;
	if (pos<=mid)
		segement_tree_insert(l,mid,pos,sum,t[x].lson);
	else
		segement_tree_insert(mid+1,r,pos,sum,t[x].rson);
}
inline int segement_tree_ask(int L,int R,int l,int r,int x)
{
	if (!x)
		return 0;
	if (L==l&&R==r)
		return t[x].sum;
	int mid=(l+r)/2;
	if (L>mid)
		return segement_tree_ask(L,R,mid+1,r,t[x].rson);
	if (R<=mid)
		return segement_tree_ask(L,R,l,mid,t[x].lson);
	return segement_tree_ask(L,mid,l,mid,t[x].lson)+segement_tree_ask(mid+1,R,mid+1,r,t[x].rson);
}
inline void write(int x)
{
	if (x==0)
		putchar('0');
	int a[20];
	a[0]=0;
	while (x)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
		putchar(a[a[0]--]+'0');
	putchar('\n');
}
struct data
{
	int x,id;
	bool operator<(const data &a)const
	{
		return x==a.x?id<a.id:x<a.x;
	}
	bool operator==(const data &a)const
	{
		return (x==a.x)&&(id==a.id);
	}
};
struct heap_with_delete
{
	priority_queue<data,vector<data>,less<data> > X,Y;
	void push(data a)
	{
		X.push(a);
	}
	void del(data a)
	{
		Y.push(a);
	}
	data top()
	{
		while (Y.size()&&X.top()==Y.top())
		{
			X.pop();
			Y.pop();
		}
		return X.top();
	}
}heap;
set<int> s;
map<int,int> mp;
int main()
{
	freopen("seat.in","r",stdin);
	freopen("seat.out","w",stdout);
	read(n);
	read(m);
	s.insert(0);
	s.insert(n+1);
	heap.push((data){n,1});
	for (int i=1;i<=m;++i)
	{
		read(opt);
		if (opt)
		{
			if (mp[opt])
			{
				int x=mp[opt];
				auto it=s.lower_bound(x);
				it++;
				r=*it;
				it--;
				it--;
				l=*it;
				s.erase(x);
				heap.del((data){x-l-1,l+1});
				heap.del((data){r-x-1,x+1});
				heap.push((data){r-l-1,l+1});
				segement_tree_insert(1,n,x,-1,root);
				mp[opt]=0;
			}
			else
			{
				data y=heap.top();
				l=y.id;
				r=l+y.x-1;
				int x=(l+r+1)/2;
				heap.del(y);
				heap.push((data){x-l,l});
				heap.push((data){r-x,x+1});
				mp[opt]=x;
				s.insert(x);
				// cout<<i<<' '<<x<<'\n';
				segement_tree_insert(1,n,x,1,root);
			}
		}
		else
		{
			read(l);
			read(r);
			write(segement_tree_ask(l,r,1,n,1));
		}
	}
	return 0;
}