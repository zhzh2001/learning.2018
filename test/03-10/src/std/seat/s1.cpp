#include<cstdio>
#include<cstring>
#include<algorithm>
#include<set>
#include<map>
#include<queue>
#define N 100010
#define pa(a, b) (data){a, b}
using namespace std;

struct data{int x, id;}t1;
struct tt{int l, r, sum;}t[3000010];
int n, m, l, k, L, R, xx, x, y;
map<int, int>mp;
set<int>s;
set<int>::iterator it, it1, it2;

bool operator<(data a, data b){
    if(a.x!=b.x)return a.x<b.x;
    return a.id<b.id;
}
bool operator==(data a, data b){return a.x==b.x&&a.id==b.id;}
struct heap{
    priority_queue<data, vector<data>, less<data> >A, B;
    void ins(data x){A.push(x);}
    void del(data x){B.push(x);}
    data top(){
        while(B.size()&&A.top()==B.top()){A.pop(); B.pop();}
        return A.top();
    }
}A;

inline char gc(){
    static char now[1<<16], *S, *T;
    if(S==T){T=(S=now)+fread(now, 1, 1<<16, stdin); if(S==T)return EOF;}
    return *S++;
}
inline int read(){
    int x=0, f=1; char ch=gc();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1; ch=gc();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0'; ch=gc();}
    return x*f;
}

inline void ins(int i, int x, int y, int L, int R){
    t[i].sum+=y; if(L==R)return;
    int mid=(L+R)>>1;
    if(x<=mid){
        if(!t[i].l){l++; t[l].l=t[l].r=t[l].sum=0; t[i].l=l;}
        ins(t[i].l, x, y, L, mid);
    }else{
        if(!t[i].r){l++; t[l].l=t[l].r=t[l].sum=0; t[i].r=l;}
        ins(t[i].r, x, y, mid+1, R);
    }
}
inline int csum(int i, int a, int b, int L, int R){
    if(!i)return 0; if(a<=L&&R<=b)return t[i].sum;
    int mid=(L+R)>>1, s=0;
    if(a<=mid)s+=csum(t[i].l, a, b, L, mid);
    if(mid<b)s+=csum(t[i].r, a, b, mid+1, R);
    return s;
}

int main(){
freopen("seat.in","r",stdin);
    freopen("seat2.out","w",stdout);
    n=read(); m=read();
    l=1; t[1].l=t[1].r=t[1].sum=0;
    mp.clear(); A.ins(pa(n, 1));
    s.clear(); s.insert(0); s.insert(n+1);
    for(int i=1; i<=m; i++){
        k=read();
        if(!k){L=read(); R=read(); printf("%d\n", csum(1, L, R, 1, n)); continue;}
        if(mp[k]){
            xx=mp[k]; 
            it=s.lower_bound(xx);
            it1=it2=it; 
            it1--; 
            it2++; 
            x=*it1; 
            y=*it2;
            s.erase(xx);
            A.del(pa(xx-x-1, x+1)); 
            A.del(pa(y-xx-1, xx+1)); 
            A.ins(pa(y-x-1, x+1));
            ins(1, xx, -1, 1, n); mp[k]=0;
        }else{
            t1=A.top(); 
            L=t1.id; 
            R=L+t1.x-1;
            x=(L+R+1)>>1;
            A.del(pa(R-L+1, L)); 
            A.ins(pa(x-L, L)); 
            A.ins(pa(R-x, x+1));
            ins(1, x, 1, 1, n);
            mp[k]=x; 
            s.insert(x);
        }
    }
    return 0;
}
