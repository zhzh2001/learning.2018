#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("list.in","r",stdin);
	freopen("list.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
int a[100002],ans[100002];
pair<int,int> qwq[100002];
struct bit{
	int n;
	int a[100002];
	void add(int p,int v){
		for(;p<=n;p+=p&-p)
			a[p]=max(a[p],v);
	}
	int query(int p){
		int ans=0;
		for(;p;p-=p&-p)
			ans=max(ans,a[p]);
		return ans;
	}
}b;
int main(){
	init();
	int n=readint();
	for(int i=1;i<=n;i++)
		a[i]=readint(),qwq[i]=make_pair(a[i],n-i+1);
	sort(qwq+1,qwq+n+1);
	for(int i=1;i<=n;i++)
		ans[n-qwq[i].second+1]=i;
	/*b.n=n;
	for(int i=1;i<=n;i++){
		printf("%d\n",ans[i]);
		int qwq=b.query(ans[i])+1;
		assert(qwq==a[i]);
		b.add(ans[i],qwq);
	}*/
	ll sum=0;
	b.n=n;
	memset(b.a,0,sizeof(b.a));
	for(int i=n;i;i--){
		int qwq=b.query(ans[i])+1;
		b.add(ans[i],qwq);
		sum+=qwq;
	}
	printf("%lld\n",sum);
}