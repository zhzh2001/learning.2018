#include <bits/stdc++.h>
using namespace std;
typedef int ll;
const int MAXSIZE=10000020;
const int INF=0x3f3f3f3f;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("prime.in","r",stdin);
	freopen("prime.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
const int maxm=5000002;
const int maxn=3009;
/*struct graph{
	int n,m;
	struct edge{
		int to,next;
	}e[maxm];
	int first[maxn];
	void addedge(int from,int to){
		e[++m]=(edge){to,first[from]};
		first[from]=m;
	}
	bool vis[maxn];
	int match[maxn];
	bool left[maxn];
	bool dfs(int u){
		if (vis[u])
			return false;
		vis[u]=1;
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			// assert(!left[v]);
			if (!match[v] || dfs(match[v])){
				match[u]=v;
				match[v]=u;
				return true;
			}
		}
		return false;
	}
	bool cnt[maxn];
	int work(){
		int ans=0;
		for(int i=1;i<=n;i++){
			if (!left[i] || match[i])
				continue;
			memset(vis,0,n+1);
			ans+=dfs(i);
		}
		return ans;
	}
}g;
*/
struct graph{
	int n,m;
	struct edge{
		int to,cap,tot,rev;
	}e[maxm];
	struct e0{
		int from,to,cap;
	}w[maxm];
	void init(int n){
		this->n=n;
	}
	int first[maxn],now[maxn];
	void addedge(int from,int to,int cap){
		w[++m]=(e0){from,to,cap};
		first[from]++;
		w[++m]=(e0){to,from,0};
		first[to]++;
	}
	void pre(){
		for(int i=1;i<=n;i++)
			first[i]+=first[i-1],now[i]=first[i];
		first[n+1]=m+1;
		for(int i=1;i<=m;i++){
			e[now[w[i].from]--]=(edge){w[i].to,w[i].cap,0,0};
			if (i%2==0){
				int x=now[w[i].from]+1,y=now[w[i].to]+1;
				e[x].rev=y;
				e[y].rev=x;
			}
		}
		// for(int i=1;i<=n;i++)
			// printf("first[%d]=%d\n",i,first[i]);
		for(int i=1;i<=n;i++)
			first[i]=now[i]+1;
	}
	int q[maxn];
	int d[maxn];
	int t;
	bool bfs(int s){
		int l=1,r=1;
		q[1]=s;
		memset(d,0x3f,sizeof(d));
		d[s]=0;
		while(l<=r){
			int u=q[l++];
			for(int i=first[u];i<first[u+1];i++){
				int v=e[i].to;
				// printf("from %d to %d\n",u,v);
				if (d[v]!=INF || !e[i].cap)
					continue;
				d[v]=d[u]+1;
				if (v==t)
					return true;
				q[++r]=v;
			}
		}
		return false;
	}
	int dfs(int u,int flow){
		if (u==t)
			return flow;
		for(int &i=now[u];i<first[u+1];i++){
			int v=e[i].to;
			if (d[v]!=d[u]+1 || !e[i].cap)
				continue;
			int res=dfs(v,min(e[i].cap,flow));
			if (res){
				e[i].cap-=res;
				e[e[i].rev].tot+=res;
				return res;
			}
		}
		return 0;
	}
	int dinic(int s,int t){
		this->t=t;
		int ans=0;
		while(bfs(s)){
			do{
				int flow;
				for(int i=1;i<=n;i++)
					now[i]=first[i];
				while(flow=dfs(s,INF))
					ans+=flow;
			}while(bfs(s));
			for(int i=1;i<=m;i++)
				e[i].cap+=e[i].tot,e[i].tot=0;
		}
		return ans;
	}
}g;
int a[maxn];
bool ntprime[200003];
int main(){
	init();
	ntprime[1]=1;
	for(int i=2;i<=200000;i++)
		if (!ntprime[i])
			for(int j=i*2;j<=200000;j+=i)
				ntprime[j]=1;
	int n=readint();
	for(int i=1;i<=n;i++)
		a[i]=readint();
	sort(a+1,a+n+1);
	reverse(a+1,a+n+1);
	while(n>1 && a[n]==1 && a[n-1]==1)
		n--;
	reverse(a+1,a+n+1);
	// printf("%d\n",n);
	int s=n+1,t=n+2;
	g.init(t);
	for(int i=1;i<=n;i++)
		if (a[i]%2)
			g.addedge(s,i,1);
		else g.addedge(i,t,1);
	for(int i=1;i<=n;i++){
		if (a[i]%2==0)
			continue;
		for(int j=1;j<=n;j++)
			if (j!=i && !ntprime[a[i]+a[j]])
				g.addedge(i,j,1);
	}
	// fprintf(stderr,"WTF\n");
	g.pre();
	printf("%d\n",n-g.dinic(s,t));
	// printf("%d",g.m);
	// fprintf(stderr,"%d\n",clock());
	// fprintf(stderr,"%d",sizeof(g));
}