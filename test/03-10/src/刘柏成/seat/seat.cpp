#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("seat.in","r",stdin);
	freopen("seat.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
const int maxn=100002;
struct segtree{
	int n,cur;
	struct node{
		int sum,suf,pre,mx,ls,rs;
	}t[maxn*66];
	#define ls t[o].ls
	#define rs t[o].rs
	int newnode(int l,int r){
		cur++;
		t[cur].suf=t[cur].pre=t[cur].mx=r-l+1;
		return cur;
	}
	int rt;
	void init(int n){
		this->n=n;
		rt=newnode(1,n);
	}
	void maintain(int o,int l,int r){
		int mid=(l+r)/2;
		t[o].sum=t[ls].sum+t[rs].sum;
		if (!t[o].sum){
			t[o].suf=t[o].pre=t[o].mx=r-l+1;
			return;
		}
		if (!ls)
			ls=newnode(l,mid);
		if (!rs)
			rs=newnode(mid+1,r);
		t[o].pre=t[ls].pre;
		if (!t[ls].sum)
			t[o].pre+=t[rs].pre;
		t[o].suf=t[rs].suf;
		if (!t[rs].sum)
			t[o].suf+=t[ls].suf;
		t[o].mx=max(t[ls].suf+t[rs].pre,max(t[ls].mx,t[rs].mx));
	}
	int p,v;
	void update(int& o,int l,int r){
		if (!o)
			o=++cur;
		if (l==r){
			t[o].sum=v;
			t[o].suf=t[o].pre=t[o].mx=!v;
			return;
		}
		int mid=(l+r)/2;
		if (p<=mid)
			update(ls,l,mid);
		else update(rs,mid+1,r);
		maintain(o,l,r);
	}
	void update(int p,int v){
		this->p=p;
		this->v=v;
		// printf("update %d %d\n",p,v);
		update(rt,1,n);
	}
	int query_suf(int o,int l,int r){
		if (!t[o].suf)
			return r+1;
		if (!t[o].sum || l==r)
			return l;
		int mid=(l+r)/2;
		if (!t[rs].sum)
			return query_suf(ls,l,mid);
		else return query_suf(rs,mid+1,r);
	}
	int query_pre(int o,int l,int r){
		if (!t[o].pre)
			return l-1;
		if (!t[o].sum || l==r)
			return r;
		int mid=(l+r)/2;
		if (!t[ls].sum)
			return query_pre(rs,mid+1,r);
		else return query_pre(ls,l,mid);
	}
	pair<int,int> query(int o,int l,int r){
		if (!t[o].sum || l==r)
			return make_pair(l,r);
		int mid=(l+r)/2;
		if (t[o].mx==t[rs].mx)
			return query(rs,mid+1,r);
		if (t[o].mx==t[ls].suf+t[rs].pre && t[rs].pre)
			return make_pair(query_suf(ls,l,mid),query_pre(rs,mid+1,r));
		return query(ls,l,mid);
	}
	pair<int,int> query(){
		return query(1,1,n);
	}
	int ql,qr;
	int getsum(int o,int l,int r){
		if (!t[o].sum)
			return 0;
		if (ql<=l && qr>=r)
			return t[o].sum;
		int mid=(l+r)/2,ans=0;
		if (ql<=mid)
			ans+=getsum(ls,l,mid);
		if (qr>mid)
			ans+=getsum(rs,mid+1,r);
		return ans;
	}
	int query(int l,int r){
		ql=l,qr=r;
		return getsum(1,1,n);
	}
	#undef ls
	#undef rs
}t;
struct query{
	int op,l,r;
}q[maxn];
int nums[maxn],cur;
int p[maxn];
int main(){
	init();
	int n=readint(),m=readint();
	for(int i=1;i<=m;i++){
		q[i].op=readint();
		if (!q[i].op)
			q[i].l=readint(),q[i].r=readint();
		else nums[++cur]=q[i].op;
	}
	sort(nums+1,nums+cur+1);
	cur=unique(nums+1,nums+cur+1)-nums-1;
	t.init(n);
	for(int i=1;i<=m;i++){
		if (q[i].op){
			q[i].op=lower_bound(nums+1,nums+cur+1,q[i].op)-nums;
			if (p[q[i].op])
				t.update(p[q[i].op],0),p[q[i].op]=0;
			else{
				pair<int,int> qwq=t.query();
				// printf("%d %d\n",qwq.first,qwq.second);
				int mid=(qwq.first+qwq.second+1)/2;
				t.update(p[q[i].op]=mid,1);
			}
		}else printf("%d\n",t.query(q[i].l,q[i].r));
	}
}