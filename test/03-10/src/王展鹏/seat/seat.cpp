#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<set>
#include<map>
#include<queue>
#include<bitset>
#include<ctime>
using namespace std;
typedef long long ll;
#define sqr(x) ((x)*(x))
#define mp make_pair
#define uint unsigned
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(int a){
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(int a){
    if(a<0){
    	a=-a; putchar('-');
	}
	write(a); puts("");
}
const int M=10000005;
struct data{
	int lsum,rsum,sum;
	pair<int,int> po;
}tree[M];
int lson[M],rson[M];
int nodecnt,n,q;
map<int,int> Ma;
void insert(int l,int r,int pos,int add,int &nod){
	if(!nod)nod=++nodecnt;
	if(l==r){
		tree[nod].sum+=add; tree[nod].lsum=tree[nod].rsum=tree[nod].sum^1; return;
	}
	int mid=(l+r)>>1;
	if(pos<=mid)insert(l,mid,pos,add,lson[nod]); else insert(mid+1,r,pos,add,rson[nod]);
 	if(!lson[nod]&&!rson[nod]){
		tree[nod].rsum=tree[nod].lsum=r-l+1;
		tree[nod].po=mp(mid+((r-l)&1),r-l+1);
	}else if(!lson[nod]){
		tree[nod].lsum=mid-l+1+tree[rson[nod]].lsum;
		tree[nod].rsum=tree[rson[nod]].rsum==r-mid?r-l+1:tree[rson[nod]].rsum;
		tree[nod].po=max(tree[rson[nod]].po,mp(tree[nod].lsum,l+(tree[nod].lsum>>1)));
	}else if(!rson[nod]){
		tree[nod].rsum=tree[lson[nod]].rsum+r-mid;
		tree[nod].lsum=tree[lson[nod]].lsum==mid-l+1?r-l+1:tree[lson[nod]].lsum;
		tree[nod].po=max(tree[lson[nod]].po,mp(tree[nod].rsum,r-tree[nod].rsum+1+(tree[nod].rsum>>1)));
	}else{
		tree[nod].lsum=tree[lson[nod]].lsum==mid-l+1?tree[lson[nod]].lsum+tree[rson[nod]].lsum:tree[lson[nod]].lsum;
		tree[nod].rsum=tree[rson[nod]].rsum==r-mid?tree[rson[nod]].rsum+tree[lson[nod]].rsum:tree[rson[nod]].rsum;
		tree[nod].po=max(tree[lson[nod]].po,tree[rson[nod]].po);
		tree[nod].po=max(tree[nod].po,mp(tree[lson[nod]].rsum+tree[rson[nod]].lsum,mid-tree[lson[nod]].rsum+1+((tree[lson[nod]].rsum+tree[rson[nod]].lsum)>>1)));
	}
	tree[nod].sum=tree[lson[nod]].sum+tree[rson[nod]].sum;
	//cout<<l<<" "<<r<<" "<<lson[nod]<<" "<<rson[nod]<<" "<<tree[nod].po.first<<" "<<tree[nod].po.second<<" "<<endl;
}
int ask(int l,int r,int i,int j,int nod){
	if(!nod)return 0;
	if(l==i&&r==j)return tree[nod].sum;
	int mid=(l+r)>>1;
	if(j<=mid)return ask(l,mid,i,j,lson[nod]); else if(i>mid)return ask(mid+1,r,i,j,rson[nod]);
	else{
		return ask(l,mid,i,mid,lson[nod])+ask(mid+1,r,mid+1,j,rson[nod]);
	}
}
int rt;
int main(){
	freopen("seat.in","r",stdin); freopen("seat.out","w",stdout);
	n=read(); q=read();
	tree[nodecnt=rt=1].lsum=tree[1].rsum=n; tree[1].po=mp(n,(n+2)>>1);
	for(int i=1;i<=q;i++){
		int op=read();
		if(op){
			if(Ma.count(op)){
				insert(1,n,Ma[op],-1,rt); Ma.erase(op);
			}else{
				int pos=tree[rt].po.second;
				//cout<<pos<<" aighsfuiabhg\n";
				insert(1,n,Ma[op]=pos,1,rt);
			}
		}else{
			int l=read(),r=read();
			writeln(ask(1,n,l,r,rt));
		}
	}
}
//ldbcAK��
