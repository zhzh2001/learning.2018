#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<set>
#include<map>
#include<queue>
using namespace std;
typedef long long LL;
#define sqr(x) ((x)*(x))
#define mp make_pair
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(int a){
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(int a){
    if(a<0){
    	a=-a; putchar('-');
	}
	write(a); puts("");
}
const int p=200005,N=3005;
bool heshu[p]; int n,T,k,r,que[p],m;
int a[N],q[N],ans,tong[p];
bool e[N][N];
int main(){
	freopen("prime.in","r",stdin); freopen("prime.out","w",stdout);
	for(int i=2;i<p;i++){
	    if(!heshu[i]){que[++r]=i;}
        for(int j=1;j<=r&&i*que[j]<p;j++){
            heshu[k=i*que[j]]=1;
            if (i%que[j]==0){break;}
        }
    }
    n=read();
    for(int i=1;i<=n;i++)a[i]=read();
    for(int i=1;i<=n;i++){
    	for(int j=1;j<=n;j++)if(heshu[a[i]+a[j]])e[i][j]=1;
	}
	for(int i=1;i<=n;i++)q[i]=i;
	for(int sum=0;sum<=40000000;){
		random_shuffle(&q[1],&q[n+1]); int tot=0;
		for(int i=1;i<=n;i++){
			int flag=0;
			for(int j=1;j<=tot;j++)if(!e[que[j]][q[i]]){
				sum+=j; flag=1; break;
			}
			if(!flag){
				que[++tot]=q[i]; sum+=tot;
			}
		}
		ans=max(ans,tot);
	}
	for(int i=1;i<=n;i++)tong[a[i]]++;
	for(int i=2;i<p;i++)if(i==2||i%2==1){
		int sum=0;
		for(int j=i;j<p;j+=i){
			sum+=tong[j];
		}
		ans=max(ans,sum);
	}
	int sum=0;
	for(int i=1;i<=n;i++)if(a[i]&1)sum++;
	ans=max(ans,sum);
	cout<<ans<<endl;
}
//��ldbc����
