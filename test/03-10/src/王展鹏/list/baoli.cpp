#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<set>
#include<map>
#include<queue>
#include<bitset>
#include<ctime>
using namespace std;
typedef long long ll;
#define sqr(x) ((x)*(x))
#define mp make_pair
#define uint unsigned
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(int a){
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(int a){
    if(a<0){
    	a=-a; putchar('-');
	}
	write(a); puts("");
}
const int N=100005;
int n,a[N],dp[N],b[N],ans;
void dfs(int p){
	if(p>n){
		int sum=0;
		for(int i=n;i;i--){
			int t=0;
			for(int j=i+1;j<=n;j++)if(b[j]<b[i])t=max(t,dp[j]);
			dp[i]=++t;
			sum+=dp[i];
		}
		ans=max(ans,sum); return;
	}
	for(int i=1;i<=n;i++){
		int t=0;
		for(int j=1;j<p;j++)if(b[j]<i)t=max(t,a[j]);
		if(++t!=a[p])continue;
		b[p]=i; dfs(p+1);
	}
}
int main(){
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	dfs(1);
	writeln(ans);
}

