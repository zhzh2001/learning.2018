#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<set>
#include<map>
#include<queue>
#include<bitset>
#include<ctime>
using namespace std;
typedef long long ll;
#define sqr(x) ((x)*(x))
#define mp make_pair
#define uint unsigned
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(int a){
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(int a){
    if(a<0){
    	a=-a; putchar('-');
	}
	write(a); puts("");
}
const int N=1005;
int n,a[N],dp[N];
ll ans;
bitset<N> e[N];
int main(){
	freopen("list.in","r",stdin);
	n=read();
	for(int i=1;i<=n;i++){
		a[i]=read();
		for(int j=1;j<i;j++)if(a[i]<=a[j])e[j][i]=1;
		for(int j=i-1;j;j--)if(a[j]+1==a[i]){
			e[i][j]=1; break;
		}
	}
	for(int k=1;k<=n;k++){
		for(int i=1;i<=n;i++)if(e[i][k]){
			e[i]|=e[k];
		}
	}
	for(int i=n;i;i--){
		for(int j=i+1;j<=n;j++)if(!e[j][i])dp[i]=max(dp[i],dp[j]);
		ans+=++dp[i];
	}
	cout<<ans<<endl;
}

