#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<set>
#include<map>
#include<queue>
#include<bitset>
#include<ctime>
using namespace std;
typedef long long ll;
#define sqr(x) ((x)*(x))
#define mp make_pair
#define uint unsigned
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(int a){
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(int a){
    if(a<0){
    	a=-a; putchar('-');
	}
	write(a); puts("");
}
const int N=100005;
int n,a[N],c[N];
ll ans;
#define lowbit(i) i&-i
inline void change(int pos,int de){
	for(int i=pos;i<=n;i+=lowbit(i))c[i]=max(c[i],de);
}
inline int getsum(int pos){
	int ans=0;
	for(int i=pos;i;i-=lowbit(i))ans=max(ans,c[i]);
	return ans;
}
int main(){
	//freopen("list.in","r",stdin);
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=n;i;i--){
		int t=getsum(a[i]);
		ans+=++t;
		change(a[i],t);
	}
	cout<<ans<<endl;
}
