#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<set>
#include<map>
#include<queue>
#include<bitset>
#include<ctime>
using namespace std;
typedef long long ll;
#define sqr(x) ((x)*(x))
#define mp make_pair
#define uint unsigned
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(int a){
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(int a){
    if(a<0){
    	a=-a; putchar('-');
	}
	write(a); puts("");
}
const int N=100005,B=400;
int n,a[N],dp[N],c[N],b[N],tree[N<<2],belong[N];
int ycl1[B][N],ycl2[B][N],L[N],R[N];
ll ans;
#define lowbit(i) i&-i
inline void change(int pos,int de){
	for(int i=pos;i<=n;i+=lowbit(i))c[i]=max(c[i],de);
}
inline int getsum(int pos){
	int ans=0;
	for(int i=pos;i;i-=lowbit(i))ans=max(ans,c[i]);
	return ans;
}
void update(int l,int r,int pos,int nod){
	tree[nod]--;
	if(l==r)return;
	int mid=(l+r)>>1;
	if(pos<=mid)update(l,mid,pos,nod<<1); else update(mid+1,r,pos,nod<<1|1);
}
int ask(int l,int r,int k,int nod){
	if(l==r)return l;
	int mid=(l+r)>>1;
	if(tree[nod<<1|1]>=k)return ask(mid+1,r,k,nod<<1|1);
	else return ask(l,mid,k-tree[nod<<1|1],nod<<1);
}
void build(int l,int r,int nod){
	tree[nod]=r-l+1;
	if(l==r)return;
	int mid=(l+r)>>1;
	build(l,mid,nod<<1); build(mid+1,r,nod<<1|1);
}
int main(){
	freopen("list.in","r",stdin); freopen("list.out","w",stdout);
	n=read(); int block=sqrt(n);
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<=n;i++){
		if(i%block==1){
			belong[i]=belong[i-1]+1; L[belong[i]]=i;
		}else belong[i]=belong[i-1];
		R[belong[i]]=i;
	}
	build(1,n,1);
	for(int i=1;i<=belong[n];i++){
		for(int j=L[i];j<=R[i];j++){
			b[j-L[i]+1]=a[j];
			if(ycl2[i][a[j]])continue;
			int dd=a[j];
			for(int k=L[i];k<=R[i];k++){
				if(a[k]>dd)ycl1[i][a[j]]++;
				if(dd==a[k])dd++;
			}
			ycl2[i][a[j]]=dd;
		}
		sort(&b[1],&b[R[i]-L[i]+2]);
		for(int k=1;k<=R[i]-L[i]+1;k++){
			for(int j=b[k-1]+1;j<b[k];j++){
				ycl2[i][j]=j; ycl1[i][j]=R[i]-L[i]+2-k;
			}
		}
		for(int j=b[R[i]-L[i]+1]+1;j<=n;j++)ycl2[i][j]=j;
	}
	for(int i=1;i<=n;i++){
		int dd=a[i],sum=0;
		for(int j=i+1;j<=R[belong[i]];j++){
			if(a[j]>dd)sum++;
			if(dd==a[j])dd++;
		}
		for(int j=belong[i]+1;j<=belong[n];j++){
			//if(i==2)cout<<L[j]<<" "<<R[j]<<" "<<ycl1[j][dd]<<" "<<ycl2[j][dd]<<endl;
			sum+=ycl1[j][dd];
			dd=ycl2[j][dd];
		}
		b[i]=ask(1,n,sum+1,1);
		update(1,n,b[i],1);
	}
	for(int i=n;i;i--){
		int t=getsum(b[i])+1;
		change(b[i],t);
		ans+=t;
	}
	cout<<ans<<endl;
}

