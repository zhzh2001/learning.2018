#include <bits/stdc++.h>

using namespace std;

namespace program
{
	typedef long long big;
	
	const int MAXN = 100000, MAXC = 4000000, INF = 0x3f3f3f3f;
	priority_queue<int> Q;
	int n, cnt = 0, A[MAXN + 10], Deg[MAXN + MAXC + 10], Pre[MAXN + 10], G[MAXN + 10];
	struct node { int lt, rt; } T[MAXC + 10];
	struct edge
	{
		int v;
		edge *next;
		edge(int _v, edge *_next) : v(_v), next(_next) { }
	} *Head[MAXN + MAXC + 10];
	
	inline void add_edge(int u, int v)
	{ Head[u] = new edge(v, Head[u]); Deg[v]++; }
	
	void update(int &p, int a, int b, int pos, int val)
	{
		T[++cnt] = T[p];
		if(p)
			add_edge(n + cnt - 1, n + p - 1);
		p = cnt;
		if(a < b)
		{
			int mid = (a + b) >> 1;
			if(pos <= mid)
			{
				update(T[p].lt, a, mid, pos, val);
				add_edge(n + p - 1, n + T[p].lt - 1);
			}
			else
			{
				update(T[p].rt, mid + 1, b, pos, val);
				add_edge(n + p - 1, n + T[p].rt - 1);
			}
		}
		else
			add_edge(n + p - 1, val);
	}
	
	void query(int p, int a, int b, int x, int y, int val)
	{
		if(p && x <= b && a <= y)
		{
			if(x <= a && b <= y)
				add_edge(val, n + p - 1);
			else
			{
				int mid = (a + b) >> 1;
				query(T[p].lt, a, mid, x, y, val);
				query(T[p].rt, mid + 1, b, x, y, val);
			}
		}
	}
	
	void work()
	{
		int t = 0, root = 0;
		big tot = 0;
		scanf("%d", &n);
		for(int i = 0; i < n; i++)
		{
			scanf("%d", &A[i]);
			query(root, 1, n, A[i], n, i);
			update(root, 1, n, A[i], i);
//			for(int j = 0; j < i; j++)
//				if(A[j] >= A[i])
//					add_edge(i, j);
			if(A[i] > 1)
				add_edge(Pre[A[i] - 1], i);
			Pre[A[i]] = i;
		}
		for(int i = 0; i < n + cnt; i++)
			if(!Deg[i])
				Q.push(i);
		while(!Q.empty())
		{
			int u = Q.top();
			if(u < n)
				A[u] = t++;
			Q.pop();
			for(edge *p = Head[u]; p; p = p->next)
				if(!--Deg[p->v])
					Q.push(p->v);
		}
		memset(G + 1, INF, sizeof(int) * n);
		for(int i = n - 1; i >= 0; i--)
		{
			int *p = lower_bound(G + 1, G + n + 1, A[i]);
			tot += p - G;
			*p = A[i];
		}
		printf("%lld\n", tot);
	}
}

int main()
{
	freopen("list.in", "r", stdin);
	freopen("list.out", "w", stdout);
	program::work();
	return 0;
}
