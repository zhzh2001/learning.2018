#include <bits/stdc++.h>

using namespace std;

namespace program
{
	#define S (0)
	#define T (n + 1)
	#define V (n + 2)
	
	const int MAXN = 3000, MAXS = 200000, INF = 0x3f3f3f3f;
	bool IsPrime[MAXS + 10];
	int n, ec = 0, A[MAXN + 10], Head[MAXN + 10], Cur[MAXN + 10];
	int Q[MAXN + 10], Dis[MAXN + 10];
	struct edge { int v, next, f; } E[(MAXN * MAXN) + 10];
	
	void add_edge(int u, int v)
	{
		E[ec] = (edge){v, Head[u], 1};
		Head[u] = ec++;
		E[ec] = (edge){u, Head[v], 0};
		Head[v] = ec++;
	}
	
	bool bfs()
	{
		int *head = Q, *tail = Q;
		memset(Dis, -1, sizeof(int) * V);
		Dis[*tail++ = T] = 0;
		while(head < tail)
		{
			int u = *head++;
			for(int p = Head[u]; p != -1; p = E[p].next)
				if(E[p ^ 1].f && Dis[E[p].v] == -1)
					Dis[*tail++ = E[p].v] = Dis[u] + 1;
		}
		return Dis[S] != -1;
	}
	
	int dfs(int u, int f)
	{
		if(u == T)
			return f;
		else
		{
			int tot = 0;
			for(int &p = Cur[u]; p != -1; p = E[p].next)
				if(E[p].f && Dis[u] == Dis[E[p].v] + 1)
				{
					int t = dfs(E[p].v, min(f - tot, E[p].f));
					E[p].f -= t;
					E[p ^ 1].f += t;
					tot += t;
					if(tot >= f)
						break;
				}
			return tot;
		}
	}
	
	int dinic()
	{
		int tot = 0;
		while(bfs())
		{
			memcpy(Cur, Head, sizeof(int) * V);
			tot += dfs(S, INF);
		}
		return tot;
	}
	
	void work()
	{
		int f = 0;
		memset(IsPrime, 1, sizeof(IsPrime));
		for(int i = 2; i <= MAXS; i++)
			if(IsPrime[i])
				for(int j = i << 1; j <= MAXS; j += i)
					IsPrime[j] = 0;
		scanf("%d", &n);
		memset(Head, -1, sizeof(int) * V);
		for(int i = 1; i <= n; i++)
		{
			scanf("%d", &A[i]);
			if(A[i] == 1 && f++)
			{
				i--;
				n--;
			}
			else
			{
				if(A[i] & 1)
					add_edge(S, i);
				else
					add_edge(i, T);
			}
		}
		for(int i = 1; i <= n; i++)
			if(A[i] & 1)
				for(int j = 1; j <= n; j++)
					if(!(A[j] & 1) && IsPrime[A[i] + A[j]])
						add_edge(i, j);
		printf("%d\n", n - dinic());
	}
	
	#undef S
	#undef T
	#undef V
}

int main()
{
	freopen("prime.in", "r", stdin);
	freopen("prime.out", "w", stdout);
	program::work();
	return 0;
}
