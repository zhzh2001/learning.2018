#include <bits/stdc++.h>

using namespace std;

namespace program
{
	struct range
	{
		int a, b;
		range(int _a, int _b) : a(_a), b(_b) { }
		int len() const { return b - a + 1; }
		int mid() const { return (a + b + 1) >> 1; }
		friend bool operator <(const range &x, const range &y)
		{ return x.len() != y.len() ? x.len() < y.len() : x.b < y.b; }
	};
	
	struct data_t
	{
		int sum, lsum, rsum;
		range maxsum;
		data_t(int a, int b) : sum(b - a + 1), lsum(sum), rsum(sum), maxsum(a, b) { }
		data_t(int _sum, int _lsum, int _rsum, const range &_maxsum) :
			sum(_sum), lsum(_lsum), rsum(_rsum), maxsum(_maxsum) { }
		friend data_t merge(const data_t &x, const data_t &y, int a, int mid, int b)
		{ return data_t(x.sum + y.sum, x.lsum == mid - a + 1 ? x.lsum + y.lsum : x.lsum,
			y.rsum == b - mid ? y.rsum + x.rsum : y.rsum,
			max(range(mid - x.rsum + 1, mid + y.lsum), max(x.maxsum, y.maxsum))); }
	};
	
	namespace seg_tree
	{
		struct node
		{
			data_t data;
			node *lt, *rt;
			node(int a, int b) : data(a, b), lt(NULL), rt(NULL) { }
			void maintain(int a, int b) { int mid = (a + b) >> 1;
				data = merge(lt ? lt->data : data_t(a, mid),
				rt ? rt->data : data_t(mid + 1, b), a, mid, b); }
			void update(int pos, int a, int b);
			int query(int x, int y, int a, int b);
		};
		
		void node::update(int pos, int a, int b)
		{
			if(a < b)
			{
				int mid = (a + b) >> 1;
				if(pos <= mid)
				{
					if(!lt)
						lt = new node(a, mid);
					lt->update(pos, a, mid);
				}
				else
				{
					if(!rt)
						rt = new node(mid + 1, b);
					rt->update(pos, mid + 1, b);
				}
				maintain(a, b);
			}
			else
				data = data_t(b + data.sum, b);
		}
		
		int node::query(int x, int y, int a, int b)
		{
			if(this && x <= b && a <= y)
			{
				if(x <= a && b <= y)
					return b - a + 1 - data.sum;
				else
				{
					int mid = (a + b) >> 1;
					return lt->query(x, y, a, mid) + rt->query(x, y, mid + 1, b);
				}
			}
			else
				return 0;
		}
	}
	
	int read()
	{
		int s = 0, ch;
		while(!isdigit(ch = getchar()));
		do
			s = s * 10 + ch - '0';
		while(isdigit(ch = getchar()));
		return s;
	}
	
	map<int, int> Pos;
	
	void work()
	{
		int n = read(), q = read();
		seg_tree::node *seg = new seg_tree::node(1, n);
		while(q--)
		{
			int t;
			scanf("%d", &t);
			if(t)
			{
				if(Pos.count(t))
				{
					seg->update(Pos[t], 1, n);
					Pos.erase(t);
				}
				else
					seg->update(Pos[t] = seg->data.maxsum.mid(), 1, n);
			}
			else
			{
				int a, b;
				scanf("%d%d", &a, &b);
				printf("%d\n", seg->query(a, b, 1, n));
			}
		}
	}
}

int main()
{
	freopen("seat.in", "r", stdin);
	freopen("seat.out", "w", stdout);
	program::work();
	return 0;
}
