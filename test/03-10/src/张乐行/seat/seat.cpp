#include<cstdio>
#include<map>
#include<algorithm>
#define LS ls[p],l,m
#define RS rs[p],m+1,r
using namespace std;
const int P=4e6;
int n,Q,x,y,k;
map<int,int> mp;
int read(){
	char c=getchar();int k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
void write(int x){if (x>9) write(x/10);putchar(x%10+48);}
int nd,rt,ls[P],rs[P],sum[P],lmx[P],rmx[P],fl[P],fr[P];
void up(int p,int l,int m,int r){
	int ld=ls[p],rd=rs[p],lz=m-l+1,rz=r-m;
	if (!ld) lmx[0]=rmx[0]=lz,fl[0]=l,fr[0]=m;
	if (!rd) lmx[0]=rmx[0]=rz,fl[0]=m+1,fr[0]=r;
	sum[p]=sum[ld]+sum[rd];
	lmx[p]=lmx[ld]==lz?lz+lmx[rd]:lmx[ld];
	rmx[p]=rmx[rd]==rz?rz+rmx[ld]:rmx[rd];
	fl[p]=fl[rd];fr[p]=fr[rd];
	if (rmx[ld]+lmx[rd]>fr[p]-fl[p]+1)
		fl[p]=m-rmx[ld]+1,fr[p]=m+lmx[rd];
	if (fr[ld]-fl[ld]>fr[p]-fl[p])
		fl[p]=fl[ld],fr[p]=fr[ld];
}
void change(int &p,int l,int r){
	if (!p) p=++nd;
	if (l==r){
		if (k) sum[p]=1,lmx[p]=rmx[p]=0,fl[p]=0,fr[p]=-1;
		else   sum[p]=0,lmx[p]=rmx[p]=1,fl[p]=fr[p]=l;
		return;
	}
	int m=l+r>>1;
	if (x<=m) change(LS);else change(RS);
	up(p,l,m,r);
}
int ask(int p,int l,int r){
	if (!sum[p]) return 0;
	if (x<=l&&r<=y) return sum[p];
	int m=l+r>>1;
	if (y<=m) return ask(LS);
	if (x>m) return ask(RS);
	return ask(LS)+ask(RS);
}
void change_it(int p){
	int px=mp[p];
	if (!px){
		int l=fl[1],r=fr[1];
		x=l+r+1>>1;k=1;mp[p]=x;
	}
	else{
		x=px;k=0;mp[p]=0;
	}
	change(rt,1,n);
}
void query_it(){
	x=read();y=read();
	write(ask(1,1,n));putchar('\n');
}
int main(){
	freopen("seat.in","r",stdin);
	freopen("seat.out","w",stdout);
	n=read();Q=read();rt=nd=1;fl[1]=1;fr[1]=n;
	for (;Q--;){
		int x=read();
		if (x==0) query_it();
		else change_it(x);
	}
}
