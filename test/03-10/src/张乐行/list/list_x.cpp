#include<cstdio>
#include<algorithm>
#define ll long long
using namespace std;
const int N=1e5+5;
int n,i,a[N],p[N],f[N],mx[N],g[N];
ll ans;
int read(){
	char c=getchar();int k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
void write(ll x){if (x>9) write(x/10);putchar(x%10+48);}
void add(int x,int k){for (;x<=n&&g[x]<k;x+=x&-x) g[x]=k;}
int ask(int x){int k=0;for (;x;x-=x&-x) k=max(k,g[x]);return k;}
int main(){
	freopen("list.in","r",stdin);
	freopen("list.out","w",stdout);
	for (n=read(),i=1;i<=n;i++) a[i]=read();
	for (p[0]=n+1,i=n;i;i--){
		f[i]=mx[p[a[i]]];p[a[i]]=i;
		f[i]=max(f[i],ask(a[i]))+1;
		ans+=f[i];
		mx[i]=max(f[i],mx[i+1]);add(a[i],f[i]);
	}
	write(ans);
}
