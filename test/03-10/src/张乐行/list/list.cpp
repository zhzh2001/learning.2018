#include<cstdio>
#include<algorithm>
#define ll long long
using namespace std;
const int N=1e5+5;
int n,i,j,a[N],f[N],g[N],c[N];
ll ans;
int read(){
	char c=getchar();int k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
void write(ll x){if (x>9) write(x/10);putchar(x%10+48);}
void add(int x,int k){for (;x<=n&&c[x]<k;x+=x&-x) c[x]=k;}
int ask(int x){int k=0;for (;x;x-=x&-x) k=max(k,c[x]);return k;}
int main(){
	freopen("list.in","r",stdin);
	freopen("list.out","w",stdout);
	for (n=read(),i=1;i<=n;i++) a[i]=read();
	for (i=n;i;i--){
		int k=ask(a[i]);
		f[i]=max(k,g[a[i]])+1;k=max(k,f[i]);
		g[a[i]]=max(k,g[a[i]+1]);
		add(a[i],f[i]);ans+=f[i];
	}
	write(ans);
}
