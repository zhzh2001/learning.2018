#include<cstdio>
#include<algorithm>
#define ll long long
using namespace std;
const int N=1e5+5;
int n,i,j,a[N],p[N],hv[N],f[N],mx[N];
ll ans;
int read(){
	char c=getchar();int k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
void write(ll x){if (x>9) write(x/10);putchar(x%10+48);}
int main(){
	freopen("list.in","r",stdin);
	freopen("bl.out","w",stdout);
	for (n=read(),i=1;i<=n;i++) a[i]=read();
	for (i=1;i<=n;i++) p[i]=n+1;
	for (i=n;i;i--){
		for (j=0;j<a[i];j++) hv[j]=i;
		for (j=i+1;j<=n;j++)
			if (hv[a[j]-1]==i)
				f[i]=max(f[i],f[j]),hv[a[j]]=i;
		p[a[i]]=i;f[i]++;
		ans+=f[i];
		mx[i]=max(f[i],mx[i+1]);
	}
	printf("%lld",ans);
}
