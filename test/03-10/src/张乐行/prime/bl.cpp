#include<cstdio>
#include<cstdlib>
#include<algorithm>
using namespace std;
const int N=3005,M=2e5+5,LIM=3e8;
int n,m,i,j,a[N],f[M],dn,d[N],to[N],cnt,ans;
int read(){
	char c=getchar();int k=0;for (;c<48||c>57;c=getchar());
	for (;c>47&&c<58;c=getchar()) k=(k<<3)+(k<<1)+c-48;return k;
}
bool chk(int k){
	for (int i=1;i<=dn;i++) if (!f[d[i]+k]) return 0;
	return 1;
}
void dfs(int x,int k){
	cnt++;
	if (cnt>LIM){printf("%d",ans);exit(0);}
	if (n-x+1+k<=ans) return;
	if (x>n){ans=k;return;}
	bool flag=1;
	for (int i=1;i<=k;i++)
		if (!f[a[x]+d[i]]){flag=0;break;}
	cnt+=k;
	if (cnt>LIM){printf("%d",ans);exit(0);}
	if (flag) d[k+1]=a[x],dfs(x+1,k+1);
	dfs(to[x],k);
}
int main(){
	freopen("prime.in","r",stdin);
	freopen("bl.out","w",stdout);
	bool flag=0;
	for (int num=read();num--;){
		int x=read();if (x>m) m=x;
		if (x==1){
			if (!flag) a[++n]=1,flag=1;
		}
		else a[++n]=x;
	}
	sort(a+1,a+n+1);
	for (i=n;i;i--) to[i]=a[i]==a[i+1]?to[i+1]:i+1;
	for (m*=2,i=2;i<=m;i++) if (!f[i])
		for (j=i+i;j<=m;j+=i) f[j]=1;
	for (i=1;i<=n;i++) if (a[i]&1) d[++dn]=a[i];
	for (i=1;i<=n;i++)
		if ((!a[i]&1)&&chk(a[i])) d[++dn]=a[i];
	ans=dn;dn=0;
	for (i=1;i<=n;i++) if (!(a[i]&1)) d[++dn]=a[i];
	for (i=1;i<=n;i++)
		if ((a[i]&1)&&chk(a[i])) d[++dn]=a[i];
	if (dn>ans) ans=dn;
//	dfs(1,0);
	printf("%d",ans);
}
