#include<cstdio>
#include<algorithm>
using namespace std;
const int N=3005,M=2e5+5;
int n,m,i,j,a[N],to[N],d[N],f[M],vx,vy,ans;
void dfs(int x,int k){
	if (n-x+1+k<=ans) return;
	if (x>n){ans=k;return;}
	bool flag=1;
	for (int i=1;i<=k;i++)
		if (!f[a[x]+d[i]]){flag=0;break;}
	if (flag) d[k+1]=a[x],dfs(x+1,k+1);
	dfs(to[x],k);
}
int main(){
	freopen("prime.in","r",stdin);
	freopen("bl_.out","w",stdout);
	for (scanf("%d",&n),i=1;i<=n;i++)
		scanf("%d",&a[i]);
	sort(a+1,a+n+1);m=a[n]*2;
	for (i=2;i<=m;i++) if (!f[i])
		for (j=i+i;j<=m;j+=i) f[j]=1;
	for (i=1;i<=n;i++){
		if ((a[i]&1)&&a[i]!=1) vx++;
		if (!(a[i]&1)) vy++;
	}
	ans=max(vx,vy);
	for (i=n;i;i--) to[i]=a[i]==a[i+1]?to[i+1]:i+1;
	dfs(1,0);
	printf("%d",ans);
}
