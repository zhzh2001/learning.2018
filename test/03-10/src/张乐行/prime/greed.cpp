#include<cstdio>
#include<ctime>
#include<algorithm>
using namespace std;
const int N=2005,M=2e5+5;
int n,m,i,j,a[N],f[M],dn,d[N],ans;
int main(){
	freopen("prime.in","r",stdin);
	freopen("greed.out","w",stdout);
	srand(51792361);rand();
	for (scanf("%d",&n),i=1;i<=n;i++){
		scanf("%d",&a[i]);
		if (a[i]>m) m=a[i];
	}
	for (m*=2,i=2;i<=m;i++) if (!f[i])
		for (j=i+i;j<=m;j+=i) f[j]=1;
	for (int turns=100;turns--;){
		for (i=1;i<=n;i++){
			int x=rand()%n+1,y=rand()%n+1;
			swap(a[x],a[y]);
		}
		for (dn=0,i=1;i<=n;i++){
			if (dn+n-i+1<=ans) break;
			bool flag=1;
			for (j=1;j<=dn;j++) if (!f[d[j]+a[i]]){
				flag=0;break;
			}
			if (flag) d[++dn]=a[i];
		}
		ans=max(ans,dn);
	}
	printf("%d",ans);
}
