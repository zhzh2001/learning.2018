#include<bits/stdc++.h>
#define ll long long
#define N 100005
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
using namespace std;
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
int n,m,op,l,r,cnt;
map <int,int> q;
set <int> s;
struct xx{
	int l,r,num,le,ri;
}tree[N*50];
struct node{
	int lon,r;
	bool operator <(const node &a) const{
		return lon==a.lon?r>a.r:lon>a.lon;
	}
}t;
void newp(int &x,int l,int r){
	x=++cnt;tree[x].le=l;tree[x].ri=r;
}
void qadd(int x,int k,int p){
	tree[x].num+=p;
	if (tree[x].le==tree[x].ri) return;
	int mid=(tree[x].le+tree[x].ri)>>1;
	if (k<=mid){
		if (!tree[x].l) newp(tree[x].l,tree[x].le,mid);
		qadd(tree[x].l,k,p);
	}
	else{
		if (!tree[x].r) newp(tree[x].r,mid+1,tree[x].ri);
		qadd(tree[x].r,k,p);
	}
}
int query(int x,int l,int r){
	if (!x) return 0;
	if (tree[x].le==l&&tree[x].ri==r) return tree[x].num;
	int mid=(tree[x].le+tree[x].ri)>>1;
	if (r<=mid) return query(tree[x].l,l,r);
	if (l>mid) return query(tree[x].r,l,r);
	return query(tree[x].l,l,mid)+query(tree[x].r,mid+1,r);
}
set <node> num;
int ins(){
	t=*num.begin();
	r=t.r;l=t.r-t.lon+1;
	int mid=(l+r+1)>>1;
	num.erase(t);
	s.insert(mid);
	qadd(1,mid,1);
	t.lon=t.r-mid;num.insert(t);
	t.r=mid-1;t.lon=t.r-l+1;num.insert(t);
	return mid;
}
void del(int x){
	s.erase(x);qadd(1,x,-1);
	set <int>::iterator it=s.upper_bound(x);
	r=*it;r--;
	it--;
	l=*it;l++;
	t.r=x-1;t.lon=t.r-l+1;
	num.erase(t);
	t.r=r;t.lon=t.r-x;
	num.erase(t);
	t.r=r;t.lon=t.r-l+1;
	num.insert(t);
}
signed main(){
	freopen("seat.in","r",stdin);freopen("seat.out","w",stdout);
	n=read();m=read();
	tree[1].le=1;tree[1].ri=n;cnt=1;
	t.lon=n;t.r=n;num.insert(t);s.insert(0);s.insert(n+1);
	for (int i=1;i<=m;i++){
		op=read();
		if (op==0){
			l=read();r=read();
			wrn(query(1,l,r));
		}
		else{
			if (q.find(op)!=q.end()){
				if (q[op]==0){
					q[op]=ins();
				}
				else{
					del(q[op]);
					q[op]=0;
				}
			}
			else q[op]=ins();
		}
	}
	return 0;
}
