#include<bits/stdc++.h>
#define ll long long
#define N 800005
#define M 2500000
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
using namespace std;
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
int vis[N],z[N],tot,tot1,tot2,b[N],t,d,t1,k,ans,s,n,m,dis[N],cur[N],dui[N],x,y,a[N];
int nedge,Next[M*2],head[N],to[M*2],lon[M*2];
void add(int a,int b,int c){nedge++;Next[nedge]=head[a];head[a]=nedge;to[nedge]=b;lon[nedge]=c;}
void add2(int a,int b,int c){add(a,b,c);add(b,a,0);}
inline int g(int x){return x+(x&1?1:-1);}
bool bfs(){
	memset(dis,0,sizeof(dis));
	dui[1]=s;dis[s]=1;
	for (int l=1,r=1;l<=r;l++){
		t1=dui[l];
		for (int i=head[t1];i;i=Next[i]){
			if (!dis[to[i]]&&lon[i]){
				dis[to[i]]=dis[t1]+1;dui[++r]=to[i];
			}
		}
	}
	return dis[t];
}
int dfs(int x,int s){
	if (x==t) return s;
	for (int &i=cur[x];i;i=Next[i]){
		if (lon[i]&&dis[to[i]]==dis[x]+1){
			int t1=dfs(to[i],min(s,lon[i]));
			if (t1){
				lon[g(i)]+=t1;lon[i]-=t1;
				return t1;
			}
		}
	}
	return 0;
}
int main(){
	freopen("prime.in","r",stdin);freopen("prime.out","w",stdout);
	n=read();s=0;t=n+1;
	int pd=0;
	for (int i=1;i<=n;i++){
		x=read();
		if (x==1) pd=1;
		else{
			if (x%2==1) a[++tot1]=x;
			else b[++tot2]=x;
		}
	}
	for (int i=2;i<=200005;i++){
		if (!vis[i]) z[++tot]=i;
		for (int j=1;j<=tot&&z[j]<=200005/i;j++){
			vis[z[j]*i]=1;
			if (i%z[j]==0) break;
		}
	}
	if (pd) a[++tot1]=1;
	for (int i=1;i<=tot1;i++) add2(s,i,1);
	for (int i=1;i<=tot2;i++) add2(i+tot1,t,1);
	for (int i=1;i<=tot1;i++){
		for (int j=1;j<=tot2;j++){
			if (!vis[a[i]+b[j]]) add2(i,j+tot1,1);
		}
	}
	while (bfs()){
		for (int i=s;i<=t;i++) cur[i]=head[i];//这边的上下界注意修改
		d=dfs(s,inf);
		while (d){
			ans+=d;d=dfs(s,inf);
		}
	}
	wrn(tot1+tot2-ans); 
	return 0;
}
