#include<bits/stdc++.h>
#define ll long long
#define N 200005
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
using namespace std;
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
int n,m,a[N],b[N],num,p[N],ans,f[N];
signed main(){
	freopen("list.in","r",stdin);freopen("list.out","w",stdout);
	n=read();
	for (int i=1;i<=n;i++){
		b[i]=read();
		num=p[b[i]-1]+1;
		for (int j=i;j>num;j--) a[j]=a[j-1];
		a[num]=i;
		for (int j=i;j>=1;j--){
			p[b[a[j]]]=j;
		}
	}
	for (int i=1;i<=n;i++) b[n-a[i]+1]=i;
	for (int i=1;i<=n;i++){
		f[i]=1;
		for (int j=1;j<i;j++){
			if (b[i]>b[j]) f[i]=max(f[i],f[j]+1);
		}
		ans+=f[i];
	}
	wrn(ans);
	return 0;
}
