#include<bits/stdc++.h>
#define ll long long
#define N 200005
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
using namespace std;
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
int cmax,tot,n,m,l,r,num,a,b,root,g[N],g1[N],c[N],f[N],yk[N];
ll ans;
struct tre{
	int size,rd,key,l,r;
}t[N];
struct xx{
	int l,r,num,add;
}tree[N*4];
struct treap{
int build(int x){
	t[++num].rd=rand();t[num].key=x;t[num].size=1;return num;
}
void change(int x){
	t[x].size=t[t[x].r].size+t[t[x].l].size+1;
}
int merge(int x,int y){
	if (!x||!y) return x+y;
	if (t[x].rd<t[y].rd){
		t[x].r=merge(t[x].r,y);
		change(x);
		return x;
	}
	else{
		t[y].l=merge(x,t[y].l);
		change(y);
		return y;
	}
}
void split(int i,int k,int &x,int &y){
	if (!i){
		x=y=0;return;
	}
	if (t[t[i].l].size<k){
		x=i;split(t[i].r,k-t[t[i].l].size-1,t[i].r,y);
	}
	else{
		y=i;split(t[i].l,k,x,t[i].l);
	}
	change(i);
}
}t1;
void w(int k){
	if (!k) return;
	w(t[k].l);
	g1[t[k].key]=++tot;
	w(t[k].r);
}
void build(int x,int l,int r){
	tree[x].l=l;tree[x].r=r;
	if (l==r) return;
	int mid=(l+r)>>1;
	build(x*2,l,mid);build(x*2+1,mid+1,r); 
}
void add(int x,int l,int r){
	if (tree[x].l==l&&tree[x].r==r){
		tree[x].add++;return;
	}
	int mid=tree[x].l+tree[x].r>>1;
	if (r<=mid) add(x*2,l,r);
	else if (l>mid) add(x*2+1,l,r);
	else add(x*2,l,mid),add(x*2+1,mid+1,r);
}
void change(int x,int k,int p){
	p-=tree[x].add;
	if (tree[x].l==tree[x].r){
		tree[x].num=p;return;
	}
	int mid=tree[x].l+tree[x].r>>1;
	if (k<=mid) change(x*2,k,p);
	else change(x*2+1,k,p);
}
int find(int x,int k){
	if (tree[x].l==tree[x].r) return tree[x].num+tree[x].add;
	int mid=tree[x].l+tree[x].r>>1;
	if (k<=mid) return find(x*2,k)+tree[x].add;
	else return find(x*2+1,k)+tree[x].add;
}
signed main(){
	freopen("list.in","r",stdin);freopen("list.out","w",stdout);
	n=read();srand(23333);build(1,0,n);
	for (int i=1;i<=n;i++){
		c[i]=read();cmax=max(cmax,c[i]);
		l=find(1,c[i]-1)+1; 
		t1.split(root,l-1,a,b);
		root=t1.merge(a,t1.merge(t1.build(i),b));
		if (cmax>=c[i]+1) add(1,c[i]+1,cmax);
		change(1,c[i],l);
	}
	w(root);
	for (int i=1;i<=n;i++) g[n-i+1]=g1[i];
	tot=0;
	for (int i=1;i<=n;i++){
		if (g[i]>yk[tot]){
			yk[++tot]=g[i];f[i]=tot;continue;
		}
		int l=0,r=tot;
		while (l<r){
			int mid=(l+r+1)>>1;
			if (yk[mid]<g[i]) l=mid;
			else r=mid-1; 
		}
		f[i]=l+1;
		yk[l+1]=min(yk[l+1],g[i]);
	}
	for (int i=1;i<=n;i++) ans+=f[i];
	printf("%lld",ans);
	return 0;
}
