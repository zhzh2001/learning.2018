#include<bits/stdc++.h>
#define ll long long
#define N 100005
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
using namespace std;
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
int n,m,x,y,k,dis[N],vis[3005],p,t,pre[N];
int nedge,Next[N*2],head[N],to[N*2],lon[N*2];
void add(int a,int b,int c){nedge++;Next[nedge]=head[a];head[a]=nedge;to[nedge]=b;lon[nedge]=c;}
queue <int> q;
signed main(){
	freopen("cow.in","r",stdin);freopen("cow.out","w",stdout);
	n=read();m=read();
	for (int i=1;i<=m;i++){
		x=read();y=read();k=read();
		add(x,y,k);add(y,x,k);
	}
	for (int i=1;i<=n;i++) dis[i]=inf;
	dis[1]=0;vis[1]=1;q.push(1);
	while (!q.empty()){
		t=q.front();q.pop();vis[t]=0;
		for (int i=head[t];i;i=Next[i]){
			p=to[i];
			if (dis[t]+lon[i]<dis[p]){
				pre[p]=i;dis[p]=dis[t]+lon[i];
				if (!vis[p]) q.push(p);
			}
		}
	}
for (int ii=2;ii<=n;ii++){
	for (int i=1;i<=n;i++) dis[i]=inf;
	dis[1]=0;vis[1]=1;q.push(1);
	memset(vis,0,sizeof(vis));
	while (!q.empty()){
		t=q.front();q.pop();vis[t]=0;
		for (int i=head[t];i;i=Next[i]){
			if (i==pre[ii])continue;
			p=to[i];
			if (dis[t]+lon[i]<dis[p]){
				dis[p]=dis[t]+lon[i];
				if (!vis[p]) q.push(p);
			}
		}
	}
	if (dis[ii]==inf) wrn(-1);
	else wrn(dis[ii]);
}
	return 0;
}
