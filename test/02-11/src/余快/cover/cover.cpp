#include<bits/stdc++.h>
#define ll long long
#define N 100005
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
using namespace std;
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
int n,m,ans,vis[N];
struct xx{
	int g,c;
}z[N];
bool cmp(xx a,xx b){
	return a.g<b.g;
}
struct xx2{
	int l,r;
}c[N];
bool cmp2(xx2 a,xx2 b){
	return a.r<b.r;
}
signed main(){
	freopen("cover.in","r",stdin);freopen("cover.out","w",stdout);
	n=read();m=read();
	for (int i=1;i<=n;i++){
		c[i].l=read();c[i].r=read();
	}
	for (int i=1;i<=m;i++){
		z[i].g=read();z[i].c=read();
	}
	sort(z+1,z+m+1,cmp);
	sort(c+1,c+n+1,cmp2);
	for (int i=1;i<=m;i++){
		for (int j=1;j<=n;j++){
			if (z[i].c&&!vis[j]&&z[i].g>=c[j].l&&z[i].g<=c[j].r){
				ans++;z[i].c--;
				vis[j]=1;
			}
		}
	}
	wr(ans);
	return 0;
}
