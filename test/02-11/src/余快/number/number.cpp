#include<bits/stdc++.h>
#define ll long long
#define N 40005
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
using namespace std;
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
int n,m,t,a[N],num,l,vis[N],dp[N],f[N][205];
signed main(){
	freopen("number.in","r",stdin);freopen("number.out","w",stdout);
	n=read();m=read();
	for (int i=1;i<=n;i++) a[i]=read();
	t=sqrt(n);
	for (int i=1;i<=t;i++){
		memset(vis,0,sizeof(vis));num=0;l=1;
		for (int j=1;j<=n;j++){
			vis[a[j]]++;num+=(vis[a[j]]==1);
			while (num>i){
				vis[a[l]]--;num-=(vis[a[l]]==0);l++;
			}
			f[j][i]=l;
		}
	}
	for (int i=1;i<=n;i++){
		dp[i]=i;
		for (int j=1;j<=t;j++){
			dp[i]=min(dp[i],dp[f[i][j]-1]+j*j);
		}
	}
	wr(dp[n]);
	return 0;
}
