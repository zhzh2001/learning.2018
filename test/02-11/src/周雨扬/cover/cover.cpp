#include<bits/stdc++.h>
#define MX 1000
#define M 233333
#define N 10000
using namespace std;
struct edge{int to,next,f;}e[M];
int id[N],head[N],cur[N],s[N];
int tot=1,S,T,n,l,sum,ans;
int q[N],dis[N];
void add(int x,int y,int v){
	e[++tot]=(edge){y,head[x],v};
	head[x]=tot;
	e[++tot]=(edge){x,head[y],0};
	head[y]=tot;
}
void build(int k,int l,int r){
	id[k]=++sum;
	if (l==r) return;
	int mid=(l+r)/2;
	build(k*2,l,mid);
	build(k*2+1,mid+1,r);
	add(id[k],id[k*2],1e9);
	add(id[k],id[k*2+1],1e9);
}
void insert(int k,int l,int r,int x,int y,int at){
	if (l==x&&r==y){
		add(at,id[k],1e9);
		return;
	}
	int mid=(l+r)/2;
	if (y<=mid) insert(k*2,l,mid,x,y,at);
	else if (x>mid) insert(k*2+1,mid+1,r,x,y,at);
	else insert(k*2,l,mid,x,mid,at),
		 insert(k*2+1,mid+1,r,mid+1,y,at);
}
void work(int k,int l,int r,int p,int v){
	if (l==r){
		add(id[k],T,v);
		return;
	}
	int mid=(l+r)/2;
	if (p<=mid) work(k*2,l,mid,p,v);
	else work(k*2+1,mid+1,r,p,v);
}

bool bfs(){
	for (int i=1;i<=sum;i++)
		dis[i]=-1;
	int h=0,t=1;
	dis[S]=0; q[1]=S;
	while (h!=t){
		int x=q[++h];
		for (int i=head[x];i;i=e[i].next)
			if (e[i].f&&dis[e[i].to]==-1){
				dis[e[i].to]=dis[x]+1;
				if (e[i].to==T) return 1;
				q[++t]=e[i].to;
			}
	}
	return 0;
}
int dinic(int x,int flow){
	if (x==T) return flow;
	int k,rest=flow;
	for (int i=cur[x];i&&rest;i=e[i].next)
		if (dis[e[i].to]==dis[x]+1&&e[i].f){
			cur[x]=i;
			k=dinic(e[i].to,min(e[i].f,rest));
			e[i].f-=k; rest-=k; e[i^1].f+=k;
		}
	if (rest) dis[x]=-1;
	return flow-rest;
}

int main(){
	freopen("cover.in","r",stdin);
	freopen("cover.out","w",stdout);
	S=++sum; T=++sum;
	scanf("%d%d",&n,&l);
	build(1,1,MX);
	for (int i=1;i<=n;i++){
		int x,y;
		scanf("%d%d",&x,&y);
		add(S,++sum,1);
		insert(1,1,MX,x,y,sum);
	}
	for (int i=1;i<=l;i++){
		int x,y;
		scanf("%d%d",&x,&y);
		s[x]+=y;
	}
	for (int i=1;i<=MX;i++)
		work(1,1,MX,i,s[i]);
	while (bfs()){
		for (int i=1;i<=sum;i++)
			cur[i]=head[i];
		ans+=dinic(S,1e9);
	}
	printf("%d",ans);
}
/*
3 2
3 10
2 6
2 5
6 2
4 2
*/
