#include<bits/stdc++.h>
#define N 40005
using namespace std;
int cnt,n,m;
int s[N],a[N],f[N],to[N][205];
int main(){
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++) 
		scanf("%d",&a[i]);
	int jdb=1;
	for (;1ll*jdb*jdb<=n;jdb++);
	for (int times=1;times<=jdb;times++){
		memset(s,0,sizeof(s));
		int ri=0,cnt=1; s[0]=1;
		for (int i=1;i<=n;i++){
			s[a[i-1]]--;
			if (!s[a[i-1]]) cnt--;
			for (;cnt<=times&&ri<=n;){
				++ri;
				if (!s[a[ri]]) cnt++;
				s[a[ri]]++;
			}
			to[i][times]=ri;
		}
	}
	memset(f,10,sizeof(f));
	f[1]=0;
	for (int i=1;i<=n;i++)
		for (int j=1;j<=jdb;j++)
			f[to[i][j]]=min(f[to[i][j]],f[i]+j*j);
	printf("%d",f[n+1]);
}
/*
11 3
1 2 1 3 2 2 3 3 3 3 3
*/
