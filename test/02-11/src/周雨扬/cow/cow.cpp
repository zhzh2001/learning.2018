#include<bits/stdc++.h>
#define pa pair<int,int>
#define N 100005
using namespace std;
int n,m;
int x[N*2],y[N*2],v[N*2];
int vis[N*2],ans[N];
struct Segment{
	int mn[N*4];
	void build(int k,int l,int r){
		mn[k]=2019260817+1;
		if (l==r) return;
		int mid=(l+r)/2;
		build(k*2,l,mid);
		build(k*2+1,mid+1,r);
	}
	void change(int k,int l,int r,int x,int y,int v){
		if (l==x&&r==y){
			mn[k]=min(mn[k],v);
			return;
		}
		int mid=(l+r)/2;
		if (y<=mid) change(k*2,l,mid,x,y,v);
		else if (x>mid) change(k*2+1,mid+1,r,x,y,v);
		else change(k*2,l,mid,x,mid,v),
			change(k*2+1,mid+1,r,mid+1,y,v);
	}
	int ask(int k,int l,int r,int p){
		int ans=mn[k];
		if (l==r) return ans;
		int mid=(l+r)/2;
		if (p<=mid) ans=min(ans,ask(k*2,l,mid,p));
		else ans=min(ans,ask(k*2+1,mid+1,r,p));
		return ans;
	}
};
Segment Seg;
struct Graph1{;
	struct edge1{int to,next,v,id;};
	edge1 e[N*4];
	int head[N],dis[N];
	priority_queue<pa > q;
	int tot,from[N];
	void add(int x,int y,int v,int id){
		e[++tot]=(edge1){y,head[x],v,id};
		head[x]=tot;
	}
	void SSSP(){
		for (int i=1;i<=n;i++)
			dis[i]=2e9,vis[i]=0;
		dis[1]=0;
		for (;!q.empty();q.pop());
		q.push(pa(0,1));
		while (!q.empty()){
			int x=q.top().second; q.pop();
			if (vis[x]==1) continue; vis[x]=1;
			for (int i=head[x];i;i=e[i].next)
				if (dis[e[i].to]>dis[x]+e[i].v){
					dis[e[i].to]=dis[x]+e[i].v;
					from[e[i].to]=i;
					q.push(pa(-dis[e[i].to],e[i].to));
				}
		}
	}
};
Graph1 T1;
struct Graph2{
	struct edge2{
		int to,next;
	};
	edge2 e[N*2];
	int head[N],dfn[N];
	int fa[N],top[N];
	int dep[N],sz[N];
	int tot,T;
	Graph2(){
		tot=T=sz[0]=0;
		dep[0]=0;
		fa[1]=0;
	}
	void add(int x,int y){
		e[++tot]=(edge2){y,head[x]};
		head[x]=tot;
	}
	void dfs(int x){
		dep[x]=dep[fa[x]]+1; sz[x]=1;
		for (int i=head[x];i;i=e[i].next)
			if (e[i].to!=fa[x]){
				fa[e[i].to]=x;
				dfs(e[i].to);
				sz[x]+=sz[e[i].to];
			}
	}
	void dfs(int x,int tp){
		top[x]=tp; dfn[x]=++T;
		int k=0;
		for (int i=head[x];i;i=e[i].next)
			if (e[i].to!=fa[x]&&sz[e[i].to]>sz[k])
				k=e[i].to;
		if (k) dfs(k,tp);
		for (int i=head[x];i;i=e[i].next)
			if (e[i].to!=fa[x]&&e[i].to!=k)
				dfs(e[i].to,e[i].to);
	}
	void insert(int x,int y,int v){
		v=v+T1.dis[x]+T1.dis[y];
		for (;top[x]!=top[y];x=fa[top[x]]){
			if (dep[top[x]]<dep[top[y]]) swap(x,y);
			Seg.change(1,1,n,dfn[top[x]],dfn[x],v);
		}
		if (dep[x]>dep[y]) swap(x,y);
		if (dfn[x]!=dfn[y])
			Seg.change(1,1,n,dfn[x]+1,dfn[y],v);
	}
};
Graph2 T2;
void init(){
	memset(vis,0,sizeof(vis));
	for (int i=2;i<=n;i++)
		vis[T1.e[T1.from[i]].id]=1;
	for (int i=1;i<=m;i++)
		if (vis[i]){
			T2.add(x[i],y[i]);
			T2.add(y[i],x[i]);
		}
	T2.dfs(1);
	T2.dfs(1,1);
}
void work(){
	for (int i=2;i<=n;i++){
		int x=Seg.ask(1,1,n,T2.dfn[i]);
		if (x>2003289471) ans[i]=-1;
		else ans[i]=x-T1.dis[i];
	}
}
int main(){
	freopen("cow.in","r",stdin);
	freopen("cow.out","w",stdout);
	scanf("%d%d",&n,&m);
	Seg.build(1,1,n);
	for (int i=1;i<=m;i++){
		scanf("%d%d%d",&x[i],&y[i],&v[i]);
		T1.add(x[i],y[i],v[i],i);
		T1.add(y[i],x[i],v[i],i);
	}
	T1.SSSP();
	init();
	for (int i=1;i<=m;i++)
		if (!vis[i])
			T2.insert(x[i],y[i],v[i]);
	work();
	for (int i=2;i<=n;i++)
		printf("%d\n",ans[i]);
}
/*
4 5
1 2 2
1 3 6
3 4 3
3 2 2
2 4 2
*/
