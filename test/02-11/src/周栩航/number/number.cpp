#include<iostream>
#include<cstdio>
#include<cmath>
#include<queue>
#include<cstring>
#include<ctime>
#include<algorithm>
#define For(i,j,k)  for(register int i=j;i<=k;++i)
#define Dow(i,j,k)  for(register int i=k;i>=j;--i)
#define ll long long
#define mk make_pair
#define pb push_back
#define eps 1e-8
#define pa pair<int,int>
#define fir first
#define sec second
using namespace std;
inline ll read()
{
    ll t=0,dp=1;char cnt=getchar();
    while(cnt<'0'||cnt>'9')   {if(cnt=='-') dp=-1;cnt=getchar();}
    while(cnt>='0'&&cnt<='9') t=t*10+cnt-48,cnt=getchar();
    return t*dp;
}
int n,m,top,a[50001],dp[50001],cnt[50001],nxt[50001],x,pos[50001];
int main()//max ans=n  所以单段不超过sqrt(n) 
{
	freopen("number.in","r",stdin);freopen("number.out","w",stdout);
    n=read();m=read();
    For(i,1,n)
    {
        x=read();if(a[top]!=x)  a[++top]=x;
    }
    n=top;
    int t=sqrt(n)+1;
    For(i,1,n)
    {
        dp[i]=1e9;
        For(j,1,t)
            if(nxt[a[i]]<=pos[j])  cnt[j]++; 
        nxt[a[i]]=i;//前一个数出现位置
        For(j,1,t)
        {
            if(cnt[j]>j)
            {
                int t=pos[j]+1;
                while(nxt[a[t]]>t)   t++;
                pos[j]=t;cnt[j]--;
            }
        }
        For(j,1,t)  dp[i]=min(dp[i],dp[pos[j]]+j*j);
    }
    cout<<dp[n]<<endl;
}
/*
11 3
1
2
1
3
2
2
3
3
3
3
3
*/
