#include<iostream>
#include<cstdio>
#include<cmath>
#include<queue>
#include<cstring>
#include<ctime>
#include<algorithm>
#define For(i,j,k)  for(register int i=j;i<=k;++i)
#define Dow(i,j,k)  for(register int i=k;i>=j;--i)
#define ll long long
#define mk make_pair
#define pb push_back
#define eps 1e-8
#define pa pair<int,int>
#define fir first
#define sec second
using namespace std;
inline ll read()
{
    ll t=0,f=1;char c=getchar();
    while(c<'0'||c>'9')   {if(c=='-') f=-1;c=getchar();}
    while(c>='0'&&c<='9') t=t*10+c-48,c=getchar();
    return t*f;
}
int n,m;
int main()
{
	srand(time(0));
	freopen("cover.in","w",stdout);
	n=2500;m=250;
	cout<<n<<' '<<m<<endl;
	For(i,1,n)
	{
		int x=rand()%1000+1,y=rand()%1000+1;
		if(x>y)	swap(x,y);
		cout<<x<<' '<<y<<endl;
	}
	For(i,1,m)
	{
		int x=rand()%1000+1,y=rand()%10+1;
		cout<<x<<' '<<y<<endl;
	}
}
