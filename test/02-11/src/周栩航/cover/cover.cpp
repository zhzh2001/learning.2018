#include<iostream>
#include<cstdio>
#include<cmath>
#include<queue>
#include<cstring>
#include<ctime>
#include<algorithm>
#define For(i,j,k)  for(register int i=j;i<=k;++i)
#define Dow(i,j,k)  for(register int i=k;i>=j;--i)
#define ll long long
#define mk make_pair
#define pb push_back
#define eps 1e-8
#define pa pair<int,int>
#define fir first
#define sec second
using namespace std;
inline ll read()
{
    ll t=0,dp=1;char cnt=getchar();
    while(cnt<'0'||cnt>'9')   {if(cnt=='-') dp=-1;cnt=getchar();}
    while(cnt>='0'&&cnt<='9') t=t*10+cnt-48,cnt=getchar();
    return t*dp;
}
int n,m,ans;
struct node{int mx,mi;} c[5001];
struct fkfks{int x,num;}	p[5001];
inline bool cmp(node x,node y){return x.mx<y.mx;}
int main()
{
	freopen("cover.in","r",stdin);freopen("cover.out","w",stdout);
	n=read();m=read();
	For(i,1,n)
		c[i].mi=read(),c[i].mx=read();
	For(i,1,m)	
		p[i].x=read(),p[i].num=read();
	sort(c+1,c+n+1,cmp);
	p[0].x=1e9;
	For(i,1,n)
	{
		int now=0;
		For(j,1,m)
			if(p[j].num&&c[i].mi<=p[j].x&&p[j].x<=c[i].mx)	
				if(p[j].x<p[now].x)	
					now=j;
		if(now!=0)	p[now].num--,ans++;
	}
	cout<<ans<<endl;
}
/*
3 2
3 10
2 6
2 5
6 2
4 2
*/
