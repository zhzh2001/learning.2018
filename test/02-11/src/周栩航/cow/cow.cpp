#include<iostream>
#include<cstdio>
#include<cmath>
#include<queue>
#include<cstring>
#include<ctime>
#include<algorithm>
#define For(i,j,k)  for(register int i=j;i<=k;++i)
#define Dow(i,j,k)  for(register int i=k;i>=j;--i)
#define ll long long
#define mk make_pair
#define pb push_back
#define eps 1e-8
#define pa pair<int,int>
#define fir first
#define sec second
using namespace std;
inline ll read()
{
    ll t=0,dp=1;char cnt=getchar();
    while(cnt<'0'||cnt>'9')   {if(cnt=='-') dp=-1;cnt=getchar();}
    while(cnt>='0'&&cnt<='9') t=t*10+cnt-48,cnt=getchar();
    return t*dp;
}
inline void write(int x){if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(int x){write(x);puts("");}
int n,cnt,m,x,y,z,poi[500001],nxt[500001],head[500001],v[500001],ans[500001],dis[500001],fro[500001];
int Fa[500001],F[500001],dep[500001],top,Ans[500001];
struct node{int v,x,y;}	huan[500001];
inline bool cmp(node x,node y){return x.v<y.v;}
inline int Get(int x){return x==Fa[x]?Fa[x]:Fa[x]=Get(Fa[x]);}
inline int Build(int x){return dep[x]?dep[x]:dep[x]=Build(F[x])+1;}
inline void add(int x,int y,int z){poi[++cnt]=y;nxt[cnt]=head[x];head[x]=cnt;v[cnt]=z;fro[cnt]=x;}
priority_queue<pa> Q;
bool vis[500001];
inline void Dij()
{
	For(i,2,n)	dis[i]=ans[i]=1e9;dep[1]=1;
	Q.push(mk(0,1));
	while(!Q.empty())
	{
		pa tmp=Q.top();Q.pop();
		if(vis[tmp.sec])	continue;
		vis[tmp.sec]=1;
		for(int i=head[tmp.sec];i;i=nxt[i])
		{
			if(vis[poi[i]])	continue;
			if(-tmp.fir+v[i]<dis[poi[i]])	dis[poi[i]]=-tmp.fir+v[i],F[poi[i]]=tmp.sec,Q.push(mk(-dis[poi[i]],poi[i]));		
		}	
	}
}
int main()
{
	freopen("cow.in","r",stdin);freopen("cow.out","w",stdout);
	n=read();m=read();
	For(i,1,n)	Fa[i]=i,Ans[i]=2e9+7;
	For(i,1,m)	x=read(),y=read(),z=read(),add(x,y,z),add(y,x,z);
	Dij();
	For(i,2,n)	dep[i]=Build(i);//���������� 
	For(j,1,cnt/2)	
	{
		int i=j*2;
		if(abs(dis[fro[i]]-dis[poi[i]])==v[i])	continue;
		huan[++top].v=dis[fro[i]]+dis[poi[i]]+v[i];huan[top].x=fro[i];huan[top].y=poi[i];
	}
	sort(huan+1,huan+top+1,cmp);
	For(i,1,top)
	{
		int tx=Get(huan[i].x),ty=Get(huan[i].y);
		while(1)
		{
			if(dep[tx]<dep[ty])	swap(tx,ty);
			Ans[tx]=min(Ans[tx],huan[i].v-dis[tx]);
			Fa[tx]=Get(F[tx]);
			tx=Get(tx);
			if(tx==ty)	break;
		}
	}
	For(i,2,n)	if(Ans[i]==2e9+7)	puts("-1");else writeln(Ans[i]);	
}
/*
4 5
1 2 2
1 3 6
3 4 3
3 2 2
2 4 2
*/
