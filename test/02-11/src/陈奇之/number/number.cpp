#include<bits/stdc++.h>
using namespace std;
#define gc getchar
#define pc putchar
#define RG register
#define mem(x,v) memset(x,v,sizeof(x))
#define rep(i,a,b) for(RG int i = (a);i < (b);i++)
#define Rep(i,a,b) for(RG int i = (a);i <=(b);i++)
#define Down(i,a,b) for(RG int i = (a);i >= (b);i--)
inline int read(){
	int T = 0,f = false;
	char c = gc();
	for(;!isdigit(c);c=gc()) f |= (c == '-');
	for(;isdigit(c);c=gc()) T = (T << 1) + (T << 3) + (c ^ 48);
	return f ? -T : T;
}
#define rd read
const int MAXN = 40005;
int f[MAXN],a[MAXN];
bool vis[MAXN];
int main(){
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	RG int n,m,res;
	n = rd();m = rd();
	Rep(i,1,n)a[i] = rd();
	f[0] = 0;
	Rep(i,1,n){
		f[i] = n;
		mem(vis,false);
		vis[a[i]] = true;
		res = 1;
		Down(j,i-1,0){
			f[i] = min(f[i],f[j] + res * res);
			if(!vis[a[j]]) res++;
			if(res * res >= n) break;
			vis[a[j]] = true;
		}
	}
	printf("%d\n",f[n]);
	return 0;
}
/*
11 3
1
2
1
3
2
2
3
3
3
3
3
*/
