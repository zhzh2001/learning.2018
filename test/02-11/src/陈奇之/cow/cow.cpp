#include<bits/stdc++.h>
using namespace std;
#define gc getchar
#define pc putchar
#define RG register
#define mem(x,v) memset(x,v,sizeof(x))
#define rep(i,a,b) for(RG int i = (a);i < (b);i++)
#define Rep(i,a,b) for(RG int i = (a);i <=(b);i++)
#define rd read
int read(){
	int T = 0,f = false;
	char c = gc();
	for(;!isdigit(c);c=gc()) f |= (c == '-');
	for(;isdigit(c);c=gc()) T = (T << 1) + (T << 3) + (c ^ 48);
	return f ? -T : T;
}
#define N 100050
#define M 200050
#define mp make_pair
struct Edge{
	int to,nxt,dist,flag;
	Edge(){}
	Edge(int to,int nxt,int dist):to(to),nxt(nxt),dist(dist){flag = false;}
}edge[M*2];
struct E{
	int u,v,w;
	bool operator < (const E &x) const{
		return w < x.w;
	}
}e[M];
int first[N],nume;
void Addedge(int a,int b,int c){
	edge[nume] = Edge(b,first[a],c);
	first[a] = nume++;
}

int fa[N],pre[N],dep[N],ans[N];
int dis[N],vis[N];
typedef pair<int,int> pii;
void dijkstra(){
	priority_queue<pii,vector<pii>,less<pii> > Q;
	mem(dis,0x3f);mem(vis,false);
	dis[1] = 0;
	Q.push(mp(0,1));
	while(!Q.empty()){
		int u = Q.top().second;Q.pop();
		if(vis[u]) continue;vis[u] = true;
		for(int e=first[u];~e;e=edge[e].nxt){
			int v = edge[e].to;
			if(dis[v] > dis[u] + edge[e].dist){
				dis[v] = dis[u] + edge[e].dist;
				pre[v] = u;
				dep[v] = dep[u] + 1;
				Q.push(mp(dis[v],v));
			}
		}
	}
}

int find(int x){
	return x==fa[x]?x:fa[x]=find(fa[x]);
}

int n,m;
int main(){
	freopen("cow.in","r",stdin);
	freopen("cow.out","w",stdout);
	n = rd(),m = rd();
	mem(first,-1);nume = 0;
	Rep(i,1,m){
		int x = rd(),y = rd(),z = rd();
		Addedge(x,y,z);
		Addedge(y,x,z);
	}
	dijkstra();
	m = 0;
	for(int i=0;i<nume;i+=2){
		int u = edge[i^1].to,v = edge[i].to,w = dis[u] + dis[v] + edge[i].dist;
		if(dis[u] + edge[i].dist == dis[v]) continue;
		if(dis[v] + edge[i].dist == dis[u]) continue;
		++m;
		e[m].u = u,e[m].v = v,e[m].w = w;
	}
	sort(e+1,e+1+m);
	Rep(i,1,n) fa[i] = i;
	mem(ans,-1);
	Rep(i,1,m){
		int u = find(e[i].u),v = find(e[i].v),w = e[i].w;
		while(u != v){
			if(dep[u] > dep[v]){
				ans[u] = w - dis[u];
				fa[u] = pre[u];
			} else{
				ans[v] = w - dis[v];
				fa[v] = pre[v];
			}
			u = find(u),v = find(v);
		}
	}
	Rep(i,2,n)
		printf("%d\n",ans[i]);
	return 0;
}
/*
4 5
1 2 2
1 3 6
3 4 3
3 2 2
2 4 2
*/
