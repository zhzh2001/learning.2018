#include<bits/stdc++.h>
using namespace std;
#define gc getchar
#define pc putchar
#define RG register
#define mem(x,v) memset(x,v,sizeof(x))
#define rep(i,a,b) for(RG int i = (a);i < (b);i++)
#define Rep(i,a,b) for(RG int i = (a);i <=(b);i++)
#define rd read
inline int read(){
	int T = 0,f = false;
	char c = gc();
	for(;!isdigit(c);c=gc()) f |= (c == '-');
	for(;isdigit(c);c=gc()) T = (T << 1) + (T << 3) + (c ^ 48);
	return f ? -T : T;
}
struct A{
	int L,R,flag;
	bool operator < (const A &x) const{
		return R < x.R;
	}
}a[2505];
struct B{
	int v,num;
	bool operator < (const B &x) const{
		return v < x.v;
	}
}b[2505];
int C,L;
void solve(B x){
	for(int i=1;i<=C;i++){
		if(!a[i].flag && a[i].L <= x.v && x.v <= a[i].R){
			a[i].flag = true;
			if(--x.num==0) break;
		}
	}
}
int main(){
	freopen("cover.in","r",stdin);
	freopen("cover.out","w",stdout);
	C = rd(),L = rd();
	Rep(i,1,C)
		a[i].L = rd(),a[i].R = rd(),a[i].flag = false;
	Rep(i,1,L){
		b[i].v = rd();
		b[i].num = rd();
	}
	sort(a+1,a+1+C);
	sort(b+1,b+1+L);
	Rep(i,1,L) solve(b[i]);
	int ans = 0;
	Rep(i,1,C) if(a[i].flag) ans++;
	printf("%d\n",ans);
	return 0;
}
