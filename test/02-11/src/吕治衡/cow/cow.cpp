#include<bits/stdc++.h>
inline int read(){
	char c=getchar();while (c!='-'&&(c<'0'||c>'9'))c=getchar();
	int k=0,kk=1;if (c=='-')c=getchar(),kk=-1;
	while (c>='0'&&c<='9')k=k*10+c-'0',c=getchar();return kk*k;
}using namespace std;
void write(int x){if (x<0)x=-x,putchar('-');if (x/10)write(x/10);putchar(x%10+'0');}
void writeln(int x){write(x);puts("");}
int n,m,x,y,z,a[400010][3],last[100010],f[100010],d[1000010],kk,flag[100010],ff[100010],fa[100010][21],de[100010];
int lazy[5000000],l[5000000],r[5000000],mi[5000000],kkk,t[100010];
void doit(int x,int y,int z){a[++kk][0]=last[x];a[kk][1]=y;a[kk][2]=z;last[x]=kk;}
inline void pushdown(int x){//下传 
	if (l[x])mi[l[x]]+=lazy[x],lazy[l[x]]+=lazy[x];
	if (r[x])mi[r[x]]+=lazy[x],lazy[r[x]]+=lazy[x];
	lazy[x]=0;
}void putit(int d,int x,int y,int ll,int rr){//单点min覆盖 
	if (ll==rr){mi[d]=min(mi[d],y);return;}
	if (lazy[d])pushdown(d);
	int m=(ll+rr)/2;if (x<=m){
		if (!l[d])l[d]=++kkk;putit(l[d],x,y,ll,m);
	}else{
		if (!r[d])r[d]=++kkk;putit(r[d],x,y,m+1,rr);
	}mi[d]=min(mi[l[d]],mi[r[d]]);
}int findit(int d,int x,int ll,int rr){//前缀min 
	if (rr<=x||!d)return mi[d];if (lazy[d])pushdown(d);
	int m=(ll+rr)/2;
	if (x<=m)return findit(l[d],x,ll,m);
		else return min(mi[l[d]],findit(r[d],x,m+1,rr));
}int hb(int x,int y){//线段树合并 
	if (!x)return y;if (!y)return x;
	if (lazy[x])pushdown(x);if (lazy[y])pushdown(y);
	mi[x]=min(mi[x],mi[y]);l[x]=hb(l[x],l[y]);r[x]=hb(r[x],r[y]);
	return x;
}int lca(int x,int y){
	if (de[x]<de[y])swap(x,y);
	for (int i=20;i>=0;i--)if (de[fa[x][i]]>=de[y])x=fa[x][i];
	for (int i=20;i>=0;i--)if (fa[x][i]!=fa[y][i])x=fa[x][i];
	if (x==y)return x;else return fa[x][0]; 
}void csh(int x,int dee,int fh){
	de[x]=dee;fa[x][0]=fh;
	for (int i=last[x];i;i=a[i][0])
		if (f[a[i][1]]==f[x]+a[i][2])csh(a[i][1],dee+1,x);
}void dfs(int x){
	t[x]=++kkk;for (int i=last[x];i;i=a[i][0])if (f[a[i][1]]==f[x]+a[i][2]){
		dfs(a[i][1]);lazy[t[a[i][1]]]+=a[i][2];t[x]=hb(t[x],t[a[i][1]]);
	}else if (f[a[i][1]]+a[i][2]!=f[x]){
		ff[x]=min(ff[x],f[a[i][1]]+a[i][2]);putit(t[x],de[lca(x,a[i][1])],f[a[i][1]]+a[i][2],1,n);
	}ff[x]=min(ff[x],findit(t[x],de[x]-1,1,n));
}
signed main(){
	freopen("cow.in","r",stdin);freopen("cow.out","w",stdout);
	n=read();m=read();for (int i=1;i<=m;i++)x=read(),y=read(),z=read(),doit(x,y,z),doit(y,x,z);
	memset(f,63,sizeof(f));memset(ff,63,sizeof(ff));memset(mi,63,sizeof(mi));
	ff[1]=f[1]=0;int l=0,r=1;d[1]=1;flag[1]=1;
	while (l<r){
		l=l==1000000?0:l+1;for (int i=last[d[l]];i;i=a[i][0])if (f[d[l]]+a[i][2]<f[a[i][1]]){
			f[a[i][1]]=f[d[l]]+a[i][2];if (!flag[a[i][1]]){
				r=r==1000000?0:r+1;d[r]=a[i][1];flag[a[i][1]]=1;
			}
		}
	}csh(1,1,0);
	for (int i=1;i<=20;i++)for (int j=1;j<=n;j++)fa[j][i]=fa[fa[j][i-1]][i-1];
	dfs(1);for (int i=2;i<=n;i++)writeln(ff[i]>1e9?-1:ff[i]);
}
/*
4 5
1 2 2
1 3 6
3 4 3
3 2 2
2 4 2
*/

