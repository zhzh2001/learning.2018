#include<bits/stdc++.h>
#define ll long long
#define inf 1000000000
#define N 1000000
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
int n,m,s[N];
struct data{
	int l,r;
	bool operator <(const data &a)const{
		return r<a.r;
	}
}a[N];
int main(){
	freopen("cover.in","r",stdin);
	freopen("cover.out","w",stdout);
	scanf("%d%d",&n,&m);int ans=0;
	For(i,1,n) scanf("%d%d",&a[i].l,&a[i].r);sort(a+1,a+1+n);
	For(i,1,m){
		int x,y;scanf("%d%d",&x,&y);
		s[x]+=y;
	}
	For(i,1,n) For(j,a[i].l,a[i].r) if(s[j]){s[j]--,ans++;break;}
	printf("%d\n",ans);
	return 0;
}
