#include<bits/stdc++.h>
#define ll long long
#define inf 1000000000
#define N 100005
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
int n,m;
struct data{
	int x,val;
	bool operator <(const data &a)const{return val>a.val;}
};
struct edge{int nxt,to,u,v,w;}e[N<<2];
int cnt,head[N],mark[N<<2];
inline void insert(int u,int v,int w){e[++cnt]=(edge){head[u],v,u,v,w};head[u]=cnt;}
int dis[N],vis[N];
inline void spfa(){
	priority_queue <data> Q;Q.push((data){1,0});memset(dis,63,sizeof dis);dis[1]=0;
	while(!Q.empty()){
		data x=Q.top();Q.pop();
		if(vis[x.x]) continue;vis[x.x]=1;
		for(int i=head[x.x];i;i=e[i].nxt)
			if(dis[e[i].to]>dis[x.x]+e[i].w) dis[e[i].to]=dis[x.x]+e[i].w,Q.push((data){e[i].to,dis[e[i].to]});
	}
}
int dep[N],son[N],sz[N],ind,l[N],r[N],fa[N][25],bin[25],bl[N];
inline void dfs(int x,int f){
	sz[x]=1;
	For(i,1,20){
		if(dep[x]<bin[i]) break;
		fa[x][i]=fa[fa[x][i-1]][i-1];
	}
	for(int y,i=head[x];i;i=e[i].nxt)
		if((y=e[i].to)!=f&&dis[y]==dis[x]+e[i].w){
			mark[i]=1;dep[y]=dep[x]+1;fa[y][0]=x;
			dfs(y,x);sz[x]+=sz[y];
			if(sz[son[x]]<sz[y]) son[x]=y;
		}
}
inline void Dfs(int x,int fks){
	l[x]=++ind;bl[x]=fks;
	if(son[x]) Dfs(son[x],fks);
	for(int y,i=head[x];i;i=e[i].nxt)
		if((y=e[i].to)!=fa[x][0]&&y!=son[x]&&mark[i]) Dfs(y,y);
	r[x]=ind;
}
int t[N<<2];
inline void build(int k,int l,int r){
	t[k]=inf;if(l==r) return;int mid=(l+r)>>1;
	build(k<<1,l,mid);build(k<<1|1,mid+1,r);
}
inline void pushdown(int k,int l,int r){
	if(t[k]==inf||l==r) return;
	t[k<<1]=min(t[k<<1],t[k]);
	t[k<<1|1]=min(t[k<<1|1],t[k]);
}
inline void change(int k,int l,int r,int x,int y,int val){
	pushdown(k,l,r);
	if(l==x&&r==y){t[k]=min(t[k],val);return;}
	int mid=(l+r)>>1;
	if(y<=mid) change(k<<1,l,mid,x,y,val);
	else if(x>mid) change(k<<1|1,mid+1,r,x,y,val);
	else change(k<<1,l,mid,x,mid,val),change(k<<1|1,mid+1,r,mid+1,y,val);
}
inline void Change(int t,int x,int val){
	while(bl[x]!=bl[t]){
		change(1,1,n,l[bl[x]],l[x],val);
		x=fa[bl[x]][0];
	}
	if(x!=t)change(1,1,n,l[t]+1,l[x],val);
}
inline int query(int k,int l,int r,int x){
	pushdown(k,l,r);
	if(l==r) return t[k];
	int mid=(l+r)>>1;
	if(x<=mid) return query(k<<1,l,mid,x);
	else return query(k<<1|1,mid+1,r,x);
}
inline int lca(int x,int y){
	if(dep[x]<dep[y]) swap(x,y);
	int del=dep[x]-dep[y];
	For(i,0,20) if(del&bin[i]) x=fa[x][i];
	Rep(i,20,0) if(fa[x][i]!=fa[y][i]) x=fa[x][i],y=fa[y][i];
	if(x==y) return x;
	return fa[x][0];
}
int main(){
	freopen("cow.in","r",stdin);
	freopen("cow.out","w",stdout);
	bin[0]=1;For(i,1,24) bin[i]=bin[i-1]<<1;
	scanf("%d%d",&n,&m);
	For(i,1,m){
		int x,y,w;scanf("%d%d%d",&x,&y,&w);
		insert(x,y,w);insert(y,x,w);
	}spfa();dfs(1,0);Dfs(1,1);build(1,1,n);
	For(i,1,m*2)if(!mark[i]){
		int t=lca(e[i].u,e[i].v);
		Change(t,e[i].v,dis[e[i].u]+e[i].w+dis[e[i].v]);
	}
	For(i,2,n){
		int ans=query(1,1,n,l[i]);
		if(ans==inf) puts("-1");
		else printf("%d\n",ans-dis[i]);
	}
	return 0;
}
