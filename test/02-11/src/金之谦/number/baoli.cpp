#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int n,m,a[100010],f[100010],nex[100010],q[100010],p[100010];
inline int sqr(int x){return x*x;}
signed main()
{
	freopen("number.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=n;i++)a[i]=read(),f[i]=1e18;
	for(int i=1;i<=n;i++){
		for(int j=i-1;j>=nex[a[i]];j--)p[j]++;
		nex[a[i]]=i;
		for(int j=i-1;j>=0;j--)f[i]=min(f[i],f[j]+sqr(p[j]));
	}
	writeln(f[n]);
	return 0;
}
