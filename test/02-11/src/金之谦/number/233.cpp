#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int n,m,a[100010],f[100010],nex[100010];
inline int sqr(int x){return x*x;}
int t[400010];
inline void build(int l,int r,int nod){
	if(l==r){t[n]=1e9;return;}
	int mid=l+r>>1;
	build(l,mid,nod*2);build(mid+1,r,nod*2+1);
	t[nod]=min(t[nod*2],t[nod*2+1]);
}
inline void xg(int l,int r,int x,int nod){
	if(l==r){t[nod]=f[x];return;}
	int mid=l+r>>1;
	if(x<=mid)xg(l,mid,x,nod*2);else xg(mid+1,r,x,nod*2+1);
	t[nod]=min(t[nod*2],t[nod*2+1]);
}
inline int smin(int l,int r,int i,int j,int nod){
	if(l>=i&&r<=j)return t[nod];
	int mid=l+r>>1,ans=1e9;
	if(i<=mid)ans=min(ans,smin(l,mid,i,j,nod*2));
	if(j>mid)ans=min(ans,smin(mid+1,r,i,j,nod*2+1));
	return ans;
}
multiset<int>s;
multiset<int>::iterator it;
int main()
{
//	freopen("number.in","r",stdin);
//	freopen("number.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=n;i++){a[i]=read();f[i]=1e9;}
	for(int i=1;i<=m;i++)s.insert(0);
	build(0,n,1);xg(0,n,0,1);
	for(int i=1;i<=n;i++){
		s.erase(nex[a[i]]);s.insert(i);
		nex[a[i]]=i;int cnt=0;int p=sqrt(i);
		for(it=s.end(),it--;cnt<=p;cnt++){
			int r=*it;it--;int l=*it;
			cout<<l<<' '<<r<<" "<<cnt+1<<endl;
			int mi=smin(0,n,l,r-1,1);
			f[i]=min(f[i],mi+sqr(cnt+1));
			if(it==s.begin())break;
		}
		cout<<i<<' '<<f[i]<<endl;
		xg(0,n,i,1);
	}
	writeln(f[n]);
	return 0;
}
/*
11 3
1
2
1
3
2
2
3
3
3
3
3
*/
