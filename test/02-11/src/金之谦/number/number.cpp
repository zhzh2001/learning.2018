#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int n,m,a[100010],f[100010],nex[100010];
inline int sqr(int x){return x*x;}
multiset<int>s;
multiset<int>::iterator it;
int main()
{
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=n;i++){a[i]=read();f[i]=1e9;}
	s.insert(0);
	for(int i=1;i<=m;i++)s.insert(0);
	int rp=0;
	for(int i=1;i<=n;i++){
		if(!nex[a[i]])rp++;
		s.erase(s.find(nex[a[i]]));s.insert(i);
		nex[a[i]]=i;int cnt=0;int p=sqrt(i);
		for(it=s.end(),it--;cnt<p;cnt++){
			it--;int l=*it;
			f[i]=min(f[i],f[l]+sqr(cnt+1));
			if(cnt==rp-1){break;}
		}
	}
	writeln(f[n]);
	return 0;
}
/*
11 3
1
2
1
3
2
2
3
3
3
3
3
*/
