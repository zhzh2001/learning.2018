#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int n,m,s,t,cur[1000010],dist[1000010],vis[1000010];
int nedge=-1,p[2000010],c[2000010],nex[2000010],head[2000010];
inline void addedge(int a,int b,int v){
	p[++nedge]=b;c[nedge]=v;nex[nedge]=head[a];head[a]=nedge;
}
queue<int>q;
inline bool bfs(){
	q.push(s);memset(vis,0,sizeof vis);
	memset(dist,-1,sizeof dist);dist[s]=1;
	while(!q.empty()){
		int now=q.front();q.pop();vis[now]=1;
		for(int k=head[now];k>-1;k=nex[k])if(c[k]&&dist[p[k]]==-1){
			dist[p[k]]=dist[now]+1;q.push(p[k]);
		}
	}
	return dist[t]>-1;
}
inline int dfs(int x,int low){
	if(x==t)return low;
	int used=0,a;
	for(int k=cur[x];k>-1;k=nex[k])if(c[k]&&dist[p[k]]==dist[x]+1){
		a=dfs(p[k],min(c[k],low-used));
		if(a)c[k]-=a,c[k^1]+=a,used+=a;
		if(c[k])cur[x]=k;
		if(used==low)break;
	}
	if(!used)dist[x]=-1;
	return used;
}
inline int dinic(){
	int flow=0;
	while(bfs()){
		for(int i=s;i<=t;i++)cur[i]=head[i];
		flow+=dfs(s,1e9);
	}
	return flow;
}
int main()
{
	freopen("cover.in","r",stdin);
	freopen("cover.out","w",stdout);
	n=read();m=read();
	memset(nex,-1,sizeof nex);memset(head,-1,sizeof head);
	s=0;t=n+1000+1;
	for(int i=1;i<=n;i++){
		int mi=read(),ma=read();
		addedge(s,i,1);addedge(i,s,0);
		for(int j=mi;j<=ma;j++)addedge(i,n+j,1),addedge(n+j,i,0);
	}
	for(int i=1;i<=m;i++){
		int x=read(),v=read();
		addedge(x+n,t,v);addedge(t,x+n,0);
	}
	writeln(dinic());
	return 0;
}
