#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int nedge=0,p[400010],c[400010],nex[400010],head[400010];
int vis[100010],dist[100010],fa[100010],n,m;
vector<int>e[100010];
inline void addedge(int a,int b,int v){
	p[++nedge]=b;c[nedge]=v;nex[nedge]=head[a];head[a]=nedge;
}
struct ppap{int x,v;};
bool operator <(ppap a,ppap b){return a.v>b.v;}
priority_queue<ppap>q;
inline void dij(){
	memset(dist,0x3f,sizeof dist);dist[1]=0;
	q.push((ppap){1,0});
	while(!q.empty()){
		ppap ppa=q.top();q.pop();int now=ppa.x;
		if(vis[now])continue;vis[now]=1;
		for(int k=head[now];k;k=nex[k])if(!vis[p[k]]&&dist[p[k]]>dist[now]+c[k]){
			dist[p[k]]=dist[now]+c[k];fa[p[k]]=now;
			q.push((ppap){p[k],dist[p[k]]});
		}
	}
}
int deep[100010],s[100010],son[100010],sx[100010],top[100010],cnt=0;
inline void dfs(int x){
	deep[x]=deep[fa[x]]+1;s[x]=1;
	int S=e[x].size();
	for(int i=0;i<S;i++)if(e[x][i]!=fa[x]){
		int to=e[x][i];
		dfs(to);s[x]+=s[to];
		if(s[son[x]]<s[to])son[x]=to;
	}
}
inline void dfss(int x,int t){
	top[x]=t;int S=e[x].size();sx[x]=++cnt;
	if(son[x])dfss(son[x],t);
	for(int i=0;i<S;i++)if(e[x][i]!=fa[x]&&e[x][i]!=son[x])dfss(e[x][i],e[x][i]);
}
inline int lca(int x,int y){
	int fx=top[x],fy=top[y];
	while(fx!=fy){
		if(deep[fx]<deep[fy])swap(x,y),swap(fx,fy);
		x=fa[fx];fx=top[x];
	}
	if(deep[x]>deep[y])swap(x,y);
	return x;
}
int lt[400010],rt[400010],t[400010],add[400010];
inline void build(int l,int r,int nod){
	lt[nod]=l;rt[nod]=r;add[nod]=1e9;
	if(l==r){t[nod]=1e9;return;}
	int mid=l+r>>1;
	build(l,mid,nod*2);build(mid+1,r,nod*2+1);
	t[nod]=1e9;
}
inline void pushdown(int nod){
	if(add[nod]==1e9)return;
	if(lt[nod]!=rt[nod]){
		t[nod*2]=min(t[nod*2],add[nod]);t[nod*2+1]=min(t[nod*2+1],add[nod]);
		add[nod*2]=min(add[nod*2],add[nod]);add[nod*2+1]=min(add[nod*2+1],add[nod]);
	}add[nod]=1e9;
}
inline void sxg(int l,int r,int v,int nod){
	pushdown(nod);
	if(lt[nod]>=l&&rt[nod]<=r){t[nod]=min(t[nod],v);add[nod]=v;return;}
	int mid=lt[nod]+rt[nod]>>1;
	if(l<=mid)sxg(l,r,v,nod*2);
	if(r>mid)sxg(l,r,v,nod*2+1);
	t[nod]=min(t[nod*2],t[nod*2+1]);
}
inline void xg(int x,int y,int v){
	int fx=top[x],fy=top[y];
	while(fx!=fy){
		if(deep[fx]<deep[fy])swap(x,y),swap(fx,fy);
		sxg(sx[fx],sx[x],v,1);
		x=fa[fx];fx=top[x];
	}
	if(deep[x]>deep[y])swap(x,y);
	sxg(sx[x]+1,sx[y],v,1);
}
inline int smin(int x,int nod){
	pushdown(nod);
	if(lt[nod]==rt[nod])return t[nod];
	int mid=lt[nod]+rt[nod]>>1;
	if(x<=mid)return smin(x,nod*2);else return smin(x,nod*2+1);
}
int main()
{
	freopen("cow.in","r",stdin);
	freopen("cow.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=m;i++){
		int x=read(),y=read(),v=read();
		addedge(x,y,v);addedge(y,x,v);
	}
	dij();
	for(int i=2;i<=n;i++)e[fa[i]].push_back(i);
	dfs(1);dfss(1,1);
	build(1,n,1);
	for(int i=1;i<=n;i++)
		for(int k=head[i];k;k=nex[k])if(fa[i]!=p[k]&&fa[p[k]]!=i){
			int LCA=lca(i,p[k]);
			xg(LCA,p[k],dist[i]+dist[p[k]]+c[k]);
		}
	for(int i=2;i<=n;i++){
		int ans=smin(sx[i],1)-dist[i];
		if(ans+dist[i]>=1e9)ans=-1;
		writeln(ans);
	}
	return 0;
}
/*
4 5
1 2 2
1 3 6
3 4 3
3 2 2
2 4 2
*/
