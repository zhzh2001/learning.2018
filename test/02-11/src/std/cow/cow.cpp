#include<cstdio>
#include<vector>
#include<cstring>
#include<queue>
#include<algorithm>
#define pa pair<int ,int >
using namespace std;
inline int read(){int x=0,f=1; char ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int maxm=400005,maxn=100005;
struct edge{
	int link,next,val;
}e[maxm];
struct Edge{
	int u,v,w;
}E[maxm];
int n,m,head[maxn],tot;
inline void add(int u,int v,int w){
	e[++tot]=(edge){v,head[u],w}; head[u]=tot;
}
inline void insert(int u,int v,int w){
	add(u,v,w); add(v,u,w); 
}
inline void init(){
	n=read(); m=read();
	for (int i=1;i<=m;i++){
		int u=read(),v=read(),w=read(); insert(u,v,w);
		E[i]=(Edge){u,v,w};
	}
}
int pree[maxn],dis[maxn],pre[maxn],vis[maxn];
priority_queue<pa ,vector<pa >, greater<pa > > heap;
inline void dijkstra(){
	memset(dis,127/3,sizeof(dis)); dis[1]=0;
	heap.push(make_pair(0,1));
	while (!heap.empty()){
		int u=heap.top().second;
		heap.pop();
		if (vis[u]) continue;
		vis[u]=1;
		for (int i=head[u];i;i=e[i].next){
			int v=e[i].link;
			if (dis[v]>dis[u]+e[i].val){
				pre[v]=u;
				pree[v]=i;
				dis[v]=dis[u]+e[i].val;
				if (!vis[v]){
					heap.push(make_pair(dis[v],v));
				} 
			}
		}
	}
}
int dep[maxn],heavy[maxn],top[maxn],size[maxn],fa[maxn];
int num[maxn],pos[maxn],tim;
void dfs1(int u,int f){
	fa[u]=f; dep[u]=dep[f]+1; size[u]=1;
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=f){
			dfs1(v,u); size[u]+=size[v]; 
			if (!heavy[u]||size[v]>size[heavy[u]]){
				heavy[u]=v;
			}
		}
	}
}
void dfs2(int u,int tp){
	num[u]=++tim; pos[tim]=u; top[u]=tp;
	if (heavy[u]){
		dfs2(heavy[u],tp);
	}
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=fa[u]&&v!=heavy[u]){
			dfs2(v,v);
		}
	}
}
const int inf=1e9;
struct node{
	int val,lazy;
}a[maxn*8];
void build(int k,int l,int r){
	a[k].lazy=inf; a[k].val=inf;
	if (l==r){
		return;
	}
	int mid=(l+r)>>1;
	build(k<<1,l,mid); build(k<<1|1,mid+1,r);
}
inline void prepare(){
	dijkstra(); tot=0;
	memset(head,0,sizeof(head));
	for (int i=1;i<=n;i++){
		insert(i,pre[i],0);
	}
	dfs1(1,0); dfs2(1,1); build(1,1,n); 
}
inline void change(int k,int w){
	a[k].lazy=min(a[k].lazy,w);
	a[k].val=min(a[k].val,w);
}
inline void pushdown(int k){
	if (a[k].lazy!=inf){
		change(k<<1,a[k].lazy); 
		change(k<<1|1,a[k].lazy);
		a[k].lazy=inf;
	}
}
void update(int k,int l,int r,int x,int y,int w){
	pushdown(k);	
	if (l==x&&r==y){
		change(k,w);
		return;
	}
	int mid=(l+r)>>1;
	if (mid>=y){
		update(k<<1,l,mid,x,y,w);
	}else{
		if (mid<x){
			update(k<<1|1,mid+1,r,x,y,w);
		}else{
			update(k<<1,l,mid,x,mid,w);
			update(k<<1|1,mid+1,r,mid+1,y,w);
		}
	}
}
int query(int k,int l,int r,int x){
	if (l==r){
		return a[k].val;
	}
	pushdown(k);
	int mid=(l+r)>>1;
	if (mid>=x){
		return query(k<<1,l,mid,x);
	}else{
		return query(k<<1|1,mid+1,r,x);
	}
}
inline int asklca(int u,int v){
	while (top[u]!=top[v]){
		if (dep[top[u]]<dep[top[v]]) swap(u,v);
		u=fa[top[u]];
	}
	if (num[u]>num[v]){
		swap(u,v);
	}
	return u;
}
inline void make(int u,int v,int w){
	while (top[u]!=top[v]){
		update(1,1,n,num[top[u]],num[u],w);
		u=fa[top[u]];
	}
	if (num[v]+1<=num[u]) update(1,1,n,num[v]+1,num[u],w);
}
bool mark[maxm];
inline void solve(){
	prepare();
	for (int i=1;i<=n;i++){
		mark[(pree[i]+1)/2]=1;
	}
	for (int i=1;i<=m;i++){
		if (!mark[i]) {
			int lca=asklca(E[i].u,E[i].v);
			make(E[i].u,lca,dis[E[i].u]+dis[E[i].v]+E[i].w);
			make(E[i].v,lca,dis[E[i].u]+dis[E[i].v]+E[i].w);
		}
	}
	for (int i=2;i<=n;i++){
		int q=query(1,1,n,num[i]);
		if (q!=inf) writeln(q-dis[i]);
			else puts("-1");
	}
}
int main(){
	freopen("cow.in", "r", stdin);
	freopen("cow.out", "w", stdout);
	init();
	solve();
	return 0;
}
