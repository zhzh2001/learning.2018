#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
const int maxn=40005;
int n,m,a[maxn];
inline void init(){
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++) scanf("%d",&a[i]);
}
int pos[maxn],dp[maxn],cnt[maxn],pre[maxn];
inline void solve(){
	memset(dp,127/3,sizeof(dp));
	int lim=(int )sqrt(n); dp[0]=0;
	for (int i=1;i<=n;i++){
		for (int j=1;j<=lim;j++){
			if (pre[a[i]]<=pos[j]){
				cnt[j]++;
			}
		}
		pre[a[i]]=i;
		for (int j=1;j<=lim;j++){
			if (cnt[j]>j){
				int x=pos[j]+1;
				while (pre[a[x]]>x) x++;
				pos[j]=x; cnt[j]--;
			}
		}
		for (int j=1;j<=lim;j++){
			dp[i]=min(dp[pos[j]]+j*j,dp[i]);
		}
	}
	printf("%d\n",dp[n]);
}
int main(){
	freopen("number.in", "r", stdin);
	freopen("number.out", "w", stdout);
	init();
	solve();
	return 0;
}
