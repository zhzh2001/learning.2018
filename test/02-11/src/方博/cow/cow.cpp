#include<bits/stdc++.h>
#include<queue>
#define pa pair<int,int>
#define mk make_pair
#define ll long long
inline ll read(){ll x=0,f=1;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';return x*f;}
inline void write(ll x){if(x<0)x=-x,putchar('-');if(x>9)write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
using namespace std;
const int N=100005;
const int M=200005;
int n,m,t;
int dis[N],p[N],a[N];
int dist[N],ans[N];
int head[N],nxt[M*2],tail[M*2],e[M*2];
bool cant[M*2],vis[N];
inline void addto(int x,int y,int z){nxt[++t]=head[x];head[x]=t;tail[t]=y;e[t]=z;}
inline void dij(int s)
{
	memset(dis,127/3,sizeof(dis));
	dis[s]=0;
	priority_queue<pa >q;
	q.push(mk(0,s));
	while(!q.empty()){
		while(!q.empty()&&q.top().first>dis[q.top().second])q.pop();
		if(q.empty())break;
		pa k=q.top();q.pop();
		int x=k.second;
		for(int i=head[x];i;i=nxt[i])
			if(k.first+e[i]<dis[tail[i]]){
				p[tail[i]]=i;
				dis[tail[i]]=k.first+e[i];
				q.push(mk(dis[tail[i]],tail[i]));
			}
	}
}
inline int getans(int s,int t)
{
	memset(dist,127/3,sizeof(dist));
	memset(vis,false,sizeof(vis));
	dist[s]=0;
	priority_queue<pa >q;
	q.push(mk(0,s));
	for(;!q.empty();){
		while(!q.empty()&&q.top().first>dist[q.top().second])q.pop();
		if(q.empty())break;
		pa k=q.top();q.pop();
		int x=k.second;
		for(int i=head[x];i;i=nxt[i])
			if(!cant[i]&&k.first+e[i]<dist[tail[i]]){
				dist[tail[i]]=k.first+e[i];
				q.push(mk(dist[tail[i]],tail[i]));
			}
	}
//	cout<<s<<':'<<t<<endl;
//	for(int i=1;i<=n;i++)
//		cout<<' '<<dist[i]<<endl;
	return dist[t];
}
int main()
{
	freopen("cow.in","r",stdin);
	freopen("cow.out","w",stdout);
	n=read();m=read();
	if(n>3000){puts("-1");return 0;}
	t=1;
	for(int i=1;i<=m;i++){
		int x,y,z;
		x=read();y=read();z=read();
		addto(x,y,z);
		addto(y,x,z);
	}
	dij(1);
	for(int i=2;i<=n;i++){
		cant[p[i]]=true;
		cant[p[i]^1]=true;
		ans[i]=getans(1,i);
		cant[p[i]]=false;
		cant[p[i]^1]=false;
	}
	for(int i=2;i<=n;i++)
		if(ans[i]==707406378){puts("-1");return 0;}
	for(int i=2;i<=n;i++)
		writeln(ans[i]);
}
