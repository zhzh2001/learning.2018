#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read(){ll x=0,f=1;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';return x*f;}
inline void write(ll x){if(x<0)x=-x,putchar('-');if(x>9)write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
const int N=40005;
int a[N],b[N];
int n,m;
int f[N],ans,t;
int main()
{
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=n;i++)a[i]=read();
	int l=0;
	for(int i=1;i<=n;i++)
		if(a[i]!=a[i-1])a[++l]=a[i];
	n=l;memset(f,127/3,sizeof(f));f[0]=0;
	for(int k=sqrt(n);k>=1;k--){
		l=1;t=0;
		for(int i=1;i<=n;i++){
			b[a[i]]++;if(b[a[i]]==1)t++;
			while(t>k){if(b[a[l]]==1)t--;b[a[l]]--,l++;}
			f[i]=min(f[i],f[l-1]+t*t);
		}
		while(l<=n)b[a[l]]--,l++;
	}
	for(int k=1;k<=n;k++){
		l=1;t=0;
		for(int i=1;i<=n;i++){
			b[a[i]]++;if(b[a[i]]==1)t++;
			while(t>k){if(b[a[l]]==1)t--;b[a[l]]--,l++;}
			f[i]=min(f[i],f[l-1]+t*t);
		}
		while(l<=n)b[a[l]]--,l++;
	}
	writeln(f[n]);
}
