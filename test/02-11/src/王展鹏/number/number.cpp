#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<set>
#include<map>
#include<queue>
using namespace std;
typedef long long LL;
#define sqr(x) ((x)*(x))
#define mp make_pair
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(int a){
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(int a){
    if(a<0){
    	a=-a; putchar('-');
	}
	write(a); puts("");
}
const int N=40005;
int n,m,ans,zs[500],dp[N],last[N];
int main(){
	freopen("number.in","r",stdin); freopen("number.out","w",stdout);
	n=read(); m=read(); int block=sqrt(n); memset(last,-1,sizeof(last)); zs[0]=1;
	for(int i=1;i<=n;i++){
		int t=read(); dp[i]=n+1;
		//cout<<zs[1]<<" "<<last[t]<<" "<<zs[0]<<endl;
		for(int i=block;i;i--)if(zs[i]>last[t])zs[i]=zs[i-1];
		//for(int i=0;i<=block;i++)cout<<zs[i]<<" "; puts("");
		zs[0]=i+1;
		for(int j=1;j<=block;j++){
			dp[i]=min(dp[zs[j]-1]+j*j,dp[i]);
		}
		last[t]=i;
	}
	cout<<dp[n]<<endl;
}

