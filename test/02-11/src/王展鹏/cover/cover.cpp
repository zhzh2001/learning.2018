#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<set>
#include<map>
#include<queue>
using namespace std;
typedef int LL;
#define sqr(x) ((x)*(x))
#define mp make_pair
inline int read(){
	int x = 0; char ch = getchar(); bool positive = 1;
	for (; !isdigit(ch); ch = getchar())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = getchar())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(int a){
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(int a){
    if(a<0){
    	a=-a; putchar('-');
	}
	write(a); puts("");
}
inline bool cmp(pair<int,int> a,pair<int,int> b){
	return a.second<b.second;
}
const int N=2505;
int cow,n,vis[N],ans,s1,s2;
pair<int,int> a[N],b[N];
int main(){
	freopen("cover.in","r",stdin); freopen("cover.out","w",stdout);
    cow=read(); n=read(); 
    for(int i=1;i<=cow;i++){s1=read(); s2=read(); a[i]=mp(s1,s2);}
    for(int i=1;i<=n;i++){s1=read(); s2=read(); b[i]=mp(s1,s2);}
    sort(&a[1],&a[cow+1],cmp);
    sort(&b[1],&b[n+1]);
    for(int i=1;i<=cow;i++){
    	int flag=0;
    	for(int j=1;j<=n;j++)if(b[j].second&&b[j].first>=a[i].first&&b[j].first<=a[i].second){
    		flag=1; b[j].second--; break;
		}
		ans+=flag;
	}
	writeln(ans);
}
