#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<set>
#include<map>
#include<queue>
using namespace std;
typedef long long LL;
#define sqr(x) ((x)*(x))
#define mp make_pair
inline int read(){
    int x = 0; char ch = getchar(); bool positive = 1;
    for (; !isdigit(ch); ch = getchar())    if (ch == '-')    positive = 0;
    for (; isdigit(ch); ch = getchar())    x = x * 10 + ch - '0';
    return positive ? x : -x;
}
inline void write(int a){
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(int a){
    if(a<0){
        a=-a; putchar('-');
    }
    write(a); puts("");
}
const int M=400005,N=100005;
int f[N],father[N],s3,nedge,dist[N],s1,s2,n,m,s,t,di[M],nextt[M],son[N],ed[M],n1,oo=1000000005,la[N],in[N],out[N],ti,v[M],an[N];
struct edge{
    int u,v,val;
}e[M];
inline bool cmp(edge a,edge b){
    return a.val<b.val;
}
struct data{
    int num,far,last;
}dui[M];
inline void up(int p){
    int i=p,j;
    while((j=i>>1)){ 
         if(dui[j].far>dui[i].far){
            data zs=dui[i]; dui[i]=dui[j]; dui[j]=zs; i=j;
        }else return;
    }
}
inline void down(int p){
    int i=p,j;
    while((j=i<<1)<=n1){
        if(j+1<=n1&&dui[j+1].far<dui[j].far)j++; 
        if(dui[j].far<dui[i].far){
            data zs=dui[i]; dui[i]=dui[j]; dui[j]=zs; i=j;
        }else return;
    }
}
inline void duidi(int s){
    n1=1;
    for(int i=1;i<=n;i++)dist[i]=oo; dist[s]=0;
    dui[1].far=0; dui[1].num=s;
    while(n1){
        int k=dui[1].num,k1=dui[1].far,k2=dui[1].last;  
        dui[1].far=oo; data zs=dui[1]; dui[1]=dui[n1]; dui[n1]=zs; down(1); n1--;
         if(k1!=dist[k])continue; la[k]=k2; v[k2]=1;
        for(int j=son[k];j;j=nextt[j])if(dist[ed[j]]>dist[k]+di[j]){
            dist[ed[j]]=dist[k]+di[j]; dui[++n1].num=ed[j]; dui[n1].far=dist[k]+di[j];  dui[n1].last=j;
            up(n1); 
        }
    }
    return;
}
inline void aedge(int a,int b,int c){
    nextt[++nedge]=son[a]; son[a]=nedge; ed[nedge]=b; di[nedge]=c;
}
inline int getfather(int x){
    return father[x]==x?x:father[x]=getfather(father[x]);
}
inline void dfs(int p){
    in[p]=++ti;
    for(int i=son[p];i;i=nextt[i])if(v[i]){dist[ed[i]]=dist[p]+di[i]; f[ed[i]]=p; dfs(ed[i]);}
    out[p]=++ti;
}
inline bool zu(int a,int b){
    return in[a]<=in[b]&&out[a]>=out[b];
}
inline void cao(int p,int x,int len){
    for(int i=getfather(p);!zu(i,x);i=t){
        t=getfather(f[i]);
        father[i]=t;
        an[i]=len-dist[i];
    }
}
const int DEBUG=0;
int main(){
	freopen("cow.in","r",stdin); freopen("cow.out","w",stdout);
    n=read(); m=read();
    for(int i=1;i<=n;i++)father[i]=i;
    for(int i=1;i<=m;i++){
        s1=read(),s2=read(),s3=read();
        aedge(s1,s2,s3); aedge(s2,s1,s3);
    }
	m=0; 
    duidi(1);
    dfs(1);
    for(int i=1;i<=n;i++){
        for(int j=son[i];j;j=nextt[j])if(!v[j]&&!v[(1^(j-1))+1]&&i<ed[j]){
            e[++m]=(edge){i,ed[j],di[j]+dist[i]+dist[ed[j]]};
        }
    }
    sort(&e[1],&e[m+1],cmp);
    for(int i=1;i<=n;i++)an[i]=-1;
    for(int i=1;i<=m;i++){
        cao(e[i].u,e[i].v,e[i].val);
        cao(e[i].v,e[i].u,e[i].val);
    }
    for(int i=2;i<=n;i++)writeln(an[i]);
}
