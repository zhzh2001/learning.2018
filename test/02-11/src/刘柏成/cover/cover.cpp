#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
    freopen("cover.in","r",stdin);
    freopen("cover.out","w",stdout);
    buf[fread(buf,1,MAXSIZE,stdin)]='\0';
    bufpos=0;
}
#if NEG
int readint(){
    bool isneg;
    int val=0;
    for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
    bufpos+=(isneg=buf[bufpos]=='-');
    for(;isdigit(buf[bufpos]);bufpos++)
        val=val*10+buf[bufpos]-'0';
    return isneg?-val:val;
}
#else
int readint(){
    int val=0;
    for(;!isdigit(buf[bufpos]);bufpos++);
    for(;isdigit(buf[bufpos]);bufpos++)
        val=val*10+buf[bufpos]-'0';
    return val;
}
#endif
char readchar(){
    for(;isspace(buf[bufpos]);bufpos++);
    return buf[bufpos++];
}
int readstr(char* s){
    int cur=0;
    for(;isspace(buf[bufpos]);bufpos++);
    for(;!isspace(buf[bufpos]);bufpos++)
        s[cur++]=buf[bufpos];
    s[cur]='\0';
    return cur;
}
struct seg{
    int l,r;
    bool operator <(const seg& rhs)const{
        return l<rhs.l;
    }
}a[3001];
struct cream{
    int p,c;
    bool operator <(const cream& rhs)const{
        return p<rhs.p;
    }
}b[3001];
set<int> s;
int main(){
    init();
    int n=readint(),m=readint();
    for(int i=1;i<=n;i++)
        a[i].l=readint(),a[i].r=readint();
    sort(a+1,a+n+1);
    for(int i=1;i<=m;i++)
        b[i].p=readint(),b[i].c=readint();
    sort(b+1,b+m+1);
    int now=0,ans=0;
    for(int i=1;i<=m;i++){
        while(now<n && a[now+1].l<=b[i].p)
            s.insert(a[++now].r);
        while(b[i].c--){
            if (s.empty())
                break;
            ans++;
            s.erase(s.begin());
        }
    }
    printf("%d",ans);
}