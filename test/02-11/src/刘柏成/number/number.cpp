#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
const int INF=0x3f3f3f3f;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
    freopen("number.in","r",stdin);
    freopen("number.out","w",stdout);
    buf[fread(buf,1,MAXSIZE,stdin)]='\0';
    bufpos=0;
}
#if NEG
int readint(){
    bool isneg;
    int val=0;
    for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
    bufpos+=(isneg=buf[bufpos]=='-');
    for(;isdigit(buf[bufpos]);bufpos++)
        val=val*10+buf[bufpos]-'0';
    return isneg?-val:val;
}
#else
int readint(){
    int val=0;
    for(;!isdigit(buf[bufpos]);bufpos++);
    for(;isdigit(buf[bufpos]);bufpos++)
        val=val*10+buf[bufpos]-'0';
    return val;
}
#endif
char readchar(){
    for(;isspace(buf[bufpos]);bufpos++);
    return buf[bufpos++];
}
int readstr(char* s){
    int cur=0;
    for(;isspace(buf[bufpos]);bufpos++);
    for(;!isspace(buf[bufpos]);bufpos++)
        s[cur++]=buf[bufpos];
    s[cur]='\0';
    return cur;
}
int a[40003],qwq[40003];
int now[40003],dp[40003];
int nxt[40003],lst[40003];
void del(int x){
    nxt[lst[x]]=nxt[x];
    lst[nxt[x]]=lst[x];
}
int main(){
    init();
    int n=readint(),m=readint(),k=sqrt(n)+0.5;
    for(int i=1;i<=n;i++)
        a[i]=readint();
    for(int i=1;i<=n;i++){
        qwq[i]=now[a[i]];
        now[a[i]]=i;
    }
    int tat=0;
    for(int i=1;i<=n;i++){
        lst[i]=i-1;
        nxt[i-1]=i;
        dp[i]=INF;
        // fprintf(stderr,"qwq[%d]=%d\n",i,qwq[i]);
        if (qwq[i])
            del(qwq[i]);
        else tat++;
        dp[i]=tat*tat;
        int j=lst[i];
        for(int o=1;o<=k;o++){
            if (!j)
                break;
            dp[i]=min(dp[i],o*o+dp[j]);
            // fprintf(stderr,"on %d %d %d\n",i,j,o);
            j=lst[j];
        }
        // fprintf(stderr,"dp[%d]=%d\n",i,dp[i]);
    }
    printf("%d",dp[n]);
}