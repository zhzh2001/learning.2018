#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
const int INF=0x3f3f3f3f;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
    freopen("cow.in","r",stdin);
    freopen("cow.out","w",stdout);
    buf[fread(buf,1,MAXSIZE,stdin)]='\0';
    bufpos=0;
}
#if NEG
int readint(){
    bool isneg;
    int val=0;
    for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
    bufpos+=(isneg=buf[bufpos]=='-');
    for(;isdigit(buf[bufpos]);bufpos++)
        val=val*10+buf[bufpos]-'0';
    return isneg?-val:val;
}
#else
int readint(){
    int val=0;
    for(;!isdigit(buf[bufpos]);bufpos++);
    for(;isdigit(buf[bufpos]);bufpos++)
        val=val*10+buf[bufpos]-'0';
    return val;
}
#endif
char readchar(){
    for(;isspace(buf[bufpos]);bufpos++);
    return buf[bufpos++];
}
int readstr(char* s){
    int cur=0;
    for(;isspace(buf[bufpos]);bufpos++);
    for(;!isspace(buf[bufpos]);bufpos++)
        s[cur++]=buf[bufpos];
    s[cur]='\0';
    return cur;
}
const int maxn=100002;
const int maxm=400002;
/*struct mypq{
    priority_queue<int,vector<int>,greater<int> > q1,q2;
    void push(int x){
        q1.push(x);
    }
    void erase(int x){
        q2.push(x);
    }
    int top(){
        while(!q1.empty() && !q2.empty() && q1.top()==q2.top()){
            q1.pop();
            q2.pop();
        }
        return q1.empty()?INF:q1.top();
    }
};*/
struct segtree{
    int n;
    int minv[maxn*4];
    int ans[maxn];
    int ul,ur,uv;
    void init(int n){
        memset(minv,0x3f,sizeof(minv));
        this->n=n;
    }
    void update(int o,int l,int r){
        if (ul<=l && ur>=r){
            minv[o]=min(minv[o],uv);
            return;
        }
        if (uv>minv[o])
            return;
        int mid=(l+r)/2;
        if (ul<=mid)
            update(o*2,l,mid);
        if (ur>mid)
            update(o*2+1,mid+1,r);
    }
    void modify(int l,int r,int v){
        ul=l,ur=r,uv=v;
        update(1,1,n);
    }
    void dfs(int o,int l,int r){
        if (l==r){
            ans[l]=minv[o];
            return;
        }
        int mid=(l+r)/2;
        minv[o*2]=min(minv[o*2],minv[o]);
        minv[o*2+1]=min(minv[o*2+1],minv[o]);
        dfs(o*2,l,mid);
        dfs(o*2+1,mid+1,r);
    }
}t;
struct graph{
    int n,m;
    struct edge{
        int from,to,cost,next;
    }e[maxm];
    int first[maxn];
    void addedge(int from,int to,int cost){
        e[++m]=(edge){from,to,cost,first[from]};
        first[from]=m;
    }
};
struct sssp_graph : graph{
    struct node{
        int u,d;
        bool operator <(const node& rhs)const{
            return d>rhs.d;
        }
    };
    priority_queue<node> q;
    int dist[maxn];
    bool vis[maxn];
    int p[maxn];
    void dijkstra(int s){
        memset(dist,0x3f,(n+1)*4);
        memset(vis,0,n+1);
        dist[s]=0;
        q.push((node){s,0});
        while(!q.empty()){
            int u=q.top().u;
            q.pop();
            if (vis[u])
                continue;
            vis[u]=1;
            for(int i=first[u];i;i=e[i].next){
                int v=e[i].to;
                if (dist[v]>dist[u]+e[i].cost){
                    dist[v]=dist[u]+e[i].cost;
                    p[v]=u;
                    q.push((node){v,dist[v]});
                }
            }
        }
    }
}g;
struct tree : graph{
    int fa[maxn],sz[maxn],dep[maxn],son[maxn],id[maxn],top[maxn];
    void dfs(int u){
        sz[u]=1;
        for(int i=first[u];i;i=e[i].next){
            int v=e[i].to;
            if (sz[v])
                continue;
            fa[v]=u;
            dep[v]=dep[u]+1;
            dfs(v);
            sz[u]+=sz[v];
            if (sz[v]>sz[son[u]])
                son[u]=v;
        }
    }
    int cl;
    void dfs2(int u,int tp){
        top[u]=tp;
        id[u]=++cl;
        if (son[u])
            dfs2(son[u],tp);
        for(int i=first[u];i;i=e[i].next){
            int v=e[i].to;
            if (!id[v])
                dfs2(v,v);
        }
    }
    void prepare(){
        dfs(1);
        dfs2(1,1);
        t.init(n);
    }
    void update(int u,int v,int w){
        // printf("update %d %d %d\n",u,v,w);
        for(;top[u]!=top[v];u=fa[top[u]]){
            if (dep[top[u]]<dep[top[v]])
                swap(u,v);
            t.modify(id[top[u]],id[u],w);
        }
        if (dep[u]>dep[v])
            swap(u,v);
        t.modify(id[u]+1,id[v],w);
    }
}g2;
int main(){
    init();
    int n=readint(),m=readint();
    g.n=g2.n=n;
    for(int i=1;i<=m;i++){
        int u=readint(),v=readint(),w=readint();
        g.addedge(u,v,w);
        g.addedge(v,u,w);
    }
    g.dijkstra(1);
    for(int i=2;i<=n;i++){
        // printf("on %d %d %d\n",g.p[i],i,g.dist[i]);
        g2.addedge(g.p[i],i,g.dist[i]-g.dist[g.p[i]]);
    }
    g2.prepare();
    for(int i=1;i<=g.m;i++)
        if (i%2==1){
            int u=g.e[i].from,v=g.e[i].to;
            // printf("%d %d %d\n",u,v,g.e[i].cost);
            if (g.dist[u]!=g.dist[v]+g.e[i].cost && g.dist[v]!=g.dist[u]+g.e[i].cost)
                g2.update(u,v,g.dist[u]+g.dist[v]+g.e[i].cost);
        }
    t.dfs(1,1,n);
    for(int i=2;i<=n;i++)
        printf("%d\n",t.ans[g2.id[i]]==INF?-1:t.ans[g2.id[i]]-g.dist[i]);
    // printf("%d",sizeof(t)+sizeof(g)+sizeof(g2));
}