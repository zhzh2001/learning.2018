#include <cstdio>
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
#define LL long long
using namespace std;

inline int read() {
	int x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch=getchar(); }
	while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
	return x * f;
}

const int N = 40011; 
const LL INF = 1e15; 
int n, m; 
int a[N],ton[N];
LL f[N],sum;  
inline LL min(LL x,LL y) {
	return x<y ? x:y; 
}

int main() {
	freopen("number.in","r",stdin); 
	freopen("number.out","w",stdout); 
	n = read(); m = read(); 
	For(i, 1, n) a[i] = read(); 
	For(i, 1, n) f[i] = INF; 
	f[1] = 1; f[0] = 0; 
	For(i, 2, n) {
		For(j, 0, 40000) ton[j] = 0; 
		sum = 0; 
		Dow(j, i-1, 0) {
			if(!ton[a[j+1]]) ton[a[j+1]]=1, ++sum; 
			f[i] = min(f[i], f[j]+sum*sum); 
		}
	}
	printf("%lld\n",f[n]); 
	return 0; 
}

/*

11 3
1
2
1
3
2
2
3
3
3
3
3


*/


