#include <bits/stdc++.h>
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
#define LL long long
using namespace std;

inline int read() {
	int x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch=getchar(); }
	while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
	return x * f;
}

const int N = 3001; 
struct segment{
	int l, r; 
}seg[N];
struct point{
	int val, num; 
}a[N];
int C, n, ans; 

inline bool cmp_val(point a, point b) {
	return a.val < b.val; 
} 
inline bool cmp_l(segment a, segment b) {
	return a.l < b.l; 
} 
int main() {
	freopen("cover.in","r",stdin); 
	freopen("cover.out","w",stdout); 
	C = read(); n = read();  
	For(i, 1, C) {
		seg[i].l = read(), seg[i].r = read(); 
	} 
	sort(seg+1, seg+C+1, cmp_l); 
	For(i, 1, n) {
		a[i].val = read(), a[i].num = read(); 
	}
	sort(a+1, a+n+1, cmp_val); 
	
	int x = 1, i = 1; 
	while(i<=C) {
		while(x<=n && i<=C && a[x].num && seg[i].l<=a[x].val) {
			if(a[x].val<=seg[i].r) --a[x].num, ++ans; 
			++i;  
		}
		if(x > n) break; 
		if(i > C) break; 
		++x; if(x > n) break;
	}
	printf("%d\n", ans); 
	return 0; 
}

/*

3 2
3 10
2 6
2 5
1 2
4 2




*/


