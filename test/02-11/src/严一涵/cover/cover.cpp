#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstring>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline int read(){int a=0,b=1;char dd;while(!isdigit(c)&&c!='-')dd;
	if(c=='-'){b=-b;dd;}while(isdigit(c)){a=a*10+c-'0';dd;}return a*b;}
#undef dd
struct st{int a,b;}a[2503],b[2503];
bool operator<(st a,st b){return a.a!=b.a?a.a<b.a:a.b<b.b;}
int n,m,ans=0;
int main(){
	freopen("cover.in","r",stdin);
	freopen("cover.out","w",stdout);
	n=read();
	m=read();
	for(int i=1;i<=n;i++){
		a[i].a=read();
		a[i].b=read();
	}
	sort(a+1,a+n+1);
	for(int i=1;i<=m;i++){
		b[i].a=read();
		b[i].b=read();
	}
	sort(b+1,b+m+1);
	for(int i=1,j=1;i<=m&&j<=n;i++){
		for(;b[i].b>0&&j<=n;){
			if(a[j].a>b[i].a)break;
			if(a[j].b>=b[i].a){
				ans++;
				b[i].b--;
			}
			j++;
		}
	}
	printf("%d\n",ans);
	return 0;
}
