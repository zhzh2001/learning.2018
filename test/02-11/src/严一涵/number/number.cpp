#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstring>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline int read(){int a=0,b=1;char dd;while(!isdigit(c)&&c!='-')dd;
	if(c=='-'){b=-b;dd;}while(isdigit(c)){a=a*10+c-'0';dd;}return a*b;}
#undef dd
int a[40003],cnt[40003],va[40003],f[40003],q[40003],n,x,y,z;
inline int sqr(int v){return v*v;}
int main(){
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	n=read();
	read();
	x=0;
	for(int i=1;i<=n;i++){
		y=read();
		if(x==0||y!=a[x]){
			a[++x]=y;
		}
	}
	n=x;
	y=0;
	f[0]=0;
	for(int i=1;i<=n;i++){
		x=a[i];
		cnt[x]++;
		if(cnt[x]==1)y++;
		z=0;
		f[i]=f[i-1]+1;
		for(int j=i;j>0;j--){
			va[a[j]]++;
			if(va[a[j]]==1){
				q[++z]=a[j];
			}
			f[i]=min(f[i],f[j-1]+z*z);
		}
		for(int j=1;j<=z;j++)va[q[j]]=0;
	}
	printf("%d\n",f[n]);
	return 0;
}
