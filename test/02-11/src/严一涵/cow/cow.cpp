#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstring>
using namespace std;
typedef long long LL;
const int oo=1<<29;
#define dd c=getchar()
inline int read(){int a=0,b=1;char dd;while(!isdigit(c)&&c!='-')dd;
	if(c=='-'){b=-b;dd;}while(isdigit(c)){a=a*10+c-'0';dd;}return a*b;}
#undef dd
struct edge{int a,b,c;edge(){}edge(int x,int y,int z){a=x;b=y;c=z;}}e[400003];
int lis[3003],dis[3003],n,m,en[3003],fr[3003],et=0,x,y,z;
bool vis[3003];
void adde(int a,int b,int c){
	e[++et]=edge(en[a],b,c);
	en[a]=et;
}
void spfa(int A,int B,bool p){
	int l=0,r=0,x,y,w;
	lis[0]=1;
	vis[1]=1;
	for(int i=1;i<=n;i++)dis[i]=oo;
	dis[1]=0;
	for(;l<=r;l++){
		x=lis[l%n];
		for(int i=en[x];i>0;i=e[i].a){
			y=e[i].b;
			w=e[i].c;
			if((x==A&&y==B)||(x==B&&y==A))continue;
			if(dis[x]+w<dis[y]){
				dis[y]=dis[x]+w;
				if(p)fr[y]=x;
				if(!vis[y]){
					vis[y]=1;
					lis[(++r)%n]=y;
				}
			}
		}
		vis[x]=0;
	}
}
int main(){
	freopen("cow.in","r",stdin);
	freopen("cow.out","w",stdout);
	n=read();
	m=read();
	for(int i=1;i<=m;i++){
		x=read();
		y=read();
		z=read();
		adde(x,y,z);
		adde(y,x,z);
	}
	spfa(0,0,1);
	for(int i=2;i<=n;i++){
		spfa(fr[i],i,0);
		if(dis[i]>=oo)puts("-1");
		else printf("%d\n",dis[i]);
	}
	return 0;
}
