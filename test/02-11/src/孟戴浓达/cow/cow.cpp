//#include<iostream>
#include<stdio.h>
#include<algorithm>
#include<queue>
#include<math.h>
#include<string.h>
#include<fstream>
using namespace std;
ifstream fin("cow.in");
ofstream fout("cow.out");
int n,m;
int a[200003],b[200003];
int to[200003],head[100003],nxt[200003],val[200003],tot;
long long dis1[100003],dis2[100003],from1[100003],from2[100003];
long long vis[100003];
inline void add(int u,int v,int f){
	to[++tot]=v,val[tot]=f,nxt[tot]=head[u],head[u]=tot;
}
queue<int> q;
inline void spfa(){
	for(int i=1;i<=n;i++){
		dis1[i]=9999999999;
		dis2[i]=9999999999;
		vis[i]=0;
	}
	from1[1]=-1;
	dis1[1]=0;
	vis[1]=1;
	q.push(1);
	while(!q.empty()){
		int x=q.front();
		q.pop();
		for(int i=head[x];i;i=nxt[i]){
			if(from1[x]==to[i]){
				continue;
			}
			if(dis1[to[i]]>dis1[x]+val[i]&&from1[to[i]]!=x){
				dis2[to[i]]=dis1[to[i]];
				from2[to[i]]=from1[to[i]];
				dis1[to[i]]=dis1[x]+val[i];
				from1[to[i]]=x;
				if(!vis[to[i]]){
					q.push(to[i]);
					vis[to[i]]=1;
				}
			}else if(dis1[to[i]]==dis1[x]+val[i]&&from1[to[i]]!=x){
				from2[to[i]]=x;
				dis2[to[i]]=dis1[x]+val[i];
				if(!vis[to[i]]){
					q.push(to[i]);
					vis[to[i]]=1;
				}
			}else if(dis2[to[i]]>dis1[x]+val[i]&&from1[to[i]]!=x){
				from2[to[i]]=x;
				dis2[to[i]]=dis1[x]+val[i];
				if(!vis[to[i]]){
					q.push(to[i]);
					vis[to[i]]=1;
				}
			}
		}
		vis[x]=0;
	}
}
long long dis[100003],from[100003],s,t;
inline int spfa2(){
	for(int i=1;i<=n;i++){
		dis[i]=9999999999,vis[i]=0;
	}
	dis[1]=0,vis[1]=1;
	q.push(1);
	while(!q.empty()){
		int x=q.front();
		q.pop();
		for(int i=head[x];i;i=nxt[i]){
			if(dis[to[i]]>dis[x]+val[i]){
				dis[to[i]]=dis[x]+val[i];
				from[to[i]]=x;
				if(!vis[to[i]]){
					q.push(to[i]);
					vis[to[i]]=1;
				}
			}
		}
		vis[x]=0;
	}
}
long long dis3[100003];
inline int spfa3(){
	for(int i=1;i<=n;i++){
		dis3[i]=9999999999,vis[i]=0;
	}
	dis3[1]=0,vis[1]=1;
	q.push(1);
	while(!q.empty()){
		int x=q.front();
		q.pop();
		for(int i=head[x];i;i=nxt[i]){
			if(!((to[i]==t&&x==s)||(to[i]==s&&x==t))){
				if(dis3[to[i]]>dis3[x]+val[i]){
					dis3[to[i]]=dis3[x]+val[i];
					if(!vis[to[i]]){
						q.push(to[i]);
						vis[to[i]]=1;
					}
				}
			}
		}
		vis[x]=0;
	}
}
inline void baoli(){
	spfa2();
	for(int i=2;i<=n;i++){
		s=from[i],t=i;
		spfa3();
		if(dis3[i]==9999999999){
			fout<<"-1"<<endl;
		}else{
			fout<<dis3[i]<<endl;
		}
	}
}
int main(){
	fin>>n>>m;
	for(int i=1;i<=m;i++){
		int t;
		fin>>a[i]>>b[i]>>t;
		add(a[i],b[i],t);
		add(b[i],a[i],t);
	}
	if(n<=3000){
		baoli();
		return 0;
	}
	spfa();
	for(int i=2;i<=n;i++){
		if(dis2[i]==9999999999){
			fout<<"-1"<<endl;
			continue;
		}
		fout<<dis2[i]<<endl;
	}
	return 0;
}
/*

in:
4 5
1 2 2
1 3 6
3 4 3
3 2 2
2 4 2

out:
8
6
7

*/
