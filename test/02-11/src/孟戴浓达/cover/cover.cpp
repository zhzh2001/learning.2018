//#include<iostream>
#include<stdio.h>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
using namespace std;
ifstream fin("cover.in");
ofstream fout("cover.out");
int c,l,mnspf[2503],mxspf[2503],spf[2503],cov[2503];
int q[300003],dis[5004];
int head[5005],nxt[4500003],to[4500003],flow[4500003],cnt;
inline void add(int u,int v,int f){
	to[cnt]=v,flow[cnt]=f,nxt[cnt]=head[u],head[u]=cnt++;
	to[cnt]=u,flow[cnt]=0,nxt[cnt]=head[v],head[v]=cnt++;
}
inline int bfs(int s,int t){
    memset(dis,0,sizeof(dis));
    dis[s]=1;
    int h=0,w=1;
    q[h]=s;
    while(h<w){
        int x=q[h++];
        if(x==t){
        	return 1;
		}
        for(int i=head[x];i!=-1;i=nxt[i]){
            int v=to[i],f=flow[i];
            if(!dis[v]&&f){
                dis[v]=dis[x]+1;
                q[w++]=v;
        	}
        }
    }
    return 0;
}
int dfs(int u,int maxf,int t){
    if(u==t){
    	return maxf;
	}
    int ret=0;
    for(int i=head[u];i!=-1;i=nxt[i]){
        int v=to[i],f=flow[i];
        if(dis[u]+1==dis[v]&&f){
            int mn=min(maxf-ret,f);
            f=dfs(v,mn,t);
            flow[i]-=f;
            flow[i^1]+=f;
            ret+=f;
            if(ret==maxf)return ret;
        }
    }
    return ret;
}
inline int dinic(int s,int t){
    int ans=0;
    while(bfs(s,t)){
    	ans+=dfs(s,99999999,t);
	}
    return ans;
}
int main(){
	memset(head,-1,sizeof(head));
	fin>>c>>l;
	if(c==0||l==0){
		fout<<"0"<<endl;
		return 0;
	}
	for(int i=1;i<=c;i++){
		fin>>mnspf[i]>>mxspf[i];
	}
	for(int i=1;i<=l;i++){
		fin>>spf[i]>>cov[i];
	}
	for(int i=1;i<=c;i++){
		for(int j=1;j<=l;j++){
			if(spf[j]>=mnspf[i]&&spf[j]<=mxspf[i]){
				add(j+1,i+l+1,1);
			}
		}
	}
	for(int i=1;i<=l;i++){
		add(1,i+1,cov[i]);
	}
	for(int i=1;i<=c;i++){
		add(l+i+1,c+l+2,1);
	}
	fout<<dinic(1,c+l+2)<<endl;
	return 0;
}
/*

in:
3 2
3 10
2 6
2 5
6 2
4 2

out:
3

*/
