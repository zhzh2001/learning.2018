//#include<iostream>
#include<stdio.h>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
using namespace std;
ifstream fin("number.in");
ofstream fout("number.out");
long long n,m,num[40003];
long long val[2003][2003],dp[40003];
bool vis[40003];
inline void baoli(){
	for(int i=1;i<=n;i++){
		int tot=0;
		for(int j=i;j<=n;j++){
			if(!vis[num[j]]){
				tot++;
				vis[num[j]]=1;
			}
			val[i][j]=tot;
		}
		for(int j=1;j<=n;j++){
			vis[num[j]]=0;
		}
	}
	for(int i=1;i<=n;i++){
		for(int j=1;j<=i;j++){
			dp[i]=min(dp[i],dp[j-1]+val[j][i]*val[j][i]);
		}
	}
	fout<<dp[n]<<endl;
}
inline void biaosuan(){
	for(int i=1;i<=n;i++){
		int tot=0;
		for(int j=i;j>=max(1,i-1000);j--){
			if(!vis[num[j]]){
				vis[num[j]]=1;
				tot++;
			}
			dp[i]=min(dp[i],dp[j-1]+tot*tot);
		}
		for(int j=i;j>=max(1,i-1000);j--){
			vis[num[j]]=0;
		}
	}
	fout<<dp[n]<<endl;
}
int pre[40003],b[40003],a[40003],cnt[40003];
inline void biaosuan2(){
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			if(pre[a[i]]<=b[j])cnt[j]++;
		}
		pre[a[i]]=i;
		for(int j=1;j<=m;j++){
			if(cnt[j]>j){
				int t=b[j]+1;
				b[j]=pre[a[t]];cnt[j]--;
			}
		}
		for(int j=1;j<=m;j++){
			dp[i]=min(dp[i],dp[b[j]]+j*j);
		}
	}
	fout<<dp[n]<<endl;
}
int main(){
	fin>>n>>m;
	for(int i=1;i<=n;i++){
		fin>>num[i];
		dp[i]=999999999;
	}
	if(n<=2000){
		baoli();
		return 0;
	}else{
		biaosuan();
		return 0;
	}
	return 0;
}
/*

in:
11 3
1
2
1
3
2
2
3
3
3
3
3

out:
6

*/
