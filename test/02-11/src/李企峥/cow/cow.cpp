#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
#define pb push_back
using namespace std; 
vector<int>a[100100];
vector<int>b[100100];
ll ans[100100],fm[100100],f[100100],fm1[100100];
bool ff[100100];
ll n,m,h,t,xx,x,y,z;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
void spfa(int x,int y)
{
	For(i,1,n)ans[i]=1e9;
	h=0;t=1;f[t]=1;ans[1]=0;
	memset(ff,false,sizeof(ff));
	ff[1]=true;
	while(h!=t)
	{
		h%=100000;
		h++;
		xx=f[h];
		For(i,0,a[xx].size()-1)
		{
			if(x==xx&&y==a[xx][i])continue;
			if(x==a[xx][i]&&y==xx)continue;
			if(ff[a[xx][i]])
			{
				if(ans[a[xx][i]]>ans[i]+b[xx][i])
				{
					//ans1[a[xx][i]]=ans[a[xx][i]];
					fm[a[xx][i]]=i;
					ans[a[xx][i]]=ans[i]+b[xx][i];
				}
				/*else
				{
					if(ans1[a[xx][i]]>ans[i]+b[xx][i])
					{
						ans1[a[xx][i]]=ans[i]+b[xx][i];
					}
				}*/
			}
			else
			{
				if(ans[a[xx][i]]>ans[i]+b[xx][i])
				{
					//ans1[a[xx][i]]=ans[a[xx][i]];
					fm[a[xx][i]]=i;
					ans[a[xx][i]]=ans[i]+b[xx][i];
					t%=100000;
					t++;
					f[t]=a[xx][i];
				}
				/*else
				{
					if(ans1[a[xx][i]]>ans[i]+b[xx][i])
					{
						ans1[a[xx][i]]=ans[i]+b[xx][i];
					}
				}*/
			}
		}
		ff[xx]=false;
	}
}
int main()
{
	freopen("cow.in","r",stdin);
	freopen("cow.out","w",stdout);
	n=read();m=read();
	For(i,1,m)
	{
		x=read();y=read();z=read();
		a[x].pb(y);b[x].pb(z);
		a[y].pb(x);b[y].pb(z);
	}
	spfa(0,0);
	For(i,1,n)fm1[i]=fm[i];
	For(i,2,n)
	{
		spfa(fm1[i],i);
		if(ans[i]==1e9)cout<<-1<<endl;
					else cout<<ans[i]<<endl;
	}
	return 0;
}
