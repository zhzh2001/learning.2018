#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std; 
struct D{
	int sp,l;
}b[3000];
struct E{
	int mn,mx;
}a[3000];
bool ff[3000];
int n,m,x,ans;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
bool cmp(D a,D b){return a.sp<b.sp;}
bool cmp1(E a,E b){return a.mx<b.mx||(a.mx==b.mx&&a.mn<b.mn);}
int main()
{
	freopen("cover.in","r",stdin);
	freopen("cover.out","w",stdout);
	n=read();m=read();
	For(i,1,n)
	{
		a[i].mn=read();
		a[i].mx=read();
	}
	For(i,1,m)
	{
		b[i].sp=read();
		b[i].l=read();
	}
	sort(b+1,b+m+1,cmp);
	sort(a+1,a+n+1,cmp1);
//	For(i,1,n)cout<<a[i].mn<<" "<<a[i].mx<<endl;
//	For(i,1,m)cout<<b[i].sp<<" "<<b[i].l<<endl;
	memset(ff,false,sizeof(ff));
	For(i,1,m)
	{
		x=0;
		while(b[i].l>0&&x<=n)
		{
			while(x<=n)
			{
				x++;
				if(ff[x])continue;
				if(b[i].sp<=a[x].mx&&b[i].sp>=a[x].mn)
				{
					b[i].l--;
					ans++;
					ff[x]=true;
					break;
				}
			}
		}
	}
	cout<<ans<<endl;
	return 0;
}
