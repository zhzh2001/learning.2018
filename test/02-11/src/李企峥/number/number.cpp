#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std; 
ll ans[40100],a[40010];
ll n,m,sum;
bool f[40010];
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	n=read();m=read();
	For(i,1,n)a[i]=read(),ans[i]=1e9;
	ans[0]=0;
	For(i,1,n)
	{
		memset(f,false,sizeof(f));
		sum=0;
		For(j,i,n)
		{
			if(sum*sum+ans[i-1]>n)break;
			if(f[a[j]])
			{
				ans[j]=min(ans[i-1]+sum*sum,ans[j]); 
			}
			else
			{
				sum++;
				f[a[j]]=true;
				ans[j]=min(ans[i-1]+sum*sum,ans[j]);
			}
		}
	}
	//For(i,1,n)cout<<ans[i]<<endl; 
	cout<<ans[n]<<endl;
	return 0;
}
