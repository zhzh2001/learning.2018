#include<bits/stdc++.h>
using namespace std;
const int LEN = 200005;
char STR[LEN],*ST = STR,*ED = STR;
inline char gc(){
	if(ST == ED){
		ED = (ST = STR) + fread(STR,1,LEN,stdin);
		if(ST == ED) return EOF;
	}
	return *ST++;
}

inline int read(){
	int x = 0,f = 1;char ch = gc();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = gc();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = gc();}
	return x*f;
}

const int N = 2505,M = 2505;
int n,m,ans;
struct cow{
	int l,r;
}a[N];
struct fangshai{
	int spf,cover;
}c[N];

struct headp{		//min top
	int top;
	int a[M];
	inline void push(int w){
		a[++top] = w;
		up(top);
	}
	inline void pop(){
		a[1] = a[top--];
		down(1);
	}
	void up(int x){
		if(x == 1) return;
		int f = x>>1;
		if(a[x] < a[f]){
			swap(a[f],a[x]);
			up(f);
		}
	}
	void down(int x){
		int l = x<<1;
		if(l > top) return;
		if(l < top && a[l|1] < a[l])
			l |= 1;
		if(a[l] < a[x]){
			swap(a[l],a[x]);
			down(l);
		}
	}
}Q;

inline bool cmp1(cow a,cow b){
	return a.l < b.l || (a.l == b.l && a.r < b.r);
}

inline bool pa(cow a,cow b){
	return a.r > b.r;
}

inline bool cmp2(fangshai a,fangshai b){
	return a.spf < b.spf;
}

int main(){
	freopen("cover.in","r",stdin);
	freopen("cover.out","w",stdout);
	n = read(); m = read();
	for(int i = 1;i <= n;++i){
		a[i].l = read(); a[i].r = read();
	}
	sort(a+1,a+n+1,pa);
	for(int i = 1;i <= m;++i){
		c[i].spf = read(); c[i].cover = read();
	}
	sort(c+1,c+m+1,cmp2);
	while(a[n].r < c[1].spf)
		--n;
	sort(a+1,a+n+1,cmp1);
	int cnt = 1;
	for(int i = 1;i <= m;++i){
		while(Q.top > 0 && Q.a[1] < c[i].spf) Q.pop();
		while(cnt <= n && a[cnt].l <= c[i].spf){
			Q.push(a[cnt].r);
			++cnt;
		}
		while(Q.top > 0 && Q.a[1] < c[i].spf) Q.pop();
		while(Q.top > 0 && c[i].cover > 0){
			c[i].cover--;
			ans++;
			Q.pop();
		}
	}
	printf("%d\n", ans);
	return 0;
}