#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int LEN = 200005;
char STR[LEN],*ST = STR,*ED = STR;
inline char gc(){
	if(ST == ED){
		ED = (ST = STR) + fread(STR,1,LEN,stdin);
		if(ST == ED) return EOF;
	}
	return *ST++;
}

inline ll read(){
	ll x = 0,f = 1;char ch = gc();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = gc();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = gc();}
	return x*f;
}

const int N = 1005;
ll n,m;
ll a[N],cnt;
ll b[N][N];
ll sum;
ll num[N];
ll nxt[N],last[N];
ll f[N];

inline void checkMin(ll &x,ll y){
	if(x > y) x = y;
}

signed main(){
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	n = read(); m = read();
	for(ll i = 1;i <= n;++i){
		ll x = read();
		if(a[cnt] != x)
			a[++cnt] = x;
	}
	n = cnt;
	for(ll i = n;i;--i){
		nxt[i] = last[a[i]];
		last[a[i]] = i;
	}
	for(ll i = 1;i <= m;++i){
		if(last[i] != 0){
			num[last[i]] = 1;
			++sum;
		}
	}
	for(ll i = 1;i <= n;++i){
		ll val = sum;
		for(ll j = n;j >= i;--j){
			b[i][j] = val*val;
			if(num[j] == 1) --val;
		}
		num[i] = 0;
		if(nxt[i] != 0)
			num[nxt[i]] = 1;
		else --sum;
	}
	// for(ll i = 1;i <= n;++i,puts(""))
	// 	for(ll j = i;j <= n;++j)
	// 		printf("%lld ",b[i][j]);
	for(ll i = 1;i <= n;++i) f[i] = 1000000000000LL;
	for(ll i = 1;i <= n;++i)
		for(ll j = i;j >= 1;--j)
			checkMin(f[i],f[j-1]+b[j][i]);
	printf("%lld\n", f[n]);
	return 0;
}