#include<bits/stdc++.h>
using namespace std;
const int LEN = 200005;
char STR[LEN],*ST = STR,*ED = STR;
inline char gc(){
	if(ST == ED){
		ED = (ST = STR) + fread(STR,1,LEN,stdin);
		if(ST == ED) return EOF;
	}
	return *ST++;
}
// #define gc getchar
inline int read(){
	int x = 0,f = 1;char ch = gc();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = gc();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = gc();}
	return x*f;
}

const int N = 2505,M = 2505;
int n,m,ans;

struct headp{
	int top;
	int a[M];
	inline void push(int w){
		a[++top] = w;
		up(top);
	}
	inline void pop(){
		a[1] = a[top--];
		down(1);
	}
	void up(int x){
		if(x == 1) return;
		int f = x>>1;
		if(a[x] < a[f]){
			swap(a[f],a[x]);
			up(f);
		}
	}
	void down(int x){
		int l = x<<1;
		if(l > top) return;
		if(l < top && a[l|1] < a[l])
			l |= 1;
		if(a[l] < a[x]){
			swap(a[l],a[x]);
			down(l);
		}
	}
}Q;

int main(){
	freopen("qheap.in","r",stdin);
	n = read();
	for(int i = 1;i <= n;++i){
		int x = read();
		Q.push(x);
	}
	for(int i = 1;i <= n;++i){
		printf("%d\n",Q.a[1]);
		Q.pop();
	}
	return 0;
}