#include<bits/stdc++.h>
using namespace std;
const int LEN = 200005;
char STR[LEN],*ST = STR,*ED = STR;
inline char gc(){
	if(ST == ED){
		ED = (ST = STR) + fread(STR,1,LEN,stdin);
		if(ST == ED) return EOF;
	}
	return *ST++;
}

inline int read(){
	int x = 0,f = 1;char ch = gc();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = gc();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = gc();}
	return x*f;
}

const int N = 100005,M = 200005,INF = 1e9;
int n,m;
bool vis[N];
int to[M*2],nxt[2*M],w[M*2],cnt,head[N];
int To[M*2],Nxt[2*M],Cnt,Head[N];
int dist[N],dist2[N];
int from[N];
struct node{
	int x,w;
	friend bool operator <(node a,node b){
		return a.w < b.w;
	}
};
struct headp{
	int top;
	node a[M];
	inline void push(int x,int w){
		a[++top] = (node){x,w};
		up(top);
	}
	inline void pop(){
		a[1] = a[top--];
		down(1);
	}
	void up(int x){
		if(x == 1) return;
		int f = x>>1;
		if(a[x] < a[f]){
			swap(a[f],a[x]);
			up(f);
		}
	}
	void down(int x){
		int l = x<<1;
		if(l > top) return;
		if(l < top && a[l|1] < a[l])
			l |= 1;
		if(a[l] < a[x]){
			swap(a[l],a[x]);
			down(l);
		}
	}
}Q;

inline void Insert(int x,int y){
	To[++Cnt] = y; Nxt[Cnt] = Head[x]; Head[x] = Cnt;
}

inline void insert(int x,int y,int t){
	to[++cnt] = y; nxt[cnt] = head[x]; w[cnt] = t; head[x] = cnt;
}

void dij(){
	for(int i = 1;i <= n;++i)
		dist[i] = INF;
	dist[1] = 0;
	for(int i = head[1];i;i = nxt[i]){
		dist[to[i]] = w[i];
		from[to[i]] = 1;
		Q.push(to[i],w[i]);
	}
	for(int i = 2;i <= n;++i){
		int u = Q.a[1].x; Q.pop();
		while(vis[u]){
			u = Q.a[1].x;
			Q.pop();
		}
		Insert(from[u],u);
		for(int i = head[u];i;i = nxt[i]){
			int v = to[i];
			int newdist = dist[u]+w[i];
			if(dist[v] > newdist){
				from[v] = u;
				dist[v] = newdist;
				Q.push(v,newdist);
			}
		}
	}
}


int dfs(int x){
	for(int i = head[x];i;i = nxt[i]){		//(1)
		int v = to[i];
		if(from[v] == x || vis[v]) continue;
		if(dist2[v] > dist[x]+w[i]) dist2[v] = dist[x]+w[i];
	}
	for(int i = Head[x];i;i = Nxt[i]){
        int v = To[i];
        vis[x] = true;
        dfs(v);
        vis[x] = false;
	}
}

void get2(){
	for(int i = 1;i <= n;++i)
		dist2[i] = INF;
	dist2[1] = 0;
	memset(vis,false,sizeof vis);
	// dfs2(1)
	dfs(1);
}

int main(){
	freopen("cow.in","r",stdin);
	freopen("cow.out","w",stdout);
	n = read(); m = read();
	for(int i = 1;i <= m;++i){
		int x = read(),y = read(),t = read();
		insert(x,y,t); insert(y,x,t);
	}
	dij();
	get2();
	for(int i = 2;i <= n;++i){
		// printf("%d ",dist[i]);
		if(dist2[i] == INF) puts("-1");
		else printf("%d\n", dist2[i]);
	}
	return 0;
}