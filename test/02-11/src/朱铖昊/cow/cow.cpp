#include<bits/stdc++.h>
using namespace std;
const int N=100005,M=400005;
int la[N],to[M],pr[M],vis[M],dis[N],v[M],fat[N],fa[N],q[N],h[N],in[N],ans[N];
int len,n,m,x,y,z,cnt;
vector<int> vec;
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void add(int x,int y,int z)
{
	to[++cnt]=y;
	pr[cnt]=la[x];
	la[x]=cnt;
	v[cnt]=z;
}
inline void spfa()
{
	int l=0,r=1;
	memset(dis,0x3f,sizeof(dis));
	q[1]=1;
	in[1]=1;
	dis[1]=0;
	while (l!=r)
	{
		++l;
		if (l==n)
			l=0;
		int p=q[l];
		for (int i=la[p];i;i=pr[i])
		{
			int k=dis[p]+v[i];
			if (dis[to[i]]>k)
			{
				dis[to[i]]=k;
				if (!in[to[i]])
				{
					in[to[i]]=1;
					++r;
					if (r==n)
						r=0;
					q[r]=to[i];
				}
			}
		}
	}
}
int get_father(int x)
{
	return x==fa[x]?x:fa[x]=get_father(fa[x]);
}
inline void make_tree(int x,int len)
{
	h[x]=len;
	for (int i=la[x];i;i=pr[i])
		if (!vis[i]&&dis[x]+v[i]==dis[to[i]])
		{
			vis[i]=1;
			vis[i^1]=1;
			fat[to[i]]=x;
			make_tree(to[i],len+1);
		}
}
struct edge
{
	int u,v,w;
	bool operator<(const edge &a)const
	{
		return dis[u]+dis[v]+w<dis[a.u]+dis[a.v]+a.w;
	}
}e[M/2];
inline void work(int &u)
{
	if (ans[u]!=-1)
		u=get_father(u);
	ans[u]=len-dis[u];
	vec.push_back(u);
	u=fat[u];
}
int main()
{
	freopen("cow.in","r",stdin);
	freopen("cow.out","w",stdout);
	read(n);
	read(m);
	cnt=1;
	for (int i=1;i<=m;++i)
	{
		read(x);
		read(y);
		read(z);
		add(x,y,z);
		add(y,x,z);
	}
	spfa();
	make_tree(1,1);
	cnt=0;
	for (int i=1;i<=n;++i)
		for (int j=la[i];j;j=pr[j])
			if (!vis[j]&&!vis[j^1])
			{
				e[++cnt]=(edge){i,to[j],v[j]};
				vis[j]=1;
				vis[j^1]=1;
			}
	sort(e+1,e+1+cnt);
	for (int i=1;i<=n;++i)
		fa[i]=i;
	memset(ans,-1,sizeof(ans));
	for (int i=1;i<=cnt;++i)
	{
		int u=e[i].u,v=e[i].v,w=e[i].w;
		len=dis[u]+dis[v]+w;
		vec.clear();
		while (u!=v)
		{
			if (h[u]>h[v])
				work(u);
			else
				work(v);
		}
		for (unsigned j=0;j<vec.size();++j)
			fa[get_father(vec[j])]=u;
	}
	for (int i=2;i<=n;++i)
		printf("%d\n",ans[i]);
	return 0;
}
