#include<bits/stdc++.h>
using namespace std;
const int N=10005;
pair<int,int> a[N];
int n,x,y,f[N],ans,m;
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
int main()
{
	freopen("cover.in","r",stdin);
	freopen("cover.out","w",stdout);
	read(n);
	read(m);
	for (int i=1;i<=n;++i)
	{
		read(x);
		read(y);
		a[i]=make_pair(y,x);
	}
	for (int i=1;i<=m;++i)
	{
		read(x);
		read(y);
		f[x]+=y;
	}
	sort(a+1,a+1+n);
	for (int i=1;i<=n;++i)
	{
		for (int j=a[i].second;j<=a[i].first;++j)
			if (f[j])
			{
				--f[j];
				++ans;
				break;
			}
	}
	cout<<ans;
	return 0;
}