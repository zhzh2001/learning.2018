#include<bits/stdc++.h>
using namespace std;
const int N=40005,inf=1e9+7;
int a[N],f[N],n,m,cnt,sum;
bitset<N> t;
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
int main()
{
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	read(n);
	read(m);
	for (int i=1;i<=n;++i)
		read(a[i]);
	for (int i=1;i<=n;++i)
		if (a[cnt]!=a[i])
			a[++cnt]=a[i];
	n=cnt;
	f[0]=0;
	for (int i=1;i<=n;++i)
	{
		t=0;
		sum=0;
		f[i]=inf;
		for (int j=i;j>=1;--j)
		{
			if (!t[a[j]])
			{
				++sum;
				t[a[j]]=1;
			}
			if (sum*sum>=f[i])
				break;
			f[i]=min(f[i],f[j-1]+sum*sum);
		}
	}
	cout<<f[n];
	return 0;
}