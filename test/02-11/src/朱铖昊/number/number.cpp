#include<bits/stdc++.h>
using namespace std;
const int N=40005,inf=1e9+7,M=205;
int a[N],f[N],n,m,cnt[M],g[N],p[M][M],si,q[N];
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void del(int x,int pos)
{
	int Q;
	for (int i=1;i<=cnt[pos];++i)
		if (p[pos][i]==x)
		{
			Q=i;
			break;
		}
	--cnt[pos];
	for (int i=Q;i<=cnt[pos];++i)
		p[pos][i]=p[pos][i+1];
}
inline void insert(int x,int y)
{
	if (g[x])
		del(x,g[x]/si);
	g[x]=y;
	p[g[x]/si][++cnt[g[x]/si]]=x;
}
int main()
{
	freopen("number.in","r",stdin);
	freopen("number.out","w",stdout);
	read(n);
	read(m);
	si=(int)sqrt(n);
	for (int i=1;i<=n;++i)
		read(a[i]);
	int cntt=0;
	for (int i=1;i<=n;++i)
		if (a[cntt]!=a[i])
			a[++cntt]=a[i];
	n=cntt;
	f[0]=0;
	insert(0,0);
	for (int i=1;i<=n;++i)
	{
		insert(a[i],i);
		int k=0;
		f[i]=inf;
		for (int j=i/si;k!=si+2&&j>=0;--j)
		{
			for (int l=cnt[j];l>=1&&k!=si+2;--l)
				q[++k]=g[p[j][l]];
		}
		for (int j=1;j<k;++j)
			f[i]=min(f[i],f[q[j+1]]+j*j);
	}
	cout<<f[n];
	return 0;
}