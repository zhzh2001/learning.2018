#include<bits/stdc++.h>
#define LL long long
using namespace std;
LL ans,n,k,i,j,a[200003];
inline LL read()
{
	LL x=0,f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9')
	{
		if(ch=='-')
		f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9')
	{
		x=x*10+ch-48;ch=getchar();
	}
	return x*f;
}
long long f(long long a,long long b)
{
	if(abs(a-b)<3)
	return 0;
	else
	return b-a;
}
int main()
{
	freopen("function.in","r",stdin);
	freopen("function.out","w",stdout);
	n=read();
	for(i=1;i<=n;i++)
	a[i]=read();
	for(i=1;i<=n;i++)
	{
		for(j=i+1;j<=n;j++)
		ans+=f(a[i],a[j]);
	}
	cout<<ans;
	fclose(stdin);
	fclose(stdout);
}

