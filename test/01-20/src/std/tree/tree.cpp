#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <queue>
#include <string>
#include <map>
#include <cstring>
#include <ctime>
#include <vector>
#define inf 1e9
#define ll long long
#define For(i,j,k) for(ll i=j;i<=k;i++)
#define Dow(i,j,k) for(ll i=k;i>=j;i--)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){write(x);puts("");}
int n,poi[500001],huanx[500001],huany[500001],tot,vis[500001],nxt[500001],f[500001],q[500001],p[500001],cnt=1,flag;
bool ve[500001];
inline void add(int x,int y){poi[++cnt]=y;nxt[cnt]=f[x];f[x]=cnt;}
inline void bfs(int x)
{
	int l=1,r=1;
	q[l]=x;
	int h1,h2;
	vis[x]=x;
	while(l<=r)
	{
		int t=q[l];
		for(int i=f[t];i;i=nxt[i])
		{
			if(ve[i])	continue;
			if(!vis[poi[i]])	q[++r]=poi[i],vis[poi[i]]=x,ve[i^1]=1,ve[i]=1;else 
				if(vis[poi[i]]==x)	h1=t,h2=poi[i];
		}
		l++;
	}
	huanx[++tot]=h1;huany[tot]=h2;
}
int main()
{
	freopen("tree.in","r",stdin);freopen("tree.out","w",stdout);
	n=read();
	For(i,1,n)	p[i]=read(),add(i,p[i]),add(p[i],i);
	For(i,1,n)
		if(!vis[i])	bfs(i);
	For(i,1,tot)	if(huanx[i]==huany[i])	{flag=i;break;}
	if(flag)	printf("%d\n",tot-1);
	else 
	printf("%d\n",tot);
	if(!flag)	flag=1,p[huanx[1]]=huanx[1];
	For(i,1,tot)
	{
		if(flag==i)	continue;
		else	p[huanx[i]]=huanx[flag];	
	}
	For(i,1,n)	printf("%d ",p[i]);
}  
