#include<iostream>
#include<cstdio>
#include<queue>
#include<cmath>
#include<algorithm>
#include<map>
#include<cstring>
#define For(i,j,k) for(int i=j;i<=k;++i)
#define Dow(i,j,k) for(int i=k;i>=j;--i)
#define ll long long
using namespace std;
inline ll read()
{
    ll t=0,f=1;char c=getchar();
    while(c<'0'||c>'9')   {if(c=='-') f=-1;c=getchar();}
    while(c<='9'&&c>='0') t=t*10LL+c-48LL,c=getchar();
    return t*f;
}
struct node{int d,x;node(int _d=0,int _x=0) {x=_x;d=_d;}};
bool operator < (node x,node y){return x.d>y.d;}
int poi[500001],nxt[500001],head[500001],v[500001],b_head[500001],b_nxt[500001],from[500001],dis[50001];
bool vis[50001];
int n,m,x,y,z,z1,fr[50001],S,T;
int cnt,cnt1,t_poi[500001],t_nxt[500001],t_head[500001],t_v[500001];
inline void ADD(int x,int y,int z)
{
    poi[++cnt]=y;nxt[cnt]=head[x];v[cnt]=z;
    head[x]=cnt;b_nxt[cnt]=b_head[y];b_head[y]=cnt;
    from[cnt]=x;
}
inline void add(int x,int y,int v1,int v2)
{
    ADD(x,y,v1);
    ADD(y,x,v2);
}
priority_queue<node> q;
inline void dij()
{
    For(i,1,T)  dis[i]=1e9,fr[i]=0,vis[i]=0;
    dis[1]=0;
    q.push(node(dis[1],1));
    while(!q.empty())
    {
        node tmp=q.top();q.pop();
        if(vis[tmp.x])  continue;
        vis[tmp.x]=1;
        for(int i=head[tmp.x];i;i=nxt[i])   
        {
            if(vis[poi[i]]) continue;
            if(dis[poi[i]]>dis[tmp.x]+v[i])
            {
                dis[poi[i]]=dis[tmp.x]+v[i];
                if(tmp.x==1)    fr[poi[i]]=poi[i];
                    else    fr[poi[i]]=fr[tmp.x];
                q.push(node(dis[poi[i]],poi[i]));
            }
        }
    }
}
inline void add1(int x,int y,int z){t_poi[++cnt1]=y;t_nxt[cnt1]=t_head[x];t_head[x]=cnt1;t_v[cnt1]=z;}
inline void re()
{
    S=1;T=n+1;
    for(int i=b_head[1];i;i=b_nxt[i])
    {
        if(fr[from[i]]!=from[i])    add1(S,T,dis[from[i]]+v[i]);
        else    add1(from[i],T,v[i]);
    }
    for(int i=head[1];i;i=nxt[i])
    {
        if(fr[poi[i]]==poi[i])  continue;
        add1(S,poi[i],v[i]);
    }
    For(i,1,cnt)
    {
        if(poi[i]==1||from[i]==1)   continue;
        if(fr[poi[i]]!=fr[from[i]])
        {
            add1(S,poi[i],dis[from[i]]+v[i]);
        }   else
            add1(from[i],poi[i],v[i]);
    }
    cnt=cnt1;swap(poi,t_poi);swap(t_nxt,nxt);swap(t_head,head);swap(t_v,v);
}
int main()
{
	freopen("road.in","r",stdin);freopen("road.out","w",stdout);
    n=read();m=read();
    For(i,1,m)  x=read(),y=read(),z=read(),z1=read(),add(x,y,z,z1);
    T=n;dij();
    re();
    dij();
    if(dis[T]==1e9) puts("-1");else printf("%d\n",dis[T]);
}
