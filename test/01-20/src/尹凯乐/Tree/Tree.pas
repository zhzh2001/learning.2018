program Tree;
 uses math;
 var
  a,f,d,t,g,k:array[0..200001] of longint;
  i,n,o,p,s,sp,mp:longint;
 function hahafind(k:longint):longint;
  begin
   if t[k]<>k then t[k]:=hahafind(t[k]);
   exit(t[k]);
  end;
 begin
  assign(input,'Tree.in');
  assign(output,'Tree.out');
  reset(input);
  rewrite(output);
  readln(n);
  for i:=1 to n do
   begin
    t[i]:=i;
    f[i]:=0;
    d[i]:=0;
   end;
  s:=n;
  sp:=0;
  mp:=0;
  for i:=1 to n do
   begin
    read(a[i]);
    if a[i]=i then continue;
    o:=hahafind(i);
    p:=hahafind(a[i]);
    if o<>p then
     begin
      dec(s);
      t[o]:=p;
     end
            else
     begin
      f[i]:=1;
      inc(mp);
     end;
   end;
  for i:=1 to n do
   begin
    p:=hahafind(i);
    if d[p]=0 then
     begin
      inc(sp);
      g[sp]:=p;
      k[sp]:=i;
      d[p]:=1;
     end;
   end;
  writeln(max(sp-1,mp));
  for i:=1 to n do
   if f[i]=1 then
    if sp=1 then a[i]:=i
            else
    begin
     a[i]:=g[sp];
     dec(sp);
    end;
  for i:=1 to sp-1 do
   a[k[i]]:=a[k[i+1]];
  for i:=1 to n-1 do
   write(a[i],' ');
  writeln(a[n]);
  close(input);
  close(output);
 end.
