#include<bits/stdc++.h>
#define ll long long
using namespace std;
const int N=200005;
ll n,a[N];
bool f[N];
ll t,fa[N];
ll ans,ff[N];
inline ll read(){int x=0,f=1;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')ch=getchar();for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';return x*f;}
inline void write(ll x){if(x<0){x=-x;putchar('-');}if(x>9)write(x/10);	putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
int dfs(int x)
{
	if(f[x]==true){
		if(fa[x]==0){
			ans++;
			fa[x]=ans;
			ff[ans]=x;
		}
		return fa[x];
	}
	f[x]=true;
	fa[x]=dfs(a[x]);
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	memset(f,false,sizeof(f));
	for(int i=1;i<=n;i++)if(f[i]==false)fa[i]=dfs(i);
	bool flag=false;t=0;
	for(int i=1;i<=n;i++)if(a[i]==i){flag=true;t=i;break;}
	if(flag){
		write(ans-1);puts("");
		for(int i=1;i<=ans;i++){
			if(ff[i]==t)continue;
			a[ff[i]]=t;
		}
		for(int i=1;i<=n;i++)write(a[i]),putchar(' ');}
	else{
		write(ans);puts("");
		for(int i=1;i<=ans;i++)a[ff[i]]=ff[1];
		for(int i=1;i<=n;i++)write(a[i]),putchar(' ');
	}
	return 0;
}

