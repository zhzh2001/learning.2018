#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int N=200005;
struct xxx{
	ll rk,k,i;
}a[N];
inline ll read(){int x=0,f=1;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')ch=getchar();for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';return x*f;}
inline void write(ll x){if(x<0){x=-x;putchar('-');}if(x>9)write(x/10);	putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
ll n;
ll ans;
ll t[N],s[N],tmp;
bool com(xxx a,xxx b){return a.k<b.k;}
bool com1(xxx a,xxx b){return a.i<b.i;}
int main()
{
	freopen("function.in","r",stdin);
	freopen("function.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)
		a[i].k=read(),a[i].i=i;
	sort(a+1,a+n+1,com);
	for(int i=1;i<=n;i++){
		if(a[i].k!=a[i-1].k)a[i].rk=a[i-1].rk+1;
		else a[i].rk=a[i-1].rk;
		t[a[i].rk]=a[i].k;
	}
	sort(a+1,a+n+1,com1);
	for(int i=1;i<=n;i++)
		s[a[i].rk]++,tmp+=a[i].k;
	
	for(int i=n;i>=1;i--){
		ll now=i,tt=tmp;
		for(int j=max(a[i].rk-2,1ll);j<=min(a[i].rk+2,n);j++)
			if(abs(t[j]-t[a[i].rk])<3){
				now-=s[j];
				tt-=s[j]*t[j];
			}
		ans+=now*a[i].k-tt;
		tmp-=a[i].k;
		s[a[i].rk]--;
	}
	writeln(ans);
	return 0;
}
