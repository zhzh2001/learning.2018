#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline LL read(){LL a=0,b=1;char dd;while(!isdigit(c)&&c!='-')dd;
	if(c=='-'){b=-b;dd;}while(isdigit(c)){a=a*10+c-'0';dd;}return a*b;}
#undef dd
struct st{LL a,b;st(){}st(LL x,LL y){a=x;b=y;}}b[200003];
bool operator<(st a,st b){return a.a<b.a;}
LL tree[800003],cnt[800003],n,a[200003],c[200003],v,x,y,z,ans=0;
#define ls (nod<<1)
#define rs (nod<<1|1)
void pushup(LL nod,LL l,LL r){
	tree[nod]=tree[ls]+tree[rs];
	cnt[nod]=cnt[ls]+cnt[rs];
}
void build(LL nod,LL l,LL r){
	if(l==r){
		tree[nod]=b[l].a;
		cnt[nod]=1;
		return;
	}
	LL mid=l+r>>1;
	build(ls,l,mid);
	build(rs,mid+1,r);
	pushup(nod,l,r);
}
void del(LL nod,LL l,LL r,LL x){
	if(l==r){
		tree[nod]=0;
		cnt[nod]=0;
		return;
	}
	LL mid=l+r>>1;
	if(x<=mid)del(ls,l,mid,x);
	else del(rs,mid+1,r,x);
	pushup(nod,l,r);
}
LL query(LL nod,LL l,LL r,LL x,LL y){
	if(l==x&&r==y)return tree[nod];
	LL mid=l+r>>1,ans=0;
	if(x<=mid)ans+=query(ls,l,mid,x,min(y,mid));
	if(y>mid)ans+=query(rs,mid+1,r,max(x,mid+1),y);
	return ans;
}
LL qco(LL nod,LL l,LL r,LL x,LL y){
	if(l==x&&r==y)return cnt[nod];
	LL mid=l+r>>1,ans=0;
	if(x<=mid)ans+=qco(ls,l,mid,x,min(y,mid));
	if(y>mid)ans+=qco(rs,mid+1,r,max(x,mid+1),y);
	return ans;
}
int main(){
	freopen("function.in","r",stdin);
	freopen("function.out","w",stdout);
	n=read();
	for(LL i=1;i<=n;i++){
		b[i].a=read();
		a[i]=a[i-1]+b[i].a;
		b[i].b=i;
	}
	sort(b+1,b+n+1);
	b[n+1].a=b[n].a+3;
	for(int i=1;i<=n;i++)c[b[i].b]=i;
	build(1,1,n);
	for(LL i=1;i<=n;i++){
		v=a[i]-a[i-1];
		y=lower_bound(b+1,b+n+2,st(v-2,0))-b;
		z=upper_bound(b+1,b+n+2,st(v+2,0))-b-1;
		ans+=a[n]-a[i]-v*(n-i);
		ans-=query(1,1,n,y,z)-v*qco(1,1,n,y,z);
		del(1,1,n,c[i]);
	}
	printf("%lld\n",ans);
	return 0;
}
