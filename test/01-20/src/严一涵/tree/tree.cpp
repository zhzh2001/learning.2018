#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline LL read(){LL a=0,b=1;char dd;while(!isdigit(c)&&c!='-')dd;
	if(c=='-'){b=-b;dd;}while(isdigit(c)){a=a*10+c-'0';dd;}return a*b;}
#undef dd
LL f[200003],v[200003],a[200003],ta=0,b[200003],tb=0,n,ans=0;
LL getf(LL v){return f[v]==v?v:f[v]=getf(f[v]);}
void add(LL a,LL b){f[getf(a)]=getf(b);}
int main(){
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	for(LL i=1;i<=n;i++)f[i]=i;
	for(LL i=1;i<=n;i++){
		v[i]=read();
		if(i==v[i])b[++tb]=i;
		else if(getf(i)==getf(v[i]))a[++ta]=i;
		else add(i,v[i]);
	}
	if(ta==0&&tb==1){
		puts("0");
		for(LL i=1;i<=n;i++)printf("%lld ",v[i]);
	}
	if(tb==0){
		v[a[ta]]=a[ta];
		b[++tb]=a[ta--];
		ans=1;
	}
	printf("%lld\n",ans+ta+tb-1);
	for(LL i=1;i<=ta;i++)v[a[i]]=b[1];
	for(LL i=1;i<=tb;i++)v[b[i]]=b[1];
	for(LL i=1;i<=n;i++)printf("%lld ",v[i]);
	return 0;
}
