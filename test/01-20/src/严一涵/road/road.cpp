#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;
const int oo=1<<29;
#define dd c=getchar()
inline int read(){int a=0,b=1;char dd;while(!isdigit(c)&&c!='-')dd;
	if(c=='-'){b=-b;dd;}while(isdigit(c)){a=a*10+c-'0';dd;}return a*b;}
#undef dd
struct edge{int a,b,c;edge(){}edge(int x,int y,int z){a=x;b=y;c=z;}}e[400003],f[400003];
int dis[10013],p[10013],en[10013],fn[10013],lis[10013],et=0,ft=0,n,m,x,y,v,w,ans=oo;
bool vis[10013];
void adde(int a,int b,int c,edge*e,int*en,int&et){
	e[++et]=edge(en[a],b,c);
	en[a]=et;
}
inline void swap(int&a,int&b){int c=a;a=b;b=c;}
void spfa(edge*e,int*en){
	int u,v,w,l=0,r=-1;
	for(int i=1;i<=n;i++){
		dis[i]=oo;
		vis[i]=0;
	}
	vis[1]=1;
	dis[1]=0;
	for(int i=en[1];i>0;i=e[i].a){
		u=e[i].b;
		dis[u]=min(dis[u],e[i].c);
		p[u]=u;
		if(!vis[u]){
			lis[++r]=u;
			vis[u]=1;
		}
	}
	for(;l<=r;l++){
		u=lis[l%n];
		for(int i=en[u];i>0;i=e[i].a){
			v=e[i].b;w=e[i].c;
			if(dis[v]>dis[u]+w){
				dis[v]=dis[u]+w;
				p[v]=p[u];
				if(!vis[v]){
					vis[v]=1;
					lis[(++r)%n]=v;
				}
			}
		}
		vis[u]=0;
	}
}
int main(){
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();
	m=read();
	for(int i=1;i<=m;i++){
		x=read();
		y=read();
		v=read();
		w=read();
		adde(x,y,v,e,en,et);
		adde(y,x,w,e,en,et);
	}
	spfa(e,en);
	for(int i=en[1];i>0;i=e[i].a){
		x=e[i].b;
		if(p[x]!=x)adde(1,x,dis[x],f,fn,ft);
	}
	for(int i=2;i<=n;i++){
		for(int j=en[i];j>0;j=e[j].a){
			x=e[j].b;
			y=e[j].c;
			if(x==1){
				if(p[i]==i){
					adde(i,n+1,y,f,fn,ft);
				}else{
					adde(1,n+1,dis[i]+y,f,fn,ft);
				}
			}else{
				if(p[i]==p[x]){
					adde(i,x,y,f,fn,ft);
				}else{
					adde(1,x,dis[i]+y,f,fn,ft);
				}
			}
		}
	}
	n++;
	spfa(f,fn);
	ans=dis[n];
	if(ans<oo){
		printf("%d\n",ans);
	}else{
		puts("-1");
	}
	return 0;
}
