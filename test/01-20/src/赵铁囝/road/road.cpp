#include<iostream>
#include<cstdio>
#include<cstring>
#include<fstream>
#define cin fin
#define cout fout
using namespace std;

ifstream fin("road.in");
ofstream fout("road.out");

long long n,m,x,y,f[2005][2005],ans=1e9,now;
bool vis[2005][2005];

void dfs(int x){
//	cout<<x<<" "<<now<<'\n';
	if(now>ans)return;
	if(x==1&&now!=0){
		if(now<ans)ans=now;
		return;
	}
	for(int i=1;i<=n;i++){
		if(!vis[i][x]&&i!=x){
			now+=f[x][i];
			vis[i][x]=true;
			vis[x][i]=true;
			dfs(i);
			vis[i][x]=false;
			vis[x][i]=false;
			now-=f[x][i];
		}
	}
	return;
}

int main(){
	cin>>n>>m;now=0;ans=1e9;
	memset(vis,true,sizeof vis);
	for(int i=1;i<=m;i++){
		cin>>x>>y;
		vis[x][y]=false;vis[y][x]=false;
		cin>>f[x][y]>>f[y][x];
	}
	dfs(1);
	if(ans==1e9)cout<<-1<<'\n';else cout<<ans<<'\n';
	return 0;
}
