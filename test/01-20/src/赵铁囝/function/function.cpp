#include<iostream>
#include<cstdio>
#include<fstream>
#include<cmath>
#define ll long long
#define cin fin
#define cout fout
using namespace std;

ifstream fin("function.in");
ofstream fout("function.out");

ll sum,n,m;
int a[200000];

ll f(int x,int y){
	if(abs(x-y)<3)return 0;
	return y-x;
}

int main(){
	cin>>n;
	for(int i=1;i<=n;i++){
		cin>>a[i];
	}
	for(int i=1;i<=n-1;i++){
		for(int j=i+1;j<=n;j++){
            sum+=f(a[i],a[j]);
		}
	}
	cout<<sum<<"\n";
	return 0;
}
