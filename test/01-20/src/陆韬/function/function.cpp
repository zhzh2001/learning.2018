#include<cstdio>
#include<iostream>
#include<cstring>
#include<cmath>
#define ll long long
using namespace std;
int a[200005];
int n;
ll ans;
ll f(int x,int y)
{
	return abs(x-y)<3?0:y-x;
}
int main()
{
	freopen("function.in","r",stdin);freopen("function.out","w",stdout);
	scanf("%d",&n);
	for (int i=0;i<n;i++) scanf("%d",&a[i]);
	for (int i=0;i<n-1;i++)
	{
		for (int j=i+1;j<n;j++) ans+=f(a[i],a[j]);
	}
	printf("%lld\n",ans);
	fclose(stdin);
	return 0;
}
