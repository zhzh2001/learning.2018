#include<cstdio>
#include<iostream>
#include<cstring>
using namespace std;
const int inf=1e5;
int f[10002][10002];
int b[10002];
int n,m;
long long ans=1ll<<60,sum=0,t;
bool bo=0;
bool judge()
{
	for (int i=2;i<=n;i++) if (b[i]==0) return 0;
	return 1;
}
long long dfs(int x)
{
	//cout<<x<<endl;
	if (x==1&&judge())
	{
		//cout<<"sum="<<sum<<endl;
		if (ans>sum) ans=sum;return ans;
	}
	int bo=-1;
	for (int i=1;i<=n;i++) if (i!=x&&f[x][i]<inf)
	{
		int t1=f[x][i];
		int t2=f[i][x];
		sum+=f[x][i];
		b[i]++;
		f[x][i]=f[i][x]=inf;
		bo=dfs(i);
		b[i]--;
		f[x][i]=t1;
		f[i][x]=t2;
		sum-=f[x][i];
	}
	return bo;
}
int main()
{
	freopen("road.in","r",stdin);freopen("road.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(f,127,sizeof(f));
	for (int i=0;i<m;i++)
	{
		int x,y,c1,c2;
		scanf("%d%d%d%d",&x,&y,&c1,&c2);
		f[x][y]=c1;
		f[y][x]=c2;
		//if (c1>1||c2>1) bo=1;
	}
	if (dfs(1)==-1)
	{
		printf("-1\n");
	}else
	{
		printf("%lld\n",ans);
	}
	//cout<<ans<<endl;
	return 0;
}
