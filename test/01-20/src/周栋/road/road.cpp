#include <iostream>
#include <cstdio>
#define ll long long
using namespace std;
inline ll read()
{
	ll f=1,x=0;char c=getchar();
	for(;c<'0'||c>'9';c=getchar())if(c=='-')f=-1;
	for(;c>='0'&&c<='9';c=getchar())x=x*10+c-'0';
	return x*f;
}
ll ans=-1,head[10010],x,y,v1,v2,edge,n,m;
bool vis[500000];
struct node{
	ll to,pay,nex;
}e[500000];
inline void addedge(ll x,ll y,ll v)
{
	e[++edge].to=y;
	e[edge].pay=v;
	e[edge].nex=head[x];
	head[x]=edge;
}
void dfs(ll k,ll cost)
{
	if (k==1&&cost>1)
	{
		if (ans==-1) ans=cost;
		else ans=min(ans,cost);
		return;
	}
	if (cost>=ans&&ans!=-1) return;
	for (ll i=head[k];i!=0;i=e[i].nex)
		if (!vis[i]) 
		{
			vis[i]=1;
			if (i&1) vis[i+1]=1;else vis[i-1]=1;
			dfs(e[i].to,cost+e[i].pay);
			vis[i]=0;
			if (i&1) vis[i+1]=0;else vis[i-1]=0;
		}
	return;
}
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	n=read();m=read();
	for (ll i=1;i<=m;i++)
	{
		x=read();y=read();v1=read();v2=read();
		addedge(x,y,v1);
		addedge(y,x,v2);
	}
	dfs(1,0);
	printf("%lld",ans);
	return 0;
}
