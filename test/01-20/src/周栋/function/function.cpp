#include <iostream>
#include <cstdio>
#define ll long long
using namespace std;
inline ll read()
{
	ll f=1,x=0;char c=getchar();
	for(;c<'0'||c>'9';c=getchar())if(c=='-')f=-1;
	for(;c>='0'&&c<='9';c=getchar())x=x*10+c-'0';
	return x*f;
}
ll n,a[300000],ans,x;
inline ll f(ll x,ll y)
{
	return (x-y)*(x-y)<9? 0:y-x;
}
int main()
{
	freopen("function.in","r",stdin);
	freopen("function.out","w",stdout);
	n=read();
	for (ll i=1;i<=n;i++)
		a[i]=read();
	for (ll i=1;i<n;i++)
		for (ll j=i+1;j<=n;j++)
			ans+=f(a[i],a[j]);
	printf("%lld",ans);
	return 0;
}
