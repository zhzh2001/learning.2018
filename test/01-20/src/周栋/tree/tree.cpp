#include <iostream>
#include <cstdio>
#define ll long long
using namespace std;
inline ll read()
{
	ll f=1,x=0;char c=getchar();
	for(;c<'0'||c>'9';c=getchar())if(c=='-')f=-1;
	for(;c>='0'&&c<='9';c=getchar())x=x*10+c-'0';
	return x*f;
}
ll n,f[200010],a[200010],ans,get[200010];
ll x,t1,t2,t=-1;
inline ll gf(ll x)
{
	return f[x]!=x? f[x]=gf(f[x]):x;
}
inline void solve()
{
	if (t==-1)
		for (ll i=1;i<ans;i++)
		{
			t1=gf(get[i]);t2=gf(get[i+1]);
			f[t1]=t2;
			a[get[i]]=t2;
		}
	else
		for (ll i=1;i<=ans;i++)
		{
			t1=gf(get[i]);
			if (t==t1) continue;
			f[t1]=t;
			a[get[i]]=t;
		}
	printf("%lld",a[1]);
	for (ll i=2;i<=n;i++) printf(" %lld",a[i]);
	return;
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	for (ll i=1;i<=n;i++) f[i]=i;
	for (ll i=1;i<=n;i++)
	{
		x=read();
		t1=gf(i),t2=gf(x);
		if (t1==t2)
		{
			get[++ans]=i;
			if (x==i) t=t1;
			a[i]=i;
		}
		else
		{
			a[i]=x;
			f[t1]=t2;
		}
	}
	if (t==-1) printf("%d\n",ans);
	else printf("%d\n",ans-1);
	solve();
	return 0;
}
