#include <bits/stdc++.h>
using namespace std;
int n,m,a,b,c,d,l[3000][3000],f[3000][3000],ans;
int main()
{
	freopen("road.in","r",stdin);
	freopen("road.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(f,0x3f3f3f3f,sizeof f);
	for(int i=1;i<=m;i++)
	{
		scanf("%d%d%d%d",&a,&b,&c,&d);
		f[a][b]=l[a][b]=c;
		f[b][a]=l[b][a]=d;
	}
	for(int k=2;k<=n;k++)
		for(int i=2;i<=n;i++)
			for(int j=2;j<=n;j++)
				if(f[i][j]>f[i][k]+f[k][j]) f[i][j]=f[i][k]+f[k][j];
	ans=0x3f3f3f3f;
	for(int i=2;i<=n;i++)
		for(int j=2;j<=n;j++)
		{
			if(i==j) continue;
			if(l[1][i]!=0&&l[j][1]!=0) ans=min(ans,l[1][i]+f[i][j]+f[j][1]);
		}
	if(ans==0x3f3f3f3f) printf("-1");
		else printf("%d",ans);
}