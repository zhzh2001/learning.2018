#include <bits/stdc++.h>
int a[1000005],ans=0,n,f[1000005],ff[1000005];
int read()
{
	int data=0,w=1;
	char ch=0;
	while(ch!='-'&&(ch<'0'||ch>'9'))
		ch=getchar();
	if(ch=='-')
	{
		w=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9')
	{
		data=data*10+ch-'0';
		ch=getchar();
	}
	data*=w;
	return data;
}
void write(int x)
{
     if(x<0)
	 {
		 putchar('-');
		 x=-x;
	 }
     if(x>9) write(x/10);
	 putchar(x%10+'0');
}
void dfs(int k)
{
	if(ff[a[k]])
	{
		ans++;
		return;
	}
	if(k==a[k])
	{
		ff[a[k]]=1;
		f[a[k]]=1;
		ans++;
		return;
	}
	if(f[a[k]]) return;
	ff[a[k]]=1;
	f[a[k]]=1;
	dfs(a[k]);
}
int main()
{
	freopen("tree.in","r",stdin);
	freopen("tree.out","w",stdout);
	n=read();
	for(int i=1;i<=n;a[i++]=read());
	for(int i=1;i<=n;i++)
		ans+=(a[i]==i);
	if(ans==0) ans++;
		else ans=0;
	for(int i=1;i<=n;i++)
		if(!f[i])
		{
			f[i]=1;
			memset(ff,0,sizeof ff);
			ff[i]=1;
			dfs(i);
		}
	write(ans-1);
	puts("");
	for(int i=1;i<=n;i++)
	{
		write(a[i]);
		putchar(' ');
	}
	return 0;
}
