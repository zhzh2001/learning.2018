#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstring>
using namespace std;

int map[1005][1005],dist[1005],ans,n,m;

void dfs(int x,int y,int k){
    if ((k<3&&x==1)&&(y!=0)) return;
    if (y!=0)dist[x]=y;
    if (x==1&&y!=0) ans=min(ans,y);
    for (int i=1;i<=n;i++)
        if ((map[x][i]>0)&&(y+map[x][i]<dist[i]&&i!=x)) dfs(i,y+map[x][i],k+1);
}
int main(){
    ans=1000000000;
    freopen("road.in","r",stdin);
    freopen("road.out","w",stdout);
    scanf("%d%d",&n,&m);
    for (int j=1;j<=m;j++){
        int a,b,x,y;
        scanf("%d%d%d%d",&a,&b,&x,&y);
        map[a][b]=x;
        map[b][a]=y;
    }
    memset(dist,50,sizeof(dist));
    if (n<1001) dfs(1,0,0);
    if (ans==1000000000)cout<<-1;
    else cout<<ans-1;
    return 0;
}
