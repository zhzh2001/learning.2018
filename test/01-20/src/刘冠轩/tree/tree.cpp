#include<iostream>
#include<cstdio>
using namespace std;
int n,a[200005],b[200005],f[200005],sum,ssm,kk,cir[200005],poi[200005],ans;

void print(){
    for (int i=1;i<=n;i++) cout<<a[i]<<' ';
}

void dfs(int x){
    if (f[x]>0&&f[x]!=kk) return;
    if (f[x]==kk){sum++;cir[sum]=x;return;}
    f[x]=kk;
    if (a[x]!=x) dfs(a[x]);else{ssm++;poi[ssm]=x;return;}
}

int main(){
    freopen("tree.in","r",stdin);
    freopen("tree.out","w",stdout);
    scanf("%d",&n);
    for (int i=1;i<=n;i++){
        scanf("%d",&a[i]);
        b[a[i]]++;
    }
    for (int i=1;i<=n;i++){
        kk++;
        if (b[i]==0)dfs(i);
    }
    if (ssm==1){
        cout<<sum<<endl;
        while (sum>1)a[cir[sum]]=a[cir[--sum]];
        a[cir[sum]]=a[poi[ssm]];
    }else if (ssm==0){
        if (sum==0)cout<<1<<endl;
        else cout<<sum<<endl;
        while (sum>1)a[cir[sum]]=a[cir[--sum]];
        a[cir[sum]]=a[cir[sum]];
    }else{
        cout<<(ssm+sum-1);
        while(ssm>1)a[poi[ssm--]]=a[cir[sum]];
        while(sum>1)a[cir[sum--]]=a[poi[ssm]];
        a[cir[sum]]=a[poi[ssm]];
    }
    print();
    return 0;
}
