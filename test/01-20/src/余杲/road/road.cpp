#include<cstdio>
#include<iostream>
#include<fstream>
#include<cstring>
#define cin fin
#define cout fout
using namespace std;
ifstream fin("road.in");
ofstream fout("road.out");
int n,m,nedge,x,y,v1,v2;
long long ans=1e18;
int head[1000000],next[1000000],p[1000000],u[1000000],ord[1000000],v[10001];
void add(int x,int y,int v,int id){
	nedge++;
	ord[nedge]=id;
	p[nedge]=y;
	u[nedge]=v;
	next[nedge]=head[x];
	head[x]=nedge;
}
void dfs(int k,int l,long long sum){
	if(k==1&&sum){
		ans=min(ans,sum);
		return;
	}
	if(v[k]<sum)return;
	if(sum>ans)return;
	v[k]=sum;
	int t=head[k];
	while(t){
		if(ord[t]!=l)dfs(p[t],ord[t],sum+u[t]);
		t=next[t];
	}
}
int main(){
	cin>>n>>m;
	for(int i=0;i<m;i++){
		cin>>x>>y>>v1>>v2;
		add(x,y,v1,i);
		add(y,x,v2,i);
	}
	memset(v,0x7f,sizeof(v));
	dfs(1,0,0);
	if(ans==1e18)cout<<-1;else cout<<ans;
}
