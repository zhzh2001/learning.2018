#include<cstdio>
#include<iostream>
#include<cstring>
#include<fstream>
#define cin fin
#define cout fout
using namespace std;
ifstream fin("tree.in");
ofstream fout("tree.out");
int n,a,b,ans,f[200010],fa[200010];
bool v[200010];
void dfs(int k){
	if(f[k]==k){
		v[k]=0;
		return;
	}
	if(v[k]){
		f[k]=k;
		fa[k]=k;
		ans++;
	}
	v[k]=1;
	dfs(f[k]);
	v[k]=0;
}
int get(int k){
	if(fa[k]==k)return k;
	fa[k]=get(fa[k]);
	return fa[k];
}
int main(){
	cin>>n;
	for(int i=1;i<=n;i++)cin>>f[i];
	memcpy(fa,f,sizeof(f));
	dfs(1);b=get(1);
	for(int i=2;i<=n;i++){
		dfs(i);
		a=get(i);
		if(a!=b)fa[a]=b;
	}
	cout<<ans<<endl;
	for(int i=1;i<=n;i++)cout<<f[i]<<' ';
}
