#include<cstdio>
#include<queue>
#include<cstring>
using namespace std;
int m,n,ans;
char s[110][110];
bool vis[110][110];
struct Node {
	int x, y,step,dir;
};
int row[4]= {-1,0,1,0};
int col[4]= {0,1,0,-1};
int bfs(int sx,int sy,int ex,int ey) {
	queue<Node>Q;
	Q.push((Node) {sx,sy,0,-1});
	vis[sx][sy]=1;
	while(!Q.empty()) {
		Node now=Q.front();
		Q.pop();
	//	printf("%d %d [%c]\n",now.x,now.y,s[now.x][now.y]);
		for(int i=0; i<4; i++) {
			int r=now.x+row[i],c=now.y+col[i];
			if(r>=0&&r<=m&&c<=n&&c>=0&&vis[r][c]==0) {
				vis[r][c]=true;
				if(r==ex&&c==ey){
					int t=now.step+(i!=now.dir);
					if(t<ans)ans=t;continue;}
				if(s[r][c]=='X')continue;
				if(now.dir!=i)Q.push((Node){r,c,now.step+1,i});
				else
				Q.push((Node) {r,c,now.step,now.dir});
			}
		}
	}
	if(ans==0x3f3f3f3f)return -1;
	else return ans;
}

int main() {
//	freopen("a.txt","w",stdout);
	int x1,y1,x2,y2,cnt1=0;
	while(~scanf("%d%d",&n,&m)) {
		if(m==0&&n==0)break;
		memset(s,-1,sizeof(s));
		cnt1++;
		getchar();
		for(int i=1; i<=m; i++)gets(s[i]+1);
		m++,n++;
		printf("Board #%d:\n",cnt1);
		int cnt2=0;
		while(~scanf("%d%d%d%d",&y1,&x1,&y2,&x2)) {
			getchar();
			ans=0x3f3f3f3f;
			if(x1==0&&y1==0&&x2==0&&y2==0)break;
			memset(vis,0,sizeof(vis));
			cnt2++;
			int ans=bfs(x1,y1,x2,y2);
			if(ans!=-1)printf("Pair %d: %d segments.\n",cnt2,ans);
			else printf("Pair %d: impossible.\n",cnt2);
		}
	}
	return 0;
}
