#include<iostream>
using namespace std;
const int N=200005,MOD=1e9+7;
long long fact[N],inv[N];
long long qpow(long long a,int b)
{
	long long ans=1;
	do
	{
		if(b&1)
			ans=ans*a%MOD;
		a=a*a%MOD;
	}while(b/=2);
	return ans;
}
int main()
{
	int n;
	cin>>n;
	fact[0]=1;
	for(int i=1;i<=n*2;i++)
		fact[i]=fact[i-1]*i%MOD;
	inv[n*2]=qpow(fact[n*2],MOD-2);
	for(int i=n*2-1;i>=0;i--)
		inv[i]=inv[i+1]*(i+1)%MOD;
	long long ans=0;
	for(int i=0;i<n;i++)
		ans+=fact[n-1+i]*inv[n-1]%MOD*inv[i]%MOD;
	cout<<(ans*2-n)%MOD<<endl;
	return 0;
}
