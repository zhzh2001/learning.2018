#include<fstream>
#include<utility>
#include<algorithm>
using namespace std;
ifstream fin("triangle.in");
ofstream fout("triangle.out");
const int N=100005;
int a[N],head[N],nxt[N*2],v[N*2],e,vis[N],now[N];
inline void add_edge(int u,int v)
{
	::v[++e]=v;
	nxt[e]=head[u];
	head[u]=e;
}
pair<int,int> q[N];
int main()
{
	int n,m;
	fin>>n>>m;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	for(int i=1;i<n;i++)
	{
		int u,v;
		fin>>u>>v;
		add_edge(u,v);
		add_edge(v,u);
	}
	for(int i=1;i<=m;i++)
	{
		int opt,x,y;
		fin>>opt>>x>>y;
		if(opt==1)
		{
			int l=1,r=1;
			q[l]=make_pair(x,0);
			vis[x]=i;
			while(l<=r)
			{
				for(int j=head[q[l].first];j;j=nxt[j])
					if(vis[v[j]]<i)
					{
						q[++r]=make_pair(v[j],l);
						vis[v[j]]=i;
						if(v[j]==y)
							break;
					}
				if(q[r].first==y)
					break;
				l++;
			}
			int cnt=0;
			for(int j=r;j;j=q[j].second)
				now[++cnt]=a[q[j].first];
			sort(now+1,now+cnt+1);
			bool flag=false;
			for(int j=1;j<=cnt-2;j++)
				if(now[j]+now[j+1]>now[j+2])
				{
					flag=true;
					break;
				}
			fout<<(flag?"Y\n":"N\n");
		}
		else
			a[x]=y;
	}
	return 0;
}
