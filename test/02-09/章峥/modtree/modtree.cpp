#include<fstream>
#include<cstring>
#include<algorithm>
using namespace std;
ifstream fin("modtree.in");
ofstream fout("modtree.out");
const int N=300005;
int a[N],d[N];
bool vis[N];
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	memset(d,0x3f,sizeof(d));
	d[1]=0;
	memset(vis,0,sizeof(vis));
	long long ans=0;
	for(int i=1;i<=n;i++)
	{
		int j=0;
		for(int k=1;k<=n;k++)
			if(!vis[k]&&d[k]<d[j])
				j=k;
		vis[j]=true;
		ans+=d[j];
		for(int k=1;k<=n;k++)
			if(!vis[k])
				d[k]=min(d[k],max(a[j],a[k])%min(a[j],a[k]));
	}
	fout<<ans<<endl;
	return 0;
}
