#include <bits/stdc++.h>
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
#define LL long long
using namespace std;
inline int read() {
	int x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f=-1; ch=getchar(); }
	while(ch>='0'&&ch<='9') { x=x*10+ch-48; ch=getchar(); }
	return x * f;
}
void write(int x) {
	if(x<0) x=-x, putchar('-');
	if(x>9) write(x/10);
	putchar(x%10+48);
}
void writeln(int x) { write(x); putchar('\n'); }

const int N = 300011; 
int n, nedge, num;
int val[N],fa[N];
LL ans;
struct edge{
	int x, y, val; 
}e[N];

inline bool cmp(int a,int b) { return a > b; }
inline bool cmp_val(edge a,edge b) { return a.val < b.val; }
int find(int x) {
	if(fa[x]==x) return x;
	return fa[x] = find(fa[x]); 
}

int main() {
	freopen("modtree.in","r",stdin); 
	freopen("modtree.out","w",stdout); 
	n = read(); 
	if(n<=3000) {
		For(i, 1, n) val[i] = read(); 
		sort(val+1, val+n+1, cmp); 
		For(i, 1, n-1) 
	  	  For(j, i+1, n) {
	  		e[++nedge].x = i; e[nedge].y = j; e[nedge].val = val[i] % val[j]; 
	  	}
		sort(e+1, e+nedge+1, cmp_val); 
		For(i, 1, n) fa[i] = i; 
		For(i, 1, nedge) {
			int x = find(e[i].x); 
			int y = find(e[i].y); 
			if(x == y) continue; 
			fa[x] = y; 
			++num; 
			ans += e[i].val; 
			if(num == n-1) break;
		}
		printf("%lld\n",ans); 
	}
	return 0; 
} 





