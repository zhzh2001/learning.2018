#include <cstdio>
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
#define LL long long
using namespace std;
inline int read() {
	int x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f=-1; ch=getchar(); }
	while(ch>='0'&&ch<='9') { x=x*10+ch-48; ch=getchar(); }
	return x * f;
}
inline LL min(LL x,LL y) { return x<y? (x):(y); }

const int N = 20011; 
const LL inf = 1e15;  
int n; 
LL f[N][2], l[N], r[N]; 

int main() {
	freopen("segment.in","r",stdin); 
	freopen("segment.out","w",stdout); 
	n = read(); 
	For(i, 1, n) l[i] = read(), r[i] = read(), f[i][0] = f[i][1] = inf; 
	f[1][1] = r[1]-1; 
	For(i, 2, n) {
		if(l[i]<=l[i-1] && l[i-1]<=r[i]) 
			f[i][1] = min(f[i][1], f[i-1][0]+1+l[i-1]-l[i]+r[i]-l[i]); 
			else if(l[i-1] < l[i]) 
				f[i][1] = min(f[i][1], f[i-1][0]+1+r[i]-l[i-1]); 
		
		if(l[i]<=r[i-1] && r[i-1]<=r[i]) 
			f[i][1] = min(f[i][1], f[i-1][1]+1+r[i-1]-l[i]+r[i]-l[i]);
			else if(r[i-1] < l[i]) 
				f[i][1] = min(f[i][1], f[i-1][1]+1+r[i]-r[i-1]); 
		
		if(l[i]<=l[i-1] && l[i-1]<=r[i]) 
			f[i][0] = min(f[i][0], f[i-1][0]+1+r[i]-l[i-1]+r[i]-l[i]); 
			else if(r[i] < l[i-1])  
				f[i][0] = min(f[i][0], f[i-1][0]+1+l[i-1]-l[i]); 
		
		if(l[i]<=r[i-1] && r[i-1]<=r[i]) 
			f[i][0] = min(f[i][0], f[i-1][1]+1+r[i]-r[i-1]+r[i]-l[i]);
			else if(r[i] < r[i-1])  
				f[i][0] = min(f[i][0], f[i-1][1]+1+r[i-1]-l[i]);
	} 
	LL ans = min(f[n][0] + n-l[n], f[n][1] + n-r[n]); 

	printf("%lld\n",ans); 
	return 0; 
} 







