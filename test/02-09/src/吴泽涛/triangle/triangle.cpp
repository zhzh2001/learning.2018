#include <bits/stdc++.h>
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
#define LL long long
using namespace std;
inline int read() {
	int x = 0, f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f=-1; ch=getchar(); }
	while(ch>='0'&&ch<='9') { x=x*10+ch-48; ch=getchar(); }
	return x * f;
}
void write(int x) {
	if(x<0) x=-x, putchar('-');
	if(x>9) write(x/10);
	putchar(x%10+48);
}
void writeln(int x) { write(x); putchar('\n'); }

const int N = 100011; 
struct node{
	int to,pre; 
}e[N*2]; 
int n, nedge, num, endd, flag, Q; 
int val[N], head[N], t[N]; 

inline void add(int x,int y) {
	e[++nedge].to = y; 
	e[nedge].pre = head[x]; 
	head[x] = nedge; 
}
void dfs(int u, int fa) {
	if(flag) return; 
	t[++num] = val[u]; 
	if(u == endd) {
		flag = 1; return; 
	}
	for(int i=head[u]; i; i=e[i].pre) {
		int v = e[i].to; 
		if(v==fa) continue; 
		dfs(v, u); 
		if(flag) return; 
	}
	--num; 
}

int main() {
	freopen("triangle.in","r",stdin); 
	freopen("triangle.out","w",stdout); 
	n = read(); Q = read(); 
	For(i, 1, n)  val[i] = read(); 
	For(i, 1, n-1) {
		int x = read(), y = read(); 
		add(x, y); add(y, x); 
	}
	while(Q--) {
		int opt = read(), x = read(), y = read(); 
		if(opt==2) {
			val[x] = y; 
			continue;
		}
		num = 0; flag = 0; 
		endd = x; 
		dfs(y, -1); 
		sort(t+1, t+num+1); 
		flag = 0; 
		For(i, 1, num-2) 
			if(t[i]+t[i+1] > t[i+2]) {
				flag = 1; break;
			}
		if(flag==1) puts("Y");
		else puts("N");
	}
	return 0; 
}

/*

5 5
1 2 3 4 5
1 2
2 3
3 4
1 5
1 1 3
1 4 5
2 1 4
1 2 5
1 2 3



7 5
1 2 3 4 5 6 7
1 2
1 5
2 3
2 4
5 6
5 7



*/






