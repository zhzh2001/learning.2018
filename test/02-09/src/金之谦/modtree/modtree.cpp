#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
struct ppap{int x,y,v;}p[2000010];
int n,a[300010],ans=0;
inline bool cmp(ppap a,ppap b){return a.v<b.v;}
int fa[100010];
inline int find(int x){return fa[x]==x?x:fa[x]=find(fa[x]);}
int main()
{
	freopen("modtree.in","r",stdin);
	freopen("modtree.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	sort(a+1,a+n+1);n=unique(a+1,a+n+1)-a-1;
	int cnt=0;
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++){
			int v=a[j]%a[i];
			p[++cnt]=(ppap){i,j,v};
		}
	sort(p+1,p+cnt+1,cmp);
	for(int i=1;i<=n;i++)fa[i]=i;
	int ans=0,s=n;
	for(int i=1;i<=cnt;i++){
		int fx=find(p[i].x),fy=find(p[i].y);
		if(fx!=fy){
			fa[fx]=fy;ans+=p[i].v;s--;
		}
		if(s==1)break;
	}
	writeln(ans);
	return 0;
}
