#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int n,a[100010][2],b[100010],ans=1e9;
inline void check(){
	int x=1,sum=-1;
	for(int i=1;i<=n;i++){
		if(b[i]==1)sum+=1+abs(x-a[i][0])+a[i][1]-a[i][0],x=a[i][1];
		else sum+=1+abs(x-a[i][1])+a[i][1]-a[i][0],x=a[i][0];
	}
	if(b[n]==1)sum+=n-a[n][1];else sum+=n-a[n][0];
	ans=min(ans,sum);
}
inline void dfs(int x){
	if(x==n+1){check();return;}
	b[x]=1;dfs(x+1);
	b[x]=0;dfs(x+1);
}
int main()
{
	freopen("segment.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)a[i][0]=read(),a[i][1]=read();
	dfs(1);
	writeln(ans);
}
