#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int f[100010][2],a[100010][2];
int main()
{
	freopen("segment.in","r",stdin);
	freopen("segment.out","w",stdout);
	int n=read();
	for(int i=1;i<=n;i++)a[i][0]=read(),a[i][1]=read();
	f[1][1]=a[1][1]-1;
	f[1][0]=2*a[1][1]-1-a[1][0];
	for(int i=2;i<=n;i++){
		f[i][0]=min(f[i-1][0]+1+abs(a[i-1][0]-a[i][1]),f[i-1][1]+1+abs(a[i-1][1]-a[i][1]))+a[i][1]-a[i][0];
		f[i][1]=min(f[i-1][0]+1+abs(a[i-1][0]-a[i][0]),f[i-1][1]+1+abs(a[i-1][1]-a[i][0]))+a[i][1]-a[i][0];
	}
	writeln(min(f[n][0]+n-a[n][0],f[n][1]+n-a[n][1]));
	return 0;
}
