#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int n,q,a[100010],deep[100010],f[100010][18],b[100010];
int nedge=0,p[200010],nex[200010],head[200010];
inline void addedge(int a,int b){
	p[++nedge]=b;nex[nedge]=head[a];head[a]=nedge;
}
inline void dfs(int x,int Fa){
	deep[x]=deep[Fa]+1;
	for(int k=head[x];k;k=nex[k])if(p[k]!=Fa){
		f[p[k]][0]=x;dfs(p[k],x);
	}
}
inline int lca(int x,int y){
	if(deep[x]<deep[y])swap(x,y);
	for(int i=17;i>=0;i--)if(deep[f[x][i]]>=deep[y])x=f[x][i];
	for(int i=17;i>=0;i--)if(f[x][i]!=f[y][i])x=f[x][i],y=f[y][i];
	if(x!=y)x=f[x][0];return x;
}
int main()
{
	freopen("triangle.in","r",stdin);
	freopen("triangle.out","w",stdout);
	n=read();q=read();
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<n;i++){
		int x=read(),y=read();
		addedge(x,y);addedge(y,x);
	}
	dfs(1,0);
	for(int j=1;j<18;j++)
		for(int i=1;i<=n;i++)f[i][j]=f[f[i][j-1]][j-1];
	while(q--){
		int op=read();
		if(op==2){int p=read();a[p]=read();continue;}
		int cnt=0,x=read(),y=read();
		int LCA=lca(x,y);
		if(deep[x]+deep[y]-2*deep[LCA]+1>50){puts("Y");continue;}
		if(deep[x]<deep[y])swap(x,y);
		while(deep[x]>deep[y])b[++cnt]=a[x],x=f[x][0];
		while(x!=y)b[++cnt]=a[x],b[++cnt]=a[y],x=f[x][0],y=f[y][0];
		b[++cnt]=a[x];
		sort(b+1,b+cnt+1);
		int flag=0;
		for(int i=2;i<cnt;i++)if(b[i]+b[i-1]>b[i+1])flag=1;
		puts(flag?"Y":"N");
	}
	return 0;
}
