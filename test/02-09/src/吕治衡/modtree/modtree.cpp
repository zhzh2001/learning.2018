#include<bits/stdc++.h>
#define int long long
inline int read(){
	char c=getchar();while (c!='-'&&(c<'0'||c>'9'))c=getchar();
	int k=0,kk=1;if (c=='-')c=getchar(),kk=-1;
	while (c>='0'&&c<='9')k=k*10+c-'0',c=getchar();return kk*k;
}using namespace std;
void write(int x){if (x<0)x=-x,putchar('-');if (x/10)write(x/10);putchar(x%10+'0');}
void writeln(int x){write(x);puts("");}
int n,a[1000000],f[1000000],flag[1000000],ans;
signed main(){
	freopen("modtree.in","r",stdin);freopen("modtree.out","w",stdout);
	n=read();for (int i=1;i<=n;i++)a[i]=read();
	memset(f,10,sizeof(f));f[1]=0;
	for (int i=1;i<=n;i++){
		int k=0;for (int j=1;j<=n;j++)if (!flag[j]&&f[j]<f[k])k=j;
		ans+=f[k];flag[k]=1;for (int j=1;j<=n;j++)f[j]=min(f[j],max(a[j],a[k])%min(a[j],a[k]));
	}writeln(ans);
}


