#include <bits/stdc++.h>
#define ll long long
#define N 100020
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}
int main(int argc, char const *argv[]) {
  freopen("triangle.in", "w", stdout);
  srand(time(0));
  int n = 100000, q = 100000;
  cout << n << " " << q << endl;
  for (int i = 1; i <= n; i++)
    cout << rand() * rand() % 1000000000 + 1 << " ";
  cout << endl;
  for (int i = 2; i <= n; i++)
    cout << i << " " << rand() * rand() % (i - 1) + 1 << endl;
    // cout << i << " " << i - 1 << endl;
  for (int i = 1; i <= q; i++)
    cout << 1 << " " << rand() * rand() % n + 1 << " " << rand() * rand() % n + 1 << endl;
    // cout << "1 1 " << n << endl;
  return 0;
}