#include <bits/stdc++.h>
#define ll long long
#define N 100020
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}
int to[N<<1], nxt[N<<1], head[N], cnt;
void insert(int x, int y) {
  to[++cnt] = y; nxt[cnt] = head[x]; head[x] = cnt;
  to[++cnt] = x; nxt[cnt] = head[y]; head[y] = cnt;
}
int fa[N];
void dfs(int x, int f) {
  fa[x] = f;
  for (int i = head[x]; i; i = nxt[i])
    if (to[i] != f) dfs(to[i], x);
}
int a[N], b[N], tot;
// n^2 pass 10w
int main(int argc, char const *argv[]) {
  freopen("triangle.in", "r", stdin);
  freopen("triangle.out", "w", stdout);
  int n = read(), q = read();
  for (int i = 1; i <= n; i++)
    a[i] = read();
  for (int i = 2; i <= n; i++)
    insert(read(), read());
  while (q --) {
    int op = read(), x = read(), y = read();
    if (op == 1) {
      dfs(x, 0);
      b[tot = 1] = a[y];
      int now = y;
      while (fa[now]) {
        now = fa[now];
        b[++tot] = a[now];
      }
      sort(b + 1, b + tot + 1);
      bool flag = false;
      for (int i = 3; i <= tot && !flag; i++)
        if (b[i - 2] + b[i - 1] > b[i])
          flag = true;
      if (flag) puts("Y");
      else      puts("N");
    } else {
      a[x] = y;
    }
  }
  return 0;
}