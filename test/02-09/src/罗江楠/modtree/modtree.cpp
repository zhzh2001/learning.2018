/**
 * 30%. No more.
 */

#include <bits/stdc++.h>
#define ll long long
#define N 1020
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}
int a[N], cnt;
struct node {
  int x, y, c;
  friend bool operator < (const node &a, const node &b) {
    return a.c < b.c;
  }
}b[N*N];
int fa[N], ans;
int find(int x) {
  if (fa[x] == x) return x;
  else return fa[x] = find(fa[x]);
}
int main(int argc, char const *argv[]) {
  freopen("modtree.in", "r", stdin);
  freopen("modtree.out", "w", stdout);
  int n = read();
  if (n > 1000)
    return puts("I lost my dream."), 0;
  for (int i = 1; i <= n; i++)
    a[i] = read();
  for (int i = 1; i <= n; i++)
    for (int j = i + 1; j <= n; j++)
      b[++ cnt] = {i, j, max(a[i], a[j]) % min(a[i], a[j])};
  sort(b + 1, b + cnt + 1);
  for (int i = 1; i <= n; i++)
    fa[i] = i;
  for (int i = 1; i <= cnt; i++) {
    int x = find(b[i].x), y = find(b[i].y);
    if (x == y) continue;
    fa[x] = y; ans += b[i].c;
  }
  cout << ans << endl;
  return 0;
}
// 天若有情天亦老
// 我为 ** 续一秒
