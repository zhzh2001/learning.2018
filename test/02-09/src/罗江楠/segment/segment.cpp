#include <bits/stdc++.h>
#define ll long long
#define N 100020
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}
int l[N], r[N], a[N], b[N];
int main(int argc, char const *argv[]) {
  freopen("segment.in", "r", stdin);
  freopen("segment.out", "w", stdout);
  int n = read();
  for (int i = 1; i <= n; i++) {
    l[i] = read(); r[i] = read();
  }
  b[1] = r[1] - 1;
  a[1] = b[1] + r[1] - l[1];
  // cout << a[1] << " " << b[1] << endl;
  for (int i = 2; i <= n; i++) {
    a[i] = min(a[i - 1] + 1 + abs(l[i - 1] - r[i]), b[i - 1] + 1 + abs(r[i - 1] - r[i])) + r[i] - l[i];
    b[i] = min(a[i - 1] + 1 + abs(l[i - 1] - l[i]), b[i - 1] + 1 + abs(r[i - 1] - l[i])) + r[i] - l[i];
    // cout << a[i] << " " << b[i] << endl;
  }
  cout << min(a[n] + n - l[n], b[n] + n - r[n]) << endl;
  return 0;
}