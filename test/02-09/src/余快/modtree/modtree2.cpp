#include<bits/stdc++.h>
#define ll long long
#define N 100005
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
using namespace std;
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(ll x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(ll x){wr(x);put;}inline void wri(ll x){wr(x);putchar(' ');}
int pd,tot,t,amax,n,m,p,num,fa[10000005],a[300005],vis[10000005];
ll ans;
struct xx{
	int a,b,l;
}z[10000005];
bool cmp(xx a,xx b){
	return a.l<b.l;
}
int gf(int x){
	return fa[x]==x?x:fa[x]=gf(fa[x]);
}
signed main(){
//	freopen(".in","r",stdin);freopen(".out","w",stdout);
	n=read();
	for (int i=1;i<=n;i++){a[i]=read();amax=max(amax,a[i]);vis[a[i]]=1;if (a[i]==0) pd=1;}
	sort(a+1,a+n+1);t=a[2]%a[1];
	for (int i=2;i<n;i++) t=max(t,min(a[i+1]%a[i],a[i+1]%a[i-1]));
	if (pd){
		wr(0);return 0;
	}
	for (int i=2;i<=amax;i++){
		if (vis[i]){
			fa[i]=i;
			num++;
			for (int j=1;j<=amax/i;j++){
				for (int k=0;k<=min(t,i-1);k++){
					if (j==1&&k==0) continue;
					if (vis[j*i+k]){
						z[++tot].a=i;z[tot].b=j*i+k;z[tot].l=k;
					}
				}
			}
		}
	}
	sort(z+1,z+tot+1,cmp);
	for (int i=1;i<=tot;i++){
		if (gf(z[i].a)!=gf(z[i].b)){
			p++;
			ans+=z[i].l;
			fa[gf(z[i].a)]=gf(z[i].b);
			if (p==num-1) break;
		}
	}
	wr(ans);
	return 0;
}
