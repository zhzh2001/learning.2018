#include<bits/stdc++.h>
#define ll long long
#define N 100005
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
using namespace std;
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
int n,m,t,x,y,fa[N][21],q,a[N],dis[N],f[N],lca,pd,tot;
int nedge,Next[N*2],head[N],to[N*2];
void add(int a,int b){nedge++;Next[nedge]=head[a];head[a]=nedge;to[nedge]=b;}
void dfs(int x){
	for (int i=head[x];i;i=Next[i]){
		if (to[i]!=fa[x][0]) {fa[to[i]][0]=x;dis[to[i]]=dis[x]+1;dfs(to[i]);}
	}
}
int get_lca(int x,int y){
	if (x==y) return x;
	if (dis[x]<dis[y]) swap(x,y);
	if (dis[x]!=dis[y]){
	for (int i=20;~i;i--){
		int t=fa[x][i];
		if (dis[t]>dis[y]) x=t;
	}
	x=fa[x][0];
	}
	if (x==y) return x;
	for (int i=20;~i;i--){
		if (fa[x][i]!=fa[y][i]){
			x=fa[x][i];y=fa[y][i];
		}
	}
	return fa[x][0];
}
const int num=50;
signed main(){
	freopen("triangle.in","r",stdin);freopen("triangle.out","w",stdout);
	n=read();q=read();
	for (int i=1;i<=n;i++) a[i]=read();
	for (int i=1;i<n;i++){
		x=read();y=read();add(x,y);add(y,x);
	}
	fa[1][0]=1;
	dfs(1);
	for (int i=1;i<=20;i++){
		for (int j=1;j<=n;j++){
			fa[j][i]=fa[fa[j][i-1]][i-1];
		}
	}
	for (int i=1;i<=q;i++){
		t=read();x=read();y=read();
		if (t==1){
			lca=get_lca(x,y);
			if (dis[x]+dis[y]-dis[lca]*2+1>=num){
				printf("Y\n");continue;
			}
			tot=0;
			for (int j=x;;j=fa[j][0]){
				f[++tot]=a[j];
				if (j==lca) break;
			}
			for (int j=y;;j=fa[j][0]){
				if (j==lca) break;
				f[++tot]=a[j];
			}
			sort(f+1,f+tot+1);
			pd=0;
			for (int j=3;j<=tot;j++){
				if (f[j-2]+f[j-1]>f[j]){
					pd=1;break;
				}
			}
			if (pd) printf("Y\n");
			else printf("N\n");
		}
		else a[x]=y;
	}
	return 0;
}
