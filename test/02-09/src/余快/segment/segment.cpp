#include<bits/stdc++.h>
#define ll long long
#define N 100005
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
using namespace std;
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
int n,m,l[N],r[N],f[N][2],ans;
signed main(){
	freopen("segment.in","r",stdin);freopen("segment.out","w",stdout);
	n=read();
	for (int i=1;i<=n;i++){
		l[i]=read();r[i]=read();
	}
	f[1][1]=r[1]-1;f[1][0]=r[1]-1+(r[1]-l[1]);
	for (int i=2;i<=n;i++){
		f[i][0]=min(f[i-1][1]+abs(r[i-1]-r[i])+(r[i]-l[i]),f[i-1][0]+abs(l[i-1]-r[i])+(r[i]-l[i]))+1;
		f[i][1]=min(f[i-1][1]+abs(r[i-1]-l[i])+(r[i]-l[i]),f[i-1][0]+abs(l[i-1]-l[i])+(r[i]-l[i]))+1;
	}
	ans=min(f[n][0]+(n-l[n]),f[n][1]+(n-r[n]));
	wr(ans);
	return 0;
}
