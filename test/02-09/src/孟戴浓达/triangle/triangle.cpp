#include<iostream>
#include<string.h>
#include<stdio.h>
#include<algorithm>
#include<fstream>
#include<math.h>
using namespace std;
//ifsteram fin("triangle.in");
//ofsteram fout("triangle.out");
long long point[100003];
long long xx[100003],yy[100003];
struct edge{
	int to,next;
}e[200003];
int f[100003][23],inn[100003],out[100003],dep[100003],head[100003];
int tot,n,m,x,y,z,p,q,r,s,ans,tim;
int tong[100003];
inline void add(int x,int y){
    e[++tot].to=y,e[tot].next=head[x],head[x]=tot;
}
void dfs(int fa,int x,int depth){
    f[x][0]=fa;
    dep[x]=depth;
    inn[x]=++tim;
    for(int i=head[x];i;i=e[i].next){
    	if(e[i].to!=fa){
    		dfs(x,e[i].to,depth+1);
		}
	}
    out[x]=++tim;
}
inline bool pd(int x,int y){
	if(inn[x]<=inn[y]&&out[x]>=out[y]) return true;
	else return false;
}
inline int lca(int x,int y){
    if(pd(x,y)) return x;
    if(pd(y,x)) return y;
    int k=x;
    for(int j=20;j>=0;j--){
    	if(!pd(f[k][j],y)){
    		k=f[k][j];
		}
	}
    return f[k][0];
}
int main(){
	cin>>n>>q;
	for(int i=1;i<=n;i++){
		cin>>point[i];
	}
	for(int i=1;i<=n-1;i++){
		cin>>xx[i]>>yy[i];
		add(xx[i],yy[i]),add(yy[i],xx[i]);
	}
	dfs(-1,1,0);
    f[1][0]=1;
    for(int j=1;j<=20;j++){
    	for(int i=1;i<=n;i++){
    		f[i][j]=f[f[i][j-1]][j-1];
		}
	}
	long long op,x,y;
	for(int i=1;i<=q;i++){
		cin>>op>>x>>y;
		if(op==1){
			tong[0]=0;
			int lcaa=lca(x,y),now;
			if(lcaa==y){
				now=x;
				while(now!=lcaa){
					tong[++tong[0]]=point[now],now=f[now][0];
				}
				tong[++tong[0]]=point[lcaa];
			}else if(lcaa==x){
				now=y;
				while(now!=lcaa){
					tong[++tong[0]]=point[now],now=f[now][0];
				}
				tong[++tong[0]]=point[lcaa];
			}else{
				now=x;
				while(now!=lcaa){
					tong[++tong[0]]=point[now],now=f[now][0];
				}
				now=y;
				while(now!=lcaa){
					tong[++tong[0]]=point[now],now=f[now][0];
				}
				tong[++tong[0]]=point[lcaa];
			}
			sort(tong+1,tong+tong[0]+1);
			bool yes=false;
			for(int j=1;j<=tong[0]-2;j++){
				if(tong[j]+tong[j+1]>tong[j+2]){
					cout<<"Y"<<endl;
					yes=true;
					break;
				}
			}
			if(!yes){
				cout<<"N"<<endl;
			}
		}else{
			point[x]=y;
		}
	}
	return 0;
}
/*

in:
5 5
1 2 3 4 5
1 2
2 3
3 4
1 5
1 1 3
1 4 5
2 1 4
1 2 5
1 2 3

out:
N
Y
Y
N

*/
