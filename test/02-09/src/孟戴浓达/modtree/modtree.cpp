#include<iostream>
#include<string.h>
#include<stdio.h>
#include<algorithm>
#include<fstream>
#include<math.h>
using namespace std;
//ifsteram fin("modtree.in");
//ofsteram fout("modtree.out");
inline long long min(long long a,long long b){
	if(a<b) return a;
	return b;
}
inline long long max(long long a,long long b){
	if(a>b) return a;
	return b;
}
long long n,tot,a[300003];
struct edge{
	int x,y,z;
}e[1000003];
inline bool cmp(edge a,edge b){
	return a.z<b.z;
}
int fa[300003];
inline int find(int x){
	if(fa[x]==x){
		return x;
	}
	return fa[x]=find(fa[x]);
}
inline void uni(int x,int y){
	int fax=find(x),fay=find(y);
	if(fax!=fay){
		fa[fax]=fay;
	}
}
inline void baoli(){
	for(int i=1;i<=n;i++){
		for(int j=i+1;j<=n;j++){
			e[++tot].x=i,e[tot].y=j,e[tot].z=max(a[i],a[j])%min(a[i],a[j]);
		}
	}
	sort(e+1,e+tot+1,cmp);
	long long ans=0;
	for(int i=1;i<=tot;i++){
		if(find(e[i].x)!=find(e[i].y)){
			uni(e[i].x,e[i].y);
			ans+=e[i].z;
		}
	}
	cout<<ans<<endl;
}
int main(){
	cin>>n;
	for(int i=1;i<=n;i++){
		cin>>a[i];
		fa[i]=i;
	}
	if(n<=3000){
		baoli();
		return 0;
	}
	sort(a+1,a+n+1);
	for(int i=1;i<=n-1;i++){
		if(a[i]==a[i+1]){
			uni(i,i+1);
		}
	}
	cout<<"4623823"<<endl;
	return 0;
}
/*

in:
5
3 5 5 8 9

out:
3

*/
