#include<iostream>
#include<string.h>
#include<stdio.h>
#include<algorithm>
#include<fstream>
#include<math.h>
using namespace std;
//ifsteram fin("segment.in");
//ofsteram fout("segment.out");
long long n,l[20003],r[20003];
long long jiyi[20003][2];
long long dp(int x,int now){
	long long &fanhui=jiyi[x][now];
	if(fanhui!=-1){
		return fanhui;
	}
	if(now==0){
		if(x==n){
			if(l[x-1]<=l[x]){
				fanhui=n-l[x];
			}else{
				fanhui=n-l[x]+(l[x-1]-l[x]);
			}
		}else{
			if(r[x]<l[x-1]){
				fanhui=l[x-1]-l[x]+dp(x+1,0)+1;
				fanhui=min(fanhui,l[x-1]-l[x]+r[x]-l[x]+dp(x+1,1)+1);
			}else if(l[x]<=l[x-1]&&r[x]>=l[x-1]){
				fanhui=l[x-1]-l[x]+(r[x]-l[x])+dp(x+1,1)+1;
				fanhui=min(fanhui,r[x]-l[x-1]+(r[x]-l[x])+dp(x+1,0)+1);
			}else if(l[x]>l[x-1]){
				fanhui=r[x]-l[x-1]+dp(x+1,1)+1;
			}
		}
	}else{
		if(x==n){
			if(r[x-1]<=l[x]){
				fanhui=n-l[x];
			}else{
				fanhui=n-l[x]+(r[x-1]-l[x]);
			}
		}else{
			if(r[x]<r[x-1]){
				fanhui=r[x-1]-l[x]+dp(x+1,0)+1;
				fanhui=min(fanhui,r[x-1]-l[x]+r[x]-l[x]+dp(x+1,1)+1);
			}else if(l[x]<=r[x-1]&&r[x]>=r[x-1]){
				fanhui=r[x-1]-l[x]+(r[x]-l[x])+dp(x+1,1)+1;
				fanhui=min(fanhui,r[x]-r[x-1]+(r[x]-l[x])+dp(x+1,0)+1);
			}else if(l[x]>r[x-1]){
				fanhui=r[x]-r[x-1]+dp(x+1,1)+1;
			}
		}
	}
	return fanhui;
}
int main(){
	cin>>n;
	for(int i=1;i<=n;i++){
		cin>>l[i]>>r[i];
	}
	memset(jiyi,-1,sizeof(jiyi));
	long long ans=min(dp(2,1)+r[1]-1,dp(2,0)+r[1]-l[1]+r[1]-1)+1;
	cout<<ans<<endl;
	return 0;
}
/*

in:
6
2 6
3 4
1 3
1 2
3 6
4 5

out:
24 

*/
