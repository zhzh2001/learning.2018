#include<bits/stdc++.h>
#define ll long long
using namespace std;
const int N=100005;
const int M=200005;
inline ll read(){ll x=0,f=1;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';return x*f;}
inline ll write(ll x){if(x<0){x=-x;putchar('-');}if(x>9)write(x/10);putchar(x%10+'0');}
inline ll writeln(ll x){write(x);puts("");}
int head[N],nxt[M],tail[M];
ll t,n,q,x,y,op;
inline void addto(int x,int y){nxt[++t]=head[x];head[x]=t;tail[t]=y;}
ll a[N],c[55],f[N],dep[N];
void dfs(int k,int fa)
{
    for(int i=head[k];i;i=nxt[i]){
        int x=tail[i];
        if(tail[i]==fa)continue;
        f[x]=k;
        dep[x]=dep[k]+1;
        dfs(x,k);
    }
}
bool getans(int x,int y)
{
    int t=0;
    if(dep[x]<dep[y])swap(x,y);
    while(dep[x]>dep[y]&&t<=50)c[++t]=a[x],x=f[x];
    while(x!=y&&t<=50){
        c[++t]=a[x];
        c[++t]=a[y];
        x=f[x],y=f[y];
    }
    if(x!=y)return true;
    c[++t]=a[x];
    sort(c+1,c+t+1);
    for(int i=1;i<t-1;i++)
        if(c[i]+c[i+1]>c[i+2])
            return true;
    return false;
}
int main()
{
	freopen("triangle.in","r",stdin);
	freopen("triangle.out","w",stdout);
    n=read();q=read();
    for(int i=1;i<=n;i++)a[i]=read();
    for(int i=1;i<n;i++){
        x=read();y=read();
        addto(x,y);
        addto(y,x);
    }
    dfs(1,0);f[1]=0;
    for(int i=1;i<=q;i++){
        op=read();
        if(op==1){
            x=read();y=read();
            if(getans(x,y))puts("Y");
            else puts("N");
        }
        else{
            x=read();y=read();
            a[x]=y;
        }
    }
    return 0;
}
