#include<bits/stdc++.h>
using namespace std;
const int N=20005;
int n;
int f[N][2];
int l[N],r[N];
int len[N];
inline int read()
{
	int x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')ch=getchar();
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
int main()
{
	freopen("segment.in","r",stdin);
	freopen("segment.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		l[i]=read();r[i]=read();
		len[i]=r[i]-l[i]+1;
	}
	memset(f,127/3,sizeof(f));
	f[1][0]=r[1]+r[1]-l[1]-1;
	f[1][1]=r[1]-1;
	for(int i=2;i<=n;i++){
		if(l[i-1]<r[i])f[i][0]=min(f[i][0],f[i-1][0]+r[i]-l[i-1]+len[i]);
		else f[i][0]=min(f[i][0],f[i-1][0]+l[i-1]-r[i]+len[i]);
		if(r[i-1]<r[i])f[i][0]=min(f[i][0],f[i-1][1]+r[i]-r[i-1]+len[i]);
		else f[i][0]=min(f[i][0],f[i-1][1]+r[i-1]-r[i]+len[i]);
		if(l[i-1]>l[i])f[i][1]=min(f[i][1],f[i-1][0]+l[i-1]-l[i]+len[i]);
		else f[i][1]=min(f[i][1],f[i-1][0]+l[i]-l[i-1]+len[i]);
		if(r[i-1]>l[i])f[i][1]=min(f[i][1],f[i-1][1]+r[i-1]-l[i]+len[i]);
		else f[i][1]=min(f[i][1],f[i-1][1]+l[i]-r[i-1]+len[i]);
	}
//	for(int i=1;i<=n;i++)
//		cout<<' '<<f[i][0]<<' '<<f[i][1]<<endl;
	printf("%d",min(f[n][0]+n-l[n],f[n][1]+n-r[n]));
	return 0;
}
