#include<bits/stdc++.h>
#define ll long long
using namespace std;
const int N=300005;
const int M=1e7+5;
inline ll read(){ll x=0,f=1;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';return x*f;}
inline ll write(ll x){if(x<0){x=-x;putchar('-');}if(x>9)write(x/10);putchar(x%10+'0');}
inline ll writeln(ll x){write(x);puts("");}
ll ans;
int n,m,t,k,tot;
int nxt[M],vis[N];
int f[N],a[N];
struct xxx{
	int x,y,t;
}e[8*N];
inline bool com(xxx a,xxx b)
{
	return a.t<b.t;
}
inline int gf(int k)
{
	if(f[k]!=k)f[k]=gf(f[k]);
	return f[k];
}
int main()
{
	freopen("modtree.in","r",stdin);
	freopen("modtree.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)
		a[i]=read(),f[i]=i;
	sort(a+1,a+n+1);int l=0;
	if(a[1]==1){puts("0");return 0;} 
	for(int i=1;i<=n;i++)
		if(a[i]!=a[i+1])a[++l]=a[i],nxt[a[i]]=l;
	n=l;m=a[n];
	for(int i=1;i<n;i++)
		e[++tot]=(xxx){i,i+1,a[i+1]%a[i]};
	for(int i=m;i>=1;i--)
		if(!nxt[i])
			nxt[i]=nxt[i+1];
	for(int i=1;i<n;i++){
		if(vis[i])continue;
		k=1;t=a[i];
		while(t<=m){
			int now=a[nxt[t]]%a[i];
			if(now==0)vis[nxt[t]]=true;
			e[++tot]=(xxx){i,nxt[t],now};
			k=a[nxt[t]]/a[i]+1;
			t=a[i]*k;
		}
	}
	sort(e+1,e+tot+1,com);
	for(int i=1;i<=tot;i++){
		int fa=gf(e[i].x);
		int fb=gf(e[i].y);
		if(fa!=fb){
			f[fa]=fb;
			ans+=e[i].t;
		}
	}
	writeln(ans);
}
