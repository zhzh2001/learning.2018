#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 100005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,q,tot,a[N],val[N],head[N],deep[N],fa[N][20];
struct edge{ ll to,next; }e[N<<1];
void add(ll u,ll v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
}
void dfs1(ll u,ll last){
	rep(i,1,17) fa[u][i]=fa[fa[u][i-1]][i-1];
	for (ll i=head[u],v=e[i].to;i;i=e[i].next,v=e[i].to){
		if (v==last) continue;
		deep[v]=deep[u]+1; fa[v][0]=u; dfs1(v,u);
	}
}
ll lca(ll u,ll v){
	if (deep[u]<deep[v]) swap(u,v);
	ll tmp=deep[u]-deep[v];
	rep(i,0,17) if (tmp&(1<<i)) u=fa[u][i];
	per(i,17,0) if (fa[u][i]!=fa[v][i])
		u=fa[u][i],v=fa[v][i];
	return (v==u)?u:fa[u][0];
}
int main(){
	freopen("triangle.in","r",stdin);
	freopen("triangle.out","w",stdout);
	n=read(); q=read();
	rep(i,1,n) val[i]=read();
	rep(i,2,n){
		ll u=read(),v=read();
		add(u,v); add(v,u);
	}
	dfs1(1,-1);
	rep(i,1,q){
		ll opt=read(),u=read(),v=read();
		if (opt==2) { val[u]=v; continue; }
		ll tmp=lca(u,v),cnt=0,flag=0;
		if (deep[u]+deep[v]-2*deep[tmp]+1>50) { puts("Y"); continue; }
		while (u!=tmp) a[++cnt]=val[u],u=fa[u][0];
		while (v!=tmp) a[++cnt]=val[v],v=fa[v][0];
		a[++cnt]=val[tmp]; sort(a+1,a+1+cnt);
		rep(i,1,cnt-2) if (a[i]+a[i+1]>a[i+2]) { flag=1; break; }
		puts(flag?"Y":"N");
	}
}
