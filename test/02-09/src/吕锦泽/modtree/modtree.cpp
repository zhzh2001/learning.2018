#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 300005
#define M 1200005
#define K 10000005
#define inf (1e9)
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,ans,now,tot,fa[M],f[K],g[K],a[N],num[N];
struct edge{ ll u,v,w; }e[M*3];
ll find(ll x) { return fa[x]==x?x:fa[x]=find(fa[x]); }
bool cmp(edge x,edge y) { return x.w<y.w; }
void add(ll u,ll v,ll w) { e[++tot]=(edge){u,v,w}; }
int main(){
	freopen("modtree.in","r",stdin);
	freopen("modtree.out","w",stdout);
	n=m=read(); rep(i,1,n) a[i]=read(); sort(a+1,a+1+n);
	rep(i,1,n) f[a[i]]=i;
	per(i,a[n],a[1]) if (!f[i]) f[i]=f[i+1]; 
	rep(i,2,n) add(i,i-1,num[i-1]=a[i]%a[i-1]);
	rep(i,1,n){
		if (a[i]==a[i-1]) continue;
		for (ll j=2;a[i]*j<=a[n];++j){
			if (a[f[a[i]*j]]-a[i]*j>num[i]) continue;
			while (tot&&e[tot].u==i&&e[tot].w>a[f[a[i]*j]]-a[i]*j) --tot;
			if (!g[a[i]*j]) ++m,g[a[i]*j]=m; add(i,g[a[i]*j],0);
		}
	}
	per(i,a[n],a[1]){
		if (f[i]) now=f[i];
		if (g[i]) add(now,g[i],a[f[i]]-i);
	}
	sort(e+1,e+1+tot,cmp);
	rep(i,1,m) fa[i]=i;
	rep(i,1,tot){
		ll u=e[i].u,v=e[i].v,w=e[i].w;
		ll p=find(u),q=find(v);
		if (p!=q) fa[p]=q,ans+=w;
	}
	printf("%lld",ans);
}
