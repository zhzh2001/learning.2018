#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 20005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,lastl,lastr,nowl,nowr,anl,anr;
int main(){
	freopen("segment.in","r",stdin);
	freopen("segment.out","w",stdout);
	n=read(); lastl=lastr=1;
	rep(i,1,n){
		ll x=anl,y=anr; nowl=read(); nowr=read();
		anl=min(abs(nowr-lastr)+y,abs(nowr-lastl)+x)+nowr-nowl;
		anr=min(abs(nowl-lastr)+y,abs(nowl-lastl)+x)+nowr-nowl;
		lastl=nowl; lastr=nowr;
	}
	printf("%lld",min(anl-nowl,anr-nowr)+n*2-1);
}
