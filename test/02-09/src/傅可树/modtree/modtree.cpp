#include<cstdio>
#include<algorithm>
using namespace std;
typedef long long ll;
int n;
inline ll read(){
	int x=0,f=1; char c=getchar();
	while (c<'0'||c>'9'){
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
const int maxn=300005;
ll a[maxn];
inline void init(){
	scanf("%d",&n);
	for (int i=1;i<=n;i++){
		a[i]=read();
	}
	sort(a+1,a+1+n);
}
const int maxm=5000000;
struct edge{
	ll u,v,w;
}e[maxm];
ll tot,ans,fa[maxn];
int getfa(int x){
	return (fa[x]==x)?x:fa[x]=getfa(fa[x]);
}
inline bool cmp(edge x,edge y){
	return x.w<y.w;
}
inline void solve(){
	for (int i=1;i<n;i++){
		for (int j=i+1;j<=n;j++){
			e[++tot]=(edge){i,j,a[j]%a[i]};
		}
	}
	sort(e+1,e+1+tot,cmp);
	for (int i=1;i<=n;i++) fa[i]=i;
	int cnt=0;
	for (int i=1;i<=tot;i++){
		int u=e[i].u,v=e[i].v;
		u=getfa(u); v=getfa(v);
		if (u!=v){
			ans+=e[i].w;
			fa[u]=v;
			++cnt;
		}
		if (cnt==n-1) break;
	}
	printf("%lld\n",ans);
}
int main(){
	freopen("modtree.in","r",stdin);
	freopen("modtree.out","w",stdout);
	init();
	solve();
	return 0;
}
