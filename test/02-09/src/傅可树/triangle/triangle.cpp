#include<cstdio>
#include<algorithm>
using namespace std;
const int maxn=1e5+5,maxm=2e5+5;
struct edge{
	int link,next;
}e[maxm];
int n,Q,V[maxn],head[maxn],tot;
inline int read(){
	int x=0,f=1; char c=getchar();
	while (c<'0'||c>'9'){
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
inline void add(int u,int v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot;
}
inline void insert(int u,int v){
	add(u,v); add(v,u);
}
inline void init(){
	n=read(); Q=read();
	for (int i=1;i<=n;i++){
		V[i]=read();
	}
	for (int i=1;i<n;i++){
		int u=read(),v=read();
		insert(u,v);
	}
}
int fa[maxn][20],dep[maxn];
void dfs(int u,int f){
	fa[u][0]=f; dep[u]=dep[f]+1;
	for (int i=1;i<20;i++){
		fa[u][i]=fa[fa[u][i-1]][i-1];
	}
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].link;
		if (v!=f){
			dfs(v,u);
		}
	}
}
inline int lca(int u,int v){
	if (dep[u]<dep[v]) swap(u,v);
	int delta=dep[u]-dep[v];
	for (int i=0;i<20;i++){
		if (delta&(1<<i)){
			u=fa[u][i];
		}
	}
	for (int i=19;i>=0;i--){
		if (fa[u][i]!=fa[v][i]){
			u=fa[u][i];
			v=fa[v][i];
		}
	}
	if (u!=v){
		return fa[u][0];
	}else{
		return u;
	}
}
int q[maxn];
inline bool judge(){
	sort(q+1,q+1+tot);
	for (int i=1;i<=tot-2;i++){
		int sum=q[i]+q[i+1];
		if (sum>q[i+2]){
			return 1;
		}
	}
	return 0;
}
inline void solve(){
	dfs(1,0); 
	for (int i=1;i<=Q;i++){
		int opt=read(),x=read(),y=read();
		if (opt==1){
			int LCA=lca(x,y); tot=0;
			for (;x!=LCA;x=fa[x][0]) q[++tot]=V[x];
			for (;y!=LCA;y=fa[y][0]) q[++tot]=V[y];
			q[++tot]=V[LCA];
			if (judge()) puts("Y");
				else puts("N");
		}else{
			V[x]=y;
		}
	}
}
int main(){
	freopen("triangle.in","r",stdin);
	freopen("triangle.out","w",stdout);
	init();
	solve();
	return 0;
}
