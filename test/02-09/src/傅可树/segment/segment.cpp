#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
const int maxn=20005;
int n,s[maxn][2];
inline void init(){
	scanf("%d",&n);
	for (int i=1;i<=n;i++){
		scanf("%d%d",&s[i][0],&s[i][1]);
	}
}
inline void update(int &x,int v){
	x=min(x,v);
}
int dp[maxn][2];
inline int abs(int x){
	return (x>0)?x:-x;
}
inline int dis(int x,int y,int k1,int k2){
	return abs(s[x][k1]-s[y][k2]);
}
const int inf=1e9;
inline void solve(){
	memset(dp,127/3,sizeof(dp));
	dp[1][0]=(s[1][1]-1)+dis(1,1,0,1);
	dp[1][1]=(s[1][1]-1);
	for (int i=1;i<n;i++){
		for (int j=0;j<2;j++){
			for (int k=0;k<2;k++){
				update(dp[i+1][j],dp[i][k]+dis(i,i+1,k,j^1)+1+dis(i+1,i+1,0,1));
			}
		}
	}
	int ans=inf;
	update(ans,dp[n][0]+(n-s[n][0]));
	update(ans,dp[n][1]+(n-s[n][1]));
	printf("%d\n",ans);
}
int main(){
	freopen("segment.in","r",stdin);
	freopen("segment.out","w",stdout);
	init();
	solve();
	return 0;
}
