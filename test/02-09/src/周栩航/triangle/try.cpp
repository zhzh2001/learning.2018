
#include<iostream>
#include<cstdio>
#include<cmath>
#include<queue>
#include<cstring>
#include<algorithm>
#define For(i,j,k)  for(ll i=j;i<=k;++i)
#define Dow(i,j,k)  for(ll i=k;i>=j;--i)
#define ll long long
#define mk make_pair
#define eps 1e-8
using namespace std;
inline ll read()
{
    ll t=0,f=1;char c=getchar();
    while(c<'0'||c>'9')   {if(c=='-') f=-1;c=getchar();}
    while(c>='0'&&c<='9') t=t*10+c-48,c=getchar();
    return t*f;
}
int poi[20001],nxt[20001],head[20001],Fa[20001][21],Q[20001],top,n,m,q,cnt,dep[20001],a[20001],opt,x,y;
inline void add(int x,int y){poi[++cnt]=y;nxt[cnt]=head[x];head[x]=cnt;poi[++cnt]=x;nxt[cnt]=head[y];head[y]=cnt;}
inline void Dfs(int x,int fa)
{	
	Fa[x][0]=fa;
	for(int i=head[x];i;i=nxt[i])
	{
		if(poi[i]==fa)	continue;dep[poi[i]]=dep[x]+1;
		Dfs(poi[i],x);
	}
}
inline void Got() 	{For(i,1,20)	For(j,1,n)	Fa[j][i]=Fa[Fa[j][i-1]][i-1];}
inline int lca(int x,int y)
{
	if(dep[x]<dep[y])	swap(x,y);
	int dis=dep[x]-dep[y],now=0;
	Dow(i,0,20) if(dis>>i&1)	x=Fa[x][i];
	if(x==y)	return x;
	Dow(i,0,20)	if(Fa[x][i]!=Fa[y][i])	x=Fa[x][i],y=Fa[y][i];
	return Fa[x][0];
}
int main()
{
	freopen("triangle.in","r",stdin);
	freopen("triangle1.out","w",stdout);
	n=read();q=read();	//puts("..");
	For(i,1,n)	a[i]=read();
	For(i,1,n-1)	add(read(),read());
	dep[1]=1;
	Dfs(1,1);
	Got();

	For(i,1,q)
	{
		opt=read();x=read();y=read();
		if(opt==1)
		{
			top=0;
			int t=lca(x,y);
			
			while(x!=t)
			{
				Q[++top]=a[x];
				x=Fa[x][0];
			}
			while(1)
			{
				Q[++top]=a[y];//cout<<x<<' '<<y<<'-'<<t<<' '<<y<<' '<<Fa[y][0]<<endl;
				if(t==y)	break;
				y=Fa[y][0];
			}
			sort(Q+1,Q+top+1);
			bool flag=0;
			For(i,1,top-2)	if(Q[i]+Q[i+1]>Q[i+2])	flag=1;
			if(flag)	puts("Y");else puts("N");
		}else
			a[x]=y;
	}
}
