#include<iostream>
#include<cstdio>
#include<cmath>
#include<queue>
#include<cstring>
#include<algorithm>
#define For(i,j,k)  for(ll i=j;i<=k;++i)
#define Dow(i,j,k)  for(ll i=k;i>=j;--i)
#define ll long long
#define mk make_pair
#define eps 1e-8
#define inf 1e9;
using namespace std;
inline ll read()
{
    ll t=0,f=1;char c=getchar();
    while(c<'0'||c>'9')   {if(c=='-') f=-1;c=getchar();}
    while(c>='0'&&c<='9') t=t*10+c-48,c=getchar();
    return t*f;
}
ll l[100001],r[100001],dp[100001][2],n;
int main()
{
	freopen("segment.in","r",stdin);freopen("segment.out","w",stdout);
	n=read();
	For(i,1,n)	l[i]=read(),r[i]=read();
	r[n]=n;
	l[0]=1;r[0]=n;dp[0][1]=inf;
	For(i,1,n)	dp[i][1]=dp[i][0]=inf;
	For(i,1,n)
	{
		dp[i][0]=min(dp[i][0],abs(l[i-1]-r[i])+abs(r[i]-l[i])+dp[i-1][0]);
		dp[i][0]=min(dp[i][0],abs(r[i-1]-r[i])+abs(r[i]-l[i])+dp[i-1][1]);
		dp[i][1]=min(dp[i][1],abs(l[i-1]-l[i])+abs(r[i]-l[i])+dp[i-1][0]);
		dp[i][1]=min(dp[i][1],abs(r[i-1]-l[i])+abs(r[i]-l[i])+dp[i-1][1]);
	}
	cout<<dp[n][1]+n-1<<endl;
}
