#include<iostream>
#include<cstdio>
#include<cmath>
#include<queue>
#include<cstring>
#include<algorithm>
#define For(i,j,k)  for(int i=j;i<=k;++i)
#define Dow(i,j,k)  for(int i=k;i>=j;--i)
#define ll long long
#define mk make_pair
#define pb push_back
#define eps 1e-8
#define pa pair<int,int>
#define fir first
#define sec second
using namespace std;
inline ll read()
{
    ll t=0,f=1;char c=getchar();
    while(c<'0'||c>'9')   {if(c=='-') f=-1;c=getchar();}
    while(c>='0'&&c<='9') t=t*10+c-48,c=getchar();
    return t*f;
}
int n,a[200001],Fa[200001],vis[200001],ans,tot,mx;
inline int get(int x){return x==Fa[x]?x:Fa[x]=get(Fa[x]);}
inline void Get(int x,int y,int v)
{
	int tx=get(x),ty=get(y);
//	cout<<x<<' '<<y<<' '<<v<<endl;
	if(tx!=ty)	Fa[tx]=ty,ans+=v,++tot;
}
vector<int> hav[100001];
int main()
{
	freopen("modtree.in","r",stdin);
	freopen("modtree1.out","w",stdout);
	n=read();
	For(i,1,n)	a[i]=read(),mx=max(mx,a[i]),vis[i]=-1,hav[a[i]].pb(i);
	sort(a+1,a+n+1);
	int l=1;
	For(i,1,n)	Fa[i]=i;
	For(i,0,mx)	
	{
		while(a[l]<=i)	++l;
		For(j,l,n)
		{
//			if(vis[j]!=i)	
//			{
				if(a[j]<=1000||i==0)
				{
					For(k,j+1,n)
					{
						int tmp=(a[k]-i)%a[j];
	//					cout<<' '<<j<<' '<<k<<' '<<tmp<<' '<<i<<endl;
						if(tmp==0)	Get(j,k,i);
						if(tot==n-1)	{cout<<ans<<endl;return 0;}
						if(a[k]%a[j]==0)	vis[k]=i;
					}
				}
				else
				{
					int now=a[j]+i;
					while(now<=mx)
					{
						for(int t=0;t<hav[now].size();++t)	Get(j,hav[now][t],i);
						if(tot==n-1){cout<<ans<<endl;return 0;}
						now+=a[j];
					}
				}
//			}
		}
	}
}
