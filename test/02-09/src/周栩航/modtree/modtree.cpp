#include<iostream>
#include<cstdio>
#include<cmath>
#include<queue>
#include<cstring>
#include<algorithm>
#define For(i,j,k)  for(int i=j;i<=k;++i)
#define Dow(i,j,k)  for(int i=k;i>=j;--i)
#define ll long long
#define mk make_pair
#define eps 1e-8
#define pa pair<int,int>
#define fir first
#define sec second
using namespace std;
inline ll read()
{
    ll t=0,f=1;char c=getchar();
    while(c<'0'||c>'9')   {if(c=='-') f=-1;c=getchar();}
    while(c>='0'&&c<='9') t=t*10+c-48,c=getchar();
    return t*f;
}
int n,a[200001],tot,Fa[200001];
ll ans;
struct edge{int x,y,v;}	e[2000001];
bool cmp(edge x,edge y){return x.v<y.v;}
inline int get(int x){return x==Fa[x]?x:Fa[x]=get(Fa[x]);}
int main()
{
	freopen("modtree.in","r",stdin);
	freopen("modtree.out","w",stdout);
	n=read();
	For(i,1,n)	a[i]=read(),Fa[i]=i;
	For(i,1,n)	For(j,i+1,n)	e[++tot].x=i,e[tot].y=j,e[tot].v=max(a[i],a[j])%min(a[i],a[j]);
	sort(e+1,e+tot+1,cmp);
	For(i,1,tot)
	{
		int tx=get(e[i].x),ty=get(e[i].y);
		if(tx==ty)	continue;
		Fa[tx]=ty;
		ans+=1LL*e[i].v;
		tot++;
		if(tot==n-1)	break;
	}
	cout<<ans<<endl;
}
