#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int L = 200005;
char STR[L],*S = STR,*T = STR;
inline char gc(){
	if(S == T){
		T = (S = STR) + fread(STR,1,L,stdin);
		if(S == T) return EOF;
	}
	return *S++;
}

inline ll read(){
	ll x = 0,f = 1;char ch = gc();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = gc();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = gc();}
	return x*f;
}
const ll N = 20005;
ll x[N],y[N];
ll n,ans;
ll f[N][2];		// 0 : left,  1 = right;
inline ll Abs(ll a){
	if(a < 0) return -a;
	return a;
}

inline void Min(ll &a,ll b){
	if(a > b) a = b;
}

signed main(){
	freopen("segment.in","r",stdin);
	freopen("segment.out","w",stdout);
	n = read();
	for(ll i = 1;i <= n;++i){
		x[i] = read();
		y[i] = read();
	}
	f[1][0] = y[1]-1+(y[1]-x[1]);
	f[1][1] = y[1]-1;
	for(ll i = 2;i <= n;++i){
		// left
		f[i][0] = (1+f[i-1][0]+Abs(y[i]-x[i-1])+y[i]-x[i]);
		Min(f[i][0],1+f[i-1][1]+Abs(y[i]-y[i-1])+y[i]-x[i]);
		//right
		f[i][1] = (1+f[i-1][0]+Abs(x[i]-x[i-1])+y[i]-x[i]);
		Min(f[i][1],1+f[i-1][1]+Abs(x[i]-y[i-1])+y[i]-x[i]);
	}
	ans = 1LL<<58;
	Min(ans,f[n][0]+n-x[n]);
	Min(ans,f[n][1]+n-y[n]);
	printf("%lld\n", ans);
	return 0;
}