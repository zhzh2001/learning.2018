#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int LEN = 200005;
char STR[LEN],*S = STR,*T = STR;
inline char gc(){
	if(S == T){
		T = (S = STR) + fread(STR,1,LEN,stdin);
		if(S == T) return EOF;
	}
	return *S++;
}
// #define gc getchar
inline int read(){
	int x = 0,f = 1;char ch = gc();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = gc();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = gc();}
	return x*f;
}
#define Min(a,b) ((a)>(b)?(b):(a))
#define Max(a,b) ((a)>(b)?(a):(b))
const int N = 300005,M = 10000001,Q = 10000001;
int n,num,p,cnt,EDGE,Mi,L,R;
int f[N];
int a[N],d[N];
struct node{
	int a,x;
	bool flag;
	friend bool operator <(node a,node b){
		return a.a < b.a || (a.a == b.a && b.flag);
	}
}x[M];
struct edge{
	int a,b,w;
	friend bool operator <(edge a,edge b){
		return a.w < b.w;
	}
}e[Q];

int getf(int x){
	if(x == f[x]) return x;
	return f[x] = getf(f[x]);
}

inline void minst(){
	p = Min(100,n); L = 0; cnt = 0;
	// printf("%d %d\n", p,n);
	for(int i = 1;i <= p;++i){
		if(a[i]/Mi >= 10000) break;
		++L;
	}
	// printf("%d\n", L);
	for(int i = 1;i <= L;++i){
		int Mx = a[L]/a[i];
		x[++cnt] = (node){a[i],i,true};
		for(int j = 2;j <= Mx;++j)
			x[++cnt] = (node){a[i]*j,i,false};
	}
	sort(x+1,x+cnt+1);
	// for(int i = 1;i <= cnt;++i){
	// 	printf("%d %d %d\n",x[i].a,x[i].x,x[i].flag);
	// }
	// puts("");
	int past = cnt;
	for(int i = cnt-1;i;--i){
		if(x[i].flag){
			if(past != 0) e[++EDGE] = (edge){x[i].x,x[past].x,x[past].a-x[i].a};
			past = i;
		}
		else{
			if(past != 0){
				int val = x[past].a-x[i].a;
				if(val >= Mi){
					past = 0;
					continue;
				}
				e[++EDGE] = (edge){x[i].x,x[past].x,val};
			}
		}
	}
	for(int i = 1;i <= L;++i)
		for(int j = L+1;j <= n;++j){
			int val = a[j] %a[i];
			e[++EDGE] = (edge){i,j,val};
		}
}

inline void mxnst(){
	cnt = 0;
	p = Min(100,n-L);
	// printf("%d\n", p);
	R = n+1;
	for(int i = n;i >= n-p+1;--i){
		if(a[n]/a[i] >= 10000) break;
		--R;
	}
	// printf("%d\n", R);
	if(R == n+1) return;
	for(int i = R;i <= n;++i){
		int Mx = a[n]/a[i];
		x[++cnt] = (node){a[i],i,true};
		for(int j = 2;j <= Mx;++j)
			x[++cnt] = (node){a[i]*j,i,false};
	}
	sort(x+1,x+cnt+1);
	int past = cnt;
	for(int i = cnt-1;i;--i){
		if(x[i].flag){
			if(past != 0) e[++EDGE] = (edge){x[i].x,x[past].x,x[past].a-x[i].a};
			past = i;
		}
		else{
			if(past != 0){
				int val = x[past].a-x[i].a;
				if(val >= Mi){
					past = 0;
					continue;
				}
				e[++EDGE] = (edge){x[i].x,x[past].x,val};
			}
		}
	}
	for(int i = R;i <= n;++i)
		for(int j = L+1;j < R;++j) {
			int val = a[i] %a[j];
			e[++EDGE] = (edge){i,j,val};
		}
}

inline void getal(){
	cnt = 0;
	// printf("%d %d\n", L,R);
	if(L == R-1) return;
	int rr = R-1;
	for(int i = L+1;i < R;++i){
		int Mx = a[rr]/a[i];
		x[++cnt] = (node){a[i],i,true};
		for(int j = 2;j <= Mx;++j)
			x[++cnt] = (node){a[i]*j,i,false};
	}
	sort(x+1,x+cnt+1);
	int past = cnt;
	// printf("%d\n", cnt);
	for(int i = cnt-1;i;--i){
		if(x[i].flag){
			if(past != 0) e[++EDGE] = (edge){x[i].x,x[past].x,x[past].a-x[i].a};
			past = i;
		}
		else{
			if(past != 0){
				int val = x[past].a-x[i].a;
				if(val >= Mi){
					past = 0;
					continue;
				}
				e[++EDGE] = (edge){x[i].x,x[past].x,val};
			}
		}
	}
}



inline void getans(){
	sort(e+1,e+EDGE+1);
	ll ans = 0;
	int num = 1;
	for(int i = 1;i <= n;++i) f[i] = i;
	for(int i = 1;i <= EDGE;++i){
		// printf("%d %d %d\n", e[i].a,e[i].b,e[i].w);
		int fa = getf(e[i].a),fb = getf(e[i].b);
		if(fa == fb) continue;
		f[fa] = fb;
		ans += e[i].w;
		++num;
		if(num == n) break;
	}
	printf("%lld\n", ans);
}

int main(){
	freopen("modtree.in","r",stdin);
	freopen("modtree.out","w",stdout);
	n = read();
	for(int i = 1;i <= n;++i) a[i] = read();
	sort(a+1,a+n+1);
	if(a[1] == 1){
		printf("0\n");
		return 0;
	}
	Mi = a[1];
	num = 1;
	for(int i = 2;i <= n;++i)
		if(a[i] != a[i-1])
			a[++num] = a[i];
	n = num;
	if(n == 1){
		printf("0\n");
		return 0;
	}
	minst();
	mxnst();
	getal();
	getans();
	return 0;
}
