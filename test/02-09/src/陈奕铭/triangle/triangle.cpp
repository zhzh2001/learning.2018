#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int L = 200005;
char STR[L],*S = STR,*T = STR;
inline char gc(){
	if(S == T){
		T = (S = STR) + fread(STR,1,L,stdin);
		if(S == T) return EOF;
	}
	return *S++;
}
// #define gc getchar
inline int read(){
	int x = 0,f = 1;char ch = gc();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = gc();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = gc();}
	return x*f;
}

const int N = 100005;
int n,q;
int a[N],deep[N];
int ST[N][18];
int head[N],to[N*2],nxt[N*2],cnt;
int ax[50],top;

inline void insert(int x,int y){
	to[++cnt] = y; nxt[cnt] = head[x]; head[x] = cnt;
}

void dfs1(int x,int f){
	for(int i = head[x];i;i = nxt[i])
		if(to[i] != f){
			int v = to[i];
			ST[v][0] = x;
			deep[v] = deep[x]+1;
			dfs1(v,x);
		}
}

inline void getST(){
	for(int j = 1;j < 18;++j)
		for(int i = 1;i <= n;++i)
			ST[i][j] = ST[ST[i][j-1]][j-1];
}

inline int getlca(int x,int y){
	if(deep[x] < deep[y]) swap(x,y);
	int num = deep[x]-deep[y];
	for(int j = 17;j >= 0;--j)
		if((num>>j)&1)
			x = ST[x][j];
	if(x == y) return x;
	for(int j = 17;j >= 0;--j)
		if(ST[x][j] != ST[y][j]){
			x = ST[x][j];
			y = ST[y][j];
		}
	return ST[x][0];
}

int main(){
	freopen("triangle.in","r",stdin);
	freopen("triangle.out","w",stdout);
	n = read(); q = read();
	for(int i = 1;i <= n;++i) a[i] = read();
	for(int i = 1;i < n;++i){
		int x = read(),y = read();
		insert(x,y); insert(y,x);
	}
	deep[1] = 1; ST[1][0] = 1;
	dfs1(1,1);
	getST();
	for(int i = 1;i <= q;++i){
		int opt = read(),x = read(),y = read();
		if(opt == 1){	// max len+1 >= 45
			int lca = getlca(x,y);
			// printf("%d\n", lca);
			int len = deep[x]+deep[y]-deep[lca]*2+1;
			if(len >= 45){
				puts("Y");
				continue;
			}
			if(len <= 2){
				puts("N");
				continue;
			}
			top = 0;
			while(x != lca){
				ax[++top] = a[x];
				x = ST[x][0];
			}
			while(y != lca){
				ax[++top] = a[y];
				y = ST[y][0];
			}
			ax[++top] = a[lca];
			sort(ax+1,ax+top+1);
			bool flag = false;
			for(int j = 3;j <= top;++j)
				if(ax[j] < ax[j-1]+ax[j-2]){
					flag = true;
					break;
				}
			if(flag) puts("Y");
			else puts("N");
		}
		else a[x] = y;
	}
	return 0;
}