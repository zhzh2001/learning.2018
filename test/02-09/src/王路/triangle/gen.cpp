#include <bits/stdc++.h>
#include <Windows.h>
using namespace std;

inline int Rand32() { return (rand() << 15) + rand(); }

inline int Uniform(int l, int r) {
  return Rand32() % (r - l + 1) + l;
}

struct DisjointSet {
  int fa[2333];
  inline void Init() {
    for (int i = 0; i < 2333; ++i)
      fa[i] = i;
  }
  inline int Find(int x) { return x == fa[x] ? x : fa[x] = Find(fa[x]); }
  inline bool Same(int x, int y) { return Find(x) == Find(y); }
  inline bool Merge(int x, int y) {
    int fx = Find(x), fy = Find(y);
    if (fx != fy)
      return fa[fx] = fy, true;
    return false;
  }
} uf;

int main() {
  ios::sync_with_stdio(false);
  srand(GetTickCount());
  freopen("triangle.in", "w", stdout);
  int n = 5, q = 5;
  cout << n << ' ' << q << endl;
  for (int i = 1; i <= n; ++i)
    cout << Uniform(1, n) << ' ';
  cout << endl;
  uf.Init();
  for (int i = 1; i < n; ++i) {
    int u = Uniform(1, n), v;
    do {
      v = Uniform(1, n);
    } while (uf.Same(u, v));
    uf.Merge(u, v);
    cout << u << ' ' << v << endl;
  }
  for (int i = 1; i <= q; ++i) {
    int op = Uniform(0, 3);
    if (op > 0) {
      cout << 1 << ' ' << Uniform(1, n) << ' ' << Uniform(1, n) << endl;
    } else {
      cout << 2 << ' ' << Uniform(1, n) << ' ' << Uniform(1, n) << endl; 
    }
  }
  return 0;
}
