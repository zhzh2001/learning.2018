#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <ctime>
using namespace std;

template <typename T>
inline void Read(T &x) {
  x = 0;
  T flag = 1;
  char ch;
  for (ch = getchar(); isspace(ch); ch = getchar())
    ;
  if (ch == '-') {
    ch = getchar();
    flag = -1;
  }
  for (; isdigit(ch); ch = getchar())
    x = x * 10 + ch - '0';
  x *= flag;
}

const int kMaxN = 1e5 + 5;

int n, q, a[kMaxN], head[kMaxN], dep[kMaxN], fa[kMaxN], ecnt, b[kMaxN], bcnt;
struct Edge { int to, nxt; } e[kMaxN << 1];

inline void AddEdge(int u, int v) {
  e[++ecnt] = (Edge) { v, head[u] };
  head[u] = ecnt;
}

inline void Solve(int x, int f = 0) {
  dep[x] = dep[f] + 1;
  for (int i = head[x]; i; i = e[i].nxt) {
    if (e[i].to == f)
      continue;
    fa[e[i].to] = x;
    Solve(e[i].to, x);
  }
}

inline int Brute1() {
  for (int i = 1; i <= n; ++i) {
    Read(a[i]);
  }
  for (int i = 1, x, y; i < n; ++i) {
    Read(x), Read(y);
    AddEdge(x, y);
    AddEdge(y, x);
  }
  Solve(1);
  for (int i = 1, op, x, y; i <= q; ++i) {
    Read(op), Read(x), Read(y);
    if (op == 2)
      a[x] = y;
    else {
      bcnt = 0;
      while (dep[x] > dep[y])
        b[++bcnt] = a[x], x = fa[x];
      while (dep[x] < dep[y])
        b[++bcnt] = a[y], y = fa[y];
      while (x != y)
        b[++bcnt] = a[x], b[++bcnt] = a[y], x = fa[x], y = fa[y];
      b[++bcnt] = a[x];
      sort(b + 1, b + bcnt + 1);
      bool mark = false;
      for (int i = 2; i <= bcnt - 1; ++i) {
        if (b[i - 1] + b[i] > b[i + 1]) {
          mark = true;
          break;
        }
      }
      puts(mark ? "Y" : "N");
    }
  }
  return 0;
}

inline int Brute2() {

  return 0;
}

int main() {
  freopen("triangle.in", "r", stdin);
  freopen("triangle.out", "w", stdout);
  memset(head, 0x00, sizeof head);
  Read(n), Read(q);
  return Brute1();
}
