#include <cstdio>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <cstdlib>
using namespace std;

template <typename T>
inline void Read(T &x) {
  x = 0;
  T flag = 1;
  char ch;
  for (ch = getchar(); isspace(ch); ch = getchar())
    ;
  if (ch == '-') {
    ch = getchar();
    flag = -1;
  }
  for (; isdigit(ch); ch = getchar())
    x = x * 10 + ch - '0';
  x *= flag;
}

const int kMaxN = 20005;

typedef long long ll;

int n;
ll l[kMaxN], r[kMaxN];
ll f[kMaxN][2];

int main() {
  freopen("segment.in", "r", stdin);
  freopen("segment.out", "w", stdout);
  Read(n);
  for (int i = 1; i <= n; ++i) {
    Read(l[i]), Read(r[i]);
  }
  f[1][0] = r[1] - l[1] + r[1] - 1;
  f[1][1] = r[1] - 1;
  for (int i = 2; i <= n; ++i) {
    f[i][0] = 1LL + min(abs(r[i] - l[i - 1]) + f[i - 1][0], abs(r[i] - r[i - 1]) + f[i - 1][1]) + r[i] - l[i];
    f[i][1] = 1LL + min(abs(l[i] - l[i - 1]) + f[i - 1][0], abs(l[i] - r[i - 1]) + f[i - 1][1]) + r[i] - l[i];
  }
  printf("%lld\n", min((ll)n - l[n] + f[n][0], (ll)n - r[n] + f[n][1]));
  return 0;
}
