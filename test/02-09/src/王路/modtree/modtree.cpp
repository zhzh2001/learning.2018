#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
using namespace std;

template <typename T>
inline void Read(T &x) {
  x = 0;
  T flag = 1;
  char ch;
  for (ch = getchar(); isspace(ch); ch = getchar())
    ;
  if (ch == '-') {
    ch = getchar();
    flag = -1;
  }
  for (; isdigit(ch); ch = getchar())
    x = x * 10 + ch - '0';
  x *= flag;
}

const int kMaxN = 1005;

int n, a[kMaxN], ecnt;

struct Edge { int u, v, w; } e[kMaxN * kMaxN];

inline bool operator<(const Edge &a, const Edge &b) {
  return a.w < b.w;
}

struct DisjointSet {
  int fa[kMaxN];
  inline void Init() {
    for (int i = 0; i < kMaxN; ++i)
      fa[i] = i;
  }
  inline int Find(int x) { return x == fa[x] ? x : fa[x] = Find(fa[x]); }
  inline bool Same(int x, int y) { return Find(x) == Find(y); }
  inline bool Merge(int x, int y) {
    int fx = Find(x), fy = Find(y);
    if (fx != fy)
      return fa[fx] = fy, true;
    return false;
  }
} uf;

inline int Brute1() {
  for (int i = 1; i <= n; ++i)
    Read(a[i]);
  for (int i = 1; i <= n; ++i) {
    for (int j = 1; j <= n; ++j) {
      e[++ecnt] = (Edge) { i, j, max(a[i], a[j]) % min(a[i], a[j]) };
    }
  }
  uf.Init();
  sort(e + 1, e + ecnt + 1);
  int edges = 0, tot = 0;
  for (int i = 1; i <= ecnt && edges <= n - 1; ++i) {
    if (uf.Merge(e[i].u, e[i].v)) {
      ++edges; 
      tot += e[i].w;
    }
  }
  printf("%d\n", tot);
  return 0;
}

inline int Brute2() {
  
  return 0;
}

int main() {
  freopen("modtree.in", "r", stdin);
  freopen("modtree.out", "w", stdout);
  Read(n);
  if (n <= 1000)
    return Brute1();
  return Brute2();
}
