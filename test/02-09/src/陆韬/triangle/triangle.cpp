#include<cstdio>
#include<iostream>
#include<cstring>
#include<cmath>
#include<algorithm>
#define ll long long
using namespace std;
int n,q;
int a[100005];
int tem[100005];
bool b[100005];
int p=0;
bool f[1001][1001];
int read()
{
	char c=getchar();
	int a=0;
	while (c<'0'||c>'9') c=getchar();
	while (c>='0'&&c<='9')
	{
		a=a*10+c-48;
		c=getchar();
	}
	return a;
}
int dfs(int x,int y)
{
	//cout<<x<<' ';
	if (x==y)
	{
		tem[p]=a[y];p++;return 1;
	} else
	{
		int t=-1;
		for (int i=1;i<=n;i++) if (!b[i]&&f[x][i])
		{
			b[i]=1;
			if (t=dfs(i,y)==1)
			{
				tem[p]=a[x];
				p++;return 1;
			}
			b[i]=0;
		}
		return -1;
	}
}
int main()
{
	freopen("triangle.in","r",stdin);freopen("triangle.out","w",stdout);
	n=read();q=read();
	for (int i=1;i<=n;i++) a[i]=read();
	for (int i=0;i<n-1;i++)
	{
		int x=read(),y=read();
		f[x][y]=f[y][x]=1;
	}
	while (q--)
	{
		int op=read(),x=read(),y=read();
		if (op==1)
		{
			memset(tem,0,sizeof(tem));
			memset(b,0,sizeof(b));
			p=0;
			b[x]=1;
			int t=dfs(x,y);//cout<<endl;
			//cout<<p<<endl;
			if (p<3) printf("N\n"); else
			{
				//for (int i=0;i<p;i++) cout<<tem[i]<<' ';cout<<endl;
				sort(tem,tem+p);
				//for (int i=0;i<p;i++) cout<<tem[i]<<' ';cout<<endl;
				for (int i=0;i<p-2;i++) if (tem[i]+tem[i+1]>tem[i+2])
				{
					printf("Y\n");t=-2;break;
				}
				if (t!=-2) printf("N\n");
			}
		} else
		{
			a[x]=y;
			//for (int i=1;i<=n;i++) cout<<a[i]<<' ';cout<<endl;
		}
	}
	return 0;
}
