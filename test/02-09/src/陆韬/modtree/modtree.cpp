#include<cstdio>
#include<iostream>
#include<cstring>
#include<cmath>
#include<algorithm>
typedef long long ll;
using namespace std;
int a[300005];
int d[300005];
bool b[300005];
int n;
ll ans=0;
int max(int x,int y)
{
	return x>y?x:y;
}
int min(int x,int y)
{
	return x<y?x:y;
}
int read()
{
	char c=getchar();
	int a=0;
	while (c<'0'||c>'9') c=getchar();
	while (c>='0'&&c<='9')
	{
		a=a*10+c-48;
		c=getchar();
	}
	return a;
}
int main()
{
	freopen("modtree.in","r",stdin);freopen("modtree.out","w",stdout);
	n=read();
	for (int i=0;i<n;i++) a[i]=read();
	memset(d,127,sizeof(d));
	d[0]=0;
	for (int k=0;k<n;k++)
	{
		int minn=d[300004],p;
		for (int i=0;i<n;i++) if (!b[i]&&d[i]<minn) minn=d[i],p=i;
		b[p]=1;
		//cout<<a[p]<<' ';
		ans+=minn;
		for (int i=0;i<n;i++) if (i!=p&&d[i]>max(a[i],a[p])%min(a[i],a[p]))
		{
			d[i]=max(a[i],a[p])%min(a[i],a[p]);
		}
		//for (int i=0;i<n;i++) cout<<d[i]<<' ';cout<<endl;
	}
	//cout<<endl;
	printf("%lld\n",ans);
	return 0;
}
