#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("segment.in");
ofstream fout("segment.out");
const int N = 20005;
int l[N], r[N], f[N][2];
int main()
{
	int n;
	fin >> n;
	for (int i = 1; i <= n; i++)
		fin >> l[i] >> r[i];
	f[1][0] = r[1] - 1 + r[1] - l[1];
	f[1][1] = r[1] - 1;
	for (int i = 2; i <= n; i++)
	{
		f[i][0] = min(abs(l[i - 1] - r[i]) + f[i - 1][0], abs(r[i - 1] - r[i]) + f[i - 1][1]) + r[i] - l[i] + 1;
		f[i][1] = min(abs(l[i - 1] - l[i]) + f[i - 1][0], abs(r[i - 1] - l[i]) + f[i - 1][1]) + r[i] - l[i] + 1;
	}
	fout << min(f[n][0] + n - l[n], f[n][1] + n - r[n]) << endl;
	return 0;
}