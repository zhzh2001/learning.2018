#include <fstream>
#include <cstring>
#include <algorithm>
using namespace std;
ifstream fin("modtree.in");
ofstream fout("modtree.out");
const int N = 300005, M = 10000005;
int a[N], ra[M], f[N];
struct edge
{
	int u, v, w;
	edge() {}
	edge(int u, int v, int w) : u(u), v(v), w(w) {}
	bool operator<(const edge &rhs) const
	{
		return w < rhs.w;
	}
} e[M];
int getf(int x)
{
	return f[x] == x ? x : f[x] = getf(f[x]);
}
int main()
{
	int n;
	fin >> n;
	for (int i = 1; i <= n; i++)
		fin >> a[i];
	sort(a + 1, a + n + 1);
	int m = a[n];
	for (int i = 1; i <= n; i++)
	{
		ra[a[i]] = i;
		f[i] = i;
	}
	for (int i = m; i; i--)
		if (!ra[i])
			ra[i] = ra[i + 1];
	int cnt = 0;
	for (int i = 1; i < n; i++)
		e[++cnt] = edge(i, i + 1, a[i + 1] % a[i]);
	for (int i = 1; i < n; i++)
		for (int j = 1; a[i] * j <= m; j++)
			e[++cnt] = edge(i, ra[a[i] * j], a[ra[a[i] * j]] % a[i]);
	sort(e + 1, e + cnt + 1);
	int now = 0;
	long long ans = 0;
	for (int i = 1; i <= cnt; i++)
	{
		int ru = getf(e[i].u), rv = getf(e[i].v);
		if (ru != rv)
		{
			f[ru] = rv;
			ans += e[i].w;
			if (++now == n - 1)
				break;
		}
	}
	fout << ans << endl;
	return 0;
}