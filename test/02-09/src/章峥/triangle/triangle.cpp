#include <fstream>
#include <utility>
#include <algorithm>
using namespace std;
ifstream fin("triangle.in");
ofstream fout("triangle.out");
const int N = 100005;
int a[N], head[N], nxt[N * 2], v[N * 2], e, now[N], dep[N], f[N];
inline void add_edge(int u, int v)
{
	::v[++e] = v;
	nxt[e] = head[u];
	head[u] = e;
}
void dfs(int k)
{
	for (int i = head[k]; i; i = nxt[i])
		if (v[i] != f[k])
		{
			dep[v[i]] = dep[k] + 1;
			f[v[i]] = k;
			dfs(v[i]);
		}
}
int main()
{
	int n, m;
	fin >> n >> m;
	for (int i = 1; i <= n; i++)
		fin >> a[i];
	for (int i = 1; i < n; i++)
	{
		int u, v;
		fin >> u >> v;
		add_edge(u, v);
		add_edge(v, u);
	}
	dfs(1);
	for (int i = 1; i <= m; i++)
	{
		int opt, x, y;
		fin >> opt >> x >> y;
		if (opt == 1)
		{
			int cnt = 0;
			for (; cnt <= 50 && dep[x] > dep[y]; x = f[x])
				now[++cnt] = a[x];
			for (; cnt <= 50 && dep[x] < dep[y]; y = f[y])
				now[++cnt] = a[y];
			for (; cnt <= 50 && x != y; x = f[x], y = f[y])
			{
				now[++cnt] = a[x];
				now[++cnt] = a[y];
			}
			now[++cnt] = a[x];
			if (cnt <= 50)
			{
				sort(now + 1, now + cnt + 1);
				bool flag = false;
				for (int j = 1; j <= cnt - 2; j++)
					if (now[j] + now[j + 1] > now[j + 2])
					{
						flag = true;
						break;
					}
				fout << (flag ? "Y\n" : "N\n");
			}
			else
				fout << "Y\n";
		}
		else
			a[x] = y;
	}
	return 0;
}