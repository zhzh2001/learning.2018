#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for(int i=a;i<=b;i++)
#define pb push_back
using namespace std;
ll n,q,l;
bool flag;
vector<int>c[100100];
int fa[100100];
int sd[100100];
bool f[100100];
int a[100100];
int b[100100];
int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-') f=-1;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
void dfs(int x,int y)
{
	sd[x]=y;
	f[x]=true;
	For(i,0,c[x].size()-1)
	{
		if(f[c[x][i]])continue;	
		fa[c[x][i]]=x;
		dfs(c[x][i],y+1);
	}
	return;
}
int main()
{
	freopen("triangle.in","r",stdin);
	freopen("triangle.out","w",stdout);
	n=read();q=read();
	For(i,1,n)a[i]=read();
	For(i,1,n-1)
	{
		int x=read(),y=read();
		c[x].pb(y);c[y].pb(x);
	}
	memset(f,false,sizeof(f));
	dfs(1,1);
	while(q--)
	{
		int op=read();
		if(op==2)
		{
			int x=read(),y=read();
			a[x]=y;
		}
		else
		{
			l=0;
			int x=read(),y=read();
			if(sd[x]<sd[y])swap(x,y);
			while(sd[x]>sd[y])
			{
				l++;b[l]=a[x];
				x=fa[x];
			}
			bool fff=false;
			while(x!=y)
			{
				l++;b[l]=a[x];
				x=fa[x];
				l++;b[l]=a[y];
				y=fa[y];
				if(l>50)
				{
					cout<<"Y"<<endl;
					fff=true;
					break;
				}
			}
			if(fff)continue;
			l++;
			b[l]=a[x];
			sort(b+1,b+l+1);
			flag=false;
			//For(i,1,l)cout<<b[i]<<" ";
			//cout<<endl;
			For(i,1,l-2)
			{
				if(b[i]+b[i+1]>b[i+2])
				{
					cout<<"Y"<<endl;
					flag=true;
					break;
				}
			}
			if(!flag)cout<<"N"<<endl;
		}
	}
	return 0;
}


