#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for(int i=a;i<=b;i++)
#define pb push_back
using namespace std;
bool f[10000100];
struct D{
	int x,y,l;
}b[1000100];
int fa[300100],a[300100];
int n,mx,ans,sum,bs;
int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-') f=-1;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int get(int x)
{
	if(fa[x]!=x)fa[x]=get(fa[x]);
	return fa[x];
}
bool cmp(D a,D b){return a.l<b.l;}
int main()
{
	freopen("modtree.in","r",stdin);
	freopen("modtree.out","w",stdout);
	n=read();
	For(i,1,n)
	{
		int x=read();
		f[x]=true;
		mx=max(mx,x);
	}
	if(f[1])
	{
		cout<<0<<endl;
		return 0;
	}
	For(i,2,mx)
	{
		if(f[i]==false)continue;
		sum++;
		a[sum]=i;
	}
	For(i,1,sum)
		For(j,i+1,sum)
		{
			bs++;
			b[bs].l=max(a[i],a[j])%min(a[i],a[j]);
			b[bs].x=i;b[bs].y=j;
		}
	sort(b+1,b+bs+1,cmp);
	//For(i,1,bs)cout<<b[i].l<<" "<<a[b[i].x]<<" "<<a[b[i].y]<<endl;
	For(i,1,sum)fa[i]=i;
	int zbs=n-1;
	For(i,1,bs)
	{
		int x=get(b[i].x),y=get(b[i].y);
		if(x==y)continue;
		fa[x]=y;
		ans+=b[i].l;
		zbs--;
		if(zbs==0)break;
	}
	cout<<ans<<endl;
	return 0;
}


