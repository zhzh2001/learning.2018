#include <iostream>
#include <algorithm>
#include <cstdio>
#include <queue>
#include <cstring>
#include <vector>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline int read(){int a=0,b=1;char dd;while(!isdigit(c)&&c!='-')dd;
	if(c=='-'){b=-b;dd;}while(isdigit(c)){a=a*10+c-'0';dd;}return a*b;}
#undef dd
int a[100003],n,m,f[100003],x,y,z;
LL ans,c[100003];
struct edge{
	int a,b,c;
	edge(){
		a=b=c=0;
	}
	edge(int x,int y,int z){
		a=x;b=y;c=z;
	}
};
bool operator<(edge a,edge b){
	return a.c<b.c;
}
void finit(){
	for(int i=1;i<=n;i++)f[i]=i;
}
int fgetf(int v){
	return f[v]==v?v:(f[v]=fgetf(f[v]));
}
void fadd(int a,int b){
	f[fgetf(a)]=fgetf(b);
}
void td1(){
	vector<edge>e;
	for(int i=1;i<=n;i++)a[i]=read();
	finit();
	ans=0;
	sort(a+1,a+n+1);
	e.resize(n*(n+1)/2);
	for(int i=1;i<=n;i++)for(int j=1;j<i;j++){
		e[++m]=edge(i,j,a[i]%a[j]);
	}
	sort(e.data()+1,e.data()+m+1);
	for(int i=1;i<=m;i++)if(fgetf(e[i].a)!=fgetf(e[i].b)){
		fadd(e[i].a,e[i].b);
		ans+=e[i].c;
	}
	printf("%lld\n",ans);
}
int main(){
	freopen("modtree.in","r",stdin);
	freopen("modtree.out","w",stdout);
	n=read();
	if(n<=1000){
		td1();
		return 0;
	}
	for(int i=1;i<=n;i++){
		a[i]=read();
		f[a[i]]=1;
	}
	for(int i=1;i<=100000;i++)if(f[i]){
		for(int j=i+i;j<=100000;j+=i)f[j]=0;
	}
	n=0;
	for(int i=1;i<=100000;i++)if(f[i])a[++n]=i;
	memset(f,0,sizeof(f));
	for(int i=1;i<=n;i++){
		x=a[i];
		for(int j=x;j<=100000;j+=x)f[j]++;
	}
	for(int j=1;j<=100000;j++)f[j]+=f[j-1];
	ans=0;
	for(int i=2;i<=n;i++){
		c[i]=min(a[i]-(lower_bound(f,f+100001,f[i-1])-f),
			(upper_bound(f,f+100001,f[i])-f)%a[i]);
	}
	for(int i=2;i<=n;i++)ans+=c[i];
	printf("%lld\n",ans);
	return 0;
}
