#include <iostream>
#include <algorithm>
#include <cstdio>
#include <queue>
#include <vector>
using namespace std;
typedef long long LL;
const LL oo=1ll<<60,lmt=50;
#define dd c=getchar()
inline LL read(){LL a=0,b=1;char dd;while(!isdigit(c)&&c!='-')dd;
	if(c=='-'){b=-b;dd;}while(isdigit(c)){a=a*10+c-'0';dd;}return a*b;}
#undef dd
LL f[88];
int main(){
	f[1]=f[2]=1;
	for(LL i=3;;i++){
		f[i]=f[i-2]+f[i-1];
		if(f[i]>1e9){
			printf("%lld %lld\n",i,f[i]);
			break;
		}
	}
	return 0;
}
