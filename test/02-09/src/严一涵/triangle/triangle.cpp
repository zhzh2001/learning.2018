#include <iostream>
#include <algorithm>
#include <cstdio>
#include <queue>
#include <vector>
using namespace std;
typedef long long LL;
const LL oo=1ll<<60,lmt=50;
#define dd c=getchar()
inline LL read(){LL a=0,b=1;char dd;while(!isdigit(c)&&c!='-')dd;
	if(c=='-'){b=-b;dd;}while(isdigit(c)){a=a*10+c-'0';dd;}return a*b;}
#undef dd
struct edge{
	LL a,b;
	edge(){a=b=0;}
	edge(LL x,LL y){
		a=x;
		b=y;
	}
}e[200003];
LL en[100003],et=0,f[23][100003],dep[100003],n,a[100003],m,x,y,z;
void adde(LL a,LL b){
	e[++et]=edge(en[a],b);
	en[a]=et;
	e[++et]=edge(en[b],a);
	en[b]=et;
}
void dfs(LL u,LL fa){
	dep[u]=dep[fa]+1;
	f[0][u]=fa;
	LL v;
	for(LL i=en[u];i>0;i=e[i].a){
		v=e[i].b;
		if(v==fa)continue;
		dfs(v,u);
	}
}
void init(){
	for(LL i=1;i<=20;i++){
		for(LL j=1;j<=n;j++){
			f[i][j]=f[i-1][f[i-1][j]];
		}
	}
}
LL lca(LL a,LL b){
	if(dep[a]<dep[b])return lca(b,a);
	for(LL i=20;i>=0;i--)if(dep[f[i][a]]>=dep[b])a=f[i][a];
	if(a==b)return a;
	for(LL i=20;i>=0;i--)if(f[i][a]!=f[i][b]){
		a=f[i][a];
		b=f[i][b];
	}
	return f[0][a];
}
priority_queue<LL,vector<LL>,greater<LL> >p;
bool check(LL x,LL y,LL z){
	while(!p.empty())p.pop();
	p.push(a[z]);
	while(x!=z){
		p.push(a[x]);
		x=f[0][x];
	}
	while(y!=z){
		p.push(a[y]);
		y=f[0][y];
	}
	x=-oo;
	y=-oo;
	while(!p.empty()){
		z=p.top();
		p.pop();
		if(z<x+y){
			return 1;
		}
		x=y;
		y=z;
	}
	return 0;
}
int main(){
	freopen("triangle.in","r",stdin);
	freopen("triangle.out","w",stdout);
	n=read();
	m=read();
	for(LL i=1;i<=n;i++)a[i]=read();
	for(LL i=1;i<n;i++){
		x=read();
		y=read();
		adde(x,y);
	}
	dfs(1,1);
	init();
	for(LL i=1;i<=m;i++){
		x=read();
		if(x==1){
			x=read();
			y=read();
			z=lca(x,y);
			if(dep[x]+dep[y]-2*dep[z]>=lmt){
				puts("Y");
				continue;
			}else{
				puts(check(x,y,z)?"Y":"N");
			}
		}else{
			x=read();
			y=read();
			a[x]=y;
		}
	}
	return 0;
}
