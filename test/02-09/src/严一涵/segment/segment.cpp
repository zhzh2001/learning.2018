#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;
typedef long long LL;
const LL oo=1ll<<60;
#define dd c=getchar()
inline LL read(){LL a=0,b=1;char dd;while(!isdigit(c)&&c!='-')dd;
	if(c=='-'){b=-b;dd;}while(isdigit(c)){a=a*10+c-'0';dd;}return a*b;}
#undef dd
LL n,f[2][4],a,b,x,y,z,ti,tl,ans;
int main(){
	freopen("segment.in","r",stdin);
	freopen("segment.out","w",stdout);
	n=read();
	a=-oo;
	b=1;
	f[0][3]=-1;
	for(LL i=1;i<=n;i++){
		ti=i&1;
		tl=ti^1;
		x=read();
		y=read();
		f[ti][0]=min(f[tl][2]+abs(a-x),f[tl][3]+abs(b-x))+1;
		f[ti][1]=min(f[tl][2]+abs(a-y),f[tl][3]+abs(b-y))+1;
		z=abs(x-y);
		f[ti][2]=f[ti][1]+z;
		f[ti][3]=f[ti][0]+z;
		f[ti][2]=min(f[ti][2],f[ti][3]+z);
		f[ti][3]=min(f[ti][3],f[ti][2]+z);
		a=x;b=y;
	}
	ans=min(n-a+f[ti][2],n-b+f[ti][3]);
	printf("%lld\n",ans);
	return 0;
}
