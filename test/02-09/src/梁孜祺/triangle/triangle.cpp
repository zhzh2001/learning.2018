#include<bits/stdc++.h>
#define ll long long
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define N 100005
#define inf 1000000000
using namespace std;
int n,Q,a[N],head[N],cnt,fa[N],dep[N];
struct que{int opt,x,y;}q[N];
struct edge{int nxt,to;}e[N<<1];
inline void insert(int u,int v){
	e[++cnt]=(edge){head[u],v};head[u]=cnt;
	e[++cnt]=(edge){head[v],u};head[v]=cnt;
}
inline void dfs(int x,int f){
	for(int i=head[x];i;i=e[i].nxt)
		if(e[i].to!=f){
			fa[e[i].to]=x;
			dep[e[i].to]=dep[x]+1;
			dfs(e[i].to,x);
		}
}
int b[N];
inline bool baoli(int x,int y){
	if(dep[x]<dep[y]) swap(x,y);int tot=0;
	For(i,1,dep[x]-dep[y]) b[++tot]=a[x],x=fa[x];
	while(x!=y) b[++tot]=a[x],b[++tot]=a[y],x=fa[x],y=fa[y];
	b[++tot]=a[x];sort(b+1,b+1+tot);
	For(i,1,tot-2) if(b[i]+b[i+1]>b[i+2]) return 1;
	return 0;
}
int main(){
	freopen("triangle.in","r",stdin);
	freopen("triangle.out","w",stdout);
	scanf("%d%d",&n,&Q);
	For(i,1,n) scanf("%d",&a[i]);
	For(i,2,n){
		int x,y;scanf("%d%d",&x,&y);
		insert(x,y);
	}
	dfs(1,0);
	For(i,1,Q) scanf("%d%d%d",&q[i].opt,&q[i].x,&q[i].y);
	For(i,1,Q){
		int opt=q[i].opt,x=q[i].x,y=q[i].y;
		if(opt==2) a[x]=y;
		else{
			if(baoli(x,y)) puts("Y");
			else puts("N");
		}
	}
	return 0;
}
