#include<bits/stdc++.h>
#define ll long long
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define N 200005
#define inf 1000000000
using namespace std;
int l[N],r[N],n;
ll f[N][2];
int main(){
	freopen("segment.in","r",stdin);
	freopen("segment.out","w",stdout);
	scanf("%d",&n);
	For(i,1,n) scanf("%d%d",&l[i],&r[i]);
	memset(f,63,sizeof f);
	f[1][1]=r[1]-1;f[1][0]=r[1]-1+r[1]-l[1];
	For(i,2,n){
		f[i][0]=min(f[i-1][0]+abs(r[i]-l[i-1]),f[i-1][1]+abs(r[i]-r[i-1]))+r[i]-l[i]+1;
		f[i][1]=min(f[i-1][0]+abs(l[i]-l[i-1]),f[i-1][1]+abs(l[i]-r[i-1]))+r[i]-l[i]+1;
	}
	printf("%lld\n",min(f[n][0]+n-l[n],f[n][1]+n-r[n]));
	return 0;
}
