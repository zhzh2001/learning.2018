#include<bits/stdc++.h>
#define ll long long
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define N 300005
#define M 10000000
#define inf 1000000000
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
int n,a[N],p[M],mx,g[M],s[M];
ll ans;
int main(){
	freopen("modtree.in","r",stdin);
	freopen("modtree.out","w",stdout);
	n=read();
	For(i,1,n) a[i]=read(),mx=max(mx,a[i]);
	For(i,1,n) p[a[i]]=a[i];
	For(i,1,mx) g[i]=inf;
	Rep(i,mx,1) if(!p[i]) p[i]=p[i+1];
	For(i,1,mx-1) if(p[i]==i){
		p[i]=p[i+1];int x=0;
		if(g[i]!=0){
			for(int j=i;j<=mx;j+=i){
				if(g[i]>g[p[j]]){
					if(g[i]>p[j]%i) g[i]=p[j]%i,x=j;
				}
				else g[p[j]]=min(g[p[j]],p[j]%i);
			}
			if(!x) continue;
			for(int j=i;j<=mx;j+=i)if(j!=x){
				g[p[j]]=min(g[p[j]],p[j]%i);
			}
		}
	}
	For(i,1,mx) if(g[i]!=inf) ans+=g[i];
	printf("%lld\n",ans);
	return 0;
}
