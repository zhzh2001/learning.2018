#include <iostream>
#include <algorithm>
using namespace std;
const int N = 305;
int h[N];
int main()
{
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
		cin >> h[i];
	sort(h + 1, h + n + 1);
	int pred = 0;
	long long ans = 0;
	for (int i = 1; i <= n; i++)
		if (i & 1)
		{
			ans += (h[n - i / 2] - h[pred]) * (h[n - i / 2] - h[pred]);
			pred = n - i / 2;
		}
		else
		{
			ans += (h[i / 2] - h[pred]) * (h[i / 2] - h[pred]);
			pred = i / 2;
		}
	cout << ans << endl;
	return 0;
}