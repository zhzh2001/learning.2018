#!/bin/bash
for ((i=1;;i++)) do
echo No. $i
./gen > b.in
./b < b.in > b.ans
./b_greedy < b.in > b.out
diff b.out b.ans
if [ $? -eq 1 ]; then read -rsp $'Wrong Answer\n' -n 1; fi
done;