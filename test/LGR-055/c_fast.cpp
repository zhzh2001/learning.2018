#include <iostream>
#include <string>
#include <algorithm>
using namespace std;
const int N = 20, MOD = 998244353;
const int tab[N + 1] = {1, 1, 3, 13, 75, 541, 4683, 47293, 545835, 7087261, 102247563, 624388220, 140725711, 783574350, 58167463, 197672989, 507523296, 622140839, 604776563, 768918857, 809378366};
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	int ans = 0;
	while (m--)
	{
		string state;
		int val;
		cin >> state >> val;
		int cnt = count(state.begin(), state.end(), '1');
		ans = (ans + 1ll * tab[cnt] * tab[n - cnt] % MOD * val) % MOD;
	}
	cout << ans << endl;
	return 0;
}