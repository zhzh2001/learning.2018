#include <iostream>
#include <algorithm>
using namespace std;
const int N = 305;
int h[N], p[N];
int main()
{
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
	{
		cin >> h[i];
		p[i] = i;
	}
	int ans = 0;
	do
	{
		int now = 0;
		for (int i = 1; i <= n; i++)
			now += (h[p[i]] - h[p[i - 1]]) * (h[p[i]] - h[p[i - 1]]);
		ans = max(ans, now);
	} while (next_permutation(p + 1, p + n + 1));
	cout << ans << endl;
	return 0;
}