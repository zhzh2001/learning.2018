#include <iostream>
using namespace std;
const int N = 20, MOD = 998244353;
int cnt[1 << N];
int main()
{
	ios::sync_with_stdio(false);
	for (int n = 0; n <= N; n++)
	{
		cnt[0] = 1;
		for (int i = 1; i < 1 << n; i++)
		{
			cnt[i] = 0;
			for (int j = (i - 1) & i; j; j = (j - 1) & i)
				cnt[i] = (cnt[i] + cnt[j]) % MOD;
			cnt[i]++;
		}
		cout << cnt[(1 << n) - 1] << ',';
	}
	return 0;
}