#include <iostream>
using namespace std;
int main()
{
	int mod;
	cin >> mod;
	int f1 = 0, f2 = 1;
	for (int i = 2;; i++)
	{
		int f = (f1 + f2) % mod;
		if (f2 == 0 && f == 1)
		{
			cout << i - 1 << endl;
			break;
		}
		f1 = f2;
		f2 = f;
	}
	return 0;
}