#include <iostream>
#include <set>
#include <random>
using namespace std;
const int N = 605, dx[] = {-1, 1, 0, 0}, dy[] = {0, 0, -1, 1};
int n, mat[N][N], id, bel[N][N], mark[N][N];
set<pair<int, int>> qi[N * N];
pair<int, int> avail[N * N];
void dfs(int x, int y)
{
	bel[x][y] = id;
	for (int i = 0; i < 4; i++)
	{
		int nx = x + dx[i], ny = y + dy[i];
		if (nx && nx <= n && ny && ny <= n && mat[nx][ny] == mat[x][y] && !bel[nx][ny])
			dfs(nx, ny);
	}
}
int main()
{
	minstd_rand gen(19260817);
	cin >> n;
	if (n == 1)
	{
		cout << -1 << ' ' << -1 << endl;
		return 0;
	}
	int fill = 0;
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= n; j++)
		{
			char c;
			cin >> c;
			if (c == 'X')
				mat[i][j] = 1;
			if (c == 'O')
				mat[i][j] = 2;
			fill += mat[i][j];
		}
	if (n > 50 && !fill)
	{
		//n is even (=400)
		int x1 = 1, y1 = 1, x2 = n, y2 = n;
		for (;;)
		{
			cout << x1 << ' ' << y1 << '\n';
			if (x1 == n / 2 && y1 == n)
				break;
			cout << x2 << ' ' << y2 << '\n';
			if (y1 < n)
				y1++;
			else
			{
				x1++;
				y1 = 1;
			}
			if (y2 > 1)
				y2--;
			else
			{
				x2--;
				y2 = n;
			}
		}
		cout << -1 << ' ' << -1 << endl;
		return 0;
	}
	for (int rnd = 1;; rnd++)
	{
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= n; j++)
				bel[i][j] = 0;
		id = 0;
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= n; j++)
				if (mat[i][j] && !bel[i][j])
				{
					qi[++id].clear();
					dfs(i, j);
				}
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= n; j++)
				if (mat[i][j])
					for (int k = 0; k < 4; k++)
					{
						int nx = i + dx[k], ny = j + dy[k];
						if (nx && nx <= n && ny && ny <= n && !mat[nx][ny])
							qi[bel[i][j]].insert(make_pair(nx, ny));
					}
		for (int i = 1; i <= id; i++)
			if (qi[i].size() == 1)
				mark[qi[i].begin()->first][qi[i].begin()->second] = rnd;
		int cc = 0;
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= n; j++)
				if (!mat[i][j] && mark[i][j] < rnd)
					avail[++cc] = make_pair(i, j);
		if (!cc || rnd * n * n > 2e7)
			break;
		pair<int, int> now = avail[uniform_int_distribution<>(1, cc)(gen)];
		cout << now.first << ' ' << now.second << '\n';
		if (rnd & 1)
			mat[now.first][now.second] = 1;
		else
			mat[now.first][now.second] = 2;
	}
	cout << -1 << ' ' << -1 << endl;
	return 0;
}
