#include<bits/stdc++.h>
using namespace std;
#define int long long
const int N=31;
int a[N],n,mod,ans;
inline void read(int &x)
{
	x=0;
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
signed main()
{
	freopen("clever.in","r",stdin);
	freopen("clever.out","w",stdout);
	read(n);
	read(mod);
	int k=1;
	for (int i=1;i<=n;++i)
		k*=10;
	for (int i=1;i<=k;++i)
	{
		int tmp=i;
		for (int j=1;j<=n;++j)
		{
			a[j]=tmp%10;
			tmp/=10;
		}
		sort(a+1,a+1+n);
		int x=0,y=0;
		for (int j=1;j<=n;++j)
			x=x*10+a[j];
		for (int j=n;j;--j)
			y=y*10+a[j];
		if (y-x==i)
		{
			cout<<i<<'\n';
			(ans+=i*i)%=mod;
		}
	}
	return 0;
}