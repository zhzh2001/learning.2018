#include<bits/stdc++.h>
using namespace std;
#define int long long
const int N=31;
int a[N],n,mod,ans,h[N],f[N],g[N],cnt,b[N];
inline void read(int &x)
{
	x=0;
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void work()
{
	cnt=0;
	for (int i=0;i<=9;++i)
		for (int j=1;j<=a[i];++j)
			f[++cnt]=i;
	for (int i=1;i<=n;++i)
		g[i]=f[n-i+1];
	for (int i=n;i;--i)
	{
		h[i]=g[i]-f[i];
		if (h[i]<0)
		{
			h[i]+=10;
			--g[i-1];
		}
	}
	memset(b,0,sizeof(b));
	for (int i=1;i<=n;++i)
		++b[h[i]];
	for (int i=0;i<=9;++i)
		if (a[i]!=b[i])
			return;
	for (int i=1;i<=n;++i)
		cout<<h[i];
	putchar(' ');
}
inline void dfs(int x,int y)
{
	if (x==9)
	{
		a[9]=y;
		work();
		return;
	}
	for (int i=0;i<=y;++i)
	{
		a[x]=i;
		dfs(x+1,y-i);
	}
}
signed main()
{
	freopen("clever.in","r",stdin);
	freopen("clever2.out","w",stdout);
	for (n=1;n<=30;++n)
	{
		putchar('"');
		dfs(0,n);
		putchar('"');
		putchar('\n');
	}
	return 0;
}