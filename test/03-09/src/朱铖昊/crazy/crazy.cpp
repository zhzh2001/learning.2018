#include<bits/stdc++.h>
using namespace std;
const int N=2005;
const double inf=1e9+7,eps=1e-7;
int n,ans,R,q[N],m;
bitset<N> a[N],f;
inline void read(int &x)
{
	x=0;
	char c=getchar();
	int y=1;
	while (c>'9'||c<'0')
	{
		if (c=='-')
			y=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
	x*=y;
}
struct Point
{
	int x,y;
}p[N];
struct Line
{
	double k,b;
	int l,r;
	inline void make_line(Point A,Point B)
	{
		if (A.x>B.x)
			swap(A,B);
		l=A.x;
		r=B.x;
		if (A.x==B.x)
			k=inf;
		else
		{
			k=((double)B.y-A.y)/((double)B.x-A.x);
			b=(double)A.y-(double)A.x*k;
		}
	}
	bool cross()
	{
		if (fabs(k-inf)<eps)
			return l<=R&&l>=-R;
		double a=k*k+1.0,B=2.0*k*b,c=b*b-(double)R*R;
		double det=B*B-4.0*a*c;
		if (det<0)
			return 0;
		//double x1=(-B+sqrt(det))/(2*a),x2=(-B-sqrt(det))/(2*a);
		return 1;
		//return ((x1<=r)&&(x1>=l))||((x2<=r)&&(x2>=l));
	}
}tmp;
int main()
{
	freopen("crazy.in","r",stdin);
	freopen("crazy.out","w",stdout);
	read(n);
	read(R);
	for (int i=1;i<=n;++i)
	{
		read(p[i].x);
		read(p[i].y);
	}
	for (int i=1;i<=n;++i)
		if (p[i].x*p[i].x+p[i].y*p[i].y>=R*R)
			p[++m]=p[i];
	n=m;
	for (int i=1;i<n;++i)
		for (int j=i+1;j<=n;++j)
		{
			tmp.make_line(p[i],p[j]);
			if (!tmp.cross())
			{
				a[i][j]=1;
				a[j][i]=1;
			}
		}
	// for (int i=1;i<=n;++i,putchar('\n'))
	// 	for (int j=1;j<=n;++j)
	// 		cout<<a[i][j];
	for (int i=1;i<=n;++i)
		q[i]=i;
	for (int i=1;i<=1000;++i)
	{
		f=0;
		random_shuffle(q+1,q+1+n);
		for (int j=1;j<=n;++j)
		{
			if ((f&a[q[j]])==f)
				f[q[j]]=1;
		}
		if (f.count()>ans)
			ans=f.count();
	}
	cout<<ans;
	return 0;
}