#include<bits/stdc++.h>
using namespace std;
const int N=805,M=400005;
int in[N];
inline void read(int &x)
{
	x=0;
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
struct Heap1
{
	pair<int,int> a[M];
	int size;
	void push_up(int x)
	{
		if (x==1)
			return;
		if (a[x].first>a[x/2].first)
		{
			swap(a[x],a[x/2]);
			push_up(x/2);
		}
	}
	void push_down(int x)
	{
		if (x*2>size)
			return;
		int y=x*2;
		if (y+1<=size&&a[y].first<a[y+1].first)
			y=y+1;
		if (a[x].first<a[y].first)
		{
			swap(a[x],a[y]);
			push_down(y);
		}
	}
	void pop()
	{
		a[1]=a[size];
		--size;
		push_down(1);
	}
	pair<int,int> top()
	{
		// while (in[a[1].second])
		// 	pop();
		return a[1];
	}
	void add(pair<int,int> x)
	{
		a[++size]=x;
		push_up(size);
	}
}heap1;
struct Heap2
{
	pair<int,int> a[M];
	int size;
	void push_up(int x)
	{
		if (x==1)
			return;
		if (a[x].first<a[x/2].first)
		{
			swap(a[x],a[x/2]);
			push_up(x/2);
		}
	}
	void push_down(int x)
	{
		if (x*2>size)
			return;
		int y=x*2;
		if (y+1<=size&&a[y].first>a[y+1].first)
			y=y+1;
		if (a[x].first>a[y].first)
		{
			swap(a[x],a[y]);
			push_down(y);
		}
	}
	void pop()
	{
		a[1]=a[size];
		--size;
		push_down(1);
	}
	pair<int,int> top()
	{
		return a[1];
	}
	void add(pair<int,int> x)
	{
		a[++size]=x;
		push_up(size);
	}
}heap2;
char s[N];
int a[N][N],b[N][N],ans[N][N],n,m,K,L;
int main()
{
	freopen("hungry.in","r",stdin);
	freopen("hungry.out","w",stdout);
	read(n);
	read(m);
	read(K);
	read(L);
	for (int i=1;i<=n;++i)
	{
		int pr=0;
		scanf("%s",s+1);
		for (int j=1;j<=m;++j)
		{
			a[i][j]=s[j]-'0';
			if (a[i][j]==0)
			{
				if (pr)
				{
					b[i][pr]=j-1;
					pr=0;
				}
			}
			else
			{
				if (!pr)
					pr=j;
			}
		}
		if (pr)
			b[i][pr]=m;
	}
	for (int i=1;i<=n;++i)
		if (b[i][1])
			heap1.add(make_pair(b[i][1],i));
	if (heap1.size<K)
	{
		puts("-1");
		return 0;
	}
	for (int i=1;i<=K;++i)
	{
		pair<int,int> tmp=heap1.top();
		heap1.pop();
		in[tmp.second]=1;
		ans[1][i]=tmp.second;
		heap2.add(tmp);
	}
	for (int i=2;i<=m;++i)
	{
		for (int j=1;j<=n;++j)
			if (b[j][i])
				heap1.add(make_pair(b[j][i],j));
		for (int j=1;j<=L&&heap1.top().first>heap2.top().first;++j)
		{
			pair<int,int>tmp1=heap1.top(),tmp2=heap2.top();
			in[tmp1.second]=1;
			in[tmp2.second]=0;
			heap1.pop();
			heap2.pop();
			heap2.add(tmp1);
		}
		// for (int i=1;i<=K;++i)
		// 	cout<<heap2.a[i].second<<' ';
		// putchar('\n');
		for (int j=1;j<=K;++j)
			ans[i][j]=heap2.a[j].second;
		if (heap2.a[1].first<i)
		{
			// cout<<i<<' '<<heap2.a[1].second<<'\n';
			puts("-1");
			return 0;
		}
	}
	for (int i=1;i<=m;++i,putchar('\n'))
		for (int j=1;j<=K;++j)
			printf("%d ",ans[i][j]);
	return 0;
}