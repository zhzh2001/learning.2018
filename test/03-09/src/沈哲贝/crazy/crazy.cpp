#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define llu unsigned long long
#define ld double
#define llu unsigned long long
#define rep(i,x,y)  for(ll i=x;i<y;++i)
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
#define pi acos(-1)
#define mk make_pair<ll,ll>
#define pa pair<ll,ll>
#define lf else if
#define max(x,y)    ((x)<(y)?(y):(x))
#define min(x,y)    ((x)<(y)?(x):(y))
#define sqr(x)      ((x)*(x))
#define abs(x)      ((x)>0?(x):-(x))
#define Mul(x,y)    ((x)=1LL*(x)*(y)%mod)
#define Add(x,y)    ((x)=((x)+(y))%mod)
#define Max(x,y)    ((x)=((x)<(y)?(y):(x)))
#define Min(x,y)    ((x)=((x)>(y)?(y):(x)))
#define E(x)        return writeln(x),0
#define p(x)        printf("~%d~\n",x)
#define pp(x,y)     printf("~~%d %d~~\n",x,y)
#define ppp(x,y,z)  printf("~~~%d %d %d~~~\n",x,y,z)
#define pppp(a,b,c,d)	printf("~~~%d %d %d %d\n",a,b,c,d)
#define f_in(x)     freopen(x".in","r",stdin)
#define f_out(x)    freopen(x".out","w",stdout)
#define open(x)     f_in(x),f_out(x)
#define fi first
#define se second
typedef complex<double> E;
namespace SHENZHEBEI{
#define NEG 1
    const int L=2333333;
    char SZB[L],*S=SZB,*T=SZB;
    inline char gc(){   if (S==T){  T=(S=SZB)+fread(SZB,1,L,stdin); if (S==T) return '\n';  }   return *S++;    }
#if NEG
    inline ll read(){    ll x=0,f=1; char ch=gc();   for (;!isdigit(ch);ch=gc()) if (ch=='-') f=-1;  for (;isdigit(ch);ch=gc())  x=x*10-48+ch;   return x*f; }
    inline void write(ll x){    if (x<0) putchar('-'),x=-x;  if (x>=10)   write(x/10);    putchar(x%10+'0');  }
#else
    inline ll read(){    ll x=0; char ch=gc();   for (;!isdigit(ch);ch=gc());    for (;isdigit(ch);ch=gc())  x=x*10-48+ch;   return x;   }
    inline void write(ll x){    if (x>=10)   write(x/10);    putchar(x%10+'0');  }
#endif
    inline char readchar(){ char ch=gc();   for(;isspace(ch);ch=gc());  return ch;  }
    inline ll readstr(char *s){ char ch=gc();   int cur=0;  for(;isspace(ch);ch=gc());      for(;!isspace(ch);ch=gc())  s[cur++]=ch;    s[cur]='\0';    return cur; }
    inline void writeln(ll x){  write(x);   puts("");   }
}using namespace SHENZHEBEI;
const ll N=2010;
const ld eps=1e-5;
struct dt{ld x,y;}p[2010];
ld R;ll n,q[N];
bitset<110>f[110],ok;
bitset<2010>F[2010],OK;
ld __XW__(dt p){return p.x*p.x+p.y*p.y;}
bool check(dt a,dt b){
	dt l=a,r=b;
	ld	x1=a.x,y1=a.y,
		x2=b.x,y2=b.y;
	ld Dist=abs((x1*y2-x2*y1)/sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)));
	if (Dist>R+eps)return 1;
	ld more_x=x2-x1,more_y=y2-y1;
	ld	wt1=__XW__(a),
		wt2=__XW__((dt){a.x+eps*more_x,a.y+eps*more_y}),
		wt3=__XW__((dt){b.x-eps*more_x,b.y-eps*more_y}),
		wt4=__XW__(b);
	return 	(wt1<wt2&&wt2<wt3&&wt3<wt4)||
			(wt1>wt2&&wt2>wt3&&wt3>wt4);
}
ll ZuiDaTuan(){
	For(i,1,n)For(j,i+1,n)f[i][j]=f[j][i]=check(p[i],p[j]);
	ll answ=0,ci=0;
	while(clock()<=CLOCKS_PER_SEC*1.5){
		ll sum=0;
		For(i,1,n)q[i]=i,ok[i]=1;
		random_shuffle(p+1,p+n+1);
		For(i,1,n)if (ok[q[i]]){
			++sum;
			For(j,1,n)ok&=f[q[i]];
		}Max(answ,sum);
	}return answ;
}
ll zuidatuan(){
	For(i,1,n)For(j,i+1,n)F[i][j]=F[j][i]=check(p[i],p[j]);
	ll answ=0,ci=0;
	while(clock()<=CLOCKS_PER_SEC*1.5){
		ll sum=0;
		For(i,1,n)q[i]=i,OK[i]=1;
		random_shuffle(p+1,p+n+1);
		For(i,1,n)if (OK[q[i]]){
			++sum;
			For(j,1,n)OK&=F[q[i]];
		}Max(answ,sum);
	}return answ;
}
int main(){
	freopen("crazy.in","r",stdin);
	freopen("crazy.out","w",stdout);
	n=read();R=read();
	For(i,1,n)p[i].x=read(),p[i].y=read();
	if (n<=100)	writeln(ZuiDaTuan());
	else		writeln(zuidatuan());
}
/*
->	y=k*x+b
->	(k*x+b)^2+x^2
->	(k^2+1)*x^2
	(2kb)  *x
	(b^2)  *1
*/
