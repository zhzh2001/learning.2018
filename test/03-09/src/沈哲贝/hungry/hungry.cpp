#include<bits/stdc++.h>
using namespace std;
#define ll int
#define llu unsigned long long
#define ld double
#define llu unsigned long long
#define rep(i,x,y)  for(ll i=x;i<y;++i)
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
#define pi acos(-1)
#define mk make_pair<ll,ll>
#define pa pair<ll,ll>
#define lf else if
#define max(x,y)    ((x)<(y)?(y):(x))
#define min(x,y)    ((x)<(y)?(x):(y))
#define sqr(x)      ((x)*(x))
#define abs(x)      ((x)>0?(x):-(x))
#define Mul(x,y)    ((x)=1LL*(x)*(y)%mod)
#define Add(x,y)    ((x)=((x)+(y))%mod)
#define Max(x,y)    ((x)=((x)<(y)?(y):(x)))
#define Min(x,y)    ((x)=((x)>(y)?(y):(x)))
#define E(x)        return writeln(x),0
#define p(x)        printf("~%lld~\n",x)
#define pp(x,y)     printf("~~%lld %lld~~\n",x,y)
#define ppp(x,y,z)  printf("~~~%lld %lld %lld~~~\n",x,y,z)
#define pppp(a,b,c,d)	printf("~~~%lld %lld %lld %lld\n",a,b,c,d)
#define f_in(x)     freopen(x,"r",stdin)
#define f_out(x)    freopen(x,"w",stdout)
#define open(x)     f_in(x".in"),f_out(x".out")
#define fi first
#define se second
typedef complex<double> E;
namespace SHENZHEBEI{
#define NEG 1
#if NEG
    inline ll read(){    ll x=0,f=1; char ch=getchar();   for (;!isdigit(ch);ch=getchar()) if (ch=='-') f=-1;  for (;isdigit(ch);ch=getchar())  x=x*10-48+ch;   return x*f; }
    inline void write(ll x){    if (x<0) putchar('-'),x=-x;  if (x>=10)   write(x/10);    putchar(x%10+'0');  }
#else
    inline ll read(){    ll x=0; char ch=getchar();   for (;!isdigit(ch);ch=getchar());    for (;isdigit(ch);ch=getchar())  x=x*10-48+ch;   return x;   }
    inline void write(ll x){    if (x>=10)   write(x/10);    putchar(x%10+'0');  }
#endif
    inline char readchar(){ char ch=getchar();   for(;isspace(ch);ch=getchar());  return ch;  }
    inline ll readstr(char *s){ char ch=getchar();   int cur=0;  for(;isspace(ch);ch=getchar());      for(;!isspace(ch);ch=getchar())  s[cur++]=ch;    s[cur]='\0';    return cur; }
    inline void writeln(ll x){  write(x);   puts("");   }
    inline ld getreal(){    static ld lbc;  scanf("%lf",&lbc);  return lbc; }
}using namespace SHENZHEBEI;
const ll N=6000000;
ll id[1010][1010],ID[1010][1010],a[1010][1010],vis[N],line[N],Line[N],head[N],nxt[N],vet[N],val[N],h[N],q[N],cur[N],n,m,cnt,tot,S,T,k,l;
char s[N];
void insert(ll x,ll y,ll w){nxt[++tot]=head[x];head[x]=tot;vet[tot]=y;val[tot]=w;}
void Insert(ll x,ll y,ll w){insert(x,y,w),insert(y,x,0);/*printf("%lld %lld %lld\n",x,y,w);*/}
bool bfs(){
	For(i,S,T)h[i]=-1;h[S]=0;
	ll he=0,ta=1;q[1]=S;
	for(;he!=ta;){
		ll x=q[++he];
		if (x==T)return 1;
		for(ll i=head[x];i;i=nxt[i])
		if (val[i]&&h[vet[i]]==-1)	h[q[++ta]=vet[i]]=h[x]+1;
	}return 0;
}
ll dfs(ll x,ll f){
	if (x==T||!f)return f;
	ll used=0,w;
	for(ll i=cur[x];i;i=nxt[i])
	if (val[i]&&h[vet[i]]==h[x]+1){
		w=dfs(vet[i],min(val[i],f-used));
		val[i]-=w;val[i^1]+=w;
		used+=w;	if (val[i])cur[x]=i;
		if (used==f)return f;
	}if (!used)h[x]=-1;
	return used;
}
void dinic(){
	ll answ=0;
	for(;k&&bfs();){
		For(i,S,T)cur[i]=head[i];
		k-=dfs(S,k);
	}if (k)return puts("-1"),void(0);
	For(j,1,m){
		For(i,1,n){
			ll x=id[i][j],y=ID[i][j];
			for(ll w=head[x];w;w=nxt[w])
				if (vet[w]==y&&!val[w])write(i),putchar(' ');
		}puts("");
	}
}
int main(){
	freopen("hungry.in","r",stdin);
	freopen("hungry.out","w",stdout);
	n=read(),m=read(),k=read(),l=read();
	For(i,1,n){
		scanf("%s",s+1);
		For(j,1,m)a[i][j]=s[j]-'0';
	}
	S=0;T=n*m+n*m+m+m+1;
	For(i,1,n)For(j,1,m)id[i][j]=++cnt;
	For(i,1,n)For(j,1,m)ID[i][j]=++cnt,Insert(id[i][j],ID[i][j],1);
	For(i,1,m)line[i]=++cnt;
	For(i,1,m)Line[i]=++cnt,Insert(line[i],Line[i],l);
	For(i,1,m)	For(j,1,n)if (a[j][i])	Insert(i==1?S:ID[j][i-1],id[j][i],1);
	For(i,1,n)	if (a[i][m])Insert(ID[i][m],T,1);
	For(i,1,n)For(j,1,m-1)	if (a[i][j])Insert(ID[i][j],line[j],1);
	For(i,1,n)For(j,2,m)	if (a[i][j])Insert(Line[j-1],id[i][j],1);
	dinic();
}
/*
单次增广n*m,k次 
*/
