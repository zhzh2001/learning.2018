#include<cmath>
#include<cstdio>
#include<algorithm>
#define Pi M_PI
using namespace std;
struct Pair{
	double x,y;
}d[2005],p[2005];
int n,ans;
bool cmp(const Pair &x,const Pair &y)
{
	return x.x<y.x;
}
double r[2005];
int t;
void Work(double qq)
{
	int i,N=0,t=0;
	double PP=Pi-qq;
	if(PP<=0)PP+=2*Pi;
	if(PP>2*Pi)PP+=2*Pi;
	for(i=1;i<=n;i++)
	{
		p[++N].x=d[i].x-qq;
		p[N].y=d[i].y-qq;
		while(p[N].x<=0)p[N].x+=2*Pi;
		while(p[N].y<=0)p[N].y+=2*Pi;
		if(p[N].x<p[N].y)swap(p[N].x,p[N].y);
		if(PP>p[N].x||PP<p[N].y)N--;
	}
	sort(p+1,p+1+N,cmp);
	for(i=1;i<=N;i++)
	{
		if(p[i].y>r[t])
			r[++t]=p[i].y;
		else
			*lower_bound(r+1,r+1+t,p[i].y)=p[i].y;
	}
	ans=max(ans,t);
}
int main()
{
#ifndef ONLINE_JUDGE
	freopen("crazy.in","r",stdin);
	freopen("crazy.out","w",stdout);
#endif
	double R,x,y;
	int i;
	scanf("%d%lf",&n,&R);
	for(i=1;i<=n;i++)
	{
		scanf("%lf%lf",&x,&y);
		d[i].x=atan2(y,x);
		d[i].y=acos(R/sqrt(x*x+y*y));
		d[i].x-=d[i].y;
		d[i].y=d[i].y*2+d[i].x;
		while(d[i].x<=0)d[i].x+=2*Pi;
		while(d[i].x>2*Pi)d[i].x-=2*Pi;
		while(d[i].y<=0)d[i].y+=2*Pi;
		while(d[i].y>2*Pi)d[i].y-=2*Pi;
	}
	for(i=1;i<=n;i++)
	{
		Work(d[i].x);
		Work(d[i].y);
	}
	printf("%d\n",ans);
}
