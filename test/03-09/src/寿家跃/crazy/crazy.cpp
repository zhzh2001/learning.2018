#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<ctime>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)

using namespace std;

const int N=2005;

int n,r;
int x[N],y[N];
inline int Able(int a,int b){
	if (x[a]==x[b]){
		if (fabs(x[a])>r) return true;
			else return false;
	}
	double k=1.0*(y[a]-y[b])/(x[a]-x[b]);
	double c=y[a]-x[a]*k;
	double dis=fabs(c)/sqrt(k*k+1);
//	printf("dis=%lf\n",dis);
	if (dis+1e-9>r) return true;
		else return false;
}
int Dat[N][N],p[N],bol[N];
int main(){
	freopen("crazy.in","r",stdin);
	freopen("crazy.out","w",stdout);
	scanf("%d%d",&n,&r);
	Rep(i,1,n) scanf("%d %d",&x[i],&y[i]);
	Rep(i,1,n) Rep(j,1,n) if (i!=j) Dat[i][j]=Able(i,j);
	/*
	Rep(i,1,n){
		Rep(j,1,n) printf("%d ",Dat[i][j]);puts("");
	}
	*/
	double sec=CLOCKS_PER_SEC;
	Rep(i,1,n) p[i]=i;
	int Ans=0;
	while (1.0*clock()/sec<1.5){
		random_shuffle(p+1,p+n+1);
		Rep(i,1,n) bol[i]=false;
		int res=0;
		Rep(i,1,n){
			bool flag=true;
			Rep(j,1,i-1) if (bol[p[j]] && !Dat[p[j]][p[i]]){
				flag=false;break;
			}
			if (flag){
				res++;bol[p[i]]=true;
			}
			if (res+n-i<=Ans) break;
		}
		Ans=max(Ans,res);
	}
	printf("%d\n",Ans);
}
