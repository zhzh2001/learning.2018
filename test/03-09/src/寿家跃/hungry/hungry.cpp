#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<ctime>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

const int N=805;

int n,m,k,l,Dat[N][N],Res[N][N],Kes[N],Bol[N],p[N];

inline bool Cmp(int A,int B){
	return Kes[A]>Kes[B];
}
inline int Getchar(){
	int ch=getchar();
	while (ch!='0' && ch!='1') ch=getchar();
	return ch;
}
int main(){
	freopen("hungry.in","r",stdin);
	freopen("hungry.out","w",stdout);
	scanf("%d %d %d %d",&n,&m,&k,&l);
	Rep(i,1,n){
		Rep(j,1,m) Dat[i][j]=Getchar()-48;
		Dep(j,m,1) if (Dat[i][j]) Dat[i][j]+=Dat[i][j+1];
	}
	Rep(i,1,n) p[i]=i;
	Rep(t,1,m){
		Rep(i,1,n) Kes[i]=Dat[i][t];
		sort(p+1,p+n+1,Cmp);
//		Rep(i,1,n) printf("p[%d]=%d ",i,p[i]);puts("");
		if (t==1){
			Rep(i,1,k) Bol[p[i]]=true;
		}
		else{
			int pos=n,cnt=0;
			while (pos>0 && !Bol[p[pos]]) pos--; 
			Rep(i,1,n) if (!Bol[p[i]]){
				if (i>pos) break;
				Bol[p[pos]]=false;
				Bol[p[i]]=true;
				pos--;
				while (pos>0 && !Bol[p[pos]]) pos--;
				if ((++cnt)==l) break;
			}
		}
		bool flag=true;
		Rep(i,1,n) if (Bol[i] && !Kes[i]) flag=false;
		if (flag){
			Rep(i,1,n) if (Bol[i]) Res[t][++*Res[t]]=i;
		}
		else{
			puts("-1");return 0;
		}
	}
	Rep(t,1,m){
		Rep(i,1,k) printf("%d ",Res[t][i]);puts("");
	}
}
