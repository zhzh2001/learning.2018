#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<ctime>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)

using namespace std;

typedef long long ll;

const int N=35;

int n,Mod,a[N],b[N],d[N];

inline bool Cmp_less(int A,int B){
	return A<B;
}
inline bool Cmp_greater(int A,int B){
	return A>B;
}
int main(){
	ll val=1;
	scanf("%lld",&val);
	while (true){
		*d=0;
		ll v=val;
		while (v>0) d[++*d]=v%10,v/=10;
		*a=*d;Rep(i,1,*a) a[i]=d[i];
		*b=*d;Rep(i,1,*b) b[i]=d[i];
		sort(a+1,a+*a+1,Cmp_less);
		sort(b+1,b+*b+1,Cmp_greater);
		ll g=0,f=0,base=1;
		Rep(i,1,*d){
			g+=base*a[i];
			f+=base*b[i];
			base=base*10;
		}
		printf("g=%lld f=%lld g-f=%lld val=%lld\n",g,f,g-f,val);
		return 0;
	}
}
