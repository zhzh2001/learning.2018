#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<ctime>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)

using namespace std;

typedef long long ll;

const int N=35;

int n,Mod;

inline void Add(ll &num,int val){
	if ((num+=val)>=Mod) num-=Mod;
}
int main(){
	freopen("clever.in","r",stdin);
	freopen("clever.out","w",stdout);
	scanf("%d %d",&n,&Mod);
	ll Ans=0;
	if (n>=3 && (n-3)%3==0){
		int k=(n-3)/3,base=1;
		ll res=0;
		Add(res,5ll*base%Mod);base=1ll*base*10%Mod;
		Rep(i,1,k) Add(res,4ll*base%Mod),base=1ll*base*10%Mod;
		Rep(i,1,k+1) Add(res,9ll*base%Mod),base=1ll*base*10%Mod;
		Add(res,4ll*base%Mod);base=1ll*base*10%Mod;
		Rep(i,1,k) Add(res,5ll*base%Mod),base=1ll*base*10%Mod;
		Add(Ans,res*res%Mod);
	}
	if (n>=4 && (n-4)%2==0){
		int k=(n-4)/2,base=1;
		ll res=0;
		Add(res,4ll*base%Mod);base=1ll*base*10%Mod;
		Rep(i,1,k) Add(res,6ll*base%Mod),base=1ll*base*10%Mod;
		Add(res,7ll*base%Mod);base=1ll*base*10%Mod;
		Add(res,1ll*base%Mod);base=1ll*base*10%Mod;
		Rep(i,1,k) Add(res,3ll*base%Mod),base=1ll*base*10%Mod;
		Add(res,6ll*base%Mod);base=1ll*base*10%Mod;
		Add(Ans,res*res%Mod);
	}
	if (n>=8 && (n-8)%2==0){
		int k=(n-8)/2,base=1;
		ll res=0;
		Add(res,1ll*base%Mod);base=1ll*base*10%Mod;
		Add(res,2ll*base%Mod);base=1ll*base*10%Mod;
		Add(res,4ll*base%Mod);base=1ll*base*10%Mod;
		Rep(i,1,k) Add(res,6ll*base%Mod),base=1ll*base*10%Mod;
		Add(res,8ll*base%Mod);base=1ll*base*10%Mod;
		Add(res,0ll*base%Mod);base=1ll*base*10%Mod;
		Rep(i,1,k) Add(res,3ll*base%Mod),base=1ll*base*10%Mod;
		Add(res,5ll*base%Mod);base=1ll*base*10%Mod;
		Add(res,7ll*base%Mod);base=1ll*base*10%Mod;
		Add(res,9ll*base%Mod);base=1ll*base*10%Mod;
		Add(Ans,res*res%Mod);
	}
	if (n==9){
		int ads=1ll*864197532%Mod;
		Add(Ans,1ll*ads*ads%Mod);
	}
	printf("%lld\n",Ans);
}
