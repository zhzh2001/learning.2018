#include <bits/stdc++.h>

using namespace std;

namespace program
{
	typedef long long big;

	const int MAXN = 2000;
	vector<int> G[MAXN + 10];
	int n, r, X[MAXN + 10], Y[MAXN + 10];

	inline big sqr(int x) { return (big)x * x; }

	bool check(int x1, int y1, int x2, int y2)
	{
		big sqr_AB = sqr(x1 - x2) + sqr(y1 - y2);
		big sqr_AH_sqr_AB = sqr(-x1 * (x2 - x1) - y1 * (y2 - y1));
		big sqr_OH_sqr_AB = (sqr(x1) + sqr(y1)) * sqr_AB - sqr_AH_sqr_AB;
		return sqr_OH_sqr_AB <= sqr(r) * sqr_AB;
	}

	namespace bf
	{
		const int MAXN = 20;
		int M[MAXN + 10], F[(1 << MAXN) + 10];

		void work()
		{
			for(int u = 0; u < n; u++)
			{
				M[u] = 1 << u;
				for(vector<int>::iterator p = G[u].begin(); p != G[u].end(); p++)
					M[u] |= 1 << *p;
			}
			for(int S = (1 << n) - 1; S >= 0; S--)
			{
				F[S] = 0;
				for(int u = 0; u < n; u++)
					if((~S >> u) & 1)
						F[S] = max(F[S], F[S | M[u]] + 1);
			}
			printf("%d\n", F[0]);
		}
	}

	namespace qwq
	{
		int P[MAXN + 10];
		bitset<MAXN> M[MAXN + 10], Vis;

		inline bool cmp(int x, int y) { return G[x].size() < G[y].size(); }

		void work()
		{
			int times = 200000000 / (n * MAXN / 32), ans = 0;
			srand(n ^ 233);
			for(int u = 0; u < n; u++)
			{
				P[u] = u;
				M[u].set(u);
				for(vector<int>::iterator p = G[u].begin(); p != G[u].end(); p++)
					M[u].set(*p);
			}
			sort(P, P + n, cmp);
			while(times--)
			{
				int t = 0;
				Vis = 0;
				for(int i = 0; i < n; i++)
				{
					int u = P[i];
					if(!Vis.test(u))
					{
						t++;
						Vis |= M[u];
					}
				}
				if(t > ans)
					ans = t;
				random_shuffle(P, P + n);
			}
			printf("%d\n", ans);
		}
	}

	void work()
	{
		scanf("%d%d", &n, &r);
		for(int i = 0; i < n; i++)
			scanf("%d%d", &X[i], &Y[i]);
		for(int i = 0; i < n; i++)
			for(int j = 0; j < n; j++)
				if(check(X[i], Y[i], X[j], Y[j]))
					G[i].push_back(j);
		if(n <= bf::MAXN)
			bf::work();
		else
			qwq::work();
	}
}

int main()
{
#if !JUMPMELON || DEBUG
	freopen("crazy.in", "r", stdin);
	freopen("crazy.out", "w", stdout);
#endif
	program::work();
	return 0;
}
