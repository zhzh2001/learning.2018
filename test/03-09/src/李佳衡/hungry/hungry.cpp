#include <bits/stdc++.h>

using namespace std;

namespace program
{
	typedef pair<int, int> pa;

	const int MAXN = 800, MAXM = 800;
	char Str[MAXN + 10][MAXM + 10];
	int Next[MAXN + 10][MAXM + 10];
	int n, m, k, l, Ans[MAXM + 10][MAXN + 10];
	set<pa> S1, S2;

	namespace bf
	{
		const int MAXN = 10, MAXM = 10;
		bool Vis[MAXM + 10][(1 << MAXN) + 10];
		int Dp[MAXM + 10][(1 << MAXN) + 10];
		char Str[MAXM + 10];
		int M[MAXM + 10];

		int popcount(int S)
		{
			int t = 0;
			while(S)
			{
				t++;
				S ^= S & -S;
			}
			return t;
		}

		int dp(int i, int S)
		{
			if(i >= m)
				return 0;
			if(!Vis[i][S])
			{
				Vis[i][S] = 1;
				Dp[i][S] = -1;
				if(!(M[i] & S))
				{
					for(int T = 0; T < (1 << n); T++)
						if(popcount(T) == k && popcount(T & ~S) <= l && ~dp(i + 1, T))
						{
							Dp[i][S] = T;
							break;
						}
				}
			}
			return Dp[i][S];
		}

		void work()
		{
			for(int i = 0; i < n; i++)
			{
				scanf("%s", Str);
				for(int j = 0; j < m; j++)
					if(Str[j] == '0')
						M[j] |= 1 << i;
			}
			for(int S = 0; S < (1 << n); S++)
				if(popcount(S) == k && ~dp(0, S))
				{
					for(int i = 0; i < m; S = Dp[i++][S])
					{
						for(int j = 0; j < n; j++)
							if((S >> j) & 1)
								printf("%d ", j + 1);
						putchar('\n');
					}
					return;
				}
			puts("-1");
		}
	}

	void work()
	{
		scanf("%d%d%d%d", &n, &m, &k, &l);
		if(n <= bf::MAXN && m <= bf::MAXM)
		{
			bf::work();
			return;
		}
		for(int i = 0; i < n; i++)
		{
			scanf("%s", Str[i]);
			Next[i][m] = m;
			for(int j = m - 1; j >= 0; j--)
				Next[i][j] = (Str[i][j] == '1' ? Next[i][j + 1] : j);
			S1.insert(pa(Next[i][0], i));
		}
		for(int i = 0; i < k; i++)
		{
			if(S1.rbegin()->first <= 0)
				goto fail;
			Ans[0][i] = S1.rbegin()->second + 1;
			S2.insert(*S1.rbegin());
			S1.erase(*S1.rbegin());
		}
		for(int i = 1; i < m; i++)
		{
			int t = 0;
			for(int j = 0; j < n; j++)
				if(Next[j][i] != Next[j][i - 1])
				{
					if(S1.count(pa(Next[j][i - 1], j)))
					{
						S1.erase(pa(Next[j][i - 1], j));
						S1.insert(pa(Next[j][i], j));
					}
					else
					{
						S2.erase(pa(Next[j][i - 1], j));
						S2.insert(pa(Next[j][i], j));
					}
				}
			for(int j = 0; j < l; j++)
			{
				if(S1.empty() || S2.begin()->first >= S1.rbegin()->first)
					break;
				S2.insert(*S1.rbegin());
				S1.erase(*S1.rbegin());
				S1.insert(*S2.begin());
				S2.erase(*S2.begin());
			}
			if(S2.begin()->first <= i)
				goto fail;
			for(set<pa>::iterator p = S2.begin(); p != S2.end(); p++)
				Ans[i][t++] = p->second + 1;
		}
		for(int i = 0; i < m; i++)
		{
			sort(Ans[i], Ans[i] + k);
			for(int j = 0; j < k; j++)
				printf("%d ", Ans[i][j]);
			putchar('\n');
		}
		return;
	fail:
		puts("-1");
	}
}

int main()
{
#if !JUMPMELON || DEBUG
	freopen("hungry.in", "r", stdin);
	freopen("hungry.out", "w", stdout);
#endif
	program::work();
	return 0;
}
