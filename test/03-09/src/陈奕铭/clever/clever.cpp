#include <bits/stdc++.h>
using namespace std;
#define ll long long
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}

int n,p,num,ans;
ll bin[19],binx[19];
int a[19],b[19],c[19];

void pd(){
	ll sum = 0; int top = 0;
	for(int i = 1;i <= num;++i) sum += binx[i]*a[i];
	for(int i = 0;i < n;++i)
		b[(sum/bin[i])%10]++;
	for(int i = 9;i >= 0;--i)
		while(b[i]){ c[++top] = i; --b[i]; }
	int l = 1,r = n;
	while(l < r){ if(c[l]-c[r] != a[l]) return; ++l;--r;}
	ans = (1LL*ans+sum*sum%p)%p;
}

void dfs(int x){
	if(x == num+1){
		pd();
		return;
	}
	for(int i = 0;i < 10;++i){
		a[x] = i;
		dfs(x+1);
	}
}

int main(){
	freopen("clever.in","r",stdin);
	freopen("clever.out","w",stdout);
	n = read(); p = read();
	bin[0] = 1;
	for(int i = 1;i <= n;++i) bin[i] = bin[i-1]*10;
	int l = 0,r = n-1;
	while(l < r){
		binx[++num] = bin[r]-bin[l];
		++l; --r;
	}
	dfs(1);
	writeln(ans);
	return 0;
}
