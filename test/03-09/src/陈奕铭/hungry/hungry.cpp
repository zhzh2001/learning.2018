#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}

int n,m,k,l,num;
int day[15];
int pre[15][1035];
bool now[1035],nxt[1035];
int ans[15][15];
char s[15];

inline bool pd(int x,int d){
	if((x&day[d]) != x) return false;
	int ans = 0;
	while(x){
		if(x&1) ++ans;
		x >>= 1;
	}
	return ans == k;
}

inline void Print(int x){
	for(int i = m;i >= 1;--i){
		int o = 0;
		for(int j = 0;j < n;++j)
			if(x&(1<<j))
				ans[i][++o] = j+1;
		x = pre[i][x];
	}
	for(int i = 1;i <= m;++i){
		for(int j = 1;j <= k;++j)
			write(ans[i][j]),putchar(' ');
		puts("");
	}
}

void bfs(){
	memset(now,true,sizeof now);
	for(int d = 1;d < m;++d){
		for(int i = 0;i < num;++i) if(now[i] && pd(i,d))
			for(int j = 0;j < num;++j) if((!nxt[j]) && pd(j,d+1)){
				int p = i&j,sum = 0;
				while(p){ if(p&1)++sum; p >>= 1; }
				if(sum < k-l) continue;
				nxt[j] = true;
				pre[d+1][j] = i;
			}
		memcpy(now,nxt,sizeof nxt);
		memset(nxt,false,sizeof nxt);
	}
	for(int i = 0;i < num;++i)
		if(now[i]){
			Print(i);
			return;
		}
	puts("-1");
	return;
}

int main(){
	freopen("hungry.in","r",stdin);
	freopen("hungry.out","w",stdout);
	n = read(); m = read(); k = read(); l = read(); num = 1<<n;
	for(int i = 1;i <= n;++i){
		scanf("%s",s+1);
		for(int j = 1;j <= m;++j)
			if(s[j] == '1')
				day[j] |= (1<<(i-1));
	}
	bfs();
	return 0;
}
