#include<bits/stdc++.h>
using namespace std;
#define gc getchar
#define RG register
#define pc putchar
#define rd read
#define mem(x,v) memset(x,v,sizeof(x))
#define Rep(i,a,b) for(RG int i=(a);i<=(b);i++)
#define Down(i,a,b) for(RG int i=(a);i>=(b);i--)
typedef unsigned long long ull;
inline int read(){
	int T = 0,f = 0;char c = gc();
	for(;c<'0'||c>'9';c=gc()) f|=(c=='-');
	for(;c>='0'&&c<='9';c=gc()) T=T*10+c-'0';
	return f?-T:T;
}
inline void write(int x){
	if(x<0)pc('-'),x=-x;
	if(x>=10) write(x/10);
	pc(x%10+'0');
}/*
int a[15],len;
unsigned long long Pow[30];
unsigned long long res = 0;
bool solve(){
//	printf("%lld\n",res);
	if(res==0) return false;
	unsigned long long tmp = res;
	*a = 0;
	for(int j=1;j<=len;j++){
		a[++*a] = tmp % 10;
		tmp /= 10;
	}
	sort(a+1,a+1+*a);
	unsigned long long g = 0,l = 0;
	for(int j=1;j<=*a;j++) l=l*10+a[j];
	for(int j=*a;j>=1;j--) g=g*10+a[j];
//	printf("%d %d\n",g,l);
//	system("pause");
	return g-l==res;
}
void dfs(int l,int r){
//	printf("%llu\n",res);
	if(l>=r){if(solve()){
		printf("%d %llu\n",len,res);
	}return;}
	dfs(l+1,r-1);
	for(int i=1;i<=9;i++){
		res = res + Pow[r] - Pow[l];
		dfs(l+1,r-1);
	}
	res = res - (Pow[r] - Pow[l]) * 9;
}*/
ull n,p;
ull sqr(ull x){return x*x%p;}
int main(){
//	freopen("clever.in","r",stdin);
//	freopen("clever.out","w",stdout);
	n = rd(),p = rd();
	if(n==1||n==2||n==5||n==7||n>=15) puts("0"); else
	if(n==3){
		printf("%llu\n",sqr(495%p)%p);
	} else
	if(n==4){
		printf("%llu\n",sqr(6174%p)%p);
	} else
	if(n==6){
		printf("%llu\n",(sqr(631764%p)%p+sqr(549945%p)%p)%p);
	} else
	if(n==8){
		printf("%llu\n",(sqr(63317664%p)%p+sqr(97508421%p)%p)%p);
	} else
	if(n==9){
		printf("%llu\n",(sqr(554999445%p)%p+sqr(864197532%p)%p)%p);
	} else
	if(n==10){
		printf("%llu\n",((sqr(6333176664%p)%p+sqr(9753086421%p)%p)%p+sqr(9975084201%p)%p)%p);
	} else
	if(n==11){
		printf("%llu\n",sqr(86431976532%p)%p);
	} else
	if(n==12){
		ull ans = 0;
		ans = (ans + (sqr(555499994445%p)%p))%p;
		ans = (ans + (sqr(633331766664%p)%p))%p;
		ans = (ans + (sqr(975330866421%p)%p))%p;
		ans = (ans + (sqr(997530864201%p)%p))%p;
		ans = (ans + (sqr(999750842001%p)%p))%p;
		printf("%llu",ans);
	} else
	if(n==13){
		ull ans = 0;
		ans = (ans + (sqr(8643319766532%p)%p))%p;
		printf("%llu",ans);
	} else
	if(n==14){
		ull ans = 0;
		ans = (ans + (sqr(63333317666664%p)%p))%p;
		ans = (ans + (sqr(97533308666421%p)%p))%p;
		ans = (ans + (sqr(97755108844221%p)%p))%p;
		ans = (ans + (sqr(99753308664201%p)%p))%p;
		ans = (ans + (sqr(99975308642001%p)%p))%p;
		ans = (ans + (sqr(99997508420001%p)%p))%p;
		printf("%llu",ans);
	}
	return 0;
}

/*
3:495
4:6174
6:549945
8:63317664
8:97508421
9:554999445
9:864197532
10:1375059719
10:1354150909
11:
*/
