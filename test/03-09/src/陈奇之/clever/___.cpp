#include<bits/stdc++.h>
#include<iostream>
using namespace std;
#define gc getchar
#define RG register
#define pc putchar
#define rd read
#define mem(x,v) memset(x,v,sizeof(x))
#define Rep(i,a,b) for(RG int i=(a);i<=(b);i++)
#define Down(i,a,b) for(RG int i=(a);i>=(b);i--)
//typedef __int128 ull;
#define ull __int128
#define int __int128
inline int read(){
	int T = 0,f = 0;char c = gc();
	for(;c<'0'||c>'9';c=gc()) f|=(c=='-');
	for(;c>='0'&&c<='9';c=gc()) T=T*10+c-'0';
	return f?-T:T;
}
inline void write(__int128 x){
	if(x<0)pc('-'),x=-x;
	if(x>=10) write(x/10);
	pc(x%10+'0');
}
int a[31],len;
__int128 Pow[35];
__int128 res = 0;
bool solve(){
//	printf("%lld\n",res);
	if(res==0) return false;
	__int128 tmp = res;
//	write(tmp),puts("");
	*a = 0;
	for(int j=1;j<=len;j++){
		a[++*a] = tmp % 10;
		tmp /= 10;
	}
	sort(a+1,a+1+*a);
	__int128 g = 0,l = 0;
	for(int j=1;j<=*a;j++) l=l*10+a[j];
	for(int j=*a;j>=1;j--) g=g*10+a[j];
//	write(g);pc(' ');write(l);puts("");
//	system("pause");
	return g-l==res;
}
void dfs(RG int l,RG int r,RG int limit){
	if(l>=r){
		if(solve()){
			printf("V[");write(len);
			printf("].pb(\"");
			write(res);puts("\");");
		}return;
	}
	
	//res=res+(Pow[r]-Pow[l])*10;
	//dfs(l+1,r-1,limit);
	for(RG int i=limit;i>=0;i--){
		res = res + (Pow[r] - Pow[l]) * i;
		dfs(l+1,r-1,i);
		res = res - (Pow[r] - Pow[l]) * i;
	}
//	res = res - (Pow[r] - Pow[l]) * 9;
}
signed main(){
	freopen("1.www","w",stdout);
	Pow[0]=1;
	Rep(i,1,30)Pow[i]=Pow[i-1]*10;
	res = 0;
	for(len=3;len<=30;len++){
		dfs(0,len-1,9);
	}
//	freopen("clever.out","w",stdout);
//	n = rd(),p = rd();
	return 0;
}

/*
3:495
4:6174
6:549945
8:63317664
8:97508421
9:554999445
9:864197532
10:1375059719
10:1354150909
11:
*/
