#include<bits/stdc++.h>
using namespace std;
#define gc getchar
#define RG register
#define pc putchar
#define Rep(i,a,b) for(RG int i=(a);i<=(b);i++)
#define rd read
#define mem(x,v) memset(x,v,sizeof(x))
inline int read(){
	int T = 0,f = 0;char c = gc();
	for(;c<'0'||c>'9';c=gc()) f|=(c=='-');
	for(;c>='0'&&c<='9';c=gc()) T=T*10+c-'0';
	return f?-T:T;
}
inline void write(int x){
	if(x<0)pc('-'),x=-x;
	if(x>=10) write(x/10);
	pc(x%10+'0');
}
const int maxn = 2009;
const double pi = acos(-1.0);
const double eps = 1e-8;
double L[maxn],R[maxn];
int ans = 0,n;
#define sqr(x) ((x)*(x))
int a[maxn],vis[maxn];
bool Jiao(int i,int j){
	if(L[i]>L[j]) swap(i,j);
	return R[i]>L[j] || R[j]-2*pi>L[i];
}
void solve(double x){
	*a=0;
	double l = 2333333,r = -2333333;
	for(int i=1;i<=n;i++){
//			printf("%lf %lf %lf;(%d)",L[i],x,R[i],L[i]<=x&&x<=R[i]);
//			printf("%lf %lf %lf;(%d)",L[i],x-2*pi,R[i],L[i]<=x-2*pi&&x-2*pi<=R[i]);
//			printf("%lf %lf %lf;(%d)",L[i],x+2*pi,R[i],L[i]<=x+2*pi&&x+2*pi<=R[i]);
		if(L[i]-eps<=x&&x<=R[i]+eps){
			a[++*a] = i;
			vis[i]=true;
//			l = min(l,L[i]),r = max(r,R[i]);
		} else
			vis[i]=false;
		
		
		
		 /*
		
		if(((L[i]<=x)&&(x<=R[i])) || ((L[i]<=x+2*pi)&&(x+2*pi<=R[i])) || ((L[i]<=x-2*pi)&&(x-2*pi<=R[i]))){
//			puts("");
			cnt++;
			l = min(l,L[i]),r = max(r,R[i]);
		}*/
	}
	Rep(i,1,n){
		if(vis[i]) continue;
		bool flag = true;
		Rep(j,1,*a){
			if(!Jiao(i,a[j])){
				flag = false;
				break;
			}
		}
		if(!flag)continue;
		ans=max(ans,*a+1);
	}
//	printf("%.2lf %.2lf %.2lf %d\n",x,l,r,cnt);
//	if((r-l-2*pi)>=1e-6) ans = max(ans,cnt);
}
int oR;
int main(){
	freopen("crazy.in","r",stdin);
	freopen("crazy.out","w",stdout);
	n = rd(),oR = rd();
	Rep(i,1,n){
		int x=rd(),y=rd();
		double deg1 = atan2(y,x);
		double deg2 = acos(1.0*oR / sqrt(x*x+y*y));
		
//		puts("");
//		puts("");
//		printf("%lf %lf\n",deg1,deg2);
//		puts("");
//		puts("");
		
		
		double l = deg1-deg2;
		double r = deg1+deg2;
		while(l<-pi)l+=2*pi,r+=2*pi;
//		printf("[%lf %lf]\n",l,r);
		while(r>2*pi) l-=2*pi,r-=2*pi;
//		printf("[%lf %lf]\n",l,r);
		L[i]=l;
		R[i]=r;
//		L[i+n]=l-2*pi;
//		R[i+n]=r-2*pi;
//		L[i+2*n]=l+2*pi;
//		R[i+2*n]=r+2*pi;
//		printf("%.4lf %.4lf\n",L[i],R[i]);
	}
	ans = 0;
	Rep(i,1,n){
		Rep(j,1,n){
			if(i==j)continue;
			if(Jiao(i,j)){
				Rep(k,1,n){
					if(k==i||k==j) continue;
					if(Jiao(i,k) && Jiao(j,k)){
						ans = 3;
						break;
					}
					if(ans==3)break;
				}
			}
			if(ans==3)break;
		}
		if(ans==3)break;
	}
//	Rep(i,1,n) printf("%lf %lf\n",L[i],R[i]);puts("");
//	Rep(i,n,n+n) printf("%lf %lf\n",L[i],R[i]);puts("");
//	Rep(i,2*n,3*n) printf("%lf %lf\n",L[i],R[i]);puts("");
	ans = 0;
//	for(double a=L[4];a<=R[4]+1;a+=0.001) solve(a);
	Rep(i,1,n){
		solve(L[i]);//solve(L[i]);
		solve(R[i]>pi?R[i]-pi:R[i]);//solve(R[i]);
	}
	printf("%d\n",ans);
	return 0;
}
/*
*/
