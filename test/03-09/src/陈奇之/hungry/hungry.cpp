#include<bits/stdc++.h>
using namespace std;
#define Rep(i,a,b) for(i=(a);i<=(b);i++)
#define Down(i,a,b) for(i=(a);i>=(b);i--)
#define gc getchar
#define RG register
#define pc putchar
#define rd read
#define mem(x,v) memset(x,v,sizeof(x))
inline int read(){
	int T = 0,f = 0;char c = gc();
	for(;c<'0'||c>'9';c=gc()) f|=(c=='-');
	for(;c>='0'&&c<='9';c=gc()) T=T*10+c-'0';
	return f?-T:T;
}
inline void write(int x){
	if(x<0)pc('-'),x=-x;
	if(x>=10) write(x/10);
	pc(x%10+'0');
}
const int maxn = 805;
int n,m,k,l,i,j;
int len[maxn][maxn],a[maxn][maxn],x[maxn],y[maxn],ans[maxn][maxn];
bool choose[maxn];
bool cmp(int a,int b){
	return len[a][j] > len[b][j];
}
int main(){
	freopen("hungry.in","r",stdin);
	freopen("hungry.out","w",stdout);
	n = rd(),m = rd(),k = rd(),l = rd();
	Rep(i,1,n)
		Rep(j,1,m){
			a[i][j]=gc();
			while(a[i][j]!='1'&&a[i][j]!='0') a[i][j]=gc();
			a[i][j]-='0';
		}
	mem(len,0);
	Rep(i,1,n){
		Down(j,m,1){
			if(a[i][j]==0) len[i][j] = 0; else len[i][j] = len[i][j+1] + 1;
		}
	}
//	Rep(i,1,n){
//		Rep(j,1,m) 
//			printf("%d ",len[i][j]);
//		puts("");
//	}
	mem(choose,false);
	//Rep(i,1,k) choose[i]=true;
	Rep(j,1,m){//ö������ 
		mem(x,0);mem(y,0);
		Rep(i,1,n) 
			if(choose[i]) x[++*x] = i; else
			if(len[i][j]) y[++*y] = i;
		sort(x+1,x+1+*x,cmp);sort(y+1,y+1+*y,cmp);
//		Rep(i,1,n) printf("%d ",y[i]);puts("");
		if(j!=1){
			for(i=1;i<=l&&i<=*y;i++){
				if(len[y[i]][j] > len[x[k-i+1]][j])
					swap(y[i],x[k-i+1]);
			}
		} else{
			swap(x,y);
		}
//		Rep(i,1,n) printf("%d ",x[i]);puts("");
		sort(x+1,x+1+k);
		mem(choose,false);
		Rep(i,1,k){
			choose[x[i]] = true;
			if(!a[x[i]][j] || !x[i]){
				puts("-1");
				return 0;
			}
			ans[i][j]=x[i];
		}
	}
	Rep(i,1,m){
		Rep(j,1,k-1)write(ans[j][i]),pc(' ');
		write(ans[k][i]);
		puts("");
	}
	return 0;
}
/*
5 4 3 2
1001
1101
1111
1110
0110
*/
