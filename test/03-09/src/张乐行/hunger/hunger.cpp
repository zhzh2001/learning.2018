#include<cstdio>
#include<algorithm>
using namespace std;
const int N=805;
int n,m,nk,nl,i,j,k;
int f[N][N],a[N],ans[N][N];
bool p[N];char s[N];
struct arr{int x,k;}d[N];
bool operator < (arr A,arr B){return A.k<B.k;}
void write(int x){if (x>9) write(x/10);putchar(x%10+48);}
int main(){
	freopen("hunger.in","r",stdin);
	freopen("hunger.out","w",stdout);
	scanf("%d%d%d%d",&n,&m,&nk,&nl);
	for (i=1;i<=n;i++)
		for (scanf("%s",s+1),j=m;j;j--)
			f[i][j]=s[j]=='1'?f[i][j+1]+1:0;
	for (j=1;j<=m;j++){
		for (i=1;i<=n;i++)
			d[i]=(arr){i,f[i][j]},p[i]=0;
		sort(d+1,d+n+1);
		if (j==1){
			for (i=1;i<=nk;i++)
				a[i]=ans[j][i]=d[n+1-i].x;
			sort(ans[1]+1,ans[1]+nk+1);
			continue;
		}
		for (i=1;i<=nk;i++) p[a[i]]=1;
		for (i=1,k=nl;k;i++)
			if (p[d[i].x]) p[d[i].x]=0,k--;
		for (i=n,k=nl;k;i--)
			if (!p[d[i].x]) p[d[i].x]=1,k--;
		for (i=1,k=0;i<=n;i++) if (p[i]){
			if (!f[i][j]){puts("-1");return 0;}
			a[++k]=i;ans[j][k]=i;
		}
	}
	for (j=1;j<=m;j++,putchar('\n'))
		for (i=1;i<=nk;i++)
			write(ans[j][i]),putchar(' ');
}
