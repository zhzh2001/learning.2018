#include<cstdio>
#include<algorithm>
using namespace std;
const int N=35;
int n,i,a[N],b[N],g[10];
void check(){
	int i;
	for (i=0;i<=9;i++) g[i]=0;
	for (i=1;i<=n;i++) b[i]=0,g[a[i]]++;
	for (i=1;i<=n;i++){
		b[i]+=a[i]-a[n+1-i];
		if (b[i]<0) b[i]+=10,b[i+1]--;
		if (!(g[b[i]]--)) return;
	}
	for (i=n;i;i--) putchar(b[i]+48);
	putchar('\n');
}
void dfs(int x,int k){
	if (x>n){
		if (a[n]) check();
		return;
	}
	for (int i=k;i<=9;i++)
		a[x]=i,dfs(x+1,i);
}
void solve(int rn){
	n=rn;
	dfs(1,0);
}
int main(){
	freopen("1.in","w",stdout);
	for (int i=1;i<=30;i++){
		solve(i);
	}
}
