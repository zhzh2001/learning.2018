#include<cstdio>
#include<cstdlib>
#include<algorithm>
#define ll long long
using namespace std;
const int N=2005,LIM=5e7,NUM=15e7;
int n,nr,i,j,d[N],ans,cnt;
bool a[N][N];
struct Pt{int x,y;}p[N];
bool chk(Pt A,Pt B){
	int k=A.x*B.y-B.x*A.y;
	int v=(A.x-B.x)*(A.x-B.x)+(A.y-B.y)*(A.y-B.y);
	return (ll)k*k>(ll)nr*nr*v;
}
void dfs(int x,int k){
	cnt++;
	if (cnt>NUM){printf("%d",ans);exit(0);}
	if (k+(n-x+1)<=ans) return;
	if (x>n){ans=k;return;}
	bool flag=1;
	for (int i=1;i<=k;i++)
		if (!a[d[i]][x]){flag=0;break;}
	cnt+=k;
	if (cnt>NUM){printf("%d",ans);exit(0);}
	if (flag) d[k+1]=x,dfs(x+1,k+1);
	dfs(x+1,k);
}
int tot;
void work(){
	srand(59382147);rand();
	for (;tot<LIM;){
		for (i=1;d[i-1]<n;i++){
			int x=d[i-1];
			if (i-1+n-x<=ans) break;
			bool flag=0;
			for (;x<n;){
				x=x+rand()%min(5,n-x)+1;
				tot+=i;flag=1;
				for (j=1;j<i;j++)
					if (!a[d[j]][x]){flag=0;break;}
				if (flag) break;
			}
			if (!flag) break;
			d[i]=x;
		}
		if (i-1>ans) ans=i-1;
	}
}
int main(){
	freopen("crazy.in","r",stdin);
	freopen("crazy.out","w",stdout);
	scanf("%d%d",&n,&nr);
	for (i=1;i<=n;i++)
		scanf("%d%d",&p[i].x,&p[i].y);
	for (i=2;i<=n;i++) for (j=1;j<i;j++)
		a[j][i]=chk(p[i],p[j]);
	work();
	dfs(1,0);
	printf("%d",ans);
}
