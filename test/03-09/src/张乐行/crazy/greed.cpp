#include<cstdio>
#include<algorithm>
#define ll long long
using namespace std;
const int N=2005,LIM=5e7;
int n,nr,i,j,d[N],tot,ans;
bool a[N][N];
struct Pt{int x,y;}p[N];
bool chk(Pt A,Pt B){
	int k=A.x*B.y-B.x*A.y;
	int v=(A.x-B.x)*(A.x-B.x)+(A.y-B.y)*(A.y-B.y);
	return (ll)k*k>(ll)nr*nr*v;
}
int main(){
	freopen("crazy.in","r",stdin);
	freopen("greed.out","w",stdout);
	srand(59382147);rand();
	scanf("%d%d",&n,&nr);
	for (i=1;i<=n;i++)
		scanf("%d%d",&p[i].x,&p[i].y);
	for (i=2;i<=n;i++) for (j=1;j<i;j++)
		a[j][i]=chk(p[i],p[j]);
	for (;tot<LIM;){
		for (i=1;d[i-1]<n;i++){
			int x=d[i-1];
			if (i-1+n-x<=ans) break;
			bool flag=0;
			for (;x<n;){
				x=x+rand()%min(5,n-x)+1;
				tot+=i;flag=1;
				for (j=1;j<i;j++)
					if (!a[d[j]][x]){flag=0;break;}
				if (flag) break;
			}
			if (!flag) break;
			d[i]=x;
		}
		if (i-1>ans) ans=i-1;
	}
	printf("%d",ans);
}
