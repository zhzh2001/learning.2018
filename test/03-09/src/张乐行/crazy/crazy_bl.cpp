#include<cstdio>
#define ll long long
const int N=2005;
int n,nr,i,j,d[N],ans;
bool a[N][N];
struct Pt{int x,y;}p[N];
bool chk(Pt A,Pt B){
	int k=A.x*B.y-B.x*A.y;
	int v=(A.x-B.x)*(A.x-B.x)+(A.y-B.y)*(A.y-B.y);
	return (ll)k*k>(ll)nr*nr*v;
}
/*
void dfs(int x,int k){
	if (k+(n-x+1)<=ans) return;
	if (x>n){ans=k;return;}
	dfs(x+1,k);
	for (int i=1;i<=k;i++)
		if (!a[d[i]][x]) return;
	d[k+1]=x;
	dfs(x+1,k+1);
}
*/
void dfs(int x,int k){
	if (k+(n-x+1)<=ans) return;
	if (x>n){ans=k;return;}
	bool flag=1;
	for (int i=1;i<=k;i++)
		if (!a[d[i]][x]){flag=0;break;}
	if (flag) d[k+1]=x,dfs(x+1,k+1);
	dfs(x+1,k);
}
int main(){
	freopen("crazy.in","r",stdin);
	freopen("crazy_bl.out","w",stdout);
	scanf("%d%d",&n,&nr);
	for (i=1;i<=n;i++)
		scanf("%d%d",&p[i].x,&p[i].y);
	for (i=2;i<=n;i++) for (j=1;j<i;j++)
		a[j][i]=chk(p[i],p[j]);
	dfs(1,0);
	printf("%d",ans);
}
