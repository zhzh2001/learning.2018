#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int n,m,k,l,ti,x,f[810][810],le[810][810],a[810],b[810],usd[810],ans[810][810];
char st[810][810];
bool cmp(int a,int b){
	return le[ti][a]>le[ti][b];
}
bool stb(int a,int b){
	return a<b;
}
int main(){
	freopen("hungry.in","r",stdin);
	freopen("hungry.out","w",stdout);
	scanf("%d%d%d%d",&n,&m,&k,&l);
	int i,j,*p=a,*q=b;
	for(i=1;i<=n;i++)
		scanf("%s",st[i]+1);
	for(j=m;j;j--)
		for(i=1;i<=n;i++)
			if(st[i][j]=='0')le[j][i]=0;
			else le[j][i]=(le[j+1][i]+1);
	for(j=1;j<=m;j++){
		ti=j;
		x=0;
		for(i=1;i<=n;i++)
			if(le[j][i]&&!usd[i]){
				x++;
				p[x]=i;
			}
		sort(p+1,p+x+1,cmp);
		if(j!=1){
			sort(q+1,q+k+1,cmp);
			for(i=1;i<=min(x,l);i++)
				if(le[j][p[i]]>le[j][q[k-i+1]])
					swap(p[i],q[k-i+1]);
			if(i!=k+1&&!le[j][q[k-i+1]]){
				puts("-1");
				return 0;
			}
			swap(p,q);
		}
		else if(x<k){
				puts("-1");
				return 0;
			}
		memset(usd,0,sizeof(usd));
		for(i=1;i<=k;i++){
			usd[p[i]]=1;
			ans[j][i]=p[i];
		}
		swap(p,q);
	}
	for(j=1;j<=m;j++){
		sort(ans[j]+1,ans[j]+k+1,stb);
		for(i=1;i<=k;i++)
			printf("%d ",ans[j][i]);
		puts("");
	}
	return 0;
}
/*
5 4 3 1
1001
1101
1111
1110
0111
*/
