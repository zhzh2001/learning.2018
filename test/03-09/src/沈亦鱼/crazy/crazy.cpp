#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int n,R,t1,t2,s,ans,x[2100],y[2100],p1[4100000],n1[4100000],h1[2100],p2[4100000],n2[4100000],h2[2100],f[2100];
double r,a,b;
void adg1(int u,int v){
	t1++;
	p1[t1]=v;
	n1[t1]=h1[u];
	h1[u]=t1;
}
void adg2(int u,int v){
	t2++;
	p2[t2]=v;
	n2[t2]=h2[u];
	h2[u]=t2;
}
void dfs(int k){
	f[k]++;
	for(int i=h2[k];i;i=n2[i])
		f[p2[i]]++;
	s++;
	if(s>ans)ans=s;
	for(int i=h1[k];i;i=n1[i])
		if(!f[p1[i]])dfs(p1[i]);
	s--;
	for(int i=h2[k];i;i=n2[i])
		f[p2[i]]--;
	f[k]--;
}
int main(){
	freopen("crazy.in","r",stdin);
	freopen("crazy.out","w",stdout);
	scanf("%d%d",&n,&R);
	r=R;
	for(int i=1;i<=n;i++)
		scanf("%d%d",&x[i],&y[i]);
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++){
			a=(y[i]-y[j])/(x[i]-x[j]);
			b=y[i]-a*x[i];
			if(b*b/(a*a+1)+1e-8>r*r){
				adg1(i,j);
				adg1(j,i);
			}
			else{
				adg2(i,j);
				adg2(j,i);
			}
		}
	for(int i=1;i<=n;i++)
		dfs(i);
	printf("%d",ans);
	return 0;
}
