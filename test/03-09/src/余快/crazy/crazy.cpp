#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
#define db double
#define N 2005
#define eps 1e-6
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void wrn(int x){
	write(x);puts("");
}
int num,vis[N],ans,n,m,x[N],y[N],r,a[N];
int nedge,Next[N*N],head[N],to[N*N];
void add(int a,int b){Next[++nedge]=head[a];head[a]=nedge;to[nedge]=b;}
double dis(int a){
	return sqrt((db)x[a]*x[a]+(db)y[a]*y[a]);
}
int pd(int a,int b){
	if (y[a]==y[b]){
		if (x[a]*x[b]<0) return y[a]<r;
		else return min(dis(a),dis(b))+eps<r;
	}
	else{
		int bbb=b;
		db t=min(dis(a),dis(b));
		db xx=x[a],yy=y[a],fx=x[b],fy=y[b];
		db k=(fy-yy)/(fx-xx);
		db b=yy-xx*k;
		db a1=b;
		db b1=-b/k;
		db s=sqrt(a1*a1+b1*b1);
		db lon=fabs(a1*b1)/s;
		db k2=-1/k;
		db o=sqrt(1+k2*k2);
		o=lon/o;
		if (b1<0) o=-o;
		return lon+eps<r;
	}
}
void dfs(int x){
	num++;
	if (num*n>3e7) return;
	int pd=1;
	for (int i=1;i<=n;i++){
		if (num*n>3e7) return;
		if (!vis[i]){
			vis[i]++;
			for (int j=head[i];j;j=Next[j]) vis[to[j]]++;
			dfs(x+1);
			vis[i]--;
			for (int j=head[i];j;j=Next[j]) vis[to[j]]--;
			pd=0;
		}
	}
	if (pd) ans=max(ans,x);
}
int a1,b1;
signed main(){
	freopen("crazy.in","r",stdin);
	freopen("crazy.out","w",stdout);
	n=read();r=read();srand(2333);
	for (int i=1;i<=n;i++){
		x[i]=read();y[i]=read();
	}
	for (int i=1;i<=n;i++){
		a1=rand()%n+1;b1=rand()%n+1;
		swap(x[a1],x[b1]);swap(y[a1],y[b1]);
	}
	for (int i=1;i<=n;i++){
		for (int j=i+1;j<=n;j++){
			if (pd(i,j)) 
			add(i,j),add(j,i);
		}
	}
	if (n<=100){
		ans=0;
		dfs(0);
		wrn(ans);
		return 0;
	}
	else{
		dfs(0);
		wrn(ans);
	}
	return 0;
}
