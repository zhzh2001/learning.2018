#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void wrn(int x){
	write(x);puts("");
}
int f[12]={0,0,0,245025,38118276,0,701565254721,0,13517018740330137,1054861758265199049,554999445,864197352};
int n,mod;
signed main(){
	freopen("clever.in","r",stdin);
	freopen("clever.out","w",stdout);
	n=read();mod=read();
	if (n<10){
		wrn(f[n]%mod);
	}
	else if (n==10){
		int ans=(f[10]%mod)*(f[10]%mod)%mod,ans2=f[11]%mod*f[11]%mod;
		ans=ans+ans2;ans=ans%mod;
		wrn(ans);
	}
	else{wrn(0);}
	return 0;
}
