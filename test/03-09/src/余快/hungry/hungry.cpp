#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
#define N 13
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void wrn(int x){
	write(x);puts("");
}
int wzp[N][N],c[N],p[1<<N],cnt,n,m,k,l,sum,g[N],f[1<<N][N],pre[1<<N][N];
void dfs(int x,int tot){
	if (x==m+1){
		if (tot!=k) return;
		p[cnt]=1;
		return;
	}
	cnt+=1<<(x-1);
	dfs(x+1,tot+1);
	cnt-=1<<(x-1);
	dfs(x+1,tot);
}
int ch(int x){
	return x==0?1:0;
}
int pd(int a,int b){
	memset(c,0,sizeof(c));
	for (int i=0;i<m;i++){
		c[i]+=(a>>i)%2;c[i]-=(b>>i)%2;
	}
	int num=0;
	for (int i=0;i<m;i++){
		if (c[i]!=0) num++;
	}
	return num/2<=l;
}
char str[N],a[N][N];
signed main(){
	freopen("hungry.in","r",stdin);
	freopen("hungry.out","w",stdout);
	n=read();m=read();k=read();l=read();
	for (int i=1;i<=n;i++){
		scanf("%s",str+1);
		for (int j=1;j<=m;j++){
			a[i][j]=str[j]-'0';
		}
	}
	swap(n,m);
	for (int i=1;i<=n;i++){
		sum=0;
		for (int j=1;j<=m;j++){
			sum+=a[j][i];g[i]+=a[j][i]<<(j-1);
		}
		if (sum<k){
			wrn(-1);return 0;
		}
	}
	if (m>20){
		wrn(-1);return 0;
	}
	dfs(1,0);
	for (int i=1;i<(1<<m)-1;i++){
//		printf("%d %d\n",p[i],i&g[1]);
		if (p[i]&&((i&g[1])==i)){
			f[i][1]=1;
		}
	}
	for (int i=2;i<=n;i++){
		for (int j=1;j<(1<<m)-1;j++){
			if (f[j][i-1]){
				for (int k=1;k<(1<<m)-1;k++){
					if (((k&g[i])==k)&&p[k]){
						if (pd(j,k)) f[k][i]=1,pre[k][i]=j;
					}
				}
			} 
		}
	}
	for (int i=1;i<(1<<m)-1;i++){
		if (f[i][n]){
			int yk=i;
			for (int j=n;j>=1;yk=pre[yk][j],j--){
				int xpp=yk,www=0;
				for (int k=0;k<m;k++){
					xpp=(yk>>k)%2;
					if (xpp) wzp[j][++www]=k+1;
				}
			}
			for (int j=1;j<=n;j++){
				for (int ii=1;ii<=k;ii++){
					printf("%d ",wzp[j][ii]);
				}
				printf("\n");
			}
			return 0;
		}
	}
	wrn(-1);
	return 0;
}
