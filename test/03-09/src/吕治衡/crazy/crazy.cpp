#include<bits/stdc++.h>
#define ll long long
#define ld long double
#define eps 1e-15
ll read(){
	char c=getchar();while (c!='-'&&(c<'0'||c>'9'))c=getchar();
	int k=0,kk=1;if (c=='-')kk=-1,c=getchar();
	while (c>='0'&&c<='9')k=k*10+c-'0',c=getchar();return kk*k;
}void write(ll x){if (x<0)putchar('-'),x=-x;if (x/10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);puts("");}
using namespace std;
int n,r,x[2010],y[2010],f[2010][2010],flag[2010],sum,ans,ddd;
ld t;
int check(int i,int j){
	if (x[i]==x[j]&&y[i]==y[j])return 1;
	if (x[i]==x[j]&&abs(x[i])<r)return 0;
	if (y[i]==y[j]&&abs(y[i])<r)return 0;
	ld k=1.0*(y[i]-y[j])/(x[i]-x[j]),b=-k*x[i]+y[i];
	return k*k*b*b<(1+k*k)*(b*b-r*r);
}inline int calc(int x){int dd=1;for (int i=1;i<=n;i++)dd+=flag[i]&f[x][i];return dd;}
signed main(){
	freopen("crazy.in","r",stdin);freopen("crazy.out","w",stdout);
	srand(time(0));
	n=read();r=read();for (int i=1;i<=n;i++)x[i]=read(),y[i]=read();
	for (int i=1;i<=n;i++)for (int j=i;j<=n;j++)f[i][j]=f[j][i]=check(i,j);
	t=100;while (t>eps){
		int d=rand()%n+1;while (flag[d])d=rand()%n+1;
		int dd=calc(d);if (dd>sum||exp(1.0*(dd-sum)/t)*RAND_MAX>rand()){
			sum=dd;flag[d]=1;for (int i=1;i<=n;i++)flag[i]&=f[d][i];
		}t*=0.99997;ans=max(ans,sum);
	}writeln(ans);
}
