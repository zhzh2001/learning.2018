#include<iostream>
#include<stdio.h>
#include<math.h>
#include<ctime>
#include<string.h>
#include<algorithm>
#define inf 0x3f3f3f3f
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define eps 1e-8
using namespace std;
inline int read(){
	int x=0;int f=1;char ch=getchar();
	while(ch<'0' || ch>'9') {if(ch=='-') f=-1;ch=getchar();}
	while(ch>='0' && ch<='9') {x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(int x){
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	if(x<0){putchar('-'); x=-x;}
	write(x); puts("");
}
int ans=0,in[2001],top,n,q[2001],t[2001];
double x[2001],y[2001],R;
bool d[2001][2001];
inline void Add(int x,int y)
{
	in[x]++;
	in[y]++;d[x][y]=d[y][x]=1;
}
inline void check(int p1,int p2)
{
	if(x[p1]==x[p2])	{if(fabs(x[p1])>=R-eps)	Add(p1,p2);return;}
	if(y[p1]==y[p2])	{if(fabs(y[p1])>=R-eps)	Add(p1,p2);return;}
	double k=(y[p1]-y[p2])/(x[p1]-x[p2]),b=y[p1]-x[p1]*k;
	double dis=fabs(b*(b/k));
	dis=dis/sqrt(b*b+(k/b)*(k/b));
	if(dis>=R-eps)	Add(p1,p2);
}
ll tim;
inline void Dfs(int x)
{
	tim++;
	ans=max(ans,top);
	if(tim>80000000)	return;//rp++++++++
	if(x==n+1)	return;
	bool can=1;
	For(i,1,top)	if(!d[t[x]][q[i]])	{can=0;tim++;break;}
	if(can)	q[++top]=t[x],Dfs(x+1),top--;
	Dfs(x+1);
}
inline bool cmp(int x,int y){return in[x]>in[y];}
int main()
{
	freopen("crazy.in","r",stdin);freopen("crazy.out","w",stdout);
	srand(time(0));
	n=read();R=read();
	For(i,1,n)	x[i]=read(),y[i]=read();
	For(i,1,n)	For(j,i+1,n)	check(i,j);	
	For(i,1,n)	t[i]=i;sort(t+1,t+n+1,cmp);
//	For(i,1,n)	For(j,1,n)	if(i!=j)	if(!d[i][j])	cout<<x[i]<<' '<<y[i]<<' '<<x[j]<<' '<<y[j]<<endl;
	Dfs(1);
	writeln(ans);
}

