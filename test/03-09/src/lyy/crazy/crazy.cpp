#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <cstring>
using namespace std;
#define sqr(_) ((_)*(_))

typedef pair<double, double> P;
typedef pair<int, int> Pii;
#define x first
#define y second
#define mp make_pair
#define pb push_back

P operator +(const P &a, const P &b) {
	return P(a.x + b.x, a.y + b.y);
}
P operator -(const P &a, const P &b) {
	return P(a.x - b.x, a.y - b.y);
}
P operator *(const P &a, double k) {
	return P(a.x * k, a.y * k);
}
P operator /(const P &a, double k) {
	return P(a.x / k, a.y / k);
}
double dot(const P &a, const P &b) {
	return a.x * b.x + a.y * b.y;
}
double len2(const P &a) {
	return dot(a, a);
}
double len(const P &a) {
	return sqrt(dot(a, a));
}

const int MAXN = 4000 + 10;

P p[MAXN];
Pii w[MAXN];
double R, c[MAXN];
int n, f[MAXN], g[MAXN], id[MAXN], Tohka, h[MAXN];

void init()
{
	scanf("%d%lf", &n, &R);
	for (int i = 1; i <= n; ++i)
		scanf("%lf%lf", &p[i].x, &p[i].y);
}
void calc(int i)
{
	double D = len(p[i]), h = sqrt(len2(p[i]) - sqr(R)) * R / D, a = sqrt(sqr(R) - sqr(h));
	P H = p[i] / D * a, v = p[i] / D;
	swap(v.x, v.y); v.x = -v.x;
	v = v * h;
	P l = H + v, r = H - v;
	double agll = atan2(l.y, l.x), aglr = atan2(r.y, r.x);
	if (agll > aglr)
		swap(agll, aglr);
//	c[i] = mp(agll, aglr);
	c[i * 2 - 1] = agll;
	c[i * 2] = aglr;
}
bool cmp(int a, int b) {
	return c[a] < c[b];
}
int lowbit(int x) {
	return x & -x;
}
void Add(int x, int d)
{
	for (; x <= n * 2; f[x] = max(f[x], d), x += lowbit(x));
}
int Gets(int x)
{
	int Tohka = 0;
	for (; x; Tohka = max(Tohka, f[x]), x -= lowbit(x));
	return Tohka;
}
int solve(int s)
{
	memset(f, 0, sizeof(f));
	Add(w[s].y, 1);
	int Tohka = 1;
	for (int i = s + 1; i <= n; ++i)
	{
		if (w[i].x > w[s].y)
			break;
		int g = Gets(w[i].y);
		if (g > 0) ++g;
		Tohka = max(Tohka, g);
		Add(w[i].y, g);
	}
	return Tohka;
}
void solve()
{
	for (int i = 1; i <= n; ++i)
		calc(i);
	for (int i = 1; i <= n * 2; ++i)
		id[i] = i;
	sort(id + 1, id + n * 2 + 1, cmp);
	for (int i = 1; i <= n * 2; ++i)
		h[id[i]] = i;
	for (int i = 1; i <= n; ++i)
//		printf("%d %d\n", h[i * 2 - 1], h[i * 2]);
		w[i] = mp(h[i * 2 - 1], h[i * 2]);
	sort(w + 1, w + n + 1);
	int Tohka = 0;
	for (int i = 1; i <= n; ++i)
		Tohka = max(solve(i), Tohka);
	cout << Tohka << endl;
}
int main()
{
	freopen("crazy.in", "r", stdin), freopen("crazy.out", "w", stdout);
	
	init();
	solve();
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}
