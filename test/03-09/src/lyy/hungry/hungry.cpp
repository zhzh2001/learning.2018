#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <cstring>
using namespace std;

#define mp make_pair

const int MAXN = 800 + 10;

bool a[MAXN][MAXN], used[MAXN];
int n, m, k, l, T, b[MAXN][MAXN], c[MAXN], id[MAXN], Tohka[MAXN][MAXN];
char str[MAXN];

void init()
{
	scanf("%d%d%d%d", &n, &m, &k, &l);
	for (int i = 1; i <= n; ++i)
	{
		scanf("%s", str + 1);
		for (int j = 1; j <= m; ++j)
			a[i][j] = (str[j] == '1');
		b[i][m + 1] = m + 1;
		for (int j = m; j >= 1; --j)
			b[i][j] = (!a[i][j]) ? j : b[i][j + 1];
	}
}
bool cmp(int x, int y) {
	return mp(b[x][T], x) > mp(b[y][T], y);
}
void solve()
{
	for (int i = 1; i <= n; ++i)
		id[i] = i;
	T = 1;
	sort(id + 1, id + n + 1, cmp);
	for (int i = 1; i <= k; ++i)
	{
		c[i] = id[i], used[id[i]] = 1;
		if (b[id[i]][1] == 1)
			puts("-1"), exit(0);
	}
	memcpy(Tohka[1], c, sizeof(c));
	for (int i = 2; i <= m; ++i)
	{
		T = i;
		sort(id + 1, id + n + 1, cmp);
		sort(c + 1, c + k + 1, cmp);
		for (int j = 1, p = k, res = l; j <= n && p >= 1 && res; ++j)
			if (!used[id[j]])
				if (b[id[j]][i] > b[c[p]][i])
					used[c[p]] = false, c[p] = id[j], used[id[j]] = true, --p, --res;
		for (int j = 1; j <= k; ++j)
			if (b[c[j]][i] == i)
				puts("-1"), exit(0);
		memcpy(Tohka[i], c, sizeof(c));
	}
	for (int i = 1; i <= m; ++i, puts(""))
		for (int j = 1; j <= k; ++j)
			printf("%d ", Tohka[i][j]);
}
int main()
{
	freopen("hungry.in", "r", stdin), freopen("hungry.out", "w", stdout);
	
	init();
	solve();
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}
