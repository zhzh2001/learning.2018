#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
typedef double D;
const D pi=acos(-1);
struct ppap{int x,y;}a[2010];
int n,r,d[2010][2010],dx[2010];
inline D sqr(D x){return x*x;}
inline bool check(int x,int y){
	int px=a[y].x-a[x].x,py=a[y].y-a[x].y;
	D p1=atan2(py,px);
	px=-a[x].x;py=-a[x].y;
	D p2=atan2(py,px);
	D p=p1-p2;
	D rk=(sqr(px)+sqr(py))*sqr(sin(p));
	return (sqr(r)<rk)?1:0;
}
int ans=0,sum=0,q[2010];
bool b[2010];
inline bool check(){
	q[0]=0;
	for(int i=1;i<=n;i++)if(b[i])q[++q[0]]=i;
	for(int i=1;i<=q[0];i++)
		for(int j=1;j<i;j++)if(!d[q[i]][q[j]]){
			return 0;
		}
	return 1;
}
inline void dfs(int x,int s){
	if(x==n+1){
		if(check())ans=max(ans,s);
		return;
	}
	b[x]=1;dfs(x+1,s+1);
	b[x]=0;dfs(x+1,s);
}
int main()
{
	freopen("crazy.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();r=read();
	for(int i=1;i<=n;i++)a[i].x=read(),a[i].y=read();
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++)if(check(i,j)){
			d[i][j]=d[j][i]=1;
			dx[i]++;dx[j]++;
			sum+=2;
		}
	dfs(1,0);
	writeln(ans);
	return 0;
}
