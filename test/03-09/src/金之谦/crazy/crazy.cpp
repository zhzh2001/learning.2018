#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
typedef double D;
const D pi=acos(-1);
struct ppap{int x,y;}a[2010];
int n,r,d[2010][2010];
inline D sqr(D x){return x*x;}
inline bool check(int x,int y){
	int px=a[y].x-a[x].x,py=a[y].y-a[x].y;
	D p1=atan2(py,px);
	px=-a[x].x;py=-a[x].y;
	D p2=atan2(py,px);
	D p=p1-p2;
	D rk=(sqr(px)+sqr(py))*sqr(sin(p));
	return (sqr(r)<rk)?1:0;
}
bool b[2010];
int s[2010],q[2010],top=0,ans=0;
int main()
{
	freopen("crazy.in","r",stdin);
	freopen("crazy.out","w",stdout);
	n=read();r=read();
	for(int i=1;i<=n;i++)a[i].x=read(),a[i].y=read();
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++)if(check(i,j)){
			d[i][j]=d[j][i]=1;
		}
	srand(time(0));
	int Cnt=0;
	for(int i=1;i<=100000;i++){
		int cnt=0;
		for(int i=1;i<=n;i++)if(!b[i])q[++cnt]=i;
		if(!cnt)break;
		int p=q[rand()%cnt+1];
		int flag=1;
		for(int j=1;j<=top;j++)if(!d[s[j]][p]){flag=0;break;}
		if(flag){
			s[++top]=p;Cnt=0;b[p]=1;
			ans=max(ans,top);
		}else Cnt++;
		if(Cnt>100){
			while(top)b[s[top]]=0,top--;
		}
	}
	writeln(ans);
	return 0;
}
