#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
typedef double D;
const D pi=acos(-1);
struct ppap{int x,y;}a[2010];
int nedge=0,p[8000010],nex[8000010],head[8000010];
inline void addedge(int a,int b){
	p[++nedge]=b;nex[nedge]=head[a];head[a]=nedge;
}
int n,r,d[2010][2010];
inline D sqr(D x){return x*x;}
inline bool check(int x,int y){
	int px=a[y].x-a[x].x,py=a[y].y-a[x].y;
	D p1=atan2(py,px);
	px=-a[x].x;py=-a[x].y;
	D p2=atan2(py,px);
	D p=p1-p2;
	D rk=(sqr(px)+sqr(py))*sqr(sin(p));
	return (sqr(r)<rk)?1:0;
}
int b[2010],c[2010];
int s[2010],q[2010],top=0,ans=0;
int main()
{
//	freopen("crazy.in","r",stdin);
//	freopen("crazy.out","w",stdout);
	n=read();r=read();
	for(int i=1;i<=n;i++)a[i].x=read(),a[i].y=read();
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++)if(check(i,j)){
			addedge(i,j);addedge(j,i);
		}
	srand(time(0));
	ans=1;
	for(int op=1;op<=10000;op++){
		memset(b,0,sizeof b);
		memset(c,0,sizeof c);
		top=1;s[1]=rand()%n+1;
		c[s[1]]=1;
		int cnt=0;
		for(int k=head[s[1]];k;k=nex[k])q[++cnt]=p[k],b[p[k]]++;
		if(!cnt)continue;
		s[++top]=q[rand()%cnt+1];
		c[s[2]]=1;
		ans=max(ans,2);
		int la=top;
		while(1){
			for(int i=la;i<=top;i++)
				for(int k=head[s[i]];k;k=nex[k])b[p[k]]++;
			la=top;
			for(int i=1;i<=n;i++)if(!c[i]&&b[i]==la)s[++top]=i,c[i]=1;
			ans=max(ans,top);
			if(la==top)break;
			la++;
		}
	}
	writeln(ans);
	return 0;
}
