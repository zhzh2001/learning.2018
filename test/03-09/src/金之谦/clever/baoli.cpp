#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int n,MOD,a[50],b[10],ans=0;
inline int sqr(int x){return x*x;}
struct bigint{int c[40];}p,q;
bigint operator -(bigint a,bigint b){
	int n=a.c[0];
	for(int i=1;i<=n;i++){
		if(a.c[i]<b.c[i]){a.c[i+1]--;a.c[i]+=10;}
		a.c[i]-=b.c[i];
	}
	return a;
}
inline void shuchu(bigint x){
	for(int i=x.c[0];i;i--)write(x.c[i]);
}
inline void check(){
	memset(b,0,sizeof b);
	p.c[0]=q.c[0]=n;
	for(int i=1;i<=n;i++)p.c[i]=a[i],b[a[i]]++;
	for(int i=n;i;i--)q.c[n-i+1]=a[i];
	bigint rp=p-q;
	for(int i=1;i<=n;i++){
		if(!b[rp.c[i]])return;
		b[rp.c[i]]--;
	}
	for(int i=0;i<10;i++)if(b[i])return;
	while(rp.c[0]&&rp.c[rp.c[0]]==0)rp.c[0]--;
	if(!rp.c[0])return;
	printf("Ans[%lld].push_back(",n);shuchu(rp);puts(");");
}
inline void dfs(int x,int p){
	if(x==n+1){
		check();return;
	}
	for(int i=p;i<10;i++){
		a[x]=i;dfs(x+1,i);
	}
}
signed main()
{
	freopen("clever.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();MOD=read();
	dfs(1,0);
	return 0;
}
/*
*/
