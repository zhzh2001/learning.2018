#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int n,m,K,L;
char s[1010][1010];
int f[11][10000];
bool b[11];
inline void check(int k){
	int ss=0;
	for(int i=1;i<=n;i++)if(b[i])ss|=(1<<(i-1));
	if(k==1)f[k][ss]=1;
	else{
		for(int i=0;i<(1<<n);i++)if(f[k-1][i]){
			int rp=i&ss,sum=0;
			for(;rp;rp-=rp&-rp)sum++;
			if(sum<K-L)continue;
			f[k][ss]=i;
			break;
		}
	}
}
inline void dfs(int k,int x,int sum){
	if(n-x+1<sum)return;
	if(!sum){check(k);return;}
	for(int i=x;i<=n;i++)if(s[i][k]=='1'){
		b[i]=1;dfs(k,i+1,sum-1);
		b[i]=0;
	}
}
inline void shuchu(int x,int y){
	if(x>1)shuchu(x-1,f[x][y]);
	for(int i=1;i<=n;i++)if(y&(1<<(i-1)))write(i),putchar(' ');
	puts("");
}
int main()
{
	freopen("hungry.in","r",stdin);
	freopen("hungry.out","w",stdout);
	n=read();m=read();K=read();L=read();
	for(int i=1;i<=n;i++){
		scanf("%s",s[i]+1);
	}
	for(int i=1;i<=m;i++)dfs(i,1,K);
	for(int i=0;i<(1<<n);i++)if(f[m][i]){
		shuchu(m,i);
		break;
	}
	return 0;
}
