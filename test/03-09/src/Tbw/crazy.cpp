#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <vector>
#include <complex>
using namespace std;
typedef complex<double> Point;
const int N = 2005;
int a,b,c,d,e,f,g,h,i,j,k;
Point G[N];
pair<double, double> H[N];
double R;
vector<double> now;
int an;

void Init()
{
	cin >> a >> R;
	for(int ii = 1; ii <= a; ii ++)
	{
		double cc, dd;
		scanf("%lf%lf", &cc, &dd);
		G[ii] = Point(cc, dd);
	}
}
double Z[N];
int Get()
{
	int top = 0;
	for(int ii = 0; ii < now.size(); ii ++)
	{
		if(top == 0 || now[ii] > Z[top]) Z[++top] = now[ii];
		else
		{
			int low = 1, high = top, pos = 0;
			while(low <= high)
			{
				int mid = (low + high) >> 1;
				if(Z[mid] > now[ii]) pos = mid, high = mid - 1;
				else low = mid + 1;
			}
			Z[pos] = now[ii];
		}
	}
	return top;
}
void Work()
{
	for(int ii = 1; ii <= a; ii ++)
	{
		double alpha = asin(R / abs(G[ii]));
		double delta = cos(alpha);
		Point t1 = G[ii] - G[ii] * exp(Point(0.0, alpha)) * delta, t2 = G[ii] - G[ii] * exp(Point(0.0, -alpha)) * delta;
		double cc = atan2(t1.real(), t1.imag()), dd = atan2(t2.real(), t2.imag());
		while(cc < 0.0) cc += 2 * M_PI;
		while(dd < 0.0) dd += 2 * M_PI;
		if(cc > dd) swap(cc, dd);
		H[ii] = make_pair(cc, dd);
	}
	sort(H + 1, H + a + 1);
	for(int ii = 1; ii <= a; ii ++)
	{
		int jj; now.clear();
		for(jj = ii; jj <= a && H[jj].first < H[ii].second; jj ++)
		if(jj == ii || H[jj].second > H[ii].second)
			now.push_back(H[jj].second);
		an = max(an, Get());
	}
	cout << an << endl;
}

int main()
{
	freopen("crazy.in", "r", stdin);
	freopen("crazy.out", "w", stdout);
	Init(), Work();	
	return 0;	
}
