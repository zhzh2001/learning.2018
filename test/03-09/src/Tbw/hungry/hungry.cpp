#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <vector>
using namespace std;
const int N = 805;
int a,b,c,d,e,f,g,h,i,j,k,l;
char w[N][N];
int R[N][N];
bool hash[N], an[N][N];

void Init()
{
	cin >> a >> b >> k >> l;
	for(int ii = 1; ii <= a; ii ++)
		scanf("%s", w[ii] + 1);
}
void Work()
{
	for(int ii = 1; ii <= a; ii ++)
		for(int jj = b; jj >= 1; jj --)
		{
			if(w[ii][jj] == '0') R[ii][jj] = 0;
			else R[ii][jj] = max(jj, R[ii][jj + 1]);
		}
	for(int ii = 1; ii <= b; ii ++)
	{
		if(ii == 1)
		{
			vector<pair<int, int> > tmp;
			for(int jj = 1; jj <= a; jj ++)
				tmp.push_back(make_pair(-R[jj][ii], jj));
			sort(tmp.begin(), tmp.end());
			for(int jj = 0; jj < k; jj ++)
				hash[tmp[jj].second] = true;
		}
		else
		{
			vector<pair<int, int> > t1, t2;
			for(int jj = 1; jj <= a; jj ++)
			{
				if(hash[jj]) t1.push_back(make_pair(R[jj][ii], jj));
				else t2.push_back(make_pair(-R[jj][ii], jj));
			}
			sort(t1.begin(), t1.end()), sort(t2.begin(), t2.end());
			for(int jj = 0; jj < l && jj < t1.size() && jj < t2.size(); jj ++)
			{
				if(R[t1[jj].second][ii] > R[t2[jj].second][ii]) break;
				hash[t1[jj].second] = false, hash[t2[jj].second] = true;
			}
		}
		for(int jj = 1; jj <= a; jj ++)
		if(hash[jj] && w[jj][ii] == '0')
			cout << -1 << endl, exit(0);
		memcpy(an[ii], hash, sizeof(an[ii]));
	}
	for(int ii = 1; ii <= b; ii ++, cout << endl)
		for(int jj = 1; jj <= a; jj ++)
		if(an[ii][jj])
			printf("%d ", jj);
}

int main()
{
	freopen("hungry.in", "r", stdin);
	freopen("hungry.out", "w", stdout);
	Init(), Work();	
	return 0;	
}
