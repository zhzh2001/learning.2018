#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("clever.in","r",stdin);
	freopen("clever.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
int a[32],b[32],cnt[10];
int n,mod;
ll ans=0,ct=0;
void check(){
	ll x=0,y=0;
	// puts("WTF");
	ct++;
	for(int i=1;i<=n;i++){
		x=x*10+a[i];
		cnt[a[i]]++;
	}
	for(int i=n;i;i--)
		y=y*10+a[i];
	ll k=y-x,t=k;
	for(int i=1;i<=n;i++){
		cnt[k%10]--;
		// b[i]=k%10;
		k/=10;
	}
	/*sort(b+1,b+n+1);
	for(int i=1;i<=n;i++)
		if (a[i]!=b[i])
			return;*/
	for(int i=0;i<=9;i++)
		if (cnt[i]){
			memset(cnt,0,sizeof(cnt));
			return;
		}
	/*(for(int i=n;i;i--)
		putchar(b[i]+'0');
	puts("");*/
	t%=mod;
	ans=(ans+t*t)%mod;
}
void dfs(int d){
	if (d>n){
		check();
		return;
	}
	for(int i=a[d-1];i<=9;i++){
		a[d]=i;
		dfs(d+1);
	}
}
int main(){
	init();
	n=readint(),mod=readint();
	dfs(1);
	printf("%lld\n",ans);
	// printf("%lld\n",ct);
}