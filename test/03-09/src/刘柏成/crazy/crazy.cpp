#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 1
void init(){
	freopen("crazy.in","r",stdin);
	freopen("crazy.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
double qwq(int x1,int y1,int x2,int y2){
	if (x1==x2)
		return x1*x1;
	if (y1==y2)
		return y1*y1;
	double k=(double)(y2-y1)/(x2-x1),s=(double)(x1-x2)/(y2-y1),b=y2-k*x2,x=b/(s-k),y=x*s;
	//sx=kx+b
	//(s-k)x=b
	//x=b/(s-k)
	return x*x+y*y;
}
bool can[2002][2002];
int x[2003],y[2002];
int a[2003],now[2003];
int main(){
	init();
	srand(20020814);  //qwq
	int n=readint(),r=readint();
	r=r*r;
	for(int i=1;i<=n;i++)
		x[i]=readint(),y[i]=readint();
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++){
			can[i][j]=qwq(x[i],y[i],x[j],y[j])>r;
			// printf("can[%d][%d]=%d\n",i,j,can[i][j]);
		}
	// puts("WTF");
	int T=30000000;
	for(int i=1;i<=n;i++)
		a[i]=i;
	int ans=0;
	while(T>=0){
		random_shuffle(a+1,a+n+1);
		int cur=0;
		for(int i=1;i<=n;i++){
			int t=a[i];
			bool flag=true;
			for(int j=1;j<=cur;j++)
				if (!can[t][now[j]]){
					flag=false;
					break;
				}
			if (flag)
				now[++cur]=t;
		}
		T-=n*cur;
		ans=max(ans,cur);
	}
	printf("%d",ans);
	// fprintf(stderr,"%d",clock());
}