#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
const int INF=0x3f3f3f3f;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("hungry.in","r",stdin);
	freopen("hungry.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]=' ';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
const int maxn=1000002;
const int maxm=5000002;
struct graph{
	int n,m;
	struct edge{
		int to,cap,next;
	}e[maxm];
	int first[maxn];
	void init(int n){
		this->n=n;
		m=1;
	}
	int addedge(int from,int to,int cap){
		// printf("addedge %d %d %d\n",from,to,cap);
		e[++m]=(edge){to,cap,first[from]};
		first[from]=m;
		e[++m]=(edge){from,0,first[to]};
		first[to]=m;
		return m-1;
	}
	int d[maxn],q[maxn];
	int t;
	bool bfs(int s){
		int l=1,r=1;
		q[1]=s;
		memset(d,0x3f,(n+1)*4);
		d[s]=0;
		while(l<=r){
			int u=q[l++];
			for(int i=first[u];i;i=e[i].next){
				int v=e[i].to;
				if (d[v]!=INF || !e[i].cap)
					continue;
				d[v]=d[u]+1;
				q[++r]=v;
				if (v==t)
					return true;
			}
		}
		return false;
	}
	int cur[maxn];
	int dfs(int u,int f){
		if (u==t)
			return f;
		for(int& i=cur[u];i;i=e[i].next){
			int v=e[i].to;
			if (d[v]!=d[u]+1 || !e[i].cap)
				continue;
			int ans=dfs(v,min(f,e[i].cap));
			if (ans){
				e[i].cap-=ans;
				e[i^1].cap+=ans;
				return ans;
			}
		}
		return 0;
	}
	int dinic(int s,int t){
		this->t=t;
		int ans=0;
		while(bfs(s)){
			int flow;
			memcpy(cur,first,(n+1)*4);
			while(flow=dfs(s,INF))
				ans+=flow;
		}
		return ans;
	}
}g;
bool have[808][808];
bool use[808][808];
char b[808];
int o1[808][808],o2[808][808];
int n,m;
int idx(int x,int y){
	return (x-1)*n+y;
}
int main(){
	init();
	n=readint(),m=readint();
	int k=readint(),l=readint(),s=n*m+2*m+2,t=s+1;
	g.init(t);
	g.addedge(s,s-1,k);
	for(int i=1;i<=n;i++){
		readstr(b+1);
		for(int j=1;j<=m;j++)
			have[j][i]=b[j]=='1';
	}
	for(int i=1;i<=n;i++)
		if (have[1][i])
			g.addedge(s-1,i,1);
	for(int i=1;i<m;i++){
		g.addedge(n*m+i*2-1,n*m+i*2,l);
		for(int j=1;j<=n;j++){
			if (have[i][j] && have[i+1][j])
				o1[i][j]=g.addedge(idx(i,j),idx(i+1,j),1);
			if (have[i][j])
				o2[i][j]=g.addedge(idx(i,j),n*m+i*2-1,1);
			if (have[i+1][j])
				g.addedge(n*m+i*2,idx(i+1,j),1);
		}
	}
	for(int i=1;i<=n;i++)
		if (have[m][i])
			o1[m][i]=g.addedge(idx(m,i),t,1);
	if (g.dinic(s,t)!=k)
		return puts("-1"),0;
	for(int i=1;i<=m;i++){
		for(int j=1;j<=n;j++)
			if ((o1[i][j] && !g.e[o1[i][j]].cap) || (o2[i][j] && !g.e[o2[i][j]].cap))
				printf("%d ",j);
		puts("");
	}
}