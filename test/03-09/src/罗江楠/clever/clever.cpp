#include <bits/stdc++.h>
#define N 35
#define ll long long
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}
int n, p;
int now[N], rev[N];
void calc(ll x) {
  for (int i = 1; i <= n; i++)
    rev[i] = x % 10, x /= 10;
  sort(rev + 1, rev + n + 1);
}
ll ans;
void dfs(int x, int d) {
  now[x] = d;
  if (x == n) {
    ll a = 0, b = 0;
    for (int i = 1; i <= n; i++)
      a = a * 10 + now[i];
    for (int i = n; i; i--)
      b = b * 10 + now[i];
    calc(a - b);
    bool flag = true;
    for (int i = 1; i <= n && flag; i++)
      if (now[i] != rev[n - i + 1])
        flag = false;
    if (flag) {
      ans = (ans + (a - b) * (a - b)) % p;
    }
  } else {
    for (int i = d; ~i; i--)
      dfs(x + 1, i);
  }
}
int main(int argc, char const *argv[]) {
  freopen("clever.in", "r", stdin);
  freopen("clever.out", "w", stdout);
  n = read(); p = read();
  dfs(0, 9);
  cout << ans << endl;
  return 0;
}