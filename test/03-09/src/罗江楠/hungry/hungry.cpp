#include <bits/stdc++.h>
#define N 820
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}
char str[N];
int fst[N][N]; // fst[i][j] 表示第 i 只兔子在第 j 天能够连任的时间
struct node {
  int x, y;
  friend bool operator < (const node &a, const node &b) {
    return a.x == b.x ? a.y < b.y : a.x > b.x;
  }
}q[N];
int sum[N], mp[N][N], chs[N], ans[N][N];
int main(int argc, char const *argv[]) {
  freopen("hungry.in", "r", stdin);
  freopen("hungry.out", "w", stdout);

  // 贪心选最远的
  int n = read(), m = read(), k = read(), l = read();
  for (int i = 1; i <= n; i++) {
    scanf("%s", str + 1);
    for (int j = m; j; j--) {
      mp[i][j] = str[j] == '1';
      fst[i][j] = mp[i][j]
        ? fst[i][j + 1] + 1 : 0;
      sum[j] += mp[i][j];
    }
  }
  // 判 -1

  for (int i = 1; i <= m; i++)
    if (sum[i] < k) return puts("-1"), 0;

  // 先选第一天的
  for (int i = 1; i <= n; i++)
    q[i] = (node){fst[i][1], i};
  sort(q + 1, q + n + 1);
  for (int i = 1; i <= k; i++) {
    ans[1][i] = q[i].y;
    chs[q[i].y] = 1;
  }

  // debug  for (int j = 1; j <= n; j++)
  // debug    printf("%d ", q[j].y);
  for (int i = 2; i <= m; i++) { // 到了第 i 天
    // debug  for (int j = 1; j <= n; j++)
    // debug    printf("[%d]", chs[j]);
    // debug  puts("");
    int top = 0;
    for (int j = 1; j <= n; j++)
      q[j] = (node){fst[j][i], j};
    sort(q + 1, q + n + 1);
    int sm = 0, pos;
    for (pos = 1; pos <= n; pos++) {
      sm += chs[q[pos].y];
      if (sm == k - l) break;
    }
    // debug  for (int j = 1; j <= n; j++)
    // debug    printf("%d ", q[j].y);
    // debug  printf("\n%d\n", pos);
    if (pos - k > l) // 如果必须换的兔子数量大于能换的兔子数量
      return puts("-1"), 0;
    if (!q[pos].x) // 如果不得不选一个找死的兔子
      return puts("-1"), 0;

    if (pos > k) { // 前 k 个要有筛选
      int tmp = 0;
      for (int j = pos; j; j--) {
        // 要过滤掉 pos - k 个
        if (!chs[q[j].y] && ++ tmp > pos - k)
          chs[q[j].y] = 1;
      }
      for (int j = pos + 1; j <= n; j++)
        chs[q[j].y] = 0;
    } else { // 直接选前 k 个
      for (int j = 1; j <= n; j++)
        chs[q[j].y] = j <= k;
    }
    int rko = 0;
    for (int j = 1; j <= n; j++)
      if (chs[q[j].y]) ans[i][++ rko] = q[j].y;
    // debug  for (int j = 1; j <= n; j++)
    // debug    printf("[%d]", chs[j]);
    // debug  puts("");
  }
  for (int i = 1; i <= m; i++) {
    for (int j = 1; j <= k; j++)
      printf("%d ", ans[i][j]);
    puts("");
  }

  return 0;
}
// a  mawatte mawatte mawari tsukarete
// あ 回って  回って  回り   疲れて
// a  ikiga   ikiga   ikiga  tomaruno
// あ 生きが  生きが  生きが 止まるの
