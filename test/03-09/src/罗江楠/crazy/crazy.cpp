#include <bits/stdc++.h>
#define N 2020
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}
struct point {
  double x, y;
}ptn[N];
int n, m;
// mul :: Point -> Point -> Number
double mul(int a, int b) {
  return ptn[a].x * ptn[b].y - ptn[a].y * ptn[b].x;
}
// dis :: Number -> Number -> Number
double dis(int a, int b) {
  return (ptn[a].x - ptn[b].x) * (ptn[a].x - ptn[b].x) + (ptn[a].y - ptn[b].y) * (ptn[a].y - ptn[b].y);
}
// across :: Point -> Point -> Boolean
bool across(int a, int b) {
  double s = abs(mul(a, b)); // s = 2S
  double d = s / sqrt(dis(a, b)); // dis = D^2
  if (d > m) return false; // 如果最短距离比半径大，直接返回 false
  if (ptn[a].x == ptn[b].x) { // if ptn[a].x == ptn[b].x
    return (ptn[a].y > 0) ^ (ptn[b].y > 0);
  }
  double k1 = (ptn[a].y - ptn[b].y) / (ptn[a].x - ptn[b].x);
  double b1 = ptn[a].y - ptn[a].x * k1;
  double k2 = 1 / k1;
  double x = b1 / (k2 - k1);
  double y = k2 * x;
  return (ptn[a].x > x) ^ (ptn[b].x > x);
}
bool mp[N][N];
int c[N], st[N];
double clk = 1.5;
int main(int argc, char const *argv[]) {
  freopen("crazy.in", "r", stdin);
  freopen("crazy.out", "w", stdout);
  n = read(); m = read();
  for (int i = 1; i <= n; i++)
    ptn[i].x = read(), ptn[i].y = read();
  for (int i = 1; i <= n; i++) {
    for (int j = i + 1; j <= n; j++) {
      if (!across(i, j)) {
        mp[i][j] = mp[j][i] = true;
      }
    }
  }
  for (int i = 1; i <= n; i++)
    c[i] = i;
  int cnt = 0;
  int ans = 0;
  while (++ cnt) {
    random_shuffle(c + 1, c + n + 1);
    int top = 0;
    st[++ top] = c[1];
    for (int i = 2; i <= n; i++) {
      bool flag = true;
      for (int j = 1; j <= top && flag; j++)
        if (!mp[c[i]][st[j]]) flag = false;
      if (flag)
        st[++ top] = c[i];
    }
    ans = max(ans, top);

    // Linux 还是 Windows WTF...
    if (cnt % 100 == 0) {
      double now = clock();
      if (now > 1.5)
        break;
    }
  }
  printf("%d\n", ans);

  return 0;
}
// woc 这样例有毒
