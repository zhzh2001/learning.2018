#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=100100;
inline int read(){
	int x=0,c=getchar(),f=0;
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
int n,h[11],hh[11],cnt;
struct cqz{int a[110],ln;}ans,g,l,now,res;
void jin(cqz&x){
	int i;
	for(i=0;i<x.ln;i++)
	x.a[i+1]+=x.a[i]/10,x.a[i]%=10;
	while(x.a[x.ln]>9)
	x.a[x.ln+1]=x.a[x.ln]/10,x.a[x.ln]%=10,x.ln++;
	while(x.ln&&!x.a[x.ln])x.ln--;
}
void mul(cqz&x,cqz&y,cqz&z){
	int i,j;
	for(i=0;i<=z.ln;i++)z.a[i]=0;
	z.ln=x.ln+y.ln;
	for(i=0;i<=x.ln;i++)
	for(j=0;j<=y.ln;j++)
	z.a[i+j]+=x.a[i]*y.a[j];
	jin(z);
}
void solve(){
	int i;
	for(i=0;i<10;i++)hh[i]=0;
	for(i=0;i<n;i++)l.a[i]=g.a[n-i-1];
	for(i=0;i<n;i++)
		now.a[i]=g.a[i]-l.a[i];
	for(i=0;i<n;i++){
		if(now.a[i]<0)now.a[i+1]--,now.a[i]+=10;
		hh[now.a[i]]++;
	}
	for(i=0;i<10;i++)if(h[i]!=hh[i])return;
	mul(now,now,res);
	cnt++;
	if(res.ln>ans.ln)ans.ln=res.ln;
	for(i=0;i<=res.ln;i++)ans.a[i]+=res.a[i];
	if(cnt==10000000){
		cnt=0;jin(ans);
	}
}
void dfs(int x,int m){
	if(x==0){
		for(int i=0;i<m;i++)
		g.a[i]=0;
		h[0]=m;
		solve();return;
	}
	for(int i=0;i<=m;i++){
		h[x]=i;
		if(i)g.a[m-i]=x;
		dfs(x-1,m-i);
	}
}
void out(cqz&x){
	jin(x);
	putchar('{');
	for(int i=x.ln;i>=0;i--)
	printf("'%d',",x.a[i]);
	putchar('}');putchar(',');
	puts("");printf("%d\n",x.ln);
}
int main(){
	freopen("clever.in","r",stdin);
	freopen("clever.out","w",stdout);
	n=read();
	g.ln=l.ln=now.ln=n-1;
	ans.ln=0;
	dfs(9,n);
	out(ans);
	return 0;
}
