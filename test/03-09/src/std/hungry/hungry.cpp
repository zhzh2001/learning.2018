#include <cstdio>
#include <cstring>
#include <vector>
#include <algorithm>

using namespace std;

const int MAXN = 2100000, inf = ~0u>>1;

struct Edge {
	int y; Edge *next, *opt; int f;
}*a[MAXN], POOL[7000000], *e[MAXN], *data = POOL;

inline void AddEdge(int x, int y, int flow) {
	Edge *tmp = data++;
	tmp->y = y; tmp->next = a[x]; tmp->f = flow; a[x] = tmp;
	tmp = data++;
	tmp->y = x; tmp->next = a[y]; tmp->f = 0; a[y] = tmp;
	a[x]->opt = a[y];
	a[y]->opt = a[x];
//	printf("Add %d %d %d\n", x, y, flow);
}

int n, k, m, vs, vt, vs_; char ch[1110][1100];

int level[MAXN], d[MAXN];
inline int Bfs(void) {
	memset(level, -1, sizeof level);
	d[1] = vs; level[vs] = 0;
	int head = 1, tail = 1;
	while (head <= tail) {
		int now = d[head++];
		e[now] = a[now];
		for (Edge *p = a[now]; p; p = p->next) if (p->f > 0 && level[p->y] == -1) 
			level[d[++tail] = p->y] = level[now] + 1;
	}
	return level[vt] != -1;
}

inline int Extend(int u, int sum) {
	if (u == vt) return sum;
	int r = 0, t;
	for (Edge *p = e[u]; p && r < sum; p = p->next) if (level[p->y] == level[u] + 1 && p->f > 0) {
		t = min(sum - r, p->f);
		t = Extend(p->y, t);
		p->f -= t, p->opt->f += t, r += t;
		e[u] = p;
	}
	if (!r) level[u] = -1;
	return r;
}

inline int Dinic(void) {
	int r = 0, t;
	while (Bfs()) {
		while (t = Extend(vs, inf)) r += t;
	}
	return r;
}

inline int getId(int i, int j) {return (i - 1) * m + j;}
inline int In(int i, int j) {return getId(i, j) * 2 - 1;}
inline int Out(int i, int j) {return getId(i, j) * 2;}
inline int Midin(int j) {return Out(n, m) + j * 2 - 1;}
inline int Midout(int j) {return Out(n, m) + j * 2;}
inline int getRow(int k) {return (((k + 1) / 2) - 1) / m + 1;}

int main(void) {
	freopen("hungry.in", "r", stdin);
	freopen("hungry.out", "w", stdout);
	int limit; scanf("%d%d%d%d", &n, &m, &k, &limit);
	for (int i = 1; i <= n; i++)
		scanf("%s", ch[i] + 1);
	vs = Midout(m - 1) + 1, vs_ = vs + 1, vt = vs_ + 1;
//	printf("vs %d, vs_  %d, vt %d\n", vs, vs_, vt);

	AddEdge(vs, vs_, k);
	for (int i = 1; i <= n; i++) AddEdge(vs_, In(i, 1), 1);
	for (int i = 1; i <= n; i++) AddEdge(Out(i, m), vt, 1);
	for (int j = 1; j < m; j++) {
		for (int i = 1; i <= n; i++) if (ch[i][j] == '1') {
			AddEdge(Out(i, j), Midin(j), 1);
		}
		for (int i = 1; i <= n; i++) if (ch[i][j + 1] == '1') {
			AddEdge(Midout(j), In(i, j + 1), 1);
		}
		for (int i = 1; i <= n; i++) if (ch[i][j] == '1' && ch[i][j + 1] == '1') {
			AddEdge(Out(i, j), In(i, j + 1), 1);
		}
		AddEdge(Midin(j), Midout(j), limit);
	}
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= m; j++) if (ch[i][j] == '1') AddEdge(In(i, j), Out(i, j), 1);

	int ret = Dinic();
	if (ret != k) puts("-1"); else {
		for (int i = 1; i <= m; i++) {
			for (int j = 1; j <= n; j++) {
				for (Edge *p = a[In(j, i)]; p; p = p->next) if (p->y == Out(j, i)) {
					if (p->f == 0) printf("%d ", j);
					break;
				}
			}
			puts("");
		}
	}
	return 0;
}

