#include <cstdio>
#include <cstring>
#include <vector>
#include <algorithm>

using namespace std;

const int MAXN = 100;

inline void go(int a[]) {
	int L = 1;
	while (L <= a[0] || a[L]) a[L + 1] += a[L] / 10, a[L] %= 10, L++;
	a[0] = L - 1;
	while (a[0] > 1 && a[a[0]] == 0) --a[0];
}
inline void mul(int a[], int x, int b[]) {
	for (int i = 1; i <= b[0]; i++) a[i] = b[i] * x;
	a[0] = b[0]; go(a);
}
inline void add(int a[], int b[]) {
	a[0] = max(a[0], b[0]);
	for (int i = 1; i <= a[0]; i++) a[i] += b[i], b[i] = 0;
	go(a);
}
inline void sub(int a[], int b[]) {
	int len = max(a[0], b[0]);
	for (int i = 1; i <= len; i++) a[i] -= b[i];
	for (int i = 1; i <= len; i++) if (a[i] < 0) a[i] += 10, a[i + 1]--;
	while (a[len] == 0 && len > 1) len--; a[0] = len;
}

int n, p;

int pow[MAXN][MAXN];int stack[MAXN], tmp[MAXN], pow_p[MAXN], Ans = 0;

int cnt = 0;

void Dfs(int now, int last) {
	if (now > n / 2) {
		int res[121] = {0}, res_a[121] = {0}, res_b[121] = {0}, A = 0;
		for (int i = 1; i <= n / 2; i++) {
			int t = (long long)stack[i] * pow_p[n - i] % p;
			A += t;
			if (A >= p) A -= p;
			mul(tmp, stack[i],  pow[n - i]);
			add(res, tmp);
		}
		memcpy(res_a, res, sizeof res);
		sort(res_a + 1, res_a + res_a[0] + 1);
		memcpy(res_b, res_a, sizeof res_a);
		reverse(res_b + 1, res_b + res_b[0] + 1);
		sub(res_a, res_b);
		int ok = (res_a[0] == res[0]);
		for (int i = 1; i <= res_a[0]; i++) if (res_a[i] != res[i]) {
			ok = 0;
			break;
		}
		if (ok) {
			Ans += (long long)A * A % p;
			if (Ans >= p) Ans -= p;
		}
		return;
	}
	for (;last >= 0; stack[now] = last, Dfs(now + 1, last--));
}

int main(void) {
	freopen("clever.in", "r", stdin);
	freopen("clever.out", "w", stdout);
	scanf("%d%d", &n, &p);
	pow_p[0] = 1; pow[0][0] = pow[0][1] = 1; for (int i = 1; i <= n; i++) mul(pow[i], 10, pow[i - 1]), pow_p[i] = (long long)pow_p[i - 1] * 10 % p;
	for (int i = 1; i <= n / 2; i++) {
		sub(pow[n - i], pow[i - 1]), pow_p[n - i] = (pow_p[n - i] - pow_p[i - 1]) % p;
		if (pow_p[n - i] < 0) pow_p[n - i] += p;
	}
	Dfs(1, 9);
	printf("%d\n", Ans);
	return 0;
}

