#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include <cstring>
#include <string>
#include <algorithm>
#include <iomanip>
#include <iostream>
#define ll long long
#define int ll
using namespace std;
inline long long read() {
	long long x = 0; int f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48; ch = getchar(); }
	return x * f;
}
inline int write(int x){
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x); puts("");
}
int n,p,q[5],tot,ans,lim=1,s1,s2;
signed main(){
	freopen("clever.in","r",stdin); freopen("clever.out","w",stdout);
	cin>>n>>p;
	if(n==8){
		q[++tot]=63317554; q[++tot]=97508421;
	}else if(n==9){
		q[++tot]=554999445; q[++tot]=864197532;
	}else if(n==10){
		q[++tot]=6333176664; q[++tot]=9753086421; q[++tot]=9975084201;
	}
	if(n>=8){
		for(int i=1;i<=tot;i++)q[i]%=p;
		for(int i=1;i<=tot;i++)ans=(ans+q[i]*q[i])%p;
		cout<<ans<<endl; return 0;
	}
	for(int i=1;i<=n;i++)lim*=10;
	for(int i=lim/10;i<lim;i++){
		int t=i;
		for(int j=1;j<=n;j++){
			q[j]=t%10; t/=10;
		}
		sort(&q[1],&q[n+1]);
		s1=s2=0;
		for(int j=1;j<=n;j++)s1=s1*10+q[j];
		for(int j=n;j;j--)s2=s2*10+q[j];
		//if(i==6714)cout<<s2<<" "<<s1<<" "<<i<<endl;
		if(s2-s1==i)ans+=i*i;
	}
	cout<<ans%p<<endl;
}


