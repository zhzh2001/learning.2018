#include<cstdio>
#include<cstring>
#include<cmath>
#include<iostream>
#include<queue>
using namespace std;

const int N=1300005,M=10000005,oo=1e9;
struct edge{
	int di,nextt,ed;
}e[M];
int son[N],nedge,cur[N],deep[N],n,m;
inline void aedge(int a,int b,int c){
	e[++nedge].nextt=son[a];
	son[a]=nedge;
	e[nedge].ed=b;
	e[nedge].di=c;
}
inline void insert(int a,int b,int c){
	aedge(a,b,c); aedge(b,a,0);
}
inline bool bfs(int s,int t){
	for(int i=1;i<=n;i++)deep[i]=oo;
    for(int i=1;i<=n;i++)cur[i]=son[i];
    deep[s]=0;
    queue<int> q;
    q.push(s);
    while(!q.empty()){
        int now=q.front();q.pop();
        for(int tmp=son[now];tmp;tmp=e[tmp].nextt)
            if(deep[e[tmp].ed]>=oo&&e[tmp].di){deep[e[tmp].ed]=deep[now]+1,q.push(e[tmp].ed);}
    }
    return deep[t]<oo;
}
int dfs(int now,int t,int limit){
    if(!limit||now==t) return limit;
    int flow=0,f;
    for(int tmp=cur[now];tmp;tmp=e[tmp].nextt){
        cur[now]=tmp;
        if(deep[e[tmp].ed]==deep[now]+1&&(f=dfs(e[tmp].ed,t,min(limit,e[tmp].di)))){
            flow+=f; limit-=f; e[tmp].di-=f; e[((tmp-1)^1)+1].di+=f; if(!limit)break;
        }
    }
    return flow;
}
inline int dinic(int s,int t){
    int ans=0;
    while(bfs(s,t))ans+=dfs(s,t,oo);
    return ans;
}
inline int read(){
	int x=0;char ch=getchar();bool positive=1;
	for(;!isdigit(ch);ch=getchar())	if(ch == '-')	positive=0;
	for(;isdigit(ch);ch=getchar())	x=x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(int a){
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(int a){
    if(a<0){
    	a=-a;putchar('-');
	}
	write(a);puts("");
}
char ch[805][805];
int k,l,tot,num[805];
#define zb(i,j,k) ((i)-1)*m+j+k*nn*m
int main(){
	freopen("hungry.in","r",stdin); freopen("hungry.out","w",stdout);
    int nn=n=read(); m=read(); k=read(); l=read();
    int s=2*n*m+(m-1)*2+1,ss=s+1,t=ss+1; tot=2*n*m+1;
	for(int i=1;i<=n;i++)scanf("%s",ch[i]+1);
    for(int i=1;i<=n;i++)for(int j=1;j<=m;j++){ch[i][j]-='0'; if(ch[i][j])insert(zb(i,j,0),zb(i,j,1),1);}
    for(int i=1;i<=n;i++){
    	if(ch[i][1])insert(ss,zb(i,1,0),1);
    	if(ch[i][m])insert(zb(i,m,1),t,1);
	}
	for(int i=2;i<=m;i++){
    	for(int j=1;j<=n;j++){
    		if(ch[j][i]&&ch[j][i-1]){
    			insert(zb(j,i-1,1),zb(j,i,0),1);
			}
			if(ch[j][i-1])insert(zb(j,i-1,1),tot,1);
			if(ch[j][i])insert(tot+1,zb(j,i,0),1);
		}
		insert(tot,tot+1,l);
		tot+=2;
	}
	insert(s,ss,k);
	n=t;
	if(dinic(s,t)<k)puts("-1");
	else{
		//for(int k=son[zb(3,3,0)];k;k=e[k].nextt)if(e[k].di==0)cout<<e[k].ed<<" "<<zb(3,3,1)<<endl;
		for(int i=1;i<=m;i++){for(int j=1;j<=nn;j++){
			int flag=0;
			for(int k=son[zb(j,i,0)];k;k=e[k].nextt)if(e[k].di==0&&e[k].ed==zb(j,i,1)){
				flag=1; break;
			}
			if(flag){
				write(j); putchar(' ');
			}
		}puts("");}
	}
}
