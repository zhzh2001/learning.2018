#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include <cstring>
#include <string>
#include <algorithm>
#include <iomanip>
#include <iostream>
#define ll long long
using namespace std;
inline long long read() {
	long long x = 0; int f = 1;
	char ch = getchar();
	while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
	while(ch>='0'&&ch<='9') { x = x * 10+ch-48; ch = getchar(); }
	return x * f;
}
inline void write(int x){
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x); puts("");
}
int n,r,que[2005],ans;
struct point{
	double x,y;
}a[2005];
inline double dist(point x,point y){return sqrt((x.x-y.x)*(x.x-y.x)+(x.y-y.y)*(x.y-y.y));}
inline double cross(double x1,double y1,double x2,double y2){return x1*y2-x2*y1;}
int e[2005][2005],q[2005];
int main(){
	freopen("crazy.in","r",stdin); freopen("crazy.out","w",stdout);
	srand(19260817);
	n=read(); r=read();
	for(int i=1;i<=n;i++){
		a[i].x=read(); a[i].y=read(); q[i]=i;
	}
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++){
			double t1=dist(a[i],a[j]),t2=fabs(cross(a[i].x,a[i].y,a[j].x,a[j].y));
			e[i][j]=(t2/t1)>r;
			//if(i==3&&j==5)cout<<t2<<" "<<t1<<endl;
		}
	}
	int sum=0;
	for(int i=1;sum<=100000000;i++){
		int tot=0;
		random_shuffle(&q[1],&q[n+1]); 
		for(int i=1;i<=n;i++){
			int flag=0; 
			for(int j=1;j<=tot;j++)if(!e[q[i]][que[j]]){
				flag=1; sum+=j+3; break;
			}
			if(!flag)sum+=tot;
			if(!flag)que[++tot]=q[i];
		}
		ans=max(ans,tot);
	}
	cout<<ans<<endl;
}


