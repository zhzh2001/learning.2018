#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

struct bignum
{
	int g[100];
	
	inline void clear()
	{
		memset(g,0,sizeof(g));
		return;
	}
	
	inline void out()
	{
		for (int i=g[0];i>0;i--)
			putchar(g[i]+48);
		puts("");
		return;
	}
	
	inline bignum operator + (const bignum t) const
	{
		bignum z;
		z.clear();
		z.g[0]=max(g[0],t.g[0]);
		for (int i=1;i<=z.g[0];i++)
			z.g[i]=g[i]+t.g[i];
		for (int i=2;i<=z.g[0]+1;i++)
			if (z.g[i-1]>=10)
				z.g[i-1]=z.g[i-1]-10,z.g[i]=z.g[i]+1;
		while (z.g[z.g[0]+1])
			z.g[0]++;
		return z;
	}
	
	inline bignum operator - (const bignum t) const
	{
		bignum z;
		z.clear();
		z.g[0]=g[0];
		for (int i=1;i<=z.g[0];i++)
			z.g[i]=g[i]-t.g[i];
		for (int i=2;i<=z.g[0]+1;i++)
			if (z.g[i-1]<0)
				z.g[i-1]=z.g[i-1]+10,z.g[i]=z.g[i]-1;
		while ((z.g[0]>1) && (! z.g[z.g[0]]))
			z.g[0]--;
		return z;
	}
	
	inline bignum operator * (const bignum t) const
	{
		bignum z;
		z.clear();
		z.g[0]=g[0]+t.g[0]-1;
		for (int i=1;i<=g[0];i++)
			for (int j=1;j<=t.g[0];j++)
				z.g[i+j-1]=z.g[i+j-1]+g[i]*t.g[j];
		for (int i=2;i<=z.g[0];i++)
			if (z.g[i-1]>=10)
				z.g[i]=z.g[i]+z.g[i-1]/10,z.g[i-1]=z.g[i-1]%10;
		while (z.g[z.g[0]]>=10)
			z.g[0]++,z.g[z.g[0]]=z.g[z.g[0]-1]/10,z.g[z.g[0]-1]=z.g[z.g[0]-1]%10;
		return z;
	}
};

bignum c,d,e,f;

int n;

inline void dfs(int x,int y)
{
	if (x==10)
	{
		if (y<=n)
			return;
		for (int i=1;i<=n;i++)
			d.g[n-i+1]=c.g[i];
		c.g[0]=d.g[0]=n;
		e=c-d;
		int h[10];
		memset(h,0,sizeof(h));
		for (int i=1;i<=n;i++)
			h[c.g[i]]++,h[e.g[i]]--;
		for (int i=0;i<10;i++)
			if (h[i])
				return;
		f=f+e*e;
		return;
	}
	dfs(x+1,y);
	for (int i=y;i<=n;i++)
		c.g[i]=x,dfs(x+1,i+1);
	return;
}

int main()
{
	freopen("clever20.out","w",stdout);
//	scanf("%d",&n);
	for (n=1;n<=20;n++)
	{
		f.clear();
		dfs(0,1);
		f.out();
	}
	return 0;
}
