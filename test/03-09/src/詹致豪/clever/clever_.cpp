#include <cstdio>
#include <algorithm>

using namespace std;

int c[40],d[40];
int m,n,s;

inline void dfs(int x)
{
	if (x==n)
	{
		for (int i=1;i<=n;i++)
			d[i]=c[i];
		sort(d+1,d+n+1);
		long long t1=0,t2=0,t3=0;
		for (int i=1;i<=n;i++)
			t1=t1*10LL+d[i];
		for (int i=n;i>0;i--)
			t2=t2*10LL+d[i];
		for (int i=1;i<=n;i++)
			t3=t3*10LL+c[i];
		if (t3==t2-t1)
			s=(s+(t3%m)*(t3%m)%m)%m;
	}
	else
		for (int i=0;i<10;i++)
			c[x+1]=i,dfs(x+1);
}

int main()
{
	freopen("clever.in","r",stdin);
	freopen("clever_.out","w",stdout);
	scanf("%d%d",&n,&m);
	dfs(0);
	printf("%d",s);
	return 0;
}
