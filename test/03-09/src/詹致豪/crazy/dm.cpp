#include <cstdio>
#include <ctime>
#include <algorithm>

using namespace std;

int main()
{
	freopen("crazy.in","w",stdout);
	srand((int) time(0));
	int n=2000,m=rand()%5000+1;
	printf("%d %d\n",n,m);
	for (int i=1;i<=n;i++)
	{
		int x=rand()%10001-5000,y=rand()%10001-5000;
		while (x*x+y*y<=m*m)
			x=rand()%10001-5000,y=rand()%10001-5000;
		printf("%d %d\n",x,y);
	}
	return 0;
}
