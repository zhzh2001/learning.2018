#include <cstdio>
#include <cstring>
#include <cmath>
#include <ctime>
#include <algorithm>

using namespace std;

const double e=1e-12;
const double p=acos(-1);

int d[2100][2100],g[2100],h[2100],x[2100],y[2100];
bool b[2100];
int i,j,k1,k2,m,n,o,q,r,s,t,u,v,w;
double k3;

inline bool check(int u,int v)
{
	double d=abs(x[u]*y[v]-x[v]*y[u])/sqrt((x[u]-x[v])*(x[u]-x[v])+(y[u]-y[v])*(y[u]-y[v]));
	int tx=x[u]-x[v],ty=y[u]-y[v]; swap(tx,ty); tx=-tx; double r=sqrt(tx*tx+ty*ty); tx=tx/r*d; ty=ty/r*d;
	if ((tx>=min(x[u],x[v])-e) && (tx<=max(x[u],x[v])+e) && (ty>=min(y[u],y[v])-e) && (ty<=max(y[u],y[v])+e)) return d<=m;
	tx=-tx,ty=-ty; if ((tx>=min(x[u],x[v])-e) && (tx<=max(x[u],x[v])+e) && (ty>=min(y[u],y[v])-e) && (ty<=max(y[u],y[v])+e)) return d<=m;
	return (x[u]*x[u]+y[u]*y[u]<=m*m) || (x[v]*x[v]+y[v]*y[v]<=m*m);
}

inline int getnext(int x)
{
	if (! b[x])
		return x;
	else
		return h[x]=getnext(h[x]);
}

inline int calc()
{
	int t=0;
	memset(b,false,sizeof(b));
	for (int i=1;i<=n;i++)
		h[i]=i+1;
	for (int i=1;i<=n;i++)
	{
		if (! b[g[i]])
			b[g[i]]=true,t++;
		for (int j=getnext(1);j<=n;j=getnext(j+1))
			if (d[g[i]][j])
				b[j]=true;
	}
	return t;
}

inline double random(int z)
{
	double t=atan2(x[z],y[z])+k3;
	if (t<-p) t=t+p; if (t>p) t=t-p;
	return t;
}

inline bool cmp(int u,int v)
{
	return random(u)<random(v);
}

int main()
{
	freopen("crazy.in","r",stdin);
	freopen("crazy.out","w",stdout);
	srand((int) time(0));
	scanf("%d%d",&n,&m);
	for (i=1;i<=n;i++)
		scanf("%d%d",&x[i],&y[i]);
	for (i=1;i<=n;i++)
		for (j=1;j<=n;j++)
			if ((i!=j) && (check(i,j)))
				d[i][j]=1,t++;
	for (i=1;i<=n;i++)
		g[i]=i;
	sort(g+1,g+n+1,cmp);
	q=calc();
	for (i=1;i<=min(20000,300000000/t);i++)
	{
		k1=(rand()<<15|rand())%200000001-100000000;
		k2=(rand()<<15|rand())%200000001-100000000;
		k3=atan2(k1,k2);
		sort(g+1,g+n+1,cmp);
		for (j=1;j<=rand()%5;j++)
			u=rand()%n+1,v=rand()%n+1,swap(g[u],g[v]);
		s=max(s,calc());
	}
	printf("%d\n",s);
	return 0;
}
