#include <cstdio>

int f[12][1024],g[12],h[1024];
char c[12][12];
int i,j,k,l,m,n,o;

inline void output(int x,int y)
{
	if (f[x][y]!=-1)
		output(x-1,f[x][y]);
	for (int i=1;i<=n;i++)
		if (y&(1<<(i-1)))
			printf("%d ",i);
	puts("");
	return;
}

int main()
{
	freopen("hungry.in","r",stdin);
	freopen("hungry_.out","w",stdout);
	scanf("%d%d%d%d",&n,&m,&k,&l);
	for (i=1;i<=n;i++)
		scanf("%s",c[i]+1);
	for (i=1;i<=n;i++)
		for (j=1;j<=m;j++)
			if (c[i][j]-48)
				g[j]=g[j]|(1<<(i-1));
	for (i=0;i<(1<<n);i++)
		for (j=1;j<=n;j++)
			if (i&(1<<(j-1)))
				h[i]++;
	for (i=0;i<(1<<n);i++)
		if ((h[i]==k) && ((i&g[1])==i))
			f[1][i]=-1;
	for (i=2;i<=m;i++)
		for (j=0;j<(1<<n);j++)
			if (f[i-1][j])
				for (o=0;o<(1<<n);o++)
					if ((h[o]==k) && (h[o^j]<=2*l) && ((o&g[i])==o))
						f[i][o]=j;
	for (i=0;i<(1<<n);i++)
		if (f[m][i])
			return puts("YES"),output(m,i),0;
	return puts("NO"),0;
}
