#include <cstdio>

char b[12][12],c[12][12];
int i,j,k,l,m,n,t;

inline bool chk()
{
	freopen("hungry_.out","r",stdin);
	char c=getchar();
	return (c=='Y');
}

inline void getinputdata()
{
	freopen("hungry.in","r",stdin);
	scanf("%d%d%d%d",&n,&m,&k,&l);
	for (i=1;i<=n;i++)
		scanf("%s",c[i]+1);
	for (i=1;i<=n;i++)
		for (j=1;j<=m;j++)
			c[i][j]=c[i][j]-48;
	return;
}

inline void getoutputdata()
{
	freopen("hungry.out","r",stdin);
	for (i=1;i<=m;i++)
		for (j=1;j<=k;j++)
			scanf("%d",&t),b[t][i]=1;
	return;
}

int main()
{
	freopen("hungrychk.out","w",stdout);
	if (! chk())
	{
		freopen("hungry.out","r",stdin);
		scanf("%d",&t);
		if (t==-1)
			return puts("AC"),0;
		else
			return puts("WA 3"),0;
	}
	getinputdata();
	getoutputdata();
	for (i=1;i<=n;i++)
		for (j=1;j<=m;j++)
			if ((b[i][j]) && (! c[i][j]))
				return puts("WA 1"),0;
	for (j=1;j<m;j++)
	{
		t=0;
		for (i=1;i<=n;i++)
			if ((! b[i][j]) && (b[i][j+1]))
				t++;
		if (t>l)
			return puts("WA 2"),0;
	}
	return puts("AC"),0;
}
