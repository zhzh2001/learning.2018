#include<bits/stdc++.h>
#define N 2005
using namespace std;
struct P{
	double x,y;
};
P p[N],O;
double R;
bool calc(P x,P y){
	if (x.x==y.x) return fabs(x.y)>R;
	if (x.y==y.y) return fabs(x.x)>R;
	if (x.x<y.x) swap(x,y);
	double a=(x.y-y.y)/(x.x-y.x);
	double c=x.y-a*x.x;
	double d=c/a;
	return R<sqrt(c*c+d*d);
}
int len;
struct Bit{
	unsigned long long a[33];
	void set(int x){
		a[x>>6]|=1ull<<(x&63);
	}
	bool at(int x){
		return (a[x>>6]>>(x&63))&1;
	}
	void operator |=(const Bit &x){
		for (int i=0;i<=len;i++)
			a[i]|=x.a[i];
	}
	void clear(){
		for (int i=0;i<=len;i++)
			a[i]=0;
	}
};
Bit mp[N],ok;
int a[N],n,m,mx;
int work(){
	int sum=0;
	ok.clear();
	for (int i=1;i<=n&&(n-i+1)+sum>=mx;i++)
		if (!ok.at(a[i])){
			ok|=mp[a[i]];
			sum++;
		}
	return sum;
}
int main(){
	srand(19260817+1); 
	freopen("crazy.in","r",stdin);
	freopen("crazy.out","w",stdout);
	O=(P){0,0};
	int u=clock();
	scanf("%d%lf",&n,&R);
	len=n/64;
	for (int i=1;i<=n;i++)
		scanf("%lf%lf",&p[i].x,&p[i].y);
	for (int i=1;i<=n;i++)
		for (int j=i+1;j<=n;j++)
			if (!calc(p[i],p[j]))
				mp[i].set(j),mp[j].set(i);
	for (int i=1;i<=n;i++) a[i]=i;
	random_shuffle(a+1,a+n+1);
	mx=work();
	while (clock()-u<=1750000){
		int x=rand()%n+1;
		int y=rand()%n+1;
		swap(a[x],a[y]);
		int v=work();
		if (v>=mx) mx=v;
		else swap(a[x],a[y]);
	}
	printf("%d\n",mx);
}
/*
6 3
0 6
-7 -4
-3 -2
7 -5
-2 3
8 -3
*/
