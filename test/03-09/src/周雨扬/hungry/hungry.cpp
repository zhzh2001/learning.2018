#include<bits/stdc++.h>
#define N 805 
using namespace std;
int v[N],a[N][N];
int q[N],ans[N][N];
int n,m,l,S,used[N];
char s[N][N];
bool cmp(int x,int y){
	return v[x]>v[y];
}
int main(){
	freopen("hungry.in","r",stdin);
	freopen("hungry.out","w",stdout);
	scanf("%d%d%d%d",&n,&m,&l,&S);
	for (int i=1;i<=n;i++)
		scanf("%s",s[i]+1);
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++)
			s[i][j]-='0';
	for (int j=m;j;j--)
		for (int i=1;i<=n;i++)
			a[i][j]=(s[i][j]?a[i][j+1]+1:0);
	for (int i=1;i<=m;i++){
		for (int j=1;j<=n;j++)
			v[j]=a[j][i];
		int tot=0;
		for (int j=1;j<=n;j++)
			if (used[j]==i-1&&v[j])
				q[++tot]=j;
		if (tot<l-S){
			puts("-1");
			return 0;
		}
		sort(q+1,q+tot+1,cmp);
		for (int j=1;j<=l-S;j++)
			used[q[j]]=i,ans[i][q[j]]=1;
		tot=0;
		for (int j=1;j<=n;j++)
			if (used[j]!=i&&v[j])
				q[++tot]=j;
		if (tot<S){
			puts("-1");
			return 0;
		}
		sort(q+1,q+tot+1,cmp);
		for (int j=1;j<=S;j++)
			used[q[j]]=i,ans[i][q[j]]=1;
	}
	for (int i=1;i<=m;i++){
		for (int j=1;j<=n;j++)
			if (ans[i][j]) printf("%d ",j);
		puts("");
	}
}
