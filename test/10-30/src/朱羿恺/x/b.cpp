#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 1e5+10;
int n,m,a[N],l1,r1,l2,r2;
namespace Subtask1{
	int sum[N],Sum[N];
	inline bool check(int l,int r){
		if ((r-l+1)&1) return sum[r]-sum[l-1]<=(r-l)/2&&Sum[r]-Sum[l-1]<=(r-l)/2;
			else return sum[r]-sum[l-1]<=(r-l)/2&&Sum[r]-Sum[l-1]<=(r-l+1)/2;
	//	if ((r-l+1)&1) return 2*sum[r]-r<=2*sum[l-1]-l&&2*Sum[r]-r<=2*Sum[l-1]-l;
	//		else return 2*sum[r]-r<=2*sum[l-1]-l&&2*Sum[r]-r-1<=2*sum[l-1]-l;
	}
	inline int Query(int l1,int r1,int l2,int r2){
		For(i,1,n) sum[i]=sum[i-1]+(a[i]<l1);
		For(i,1,n) Sum[i]=Sum[i-1]+(a[i]>r1);
		int ans=0;
		For(i,1,n){
			For(j,max(1,i-r2+1),i-l2+1) if (check(j,i)) ans++;
		//	printf("%d\n",ans);
		}
		return ans;
	}
	inline void Main(){
		while (m--){
			l1=read(),r1=read(),l2=read(),r2=read();
			printf("%d\n",Query(l1,r1,l2,r2));
		} 
	}
}
int main(){
	freopen("x.in","r",stdin);
	freopen("b.out","w",stdout);
	n=read();
	For(i,1,n) a[i]=read();
	m=read();
	if (n<=1000) return Subtask1::Main(),0;
}
/*
5
4 1 2 2 5
5
2 4 2 3
2 4 1 3
2 5 2 5
3 5 2 5
1 5 3 5
*/
