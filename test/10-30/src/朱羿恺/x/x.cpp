#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 1e5+10;
int n,m,a[N],l1[6],r1[6],l2[6],r2[6];
bool flag;
namespace Subtask1{
	int sum[N],Sum[N];
	inline bool check(int l,int r){
		if ((r-l+1)&1) return sum[r]-sum[l-1]<=(r-l)/2&&Sum[r]-Sum[l-1]<=(r-l)/2;
			else return sum[r]-sum[l-1]<=(r-l)/2&&Sum[r]-Sum[l-1]<=(r-l+1)/2;
	//	if ((r-l+1)&1) return 2*sum[r]-r<=2*sum[l-1]-l&&2*Sum[r]-r<=2*Sum[l-1]-l;
	//		else return 2*sum[r]-r<=2*sum[l-1]-l&&2*Sum[r]-r-1<=2*sum[l-1]-l;
	}
	inline int Query(int l1,int r1,int l2,int r2){
		For(i,1,n) sum[i]=sum[i-1]+(a[i]<l1);
		For(i,1,n) Sum[i]=Sum[i-1]+(a[i]>r1);
		int ans=0;
		For(i,1,n)
			For(j,max(1,i-r2+1),i-l2+1) if (check(j,i)) ans++;
		return ans;
	}
	inline void Main(){
		int l1,r1,l2,r2;
		while (m--){
			l1=read(),r1=read(),l2=read(),r2=read();
			printf("%d\n",Query(l1,r1,l2,r2));
		} 
	}
}
/*namespace Subtask2{
	int cnt,Cnt;
	struct BIT_SegmentTree{
		int tot,rt[N],lson[N*200],rson[N*200];
		ll v[N*200];
		inline void insert(int &u,int l,int r,int ql,int x){
		    if (!u) u=++tot;v[u]+=x;
		    if (l==r) return;int mid=l+r>>1;
		    if (ql<=mid) insert(lson[u],l,mid,ql,x);
		        else insert(rson[u],mid+1,r,ql,x);
		}
		inline void Add(int x,int ql,int y){
		    for (;x<=cnt;x+=x&-x) insert(rt[x],1,Cnt,ql,y);
		}
		inline int Query(int u,int l,int r,int ql,int qr){
		    if (!u) return 0;
		    if (l>=ql&&r<=qr) return v[u];int mid=l+r>>1;
		    if (qr<=mid) return Query(lson[u],l,mid,ql,qr);
		    else if (ql>mid) return Query(rson[u],mid+1,r,ql,qr);
		    else return Query(lson[u],l,mid,ql,qr)+Query(rson[u],mid+1,r,ql,qr);
		}
		inline ll query(int l,int r,int ql,int qr){
		    if (l>r||ql>qr) return 0;
		    ll sum=0;
		    for (;r;r-=r&-r) sum+=Query(rt[r],1,Cnt,ql,qr);l--;
		    for (;l;l-=l&-l) sum-=Query(rt[l],1,Cnt,ql,qr);return sum;
		}
	}t[2];
	int sum[N],Sum[N],b[N],B[N],c[N],C[N];
	inline ll Query(int l1,int r1,int l2,int r2){
		For(i,1,n) sum[i]=sum[i-1]+(a[i]<l1);
		For(i,1,n) Sum[i]=Sum[i-1]+(a[i]>r1);
		For(i,1,n) b[i]=B[i]=2*sum[i-1]-i,c[i]=C[i]=2*Sum[i-1]-i;
		sort(B+1,B+n+1),sort(C+1,C+n+1);
		cnt=unique(B+1,B+1+n)-B-1,Cnt=unique(C+1,C+1+n)-C-1;
		B[++cnt]=1e9,C[++Cnt]=1e9;
		For(i,1,n) b[i]=lower_bound(B+1,B+1+cnt,b[i])-B,c[i]=lower_bound(C+1,C+1+Cnt,c[i])-C;
		ll ans=0,l=0,r=0;
		For(i,1,n){
			t[i&1].Add(b[i],c[i],1);
			int x=lower_bound(B+1,B+1+cnt,sum[i]*2-i)-B;
			int y=lower_bound(C+1,C+1+Cnt,Sum[i]*2-i)-C;
			int X=lower_bound(B+1,B+1+cnt,sum[i]*2-i+1)-B;
			int Y=lower_bound(C+1,C+1+Cnt,Sum[i]*2-i-1)-C;
			ans+=t[(i&1)].query(x,cnt,y,Cnt)+t[(i&1)^1].query(X,cnt,Y,Cnt);//printf("%d %d %d %d %lld\n",x,y,X,Y,ans);
		//	printf("%lld %lld\n",t[(i&1)].query(x,cnt,y,Cnt),t[(i&1)^1].query(X,cnt,Y,Cnt));
		}
	//	puts("sum&Sum");
	//	For(i,1,n) printf("%d %d\n",sum[i],Sum[i]);
	//	puts("b&c:");
	//	For(i,1,n) printf("%d %d %d %d\n",b[i],c[i],B[b[i]],C[c[i]]);
		For(i,1,n) t[i&1].Add(b[i],c[i],-1);
		return ans;
	}
	inline void Main(){
		For(i,1,m) printf("%lld\n",Query(l1[i],r1[i],l2[i],r2[i]));
	}
}*/
namespace Subtask3{
	int cnt,Cnt;
	struct BIT_SegmentTree{
		int tot,rt[N],lson[N*200],rson[N*200],v[N*200];
		inline void insert(int &u,int l,int r,int ql,int x){
		    if (!u) u=++tot;v[u]+=x;
		    if (l==r) return;int mid=l+r>>1;
		    if (ql<=mid) insert(lson[u],l,mid,ql,x);
		        else insert(rson[u],mid+1,r,ql,x);
		}
		inline void Add(int x,int ql,int y){
		    for (;x<=cnt;x+=x&-x) insert(rt[x],1,Cnt,ql,y);
		}
		inline int Query(int u,int l,int r,int ql,int qr){
		    if (!u) return 0;
		    if (l>=ql&&r<=qr) return v[u];int mid=l+r>>1;
		    if (qr<=mid) return Query(lson[u],l,mid,ql,qr);
		    else if (ql>mid) return Query(rson[u],mid+1,r,ql,qr);
		    else return Query(lson[u],l,mid,ql,qr)+Query(rson[u],mid+1,r,ql,qr);
		}
		inline ll query(int l,int r,int ql,int qr){
		    if (l>r||ql>qr) return 0;
		    ll sum=0;
		    for (;r;r-=r&-r) sum+=Query(rt[r],1,Cnt,ql,qr);l--;
		    for (;l;l-=l&-l) sum-=Query(rt[l],1,Cnt,ql,qr);return sum;
		}
	}t[2];
	int sum[N],Sum[N],b[N],B[N],c[N],C[N];
	inline ll Query(int l1,int r1,int l2,int r2){
		For(i,1,n) sum[i]=sum[i-1]+(a[i]<l1);
		For(i,1,n) Sum[i]=Sum[i-1]+(a[i]>r1);
		For(i,1,n) b[i]=B[i]=2*sum[i-1]-i,c[i]=C[i]=2*Sum[i-1]-i;
		sort(B+1,B+n+1),sort(C+1,C+n+1);
		cnt=unique(B+1,B+1+n)-B-1,Cnt=unique(C+1,C+1+n)-C-1;
		B[++cnt]=1e9,C[++Cnt]=1e9;
		For(i,1,n) b[i]=lower_bound(B+1,B+1+cnt,b[i])-B,c[i]=lower_bound(C+1,C+1+Cnt,c[i])-C;
		ll ans=0,l=0,r=0;
	//	printf("%d %d\n",l2,r2);
		For(i,1,n){
			while (r<i-l2+1) ++r,t[r&1].Add(b[r],c[r],1);
			while (l<i-r2) ++l,t[l&1].Add(b[l],c[l],-1);//printf("%d %lld %lld %d %d\n",i,l,r,l2,r2);
			//t[i&1].Add(b[i],c[i],1);
			int x=lower_bound(B+1,B+1+cnt,sum[i]*2-i)-B;
			int y=lower_bound(C+1,C+1+Cnt,Sum[i]*2-i)-C;
			int X=lower_bound(B+1,B+1+cnt,sum[i]*2-i+1)-B;
			int Y=lower_bound(C+1,C+1+Cnt,Sum[i]*2-i-1)-C;
			ans+=t[(i&1)].query(x,cnt,y,Cnt)+t[(i&1)^1].query(X,cnt,Y,Cnt);//printf("%d %d %d %d %lld\n",x,y,X,Y,ans);
		//	printf("%lld %lld\n",t[(i&1)].query(x,cnt,y,Cnt),t[(i&1)^1].query(X,cnt,Y,Cnt));
		}
	//	puts("sum&Sum");
	//	For(i,1,n) printf("%d %d\n",sum[i],Sum[i]);
	//	puts("b&c:");
	//	For(i,1,n) printf("%d %d %d %d\n",b[i],c[i],B[b[i]],C[c[i]]);
		For(i,l+1,r) t[i&1].Add(b[i],c[i],-1);
		return ans;
	}
	inline void Main(){
		For(i,1,m) printf("%lld\n",Query(l1[i],r1[i],l2[i],r2[i]));
	}
}
int main(){
	freopen("x.in","r",stdin);
	freopen("x.out","w",stdout);
	n=read();
	For(i,1,n) a[i]=read();
	m=read();
	if (n<=1000) return Subtask1::Main(),0;
	flag=1;
	For(i,1,m){
		l1[i]=read(),r1[i]=read(),l2[i]=read(),r2[i]=read();
		//printf("%lld\n",Query(l1,r1,l2,r2));
		if (l2[i]!=1||r2[i]!=n) flag=0;
	} 
	Subtask3::Main();
	//if (flag) return Subtask2::Main(),0;
}
/*
5
4 1 2 2 5
5
2 4 1 5
2 4 1 5
2 5 1 5
3 5 1 5
1 5 1 5
*/
