#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 1e5+10;
int n,a[N];
ll b[N],Max;
vector<int>pos[N];
int l[N],cnt;
ll f[N]; 
inline void init(){
	b[n+1]=0,b[n+2]=1e9+7,sort(b+1,b+3+n);
	int m=unique(b+1,b+3+n)-b-1;
	For(i,1,n) a[i]=lower_bound(b+1,b+1+m,a[i])-b;
	For(i,1,n) pos[a[i]].push_back(i);
	For(i,1,m) pos[i].push_back(n+1);
	f[1]=f[2]=1;
	For(i,3,100){
		f[i]=f[i-1]+f[i-2];
		if (f[i]>Max*2){cnt=i;break;}
	}
//	printf("b:");For(i,1,m) printf("%d ",b[i]);puts("");
	For(i,1,n){
		l[i]=0;
		For(j,2,cnt){
			if (f[j]<b[a[i]]) continue;
			int x=f[j]-b[a[i]],y=lower_bound(b+1,b+1+m,x)-b,z;
			if (b[y]!=x||pos[y][0]>=i) continue;//printf("%d %d %d %d\n",i,x,y,pos[y][upper_bound(pos[y].begin(),pos[y].end(),i)-pos[y].begin()-1]);
			z=lower_bound(pos[y].begin(),pos[y].end(),i)-pos[y].begin();
			l[i]=max(l[i],pos[y][z-1]);
		}
	}
	//For(i,1,n) printf("%d ",l[i]);puts("");
}
int ans;
inline void solve(){
	int Max=l[n],ans=1;
	Dow(i,n-1,1)
		if (max(Max,l[i])>=i) ans++,Max=l[i];
			else Max=max(Max,l[i]);
	printf("%d\n",ans);
}
int main(){
	freopen("f.in","r",stdin);
	freopen("f.out","w",stdout);
	n=read();
	For(i,1,n) a[i]=b[i]=read(),Max=max(Max,(ll)a[i]);
	init(),solve();
}
/*
5
1 5 2 6 7
*/
