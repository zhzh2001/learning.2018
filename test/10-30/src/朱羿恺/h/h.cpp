#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
int n,p[110];
ll m;
namespace Subtask1{
	struct node{
		ll val,l;
		inline bool operator < (const node &b)const{return val>b.val;}
	};
	priority_queue<node>q;
	ll ans,Max;
	inline void Main(){
		For(i,1,n) q.push((node){p[i],i});Max=ans=1;
		while (!q.empty()){
			node u=q.top();Max=max(Max,u.val),ans++;q.pop();//printf("%lld\n",u.val);
			For(i,u.l,n) if (u.val<=m/p[i]) q.push((node){u.val*p[i],i});
		}
		printf("%lld\n%lld\n",Max,ans);
	}
}
int main(){
	freopen("h.in","r",stdin);
	freopen("h.out","w",stdout);
	n=read(),m=read();
	For(i,1,n) p[i]=read();
	sort(p+1,p+1+n);
	while (p[n]>m) n--;
	if (m<=1e6) Subtask1::Main();
}
/*
3 30
2 3 7
*/
