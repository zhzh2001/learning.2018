#include <cstdio>
#include <cctype>
#include <set> 
int read(){
	register int x = 0;
	register char f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = 0;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
int n, a[100005], f[50], ans;
std :: set<int> S;
int main(){
	freopen("f.in", "r", stdin);
	freopen("f.out", "w", stdout);
	n = read();
	for (register int i = 1; i <= n; ++i) a[i] = read();
	f[1] = f[2] = 1;
	for (register int i = 3; i <= 46; ++i) f[i] = f[i - 1] + f[i - 2];
	for (register int i = 1; i <= n; ++i){
		int flag = 0;
		for (register int j = 1; j <= 46; ++j)
			if (S.count(f[j] - a[i])){ flag = 1; break; }
		if (flag) ++ans, S.erase(S.begin(), S.end());
		S.insert(a[i]);
	}
	printf("%d", ans + 1);
}
