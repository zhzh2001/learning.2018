#include <cstdio>
#include <cstring>
#define N 1000005
int n, a[N], ans, mx;
int cnt, p[N], vis[N], minp[N];
long long m;
void prime(int n){
	vis[1] = 1;
	for (register int i = 2; i <= n; ++i){
		if (!vis[i]) p[++cnt] = i, minp[i] = i;
		for (register int j = 1; j <= cnt && i * p[j] <= n; ++j){
			vis[i * p[j]] = 1, minp[i * p[j]] = p[j];
			if (i % p[j] == 0) break;
		}
	}
}
int check(int x){
	for (; x > 1; x /= minp[x]) if (!a[minp[x]]) return 0;
	return 1;
}
int main(){
	freopen("h.in", "r", stdin);
	freopen("h.out", "w", stdout);
	scanf("%d%lld", &n, &m);
	for (register int i = 1, x; i <= n; ++i) scanf("%d", &x), a[x] = 1;
	prime(m);
	for (register int i = 1; i <= m; ++i)
		if (check(i)) ++ans, mx = i;
	printf("%d\n%d\n", mx, ans);
}
