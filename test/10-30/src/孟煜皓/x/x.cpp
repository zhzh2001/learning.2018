#include <cstdio>
#include <cctype>
#include <cstring>
int read(){
	register int x = 0;
	register char f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = 0;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
#define N 200005
int n, m, q, a[N], s[N], c[N];
long long ans;
void Add(int x, int y){
	for (; x <= m; x += x & -x) c[x] += y;
}
int Query(int x){
	int s = 0;
	for (; x; x -= x & -x) s += c[x];
	return s;
}
long long calc(int x, int y){
	s[0] = n + 1;
	for (register int i = 1; i <= n; ++i)
		if (a[i] <= x) s[i] = s[i - 1] + 1; else s[i] = s[i - 1] - 1;
	memset(c, 0, sizeof c);
	long long res = 0;
	if (y) Add(s[0], 1);
	for (register int i = 1; i <= n; ++i){
		res += Query(s[i]), Add(s[i], 1);
		if (i - y >= 0) Add(s[i - y], -1);
	}
	return res;
}
int main(){
	freopen("x.in", "r", stdin);
	freopen("x.out", "w", stdout);
	n = read(), m = (n << 1) + 1;
	for (register int i = 1; i <= n; ++i) a[i] = read();
	q = read();
	while (q--){
		int l1 = read() - 1, r1 = read(), l2 = read() - 1, r2 = read();
		ans = (calc(r1, r2) - calc(r1, l2)) - (calc(l1, r2) - calc(l1, l2));
		printf("%lld\n", ans);
	}
}
