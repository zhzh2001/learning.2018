#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){
	int t=0;bool p=0;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
	for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
	return p?-t:t;
}
int f[100],n,a[1001],I,dp[1001];
bool q[1001][1001],Q[1001][1001];
signed main(){
	freopen("f.in","r",stdin);
	freopen("f.out","w",stdout);
	n=read();
	f[1]=f[2]=1ll;
	for(I=3;f[I-1]<=n*1e9;I++)f[I]=f[I-1]+f[I-2];
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++)
			if(*lower_bound(f+1,f+I+1,a[i]+a[j])!=a[i]+a[j])q[i][j]=1;
	for(int i=1;i<=n;i++){
		q[i][i]=Q[i][i]=1;
		for(int j=i+1;j<=n;j++)Q[i][j]=(Q[i][j-1]&&q[i][j]);
	}
	for(int i=n;i;i--)
		for(int j=i-1;j;j--)q[j][i]=(q[j+1][i]&&Q[j][i]);
	for(int i=1;i<=n;i++){
		dp[i]=1e9;
		for(int j=1;j<=i;j++)
			if(dp[j-1]<1e9&&q[j][i])dp[i]=min(dp[i],dp[j-1]+1);
	}
	cout<<dp[n];
	return 0;
}
