#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int t=0;bool p=0;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
	for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
	return p?-t:t;
}
const int N=100000;
int n,m,l1,r1,l2,r2,a[N+5],t[N*4+5],Q,ans;
void update(int u,int l,int r,int x,int v){
	if(l==r){t[u]+=v;return;}
	int mid=(l+r)>>1;
	if(mid>=x)update(u<<1,l,mid,x,v);
	else update(u<<1|1,mid+1,r,x,v);
	t[u]=t[u<<1]+t[u<<1|1];
}
int query(int u,int l,int r,int k){
	if(l==r)return l;
	int mid=(l+r)>>1;
	if(t[u<<1]<k)return query(u<<1|1,mid+1,r,k-t[u<<1]);
	else return query(u<<1,l,mid,k);
}
int main(){
	freopen("x.in","r",stdin);
	freopen("x.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	m=read();
	while(m--){
		l2=read();r2=read();
		l1=read();r1=read();
		ans=0;
		for(int i=1;i<=n;i++){
			if(l1+i-1>n)break;
			memset(t,0,sizeof(t));
			for(int j=i;j<l1+i-1;j++)update(1,1,N,a[j],1);
			for(int j=l1+i-1;j<=min(r1+i-1,n);j++){
				update(1,1,N,a[j],1);
				Q=query(1,1,N,(j-i+2)/2);
				if(l2<=Q&&Q<=r2)ans++;
			}
		}
		cout<<ans<<'\n';
	}
	return 0;
}
