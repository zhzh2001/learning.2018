#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){
	int t=0;bool p=0;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
	for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
	return p?-t:t;
}
const int N=105;
int k,R,p[N],ans,mx;
void dfs(int s,int m){
	ans++;mx=max(mx,s);
	for(int i=m;i<=k;i++)
		if(s<=R/p[i])dfs(s*p[i],i);
}
signed main(){
	freopen("h.in","r",stdin);
	freopen("h.out","w",stdout);
	k=read();R=read();
	for(int i=1;i<=k;i++)p[i]=read();
	sort(p+1,p+k+1);
	dfs(1,1);
	cout<<mx<<'\n'<<ans;
	return 0;
}
