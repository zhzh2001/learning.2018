#include <bits/stdc++.h>
typedef long long ll;
typedef double db;
#define pa pair<int,int>
#define mk make_pair
#define LOG(format, args...) fprintf(stderr, format, args)
using namespace std;
inline ll rd() {
	ll x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) if(ch=='-') f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
//defs========================
ll K,R,p[30];
ll ss[1001000];
//main========================
int main() {
	K=rd(),R=rd();
	for(int i=1;i<=K;++i) p[i]=rd();
	int t=0;
	for(int i=1;i<=K;++i) printf("%d\n",(int)(log(R)/log(p[i]))),t+=(int)(log(R)/log(p[i]));
	printf("t=%d\n",t);
	exit(0);
	for(int i=1;i<=R;++i) {
		ll num=i;
		for(int j=1;j<=K;++j) {
			while(num%p[j]==0) num/=p[j];
		}
		if(num==1) {
			ss[++*ss]=i;
		}
	}
	printf("%lld\n%lld\n",*ss,ss[*ss]);
	
//	ll diff=0;
//	for(int i=0;i<ss.size();++i) {
//		if(i)
//			diff=max(diff,ss[i]-ss[i-1]);
//	}
//	printf("%lld\n",ss[ss.size()-1]);
//	printf("diff=%d\n",diff);
	//puts("");
//	printf("%lld\n",ss[ss.size()-1]);
//	for(int i=ss.size()-1;i>=0&&i>=ss.size()-100;--i) printf("%lld ",ss[i]);puts("");
	return 0;
}

