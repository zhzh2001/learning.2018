#include <bits/stdc++.h>
typedef long long ll;
typedef double db;
#define pa pair<int,int>
#define mk make_pair
#define LOG(format, args...) fprintf(stderr, format, args)
using namespace std;
inline ll rd() {
	ll x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) if(ch=='-') f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
//defs========================
ll K,R,p[30];
ll ss[1001000];
//main========================
int main() {
	freopen("h.in","r",stdin);
	freopen("h.out","w",stdout);
	K=rd(),R=rd();
	for(int i=1;i<=K;++i) p[i]=rd();
	for(int i=1;i<=R;++i) {
		ll num=i;
		for(int j=1;j<=K;++j) {
			while(num%p[j]==0) num/=p[j];
		}
		if(num==1) {
			ss[++*ss]=i;
		}
	}
	printf("%lld\n%lld\n",ss[*ss],*ss);
	return 0;
}

