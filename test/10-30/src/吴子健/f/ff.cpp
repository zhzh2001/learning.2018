#include <bits/stdc++.h>
typedef long long ll;
typedef double db;
#define pa pair<int,int>
#define mk make_pair
#define LOG(format, args...) fprintf(stderr, format, args)
using namespace std;
inline int rd() {
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) if(ch=='-') f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
map <ll,ll> vis;
ll a[3000],N; 
ll f[3000];
ll ans=0,lst=0;
//main========================
int main() {
	freopen("f1.in","r",stdin);
	N=rd();
	f[0]=f[1]=1;
	for(int i=2;;++i) {
		f[i]=f[i-1]+f[i-2];
		//printf("f[%d]=%lld\n",i,f[i]); 
		if(f[i]>2e9) break;
	}
	for(int i=1;i<=N;++i) a[i]=rd();
	for(int i=1;i<=N;++i) {
		if(vis[a[i]]) {
			lst=i-1;
			++ans;
			vis.clear();
			//printf("|");
		}
		for(int j=1;j<=46;++j) if(f[j]>a[i]) vis[f[j]-a[i]]=true; 
		//printf("%d ",a[i]);
	}
	if(lst!=N) lst=N,++ans;
	printf("%lld\n",ans);
	return 0;
}

