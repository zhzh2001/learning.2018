#include <bits/stdc++.h>
typedef long long ll;
typedef double db;
#define pa pair<int,int>
#define mk make_pair
#define LOG(format, args...) fprintf(stderr, format, args)
using namespace std;
inline int rd() {
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) if(ch=='-') f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
const int MAXN=200010;
ll N,M;
ll a[MAXN],b[MAXN],s[MAXN];
struct BIT {
	#define lb(x) ((x)&(-(x)))
	ll c[MAXN*3];
	void add(ll pos ,ll x) { for(;pos<=3*MAXN-10;pos+=lb(pos)) c[pos]+=x;}
	ll qry(ll pos) {
		ll ans=0;
		for(;pos;pos-=lb(pos)) ans+=c[pos];
		return ans;
	}
	void clear() {
		memset(c,0,sizeof c);
	}
}T;
//defs========================
void deal_ask(ll l1,ll r1,ll l2,ll r2);
//main========================
int main() {
	freopen("x.in","r",stdin);
	freopen("x.out","w",stdout);
	N=rd();
	for(int i=1;i<=N;++i) a[i]=rd();
	M=rd();
	while(M--) {
		int l1=rd(),r1=rd(),l2=rd(),r2=rd();
		deal_ask(l1,r1,l2,r2);
	}
	return 0;
}
void get_bs(int x) {
	s[0]=b[0]=0;
	for(int i=1;i<=N;++i) {
		if(a[i]>=x) b[i]=1;
		else b[i]=-1;
		s[i]=s[i-1]+b[i];
	}
	//for(int i=0;i<=N;++i) printf("s[%d]=%lld\n",i,s[i]);
	for(int i=0;i<=N;++i) s[i]+=(MAXN);
}
void get_val(ll L,ll R,ll & ans) {
	T.clear();
	//T.add(s[0],1);
	int lp=0,rp=0;
	for(int i=1;i<=N;++i) {
		//LOG("now considering i=%d\n",i);
		while(lp<=i-L) T.add(s[lp],1)/*,LOG("add %d\n",lp)*/,++lp;
		while(rp<=i-R-1) T.add(s[rp],-1)/*,LOG("remove %d\n",rp)*/,++rp;
		ans+=T.qry(s[i]-1);
		//LOG("added %d\n",T.qry(s[i]-1));
		//T.add(s[i],1);	
	}
}
void deal_ask(ll l1,ll r1,ll l2,ll r2) {
	ll ans=0,remove=0;
	get_bs(l1);
	get_val(l2,r2,ans);
	//LOG("ans=%lld\n",ans);
	get_bs(r1+1);
	get_val(l2,r2,remove);
	printf("%lld\n",ans-remove);
	
}
