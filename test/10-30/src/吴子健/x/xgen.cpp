#include <bits/stdc++.h>
typedef long long ll;
typedef double db;
#define pa pair<int,int>
#define mk make_pair
#define LOG(format, args...) fprintf(stderr, format, args)
using namespace std;
inline int rd() {
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) if(ch=='-') f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
int N=100000,M=5;
//main========================
int main() {
	srand((ll)(new char));
	freopen("x.in","w",stdout);
	printf("%d\n",N);
	for(int i=1;i<=N;++i) printf("%d ",rand()%(N-1)+1);
	printf("%d\n",M);
	int l1,r1,l2,r2;
	for(int i=1;i<=M;++i) {
		l1=rand(),r1=rand(),l2=rand(),r2=rand();
		if(l1>r1) swap(l1,r1);
		if(l2>r2) swap(l2,r2);
		printf("%d %d %d %d\n",l1,r1,l2,r2);
	}
	return 0;
}

