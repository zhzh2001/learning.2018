#include <bits/stdc++.h>
#define N 100020
#define ll long long
using namespace std;

inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}

int a[N];

const int fib_size = 45;
int fib[] = { 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946, 17711, 28657, 46368, 75025, 121393, 196418, 317811, 514229, 832040, 1346269, 2178309, 3524578, 5702887, 9227465, 14930352, 24157817, 39088169, 63245986, 102334155, 165580141, 267914296, 433494437, 701408733, 1134903170, 1836311903 };

namespace tanxin {

map<int, int> timber;

inline void add(int x) {
  for (int i = 0; i < fib_size; ++ i) {
    ++ timber[fib[i] - x];
  }
}
inline void remove(int x) {
  for (int i = 0; i < fib_size; ++ i) {
    -- timber[fib[i] - x];
  }
}

int solve(int n) {
  timber.clear();
  int l = 1, ans = 1;
  add(a[1]);
  for (int i = 2; i <= n; ++ i) {
    if (timber[a[i]]) {
      while (l < i) {
        remove(a[l ++]);
      }
      ++ ans;
    }
    add(a[i]);
  }
  return ans;
}

} // namespace tanxin

namespace baoli {

map<int, int> swwind;

int mr[N];

int solve(int n) {
  for (int i = 1; i <= n; ++ i) {
    mr[i] = n + 1;
  }
  for (int i = 0; i < fib_size; ++ i) {
    swwind.clear();
    for (int j = n; j; -- j) {
      if (swwind[fib[i] - a[j]]) {
        mr[j] = min(mr[j], swwind[fib[i] - a[j]]);
      }
      swwind[a[j]] = j;
    }
  }

  int now = n + 1, ans = 1;
  for (int i = 1; i <= n; ++ i) {
    if (now == i) {
      ++ ans;
      now = mr[i];
    } else {
      now = min(now, mr[i]);
    }
  }
  return ans;
}

}

int main(int argc, char const *argv[]) {

  freopen("f.in", "r", stdin);
  freopen("f.out", "w", stdout);
  int n = read();
  for (int i = 1; i <= n; ++ i) {
    a[i] = read();
  }
  printf("%d\n", tanxin::solve(n));
  /*
  srand(time(0));
  for (int t = 1; t <= 100; ++ t) {
    int n = 100000;
    for (int i = 1; i <= n; ++ i) {
      a[i] = rand() + 1;
    }
    int ansa, ansb;
    printf("Task #%d\n", t);
    printf("%d\n", ansa = tanxin::solve(n));
    printf("%d\n", ansb = baoli::solve(n));
    if (ansa != ansb) {
      puts("WAAAAAAAAAAAAAAAAAAA");
      for (int i = 1; i <= n; ++ i) {
        printf("%d ", a[i]);
      }
      puts("");
      exit(1);
    } else {
      puts("AC");
      puts("");
    }
  }
  */
  return 0;
}
/*

f[i][j] -> 第 i 只兔子，已经分了 j 组，最后一组的最大左区间

f[i][j] = i

if (check(f[i - 1][j], i, a[i])) {
  f[i][j] = max(f[i][j], f[i - 1][j]);
}

*/