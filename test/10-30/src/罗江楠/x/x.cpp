#include <bits/stdc++.h>
#define N 100020
#define ll long long
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}

int a[N];

int l1[10], r1[10], l2[10], r2[10];

int n, m;

int c[N];
inline void init() {
  memset(c, 0, sizeof c);
}
inline void push(int x) {
  for (; x <= 1e5; x += x & -x) {
    ++ c[x];
  }
}
inline int query(int x) {
  int ans = 0;
  for (; x; x ^= x & -x) {
    ans += c[x];
  }
  return ans;
}
inline int query_kth(int k) {
  int l = 1, r = 1e5;
  while (l <= r) {
    int mid = (l + r) >> 1;
    if (query(mid) >= k) {
      r = mid - 1;
    } else {
      l = mid + 1;
    }
  }
  return r + 1;
}

int ans[10];

int main(int argc, char const *argv[]) {
  freopen("x.in", "r", stdin);
  freopen("x.out", "w", stdout);

  n = read();
  for (int i = 1; i <= n; ++ i) {
    a[i] = read();
  }
  m = read();
  for (int i = 1; i <= m; ++ i) {
    l1[i] = read(); r1[i] = read();
    l2[i] = read(); r2[i] = read();
  }

  for (int l = 1; l <= n; ++ l) {
    init();
    for (int r = l; r <= n; ++ r) {
      push(a[r]);
      int len = r - l + 1;
      int res = query_kth((len + 1) / 2);
      for (int k = 1; k <= m; ++ k) {
        if (l1[k] <= res && res <= r1[k] &&
            l2[k] <= len && len <= r2[k]) {
          ++ ans[k];
        }
      }
    }
  }
  for (int i = 1; i <= m; ++ i) {
    printf("%d\n", ans[i]);
  }

  return 0;
}