#include <bits/stdc++.h>
#define N 100020
#define ll long long
using namespace std;
inline ll read(){
  ll x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}

ll p[N], n;

set<ll> st;
int main(int argc, char const *argv[]) {
  freopen("h.in", "r", stdin);
  freopen("h.out", "w", stdout);

  n = read();
  ll R = read();
  for (int i = 1; i <= n; ++ i) {
    p[i] = read();
  }
  sort(p + 1, p + n + 1);

  st.insert(1);
  ll ans = 0;
  set<ll>::iterator it = st.begin();
  while (*it * p[1] <= R) {
    ll x = *it;
    for (int i = 1; i <= n && x * p[i] <= R; ++ i) {
      st.insert(x * p[i]);
    }
    ++ it;
  }
  printf("%lld\n%u\n", *st.rbegin(), st.size());

  return 0;
}