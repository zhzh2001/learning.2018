#include <cstdio>
#include <cctype>
#include <algorithm>
#include <cstring>
#include <set>
using namespace std;

namespace zuiyt
{
	template<typename T>
	inline void read(T &x)
	{
		char c;
		bool f = false;
		x = 0;
		do
			c = getchar();
		while (c != '-' && !isdigit(c));
		if (c == '-')
			f = true, c = getchar();
		do
			x = x * 10 + c - '0', c = getchar();
		while (isdigit(c));	
		if (f)
			x = -x;
	}	
	template<typename T>
	inline void write(T x)
	{
		static char buf[20];
		char *pos = buf;
		if (x < 0)
			putchar('-'), x = -x;
		do
			*pos++ = x % 10 + '0';
		while (x /= 10);
		while (pos > buf)
			putchar(*--pos);
	}
	typedef long long ll;
	const int N = 1e5 + 10, A = 1e9 + 10, B = 20;
	int n, arr[N], dp[N], pre[N], f[N], id[N], cnt;
	ll last;
	bool cmparr(const int a, const int b)
	{
		return arr[a] < arr[b];	
	}
	namespace ST
	{
		int maxx[N][B], lg2[N];
		inline int _max(const int a, const int b)
		{
			return pre[a] > pre[b] ? a : b;	
		}
		inline void build()
		{
			int tmp = 0;
			for (int i = 1; i < N; i++)
			{
				lg2[i] = tmp;
				if (i == (1 << (tmp + 1)))
					++tmp;
			}
			for (int i = n; i; i--)
			{
				maxx[i][0] = i;
				for (int j = 1; j < B; j++)
					if (i + (1 << j) - 1 <= n)
						maxx[i][j] = _max(maxx[i][j - 1], maxx[i + (1 << (j - 1))][j - 1]);
			}
		}
		inline int query(int a, int b)
		{
			if (a > b)
				swap(a, b);
			int len = b - a + 1;
			return _max(maxx[a][lg2[len]], maxx[b - (1 << lg2[len]) + 1][lg2[len]]);
		}
	}
	int work()
	{
		read(n);
		for (int i = 1; i <= n; i++)
			read(arr[i]), id[i] = i;
		sort(id + 1, id + n + 1, cmparr);
		f[0] = f[1] = 1, cnt = 2;
		while (f[cnt - 1] < A)
			f[cnt] = f[cnt - 1] + f[cnt - 2], ++cnt;
		last = f[cnt - 1] + f[cnt - 2];
		if (last < 2e9 + 10)
			f[cnt++] = last;
		for (int i = 1; i <= n; i++)
		{
			int j = upper_bound(f, f + cnt, arr[i]) - f;
			while (j < cnt)
			{
				arr[n + 1] = f[j] - arr[i];//The element to sort
				int pos1 = lower_bound(id + 1, id + n + 1, n + 1, cmparr) - id;
				int pos2 = upper_bound(id + 1, id + n + 1, n + 1, cmparr) - id;
				for (int k = pos1; k < pos2; k++)
					if (id[k] < i && id[k] > pre[i])
						pre[i] = id[k];
				j++;
			}
		}
		ST::build();
		for (int i = 1; i <= n; i++)
		{
			dp[i] = dp[i - 1] + 1;
			int l = 0, r = i - 1, p = i - 1;
			while (l <= r)
			{
				int mid = (l + r) >> 1;
				if (pre[ST::query(mid + 1, i)] <= mid)
					r = mid - 1, p = mid;
				else
					l = mid + 1;
			}
			dp[i] = min(dp[i], dp[p] + 1);
		}
		write(dp[n]);
		return 0;
	}
}
int main()
{
	freopen("f.in", "r", stdin);
	freopen("f.out", "w", stdout);
	return zuiyt::work();	
}
