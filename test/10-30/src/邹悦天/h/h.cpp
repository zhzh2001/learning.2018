#include <cstdio>
#include <cctype>
#include <algorithm>
#include <cstring>
using namespace std;

namespace zuiyt
{
	template<typename T>
	inline void read(T &x)
	{
		char c;
		bool f = false;
		x = 0;
		do
			c = getchar();
		while (c != '-' && !isdigit(c));
		if (c == '-')
			f = true, c = getchar();
		do
			x = x * 10 + c - '0', c = getchar();
		while (isdigit(c));	
		if (f)
			x = -x;
	}	
	template<typename T>
	inline void write(T x)
	{
		static char buf[20];
		char *pos = buf;
		if (x < 0)
			putchar('-'), x = -x;
		do
			*pos++ = x % 10 + '0';
		while (x /= 10);
		while (pos > buf)
			putchar(*--pos);
	}
	typedef long long ll;
	const int N = 30, R = 1e6 + 10;
	int n;
	ll r, p[N], dp[R];
	int work()
	{
		read(n), read(r);
		for (int i = 1; i <= n; i++)
			read(p[i]);
		for (int i = 1; i <= r; i++)
			dp[i] = 1;
		for (int i = 1; i <= n; i++)
			for (int j = 1; j * p[i] <= r; j++)
				dp[j * p[i]] = max(dp[j * p[i]], dp[j] * p[i]);
		ll ans = 0, ans2 = 0;
		for (int i = 1; i <= r; i++)
			if (dp[i] == i)
				++ans2, ans = dp[i];
		write(ans);
		putchar('\n');
		write(ans2);
		return 0;	
	}
}
int main()
{
	freopen("h.in", "r", stdin);
	freopen("h.out", "w", stdout);
	return zuiyt::work();	
}
