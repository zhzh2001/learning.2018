#include <cstdio>
#include <cctype>
#include <algorithm>
#include <cstring>
#include <set>
using namespace std;

namespace zuiyt
{
	template<typename T>
	inline void read(T &x)
	{
		char c;
		bool f = false;
		x = 0;
		do
			c = getchar();
		while (c != '-' && !isdigit(c));
		if (c == '-')
			f = true, c = getchar();
		do
			x = x * 10 + c - '0', c = getchar();
		while (isdigit(c));	
		if (f)
			x = -x;
	}	
	template<typename T>
	inline void write(T x)
	{
		static char buf[20];
		char *pos = buf;
		if (x < 0)
			putchar('-'), x = -x;
		do
			*pos++ = x % 10 + '0';
		while (x /= 10);
		while (pos > buf)
			putchar(*--pos);
	}
	const int N = 1e5 + 10;
	int n, m, arr[N], buf[N];
	int work()
	{
		read(n);
		for (int i = 1; i <= n; i++)
			read(arr[i]);
		read(m);
		while (m--)
		{
			int l1, r1, l2, r2, ans = 0;
			read(l1), read(r1), read(l2), read(r2);
			for (int i = 1; i <= n; i++)
				for (int len = l2; len <= r2; len++)
				{
					int j = i + len - 1;
					if (j > n)
						break;
					for (int k = 0; k < len; k++)
						buf[k] = arr[i + k];
					sort(buf, buf + len);
					if (l1 <= buf[(len - 1) >> 1] && buf[(len - 1) >> 1] <= r1)
						++ans;
				}
			write(ans);
			putchar('\n');
		}
		return 0;
	}
}
int main()
{
	freopen("x.in", "r", stdin);
	freopen("x.out", "w", stdout);
	return zuiyt::work();	
}
