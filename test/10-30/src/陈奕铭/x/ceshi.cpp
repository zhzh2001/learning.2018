#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x = 0,f = 1; char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 100005;
int n;
int a[N];
int pre0[N],pre1[N];
int sum0[2][N],sum1[2][N];
int pos0[2],pos1[2];
int b0[2],b1[2];

signed main(){
	n = read();
	for(int i = 1;i <= n;++i) a[i] = read();
	int m = read();
	while(m--){
		int l1 = read(),r1 = read(),l2 = read(),r2 = read();
		for(int i = 1;i <= n;++i){
			pre0[i] = pre0[i-1];
			pre1[i] = pre1[i-1];
			if(a[i] < l1) ++pre0[i];
			if(a[i] <= r1) ++pre1[i];
		}
		l2--;
		ll ans1 = 0,ans2 = 0;
		if(l2 > 0){
			memset(sum0,0,sizeof sum0);
			memset(sum1,0,sizeof sum1);
			b0[0] = b1[0] = b0[1] = b1[1] = 0;
			pos0[0] = pos0[1] = pos1[0] = pos1[1] = 0;
			for(int i = 1;i <= n;++i){
				int p = i%2;
				++pos0[p]; ++pos1[p];
				++sum0[p][pos0[p]];
				++sum1[p][pos1[p]];
				if(a[i] < l1){
					b0[0] += sum0[0][pos0[0]];
					b0[1] += sum0[1][pos0[1]];
					pos0[0]--;
					pos0[1]--;
				}
				if(a[i] <= r1){
					b1[0] += sum1[0][pos1[0]];
					b1[1] += sum1[1][pos1[1]];
					pos1[0]--;
					pos1[1]--;
				}
				if(i-l2 > 0){
					int j = i-l2,p2 = j%2;
					--sum0[p2][pos0[p2]+pre0[i]-pre0[j-1]-(i-j)/2];
					--sum1[p2][pos1[p2]+pre1[i]-pre1[j-1]-(i-j)/2];
					if(pre0[i]-pre0[j-1]-(i-j)/2 > 0) --b0[p2],--b1[p2];
				}
				ans1 += b1[0]-b0[0]+b1[1]-b0[1];
			}
		}
		memset(sum0,0,sizeof sum0);
		memset(sum1,0,sizeof sum1);
		b0[0] = b1[0] = b0[1] = b1[1] = 0;
		pos0[0] = pos0[1] = pos1[0] = pos1[1] = 0;
		for(int i = 1;i <= n;++i){
			int p = i%2;
			++pos0[p]; ++pos1[p];
			++sum0[p][pos0[p]];
			++sum1[p][pos1[p]];
			if(a[i] < l1){
				b0[0] += sum0[0][pos0[0]];
				b0[1] += sum0[1][pos0[1]];
				pos0[0]--;
				pos0[1]--;
			}
			if(a[i] <= r1){
				b1[0] += sum1[0][pos1[0]];
				b1[1] += sum1[1][pos1[1]];
				pos1[0]--;
				pos1[1]--;
			}
			if(i-r2 > 0){
				int j = i-r2,p2 = j%2;
				--sum0[p2][pos0[p2]+pre0[i]-pre0[j-1]-(i-j)/2];
				--sum1[p2][pos1[p2]+pre1[i]-pre1[j-1]-(i-j)/2];
				if(pre0[i]-pre0[j-1]-(i-j)/2 > 0) --b0[p2],--b1[p2];
			}
			ans2 += b1[0]-b0[0]+b1[1]-b0[1];
		}
		for(int i = l2;i <= r2;++i){
			if(pre0[i] <= (i-1)/2)
				if(pre1[i] >= (i+1)/2)
					++ans2;
		}
		printf("%lld\n",ans2-ans1);
	}
	return 0;
}
