#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x = 0,f = 1; char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 100005;
int n;
int a[N];
int pre0[N],pre1[N];

signed main(){
	freopen("x.in","r",stdin);
	freopen("x.out","w",stdout);
	n = read();
	for(int i = 1;i <= n;++i) a[i] = read();
	int m = read();
	while(m--){
		int l1 = read(),r1 = read(),l2 = read(),r2 = read();
		for(int i = 1;i <= n;++i){
			pre0[i] = pre0[i-1];
			pre1[i] = pre1[i-1];
			if(a[i] < l1) ++pre0[i];
			else if(a[i] <= r1) ++pre1[i];
		}
		int ans = 0;
		for(int i = 1;i <= n;++i)
			for(int j = i;j <= n;++j)
				if(j-i+1 >= l2 && j-i+1 <= r2){
					int len = j-i+1;
					if(pre0[j]-pre0[i-1] <= (len-1)/2)
						if(pre1[j]-pre1[i-1]+pre0[j]-pre0[i-1] >= (len+1)/2)
							++ans;
				}
		printf("%d\n",ans);
	}
	return 0;
}
