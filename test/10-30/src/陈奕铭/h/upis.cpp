#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x = 0,f = 1; char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 100;
bool vis[N+5];
int pri[N],cnt;

signed main(){
	for(int i = 2;i <= N;++i){
		if(!vis[i]) pri[++cnt] = i;
		for(int  j =1;j <= cnt;++j){
			if(pri[j]*i > N) break;
			vis[pri[j]*i] = true;
			if(i%pri[j] == 0) break;
		}
	}
	for(int i = 1;i <= 25;++i)
		printf("%d ",pri[i]);
	return 0;
}
