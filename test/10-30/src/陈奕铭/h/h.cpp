#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x = 0,f = 1; char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 1000005;
ll a[N],cnt,ans;
ll k,R;
ll p[27];

signed main(){
	freopen("h.in","r",stdin);
	freopen("h.out","w",stdout);
	k = read(); R = read();
	for(int i = 1;i <= k;++i) p[i] = read();
	a[++cnt] = 1; ans = 1;
	for(int i = 1;i <= k;++i){
		int ncnt = cnt;
		for(int j = 1;j <= ncnt;++j){
			ll x = a[j];
			while(R/x >= p[i]){				
				x *= p[i];
				a[++cnt] = x;
				ans = max(ans,x);
			}
		}
	}
	printf("%lld\n",ans);
	printf("%lld\n",cnt);
	return 0;
}
