#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline int read(){
	int x = 0,f = 1; char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}
const int N = 100005;
int n,cnt;
int a[N];
ll d[50];
int L[N],f[N];
set<int> S;
int Mi[N<<2];

#define lowbit(x) ((x)&(-x))

inline int Min(int x,int y){
	if(x < y) return x;
	return y;
}

void update(int num,int l,int r,int x,int y){
	Mi[num] = Min(Mi[num],y);
	if(l == r) return;
	int mid = (l+r)>>1;
	if(x <= mid) update(num<<1,l,mid,x,y);
	else update(num<<1|1,mid+1,r,x,y);
}

int query(int num,int l,int r,int x,int y){
	if(x <= l && r <= y) return Mi[num];
	int mid = (l+r)>>1,ans = 2e9;
	if(x <= mid) ans = query(num<<1,l,mid,x,y);
	if(y > mid) ans = Min(ans,query(num<<1|1,mid+1,r,x,y));
	return ans;
}

signed main(){
	freopen("f.in","r",stdin);
	freopen("f.out","w",stdout);
	memset(Mi,0x3f,sizeof Mi);
	n = read();
	d[0] = d[1] = 1;
	for(int i = 2;;++i){
		d[i] = d[i-1]+d[i-2];
		if(d[i] > 2e9){
			cnt = i-1;
			break;
		}
	}
	for(int i = 1;i <= n;++i) a[i] = read();
	L[n+1] = n+1;
	for(int i = n;i >= 1;--i){
		L[i] =  L[i+1];
		while(L[i]-1 >= 1){
			if(S.count(a[L[i]-1])) break;
			for(int j = 1;j <= cnt;++j)
				if(d[j] > a[L[i]-1]){
					S.insert(d[j]-a[L[i]-1]);
				}
			--L[i];
		}
		for(int j = 1;j <= cnt;++j)
			if(d[j] > a[i])
				S.erase(d[j]-a[i]);
	}
	f[0] = 0;
//	for(int i = 1;i <= n;++i) printf("%d ",L[i]);
	update(1,0,n,0,0);
	for(int i = 1;i <= n;++i){    	
		f[i] = query(1,0,n,L[i]-1,i-1)+1;
		update(1,0,n,i,f[i]);
	}
	printf("%d\n",f[n]);
	return 0;
}
