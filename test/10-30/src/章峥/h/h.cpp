#include<fstream>
#include<algorithm>
#include<vector>
using namespace std;
ifstream fin("h.in");
ofstream fout("h.out");
const int K=30;
int k,p[K];
long long r,ans,maxv;
vector<long long> res;
void dfs(long long now,int pred)
{
	res.push_back(now);
	for(int i=pred;i<=k/2;i++)
		if(1.*now*p[i]<=2e18&&now*p[i]<=r)
			dfs(now*p[i],i);
}
void dfs2(long long now,int pred)
{
	int idx=lower_bound(res.begin(),res.end(),r/now)-res.begin();
	if(idx&&(idx==res.size()||now*res[idx]>r))
		idx--;
	ans+=idx+1;
	maxv=max(maxv,now*res[idx]);
	for(int i=pred;i<=k;i++)
		if(1.*now*p[i]<=2e18&&now*p[i]<=r)
			dfs2(now*p[i],i);
}
int main()
{
	srand(1e9+7);
	fin>>k>>r;
	for(int i=1;i<=k;i++)
		fin>>p[i];
	random_shuffle(p+1,p+k+1);
	dfs(1,1);
	sort(res.begin(),res.end());
	dfs2(1,k/2+1);
	fout<<maxv<<endl<<ans<<endl;
	return 0;
}
