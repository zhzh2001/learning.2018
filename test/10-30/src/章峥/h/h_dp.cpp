#include<fstream>
#include<map>
using namespace std;
ifstream fin("h.in");
ofstream fout("h.out");
const int K=30;
int k,p[K];
long long r;
map<pair<int,long long>,long long> dp;
long long calc(int i,long long n)
{
	if(n==1||i>k)
		return 1;
	if(dp.find(make_pair(i,n))!=dp.end())
		return dp[make_pair(i,n)];
	long long ans=0;
	long long tmp=n;
	for(int j=0;tmp;j++,tmp/=p[i])
		ans+=calc(i+1,tmp);
	return dp[make_pair(i,n)]=ans;
}
int main()
{
	fin>>k>>r;
	for(int i=1;i<=k;i++)
		fin>>p[i];
	fout<<calc(1,r)<<endl;
	return 0;
}
