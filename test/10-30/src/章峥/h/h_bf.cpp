#include<fstream>
#include<set>
using namespace std;
ifstream fin("h.in");
ofstream fout("h.ans");
const int K=30;
int p[K];
int main()
{
	int k;
	long long r;
	fin>>k>>r;
	for(int i=1;i<=k;i++)
		fin>>p[i];
	set<long long> S;
	S.insert(1);
	int cnt=0;
	long long ans=0;
	while(S.size())
	{
		long long num=*S.begin();S.erase(S.begin());
		cnt++;
		ans=num;
		for(int i=1;i<=k;i++)
			if(1.*num*p[i]<2e18&&num*p[i]<=r)
				S.insert(num*p[i]);
		for(int i=1;i<=k;i++)
		{
			int cnt=0;
			for(;num%p[i]==0;num/=p[i])
				cnt++;
			fout<<cnt<<' ';
		}
		fout<<endl;
	}
	fout<<ans<<endl<<cnt<<endl;
	return 0;
}
