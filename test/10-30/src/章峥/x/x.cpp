#include<fstream>
#include<cstring>
using namespace std;
ifstream fin("x.in");
ofstream fout("x.out");
const int N=100005;
int n,a[N],sum[N];
struct BIT
{
	int tree[N*2];
	void clear()
	{
		memset(tree,0,sizeof(tree));
	}
	void modify(int x,int val)
	{
		for(;x<=n*2;x+=x&-x)
			tree[x]+=val;
	}
	int query(int x)
	{
		int ans=0;
		for(;x;x-=x&-x)
			ans+=tree[x];
		return ans;
	}
}T;
long long solve(int val,int l,int r)
{
	sum[0]=n;
	for(int i=1;i<=n;i++)
		sum[i]=sum[i-1]+(a[i]<=val?1:-1);
	long long ans=0;
	T.clear();
	for(int i=l;i<=n;i++)
	{
		T.modify(sum[i-l],1);
		if(i>r)
			T.modify(sum[i-r-1],-1);
		ans+=T.query(sum[i]);
	}
	return ans;
}
int main()
{
	fin>>n;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	int m;
	fin>>m;
	while(m--)
	{
		int l1,l2,r1,r2;
		fin>>l1>>r1>>l2>>r2;
		fout<<solve(r1,l2,r2)-solve(l1-1,l2,r2)<<endl;
	}
	return 0;
}
