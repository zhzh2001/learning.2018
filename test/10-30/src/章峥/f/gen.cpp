#include<fstream>
#include<random>
#include<windows.h>
using namespace std;
ofstream fout("f.in");
const int n=1000;
int main()
{
	minstd_rand gen(GetTickCount());
	fout<<n<<endl;
	uniform_int_distribution<> d1(1,30),d2(1,1e9);
	for(int i=1;i<=n/2;i++)
		fout<<d1(gen)<<' ';
	for(int i=1;i<=n/2;i++)
		fout<<d2(gen)<<' ';
	fout<<endl;
	return 0;
}
