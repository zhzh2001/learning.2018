#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("f.in");
ofstream fout("f.ans");
const int N=5005,INF=1e9;
int dp[N];
long long a[N],f[50];
int main()
{
	f[1]=f[2]=1;
	int m;
	for(int i=3;;i++)
	{
		f[i]=f[i-1]+f[i-2];
		if(f[i]>2e9)
		{
			m=i;
			break;
		}
	}
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	dp[0]=0;
	for(int i=1;i<=n;i++)
	{
		dp[i]=INF;
		for(int j=i-1;j>=0;j--)
		{
			bool flag=true;
			for(int k=j+2;k<=i;k++)
				if(*lower_bound(f+1,f+m+1,a[j+1]+a[k])==a[j+1]+a[k])
				{
					flag=false;
					break;
				}
			if(flag)
				dp[i]=min(dp[i],dp[j]+1);
			else
				break;
		}
	}
	fout<<dp[n]<<endl;
	return 0;
}
