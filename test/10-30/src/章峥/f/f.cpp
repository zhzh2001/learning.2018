#pragma GCC optimize 2
#include<fstream>
#include<algorithm>
#include<set>
using namespace std;
ifstream fin("f.in");
ofstream fout("f.out");
const int N=100005;
unsigned a[N],f[50];
int main()
{
	f[1]=f[2]=1;
	int m;
	for(int i=3;;i++)
	{
		f[i]=f[i-1]+f[i-2];
		if(f[i]>2e9)
		{
			m=i;
			break;
		}
	}
	int n;
	fin>>n;
	set<pair<unsigned,int> > S;
	for(int i=1;i<=n;i++)
	{
		fin>>a[i];
		S.insert(make_pair(a[i],i));
	}
	int ans=0;
	for(int l=1,r=n+1;l<=n;l++)
	{
		S.erase(make_pair(a[l],l));
		for(int j=m;f[j]>a[l];j--)
		{
			set<pair<unsigned,int> >::iterator it=S.lower_bound(make_pair(f[j]-a[l],0));
			if(it!=S.end()&&it->first==f[j]-a[l])
				r=min(r,it->second);
		}
		if(l+1==r)
		{
			ans++;
			r=n+1;
		}
	}
	fout<<ans<<endl;
	return 0;
}
