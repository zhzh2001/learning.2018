uses math;
var n,m,i,j,k,l1,r1,l2,r2,t,p:longint;  ans:int64;
    a,b:array[0..100000]of longint;
    f:array[0..1000,0..1000]of longint;
    c:array[0..1000,0..1000]of boolean;
begin
 assign(input,'x.in');
 assign(output,'x.out');
 reset(input);
 rewrite(output);
 read(n);
 for i:=1 to n do read(a[i]);
 for i:=1 to n do
 begin
  t:=0;
  for j:=i to n do
  begin
   if j=i then begin inc(t); b[t]:=a[j]; f[i,j]:=b[t]; continue; end;
   p:=0;
   for k:=1 to t-1 do
    if (a[j]>=b[k])and(a[j]<=b[k+1]) then
    begin
     p:=k+1;
     break;
    end;
   inc(t);
   if p>0 then
   begin
    for k:=t downto p+1 do b[k]:=b[k-1];
    b[p]:=a[j];
   end
   else if b[1]>a[j] then
   begin
    for k:=t downto 2 do b[k]:=b[k-1];
    b[1]:=a[j];
   end
   else b[t]:=a[j];
   f[i,j]:=b[(t+1)div 2];
  end;
 end;
 read(m);
 for i:=1 to m do
 begin
  ans:=0; fillchar(c,sizeof(c),0);
  read(l1,r1,l2,r2);
  for j:=l1 to r1 do
   for k:=l2 to r2 do
    for p:=max(j-k+1,1) to j do
     if (p+k-1<=n)and(not c[p,p+k-1]) then
      if f[p,p+k-1]=a[j] then begin inc(ans); c[p,p+k-1]:=true; end;
  writeln(ans);
 end;
 close(input);
 close(output); 
end.