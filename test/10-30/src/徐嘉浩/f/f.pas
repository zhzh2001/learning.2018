var i,n,ans,l,r:longint;
    f:array[0..46]of int64;
    a:array[0..100000]of int64;
    p:boolean;
function pd(k:int64):boolean;
var l,r,mid:longint;
begin
 l:=1; r:=46;
 while l<r do
 begin
  mid:=(l+r)div 2;
  if k=f[mid] then exit(true);
  if k>f[mid] then l:=mid+1
  else r:=mid-1;
 end;
 if f[l]<>k then exit(false);
 exit(true);
end;
begin
 assign(input,'f.in');
 assign(output,'f.out');
 reset(input);
 rewrite(output);
 f[0]:=1; f[1]:=1;
 for i:=2 to 46 do f[i]:=f[i-1]+f[i-2];
 read(n);
 for i:=1 to n do read(a[i]);
 l:=1; r:=2;
 while r<=n do
 begin
  p:=false;
  for i:=l to r-1 do
   if pd(a[i]+a[r]) then begin p:=true; break; end;
  if p then begin inc(ans); l:=r; end;
  inc(r);
 end;
 inc(ans);
 writeln(ans);
 close(input);
 close(output);
end.