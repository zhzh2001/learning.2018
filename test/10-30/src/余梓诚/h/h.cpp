#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;
void judge() {
	freopen("h.in","r",stdin);
	freopen("h.out","w",stdout);
}
const int N=1100000;
int n,p[30],ans,prime[N],opt,R,mx;
bool used[N],vis[N];
bool check(int x) {
	for (int i=1; i<=opt; i++) {
		if (x<prime[i]) break;
		if (x%prime[i]==0&&!vis[prime[i]]) return 0;
		while (x%prime[i]==0) x/=prime[i];
	}
	if (x>1&&!vis[x]) return 0;
	return 1;
}
void init() {
	for (int i=2; i<=R; i++) {
		if (!used[i]) prime[++opt]=i;
		for (int j=1; j<=opt&&i*prime[j]<=R; j++) {
			used[i*prime[j]]=1;
			if (i%prime[j]==0) break;
		}
	}
}
int main() {
	judge();
	scanf("%d%d",&n,&R);
	for (int i=1; i<=n; i++) scanf("%d",&p[i]),vis[p[i]]=1;
	init();
	for (int i=1; i<=R; i++) if (check(i)) mx=i,ans++;
	printf("%d\n%d\n",mx,ans);
	return 0;
}
