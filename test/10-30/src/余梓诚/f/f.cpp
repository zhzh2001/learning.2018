#include <map>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;
void judge() {
	freopen("f.in","r",stdin);
	freopen("f.out","w",stdout);
}
const int N=110000;
int ans,n,a[N],fib[50];
map<int,int> pos;
int main() {
	judge();
	scanf("%d",&n);
	fib[1]=1;
	fib[2]=2;
	for (int i=3; i<=46; i++) fib[i]=fib[i-1]+fib[i-2];
	for (int i=1; i<=n; i++) scanf("%d",&a[i]);
	pos[a[1]]=1;
	int l=1;
	for (int i=2; i<=n; i++) {
		bool flag=0;
		for (int j=1; j<=46; j++) {
			if (pos[fib[j]-a[i]]>=l) {
				flag=1;
				break;
			}
		}
		pos[a[i]]=i;
		if (flag) ans++,l=i;
	}
	printf("%d\n",ans+1);
	return 0;
}
