#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;
void judge() {
	freopen("x.in","r",stdin);
	freopen("x.out","w",stdout);
}
const int N=110000;
int n,a[N],m,b[N];
int main() {
	judge();
	scanf("%d",&n);
	for (int i=1; i<=n; i++) scanf("%d",&a[i]);
	scanf("%d",&m);
	for (int s=1; s<=m; s++) {
		int l,r,L,R,ans=0;
		scanf("%d%d%d%d",&l,&r,&L,&R);
		for (int len=L; len<=R; len++) {
			for (int i=1; i+len-1<=n; i++) {
				int j=i+len-1;
				for (int k=i; k<=j; k++) b[k-i+1]=a[k];
				sort(b+1,b+len+1);
				if (len%2==0) {
					if (b[len/2]>=l&&b[len/2]<=r) ans++;
				} else if (b[len/2+1]>=l&&b[len/2+1]<=r) ans++;
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
