#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for(int i=a;i<=b;i++)
#define Rep(i,a,b) for(int i=b;i>=a;i--)
using namespace std;
inline ll read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
ll a[100100],f[100100];
ll n,cnt,ed;
ll fib[110];
int main()
{
	freopen("f.in","r",stdin);
	freopen("f.out","w",stdout);
	cnt=2;
	fib[1]=1;
	fib[2]=1;
	while(fib[cnt]<=2e9)
	{
		cnt++;
		fib[cnt]=fib[cnt-1]+fib[cnt-2];
	}
	n=read();
	For(i,1,n)a[i]=read();
	ll ans=1;int ma=0;
	For(i,1,n)
	{
		bool flag=true;
		Rep(j,1,cnt)
		{
			if(fib[j]<=a[i])break;
			For(k,1,ed)
			{
				if(f[k]==fib[j]-a[i])
				{
					flag=false;
					break;
				}
			}
			if(!flag)break;
		}
		if(!flag)
		{
			ans++;
			ed=0;
		}
		ed++;
		f[ed]=a[i];
	}
	cout<<ans<<endl;
	return 0;
}

