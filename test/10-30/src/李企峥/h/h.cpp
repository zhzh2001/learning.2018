#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for(int i=a;i<=b;i++)
using namespace std;
ll mx,r,ans,n;
ll a[30];
inline ll read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
void dfs(int x,ll y)
{
	if(x==n+1)
	{
		if(y<=r)
		{
			ans++;
			mx=max(mx,y);	
		}
		return;
	}
	if(y>r)return;
	while(y<=r)
	{
		dfs(x+1,y);
		y*=a[x];
	}
	return;
}
int main()
{
	freopen("h.in","r",stdin);
	freopen("h.out","w",stdout);
	n=read();r=read();
	For(i,1,n)a[i]=read();
	sort(a+1,a+n+1);
	dfs(1,1);
	cout<<mx<<endl;
	cout<<ans<<endl;
	return 0;
}

