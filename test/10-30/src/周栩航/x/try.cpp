#include<bits/stdc++.h>
#define ll long long
#define For(i,j,k)  for(int i=j;i<=k;++i)
#define Dow(i,j,k)  for(int i=k;i>=j;--i)
#define int ll
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))  {if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))   t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0){x=-x;putchar('-');}if(x>=10)    write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}

const int N=100005;
int n,a[N],m,b[N];

namespace sub1
{
	const int N=100005;
	int m,b[N],s0[N],s2[N];
	const int MX=6*N;
	int tr[MX+1][2],tr1[MX+1][2];
	inline void Add(int x,int v,int kd)
	{
		for(;x<=MX;x+=x&-x)    tr[x][kd]+=v;
	}
	inline int Get(int x,int kd)
	{
		int sum=0;
		for(;x;x-=x&-x) sum+=tr[x][kd];
		return sum;
	}
	inline void Add1(int x,int v,int kd)
	{
		for(;x;x-=x&-x) tr1[x][kd]+=v;
	}
	inline int Get1(int x,int kd)
	{
		int sum=0;
		for(;x<=MX;x+=x&-x) sum+=tr1[x][kd];
		return sum;
	}
	inline int Sol(int l1,int r1,int len)
	{
		if(len==0)  return 0;
		int ans=0;
		For(i,1,n)  b[i]=1;
		For(i,1,n)  if(a[i]<l1)  b[i]=0;else if(a[i]>r1)  b[i]=2;
		For(i,1,n)  ans+=min(len-1,(i-1))+(b[i]==1);
		For(i,1,n)
		{
			s0[i]=s0[i-1];s2[i]=s2[i-1];
			if(b[i]==0) s0[i]++;
			if(b[i]==2) s2[i]++;
		}
		For(i,1,n)
		{
			int zt=(i&1);
			ans-=Get(2*s0[i]-i-1+2*n+1,zt^1)+Get(2*s0[i]-i-2+2*n+1,zt);
			Add(s0[i-1]*2-i+2*n+1,1,zt);
			int j=(i-len+1);
			if(j>=1)	Add(s0[j-1]*2-j+2*n+1,-1,j&1);
		}
		For(i,n-len+2,n)  Add(s0[i-1]*2-i+2*n+1,-1,i&1);
		For(i,1,n)
		{
			int zt=(i&1);
			ans-=Get1(i-2*s2[i]+1+2*n+1,zt)+Get1(i-2*s2[i]+2+2*n+1,zt^1);
			Add1(i-2*s2[i-1]+2*n+1,1,zt);
			int j=(i-len+1);
			if(j>=1)	Add1(j-2*s2[j-1]+2*n+1,-1,j&1);
		}
		For(i,n-len+2,n)  Add1(i-2*s2[i-1]+2*n+1,-1,i&1);
		return ans;
	}
	inline void Solve(int l1,int r1,int l2,int r2)
	{
		int ans=0;
		ans=Sol(l1,r1,r2)-Sol(l1,r1,l2-1);
		writeln(ans);
	}
	inline void Main()
	{
		m=read();
		For(i,1,m)
		{
			int l1=read(),r1=read(),l2=read(),r2=read();
			Solve(l1,r1,l2,r2);
		}
	}
}
inline void Solve(int l1,int r1,int l2,int r2)
{
	int ans=0;
	For(i,1,n)  b[i]=1;
	For(i,1,n)  if(a[i]<l1)  b[i]=0;else if(a[i]>r1)  b[i]=2;
	For(ed,l2,n)
	{
		int len=0,now0=0,now2=0,tep=ed;
		len++;if(b[tep]==0) now0++;if(b[tep]==2)    now2++;
		if(ed-tep+1>=l2)
		{
				int mid=(len+1)/2;
				if(now0<mid&&now2<=len-mid)   ans++;
		}
		while(1)
		{
			tep--;len++;
			if(tep==0)  break;
			if(b[tep]==0) now0++;if(b[tep]==2)    now2++;
			if(ed-tep+1>r2) break;
			if(ed-tep+1>=l2)
			{
				int mid=(len+1)/2;
				if(now0<mid&&now2<=len-mid)   ans++;
			}
		}
	}
	writeln(ans);
}
signed main()
{
	freopen("x.in","r",stdin);freopen("x.ans","w",stdout);
	n=read();
	For(i,1,n)  a[i]=read();
	if(n>1000)  {sub1::Main();return 0;}
	m=read();
	For(i,1,m)
	{
		int l1=read(),r1=read(),l2=read(),r2=read();
		Solve(l1,r1,l2,r2);
	}
}
