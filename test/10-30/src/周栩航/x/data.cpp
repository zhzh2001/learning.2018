#include<bits/stdc++.h>
#define ll long long
#define For(i,j,k)  for(int i=j;i<=k;++i)
#define Dow(i,j,k)  for(int i=k;i>=j;--i)
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))  {if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))   t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0){x=-x;putchar('-');}if(x>=10)    write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}

const int N=233333;
int n,a[N],m;
int main()
{
	srand(time(0));
	freopen("x.in","w",stdout);
	n=100000;
	For(i,1,n)  a[i]=rand()*rand()%10000+1;
	cout<<n<<endl;
	For(i,1,n)  write_p(a[i]);puts("");
	m=5;
	cout<<m<<endl;
	For(i,1,m)
	{
		int l1=rand()*rand()%100000+1,r1=rand()*rand()%100000+1;
		if(l1>r1)   swap(l1,r1);
		int l2=rand()*rand()%n+1,r2=rand()*rand()%n+1;
		if(l2>r2)   swap(l2,r2);
		cout<<l1<<' '<<r1<<' '<<l2<<' '<<r2<<endl;
	}
}
