#include<bits/stdc++.h>
#define ll long long
#pragma GCC optimize 2
#define For(i,j,k)  for(register int i=j;i<=k;++i)
#define Dow(i,j,k)  for(register int i=k;i>=j;--i)
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))  {if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))   t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0){x=-x;putchar('-');}if(x>=10)    write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
//map<int,ll> vis;
set<ll> vis;
const int N=2e5;
ll a[N],v[N];
int n,num,ans;
inline void clear(int l,int r){For(i,l,r)  vis.erase(v[i]);}
int main()
{
	freopen("f.in","r",stdin);
	freopen("f.out","w",stdout);
	a[1]=1;a[0]=1;
	for(num=2;a[num-1]<=2000000000;++num)
		a[num]=a[num-1]+a[num-2];
	num--;

	n=read();
	For(i,1,n)  v[i]=read();
	int l=1,r=0;
	while(1)
	{
		++r;
		bool flag=1;
		if(r>n) break;
		Dow(i,1,num)
		if(a[i]>v[r])
		{
			if(vis.count(a[i]-v[r]))
			{
				ans++;clear(l,r-1);l=r;r--;
				flag=0;break;
			}
		}else   break;
		if(!flag)   continue;
		if(!vis.count(v[r]))	vis.insert(v[r]);
	}
	ans++;
	writeln(ans);
}
/*5
1 5 2 6  7
*/
