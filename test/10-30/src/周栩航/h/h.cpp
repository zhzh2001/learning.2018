#include<bits/stdc++.h>
#pragma GCC optimize 2
#define ll long long
#define For(i,j,k)  for(int i=j;i<=k;++i)
#define Dow(i,j,k)  for(int i=k;i>=j;--i)
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))  {if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))   t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0){x=-x;putchar('-');}if(x>=10)    write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}

priority_queue<ll> Q;
const int N=233;
ll n,mx,a[N],ans,siz;

int main()
{
	freopen("h.in","r",stdin);freopen("h.out","w",stdout);
	n=read();mx=read();
	For(i,1,n)  a[i]=read();
	Q.push(-1);
	while(!Q.empty())
	{
		ll now=Q.top();Q.pop();
		if(ans==-now)    continue;
		siz++;
		now=-now;
		ans=now;
		For(i,1,n)
			if(mx/a[i]+1>=now)   if(now*a[i]<=mx)	Q.push(-a[i]*now);
	}
	writeln(ans);
	writeln(siz);
}
