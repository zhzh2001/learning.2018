#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
#define file "f"
int read(){
	char c=getchar();
	int x=0,sgn=1;
	while(c<'0' || c>'9'){
		if (c=='-')
			sgn=-1;
		c=getchar();
	}
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*sgn;
}
const int maxn=110002;
int cnt[maxn],nums[maxn],cur;
int a[maxn];
int f[233],to;
int dp[maxn];
ll now;
void add(int x){
	for(int i=1;i<=to;i++){
		if (f[i]<x)
			continue;
		int w=f[i]-x;
		int p=lower_bound(nums+1,nums+cur+1,w)-nums;
		if (p<=cur && nums[p]==w)
			now+=cnt[p];
	}
	cnt[lower_bound(nums+1,nums+cur+1,x)-nums]++;
}
void del(int x){
	cnt[lower_bound(nums+1,nums+cur+1,x)-nums]--;
	for(int i=1;i<=to;i++){
		if (f[i]<x)
			continue;
		int w=f[i]-x;
		int p=lower_bound(nums+1,nums+cur+1,w)-nums;
		if (p<=cur && nums[p]==w)
			now-=cnt[p];
	}
}
int main(){
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	int n=read();
	for(int i=1;i<=n;i++)
		a[i]=nums[i]=read();
	sort(nums+1,nums+n+1);
	cur=unique(nums+1,nums+n+1)-nums-1;
	f[0]=f[1]=1;
	for(int i=2;;i++){
		if ((ll)f[i-1]+f[i-2]>2000000000)
			break;
		f[i]=f[i-1]+f[i-2];
		to=i;
	}
	int j=1;
	for(int i=1;i<=n;i++){
		add(a[i]);
		while(now>0)
			del(a[j++]);
		dp[i]=dp[j-1]+1;
	}
	printf("%d",dp[n]);
}
