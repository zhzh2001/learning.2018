#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
#define file "h"
ll read(){
	char c=getchar();
	ll x=0,sgn=1;
	while(c<'0' || c>'9'){
		if (c=='-')
			sgn=-1;
		c=getchar();
	}
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*sgn;
}
int n,a[233];
ll ans;
ll dfs(int u,ll now){
	if (u==n+1){
		ans++;
		return 1;
	}
	ll ans=1,qwq=1;
	while(now){
		ans=max(ans,qwq*dfs(u+1,now));
		qwq*=a[u];
		now/=a[u];
	}
	return ans;
}
int main(){
	freopen(file".in","r",stdin);
	freopen("bf.out","w",stdout);
	n=read();
	ll m=read();
	for(int i=1;i<=n;i++)
		a[i]=read();
	sort(a+1,a+n+1);
	printf("%lld\n",dfs(1,m));
	printf("%lld",ans);
}
