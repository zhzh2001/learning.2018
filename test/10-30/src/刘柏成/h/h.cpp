#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
#define file "h"
ll read(){
	char c=getchar();
	ll x=0,sgn=1;
	while(c<'0' || c>'9'){
		if (c=='-')
			sgn=-1;
		c=getchar();
	}
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*sgn;
}
int n,a[233];
int pre[27][1000002],mx[27][1000002];
ll ans;
ll dfs(int u,ll now){
	if (u==n+1){
		ans++;
		return 1;
	}
	if (now<=1000000){
		ans+=pre[u][now];
		return mx[u][now];
	}
	ll ans=1,qwq=1;
	while(now){
		ans=max(ans,qwq*dfs(u+1,now));
		now/=a[u];
		if (!now)
			break;
		qwq*=a[u];
	}
	return ans;
}
int main(){
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	n=read();
	ll m=read();
//	fprintf(stderr,"WTF\n");
	for(int i=1;i<=n;i++)
		a[i]=read();
	sort(a+1,a+n+1);
	reverse(a+1,a+n+1);
//	fprintf(stderr,"WTF\n");
	pre[n+1][1]=1;
	for(int i=n;i;i--){
		for(int j=1,now=a[i];j<=1000000;j++,now+=a[i]){
			pre[i][j]|=pre[i+1][j];
			if (pre[i][j] && now<=1000000)
				pre[i][now]=1;
		}
	}
//	fprintf(stderr,"WTF\n");
	for(int i=1;i<=n;i++)
		for(int j=1;j<=1000000;j++){
			mx[i][j]=pre[i][j]?j:mx[i][j-1];
			pre[i][j]+=pre[i][j-1];
		}
//	fprintf(stderr,"WTF\n");
	printf("%lld\n",dfs(1,m));
	printf("%lld",ans);
}
