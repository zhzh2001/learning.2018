#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
#define file "x"
int read(){
	char c=getchar();
	int x=0,sgn=1;
	while(c<'0' || c>'9'){
		if (c=='-')
			sgn=-1;
		c=getchar();
	}
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*sgn;
}
struct bit{
	int n;
	int a[200005];
	void init(int n){
		this->n=n;
		memset(a,0,sizeof(a));
	}
	void add(int p,int v){
		for(;p;p-=p&-p)
			a[p]+=v;
	}
	int query(int p){
		int ans=0;
		for(;p<=n;p+=p&-p)
			ans+=a[p];
		return ans;
	}
}b;
int n,sum[100002],a[100002];
ll solve(int x,int l,int r){
	sum[0]=n+1;
	for(int i=1;i<=n;i++){
		sum[i]=a[i]>x?1:-1;
		sum[i]+=sum[i-1];
	}
	b.init(2*n+1);
	ll ans=0;
	for(int i=1;i<=n;i++){ //[i-r,i-l]
		if (i>=l)
			b.add(sum[i-l],1);
		ans+=b.query(sum[i]); //>=sum[i]
		if (i>=r)
			b.add(sum[i-r],-1);
	}
	return ans;
}
int main(){
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)
		a[i]=read();
	int m=read();
	while(m--){
		int l1=read(),r1=read(),l2=read(),r2=read();
		printf("%lld\n",solve(r1,l2,r2)-solve(l1-1,l2,r2));
	}
}
