#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
using namespace std;
#define gc getchar
inline int read(){
	int x=0,f=1; char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc())
		if (ch=='-') f=0;
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return f?x:-x;
}
const int N=100005;
const int M=5000000;
int sz[M],ls[M],rs[M];
int rt[N],b[N],a[N],n,nd;
int l1,r1,l2,r2;
long long ans;
void insert(int &nk,int k,int l,int r,int p){
	nk=++nd; sz[nk]=sz[k]+1;
	ls[nk]=ls[k],rs[nk]=rs[k];
	if (l==r) return;
	int mid=(l+r)/2;
	if (p<=mid) insert(ls[nk],ls[k],l,mid,p);
	else insert(rs[nk],rs[k],mid+1,r,p);
}
int ask(int k,int l,int r,int p){
	if (p>r) return 0;
	if (!k||(l==r)) return sz[k];
	int mid=(l+r)/2;
	if (p>mid) return ask(rs[k],mid+1,r,p);
	return ask(ls[k],l,mid,p)+sz[rs[k]];
}
void solve_prob(int tp){
	b[0]=n;
	For(i,0,n) rt[i]=0;
	for (;nd;nd--) ls[nd]=rs[nd]=sz[nd]=0;
	int mn=n,mx=n;
	For(i,1,n) b[i]+=b[i-1];
	For(i,1,n) mn=min(mn,b[i]),mx=max(mx,b[i]);
	For(i,l2,n) insert(rt[i],rt[i-1],mn,mx,b[i]);
	For(i,1,n){
		int L=i+l2-1,R=min(i+r2-1,n);
		if (L>R) return;
		ans-=ask(rt[R]  ,mn,mx,b[i-1]+tp);
		ans+=ask(rt[L-1],mn,mx,b[i-1]+tp);
	}
}
void solve(){
	l1=read(); r1=read(); l2=read(); r2=read();
	ans=0;
	For(i,l2,r2) ans+=n-i+1;
	For(i,1,n) b[i]=(a[i]<l1?1:-1);
	solve_prob(0);
	For(i,1,n) b[i]=(a[i]>r1?1:-1);
	solve_prob(1);
	printf("%lld\n",ans);
}
int main(){
	freopen("x.in","r",stdin);
	freopen("x.out","w",stdout);
	n=read();
	For(i,1,n) a[i]=read();
	int T=read();
	while (T--) solve();
}
//rewrite
/*
5
4 1 2 2 5
5
2 4 2 3
2 4 1 3
2 5 2 5
3 5 2 5
1 5 3 5
*/
