#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
using namespace std;
inline int read(){
	#define gc getchar
	int x=0,f=1; char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc())
		if (ch=='-') f=0;
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return f?x:-x;
}
int rnd(){
	int x=0;
	For(i,1,9) x=x*10+rand()%10;
	return x;
}
int main(){
	freopen("x.in","w",stdout);
	const int n=100000;
	printf("%d\n",n);
	For(i,1,n) printf("%d ",rnd()%n+1);
	puts("");
	printf("%d\n",5);
	For(i,1,5){
		int l1=rnd()%n+1,r1=rnd()%n+1;
		int l2=rnd()%n+1,r2=rnd()%n+1;
		if (l1>r1) swap(l1,r1);
		if (l2>r2) swap(l2,r2);
		printf("%d %d %d %d\n",l1,r1,l2,r2);
	}
}
/*
5
4 1 2 2 5
5
2 4 2 3
2 4 1 3
2 5 2 5
3 5 2 5
1 5 3 5
*/
