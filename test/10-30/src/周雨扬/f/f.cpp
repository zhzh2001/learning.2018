#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
using namespace std;
inline int read(){
	#define gc getchar
	int x=0,f=1; char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc())
		if (ch=='-') f=0;
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return f?x:-x;
}
int fib[55];
set<int> S;
int n,a[100005];
int main(){
	freopen("f.in","r",stdin);
	freopen("f.out","w",stdout);
	fib[1]=1;
	For(i,2,46) fib[i]=fib[i-1]+fib[i-2];
	n=read(); int mx=0;
	For(i,1,n) mx=max(mx,a[i]=read());
	int ans=1; S.insert(a[1]);
	For(i,2,n){
		For(j,1,46)
			if (fib[j]>=a[i]&&a[i]+mx>=fib[j])
				if (S.find(fib[j]-a[i])!=S.end()){
					S.clear(); ans++;
					break;
				}
		S.insert(a[i]);
	}
	printf("%d\n",ans);
}
