#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
#define ll long long
using namespace std;
int n,pri[30];
ll R;
map<pair<ll,int>,ll> mp;
int times=0;
int f[1000005][30];
void init(){
	For(i,1,1000000) f[i][0]=1;
	For(j,1,n) For(i,1,1000000)
		f[i][j]=f[i][j-1]+f[i/pri[j]][j];
}
ll lastP(ll R){
	ll ans=(R!=0);
	for (R/=pri[1];R;R/=pri[1]) ans++;
	return ans;
}
ll calc(ll R,int n){
	if (!R) return 0;
	if (!n) return 1;
	if (mp.find(make_pair(R,n))!=mp.end())
		return mp[make_pair(R,n)];
	times++;
	return mp[make_pair(R,n)]=calc(R,n-1)+calc(R/pri[n],n);
}
int main(){
	freopen("h.in","r",stdin);
	freopen("bf.out","w",stdout);
	scanf("%d%lld",&n,&R);
	For(i,1,n) scanf("%d",&pri[i]);
	sort(pri+1,pri+n+1);
	init();
	printf("%lld",calc(R,n));
}
