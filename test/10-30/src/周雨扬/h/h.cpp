#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
#define ll long long
using namespace std;
const int N=10000005;
int n;
int A[50],B[50];
ll q[N],a[N],R;
int tp[N];
int main(){
	freopen("h.in","r",stdin);
	freopen("h.out","w",stdout);
	scanf("%d%lld",&n,&R);
	For(i,1,n){
		int x; scanf("%d",&x);
		if (x<=19) A[++*A]=x;
		else B[++*B]=x;
	}
	sort(A+1,A+*A+1);
	sort(B+1,B+*B+1);
	int h=0,t=1;
	q[1]=1; tp[1]=1;
	while (h!=t){
		ll x=q[++h];
		for (int i=tp[h];i<=*A&&R/A[i]>=x;i++)
			q[++t]=x*A[i],tp[t]=i;
	}
	*q=t; sort(q+1,q+*q+1);
	h=0,t=1;
	ll ans1=0,ans2=0;
	a[1]=1; tp[1]=1;
	while (h!=t){
		ll x=a[++h];
		int ans=upper_bound(q+1,q+*q+1,R/x)-q-1;
		ans1+=ans;
		ans2=max(ans2,x*q[ans]);
		for (int i=tp[h];i<=*B&&R/B[i]>=x;i++)
			a[++t]=x*B[i],tp[t]=i;
	}
	printf("%lld\n%lld",ans2,ans1);
}
