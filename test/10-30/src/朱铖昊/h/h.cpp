/*#include<bits/stdc++.h>
using namespace std;
#define ll long long
ll mx,ans;
int sum,n,a[30];
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void read(ll &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void dfs(int x,ll y)
{
	if (x>n)
	{
		++sum;
		ans=max(ans,y);
		//cout<<sum<<'\n';
		return;
	}
	ll p=y;
	while (mx/p>=a[x])
	{
		dfs(x+1,p);
		p*=a[x];
	}
	dfs(x+1,p);
}
int main()
{
	freopen("h.in","r",stdin);
	freopen("h.out","w",stdout);
	read(n);
	read(mx);
	for (int i=1;i<=n;++i)
		read(a[i]);
	sort(a+1,a+1+n,greater<int>());
	dfs(1,1);
	cout<<clock()<<'\n';
	cout<<ans<<'\n'<<sum;
	return 0;
}*/
#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define pi pair<ll,ll>
#define max(a,b) ((a)<(b)?(a)=(b):0)
const int YCL=2300000;
const int N=YCL+5,M=27;
int f[M][N],g[M][N];
int n,a[M];
//ll tmp=0;
ll mx;
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void read(ll &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline pi dfs(int x,ll y)
{
	//++tmp;
	//if (x==n+1)
	//	return (pi){1,1};
	if (y<=YCL)
		return (pi){f[x][y],g[x][y]};
	if (x==n)
	{
		ll p=1;
		int sum=0;
		while (1)
		{
			++sum;
			if (y/p>=a[x])
				p*=a[x];
			else
				break;
		}
		return (pi){sum,p};
	}
	ll ans=0,mx=0,p=1,q;
	pi z;
	while (1)
	{
		q=y/p;
		if (q>YCL)
		{
			z=dfs(x+1,y/p);
			ans+=z.first;
			max(mx,z.second*p);
		}
		else
		{
			ans+=f[x+1][q];
			max(mx,g[x+1][q]*p);
		}
		if (y/p>=a[x])
			p*=a[x];
		else
			break;
	}
	/*ll ans=0,mx=0,p=1;
	pi z;
	while (y)
	{
		if (y>YCL)
		{
			z=dfs(x+1,y);
			ans+=z.first;
			max(mx,z.second*p);
		}
		else
		{
			ans+=f[x+1][y];
			max(mx,g[x+1][y]*p);
		}
		p*=a[x];
		y/=a[x];
	}*/
	return (pi){ans,mx};
}
int main()
{
	freopen("h.in","r",stdin);
	freopen("h.out","w",stdout);
	read(n);
	read(mx);
	for (int i=1;i<=n;++i)
		read(a[i]);
	sort(a+1,a+1+n,greater<int>());
	f[n+1][1]=1;
	for (int i=n;i>=1;--i)
	{
		for (int j=1;j<=YCL;++j)
			f[i][j]=f[i+1][j];
		for (int j=1;j*a[i]<=YCL;++j)
			f[i][j*a[i]]+=f[i][j];
	}
	for (int i=1;i<=n;++i)
		for (int j=1;j<=YCL;++j)
		{
			g[i][j]=f[i][j]?j:g[i][j-1];
			f[i][j]+=f[i][j-1];
		}
	//cout<<clock()<<'\n';
	pi x=dfs(1,mx);
	//cout<<x.first<<'\n'<<x.second<<'\n';
	cout<<x.second<<'\n'<<x.first;
	//cout<<tmp<<'\n';
	//cout<<clock();
	return 0;
}
