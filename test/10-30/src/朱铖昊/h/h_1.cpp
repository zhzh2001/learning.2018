#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define pi pair<ll,ll>
const int YCL=1000000;
const int N=YCL+5,M=27;
int f[M][N],g[M][N];
int n,a[M];
ll mx;
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void read(ll &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline pi dfs(int x,ll y)
{
	if (x==n+1)
		return (pi){1,1};
	if (y<=YCL)
		return (pi){f[x][y],g[x][y]};
	ll ans=0,mx=0;
	ll p=1;
	while (1)
	{
		pi z=dfs(x+1,y/p);
		ans+=z.first;
		mx=max(z.second*p,mx);
		if (y/p>=a[x])
			p*=a[x];
		else
			break;
	}
	return (pi){ans,mx};
}
int main()
{
	freopen("h.in","r",stdin);
	freopen("h_1.out","w",stdout);
	read(n);
	read(mx);
	for (int i=1;i<=n;++i)
		read(a[i]);
	sort(a+1,a+1+n,greater<int>());
	f[n+1][1]=1;
	for (int i=n;i>=1;--i)
	{
		for (int j=1;j<=YCL;++j)
			f[i][j]=f[i+1][j];
		for (int j=1;j<=YCL/a[i];++j)
			f[i][j*a[i]]+=f[i][j];
	}
	for (int i=1;i<=n;++i)
		for (int j=1;j<=YCL;++j)
		{
			g[i][j]=f[i][j]?j:g[i][j-1];
			f[i][j]+=f[i][j-1];
		}
	pi x=dfs(1,mx);
	cout<<x.first<<'\n'<<x.second<<'\n';
	cout<<clock()<<'\n';
	return 0;
}
