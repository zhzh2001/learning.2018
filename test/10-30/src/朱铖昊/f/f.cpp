#include<bits/stdc++.h>
using namespace std;
const int N=100005;
map<int,int> mp;
int a[N],n,l,ans,p[N],m;
int f[1000];
inline void pre_work()
{
	f[0]=1;
	f[1]=1;
	m=1;
	while ((long long)f[m-1]+f[m]<=(long long)2e9)
	{
		f[m+1]=f[m-1]+f[m];
		++m;
	}
}
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
int main()
{
	freopen("f.in","r",stdin);
	freopen("f.out","w",stdout);
	pre_work();
	read(n);
	for (int i=1;i<=n;++i)
		read(a[i]);
	for (int i=1;i<=n;++i)
	{
		for (int j=1;j<=m;++j)
			if (a[i]<=f[j]&&mp.count(f[j]-a[i]))
				p[i]=max(p[i],mp[f[j]-a[i]]);
		mp[a[i]]=i;
	}
	ans=1;
	l=1;
	for (int i=2;i<=n;++i)
		if (p[i]>=l)
		{
			l=i;
			++ans;
		}
	cout<<ans;
	return 0;
}