#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int N=100005;
int a[N],f[N],g[N*2],n,m,l,r,x,y;
inline void read(int &x)
{
	char c=getchar();
	x=0;
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void add(int x,int y)
{
	for (;x<=2*n;x+=x&-x)
		g[x]+=y;
}
inline int ask(int x)
{
	int ans=0;
	for (;x;x-=x&-x)
		ans+=g[x];
	return ans;
}
inline ll work(int p,int x,int y)
{
	f[0]=n;
	for (int i=1;i<=n;++i)
		if (a[i]<=p)
			f[i]=f[i-1]+1;
		else
			f[i]=f[i-1]-1;
	for (int i=0;i<=n*2;++i)
		g[i]=0;
	ll ans=0;
	for (int i=x;i<=n;++i)
	{
		add(f[i-x],1);
		if (i>y)
			add(f[i-y-1],-1);
		ans+=ask(f[i]);
	}
	return ans;
}
int main()
{
	freopen("x.in","r",stdin);
	freopen("x.out","w",stdout);
	read(n);
	for (int i=1;i<=n;++i)
		read(a[i]);
	read(m);
	while (m--)
	{
		read(l);
		read(r);
		read(x);
		read(y);
		cout<<work(r,x,y)-work(l-1,x,y)<<'\n';
	}
	return 0;
}