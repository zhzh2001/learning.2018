#include <bits/stdc++.h>

#define Max 10005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

int n,m,l1,l2,r1,r2,ans,size,a[Max],num[Max];

inline bool check(int L,int R,int l,int r){
	size=0;
	for(int i=L;i<=R;i++)num[++size]=a[i];
	sort(num+1,num+size+1);
//	cout<<L<<" "<<R<<" "<<num[(size+1)/2]<<endl;
	if(num[(size+1)/2]<l)return false;
	if(num[(size+1)/2]>r)return false;
	return true;
}

int main(){
	freopen("x.in","r",stdin);
	freopen("1.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	m=read();
	for(int i=1;i<=m;i++){
		l1=read();r1=read();
		l2=read();r2=read();
		ans=0;
		for(int j=1;j<=n;j++){
			for(int k=l2;k<=r2;k++){
				if(j+k-1>n)break;
				if(check(j,j+k-1,l1,r1)){
					ans++;
				}
			}
		}
		writeln(ans);
	}
	return 0;
}
