#include <bits/stdc++.h>

#define lowbit(x) x&(-x)
#define Max 1005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

int n,m,sum,l1,l2,r1,r2,ans,a[Max],c[1000000];

inline void add(int x,int k){
	while(x<=1000000){
		c[x]+=k;
		x+=lowbit(x);
	}
	return;
}

inline int ask(int x){
	int ans=0;
	while(x){
		ans+=c[x];
		x-=lowbit(x);
	}
	return ans;
}

inline bool check(int l,int r,int num){
	int a=0,b=0;
	num=(num+1)/2;
	a=ask(l-1)+sum;
	b=ask(r)+sum;
	if(num>a&&num<=b)return true;
	return false;
}

int main(){
	freopen("x.in","r",stdin);
	freopen("x.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	m=read();
	for(int i=1;i<=m;i++){
		l1=read();r1=read();
		l2=read();r2=read();
		ans=0;
		for(int j=1;j<=n;j++){
			memset(c,0,sizeof c);
			sum=0;
			for(int k=j;k<=n;k++){
//				cout<<k<<endl;
				if(k-j+1>r2)break;
				if(!a[k])sum++;else add(a[k],1);
				if(k-j+1>=l2&&k-j+1<=r2){
					ans+=check(l1,r1,k-j+1);
				}
			}
		}
		writeln(ans);
	}
	return 0;
}
