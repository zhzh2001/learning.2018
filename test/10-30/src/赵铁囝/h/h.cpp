#include <bits/stdc++.h>

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

int k,r,ans,num,a[30];

inline bool check(int x){
	for(int i=1;i<=k;i++){
		while(x%a[i]==0){
			x=x/a[i];
			if(x==1)return true;
		}
	}
	if(x==1)return true;
	return false;
}

int main(){
	freopen("h.in","r",stdin);
	freopen("h.out","w",stdout);
	k=read();r=read();
	for(int i=1;i<=k;i++)a[i]=read();
	for(int i=1;i<=r;i++){
		if(check(i)){
			ans++;
			num=max(num,i);
		}
	}
	writeln(num);
	writeln(ans);
	return 0;
}
/*
1 2 3 4 6 7 8 9 12 14 16 18 21 24 27 28 
*/
