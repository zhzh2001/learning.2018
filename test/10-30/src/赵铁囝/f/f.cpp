#include <bits/stdc++.h>

#define Max 100005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

int n,l,ans,size,a[Max],num[Max];
map<int,int>vis;

inline int check(int x){
	for(int i=1;i<=size;i++){
		if(num[i]<x)continue;
		if(vis[num[i]-x])return true;
	}
	return false;
}

int main(){
	freopen("f.in","r",stdin);
	freopen("f.out","w",stdout);
	n=read();
	num[1]=1;
	num[2]=1;
	size=2;
	while(num[size-1]<=2e9-num[size]){
		num[size+1]=num[size]+num[size-1];
		size++;
	}
//	cout<<size<<endl;
	for(int i=1;i<=n;i++)a[i]=read();
	l=1;ans=1;
	vis[a[1]]++;
	for(int i=2;i<=n;i++){
		if(check(a[i])){
//			vis.clear();
			for(int j=l;j<i;j++)vis[a[j]]--;
			ans++;
			l=i;
		}
		vis[a[i]]++;
	}
	writeln(ans);
	return 0;
}
/*
1 1 2 3 5 8 13 21 34 55 89 144
*/
