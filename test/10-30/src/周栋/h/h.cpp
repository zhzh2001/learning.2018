#include <iostream>
#include <cstdio>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
ll n,R,p[30],Max,ans;
inline void dfs(ll x,ll now){
	if (now>R) return;
	if (!x){
		++ans;
		Max=max(Max,now);
		return;
	}
	do{
		dfs(x-1,now);
		if (now>R/p[x]) break;
		now*=p[x];
	}while (now<=R);
}
int main(){
	freopen("h.in","r",stdin);
	freopen("h.out","w",stdout);
	read(n),read(R);
	for (ll i=1;i<=n;++i) read(p[i]);
	dfs(n,1);
	wr(Max),puts(""),wr(ans);
	return 0;
}
//sxdakking
