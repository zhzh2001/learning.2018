#include <iostream>
#include <cstdio>
using namespace std;
typedef int ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll Max=1e9;
ll f[55],n,ans=1;
const ll P=2333333;
struct Ha{
	ll a[P],b[P],tot;
	bool vis[P];
	inline ll fid(ll x){
		ll k=x%P;
		for (;vis[k]&&a[k]!=x;k=(k+1)%P);
		return k;
	}
	inline bool count(ll x){return vis[fid(x)];}
	inline void clear(){
		for (ll i=tot;i;--i){
			ll tmp=fid(b[i]);
			vis[tmp]=0;
			a[tmp]=0;
		}tot=0;
	}
	inline void add(ll x){
		b[++tot]=x;
		ll tmp=fid(x);
		if (vis[tmp]) return;
		a[tmp]=x;
		vis[tmp]=1;
	}
}s;
int main(){
	freopen("f.in","r",stdin);
	freopen("f.out","w",stdout);
	f[0]=f[1]=1;
	for (ll i=2;i<=53;++i) f[i]=f[i-1]+f[i-2];
	read(n);
	for (ll i=1,x;i<=n;++i){
		read(x);
		if (s.count(x)) ++ans,s.clear();
		for (ll j=1;j<=53;++j) if (f[j]>x&&f[j]-x<=Max) s.add(f[j]-x);
	}
	wr(ans);
	return 0;
}
//sxdakking
/*
5
1 5 2 6 7
*/
