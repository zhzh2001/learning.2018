#include <iostream>
#include <cstdio>
#include <vector>
using namespace std;
typedef int ll;
#define rg register
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=1010;
ll n,m,a[100010],W[N][N],ans;
vector<ll>V;
inline void pre(){
	for (rg ll i=1;i<=n;++i){
		W[i][i]=a[i];
		V.clear();
		V.push_back(a[i]);
		for (rg ll j=i+1;j<=n;++j){
			V.insert(lower_bound(V.begin(),V.end(),a[j]),a[j]);
			W[i][j]=V[(j-i)/2];
		}
	}
}
int main(){
	freopen("x.in","r",stdin);
	freopen("x.out","w",stdout);
	read(n);
	for (rg ll i=1;i<=n;++i) read(a[i]);
	if (n<=1000){
		pre();
		read(m);
		for (rg ll t=1,l,r,L,R;t<=m;++t){
			read(l),read(r),read(L),read(R);
			ans=0;
			for (rg ll i=1;i<=n;++i)
				for (rg ll j=i+L-1;j<=n&&j-i+1<=R;++j)
					if (W[i][j]>=l&&W[i][j]<=r) ++ans;
			wr(ans),puts("");
		}
		return 0;
	}
	return 0;
}
//sxdakking
/*
5
4 1 2 2 5
5
2 4 2 3
2 4 1 3
2 5 2 5
3 5 2 5
1 5 3 5
*/
