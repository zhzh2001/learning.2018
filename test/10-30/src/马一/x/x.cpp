#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
using namespace std;
namespace Dango
{
	const int MAXN=100005;
	struct tree
	{
		int lt,rt,l,r,data;
	}t[MAXN*21];
	int n,m,num[MAXN],tmp[MAXN],q,l1,r1,l2,r2,root[MAXN],cnt;
	int read()
	{
		int x=0,f=0;
		char ch=getchar();
		while(!isdigit(ch)){f|=(ch=='-');ch=getchar();}
		while(isdigit(ch)){x=x*10+ch-'0';ch=getchar();}
		return f?-x:x;
	}
	int build(int l,int r)
	{
		int rt=++cnt;
		t[rt].l=l;
		t[rt].r=r;
		if(l!=r)
		{
			int mid=(l+r)>>1;
			t[rt].lt=build(l,mid);
			t[rt].rt=build(mid+1,r);
		}
		return rt;
	}
	int add(int last,int pos)
	{
		int rt=++cnt;
		t[rt]=t[last];
		t[rt].data++;
		if(t[rt].l!=t[rt].r)
		{
			int mid=(t[rt].l+t[rt].r)>>1;
			pos<=mid?t[rt].lt=add(t[last].lt,pos):t[rt].rt=add(t[last].rt,pos);
		}
		return rt;
	}
	int query(int lt,int rt,int x)
	{
		if(t[lt].l==t[lt].r)return t[lt].l;
		int mid=t[t[rt].lt].data-t[t[lt].lt].data;
		return mid>=x?query(t[lt].lt,t[rt].lt,x):query(t[lt].rt,t[rt].rt,x-mid);
	}
	int work()
	{
		n=read();
		for(int i=1;i<=n;i++)
		{
			num[i]=read();
			tmp[i]=num[i];
		}
		sort(tmp+1,tmp+n+1);
		q=unique(tmp+1,tmp+n+1)-tmp-1;
		root[0]=build(1,q);
		for(int i=1;i<=n;i++)
		{
			int x=lower_bound(tmp+1,tmp+q+1,num[i])-tmp;
			root[i]=add(root[i-1],x);
		}
		m=read();
		for(int i=1;i<=m;i++)
		{
			l1=read();r1=read();l2=read();r2=read();
			long long ans=0;
			for(int j=1;j<=n-l2+1;j++)
				for(int k=j+l2-1;k<=min(j+r2-1,n);k++)
				{
					int x=tmp[query(root[j-1],root[k],((k-j)>>1)+1)];
					if(x>=l1&&x<=r1)ans++;
				}
			printf("%lld\n",ans);
		}
		return 0;
	}
}
int main()
{
	freopen("x.in","r",stdin);
	freopen("x.out","w",stdout);
	return Dango::work();
}
