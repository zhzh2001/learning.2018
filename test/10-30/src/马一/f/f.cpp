#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<map>
using namespace std;
namespace Dango
{
	const int MAXN=100005;
	map<long long,bool> mapl;
	int n,a[MAXN],dp[MAXN],mx;
	int read()
	{
		int x=0;
		char ch=getchar();
		while(!isdigit(ch))ch=getchar();
		while(isdigit(ch)){x=x*10+ch-'0';ch=getchar();}
		return x;
	}
	void init(long long a)
	{
		long long x=0,y=1,z=1;
		while(z<=a){x=y;y=z;z=x+y;mapl[z]=true;}
	}
	int work()
	{
		n=read();
		for(int i=1;i<=n;i++)
		{
			a[i]=read();
			mx=max(mx,a[i]);
		}
		mx*=2;
		init(mx);
		int tmp=1;
		for(int i=1;i<=n;i++)
		{
			int j;
			for(j=i-1;j>=tmp;j--)
				if(mapl[a[i]+a[j]])break;
			dp[i]=dp[j]+1;
			tmp=j+1;
		}
		printf("%d",dp[n]);
		return 0;
	}
}
int main()
{
	freopen("f.in","r",stdin);
	freopen("f.out","w",stdout);
	return Dango::work();
}
