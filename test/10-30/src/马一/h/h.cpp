#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
using namespace std;
namespace Dango
{
	const int MAXN=35;
	int k,p[MAXN];
	long long r,ans,mx,tmp=1;
	long long pow_(long long a,long long b)
	{
		long long result=1;
		while(b)
		{
			if(b&1)result*=a;
			a*=a;
			b>>=1;
		}
		return result;
	}
	void dfs(int a)
	{
		if(a>k)
		{
			ans++;
			mx=max(mx,tmp);
			return;
		}
		int i;
		dfs(a+1);
		for(i=1;;i++)
		{
			tmp*=p[a];
			if(tmp>r)break;
			dfs(a+1);
		}
		tmp/=pow_(p[a],i);
	}
	int work()
	{
		ios::sync_with_stdio(false);
		cin>>k>>r;
		for(int i=1;i<=k;i++)
			cin>>p[i];
		dfs(1);
		cout<<mx<<"\n"<<ans;
		return 0;
	}
}
int main()
{
	freopen("h.in","r",stdin);
	freopen("h.out","w",stdout);
	return Dango::work();
}
