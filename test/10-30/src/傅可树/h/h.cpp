#pragma GCC optimize ("O2")
#include<bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int M=20000000,N=30,INF=1e13;
int k,R,mx,prime[N],ans;
inline void init(){
	k=read(); R=read();
	for (int i=1;i<=k;i++){
		prime[i]=read();	
	} 
}
int q[M],tot,lim;
void dfs(int now,int s){
	if (now==lim+1){
		q[++tot]=s;
		return;
	}
	dfs(now+1,s);
	while (s<=R/prime[now]) s*=prime[now],dfs(now+1,s);
}
inline int find(int x){
	int l=1,r=tot;
	while (l<r){
		int mid=(l+r+1)>>1;
		if (q[mid]<=R/x) l=mid; 
			else r=mid-1;
	}
	return l;
}
void Dfs(int now,int s){
	if (now==k+1){
		int tmp=find(s);
		mx=max(mx,s*q[tmp]); ans+=tmp;
		return;
	}
	Dfs(now+1,s);
	while (s<=R/prime[now]) s*=prime[now],Dfs(now+1,s);
}
inline bool cmp(int x,int y){
	return x>y;
}
inline void solve(){
	sort(prime+1,prime+1+k,cmp);
	lim=k*5/7;
	dfs(1,1); 
	sort(q+1,q+1+tot); 
	Dfs(lim+1,1);
	writeln(mx); writeln(ans);
}
signed main(){
	freopen("h.in","r",stdin);
	freopen("h.out","w",stdout);
	init(); solve();
	return 0;
}
