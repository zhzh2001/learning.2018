#pragma GCC optimize ("O2")
#include<bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=30;
int k,R,mx,prime[N],ans;
inline void init(){
	k=read(); R=read();
	for (int i=1;i<=k;i++){
		prime[i]=read();	
	} 
}
inline int Pow(int x,int k){
	int y=1;
	for (;k;k>>=1,x=x*x) if (k&1) y=y*x;
	return y;
}
void dfs(int now,int s){
	if (now==k){
		int tmp=R/s,res=log(tmp)/log(prime[now]);
		mx=max(mx,s*Pow(prime[now],res)); ans+=res+1;
		return;
	}
	dfs(now+1,s);
	while (s*prime[now]<=R) s*=prime[now],dfs(now+1,s);
}
inline void solve(){
	dfs(1,1); 
	writeln(mx); writeln(ans);
}
signed main(){
	init(); solve();
	return 0;
}
