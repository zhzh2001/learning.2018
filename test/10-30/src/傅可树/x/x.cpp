#include<bits/stdc++.h>
#define pa pair<int,int>
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=1005;
int ans,n,a[N];
inline void init(){
	n=read();
	for (int i=1;i<=n;i++) a[i]=read();
}
priority_queue<pa, vector<pa>, greater<pa> >heap;
inline void solve(){
	int q=read();
	while (q--){
		ans=0;
		int l1=read(),r1=read(),l2=read(),r2=read();
		for (int i=1;i<=n;i++){
			int sz=0;
			while (!heap.empty()) heap.pop();
			for (int j=i;j<=n;j++){
				sz++;
				int len=(j-i+1),mid=(len+1)/2;
				heap.push(make_pair(a[j],j));
				while (sz>mid) heap.pop(),sz--;
				int u=heap.top().second; int L=u-i+1;
				if (q==2&&L>=l2&&L<=r2&&u>=l1&&u<=r1) {
					ans++;
				}
			}
		}
		writeln(ans);
	}
}
int main(){
	init(); solve();
	return 0;
}
