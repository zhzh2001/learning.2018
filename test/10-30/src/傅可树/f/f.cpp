#include<cstdio>
#include<map>
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=1e5+5;
int n,a[N];
inline void init(){
	n=read();
	for (int i=1;i<=n;i++){
		a[i]=read();
	}
}
map<int,int>mp;
int f[50],ans;
inline void solve(){
	f[1]=f[2]=1;
	for (int i=3;i<=41;i++){
		f[i]=f[i-1]+f[i-2];
	}
	for (int i=1;i<=n;i++){
		bool flag=1;
		for (int j=2;j<=41;j++) {
			if (mp[f[j]-a[i]]) {
				flag=0;
				break;
			}
		} 
		if (!flag){
			mp.clear();
			ans++;
		}
		mp[a[i]]++;
	}
	ans++;
	writeln(ans);
}
signed main(){
	freopen("f.in","r",stdin); freopen("f.out","w",stdout);
	init(); solve();
	return 0;
}
