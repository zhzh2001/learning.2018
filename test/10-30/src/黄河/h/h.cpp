#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<ctime>
#include<cstdlib>
#include<string>
#include<queue>
#include<cmath>
#include<set>
#include<map>
#include<bitset>
#include<vector>
#define re register
#define rep(x,a,b) for (re int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (re int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (re int x=hd[a];~x;x=nx[x])
#define LL long long
#define N 5000010
#define p 998244353
#define inf 0x3f3f3f3f
using namespace std;
inline LL read(){
	LL x=0;int ch=getchar(),f=1;
	while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
	if (ch=='-'){f=-1;ch=getchar();}
	while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
	return x*f;
}
int n,k,Id[N];
LL m,a[30],ans[N];
int main(){
   freopen("h.in","r",stdin);
   freopen("h.out","w",stdout);
   n=read(),m=read();
   rep(i,1,n) a[i]=read();
   k=1;ans[1]=1;Id[1]=1;
   while(1){
      LL s=inf;
		int flag=-1;
      rep(i,1,k)
        if (Id[i]<=n){
           LL sum=ans[i]*a[Id[i]];
           if (sum==ans[k]) Id[i]++;
           if (Id[i]>n) continue;
           sum=ans[i]*a[Id[i]];
           if (sum<=m) if (s>sum) s=sum,flag=i;
		  }
	  if (flag==-1) break;
	  k++;ans[k]=s;
	  Id[k]=1;Id[flag]++;
	}
   printf("%lld\n",ans[k]);
   printf("%d\n",k);
   return 0;
}
