#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<ctime>
#include<cstdlib>
#include<string>
#include<queue>
#include<cmath>
#include<set>
#include<map>
#include<bitset>
#include<vector>
#define re register
#define rep(x,a,b) for (re int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (re int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (re int x=hd[a];~x;x=nx[x])
#define LL long long
#define N 100010
#define p 998244353
using namespace std;
inline LL read(){
	LL x=0;int ch=getchar(),f=1;
	while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
	if (ch=='-'){f=-1;ch=getchar();}
	while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
	return x*f;
}
set<int> S;
int n,a[N],f[50];
int main(){
	freopen("f.in","r",stdin);
	freopen("f.out","w",stdout);
	n=read();
	rep(i,1,n) a[i]=read();
	f[1]=1;f[2]=1;
	rep(i,3,46) f[i]=f[i-1]+f[i-2];
//	printf("%d\n",f[46]);
	int ans=0;
	rep(i,1,n){
		drp(j,46,2){
			if (f[j]<a[i]) break;
			if (S.count(f[j]-a[i])) {
				ans++;
				S.erase(S.begin(),S.end());
				//printf("%d ",i);
				break;
			}
		}
		S.insert(a[i]);
	}
	printf("%d",ans+1);
	return 0;
}
