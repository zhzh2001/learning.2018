#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<ctime>
#include<cstdlib>
#include<string>
#include<queue>
#include<cmath>
#include<set>
#include<map>
#include<bitset>
#include<vector>
#define re register
#define rep(x,a,b) for (re int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (re int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (re int x=hd[a];~x;x=nx[x])
#define LL long long
#define N 200005
#define p 998244353
using namespace std;
inline LL read(){
	LL x=0;int ch=getchar(),f=1;
	while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
	if (ch=='-'){f=-1;ch=getchar();}
	while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
	return x*f;
}
int n,m,T,a[N],c[N],t[N];
LL ans;
inline int lowbit(int x) {return (x&(-x));}
void Add(int x, int y){
	while (x<=2*n+1) t[x]+=y,x+=lowbit(x);
}
int Query(int x){
	int s=0;
	while (x) s+=t[x],x-=lowbit(x);
	return s;
}
inline LL Solve(int x, int y){ 
	c[0]=n+1;
	rep(i,1,n)
		if (a[i]<=x) c[i]=c[i-1]+1;else c[i]=c[i-1]-1;
	memset(t,0,sizeof(t));
	LL res=0;
	if (y) Add(c[0],1);
	rep(i,1,n){
		res+=Query(c[i]);
		Add(c[i],1);
		if (i-y>=0) Add(c[i-y],-1);
	}
	return res;
}
int main(){
	freopen("x.in", "r", stdin);
	freopen("x.out", "w", stdout);
	n=read();
	rep(i,1,n)a[i]=read();
	T=read();
	while (T--){
		int l1=read()-1,r1=read(),l2 =read()-1,r2=read();
		//printf("%d\n",Solve(r1, r2)-Solve(r1, l2) -Solve(l1, r2)+Solve(l1, l2));
		ans = (Solve(r1,r2)-Solve(r1,l2))-(Solve(l1,r2)-Solve(l1,l2));
		printf("%lld\n", ans);
	}
}
