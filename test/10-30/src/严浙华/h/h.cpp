#include<cstdio>
#define maxn 30
#define ll long long
int k,r,ka[maxn],ans;
void read(int &x){
    int f=1;
    x=0;
    char s=getchar();
    while(s<'0'||s>'9'){if(s=='-') f=-1;s=getchar();}
    while(s>='0'&&s<='9'){x=x*10+s-'0';s=getchar();}
    x*=f;
}
void write(int x){
    if(x<0){putchar('-');x=-x;}
    if(x>9) write(x/10);
    putchar(x%10+'0');
}
int main(){
	freopen("h.in","r",stdin);
	freopen("h.out","w",stdout);
	read(k),read(r);
	for(int i = 1;i <= k;i++)
		read(ka[i]);
	for(int i = r;i > 1;i--){
		int w = i;
		for(int j = 1;j <= k;j++){
			while(w % ka[j] == 0)
				w /= ka[j];
		}
		if(w == 1 && ans == 0){
			write(i);
			printf("\n");
		}
		if(w == 1){
			ans++;
		}
	}
	write(ans + 1);
}
