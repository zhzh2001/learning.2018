#include<cstdio>
#include<algorithm>
#define maxn 100010
using namespace std;
int n,m,a[maxn],b[maxn],l1,r1,l2,r2,ans;
void read(int &x){
    int f=1;
    x=0;
    char s=getchar();
    while(s<'0'||s>'9'){if(s=='-') f=-1;s=getchar();}
    while(s>='0'&&s<='9'){x=x*10+s-'0';s=getchar();}
    x*=f;
}
void write(int x){
    if(x<0){putchar('-');x=-x;}
    if(x>9) write(x/10);
    putchar(x%10+'0');
}
bool mids(int l,int h,int w,int q){
	int len = h - l + 1;
	int mid = (len + 1) / 2;
	int zws = l + mid - 1;
	for(int z = l;z <= h;z++)
		b[z] = a[z];
	sort(b + l,b + h + 1);
//	for(int i = l;i <= h;i++)
//		cout<<b[i]<<' ';
//	cout<<endl;
//	cout<<len<<' '<<mid<<' '<<b[zws]<<endl;
	if(b[zws] >= w && b[zws] <= q)
		return true;
	else return false;
}
void query(int g,int x,int r,int p){
	for(int i = 1;i <= n - r + 1;i++)
		for(int j = i + r - 1;j <= i + p - 1 && j <= n;j++){
		//	cout<<i<<' '<<j<<' '<<g<<' '<<x<<endl;
			if(mids(i,j,g,x))
				ans++;
		}
	write(ans);
	printf("\n");
}
int main(){
	freopen("x.in","r",stdin);
	freopen("x.out","w",stdout);
	read(n);
	for(int i = 1;i <= n;i++)
		read(a[i]);
	read(m);
	for(int i = 1;i <= m;i++){
		ans = 0;
		read(l1),read(r1),read(l2),read(r2);
		query(l1,r1,l2,r2);
	}
	return 0;
}
