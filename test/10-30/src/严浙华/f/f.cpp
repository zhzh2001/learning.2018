#include<cstdio>
#define maxn 100000 + 10
int n,a[maxn],ans,j;
struct jihe{
	int r,l;
}f[maxn];
int ex[46] = {0,1,2,3,5,8,13,21,34,55,89,144,233,377,610,987,1597,2584,4181,6765,10946,17711,28657,46368,75025,121393,196418,317811,514229,832040,1346269,2178309,3524578,5702887,9227465,14930352,24157817,39088169,63245986,102334155,165580141,267914296,433494437,701408733,1134903170,1836311903};
void read(int &x){
    int f=1;
    x=0;
    char s=getchar();
    while(s<'0'||s>'9'){if(s=='-') f=-1;s=getchar();}
    while(s>='0'&&s<='9'){x=x*10+s-'0';s=getchar();}
    x*=f;
}
void write(int x){
    if(x<0){putchar('-');x=-x;}
    if(x>9) write(x/10);
    putchar(x%10+'0');
}
bool fbnq(int w){
	for(int i = 1;i <= 45;i++)
		if(w == ex[i])
			return true;
	return false;
}
bool pd(int x,jihe y){
	int lrange = y.l,rrange = y.r;
	for(int i = lrange;i <= rrange;i++){
		if(fbnq(x + a[i]))
			return false;
	}
	return true;
}
int main(){
	freopen("f.in","r",stdin);
	freopen("f.out","w",stdout);
	read(n);
	for(int i = 1;i <= n;i++)
		read(a[i]);
	j = 1;
	ans = 1;
	f[j].l = 1;
	f[j].r = 1;
	for(int i = 2;i <= n;i++){
		if(pd(a[i],f[j]))
			f[j].r++;
		else{
			j++;
			f[j].l = i;
			f[j].r = i;
			ans++;
		}
	}
	write(ans);
}
