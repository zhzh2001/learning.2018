#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
void setIO() {
	freopen("f.in","r",stdin);
	freopen("f.out","w",stdout);
}
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}
while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
const int N = 110000;
int a[N];
int n;
int b[N];
multiset<int>c;
multiset<int>v;
multiset<int>::iterator it;
bool pd(int x) {
	it = c.upper_bound(x);
	if(it != c.end()) {
		if(v.count(*it - x)) return 0;
		++it;
		if(it != c.end() && v.count(*it - x)) return 0;
		++it;
		if(it != c.end() && v.count(*it - x)) return 0;
	}
	return 1;
}
int solve() {
	int res = 1;
	for(int i = 1; i <= n; ++i) {
		if(!pd(a[i])) {
			v.clear();
			++res;
		}
		v.insert(a[i]);
	}
	return res;
}
int main() {
	setIO();
	n = read();
	for(int i = 1; i <= n; ++i) a[i] = read();
	b[1] = 1; b[2] = 1;
	for(int i = 3; i <= 46; ++i) b[i] = b[i - 1] + b[i - 2];
	for(int i = 1; i <= 46; ++i) c.insert(b[i]);
	writeln(solve());
	return 0;
}
