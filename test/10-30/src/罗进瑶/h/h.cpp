#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
void setIO() {
	freopen("h.in","r",stdin);
	freopen("h.out","w",stdout);
}
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}
while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
const int N = 30;
int n;
ll R;
int a[N];
ll mx, ct;
void dfs(int x, ll num) {
	if(x == n + 1) {
		++ct;
		mx = max(mx, num);
		return;
	}
	dfs(x + 1, num);
	while(num <= R / a[x]) {
		num *= a[x];
		dfs(x + 1, num);
	}
}
int main() {
	setIO();
	n = read(), R = read();
	for(int i = 1; i <= n; ++i)
		a[i] = read();
	dfs(1, 1);
	writeln(mx);
	writeln(ct);
	return 0;
}
