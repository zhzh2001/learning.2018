#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
void setIO() {
	freopen("x.in","r",stdin);
	freopen("x.out","w",stdout);
}
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}
while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
const int N = 111111;
int a[N], n, m;
int b[N];
int l1, r1, l2, r2;

ll solve() {
	ll res = 0;
	for(register int i = 1; i <= n; ++i) {
		priority_queue<int>p;
		priority_queue<int, vector<int>, greater<int> >q;		
		for(register int j = i; j <= n && j <= i + r2 - 1; ++j) {
			p.push(a[j]);
			while(p.size() > q.size()) q.push(p.top()), p.pop();
			while(p.size() < q.size()) p.push(q.top()), q.pop();
			if(j - i + 1 >= l2 && p.top() >= l1 && p.top() <= r1) ++res;
		}
	}
	return res;
}
int main() {
	setIO();
	n = read();
	if(n > 1000) {
		puts("0");
		return 0; 
	}
	for(int i = 1; i <= n; ++i) a[i] = read();
	m = read();
	while(m--) {
		l1 = read(), r1 = read(), l2 = read(), r2 = read();
		writeln(solve());
	}
	return 0;
}

