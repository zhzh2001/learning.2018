#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
void setIO() {
	freopen("x.in","r",stdin);
	freopen("x.out","w",stdout);
}
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}
while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
const int N = 111111;
int a[N], n, m;
int b[N], c[N];
int l1, r1, l2, r2;
void add(int x, int v) {
	for(; x <= 100000; x += x & -x)
		c[x] += v;
}
int ask(int x) {
	int res = 0;
	for(; x; x -= x & -x)
		res += c[x];
	return res;
} 
int Find(int x) {
	int l = 0, r = 100000;
	while(r - l > 1) {
		int mid = (l + r) >> 1;
		if(ask(mid) >= x) r = mid;
		else l = mid;
	}
	return r;
}
ll solve() {
	ll res = 0;
	for(register int i = 1; i <= n; ++i) {
		for(register int j = i; j <= n && j <= i + r2 - 1; ++j) {
			add(a[j], 1);
			if(j - i + 1 >= l2) {
				int t = Find((j - i + 2) / 2);
				if(t >= l1 && t <= r1) ++res;
			}
		}
		for(int j = 1; j <= n; ++j) c[a[j]] = 0;
	}
	return res;
}
int main() {
	setIO();
	n = read();
	if(n > 1000) {
		puts("0");
		return 0; 
	}
	for(int i = 1; i <= n; ++i) a[i] = read();
	m = read();
	while(m--) {
		l1 = read(), r1 = read(), l2 = read(), r2 = read();
		writeln(solve());
	}
	return 0;
}

