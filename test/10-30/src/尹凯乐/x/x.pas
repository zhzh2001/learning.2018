program xp;
 var
  a,b,c:array[0..100001] of longint;
  i,j,k,m,n,x,xc,y,yc:longint;
  ssum:int64;
 begin
  assign(input,'x.in');
  assign(output,'x.out');
  reset(input);
  rewrite(output);
  readln(n);
  for i:=1 to n do
   read(A[i]);
  readln;
  readln(m);
  for i:=1 to m do
   begin
    readln(x,y,xc,yc);
    for j:=1 to n do
     if a[j]<x then b[j]:=-1
               else if a[j]>y then b[j]:=1
                              else b[j]:=0;
    c[0]:=0;
    for k:=1 to n do
     if b[k]=0 then c[k]:=c[k-1]+1
               else c[k]:=c[k-1];
    b[0]:=0;
    for j:=1 to n do
     inc(b[j],b[j-1]);
    ssum:=0;
    for j:=1 to n do
     for k:=j-yc to j-xc do
      if k>=0 then
       if (b[j]-b[k]>=0) and (b[j]-b[k]<=c[j]-c[k]) or (b[j]-b[k]<=0) and (b[k]-b[j]<c[j]-c[k]) then inc(ssum);
    writeln(ssum);
   end;
  close(input);
  close(output);
 end.
