#include <cstdio>
#include <algorithm>
using namespace std;
#define ll long long
ll K,R,ans1,ans2;
ll a[100];
void dfs(int x,ll S) {
	if (x>K) {
		ans2++;
		ans1=max(ans1,S);
		return;
	}
	dfs(x+1,S);
	for (;R/S>=a[x];) {
		S*=a[x];
		dfs(x+1,S);
	}
}
int main() {
	freopen("h.in","r",stdin);
	freopen("h.out","w",stdout);
	scanf("%lld%lld",&K,&R);
	for (int i=1;i<=K;i++) scanf("%lld",&a[i]);
	dfs(1,1);
	printf("%lld\n%lld\n",ans1,ans2);
}
