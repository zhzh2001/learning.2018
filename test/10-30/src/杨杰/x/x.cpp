#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
const int N=1e5+7;
int n,m;
int a[N],v[N];
struct tree {
	int mn,mx,flg;
}tr[N<<2];
#define dd c=getchar()
int read() {int s=0,w=1;char c;while (dd,c>'9' || c<'0') if (c=='-') w=-1;while (c>='0' && c<='9') s=s*10+c-'0',dd;return s*w;}
#undef dd
#define ls (ts<<1)
#define rs (ts<<1|1)
void pushup(int ts) {tr[ts].mx=max(tr[ls].mx,tr[rs].mx);tr[ts].mn=min(tr[ls].mn,tr[rs].mn);}
void Ad(int ts,int v) {tr[ts].mn+=v;tr[ts].mx+=v;tr[ts].flg+=v;}
void pushdown(int ts) {
	int v=tr[ts].flg;tr[ts].flg=0;
	Ad(ls,v);Ad(rs,v);
}
void build(int ts,int l,int r) {
	tr[ts].flg=0;
	if (l==r) {
		tr[ts].mn=tr[ts].mx=1;
		return;
	}
	int mid=(l+r)>>1;
	build(ls,l,mid);build(rs,mid+1,r);
	pushup(ts);
}
void add(int ts,int l,int r,int L,int R,int v) {
	if (l==L && r==R) {
		Ad(ts,v);
		return;
	}
	int mid=(l+r)>>1;
	pushdown(ts);
	if (R<=mid) add(ls,l,mid,L,R,v);
	else if (L>mid) add(rs,mid+1,r,L,R,v);
	else add(ls,l,mid,L,mid,v),add(rs,mid+1,r,mid+1,R,v);
	pushup(ts);
}
int query(int ts,int l,int r,int L,int R) {
//	printf("%d %d %d %d\n",l,r,tr[ts].mn,tr[ts].mx);
	if (l==L && r==R) {
		if (tr[ts].mn>0) return 0;
		if (tr[ts].mx<=0) return r-l+1;
	}
	int mid=(l+r)>>1;
	pushdown(ts);
	if (R<=mid) return query(ls,l,mid,L,R);
	else if (L>mid) return query(rs,mid+1,r,L,R);
	else return query(ls,l,mid,L,mid)+query(rs,mid+1,r,mid+1,R);
}
int solve(int x,int y) {
	int ans=0;memset(v,0,sizeof v);
	build(1,1,n);
	for (int i=1;i<=n;i++) {
		if (a[i]>y) add(1,1,n,max(i-x+1,1),i,-2);
		add(1,1,n,max(i-x+1,1),i,1);
		ans+=query(1,1,n,max(i-x+1,1),i);
//		if (a[i]>y) for (int j=max(1,i-x+1);j<=i;j++) v[j]++;
//		for (int j=max(i-x+1,1);j<=i;j++) {
//			if (v[j]<=(i-j+1)/2) ans++;
//		}
	}
//	printf("ans=%d\n",ans);
	return ans;
}
int solve1(int x,int l,int r) {
	if (!x) return 0;
	return solve(x,l-1)-solve(x,r);
}
int main() {
	freopen("x.in","r",stdin);
	freopen("x.out","w",stdout);
	n=read();
	for (int i=1;i<=n;i++) a[i]=read();
	m=read();
	for (int i=1,l1,l2,r1,r2;i<=m;i++) {
		l1=read(),r1=read(),l2=read(),r2=read();
		printf("%d\n",solve1(r2,l1,r1)-solve1(l2-1,l1,r1));
	}
	return 0;
}
