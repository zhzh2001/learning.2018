#include <cstdio>
#include <cstring>
#include <map>
#include <algorithm>
using namespace std;
#define ll long long
const int N=1e5+7;
int n;
int f[N],g[N],a[N];
ll Fb[N];
map<int,int>mp,now;
void per() {
	Fb[0]=1;Fb[1]=1;mp[1]=1;
	for (int i=2;i<=45;i++) Fb[i]=Fb[i-1]+Fb[i-2],mp[Fb[i]]=1;
}
int main() {
	freopen("f.in","r",stdin);
	freopen("f.out","w",stdout);
	per();
	scanf("%d",&n);
	for (int i=1;i<=n;i++) scanf("%d",&a[i]);
	memset(f,60,sizeof f);
	f[0]=0;f[1]=g[1]=1;now[a[1]]=1;
	for (int i=2;i<=n;i++) {
		int p=0;
		for (int j=45;j>=1 && Fb[j]>a[i];j--) if (now.count(Fb[j]-a[i])) {p=1;now.clear();break;}
		if (p) for (int j=i-1;j>=g[i-1];j--) {
			if (mp.count(a[i]+a[j])) {g[i]=j+1;break;}
			now[a[j]]=1;
		}
		else g[i]=g[i-1];
		now[a[i]]=1;
		f[i]=f[g[i]-1]+1;
	}
	printf("%d",f[n]);
}
