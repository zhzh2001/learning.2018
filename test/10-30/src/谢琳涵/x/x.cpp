#include<bits/stdc++.h>
#define ll long long
using namespace std;
ll n,Q,a[500001],b[500001],f[500001],mx=200001;
ll lowbit(ll x){
	return x&(-x);
}
ll query(ll x){
	ll i,now=0;
	for(i=x;i;i-=lowbit(i))now+=f[i];
	return now;
}
void ins(ll x,ll t){
	ll i;
	for(i=x;i<=mx;i+=lowbit(i))f[i]+=t;
}
int main(){
freopen("x.in","r",stdin);
freopen("x.out","w",stdout);	
	ll i,l1,r1,l2,r2,l,r,sum1,sum2,sum3;
	scanf("%lld",&n);
	for(i=1;i<=n;i++)scanf("%lld",&a[i]);
	scanf("%lld",&Q);
	while(Q--){
		sum1=sum2=sum3=0ll;
		scanf("%lld%lld%lld%lld",&l1,&r1,&l2,&r2);
		//中位数小于l1
		for(i=1;i<=n;i++)
		 if(a[i]<l1)b[i]=-1;
		  else b[i]=1;
		l=1;r=0;
		memset(f,0,sizeof(f));
		for(i=1;i<=n;i++){
			b[i]+=b[i-1];
			while(i-r+1>l2){
				ins(b[r]+100001,1);r++;
			}
			while(i-l+1>r2){
				ins(b[l-1]+100001,-1);l++;
			}
			sum1+=query(b[i]-1+100001);
			//if(Q==3)printf("%lld %lld %lld %lld\n",i,l,r,sum1);
		}
		memset(f,0,sizeof(f));
		for(i=1;i<=n;i++)
		 if(a[i]>r1)b[i]=-1;
		  else b[i]=1;
		l=1;r=0;  
		for(i=1;i<=n;i++){
			b[i]+=b[i-1];
			while(i-r+1>l2){
				ins(b[r]+100001,1);r++;
			}
			while(i-l+1>r2){
				ins(b[l-1]+100001,-1);l++;
			}
			sum2+=query(b[i]+100001);
			//if(Q==3)printf("%lld %lld %lld %lld\n",i,l,r,sum2);
		}
		for(i=l2;i<=r2;i++)sum3+=n-i+1;
		//printf("%lld %lld %lld\n",sum1,sum2,sum3);
		printf("%lld\n",sum1+sum2-sum3);
	}
}

/*
5
4 1 2 2 5
5
2 4 2 3
2 4 1 3
2 5 2 5
3 5 2 5
1 5 3 5
*/
