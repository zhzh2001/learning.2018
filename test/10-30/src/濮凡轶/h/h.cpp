#include <set>
#include <fstream>
#include <iostream>

using namespace std;

typedef long long LL;

set<LL> st;
set<LL> Tst;
LL bin[233];

int main()
{
	ifstream fin("h.in");
	ofstream fout("h.out");
	int k;
	LL R;
	fin >> k >> R;
	st.insert(1);
	for(int i = 1; i <= k; ++i)
	{
//		cerr << i << endl;
		LL p;
		fin >> p;
		bin[0] = 1;
		LL tr = R;
		int cnt = 0;
		while(true)
		{
			tr /= p;
			if(!tr)
				break;
			bin[++cnt] = bin[cnt - 1] * p;
		}
		Tst.clear();
//		cerr << i << ' ' << cnt << endl;
		for(set<LL>::iterator it = st.begin(); it != st.end(); ++it)
		{
			LL tmp = R / (*it);
			for(int j = 1; j <= cnt; ++j)
			{
				if(tmp >= bin[j])
				{
					LL tmp = bin[j] * (*it);
					if(tmp <= R)
						Tst.insert(tmp);
				}
			}
			Tst.insert(*it);
		}
		st = Tst;
	}
	fout << *(--st.end()) << '\n' << st.size() << '\n';
	fin.close();
	fout.close();
	return 0;
}
