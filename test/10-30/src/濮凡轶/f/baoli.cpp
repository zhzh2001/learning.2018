#include <set>
#include <fstream>
#include <iostream>
#include <algorithm>

const int maxn = 5005;

typedef long long LL;

#define int long long

LL a[maxn];
LL dp[maxn];
int lst;
std::set<LL> st;

signed main()
{
	std::ifstream fin("f.in");
	std::ofstream fout("f.ans");
	int n;
	fin >> n;
	for(int i = 1; i <= n; ++i)
		fin >> a[i];
	for(LL A = 0, B = 1; B <= 10000000000LL; )
	{
		LL tmp = B + A;
		A = B;
		B = tmp;
//		std::cout << "B = " << B << std::endl;
		st.insert(B);
	}
	for(int i = 1; i <= n; ++i)
	{
		dp[i] = dp[i-1] + 1;
		int j;
		for(j = i - 1; j && j >= lst; --j)
		{
			LL tmp = a[i] + a[j];
//			std::cout << i << ' ' << j << ' ' << tmp << ' ' << st.count(tmp) << '\n';
			if(st.count(tmp))
				break;
			dp[i] = std::min(dp[i], dp[j - 1] + 1);
		}
		lst = j + 1;
//		std::cout << "lst = " << lst << std::endl;
	}
//	std::cout << std::endl;
	fout << dp[n] << '\n';
	fin.close();
	fout.close();
	return 0;
}
