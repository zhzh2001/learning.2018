#include <set>
#include <utility>
#include <fstream>
#include <iostream>
#include <algorithm>

using namespace std;

typedef long long LL;
typedef pair<LL, int> pii;

const int maxn = 100005;
const int inf = 0x3f3f3f3f;

LL a[maxn];
LL dp[maxn];
LL F[2333];
int tot;
int lst;
multiset<pii> st;

int main()
{
	ifstream fin("f.in");
	ofstream fout("f.out");
	int n;
	fin >> n;
	for(int i = 1; i <= n; ++i)
		fin >> a[i];
	tot = 0;
	F[0] = 1, F[1] = 1;
	for(int i = 2; F[i - 1] <= 10000000000LL; ++i)
		F[i] = F[i - 1] + F[i - 2];
	st.clear();
	lst = 1;
	for(int i = 1; i <= n; ++i)
	{
		for(int j = 1; F[j]; ++j)
		{
			LL now = F[j] - a[i];
			while(!st.empty())
			{
				set<pii>::iterator it = st.lower_bound(make_pair(now, -inf));
				if(it != st.end() && it->first == now)
				{
					lst = max(it->second + 1, lst);
					st.erase(it);
				}
				else
					break;
			}
		}
//		fout << lst << '\n';
		dp[i] = dp[lst - 1] + 1;
		st.insert(make_pair(a[i], i));
	}
	fout << dp[n] << '\n';
	fin.close();
	fout.close();
	return 0;
}
