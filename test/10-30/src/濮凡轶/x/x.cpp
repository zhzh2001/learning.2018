#include <cstring>
#include <map>
#include <fstream>
#include <iostream>

using namespace std;

const int maxn = 2333;

int a[maxn];
int zt[maxn];
map<int, int> mp;

int main()
{
	ifstream fin("x.in");
	ofstream fout("x.out");
	int n, m;
	fin >> n;
	for(int i = 1; i <= n; ++i)
		fin >> a[i];
	fin >> m;
	while(m--)
	{
		memset(zt, 0, sizeof(zt));
		int ans = 0;
		int l1, r1, l2, r2;
		fin >> l1 >> r1 >> l2 >> r2;
		for(int mid = 1; mid <= n; ++mid)
		{
			if(a[mid] <= r1 && a[mid] >= l1)
			{
				mp.clear();
				zt[mid] = 0;
				for(int l = mid - 1; l; --l)
				{
					if(a[l] <= a[mid])
						zt[l] = zt[l + 1] - 1;
					else
						zt[l] = zt[l + 1] + 1;
				}
				int nowzt = 0;
				for(int r = mid + 1; r <= n; ++r)
				{
					if(a[r] < a[mid])
						zt[r] = zt[r - 1] - 1;
					else
						zt[r] = zt[r - 1] + 1;
					if(r <= r2)
						mp[zt[r]]++;
				}
				for(int l = 1, ll = l2, rr = r2; l < mid && ll <= rr; )
				{
//					cout << l << ' ' << ll << ' ' << rr << endl;
					ans += mp[-zt[l]] + mp[-zt[l] + 1];
					l++;
					if(ll > mid)
						mp[zt[ll]]--;
					ll++;
					if(rr <= n)
					{
						if(rr > mid)
							mp[zt[rr]]++;
						rr++;
					}
				}
			}
		}
//		cout << endl;
		fout << ans << '\n';
	}
	fin.close();
	fout.close();
	return 0;
}
