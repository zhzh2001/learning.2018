#include <bits/stdc++.h>
#define LL long long
#define GG long long 
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
using namespace std;
inline GG read(){
	GG k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(GG x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(GG x){ write(x);puts("");}

int n, sum; 
LL Mx, R; 
int prime[30]; 
map<LL, LL> mp; 
struct node{
    LL val; 
    friend bool operator <(node a, node b) {
        return a.val > b.val; 
    }
};
priority_queue<node> Q; 

int main() {
  freopen("h.in", "r", stdin); 
  freopen("h.out", "w", stdout); 
  n = read(); R = read(); 
  For(i, 1, n) prime[i] = read(); 
  sort(prime+1, prime+n+1); 
  node p; p.val = 1; Q.push(p); 
  mp[1] = 1; 
  ++sum; Mx = 1; 
  while(!Q.empty()) {
    LL u = Q.top().val; 
    LL tmp = u; 
    Q.pop(); 
    node p; 
    For(i, 1, n) {
      u = tmp*prime[i]; 
      if(u > R) break; 
      if(!mp.count(u)) {
        mp[u] = 1; 
        p.val = u; 
        Q.push(p); 
        if(u > Mx) Mx = u; 
        ++sum;
      }
    }
  }
  writeln(Mx); 
  writeln(sum); 
}




