#include <bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
using namespace std;
inline GG read(){
  GG k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(GG x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(GG x){ write(x);puts("");}

const int N = 100011, tot = 45; 
const int fei[50] = {0,1,2,3,5,8,13,21,34,55,89,144,233,377,610,987,1597,2584,4181,6765,10946,17711,28657,46368,75025,121393,196418,317811,514229,832040,1346269,2178309,3524578,5702887,9227465,14930352,24157817,39088169,63245986,102334155,165580141,267914296,433494437,701408733,1134903170,1836311903}; 
int n, val[N], sum; // 45
map<int,int> mp; 

inline bool check(int l, int r) {
  mp.clear(); 
  For(i, l, r) mp[val[i]] = 1; 
  For(i, 1, 45) 
    For(j, 1, n) 
      if(mp.count(fei[i]-val[j]) && 2*val[j]!=fei[i]) return 0; 
  return 1; 
}
 
int main() {
  freopen("f.in", "r", stdin); 
  freopen("f.out", "w", stdout); 
  n = read(); 
  For(i, 1, n) val[i] = read(); 
  /*
  int l = 1, r = 1; 
  bool flag = 0; 
  while(r <= n) {
    if(check(l, r)) {
      ++r;  
    }
    else {
      ++r; l = r; ++ans;
    }
  } 
  */
  For(i, 1, n) {
    if(mp.count(val[i])) {
      mp.clear(); 
      ++sum; 
    }
    For(j, 1, tot) mp[fei[j]-val[i]] = 1; 
  } 
  ++sum; 
  writeln(sum); 
}


/*

5
1 5 2 6 7

*/




