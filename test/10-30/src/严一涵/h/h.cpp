#include <iostream>
#include <algorithm>
#include <cstdio>
#include <set>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline LL read(){
	LL a=0,b=1;int dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
int n,p[26],x,y;
LL m;
int f[1000003][25];
LL sol1(int n,LL m){
	if(m<=1000000)return f[m][n];
	if(n==-1)return 1;
	int x=p[n];
	LL ans=1;
	for(LL y=1;y<=m;y*=x){
		ans=max(ans,sol1(n-1,m/y)*y);
		if(y>m/x)break;
	}
	return ans;
}
LL sol2(int n,LL m){
	if(m<=1000000)return f[m][n];
	if(n==-1)return 1;
	int x=p[n];
	LL ans=0;
	for(LL y=1;y<=m;y*=x){
		ans+=sol2(n-1,m/y);
		if(y>m/x)break;
	}
	return ans;
}
int main(){
	freopen("h.in","r",stdin);
	freopen("h.out","w",stdout);
	n=read();
	m=read();
	for(int i=0;i<n;i++){
		p[i]=read();
	}
	sort(p,p+n);
	for(int i=1;i<=1000000;i++){
		x=i;
		for(int j=0;j<n;j++){
			f[i][j]=f[i-1][j];
			y=p[j];
			while(x%y==0)x/=y;
			if(x==1)f[i][j]=i;
		}
	}
	printf("%lld\n",sol1(n-1,m));
	for(int i=1;i<=1000000;i++){
		x=i;
		for(int j=0;j<n;j++){
			f[i][j]=f[i-1][j];
			y=p[j];
			while(x%y==0)x/=y;
			if(x==1)f[i][j]++;
		}
	}
	printf("%lld\n",sol2(n-1,m));
	return 0;
}
