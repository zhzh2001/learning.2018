#include <iostream>
#include <algorithm>
#include <cstdio>
#include <set>
using namespace std;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
int n,a[100003],m,ql1,qr1,ql2,qr2,x,y,z,ans;
int main(){
	freopen("x.in","r",stdin);
	freopen("x.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	m=read();
	while(m-->0){
		ql1=read();
		qr1=read();
		ql2=read();
		qr2=read();
		ans=0;
		for(int i=1;i<=n;i++){
			x=y=0;
			z=1;
			for(int j=i;j<=n&&z<=qr2;j++,z++){
				if(a[j]<ql1)x++;
				else if(a[j]>qr1)y++;
				if(z>=ql2&&x+x<z&&y+y<=z)ans++;
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
