#include <iostream>
#include <algorithm>
#include <cstdio>
#include <set>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
const int fn=45;
int fib[fn]={1,2,3,5,8,13,21,34,55,89,144,233,377,610,987,1597,2584,4181,6765,10946,17711,28657,46368,75025,121393,196418,317811,514229,832040,1346269,2178309,3524578,5702887,9227465,14930352,24157817,39088169,63245986,102334155,165580141,267914296,433494437,701408733,1134903170,1836311903};
int n,a[100003],b[100003],f[100003],la[100003],ans[100003],x;
inline int isv(int v){
	if(v<b[1]||v>b[n])return 0;
	int *p=lower_bound(b+1,b+n+1,v);
	if(*p!=v)return 0;
	return p-b;
}
int main(){
	freopen("f.in","r",stdin);
	freopen("f.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		a[i]=read();
		b[i]=a[i];
	}
	sort(b+1,b+n+1);
	for(int i=1;i<=n;i++){
		for(int j=0;j<fn;j++){
			x=isv(fib[j]-a[i]);
			if(x==0||la[x]==0)continue;
			x=la[x];
			if(x<i)f[i]=max(f[i],x);
		}
		la[isv(a[i])]=i;
	}
	for(int i=1,j=0;i<=n;i++){
		if(j<f[i])j=f[i];
		ans[i]=ans[j]+1;
	}
	printf("%d\n",ans[n]);
	return 0;
}
