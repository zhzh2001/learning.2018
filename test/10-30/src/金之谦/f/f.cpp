#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(ll x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x){
	write(x);puts("");
}
map<int,int>mp;
const int N=1e5+10;
int n,a[N];ll f[210];
int dp[N];
int main()
{
	freopen("f.in","r",stdin);
	freopen("f.out","w",stdout);
	f[0]=1;f[1]=1;int cnt;
	for(cnt=2;;cnt++){
		f[cnt]=f[cnt-1]+f[cnt-2];
		if(f[cnt]>2e9)break;
	}
	cnt--;
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	int minn=0,maxx=0;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=cnt;j++){
			if(f[j]<=a[i])continue;
			if(f[j]-a[i]>maxx)break;
			if(mp[f[j]-a[i]])minn=max(minn,mp[f[j]-a[i]]);
		}
		dp[i]=dp[minn]+1;
		maxx=max(maxx,a[i]);
		mp[a[i]]=i;
	}
	writeln(dp[n]);
	return 0;
}
