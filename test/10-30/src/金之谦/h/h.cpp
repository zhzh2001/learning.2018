#include <bits/stdc++.h>
#define int unsigned long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int K,R,a[210],ans=0,ma=0;
inline void dfs(int x,int s){
	if(x==K+1){ans++;ma=max(ma,s);return;}
	int p=s;
	for(;p<=R;p*=a[x])dfs(x+1,p);
}
signed main()
{
	freopen("h.in","r",stdin);
	freopen("h.out","w",stdout);
	K=read();R=read();
	for(int i=1;i<=K;i++)a[i]=read();
	dfs(1,1);
	writeln(ma);
	writeln(ans);
	return 0;
}
/*
10 1000000000000000000
2 3 5 7 11 13 17 19 23 29
25 1000000000000
2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71 73 79 83 89 97
*/
