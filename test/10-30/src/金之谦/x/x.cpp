#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=1e5+10;
int n,a[N],l1,r1,l2,r2;
int t[60*N],ls[60*N],rs[60*N],rt[N],cnt=0;
inline void xg(int l,int r,int &nod,int la,int x){
	nod=++cnt;t[nod]=t[la];ls[nod]=ls[la];rs[nod]=rs[la];
	if(l==r){t[nod]++;return;}
	int mid=(l+r)>>1;
	if(x<=mid)xg(l,mid,ls[nod],ls[la],x);
	else xg(mid+1,r,rs[nod],rs[la],x);
	t[nod]=t[ls[nod]]+t[rs[nod]];
}
inline int ssum(int l,int r,int nod,int la,int i,int j){
	if(l>=i&&r<=j)return t[nod]-t[la];
	int mid=(l+r)>>1,ans=0;
	if(i<=mid)ans+=ssum(l,mid,ls[nod],ls[la],i,j);
	if(j>mid)ans+=ssum(mid+1,r,rs[nod],rs[la],i,j);
	return ans;
}
inline int Main(int x){
	int sum=0,l=n*3/2;
	memset(rt,0,sizeof rt);cnt=0;
	for(int i=1;i<=n;i++){
		xg(1,3*n,rt[i],rt[i-1],l);
		if(a[i]<=x)l--;else l++;
		if(i-l2+1>0)sum+=ssum(1,3*n,rt[i-l2+1],rt[max(0ll,i-r2)],l,3*n);
	}
	return sum;
}
signed main()
{
	freopen("x.in","r",stdin);
	freopen("x.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	for(int T=read();T;T--){
		l1=read(),r1=read();
		l2=read(),r2=read();
		writeln(Main(r1)-Main(l1-1));
	}
	return 0;
}
