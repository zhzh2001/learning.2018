#include<cstdio>
#include<algorithm>
using namespace std;
int n,p[30];
long long m,ma,num;
inline long long read(){
	long long x=0,f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9'){
		if(ch=='-')f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}
void dfs(int k,long long s){
	if(k==1){
		num++;
		for(;s<=m/p[k];){
			s=s*p[k];//printf("%lld %lld\n",s,m/p[k]);
			num++;
		}
		ma=max(ma,s);
		return;
	}
	dfs(k-1,s);
	for(;s<=m/p[k];){
		s=s*p[k];
		dfs(k-1,s);
	}
}
int main(){
	freopen("h.in","r",stdin);
	freopen("h.out","w",stdout);
	n=read();
	m=read();
	for(int i=1;i<=n;i++)
		p[i]=read();
	sort(p+1,p+n+1);
	dfs(n,1);
	printf("%lld\n",ma);
	printf("%lld",num);
	return 0;
}/*
*/
