#include<cstdio>
#include<algorithm>
using namespace std;
int n,mi,ma,s,l,r,fl,num,size,f[51],a[110000],ls[20000000],rs[20000000],t[20000000];
inline int read(){
	int x=0,f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9'){
		if(ch=='-')f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}
inline int sea(int x){
	int l=0,mid=0,r=46;
	while(l+1<r){
		mid=(l+r)>>1;
		if(f[mid]<=x)l=mid;
		else r=mid;
	}
	return l;
}
inline void chan(int k,int l,int r,int x){
	if(l==r){
		t[k]=s;
		return;
	}
	int mid=(l+r)>>1;
	if(x<=mid){
		if(!ls[k]){
			num++;
			ls[k]=num;
		}
		chan(ls[k],l,mid,x);
	}
	else{
		if(!rs[k]){
			num++;
			rs[k]=num;
		}
		chan(rs[k],mid+1,r,x);
	}
	t[k]=max(t[ls[k]],t[rs[k]]);//printf("%d %d\n",k,mt[k]);
}
inline void ask(int k,int l,int r,int x){//printf("%d %d\n",l,mt[k]);
	if(t[k]<s)return;
	if(l==r){
		fl=t[k];
		return;
	}
	int mid=(l+r)>>1;
	if(x<=mid){
		if(!ls[k])return;
		else ask(ls[k],l,mid,x);
	}
	else{
		if(!rs[k])return;
		else ask(rs[k],mid+1,r,x);
	}
}
int main(){
	freopen("f.in","r",stdin);
	freopen("f.out","w",stdout);
	num=1;
	size=0;
	n=read();
	for(int i=1;i<=n;i++){
		a[i]=read();
		size=max(size,a[i]);
	}
	f[1]=1;
	f[2]=2;
	for(int i=3;i<=45;i++)
		f[i]=f[i-1]+f[i-2];
	mi=1e9+1;
	ma=-1e9-1;
	for(int i=1;i<=n;i++){
		if(mi>1e9&&ma<-1e9){
			s++;
			mi=a[i];
			ma=a[i];
			chan(1,1,size,a[i]);
			continue;
		}
		l=sea(a[i]+mi);
		if(f[l]<a[i]+mi)l++;
//		if(f[l]>a[i]+mi)puts("!");
		r=sea(a[i]+ma);//printf("%d %d\n",l,r);
		for(int j=l;j<=r;j++){
			fl=0;
			ask(1,1,size,f[j]-a[i]);//printf("%d %d\n",i,f[j]-a[i]);
			if(fl==s)break;
		}
		if(fl==s){
			s++;
			mi=a[i];
			ma=a[i];
		}
		chan(1,1,size,a[i]);
		mi=min(mi,a[i]);
		ma=max(ma,a[i]);
	}
	printf("%d",s);
	return 0;
}
