#include<map>
#include<cstdio>
#include<iostream>
using namespace std;
typedef long long LL;
#define gc c=getchar()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
const int MAXN=1e5+5;
LL f[55];
int a[MAXN];
int main(){
	freopen("f.in","r",stdin);
	freopen("f.out","w",stdout);
	f[0]=f[1]=1;
	for(int i=2;i<=50;i++)f[i]=f[i-1]+f[i-2];
	int n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	map<LL,bool>vis;
	int ans=1;
	for(int i=1;i<=n;i++){
		bool flag=1;
		for(int j=1;j<=50;j++)
			if(vis[f[j]-a[i]]){
				flag=0;
				break;
			}
		if(!flag){
			vis.clear();
			ans++;
		}
		vis[a[i]]=1;
	}
	printf("%d",ans);
}
