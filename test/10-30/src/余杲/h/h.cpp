#include<cstdio>
#include<iostream>
#include<algorithm>
using namespace std;
typedef unsigned long long LL;
#define gc c=getchar()
LL read(){
	LL x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
const int siz=3e7;
int a[105],p[105];
LL que[siz+5];
int main(){
	freopen("h.in","r",stdin);
	freopen("h.out","w",stdout);
	int n=read();LL R=read();
	register int num=n;
	for(int i=1;i<=n;i++){
		a[i]=read();
		p[i]=1;
	}
	sort(a+1,a+n+1);
	register LL Min;
	register int ans=1;
	que[1]=1;
	while(1){
		Min=5e18;
		for(register int i=1;i<=num;i++)
			if((long double)que[p[i]]*a[i]>R)num=i-1;
			else Min=min(Min,que[p[i]]*a[i]);
		if(!num)break;
		que[++ans]=Min;
		for(register int i=1;i<=num;i++)
			if(que[p[i]]*a[i]==Min)p[i]++;
	}
	printf("%lld\n%d",que[ans%siz],ans);
}
/*
25 1000000000000
2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 51 53 59 61 67 71 73 79 83 87
*/
