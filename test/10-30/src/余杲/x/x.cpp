#include<vector>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
#define gc c=getchar()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
const int MAXN=1e5+5;
int a[MAXN];
int main(){
	freopen("x.in","r",stdin);
	freopen("x.out","w",stdout);
	int n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	for(int m=read();m;m--){
		int ans=0;
		int l1=read(),r1=read(),l2=read(),r2=read();
		for(int i=l1;i<=r1;i++)
			for(int j=max(1,i-r2);j<=i;j++)
				for(int k=j+l2;k<=min(n,j+r2);k++){
					vector<int>v;
					for(int p=j;p<=k;p++)v.push_back(a[p]);
					sort(v.begin(),v.end());
					if(v[v.size()/2]==a[i])ans++; 
				}
		printf("%d\n",ans);
	}
}
