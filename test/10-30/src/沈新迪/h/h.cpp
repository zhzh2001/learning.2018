#include<bits/stdc++.h>
using namespace std;
long long read()
{
    char ch=getchar();long long x=0,ff=1;
    while(ch<'0'||ch>'9') {if(ch=='-') ff=-1;ch=getchar();}
    while(ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
    return x*ff;
}
void write(long long aa)
{
    if(aa<0) putchar('-'),aa=-aa;
    if(aa>9) write(aa/10);
    putchar(aa%10+'0');
    return;
}
long long n,R,ans,mx;
long long p[27];
struct node
{
	long long sum,mx;
};
map<long long,node>mp;
void dfs(long long now,long long zhi)
{
	if(now==n+1) {ans++;mx=max(mx,zhi);return;}
	if(zhi<=R/p[now]) dfs(now,zhi*p[now]);
	dfs(now+1,zhi);
	return ;
}
int main()
{
    freopen("h.in","r",stdin);
    freopen("h.out","w",stdout);
    n=read();R=read();
    for(long long i=1;i<=n;++i) p[i]=read();
    dfs(1,1);
    write(mx);puts("");write(ans);
    return 0;
}
