#include <iostream>
#include <cstdio>
#include <algorithm>
#include <string>
#define ll long long
using namespace std;
const int Maxn=100009;
int read(){
	char c;int num,f=1;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;num=c-'0';
	while(c=getchar(), isdigit(c))num=num*10+c-'0';
	return f*num;
}
int n,m,l1,l2,r1,r2,maxn,flag;
int a[Maxn*2],f[Maxn*2],len;
ll ans;
void ask();
int main()
{
	freopen("x.in","r",stdin);
	freopen("x.out","w",stdout);
	n=read();for(int i=1;i<=n;i++)a[i]=read(),maxn=max(a[i],maxn);
	m=read();
	while(m--){
		l1=read();r1=read();
		l2=read();r2=read();
		ans=0;ask();
	}
	return 0;
}
void ask(){
	l1=max(l1,1);r1=min(r1,maxn);
	l2=max(1,l2);r2=min(n,r2);
	for(int i=1;i<=n;i++)
		if(a[i]>r1)f[i]=1;
		else if(a[i]<l1)f[i]=-1;
		else f[i]=0;
	int s1,s2,s3;
	for(int i=1;i<=n;i++){
		s1=0;s2=0;s3=0;flag=0;len=0;
		for(int j=i;j<=n&&j<=i+r2-1;j++){
			if(f[j]==0)s2++;
			else if(f[j]==1)s3++;
			else s1++;len++;
			if((s1+s2+s3)>=l2&&s2&&(s1+s2+s3+1)/2>s1&&(s1+s2+s3+1)/2<=s1+s2)ans++;
		}
	}
	printf("%lld\n",ans);
}
