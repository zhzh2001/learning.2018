#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <map>
#define ll long long
using namespace std;
const int Maxn=100009;
ll read(){
	char c;ll num,f=1;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;num=c-'0';
	while(c=getchar(), isdigit(c))num=num*10+c-'0';
	return f*num;
}
ll fib[Maxn],a[Maxn],g[Maxn];
ll f[Maxn],d[Maxn],q[Maxn],n,h=1,t=0;
map<int,int> m;
bool ask(ll a);
void init();
int main()
{
	freopen("f.in","r",stdin);
	freopen("f.out","w",stdout);
	init();
	for(int i=1;i<=n;i++){
		while(d[h]<g[i])h++;
		f[i]=q[h]+1;
		while(f[i]<=q[t])t--;
		q[++t]=f[i];d[t]=i;
	}	
	cout<<f[n]<<endl;
	return 0;
}
bool ask(ll a){
	int l=1,r=49,mid;
	while(l<=r){
		mid=(l+r)>>1;
		if(fib[mid]==a)return 1;
		if(fib[mid]<a)l=mid+1;
		else r=mid-1;
	}
	return 0;
}
void init(){
	fib[1]=1;fib[2]=1;n=read();
	for(int i=3;i<=49;i++)fib[i]=fib[i-1]+fib[i-2];
	for(int i=1;i<=n;i++)a[i]=read();g[1]=0;m[a[1]]=1;
	for(int i=2;i<=n;i++){
		int pos=0;
		for(int j=1;j<=49;j++)
			if(fib[j]-a[i]>=1&&m.find(fib[j]-a[i])!=m.end())
				pos=max(pos,m[fib[j]-a[i]]);
		g[i]=pos;
		m[a[i]]=i;
		if(!g[i])g[i]=g[i-1];
	}
	memset(f,0x3f,sizeof(f));
	f[0]=0;q[++t]=f[0];d[t]=0;
}
/* 对每一只兔子枚举它之前第一个和他组成兔子数列的兔子
 * 然后跑一遍dp，求出最小划分数。
 * g[i]表示从i开始，最大向左拓展的长度。
 * 发现g[i]是递增的，我们可以考虑单调队列优化。 
 * 但是被预处理卡了。。
 */ 
