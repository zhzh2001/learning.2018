#include <iostream>
#include <cstdio>
#include <algorithm>
#include <string>
#define ll long long
using namespace std;
ll read(){
	char c;ll num,f=1;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;num=c-'0';
	while(c=getchar(), isdigit(c))num=num*10+c-'0';
	return f*num;
}
ll k,r,p[109],now=1,ans=0,maxn;
void dfs(int d){
	if(d>k){
		ans++;
		maxn=max(maxn,now);
		return ;
	}
	ll qwq=now;
	while(1){
		if(now<r)dfs(d+1);
		else break;
		now*=p[d];
	}
	now=qwq;
}
int main()
{
	freopen("h.in","r",stdin);
	freopen("h.out","w",stdout);
	k=read();r=read();
	for(int i=1;i<=k;i++)p[i]=read();
	dfs(1);
	printf("%lld\n%lld\n",maxn,ans);
	return 0;
}
