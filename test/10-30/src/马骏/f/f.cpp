// #pragma GCC optimize(3)
#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 100010
#define int long long
#define MOD 24739579
using namespace std;
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9) write(x/10);putchar(x%10+'0');}
int read(){int d=0,w=1;char c=getchar();for(;c<'0'||c>'9';c=getchar())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=getchar())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
int fq[50],n,ans,hsh[MOD][2],a[maxn];
int find(int k,int t)
{
	int tmp=k%MOD;
	for(;hsh[tmp][0]&&hsh[tmp][1]==t&&(hsh[tmp][0]!=k||hsh[tmp][1]!=t);)
	{
		tmp++;
		if(tmp==MOD) tmp=0;
	}
	// {
	// 	if(hsh[tmp][0]==k&&hsh[tmp][1]==t) break;
	// 	if(hsh[tmp][0]==0||hsh[tmp][1]!=t) break;
	// }
	return tmp;
}
int check(int k,int t)
{
	for(int i=0;i<46;i++)
	{
		if(fq[i]<=k) continue;
		int tmp=find(fq[i]-k,t);
		if(hsh[tmp][0]==fq[i]-k&&hsh[tmp][1]==t) return 0;
	}
	return 1;
}
void ist(int k,int t)
{
	int tmp=find(k,t);
	if(hsh[tmp][0]!=k||hsh[tmp][1]!=t)
	{
		hsh[tmp][0]=k;
		hsh[tmp][1]=t;
	}
}
signed main()
{
	freopen("f.in","r",stdin);
	freopen("f.out","w",stdout);
	fq[0]=1;
	fq[1]=2;
	for(int i=1;i<46;i++)
		fq[i]=fq[i-1]+fq[i-2];
	n=read();
	for(int i=1;i<=n;a[i++]=read());
	for(int i=1;i<=n;ist(a[i++],ans))
		if(!check(a[i],ans)) ans++;
	write(ans+1);
	return 0;
}
