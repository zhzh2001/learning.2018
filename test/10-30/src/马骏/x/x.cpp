// #pragma GCC optimize(3)
#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 1010
// #define int long long
using namespace std;
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9) write(x/10);putchar(x%10+'0');}
int read(){int d=0,w=1;char c=getchar();for(;c<'0'||c>'9';c=getchar())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=getchar())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
struct node
{
	int val,cnt;
	friend bool operator < (node a,node b)
	{
		return a.val<b.val||a.val==b.val&&a.cnt<b.cnt;
	}
};
set<node> s;
int n,m,a[maxn],f[maxn][maxn];
signed main()
{
	freopen("x.in","r",stdin);
	freopen("x.out","w",stdout);
	n=read();
	for(int i=1;i<=n;a[i++]=read());
	for(int i=1;i<=n;i++)
	{
		node tmp;
		tmp.val=a[i];
		tmp.cnt=i;
		s.insert(tmp);
		int now=a[i],cc=i;
		f[i][i]=i;
		for(int j=i+1;j<=n;j++)
		{
			node tmp;
			tmp.val=a[j];
			tmp.cnt=j;
			s.insert(tmp);
			if((j-i+1)%2==0)
			{
				if(a[j]<now||a[j]==now&&j<cc)
				{
					node tmp;
					tmp.val=now;
					tmp.cnt=cc;
					tmp=*(--s.find(tmp));
					now=tmp.val;
					cc=tmp.cnt;
				}
			}
			else
			{
				if(a[j]>now||a[j]==now&&j>cc)
				{
					node tmp;
					tmp.val=now;
					tmp.cnt=cc;
					tmp=*(++s.find(tmp));
					now=tmp.val;
					cc=tmp.cnt;
				}
			}
			f[i][j]=cc;
		}
	}
	m=read();
	for(int i=1;i<=m;i++)
	{
		int l1=read(),r1=read(),l2=read(),r2=read(),ans=0;
		for(int i=1;i<=n-l2+1;i++)
			for(int j=i+l2-1;j-i+1<=r2;j++)
				if(f[i][j]>=l1&&f[i][j]<=r1) ans++;
		wln(ans);
	}
	return 0;
}
