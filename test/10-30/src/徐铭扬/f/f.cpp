#include<bits/stdc++.h>
using namespace std;
const int K=46;
int f[K],i,x,cnt,n;
bool fl;
map<int,int>mp;
int rd(){
	int x=0,f=1;char c;
	do{c=getchar();if(c=='-')f=-1;}while(c<'0'||c>'9');
	while('0'<=c&&c<='9')x=(x<<3)+(x<<1)+(c^48),c=getchar();
	return x*f;
}
int main(){
	freopen("f.in","r",stdin);
	freopen("f.out","w",stdout);
	f[0]=f[1]=1;
	for (i=2;i<K;i++) f[i]=f[i-1]+f[i-2];
	for (n=rd();n--;){
		x=rd();fl=0;
		for (i=K-1;x<f[i] && !fl;i--)
			if (mp[f[i]-x]) fl=1;
		if (fl) cnt++,mp.clear();
		mp[x]=1;
	}
	if (mp.size()) cnt++;
	printf("%d",cnt);
}
