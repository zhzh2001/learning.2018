#include<bits/stdc++.h>
using namespace std;
const int N=1001;
int n,m,i,j,l1,r1,l2,r2,a[N],b[N],t,mid,ans,k;
int rd(){
	int x=0,f=1;char c;
	do{c=getchar();if(c=='-')f=-1;}while(c<'0'||c>'9');
	while('0'<=c&&c<='9')x=(x<<3)+(x<<1)+(c^48),c=getchar();
	return x*f;
}
int main(){
	freopen("x.in","r",stdin);
	freopen("x.out","w",stdout);
	n=rd();
	for (i=1;i<=n;i++) a[i]=rd();
	for (m=rd();m--;){
		l1=rd();r1=rd();l2=rd();r2=rd();ans=0;
		for (i=1;i<=n-l2+1;i++)
			for (j=i+l2-1;j-i+1<=r2 && j<=n;j++){
				t=0;
				for (k=i;k<=j;k++) b[++t]=a[k];
				mid=(t+1)/2;
				nth_element(b+1,b+mid,b+t+1);
				if (l1<=b[mid] && b[mid]<=r1) ans++;
			}
		printf("%d\n",ans);
	}
}
