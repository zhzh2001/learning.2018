#include<bits/stdc++.h>
using namespace std;
inline int read() {
  int x=0, f=0; char ch=getchar();
  for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
  for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
  return f?-x:x;
}
const int N = 100005;
int n, a[N], ha[N];
int main() {
  freopen("x.in","r",stdin);
  freopen("x.out","w",stdout);
  n = read();
  for (int i = 1; i <= n; i++)
    a[i] = read();
  int m = read();
  while (m--) {
    int l1 = read(), r1 = read();
    int l2 = read(), r2 = read();
    int ans = 0;
    for (int i = 1; i <= n; i++) {
      int sum = 0, sum2 = 0;
      for (int j = i; j <= n; j++) {
        if (a[j] > r1) sum++;
        else if (a[j] < l1) sum--;
        else sum2++;
        if (j-i+1>=l2&&j-i+1<=r2) {
          if (sum2 > abs(sum)||(sum2 == abs(sum)&&sum>0)) ans++;
        }
      }
    }
    printf("%d\n", ans);
  }
  return 0;
}
