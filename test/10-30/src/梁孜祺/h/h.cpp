#include<bits/stdc++.h>
using namespace std;
inline long long read() {
  long long x=0, f=0; char ch=getchar();
  for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
  for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
  return f?-x:x;
}
long long a[30], n, ans;
long long R, mx;
inline bool cmp(long long x, long long y) {
  return x > y;
}
inline void dfs(int x, long long sum) {
  if (x == n) {
    ans++;
    for (; R/a[n] >= sum;) sum *= a[n], ans++;
    mx = max(mx, sum);
    return;
  }
  dfs(x+1, sum);
  long long pnt = sum;
  for (; R/a[x] >= sum;) {
    sum *= a[x];
    dfs(x+1, sum);
  }
  sum = pnt;
}
int main() {
  freopen("h.in","r",stdin);
  freopen("h.out","w",stdout);
  n = read(); R = read();
  for (int i = 1; i <= n; i++)
    a[i] = read();
  sort(a+1,a+1+n, cmp);
  dfs(1, 1ll);
  printf("%lld\n", mx);
  printf("%lld\n", ans);
  return 0;
}
