#include<bits/stdc++.h>
using namespace std;
inline int read() {
  int x=0, f=0; char ch=getchar();
  for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
  for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
  return f?-x:x;
}
const int N = 100005;
int n, a[N], F[N], nxt[N];
long long f[55];
map <int, int> mp;
int main() {
  freopen("f.in","r",stdin);
  freopen("f.out","w",stdout);
  f[1] = 1ll;
  f[2] = 2ll;
  int tot = 2;
  for (int i = 3; f[i-1] <= 2000000000ll; i++)
    f[++tot] = f[i-1]+f[i-2];
  tot--;
  n = read();
  for (int i = 1; i <= n; i++)
    a[i] = read();
  for (int i = 1; i <= n; i++) {
    for (int j = 1; j <= tot; j++)
      if (f[j] > a[i]) {
        if (mp[f[j]-a[i]]) F[i] = max(F[i], mp[f[j]-a[i]]);
      }
    mp[a[i]] = i;
  }
  int mx = 0, ans = 0;
/*  for (int i = 1; i <= n; i++)
    cout << i <<" " <<F[i] <<endl;*/
  for (int i = n; i; i--) {
    mx = max(mx, F[i]);
//    cout << mx << " "<< i << endl;
    if (mx == i-1) ans++, mx = 0;
  }
  printf("%d\n", ans);
  return 0;
}
/*
1 2 3 5 8 13 21 34 55

*/
