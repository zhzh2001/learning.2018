#include<iostream>
#include<algorithm>
#include<map>
#include<math.h>
#include<fstream>
using namespace std;
ifstream fin("h.in");
ofstream fout("h.out");
long long n,lim,num[33];
long long mx,sum;
inline long long max(long long &a,long long &b){
	if(a>b) return a;
	return b;
}
void calc(int x,long long now){
	if(x==n){
		++sum;
		while(now*num[n]<=lim){
			now=now*num[n];
			++sum;
		}
		mx=max(mx,now);
		return;
	}
	calc(x+1,now);
	while(now*num[x]<=lim){
		now=now*num[x];
		calc(x+1,now);
	}
}
int main(){
	fin>>n>>lim;
	for(int i=1;i<=n;i++){
		fin>>num[i];
	}
	sort(num+1,num+n+1);
	for(int i=1;i<=n/2;i++){
		swap(num[i],num[n-i+1]);
	}
	calc(1,1);
	fout<<mx<<endl<<sum<<endl;
	return 0;
}
/*

in:
3 30
2 3 7

1 2 3 4 6 7
7 6 4 3 2 1

12+12+8
6+6+6+6+4+4

out:
28
16

*/
