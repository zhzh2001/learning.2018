#include<iostream>
#include<algorithm>
#include<map>
#include<math.h>
#include<stdio.h>
#include<fstream>
using namespace std;
ifstream fin("x.in");
ofstream fout("x.out");
inline long long read(){
	long long x=0,f=1,ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0',ch=getchar();}
	return x*f;
}
long long n,m,a[100003],sum[100003],l1,r1,l2,r2;
struct tree{
	long long sum;
}t[1000003];
void update(int rt,int l,int r,int pos,int val){
	if(l==r){
		t[rt].sum+=val;
		return;
	}
	int mid=l+r>>1;
	if(pos<=mid){
		update(rt<<1,l,mid,pos,val);
	}else{
		update(rt<<1|1,mid+1,r,pos,val);
	}
	t[rt].sum=t[rt<<1].sum+t[rt<<1|1].sum;
}
long long query(int rt,int l,int r,int x,int y){
	if(l==x&&r==y){
		return t[rt].sum;
	}
	int mid=l+r>>1;
	if(y<=mid){
		return query(rt<<1,l,mid,x,y);
	}else if(x>mid){
		return query(rt<<1|1,mid+1,r,x,y);
	}else{
		return query(rt<<1,l,mid,x,mid)+query(rt<<1|1,mid+1,r,mid+1,y);
	}
}
void build(int rt,int l,int r){
	t[rt].sum=0;
	if(l==r){
		return;
	}
	int mid=l+r>>1;
	build(rt<<1,l,mid),build(rt<<1|1,mid+1,r);
}
inline long long calc(long long x){
	build(1,-100000,100000);
	sum[0]=0;
	long long ret=0;
	for(register int i=1;i<=n;i++){
		if(a[i]<=x){
			sum[i]=sum[i-1]+1;
		}else{
			sum[i]=sum[i-1]-1;
		}
		if(i>=r2+1){
			update(1,-100000,100000,sum[i-r2-1],-1);
		}
		if(i>=l2){
			update(1,-100000,100000,sum[i-l2],1);
		}
		ret+=query(1,-100000,100000,-100000,sum[i]);
	}
	return ret;
}
int main(){
	freopen("x.in","r",stdin);
	freopen("x.out","w",stdout);
	n=read();
	for(register int i=1;i<=n;i++){
		a[i]=read();
	}
	m=read();
	for(register int i=1;i<=m;i++){
		l1=read(),r1=read(),l2=read(),r2=read();
		fout<<calc(r1)-calc(l1-1)<<endl;
	}
	return 0;
}
/*

in:
5
4 1 2 2 5
5
2 4 2 3
2 4 1 3
2 5 2 5
3 5 2 5
1 5 3 5

out:
5
8
8
0
6

*/
