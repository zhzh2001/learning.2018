#include<iostream>
#include<algorithm>
#include<map>
#include<fstream>
using namespace std;
ifstream fin("f.in");
ofstream fout("f.out");
map<long long,bool> mp;
long long n,f[1003],a[100003],ans;
int main(){
	fin>>n;
	f[0]=f[1]=1;
	for(register int i=2;i<=55;i++){
		f[i]=f[i-1]+f[i-2];
	}
	for(register int i=1;i<=n;i++){
		fin>>a[i];
		if(mp[a[i]]){
			ans++;
			mp.clear();
		}
		for(register int j=1;j<=55;j++){
			if(f[j]>=a[i]){
				mp[f[j]-a[i]]=true;
			}
		}
	}
	fout<<ans+1<<endl;
	return 0;
}
