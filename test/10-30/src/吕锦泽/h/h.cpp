#include<map>
#include<queue>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 100005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
map<ll,bool>vis; priority_queue<ll>Q;
ll n,r,ans1,ans2,p[30];
int main(){
	freopen("h.in","r",stdin);
	freopen("h.out","w",stdout);
	n=read(); r=read();
	rep(i,1,n) Q.push(p[i]=read());
	while(!Q.empty()){
		ll x=Q.top(); Q.pop();
		if (vis[x]) continue;
		++ans2; vis[x]=1; ans1=max(ans1,x);
		rep(i,1,n)
			if (x*p[i]>r) break;
			else Q.push(x*p[i]);
	}
	printf("%lld\n%lld",ans1,ans2);
}
/*
3 30
2 3 7

*/
