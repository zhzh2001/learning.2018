#include<map>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 100005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,a[N],f[N],ans=1; map<ll,ll> mp;
int main(){
	freopen("f.in","r",stdin);
	freopen("f.out","w",stdout);
	n=read(); f[0]=1; f[1]=1;
	rep(i,2,46) f[i]=f[i-1]+f[i-2];
	rep(i,1,n) a[i]=read();
	rep(i,1,n){
		if (mp[a[i]]==1) mp.clear(),++ans;
		rep(j,1,46) mp[f[j]-a[i]]=1;
	}
	printf("%lld",ans);
}
