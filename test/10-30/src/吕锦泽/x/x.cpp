#include<map>
#include<queue>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 100005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,l1,l2,r1,r2,ans,a[N],b[N<<3],c[N];
void build(ll l,ll r,ll p){
	if (l==r) { b[p]=0; return; }
	ll mid=l+r>>1; b[p]=0;
	build(l,mid,p<<1); build(mid+1,r,p<<1|1);
}
void modfiy(ll l,ll r,ll pos,ll v,ll p){
	if (l==r) { b[p]+=v; return; }
	ll mid=l+r>>1;
	if (pos<=mid) modfiy(l,mid,pos,v,p<<1);
	else modfiy(mid+1,r,pos,v,p<<1|1);
	b[p]=b[p<<1]+b[p<<1|1];
}
ll query(ll l,ll r,ll s,ll t,ll p){
	if (l==s&&r==t) return b[p];
	ll mid=l+r>>1;
	if (t<=mid) return query(l,mid,s,t,p<<1);
	else if (s>mid) return query(mid+1,r,s,t,p<<1|1);
	else return query(l,mid,s,mid,p<<1)+query(mid+1,r,mid+1,t,p<<1|1);
}
ll solve(ll x){
	build(-100000,100000,1);
	c[0]=0; ans=0;
	rep(i,1,n){
		c[i]=c[i-1]+(a[i]<=x?1:-1);
		if (i>=l2) modfiy(-100000,100000,c[i-l2],1,1);
		if (r2+1<=i) modfiy(-100000,100000,c[i-r2-1],-1,1);
		ans+=query(-100000,100000,-100000,c[i],1);
	}
	return ans;
}
int main(){
	freopen("x.in","r",stdin);
	freopen("x.out","w",stdout);
	n=read(); rep(i,1,n) a[i]=read();
	m=read();
	while (m--){
		l1=read(); r1=read();
		l2=read(); r2=read();
		printf("%lld\n",solve(r1)-solve(l1-1));
	}
}
/*
5
4 1 2 2 5
5
2 4 2 3
2 4 1 3
2 5 2 5
3 5 2 5
1 5 3 5

*/
