#include<bits/stdc++.h>
using namespace std;
int n,m,L,R,l,r,mid,ans;
int a[100001],b[100001];
int main(){
	freopen("x.in","r",stdin);
	freopen("x.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)
		scanf("%d",&a[i]);
	scanf("%d",&m);
	while(m--){
		ans=0;
		scanf("%d%d%d%d",&L,&R,&l,&r);
		for(int i=l;i<=r;i++){
			for(int j=1;j<=n-i+1;j++){
				for(int k=j;k<=j+i-1;k++)
					b[k-j+1]=a[k];
				sort(b+1,b+i+1);
				mid=i%2==0?i/2:i/2+1;
				if(b[mid]>=L&&b[mid]<=R)
					ans++;
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
