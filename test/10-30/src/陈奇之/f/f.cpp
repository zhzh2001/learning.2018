#include<bits/stdc++.h>
using namespace std;
#define int ll
typedef long long ll;
const int maxn = 100500;
ll f[105];
int n,a[maxn];
inline ll read() {
    ll x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
const int mod = 1e6+7;
int first[mod+2],val[2*maxn],nxt[2*maxn];
int s[maxn],top=0,tot=0;
void clear(){
	while(top){
		first[s[top]] = -1;
		top--;
	}
	tot = 0;
}
bool query(ll x){
	int w = x%mod;
	for(int i=first[w];~i;i=nxt[i]){
		if(x==val[i]) return true;
	}
	return false;
}
bool check(ll x){
	for(int i=1;i<=42;++i){
		ll more = f[i] - x;
		if(more < 0) continue;
		if(query(more)) return false;
	}
	return true;
}
void add(ll x){
	if(query(x)) return ;
	int w = x % mod;
	nxt[tot] = first[w];
	val[tot] = x;
	first[w] = tot++;
	s[++top] = w;
}
int ans = 0;
signed main(){
	freopen("f.in","r",stdin);
	freopen("f.out","w",stdout);
	memset(first,-1,sizeof(first));
	n = read();
	f[1] = f[2] = 1;
	for(int i=3;i<=42;++i)
		f[i] = f[i-1] + f[i-2];
	for(int i=1;i<=n;++i) a[i] = read();
	for(int i=1;i<=n;++i){
		if(check(a[i])){
			add(a[i]);
		} else{
			clear();
			add(a[i]);
			ans++;
		}
	}ans++;
	printf("%lld\n",ans);
	return 0;
}
