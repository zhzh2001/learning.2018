#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
ll k,R,p[29],p1[29],p2[28];
ll ans,mx;
ll a[14000005];
ll b[14000005];
inline void dfs1(int last,ll v){
	if(last == p1[0]+1){
		a[++a[0]] = v;
//		assert(a[0] <= 15000000);
		return ;
	}
	ll res = 1;
	for(;;){
		if((long double) v * res > 2 * R) break;
		if(v * res > R) break;
		dfs1(last+1,v*res);
		if((long double) res * p1[last] > 2 * R) break;
		if(res * p1[last] > R) break;
		res = res * p1[last];
	}
} 
inline void dfs2(int last,ll v){
	if(last == p2[0]+1){
		b[++b[0]] = v;
		return ;
	}
	ll res = 1;
	for(;;){
		if((long double) v * res > 2 * R) break;
		if(v * res > R) break;
		dfs2(last+1,v*res);
		if((long double) res * p2[last] > 2 * R) break;
		if(res * p2[last] > R) break;
		res = res * p2[last];
	}
} 
int main(){
	freopen("h.in","r",stdin);
	freopen("h.out","w",stdout);
	scanf("%lld%lld",&k,&R);
	for(int i=1;i<=k;++i){
		scanf("%lld",&p[i]);
	}
	sort(p+1,p+1+k);
	p1[0]=p2[0]=0;
	for(int i=1;i<=k;++i){
		if(i!=k){
			if(i&1){
				p1[++p1[0]] = p[i];
			} else{
				p2[++p2[0]] = p[i];
			}
		} else{
			p2[++p2[0]] = p[i];
		}
	}
	a[0] = 0;dfs1(1,1);
	b[0] = 0;dfs2(1,1);
//	printf("%d %d\n",a[0],b[0]);
	sort(a+1,a+1+a[0]);sort(b+1,b+1+b[0]);
	register int j = b[0];
	for(register int i=1;i<=a[0];++i){
		while((j >= 1) && (((long double)a[i] * b[j] > 2*R) || (a[i] * b[j] > R))){
			j--;
		}if(!j) break;
		mx = max(mx,a[i] * b[j]);
		ans += j;
	}
	//f[i]表示最小质因子为i，最小的数是多少
	//找到这里面最小的数 
	printf("%lld\n",mx);
	printf("%lld\n",ans);
	return 0;
}
/*

12 1000000000000000000
2 5 11 17 23 31 41 47 59 67 73 83 97

13 1000000000000000000
3 7 13 19 29 37 43 53 61 71 79 89 97

24 1000000000000000000
2 3
5 7
11 13
17 19
23 29
31 37
41 43
47 53
59 61
67 71
73 79
89 97
*/
