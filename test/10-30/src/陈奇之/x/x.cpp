/*

区间长度为L

需要有(L+1)/2-1个小于等于它的数（不包括他），也就是要有(L+1)/2个小于等于他的数字（包括他）
同时也必须要有L/2+1个大于等于他的数字（包括他）

考虑暴力的做法

1、枚举中位数

把小于等于的设为1 

考虑有多少区间中位数小于等于这个值。

中位数小于等于这个值的，当且仅当区间中1的和大于等于(L+1)/2。

1、计算中位数小于等于R1的有多少个，小于等于L1-1的有多少个，减一下就行了。

solution： 
小于等于x的时候 

把小于等于x的设为1。
问题是，有多少区间长度为L的区间，区间和大于等于(L+1)/2 
*/
#include<bits/stdc++.h>
typedef long long ll;
using namespace std;
const int maxn = 2e5+233;
int _a[maxn],sum[maxn],a[maxn];
int n,m;
#define lowbit(x) ((x & (-x)))
ll c[2][maxn],f[maxn];
void add(ll c[],int x,ll v){
	for(;x<=2*n;x+=lowbit(x)){
		c[x] += v;
	}
}
ll query(ll c[],int x){
	ll ans = 0;
	for(;x;x-=lowbit(x)){
		ans += c[x];
	}
	return ans;
}
ll solve(int x,int l,int r){
	ll ans = 0;
	for(int i=1;i<=n;++i) if(_a[i]<=x) a[i] = 1; else a[i] = 0;
	sum[0] = 0;
	for(int i=1;i<=n;++i) sum[i] = sum[i-1] + a[i];
	for(int i=0;i<=n;++i) f[i] = sum[i] - i/2 + n;
//	for(int i=0;i<=n;++i){
//		printf("f[%d] = %lld\n",i,f[i]);
//	}
	for(int i=0;i<=2*n;++i) c[0][i] = c[1][i] = 0;
//维护sum[j] - sum[i] - (j-i+1)/2 >= 0(i < j)
/*如果i和j同奇偶
sum[j] - sum[i] - (j / 2 - i / 2) >= 0
sum[j] - j/2  >= sum[i] - i/2
如果j偶数，i奇数或者j奇i偶数，那么是1 */
/*l<=j-i j-i<=r
i<=j-l i>=j-r
j-r<=i<=j-l*/
 
	for(int x=1;x<=n;++x){
		if(x - l >= 0){
			if((x-l)&1){
				add(c[1],f[x-l],1);
			} else{
				add(c[0],f[x-l],1);
			}
		}
		if(x - r - 1 >= 0){
			if((x-r-1)&1){
				add(c[1],f[x-r-1],-1);
			} else{
				add(c[0],f[x-r-1],-1);
			}
		}
		if(x & 1){
			ans += query(c[0],f[x]-1);
			ans += query(c[1],f[x]);
		}//枚举右端点 
		else{
			ans += query(c[0],f[x]);
			ans += query(c[1],f[x]);
		}
	}
//	printf("return ans\n");
	return ans;
}
/*也就是，除了i是奇数，j是偶数，剩下的[l,r]的贡献都是
sum[r] - sum[l]  -  (r/2 - l/2) >= 1的个数 
(sum[r] - r/2)  -  (sum[l] - l/2)  >= 1的个数
r是奇数
f[r] - f[l] >= 1
f[l] <= f[r] - 1

r是偶数，讨论l是不是奇数
如果l是奇数，要统计
sum[r] - sum[l]  -  (r/2 - l/2) >= 0的个数 
(sum[r] - r/2)  -  (sum[l] - l/2) >= 0的个数 */
inline ll read() {
    ll x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
int main(){
	freopen("x.in","r",stdin);
	freopen("x.out","w",stdout); 
	n = read();
	for(int i=1;i<=n;++i) _a[i] = read();
	m = read();
	while(m--){
		int l1,r1,l2,r2;
		l1 = read(),r1 = read(),l2 = read(),r2 = read();
		printf("%lld\n",solve(r1,l2,r2)-solve(l1-1,l2,r2));
	}
	return 0;
}
