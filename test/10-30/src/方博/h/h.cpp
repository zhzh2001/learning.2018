#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	ll x=0,f=1,ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
#define ll long long
queue<ll>q;
map<ll,ll>vis;
ll n,r,p[30],ans,anst;
int main()
{
	freopen("h.in","r",stdin);
	freopen("h.out","w",stdout);
	n=read();r=read();
	for(int i=1;i<=n;i++)
		p[i]=read(),q.push(p[i]),vis[p[i]]=true; 
	while(!q.empty()){
		ll k=q.front();
		q.pop();
		if(k>anst)anst=k;
		ans++;
		ll ma=r/k;
		for(int i=1;i<=n;i++)
			if(p[i]>ma)break;
			else {
				int to=p[i]*k;
				if(vis[to])continue;
				else q.push(to),vis[to]=true;
			}	
	}
	writeln(anst);
	writeln(ans+1);
}
