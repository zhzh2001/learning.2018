#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	ll x=0,f=1,ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=100005;
ll a[N];
ll n;
ll fib[105],cnt;
//map<int,int>mmp;
set<ll>s;
int main()
{
	freopen("f.in","r",stdin);
	freopen("f.out","w",stdout);
	ll x=1,y=1;
	while(x<=2e9){
		fib[++cnt]=x;
		x=x+y;
		swap(x,y);
	}
	n=read();
	for(int i=1;i<=n;i++)
		a[i]=read();
	ll ans=1;ll ma=0;
	for(int i=1;i<=n;i++){
		bool bb=true;
//		cout<<i<<' '<<fib[cnt]<<endl;
		for(int j=cnt;j;j--){
//			cout<<cnt<<' '<<j<<endl;
//			cout<<(a[i])<<' '<<fib[j]<<' '<<(a[i]>=fib[j])<<endl;
//			system("pause");
			if(a[i]>=fib[j])break;
			if(fib[j]-a[i]>ma)continue;
/*			if(mmp[fib[j]-a[i]]==ans){
				bb=false;
				break;
			}*/
			if(s.find(fib[j]-a[i])!=s.end()){
				bb=false;
				break;
			}
		}
		if(!bb){
			ans++;
			s.clear();
//			mmp.clear();
		}
		s.insert(a[i]);
		ma=max(a[i],ma);
//		mmp[a[i]]=ans;
	}
	writeln(ans);
	return 0;
}
