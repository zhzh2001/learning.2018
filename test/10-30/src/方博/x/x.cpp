#include<bits/stdc++.h>
#define ll long long
inline ll read()
{
	ll x=0,f=1,ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
using namespace std;
const int N=100005;
const int M=100000;
ll n,m,a[N],s[N];
ll l1,r1,l2,r2;
struct tree{
	ll s;
}t[N*16];
void build(int k,int l,int r)
{
	t[k].s=0;
	if(l==r)return;
	int mid=l+r>>1;
	build(k<<1,l,mid),build(k<<1|1,mid+1,r);
}
void change(int k,int l,int r,int p,int v)
{
	if(l==r){
		t[k].s+=v;
		return;
	}
	int mid=l+r>>1;
	if(p<=mid)change(k<<1,l,mid,p,v);
	else change(k<<1|1,mid+1,r,p,v);
	t[k].s=t[k<<1].s+t[k<<1|1].s;
}
ll query(int k,int l,int r,int x,int y)
{
	if(l==x&&r==y)return t[k].s;
	int mid=l+r>>1;
	if(y<=mid)return query(k<<1,l,mid,x,y);
	else if(x>mid)return query(k<<1|1,mid+1,r,x,y);
	else return query(k<<1,l,mid,x,mid)+query(k<<1|1,mid+1,r,mid+1,y);
}
inline ll solve(ll x)
{
	build(1,-M,M);
	s[0]=0;
	ll ret=0;
	for(int i=1;i<=n;i++){
		if(a[i]<=x)s[i]=s[i-1]+1;
		else s[i]=s[i-1]-1;
		if(i>=r2+1)change(1,-M,M,s[i-r2-1],-1);
		if(i>=l2)change(1,-M,M,s[i-l2],1);
		ret+=query(1,-M,M,-M,s[i]);
	}
	return ret;
}
int main()
{
	freopen("x.in","r",stdin);
	freopen("x.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)
		a[i]=read();
	m=read();
	for(int i=1;i<=m;i++){
		l1=read();r1=read();l2=read();r2=read();
		writeln(solve(r1)-solve(l1-1));
	}
	return 0;
}
/*
5
4 1 2 2 5
5
2 4 2 3
2 4 1 3
2 5 2 5
3 5 2 5
1 5 3 5
*/
