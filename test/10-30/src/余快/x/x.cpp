/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
#define N 200055
//#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
//#define getchar gc
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(ll x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(ll x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int n,m,a[N],l,r,x,y,b[N],sum[N];
ll ans;
struct szsz{
	int mx,t[N];
	void init(int x){
		mx=x;
	}
	void clear(){
		F(i,0,mx) t[i]=0;
	}
	void add(int x,int k){
		for (;x<=mx;x+=x&-x) t[x]+=k;
	}
	int query(int x){
		int sum=0;
		for (;x;x-=x&-x) sum+=t[x];
		return sum;
	}
}tr;
int solve1(int x,int s,int t){
	tr.clear();
	F(i,1,n) b[i]=(a[i]>=x)?1:-1;sum[0]=0;
	F(i,1,n) sum[i]=sum[i-1]+b[i];
	F(i,0,n) sum[i]=sum[i]+n+2;
	//查询一个区间<=一个数的..,树状数组
	int l=0,r=0;ll ans=0;
	F(i,1,n){
		if (r+s<=i){
			tr.add(sum[r],1);r++;
		}
		if (i-l+1>t){
			if (l) tr.add(sum[l-1],-1);
			l++;
		}
		ans+=tr.query(sum[i]-1);
	}
	return ans;
}
int solve2(int x,int s,int t){
	tr.clear();
	F(i,1,n) b[i]=(a[i]<=x)?1:-1;sum[0]=0;
	F(i,1,n) sum[i]=sum[i-1]+b[i];
	F(i,0,n) sum[i]=sum[i]+n+2;
	//查询一个区间<=一个数的..,树状数组
	int l=0,r=0;ll ans=0;
	F(i,1,n){
		if (r+s<=i){
			tr.add(sum[r],1);r++;
		}
		if (i-l+1>t){
			if (l) tr.add(sum[l-1],-1);l++;
		}
		ans+=tr.query(sum[i]);
	}
	return ans;
}
signed main(){
	freopen("x.in","r",stdin);
	freopen("x.out","w",stdout);
	n=read();tr.init(n*2+3);
	F(i,1,n) a[i]=read();
	m=read();
	//算出>=l的，再算出<=r的，加起来减去所有即可
	F(i,1,m){
		l=read();r=read();x=read();y=read();
		ans=solve1(l,x,y);
		ans+=solve2(r,x,y);
		F(j,n-y+1,n-x+1) ans-=j;
		wrn(ans);
	}
	return 0;
}
