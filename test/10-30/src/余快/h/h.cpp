/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
#define N 10000055
//#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
//#define getchar gc
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int n,m,p[N],x;
ll mx,ans,num,pa1,pb,z[N],z2[N],num2;
void dfs(ll x,int k){
	if (k==n+1) return;
	z[++num]=x;ans=max(ans,x);
	if (x>mx/p[k]) return;
	dfs(x*p[k],k);
	F(i,k+1,n){
		if (x>mx/p[i]) return;
		dfs(x*p[i],i);
	}
}
signed main(){
	freopen("h.in","r",stdin);
	freopen("h.out","w",stdout);
	m=n=read();scanf("%lld",&mx);
	F(i,1,n) p[i]=read();sort(p+1,p+n+1);
	if (n<=8){
		dfs(1,1);
		printf("%lld\n%lld\n",ans,num);
		return 0;
	}
	n=8;dfs(1,1);
	sort(z+1,z+num+1);
	num2=num;
	F(i,1,num2) z2[i]=z[i];num=0;
	n=m;dfs(1,9);
	sort(z+1,z+num+1);z2[0]=-1;
	int t=num2;
	ll sum=0;
	F(i,1,num){
		while (z[i]>mx/z2[t]&&t) t--;
		sum+=t;
		ans=max(ans,z2[t]*z[i]);
	}
	printf("%lld\n%lld\n",ans,sum);
	return 0;
}
