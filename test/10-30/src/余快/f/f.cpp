/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
#define N 500055
#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
//#define getchar gc
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int n,m,ans,fl[N],fr[N],a[N];
ll f[N];
const int mo=20000007;
struct hah{
	int tot,hsh[mo+5],vis[mo+5];
	void clear(){tot++;}
	void ins(int x,int k){
		x=x%mo;
		while (vis[x]==tot&&a[hsh[x]]!=a[k]) x++;
		vis[x]=tot;hsh[x]=k;
	}
	int find(ll x){
		ll p=x;
		x=x%mo;
		while (vis[x]==tot&&a[hsh[x]]!=x) x++;
		if (vis[x]==tot&&a[hsh[x]]==p) return hsh[x];
		else return -1;
	}
}h;
signed main(){
	freopen("f.in","r",stdin);
	freopen("f.out","w",stdout);
	n=read();F(i,1,n) a[i]=read();h.tot++;
	f[0]=f[1]=1;F(i,2,45) f[i]=f[i-1]+f[i-2];
	F(i,1,n){
		F(j,1,45){
			if (f[j]>=a[i]) fl[i]=max(fl[i],h.find(f[j]-a[i]));
		}
		h.ins(a[i],i);
	}
	h.clear();
	D(i,n,1){
		fr[i]=n+1;
		F(j,1,45){
			if (f[j]>=a[i]){
				int t=h.find(f[j]-a[i]);
				if (t!=-1) fr[i]=min(fr[i],t);
			}
		}
		h.ins(a[i],i);
	}
	for (int l=1;l<=n;l++){
		int r=l,rmi=fr[l];
		while (r+1<rmi&&fl[r+1]<l&&r<n) r++;
		l=r;ans++;
	}
	wrn(ans);
	return 0;
}
