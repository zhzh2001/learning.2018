#include<bits/stdc++.h>
using namespace std;
const int N=100005,M=1432781;
int b[M],flag[M],a[N],f[N],num,n;
int find(int x,int y){
	int k=x%M;
	for (;flag[k]==y&&b[k]!=x;k=(k+1)%M);
	if (flag[k]==y)return -1;
	return k;
}
int main(){
	freopen("f.in","r",stdin);
	freopen("f.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++)scanf("%d",&a[i]);
	f[0]=f[1]=1;num=1;
	for (int i=2;f[i-1]<=2e9+5-f[i-2];i++)f[i]=f[i-1]+f[i-2],num++;
	int cnt=1,l=find(a[1],1);
	flag[l]=1;b[l]=a[1];
	for (int i=2;i<=n;i++){
		for (int j=1;j<=num;j++)
			if (f[j]>=a[i]&&find(f[j]-a[i],cnt)==-1){
				cnt++;
				break;
			}
		int l=find(a[i],cnt);
		if (l!=-1)b[l]=a[i],flag[l]=cnt;	
	}
	printf("%d\n",cnt);
	return 0;
}
