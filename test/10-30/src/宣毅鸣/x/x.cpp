#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int N=400005;
ll ans;
int a[N],n,m,l1,r1,l2,r2,cnt1[N],cnt2[N];
ll num[N];
void insert(int x,int y){
	for (;x;x-=x&-x)num[x]+=y;
}
ll find(int x){
	int ans=0;
	for (;x<N;x+=x&-x)ans+=num[x];
	return ans;
}
int main(){
	freopen("x.in","r",stdin);
	freopen("x.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++)scanf("%d",&a[i]);
	scanf("%d",&m);
	while (m--){
		scanf("%d%d%d%d",&l1,&r1,&l2,&r2);
		for (int i=1;i<=n;i++){
			cnt1[i]=cnt1[i-1];cnt2[i]=cnt2[i-1];
			if (a[i]<l1)cnt1[i]+=2;
			if (a[i]>r1)cnt2[i]+=2;
		}
		ans=0;
		memset(num,0,sizeof num);
		for (int i=l2-1;i<r2;i++)insert(cnt1[i]-i+n+1,1);
		for (int i=1;i<=n+1-l2;i++)
		 	ans+=min(n,i+r2-1)-(i+l2-1)+1;
		for (int i=1;i<=n+1-l2;i++){
			insert(cnt1[i+l2-2]-(i+l2-2)+n+1,-1);
			if (i+r2-1<=n)insert(cnt1[i+r2-1]-(i+r2-1)+n+1,1);
			ans-=find(cnt1[i-1]-i+2+n);
		}
		memset(num,0,sizeof num);
		for (int i=l2-1;i<r2;i++)insert(cnt2[i]-i+n+1,1);
		for (int i=1;i<=n+1-l2;i++){
			insert(cnt2[i+l2-2]-(i+l2-2)+n+1,-1);
			if (i+r2-1<=n)insert(cnt2[i+r2-1]-(i+r2-1)+n+1,1);
			ans-=find(cnt2[i-1]-i+3+n);
		}		
		printf("%lld\n",ans);
	}
	return 0;
}
