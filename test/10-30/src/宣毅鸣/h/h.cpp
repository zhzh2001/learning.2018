#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int N=1000005,K=26;
int a[K],k,dp1[K][N],dp2[K][N];
ll n,ans,ans2;
void dfs(int x,ll y){
	if (n/y<N){
		ans+=dp1[x][n/y];
		ans2=max(ans2,y*dp2[x][n/y]);
		return;
	}
	if (!x){
		ans++;
		return;
	}
	while (1){
		dfs(x-1,y);
		if (y>n/a[x])return;
		y*=a[x];
	}
}
signed main(){
	freopen("h.in","r",stdin);
	freopen("h.out","w",stdout);
	scanf("%d%lld",&k,&n);
	for (int i=1;i<=k;i++)scanf("%d",&a[i]);
	for (int i=1;i<=1e6;i++)dp1[0][i]=1,dp2[0][i]=1;
	for (int i=1;i<=k;i++)
		for (int j=1;j<=1e6;j++)
			dp1[i][j]=dp1[i-1][j]+dp1[i][j/a[i]],
			dp2[i][j]=max(dp2[i-1][j],dp2[i][j/a[i]]*a[i]);
	dfs(k,1);
	printf("%lld\n%lld",ans2,ans);
	return 0;
}
