#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<string>
#include<iostream>
#include<algorithm>
#include<cctype>
#include<cmath>
#include<ctime>
#include<queue>
#include<set>
#include<map>
#include<vector>
using namespace std;

namespace TYC
{
	const int N=100010;
	int n,m,a[N],b[N];

	inline int read()
	{
		int x=0,f=0;char ch=getchar();
		while(!isdigit(ch)) f|=(ch=='-'),ch=getchar();
		while(isdigit(ch)) x=x*10+ch-'0',ch=getchar();
		return f?-x:x;
	}

	namespace Subtask1
	{
		const int M=1010;
		int midnum[M][M];

		namespace Splay
		{
			int head,cnt=0;
			struct node
			{
				int val,size,num,fa,son[2];
			}tree[500010];

			#define ls tree[root].son[0]
			#define rs tree[root].son[1]
			#define ffa tree[root].fa

			inline void update(int root)
			{
				tree[root].size=tree[ls].size+tree[rs].size+tree[root].num;
			}

			inline dir(int root)
			{
				return tree[ffa].son[1]==root;
			}

			void rotate(int root)
			{
				int d=dir(root),gf=tree[ffa].fa,f=ffa;
				if(gf) tree[gf].son[dir(f)]=root;
				else head=root;
				tree[root].fa=gf;

				tree[f].son[d]=tree[root].son[d^1];
				if(tree[root].son[d^1])
					tree[tree[root].son[d^1]].fa=f;

				tree[root].son[d^1]=f;
				tree[f].fa=root;

				update(f);
				update(root);
			}

			void splay(int root)
			{
				for(;tree[root].fa;rotate(root))
					if(tree[ffa].fa)
						rotate(dir(root)==dir(ffa)?ffa:root);
			}

			inline int newnode(int v,int f)
			{
				++cnt;
				tree[cnt].size=1;
				tree[cnt].num=1;
				tree[cnt].val=v;
				tree[cnt].fa=f;
				tree[cnt].son[0]=tree[cnt].son[1]=0;
				return cnt;
			}

			void insert(int x)
			{
				if(!head) head=newnode(x,0);
				else
				{
					int root=head;
					while(1)
					{
						if(x<tree[root].val)
						{
							if(ls) root=ls;
							else
							{
								ls=newnode(x,root);
								splay(cnt);
								return;
							}
						}
						else if(x>tree[root].val)
						{
							if(rs) root=rs;
							else
							{
								rs=newnode(x,root);
								splay(cnt);
								return;
							}
						}
						else
						{
							tree[root].num++;
							tree[root].size++;
							splay(root);
							return;
						}
					}
				}
			}

			int findkth(int k)
			{
				int root=head;
				while(1)
				{
					if(k<=tree[ls].size) root=ls;
					else
					{
						k-=tree[ls].size;
						if(k<=tree[root].num) return tree[root].val;
						else k-=tree[root].num,root=rs;
					}
				}
			}
		}

		void work()
		{
			for(int s=1;s<=n;s++)
			{
				Splay::head=Splay::cnt=0;
				for(int t=s;t<=n;t++)
				{
					Splay::insert(a[t]);
					int len=t-s+1;
					midnum[s][t]=Splay::findkth((len+1)/2);
				}
			}

			m=read();
			while(m--)
			{
				int l1=read(),r1=read(),lenl=read(),lenr=read(),tot=0;
				for(int s=1;s<=n-lenl+1;s++)
					for(int len=lenl;len<=lenr;len++)
					{
						int t=s+len-1;
						if(t>n) break;
						if(midnum[s][t]>=l1&&midnum[s][t]<=r1) tot++;
					}
				printf("%d\n",tot);
			}
		}
	}

	void work()
	{
		n=read();
		for(int i=1;i<=n;i++) a[i]=read();
		Subtask1::work();
	}
}

int main()
{
	freopen("x.in","r",stdin);
	freopen("x.out","w",stdout);
	TYC::work();
	return 0;
}
