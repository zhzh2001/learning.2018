#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<string>
#include<iostream>
#include<algorithm>
#include<cctype>
#include<cmath>
#include<ctime>
#include<queue>
#include<set>
#include<map>
#include<vector>
using namespace std;

namespace TYC
{
	typedef long long ll;
	int n,p[50];
	ll Limit;
	
	namespace Subtask1
	{
		int tot,mx;
		bool vis[1000010];

		void dfs(int last,int now)
		{
			for(int i=last;i<=n;i++)
			{
				int tmp=now*p[i];
				if(tmp<=Limit&&!vis[tmp])
				{
					tot++;
					mx=max(mx,tmp);
					dfs(i,tmp);
				}
			}
		}
		void work()
		{
			dfs(1,1);
			printf("%d\n%d\n",mx,tot+1);
		}
	}
	
	namespace Subtask2
	{
		ll tot,mx;
		map<ll,int> vis;
		
		void dfs(int last,ll now)
		{
			for(int i=last;i<=n;i++)
			{
				ll tmp=now*p[i];
				if(tmp<=Limit&&!vis.count(tmp))
				{
					tot++;
					mx=max(mx,tmp);
					dfs(i,tmp);
				}
			}
		}
		void work()
		{
			dfs(1,1LL);
			printf("%lld\n%lld\n",mx,tot+1);
		}
	}

	void work()
	{
		scanf("%d%lld",&n,&Limit);
		for(int i=1;i<=n;i++) scanf("%d",&p[i]);
		sort(p+1,p+1+n);
		if(Limit<=1000005) Subtask1::work();
		else Subtask2::work();
	}
}

int main()
{
	freopen("h.in","r",stdin);
	freopen("h.out","w",stdout);
	TYC::work();
	return 0;
}
