#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<string>
#include<iostream>
#include<algorithm>
#include<cctype>
#include<cmath>
#include<ctime>
#include<queue>
#include<set>
#include<map>
#include<vector>
using namespace std;

namespace TYC
{
	typedef long long ll;
	const int N=100010;
	int n,last[N],f[N],fibo[N],a[N];
	map<ll,int> num;

	inline int read()
	{
		int x=0,f=0;char ch=getchar();
		while(!isdigit(ch)) f|=(ch=='-'),ch=getchar();
		while(isdigit(ch)) x=x*10+ch-'0',ch=getchar();
		return f?-x:x;
	}

	inline void init()
	{
		ll x=1,y=1;
		fibo[1]=1,fibo[2]=1;
		for(int i=3;i<=50;i++)
		{
			x=x+y;
			fibo[i]=x;
			swap(x,y);
		}
	}

	void work()
	{
		n=read();
		init();
		for(int i=1;i<=n;i++)
		{
			a[i]=read();
			for(int j=1;j<=50;j++)
			{
				ll tmp=fibo[j]-a[i];
				if((tmp>0&&num.count(tmp)))
					last[i]=max(last[i],num[tmp]);
			}
			num[a[i]]=i;
		}
		int near=0;
		for(int i=1;i<=n;i++)
		{
			if(near<last[i]) near=last[i];
			f[i]=f[near]+1;
		}
		printf("%d\n",f[n]);
	}
}

int main()
{
	freopen("f.in","r",stdin);
	freopen("f.out","w",stdout);
	TYC::work();
	return 0;
}
