#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
#define ld long double
#define int long long
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}
const int N=100005;
int tong[N<<1],n,m,b[N],a[N];
ll ask(int x,int y){
	memset(tong,0,sizeof(tong)); b[0]=N;
	int dq=0;
	ll ans=0;
	for(int i=1;i<=n;i++){
		if(a[i]<=x){
			b[i]=b[i-1]+1;
			dq+=tong[b[i]];
		}else{
			dq-=tong[b[i-1]];
			b[i]=b[i-1]-1;
		}
		if(i>=y){
			tong[b[i-y]]++; if(b[i-y]<=b[i])dq++;
		}
		ans+=dq;
	}
	return ans;
}
signed main(){
	freopen("x.in","r",stdin); freopen("x.out","w",stdout);
	n=read(); 
	for(int i=1;i<=n;i++)a[i]=read();
	m=read();
	while(m--){
		int l1=read(),r1=read(),l2=read(),r2=read();
		cout<<ask(r1,l2)-ask(l1-1,l2)-ask(r1,r2+1)+ask(l1-1,r2+1)<<endl;
	}
}
