#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define int long long
inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
#define ld long double
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}
const int mod=1000007,N=100005,tot=50;
int n,a[N],dp[N],fib[tot+5];
struct hash_table{
	int a[mod],b[mod];
	bool hash[mod];
	void insert(int x,int de){
		for(int i=x%mod;;i=(i+1==mod?0:i+1)){
			if(!hash[i]||a[i]==x){
				hash[i]=1; a[i]=x; b[i]+=de; return;
			}
		}
	}
	int query(int x){
		for(int i=x%mod;;i=(i+1==mod?0:i+1)){
			if(!hash[i]||a[i]==x){
				return b[i];
			}
		}
	}
}T;
bool check(int x){
	for(int i=0;i<=tot;i++)if(fib[i]>=x)if(T.query(fib[i]-x))return 1;
	return 0;
}
signed main(){
	freopen("f.in","r",stdin); freopen("f.out","w",stdout);
	n=read();
	fib[1]=1; for(int i=2;i<=tot;i++)fib[i]=fib[i-1]+fib[i-2]; 
	for(int i=1;i<=n;i++)a[i]=read();
	int dq=1;
	for(int i=1;i<=n;i++){
		while(check(a[i])){
			T.insert(a[dq],-1); dq++;
		}
		T.insert(a[i],1);
		dp[i]=dp[dq-1]+1;
	}
	cout<<dp[n]<<endl;
}
//ai������ 
