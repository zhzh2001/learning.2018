#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
#define ld long double
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}
const int N=10000005;
ll ans,mx,q[N],R,block;
int tot,n,a[30];
void dfs1(int p,ll dq){
	if(p>block){
		q[++tot]=dq; return;
	}
	while(1){
		dfs1(p+1,dq);
		if(dq>R/a[p])return;
		dq*=a[p];
	}
}
void dfs2(int p,ll dq){
	if(p>n){
		int pos=upper_bound(&q[1],&q[tot+1],R/dq)-q;
		ans+=pos-1; mx=max(mx,dq*q[pos-1]);
		return;
	}
	while(1){
		dfs2(p+1,dq);
		if(dq>R/a[p])return;
		dq*=a[p];
	}
}
signed main(){
	freopen("h.in","r",stdin); freopen("h.out","w",stdout);
	n=read(); R=read(); 
	for(int i=1;i<=n;i++)a[i]=read();
	sort(&a[1],&a[n+1]);
	block=min(n,8);
	dfs1(1,1);
	sort(&q[1],&q[tot+1]); 
	dfs2(block+1,1);
	cout<<mx<<endl<<ans<<endl;
}
