#include<bits/stdc++.h>
#define LL long long
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define fy 2000000000
#define N 100010
#define M 110
using namespace std;
LL sum[M];
LL n,m,i,j,k,l,r,ans;
LL read()
{
	LL x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
set<int> Q;
int main()
{
	freopen("f.in","r",stdin);
	freopen("f.out","w",stdout);
	sum[1]=1; sum[2]=2; m=2;
	while((k=sum[m]+sum[m-1])<=fy) sum[++m]=k;
	n=read();
	rep(i,1,n)
	{
		k=read(); 
		dep(j,m,1)
		{
			if(sum[j]<k) break; 
			if (Q.count(sum[j]-k))
			{
				ans++;
				Q.erase(Q.begin(),Q.end());
				break;
			}
		}
		Q.insert(k);
	}
	printf("%d",ans+1);
	return 0;
}
