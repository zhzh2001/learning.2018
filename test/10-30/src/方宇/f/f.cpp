#include<bits/stdc++.h>
#define LL long long
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define fy 20021218020021218
#define N 100010
#define M 110
using namespace std;
LL a[N],pre[N],sum[M];
LL n,m,i,j,k,l,r,ans;
map<LL,LL> Q;
LL read()
{
	LL x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
int main()
{
	freopen("f.in","r",stdin);
	freopen("f.out","w",stdout);
	n=read();
	rep(i,1,n)
	{
		a[i]=read(); r=max(r,a[i]);
		Q.insert(pair<LL,LL>(a[i],n+1));
	}
	sum[1]=1; sum[2]=2; m=2;
	while((k=sum[m]+sum[m-1])<=r*2) sum[++m]=k;
	map<LL,LL>::iterator iter;
	dep(i,n,1)
	{
		pre[i]=Q.find(a[i])->second;
		dep(j,m,1)
		{
		  if(sum[j]<=a[i]) break;
		  if((iter=Q.find(sum[j]-a[i]))!=Q.end()) iter->second=i;
		}
	}
	k=n+1;
	rep(i,1,n)
	{
		if(i>=k) ans++,k=pre[i];
		else k=min(k,pre[i]);
	}
	printf("%lld",ans+1);
	return 0;
}
