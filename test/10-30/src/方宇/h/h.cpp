#include<bits/stdc++.h>
#define LL long long
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define fy 20021218020021218
#define N 10000010
#define M 50
using namespace std;
LL n,k,f[N];
LL m,a[M],ans[N];
LL read()
{
	LL x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
int main(){
   freopen("h.in","r",stdin);
   freopen("h.out","w",stdout);
   n=read(); m=read();
   rep(i,1,n) a[i]=read();
   k=1; f[1]=1; ans[1]=1; 
   for(;;)
   {
      LL s=fy,boo=-1;
      rep(i,1,k)
      {
        if (f[i]<=n)
		{
           LL sum=ans[i]*a[f[i]];
           if(sum==ans[k]) f[i]++;
           if(f[i]>n) continue;
           sum=ans[i]*a[f[i]];
           if(sum<=m&&s>sum) s=sum,boo=i;
		}
      }
	  if (boo==-1) break;
	  k++; ans[k]=s; f[k]=1; f[boo]++;
   }
   printf("%lld\n",ans[k]);
   printf("%lld\n",k);
   return 0;
}
