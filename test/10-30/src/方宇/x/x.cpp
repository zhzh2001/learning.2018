#include<bits/stdc++.h>
#define LL long long
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define fy 20021218020021218
#define N 3010
using namespace std;
LL a[N],f[N][N];
LL n,m,i,j,k,l,r,x,y,ans;
map<LL,LL> Q;
map<LL,LL>::iterator iter,ator;
LL read()
{
	LL x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
int main()
{
	freopen("x.in","r",stdin);
	freopen("x.out","w",stdout);
	n=read();
	rep(i,1,n) a[i]=read();
	rep(i,1,n)
	{
		Q.erase(Q.begin(),Q.end());
		f[i][i]=a[i];
		Q.insert(pair<LL,LL>(a[i],1));
		iter=Q.find(a[i]); k=1;
		rep(j,i+1,n)
		{
			ator=Q.find(a[j]);
			if(ator==Q.end()) Q.insert(pair<LL,LL>(a[j],1));
			else ator->second++;
			if((j-i)%2==0) 
			{
				if(a[j]>iter->first) 
				{
					if(iter->second>k) k++;
					else {iter++; k=1;}
				}
				else if(a[j]==iter->first) k++;
			}
			else
			{
				if(a[j]<iter->first) 
				{
					if(k>1) k--;
					else {iter--; k=iter->second;}
				}
			}
			f[i][j]=iter->first;
		}
	}
	m=read();
	rep(k,1,m)
	{
		ans=0;
		l=read(); r=read(); x=read(); y=read();
		rep(i,x,y)
		  rep(j,1,n-i+1)
		    if(f[j][j+i-1]>=l&&f[j][j+i-1]<=r) ans++;
		printf("%lld\n",ans);
	}
	return 0;
}
