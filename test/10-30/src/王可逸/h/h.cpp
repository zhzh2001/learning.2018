#include <bits/stdc++.h>
#define int long long
using namespace std;
set <int> st;
int n,m,ans,mx;
inline int read() {
	register int x=0,f=1;
	register char ch=getchar();
	while(!(ch>='0' && ch<='9')) {if(ch=='-') f=-1;ch=getchar();}
	while(ch>='0' && ch<='9') {x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
bool ok(int x) {
	if(x==1) return true;
	for(int i=2;i*i<=x;i++) {
		if(x%i==0) {
			if(st.find(i)==st.end()) {
				return false;	
			}
			while(x%i==0) x/=i;
		}
		if(x==1) break;
	}
 	if(st.find(x)!=st.end() || x==1) return true;
	else return false;
}
signed main() {
	freopen("h.in","r",stdin);
	freopen("h.out","w",stdout);
	cin>>n>>m;
	for(register int i=1;i<=n;i++) {
		int x=read();
		st.insert(x);	
	}
	for(int i=1;i<=m;i++) {
		if(ok(i)) {
			ans++;
			mx=max(i,mx);
		}
	}
	cout<<mx<<'\n'<<ans;
	return 0;
}
//sxdakking
