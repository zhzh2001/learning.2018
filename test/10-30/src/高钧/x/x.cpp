#include <bits/stdc++.h>
using namespace std;
const int N=100005;
int n,Q,a[N];
struct larg
{
	int size,heap[N];
	inline void Up(int x){while(x>1){if(heap[x]>heap[x/2]){swap(heap[x],heap[x/2]); x/=2;}else break;}}
	inline void Down(int x){int y=x*2;while(y<=size){if(y<size&&heap[y]<heap[y+1])y++;if(heap[x]<heap[y]){swap(heap[x],heap[y]); x=y; y=x*2;}else break;}}
	inline void Insert(int v){heap[++size]=v; Up(size);}
	inline int Top(){return heap[1];}
	inline void Pop(){swap(heap[1],heap[size]); size--; Down(1);}
	inline void clear(){size=0;}
}q1;
struct smal
{
	int size,heap[N];
	inline void Up(int x){while(x>1){if(heap[x]<heap[x/2]){swap(heap[x],heap[x/2]);x/=2;}else break;}}
	inline void Down(int x){int y=x*2;while(y<=size){if(y<size&&heap[y]>heap[y+1])y++;if(heap[x]>heap[y]){swap(heap[x],heap[y]); x=y; y=x*2;}else break;}}
	inline void Insert(int v){heap[++size]=v; Up(size);}
	inline int Top(){return heap[1];}
	inline void Pop(){swap(heap[1],heap[size]); size--; Down(1);}
	inline void clear(){size=0;}
}q2;
inline void inser(int v)
{
	int tmp;
	if(q1.size==q2.size+1)
	{
		if(v<q1.Top()){tmp=q1.Top(); q1.Pop(); q2.Insert(tmp); q1.Insert(v);}else q2.Insert(v);
	}else{q2.Insert(v); tmp=q2.Top(); q2.Pop(); q1.Insert(tmp);}
}
int main()
{
	freopen("x.in","r",stdin);
	freopen("x.out","w",stdout);
	int i,j,l1,r1,l2,r2,re; scanf("%d",&n); for(i=1;i<=n;i++)scanf("%d",&a[i]); scanf("%d",&Q);
	while(Q--)
	{
		scanf("%d%d%d%d",&l1,&r1,&l2,&r2); re=0;
		for(i=1;i+l2-1<=n;i++)
		{
			q1.clear(); q2.clear(); for(j=i;j<i+l2-1;j++)inser(a[j]);
			for(j=i+l2-1;j<=i+r2-1&&j<=n;j++){inser(a[j]); if(q1.Top()>=l1&&q1.Top()<=r1)re++;}
		}printf("%d\n",re);
	}
}
