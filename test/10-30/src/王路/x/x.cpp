#pragma GCC optimize 2
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <vector>
#include <set>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

namespace IO {
const int kBufSize = 1 << 20;
char buf[kBufSize], *S = buf, *T = buf;
inline bool Fetch() { return (T = (S = buf) + fread(buf, 1, kBufSize, stdin)), S != T; }
inline char NC() { return (S == T ? (Fetch() ? *S++ : EOF) : *S++); }
bool isneg;
template <typename Int> inline void RI(Int &x) {
	x = isneg = 0;
	char ch;
	for (ch = NC(); isspace(ch); ch = NC())
		;
	if (ch == '-') {
		ch = NC(), isneg = 1;	
	}
	for (; isdigit(ch); ch = NC())
		x = x * 10 + ch - '0';
	if (isneg) x = -x;
}
} // namespace IO

using IO::RI;

const int kMaxN = 1e5 + 5;
int n, m, a[kMaxN];

set<pii> S;

int l1, r1, l2, r2;

inline int Check(int x, int l, int r) {
	int len = r - l + 1;
	if (len < l2 || len > r2)
		return 0;
//	fprintf(stderr, "%d %d %d\n", l, r, x);
	return x >= l1 && x <= r1;
}

void Solve1() {
	int ans = 0;
//	fprintf(stderr, "Process %d %d %d %d\n", l1, r1, l2, r2);
	for (int i = 1; i <= n; ++i) {
		S.clear();
		S.insert(make_pair(a[i], i));
		set<pii>::iterator it = S.begin();
		int cnt = 1;
		ans += Check(it->first, i, i);
		for (int j = i + 1; j <= n; ++j) {
			++cnt;
			S.insert(make_pair(a[j], j));
			if (cnt & 1) {
				if (a[j] >= it->first) {
					++it;
				}
			} else {
				if (a[j] < it->first) {
					--it;
				}
			}
			ans += Check(it->first, i, j);
		}
	}
	printf("%d\n", ans);
}

int main() {
	freopen("x.in", "r", stdin);
	freopen("x.out", "w", stdout);
	RI(n);
	for (int i = 1; i <= n; ++i) {
		RI(a[i]);
	}
	RI(m);
	while (m--) {
		RI(l1), RI(r1), RI(l2), RI(r2);
		if (n <= 1000) {
			Solve1(); // 30 pts
		}
	}
	return 0;
}
