#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <vector>
#include <set>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

namespace IO {
const int kBufSize = 1 << 20;
char buf[kBufSize], *S = buf, *T = buf;
inline bool Fetch() { return (T = (S = buf) + fread(buf, 1, kBufSize, stdin)), S != T; }
inline char NC() { return (S == T ? (Fetch() ? *S++ : EOF) : *S++); }
bool isneg;
template <typename Int> inline void RI(Int &x) {
	x = isneg = 0;
	char ch;
	for (ch = NC(); isspace(ch); ch = NC())
		;
	if (ch == '-') {
		ch = NC(), isneg = 1;	
	}
	for (; isdigit(ch); ch = NC())
		x = x * 10 + ch - '0';
	if (isneg) x = -x;
}
} // namespace IO

using IO::RI;

const int kMaxK = 33;
int K, a[kMaxK];
ll R;

namespace SubTask1 {
const int kSubR = 1e6 + 5;
ll ans, mx;
set<ll> S;
void DFS(int d, ll cur) {
	if (cur <= R) {
		if (S.count(cur))
			return;
		S.insert(cur);
	}
	for (int i = 1; i <= K; ++i) {
		if (cur * a[i] <= R) {
			DFS(d + 1, cur * a[i]);
		} else break;
	}
}
int Solve() {
	DFS(0, 1);
	printf("%lld\n%lld\n", *S.rbegin(), S.size());
	return 0;
}
} // namespace SubTask1

int main() {
	freopen("h.in", "r" ,stdin);
	freopen("h.out", "w", stdout);
	RI(K), RI(R);
	for (int i = 1; i <= K; ++i) {
		RI(a[i]);
	}
//	if (K <= 10 && R <= 1e6)
		return SubTask1::Solve();
	return 0;
}
