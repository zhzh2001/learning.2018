#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <vector>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

namespace IO {
const int kBufSize = 1 << 20;
char buf[kBufSize], *S = buf, *T = buf;
inline bool Fetch() { return (T = (S = buf) + fread(buf, 1, kBufSize, stdin)), S != T; }
inline char NC() { return (S == T ? (Fetch() ? *S++ : EOF) : *S++); }
bool isneg;
template <typename Int> inline void RI(Int &x) {
	x = isneg = 0;
	char ch;
	for (ch = NC(); isspace(ch); ch = NC())
		;
	if (ch == '-') {
		ch = NC(), isneg = 1;	
	}
	for (; isdigit(ch); ch = NC())
		x = x * 10 + ch - '0';
	if (isneg) x = -x;
}
} // namespace IO

using IO::RI;

const int kMaxK = 33;
int K, a[kMaxK];
ll R;

namespace SubTask1 {
const int kSubR = 1e6 + 5;
ll ans, mx;
bool vis[kSubR];
void DFS(int d, ll cur) {
	if (cur <= R) {
		if (vis[cur])
			return;
		vis[cur] = true;
		++ans;
		mx = max(mx, cur);	
	}
	for (int i = 1; i <= K; ++i) {
		if (cur * a[i] <= R) {
			DFS(d + 1, cur * a[i]);
		} else break;
	}
}
int Solve() {
	DFS(0, 1);
	printf("%lld\n%lld\n", mx, ans);
	return 0;
}
} // namespace SubTask1

namespace Solution {
const int kLimit = 1e6 + 5;
ll p[kMaxK][70], c[kMaxK], lim;
bool vis[kLimit];
ll ou[kLimit];

void DFS1(int d, ll cur) {
	if (cur <= lim) {
		if (vis[cur])
			return;
		vis[cur] = true;
		ou[++ou[0]] = cur;
	}
	for (int i = 1; i <= K; ++i) {
		if (cur * a[i] <= R) {
			DFS1(d + 1, cur * a[i]);
		} else break;
	}	
}

int Solve() {
	for (int i = 1; i <= K; ++i) {
		p[i][0] = 1;
		for (int j = 1; ; ++j) {
			p[i][j] = p[i][j - 1] * a[i];
			c[i] = j;
			if (1.0 * R < 1.0 * p[i][j] * a[i])
				break;
		}
	}
	// for the first task, use meet-in-middle to solve it in sqrt(R) times,
	// then checkmax
	lim = sqrt(R) + 1;
	DFS1(0, 1);
	sort(ou + 1, ou + ou[0] + 1);
	int low = max(ou[0] - 100, 0LL);
	ll ans1 = 0;
	for (int r = ou[0]; r >= low; --r) {
		for (int l = r; l >= low; --l) {
			if (ou[r] * ou[l] <= R)
				ans1 = max(ans1, ou[r] * ou[l]);
		}
	}
	printf("%lld\n", ans1);
	fflush(stdout);
	// for the second task, how to solve it ?????
	printf("%lld\n", 1LL * ((ou[0] + 1) * ou[0]) / 2);
	return 0;
}
} // namespace SubTask2

int main() {
	freopen("h.in", "r" ,stdin);
	freopen("h.out", "w", stdout);
	RI(K), RI(R);
	for (int i = 1; i <= K; ++i) {
		RI(a[i]);
	}
	if (K <= 10 && R <= 1e6)
		return SubTask1::Solve(); // 30 pts
	return Solution::Solve(); // 1e12, 30 pts ???
}
