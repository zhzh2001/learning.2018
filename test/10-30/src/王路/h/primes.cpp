#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <vector>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

namespace IO {
const int kBufSize = 1 << 20;
char buf[kBufSize], *S = buf, *T = buf;
inline bool Fetch() { return (T = (S = buf) + fread(buf, 1, kBufSize, stdin)), S != T; }
inline char NC() { return (S == T ? (Fetch() ? *S++ : EOF) : *S++); }
bool isneg;
template <typename Int> inline void RI(Int &x) {
	x = isneg = 0;
	char ch;
	for (ch = NC(); isspace(ch); ch = NC())
		;
	if (ch == '-') {
		ch = NC(), isneg = 1;	
	}
	for (; isdigit(ch); ch = NC())
		x = x * 10 + ch - '0';
	if (isneg) x = -x;
}
} // namespace IO

const int kMaxK = 33;
int K, a[kMaxK];
ll R;
bool vis[105];

int main() {
	int cnt = 0;
	for (int i = 2; i <= 100; ++i) {
		if (!vis[i]) {
			printf("%d ", i);	
			++cnt;
		}
		for (int j = i + i; j <= 100; j += i)
			vis[j] = true;
	}
	printf("cnt = %d\n", cnt);
	return 0;
}

/*
2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97

2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71 73 79 83 89 97
*/
