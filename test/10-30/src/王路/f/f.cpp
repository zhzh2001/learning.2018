#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <vector>
#include <set>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

namespace IO {
const int kBufSize = 1 << 20;
char buf[kBufSize], *S = buf, *T = buf;
inline bool Fetch() { return (T = (S = buf) + fread(buf, 1, kBufSize, stdin)), S != T; }
inline char NC() { return (S == T ? (Fetch() ? *S++ : EOF) : *S++); }
bool isneg;
template <typename Int> inline void RI(Int &x) {
	x = isneg = 0;
	char ch;
	for (ch = NC(); isspace(ch); ch = NC())
		;
	if (ch == '-') {
		ch = NC(), isneg = 1;	
	}
	for (; isdigit(ch); ch = NC())
		x = x * 10 + ch - '0';
	if (isneg) x = -x;
}
} // namespace IO

using IO::RI;

const int kMaxN = 1e5 + 5, kLimit = 105;
int n, a[kMaxN], f[kMaxN];
ll fib[kLimit];
set<ll> S;
multiset<ll> cur;
pii q[kMaxN];
int h = 0, t = 0;
inline void PushBack(int x, int pos) {
	q[t++] = make_pair(x, pos);
	cur.insert(x);
}
inline void PopFront() {
	int val = q[h++].first;
	cur.erase(cur.find(val));
}
inline pii Shift() {
	cur.erase(cur.find(q[h].first));
	return q[h++];
}
inline bool Check(int x) {
	for (int i = 1; i <= 44; ++i) {
		if (cur.find(fib[i] - x) != cur.end())
			return false;
	}
	return true;
}
int main() {
	freopen("f.in", "r", stdin);
	freopen("f.out", "w", stdout);
	RI(n);
	for (int i = 1; i <= n; ++i) {
		RI(a[i]);
	}
	fib[0] = fib[1] = 1;
	S.insert(1);
	for (int i = 2; i <= 44; ++i) {
		fib[i] = fib[i - 1] + fib[i - 2];
		S.insert(fib[i]);
//		fprintf(stderr, "%d\n", fib[i]);
	}
	f[0] = 0, f[1] = 1;
	PushBack(a[1], 1);
	for (int i = 2; i <= n; ++i) {
		for (; h != t && !Check(a[i]); PopFront())
			;
		PushBack(a[i], i);
		if (h != t) {
			f[i] = f[q[h].second - 1] + 1;
		} else {
			f[i] = 1;
		}
//		fprintf(stderr, "%d%c", f[i], " \n"[i == n]);
	}
	printf("%d\n", f[n]);
	return 0;
}
