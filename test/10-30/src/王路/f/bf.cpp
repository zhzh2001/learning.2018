#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <vector>
#include <set>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

namespace IO {
const int kBufSize = 1 << 20;
char buf[kBufSize], *S = buf, *T = buf;
inline bool Fetch() { return (T = (S = buf) + fread(buf, 1, kBufSize, stdin)), S != T; }
inline char NC() { return (S == T ? (Fetch() ? *S++ : EOF) : *S++); }
bool isneg;
template <typename Int> inline void RI(Int &x) {
	x = isneg = 0;
	char ch;
	for (ch = NC(); isspace(ch); ch = NC())
		;
	if (ch == '-') {
		ch = NC(), isneg = 1;	
	}
	for (; isdigit(ch); ch = NC())
		x = x * 10 + ch - '0';
	if (isneg) x = -x;
}
} // namespace IO

using IO::RI;

const int kMaxN = 1e5 + 5, kInf = 0x3f3f3f3f;
int n, a[kMaxN], f[kMaxN];
set<ll> S;
ll fib[100];

bool Check(int l, int r) {
	for (int i = l; i < r; ++i)
		for (int j = i + 1; j <= r; ++j)
			if (S.find(a[i] + a[j]) != S.end())
				return false;
	return true;
}

int main() {
	freopen("f.in", "r", stdin);
	freopen("bf.out", "w", stdout);
	RI(n);
	for (int i = 1; i <= n; ++i) {
		RI(a[i]);
	}
	fib[0] = fib[1] = 1;
	S.insert(1);
	for (int i = 2; i <= 44; ++i) {
		fib[i] = fib[i - 1] + fib[i - 2];
		S.insert(fib[i]);
	}
	f[0] = 0;
	f[1] = 1;
	for (int i = 2; i <= n; ++i) {
		f[i] = kInf;
		for (int j = i - 1; j >= 0; --j) {
			if (Check(j + 1, i)) {
				f[i] = min(f[j] + 1, f[i]);
			} else break;
		}
	}
	printf("%d\n", f[n]);
	return 0;
}
