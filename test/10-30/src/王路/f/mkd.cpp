#include <bits/stdc++.h>
#include <Windows.h>
using namespace std;

int Rand16() { return rand(); }
int Rand32() { return (rand() << 15) + rand(); }
int Uni32(int l, int r) { return Rand32() % (r - l + 1) + l; }
void W(int x) { printf("%d\n", x); } 

int main() {
	freopen("f.in", "w", stdout);
	srand(GetTickCount());
	int n = 200;
	W(n);
	for (int i = 1; i <= n; ++i)
		printf("%d%c", Uni32(1, 2333), " \n"[i == n]);
	return 0;
}
