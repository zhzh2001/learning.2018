@echo off
for /L %%s in (1, 1, 10000) do (
	echo %%s
	mkd.exe
	bf.exe
	f.exe
	fc.exe bf.out f.out
	if errorlevel == 1 pause
)