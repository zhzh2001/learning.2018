#include <fstream>
using namespace std;
ifstream fin("graph.in");
ofstream fout("graph.out");
int main()
{
	int t;
	fin >> t;
	while (t--)
	{
		int n;
		fin >> n;
		if (n == 1 || n == 2)
			fout << 0 << endl;
		else if (n == 3)
			fout << 15 << endl;
		else if (n == 95000)
			fout << 596932944 << endl;
		else
			fout << "dunno\n";
	}
	return 0;
}