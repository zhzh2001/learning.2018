#include <fstream>
using namespace std;
ifstream fin("mu.in");
ofstream fout("mu.out");
const int N = 1e6;
int p[N / 10], mu[N + 5];
bool bl[N + 5];
int main()
{
	int pn = 0;
	mu[1] = 1;
	for (int i = 2; i <= N; i++)
	{
		if (!bl[i])
		{
			p[++pn] = i;
			mu[i] = -1;
		}
		for (int j = 1; j <= pn && i * p[j] <= N; j++)
		{
			bl[i * p[j]] = true;
			if (i % p[j])
				mu[i * p[j]] = -mu[i];
			else
			{
				mu[i * p[j]] = 0;
				break;
			}
		}
	}
	int val, k;
	fin >> val >> k;
	int cnt = 0;
	for (int i = 1; i <= N; i++)
		if ((cnt += (mu[i] == val)) == k)
		{
			fout << i << endl;
			break;
		}
	return 0;
}