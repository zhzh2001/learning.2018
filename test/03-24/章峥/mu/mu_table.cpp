#include <fstream>
using namespace std;
ifstream fin("mu.in");
ofstream fout("mu.out");
const int N = 1e9, B = 2e5;
int p[N / 10], cnt1[N / B + 5], cnt2[N / B + 5];
char mu[N + 5];
int main()
{
	int pn = 0;
	mu[1] = 1 << 1;
	for (int i = 2; i <= N; i++)
	{
		if (~mu[i] & 1)
		{
			p[++pn] = i;
			mu[i] = 3 << 1;
		}
		for (int j = 1; j <= pn && i * p[j] <= N; j++)
		{
			if (i % p[j])
			{
				if (mu[i] & 2)
					mu[i * p[j]] = mu[i] ^ 4;
				mu[i * p[j]] |= 1;
			}
			else
			{
				mu[i * p[j]] &= -7;
				mu[i * p[j]] |= 1;
				break;
			}
		}
	}
	/*
	int val, k;
	fin >> val >> k;
	if (val == -1)
		val = 3;
	int cnt = 0;
	for (int i = 1; i <= N; i++)
		if ((cnt += ((mu[i] >> 1) == val)) == k)
		{
			fout << i << endl;
			break;
		}
	*/
	for (int i = 1; i <= N; i += B)
		for (int j = 0; j < B; j++)
			if ((mu[i + j] >> 1) == 1)
				cnt1[i / B]++;
			else if ((mu[i + j] >> 1) == 3)
				cnt2[i / B]++;
	for (int i = 1; i <= N; i += B)
		fout << cnt1[i / B] << ',';
	fout << endl;
	for (int i = 1; i <= N; i += B)
		fout << cnt2[i / B] << ',';
	return 0;
}