#include<bits/stdc++.h>
using namespace std;
#define int long long
const int N=100005,inf=1e9+7;
int n,m,x,y,z,opt,Q,ans;
inline void read(int &x)
{
	x=0;	
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void write(int x)
{
	if (!x)
		putchar('0');
	int a[15];
	a[0]=0;
	while (x)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
		putchar(a[a[0]--]+'0');
	putchar('\n');
}
int to[N*2],pr[N*2],la[N],v[N*2],cnt;
inline void add(int x,int y,int z)
{
	to[++cnt]=y;
	pr[cnt]=la[x];
	la[x]=cnt;
	v[cnt]=z;
}

//tree_begin

	//segment_tree_begin

		int f[N*4];
		inline void push_up(int x)
		{
			f[x]=min(f[x*2],f[x*2+1]);
		}
		void insert(int l,int r,int pos,int x,int y)
		{
			if (l==r)
			{
				f[pos]=y;
				return;
			}
			int mid=(l+r)/2;
			if (x<=mid)
				insert(l,mid,pos*2,x,y);
			else
				insert(mid+1,r,pos*2+1,x,y);
			push_up(pos);
		}
		int ask(int l,int r,int L,int R,int pos)
		{
			if (l==L&&r==R)
				return f[pos];
			int mid=(l+r)/2;
			if (R<=mid)
				return ask(l,mid,L,R,pos*2);
			if (L>mid)
				return ask(mid+1,r,L,R,pos*2+1);
			return min(ask(l,mid,L,mid,pos*2),
					ask(mid+1,r,mid+1,R,pos*2+1));
		}

	//segement_tree_end

	//tree_pao_begin
		int fa[N],size[N],h[N],dfn[N],tot,top[N],g[N];
		void get_size(int x,int y)
		{
			fa[x]=y;
			size[x]=1;
			h[x]=h[y]+1;
			for (int i=la[x];i;i=pr[i])
				if (to[i]!=y)
				{
					g[to[i]]=v[i];
					get_size(to[i],x);
					size[x]+=size[to[i]];
				}
		}
		void dfs(int x)
		{
			dfn[x]=++tot;
			if (size[x]==1)
				return;
			int mx=0;
			for (int i=la[x];i;i=pr[i])
				if (to[i]!=fa[x])
					if (size[to[i]]>size[mx])
						mx=to[i];
			top[mx]=top[x];
			dfs(mx);
			for (int i=la[x];i;i=pr[i])
				if (to[i]!=fa[x]&&to[i]!=mx)
				{
					top[to[i]]=to[i];
					dfs(to[i]);
				}
		}
		inline int get_ans(int x,int y)
		{
			int ans=inf;
			while (top[x]!=top[y])
			{
				if (h[x]<h[y])
					swap(x,y);
				ans=min(ans,ask(1,n,dfn[top[x]],dfn[x],1));
				x=fa[top[x]];
			}
			if (x!=y)
			{
				if (h[x]<h[y])
					swap(x,y);
				ans=min(ans,ask(1,n,dfn[y]+1,dfn[x],1));
			}
			return ans;
		}
	//tree_pao_end

	//main_begin

		inline void work_tree()
		{
			get_size(1,0);
			top[1]=1;
			g[1]=inf;
			dfs(1);
			for (int i=1;i<=n;++i)
				insert(1,n,1,dfn[i],g[i]);
			read(Q);
			while (Q--)
			{
				read(opt);
				if (opt==1)
				{
					read(x);
					read(y);
					int u=to[x*2-1],v=to[x*2];
					if (h[u]<h[v])
						swap(u,v);
					insert(1,n,1,dfn[u],y);
					g[u]=y;
				}
				else
				{
					read(x);
					read(y);
					write(get_ans(x,y));
				}
			}
		}

	//main_end

//tree_end

//dinic_begin

	int dis[N],q[N];
	inline bool bfs()
	{
		memset(dis,0,sizeof(dis));
		q[1]=x;
		dis[x]=1;
		int l=0,r=1;
		while (l!=r)
		{
			int now=q[++l];
			for (int i=la[now];i;i=pr[i])
				if (!dis[to[i]]&&v[i])
				{
					dis[to[i]]=dis[now]+1;
					q[++r]=to[i];
				}
		}
		return dis[y];
	}
	int dfs(int x,int tmp)
	{
		if (x==y)
			return tmp;
		int res=tmp;
		for (int i=la[x];i;i=pr[i])
			if (dis[x]+1==dis[to[i]]&&v[i])
			{
				int p=dfs(to[i],min(v[i],res));
				v[i]-=p;
				v[((i-1)^1)+1]+=p;
				res-=p;
				if (res==0)
					break;
			}
		if (tmp==res)
			dis[x]=-233;
		return tmp-res;
	}
	inline void bl()
	{
		read(Q);
		read(opt);
		read(x);
		read(y);
		while (bfs())
			ans+=dfs(x,inf);
		write(ans);
	}

//dinic_end

signed main()
{
	freopen("flow.in","r",stdin);
	freopen("flow.out","w",stdout);
	read(n);
	read(m);
	for (int i=1;i<=m;++i)
	{
		read(x);
		read(y);
		read(z);
		add(x,y,z);
		add(y,x,z);
	}
	if (n==m+1)
		work_tree();
	else
		bl();
	return 0;
}