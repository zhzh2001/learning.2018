#include<bits/stdc++.h>
using namespace std;
const double eps=1e-7;
double a[100][100],ans;
int n;
inline void gauss()
{
	for (int i=1;i<=n;++i)
	{
		int k=i;
		while (abs(a[k][i])<eps&&k<=n)
			++k;
		if (k>n)
			continue;
		if (i!=k)
			for (int j=1;j<=n;++j)
				swap(a[i][j],a[k][j]);
		for (int j=i+1;j<=n;++j)
		{
			double p=-a[j][i]/a[i][i];
			for (int k=i;k<=n;++k)
				a[j][k]+=p*a[i][k];
		}
	}
}
int main()
{
	for (n=1;n<=10;++n)
	{
		for (int i=1;i<=n;++i)
			for (int j=1;j<=n;++j)
				a[i][j]=i==j?n:-1;
		gauss();
		// for (int i=1;i<=n;++i,putchar('\n'))
		// 	for (int j=1;j<=n;++j)
		// 		printf("%.3lf ",a[i][j]);
		ans=1;
		for (int i=1;i<=n;++i)
			ans*=a[i][i];
		printf("%.3lf\n",ans);
	}
}