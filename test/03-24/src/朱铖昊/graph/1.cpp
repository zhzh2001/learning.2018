#include<bits/stdc++.h>
using namespace std;
const int mod=1e9+7;
#define ll long long
inline int ksm(int x,int y)
{
	int ans=1;
	for (;y;y/=2,x=(ll)x*x%mod)
		if (y&1)
			ans=(ll)ans*x%mod;
	return ans;
}
int main()
{
	int x;
	cin>>x;
	cout<<ksm(3,x);
	return 0;
}