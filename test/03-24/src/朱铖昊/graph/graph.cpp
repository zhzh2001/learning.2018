#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define int long long
const int mod=998244353;
const int N=100005;
int t,n,a[N],b[N],fac[N],inv[N],ans;
inline void read(int &x)
{
	x=0;	
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline int ksm(int x,int y)
{
	int ans=1;
	for (;y;y/=2,x=(ll)x*x%mod)
		if (y&1)
			ans=(ll)ans*x%mod;
	return ans;
}
signed main()
{
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	read(t);
	fac[0]=1;
	for (int i=1;i<=100000;++i)
		fac[i]=(ll)fac[i-1]*i%mod;
	inv[100000]=ksm(fac[100000],mod-2);
	for (int i=100000-1;i>=0;--i)
		inv[i]=(ll)inv[i+1]*(i+1)%mod;
	for (int i=1;i<=100000;++i)
	{
		a[i]=ksm(i,i-1);
		b[i]=(ll)a[i]*i%mod;
	}
	while (t--)
	// for (n=1;n<=1000;++n)
	{
		read(n);
		if (n<=2)
		{
			puts("0");
			continue;
		}
		ans=b[n];
		int k=2LL*n*b[n-1]%mod;
		ans-=k;
		if (ans<0)
			ans+=mod;
		for (int i=1;i<=n-2;++i)
			for (int j=1;j<=n-i-1;++j)
				(ans+=(ll)inv[i]*inv[j]%mod*inv[n-1-i-j]%mod*a[i]%mod*b[j]%mod*j%mod*fac[n-1]%mod)%=mod;
		ans=(ll)ans*n%mod;
		cout<<ans<<'\n';
	}
	return 0;
}