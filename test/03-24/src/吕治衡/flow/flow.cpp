#include<bits/stdc++.h>
#define ll long long
#define int long long
inline ll read(){
	char c=getchar();while (c!='-'&&(c<'0'||c>'9'))c=getchar();
	ll k=0,kk=1;if (c=='-')c=getchar(),kk=-1;
	while (c>='0'&&c<='9')k=k*10+c-'0',c=getchar();return kk*k;
}using namespace std;
void write(ll x){if (x<0)x=-x,putchar('-');if (x/10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);puts("");}
int n,m,kk,x,y,z,a[1000010][3],kkk,dee[1000010],mi[1000010],lazy[1000010];
int son[1000010],d[1000010],S,T,ans,flag[1000010],last[1000010],fff[1000010],fh[1000010],ffa[1000010];
int dfn[1000010],q,c,ff[1000010];
int dfs(int x,int y){//十分直接网络流 
	if (x==T){ans+=y;return y;}if (flag[x]||!y||ff[x])return 0;
	ff[x]=1;int sum=0;for (int i=last[x];i;i=a[i][0]){
		int dd=dfs(a[i][1],min(y,a[i][2]));sum+=dd;y-=dd;
		a[i][2]-=dd;a[i^1][2]+=dd;
		if (!y)break;
	}if (y>0)flag[x]=1;ff[x]=0;return sum;
}void pushdown(int d){
	mi[d*2]=mi[d*2+1]=lazy[d*2]=lazy[d*2+1]=lazy[d];
	lazy[d]=0;
}void build(int l,int r,int d){//区间加，区间min询问 
	if (l==r){mi[d]=fff[l];return;}
	int m=(l+r)>>1;build(l,m,d*2);build(m+1,r,d*2+1);
	mi[d]=min(mi[d*2],mi[d*2+1]);
}void putit(int x,int y,int z,int l,int r,int d){ 
	if (x<=l&&y>=r){lazy[d]=z;mi[d]=z;return;}
	if (lazy[d])pushdown(d);int m=(l+r)>>1;
	if (x<=m)putit(x,y,z,l,m,d*2);if (y>m)putit(x,y,z,m+1,r,d*2+1);
	mi[d]=min(mi[d*2],mi[d*2+1]);
}int findit(int x,int y,int l,int r,int d){
	if (x<=l&&y>=r)return mi[d];
	if (lazy[d])pushdown(d);int m=(l+r)>>1;
	if (x<=m&&y>m)return min(findit(x,y,l,m,d*2),findit(x,y,m+1,r,d*2+1));
	if (x<=m)return findit(x,y,l,m,d*2);else return findit(x,y,m+1,r,d*2+1);
}void dfs1(int x,int fa,int de){
	d[x]=1;dee[x]=de;
	for (int i=last[x];i;i=a[i][0])if (a[i][1]!=fa){
		dfs1(a[i][1],x,de+1);d[x]+=d[a[i][1]];if (d[a[i][1]]>d[son[x]])son[x]=a[i][1];
	}
}void dfs2(int x,int fa){
	fh[x]=fa;ffa[x]=son[fa]==x?ffa[fa]:x;
	dfn[x]=++kkk;if (son[x])dfs2(son[x],x);
	for (int i=last[x];i;i=a[i][0])
		if (a[i][1]!=fa&&a[i][1]!=son[x])dfs2(a[i][1],x);
			else if (a[i][1]==fa)fff[dfn[x]]=a[i][2];
}int calc(int x,int y){
	int ans=1e15;while (x!=y){
		if (dee[ffa[x]]<dee[ffa[y]]||dee[ffa[x]]==dee[ffa[y]]&&dee[x]<dee[y])swap(x,y);
		if (ffa[x]==ffa[y]){
			ans=min(ans,findit(dfn[y]+1,dfn[x],1,n,1));
			break;
		}if (ffa[x]==x){
			ans=min(ans,findit(dfn[x],dfn[x],1,n,1));x=fh[x];
		}else{
			ans=min(ans,findit(dfn[ffa[x]]+1,dfn[x],1,n,1));x=ffa[x];
		}
	}return ans;
}void put(int x,int y){//将边x改成y 
	int dd,xx=a[x*2][1],yy=a[x*2+1][1];
	if (fh[yy]==xx)swap(xx,yy); 
	fff[dfn[xx]]=y;putit(dfn[xx],dfn[xx],y,1,n,1);
}void doit(int x,int y,int z){a[++kk][0]=last[x];a[kk][1]=y;a[kk][2]=z;last[x]=kk;}
signed main(){
	freopen("flow.in","r",stdin);freopen("flow.out","w",stdout);
	n=read();m=read();kk=1;
	for (int i=1;i<=m;i++)x=read(),y=read(),z=read(),doit(x,y,z),doit(y,x,z);
	q=read();if (q==1){
		c=read();S=read();T=read();
		while(dfs(S,1e15));writeln(ans);
	}else{
		dfs1(1,0,1);dfs2(1,0);
		build(1,n,1);
		while (q--){
			c=read();x=read();y=read();
			if (c==1)put(x,y);else writeln(calc(x,y));
		}
	}
}
/*
撸出dfs树，仙人掌的每个环会有一条返祖边
染色即可 
树剖线段树支持区间min查询
那么问题来了环不一定在重链上 
6 6
1 2 5
4 5 8
4 3 2
6 5 5
1 6 4
2 3 5
1
0 1 2 
*/
