#include <bits/stdc++.h>
#define N 1000020
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}
int pri[N], mu[N], mark[N], cnt;
int main(int argc, char const *argv[]) {
  freopen("mu.in", "r", stdin);
  freopen("mu.out", "w", stdout);
  // 30 start -----------------
  int n = 1e6;
  mu[1] = 1;
  for (int i = 2; i <= n; i++) {
    if (!mark[i]) mu[i] = -1, pri[++ cnt] = i;
    for (int j = 1; j <= cnt && i * pri[j] <= n; j++) {
      mark[i * pri[j]] = 1;
      if (i % pri[j] == 0) {
        mu[i * pri[j]] = 0;
        break;
      } else {
        mu[i * pri[j]] = - mu[i];
      }
    }
  }
  int ans = read(), kth = read();
  for (int i = 1; i <= n; i++)
    if (mu[i] == ans && !--kth)
      return printf("%d\n", i), 0;
  puts("DT is AK king");
  // 30 end -------------------
  return 0;
}