#include <bits/stdc++.h>
#define N 100020
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}
namespace map1 {
  int head[N], nxt[N<<1], to[N<<1], cnt;
  void insert(int x, int y) {
    to[++ cnt] = y; nxt[cnt] = head[x]; head[x] = cnt;
    to[++ cnt] = x; nxt[cnt] = head[y]; head[y] = cnt;
  }
}
namespace map2 {
  int head[N], nxt[N<<1], to[N<<1], cnt;
  void insert(int x, int y) {
    to[++ cnt] = y; nxt[cnt] = head[x]; head[x] = cnt;
    to[++ cnt] = x; nxt[cnt] = head[y]; head[y] = cnt;
  }
}
// tarjan 缩一波点
namespace tarjan {
  int blo[N], bnt, que[N], qnt, tot, sz[N];
  int low[N], dfn[N];
  using namespace map1;
  void tarjan(int x, int f) {
    que[++ qnt] = x;
    low[x] = dfn[x] = ++ tot;
    for (int i = head[x]; i; i = nxt[i]) {
      int y = to[i];
      if (y == f) continue;
      if (!dfn[y]) tarjan(y, x), low[x] = min(low[x], low[y]);
      else if (!blo[y]) low[x] = min(low[x], dfn[y]);
    }
    if (low[x] == dfn[x] && ++ bnt)
      while (que[qnt + 1] != x)
        blo[que[qnt --]] = bnt, sz[bnt] ++;
  }
}
// 用 blo 进行树链剖分
namespace slpf {
  int son[N], top[N], fa[N], p[N], fp[N], sz[N], tot;
  //  p: 树链剖分 -> 线段树
  // fp: 线段树 -> 树链剖分
  using namespace map2;
  void dfs1(int x, int f) {
    fa[x] = f; sz[x] = 1;
    for (int i = head[x]; i; i = nxt[i]) {
      if (to[i] == f) continue;
      dfs1(to[i], x);
      if (sz[to[i]] > sz[son[i]])
        son[i] = to[i];
      sz[x] += sz[to[i]];
    }
  }
  void dfs2(int x, int up) {
    top[x] = up;
    fp[p[x] = ++ tot] = x;
    if (!son[x]) return;
    dfs2(son[i], up);
    for (int i = head[x]; i; i = nxt[i]) {
      if (to[i] == fa[i] || to[i] == son[i])
        continue;
      dfs2(to[i], to[i]);
    }
  }
}
// 似乎还要几颗线段树
// 这个复杂度似乎不太对。。。
// edges
namespace edge {
  int x[N], y[N], z[N];
}
// 更重要的是我 TM 好像不会写树套树
int main(int argc, char const *argv[]) {
  // freopen("flow.in", "r", stdin);
  // freopen("flow.out", "w", stdout);
  int n = read(), m = read();
  for (int i = 1; i <= m; i++) {
    edge::x[i] = read();
    edge::y[i] = read();
    edge::z[i] = read();
    map1::insert(edge::x[i], edge::y[i]);
  }
  for (int i = 1; i <= n; i++)
    if (!tarjan::dfn[i]) tarjan::tarjan(i, 0);
  for (int i = 1; i <= m; i++)
    if (tarjan::blo[edge::x[i]] != tarjan::blo[edge::y[i]])
      map2::insert(tarjan::blo[edge::x[i]], tarjan::blo[edge::y[i]]);

  int Q = read();
  while (Q --) {
    int op = read(), x = read(), y = read();
    if (op) {
      // asks ...
    } else {
      // updates ...
    }
  }

  return 0;
}