#include <bits/stdc++.h>
#define N 100020
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}
int n, m, Q, vl[N];
int fuck[N];
int head[N], nxt[N<<1], to[N<<1], cnt = 1, val[N<<1];
void insert(int x, int y, int z) {
  to[++ cnt] = y; nxt[cnt] = head[x]; head[x] = cnt; val[cnt] = z;
  to[++ cnt] = x; nxt[cnt] = head[y]; head[y] = cnt; val[cnt] = z;
}
int son[N], top[N], fa[N], p[N], fp[N], sz[N], tot, dep[N];
//  p: 树链剖分 -> 线段树
// fp: 线段树 -> 树链剖分
void dfs1(int x, int f) {
  fa[x] = f; sz[x] = 1; dep[x] = dep[f] + 1;
  for (int i = head[x]; i; i = nxt[i]) {
    if (to[i] == f) continue;
    dfs1(to[i], x);
    if (sz[to[i]] > sz[son[x]])
      son[x] = to[i];
    sz[x] += sz[to[i]];
    vl[to[i]] = val[i];
    fuck[i >> 1] = to[i];
  }
}
void dfs2(int x, int up) {
  top[x] = up;
  fp[p[x] = ++ tot] = x;
  if (!son[x]) return;
  dfs2(son[x], up);
  for (int i = head[x]; i; i = nxt[i]) {
    if (to[i] == fa[x] || to[i] == son[x])
      continue;
    dfs2(to[i], to[i]);
  }
}
int mn[N << 2];
void update(int x, int k, int l, int r, int v) {
  if (l == r) {
    mn[x] = v;
    return;
  }
  int mid = l + r >> 1;
  if (k <= mid) update(x << 1, k, l, mid, v);
  else update(x<<1|1, k, mid + 1, r, v);
  mn[x] = min(mn[x << 1], mn[x<<1|1]);
}
int query(int x, int l, int r, int L, int R) {
  if (l == L && r == R)
    return mn[x];
  int mid = L + R >> 1;
  if (r <= mid) return query(x << 1, l, r, L, mid);
  if (l > mid) return query(x<<1|1, l, r, mid + 1, R);
  return min(query(x << 1, l, mid, L, mid), query(x<<1|1, mid + 1, r, mid + 1, R));
}
void build(int x, int l, int r) {
  if (l == r) {
    mn[x] = vl[fp[l]];
    return;
  }
  int mid = l + r >> 1;
  build(x << 1, l, mid);
  build(x<<1|1, mid + 1, r);
  mn[x] = min(mn[x << 1], mn[x<<1|1]);
}
int query(int x, int y) {
  int ans = 1 << 30;
  int f1 = top[x], f2 = top[y];
  while (f1 != f2) {
    if (dep[f1] < dep[f2])
      swap(f1, f2), swap(x, y);
    // f1 深
    ans = min(ans, query(1, p[f1], p[x], 1, n));
    x = fa[f1];
    f1 = top[x];
  }
  if (dep[x] < dep[y]) swap(x, y);
  if (dep[x] == dep[y]) return ans;
  else return min(ans, query(1, p[y] + 1, p[x], 1, n));
}
// 30 分做法
int main(int argc, char const *argv[]) {
  // freopen("flow.in", "r", stdin);
  // freopen("flow.out", "w", stdout);
  n = read(), m = read();

  for (int i = 1; i <= m; i++) {
    int x = read(), y = read(), z = read();
    insert(x, y, z);
  }

  dfs1(1, 0);
  dfs2(1, 1);
  build(1, 1, n);

  Q = read();

  while (Q --) {
    int op = read();
    int x = read(), y = read();
    if (!op) { // ask
      printf("%d\n", query(x, y));
    } else { // update
      update(1, p[fuck[x]], 1, n, y);
    }
  }

  return 0;
}