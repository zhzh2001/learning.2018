#include<bits/stdc++.h>
using namespace std;
const int N=400005;
char s[100];
queue<int > Q;
int data[N],sl[N],QQ,m,pre[N],S,T,dis[N],p[N],n,pos,fp[N],num[N];
int x,y,z,tot,top[N],e[N][3],val[N],deep[N],son[N],fa[N],ne[N],fi[N],zz[N];
void jb(int x,int y)
{
    ne[++tot]=fi[x];
    fi[x]=tot;
    zz[tot]=y;
}
void dfs1(int x,int y,int z)
{
    deep[x]=z;
    fa[x]=y;
    for (int i=fi[x];i;i=ne[i])
     {
        int k=zz[i];
        if (k!=y)
         {
            dfs1(k,x,z+1);
        	num[x]+=num[k];
            if (son[x]==-1||(num[son[x]]<num[k]))son[x]=k;
         }
     }
}
void dfs2(int x,int y)
{
    top[x]=y;
    if (son[x]!=-1)
     {
        p[x]=pos++;
        fp[p[x]]=x;
        dfs2(son[x],y);
     }
    else
     {
        p[x]=pos++;
        fp[p[x]]=x;
        return;
     } 
    for (int i=fi[x];i;i=ne[i])
     if (zz[i]!=son[x]&&zz[i]!=fa[x])
      dfs2(zz[i],zz[i]); 
}
void pushup(int x)
{
    data[x]=min(data[x*2],data[x*2+1]);
}
void build(int l,int r,int x)
{
    if (l==r)
     {
        data[x]=val[l];
        return;
     }
    int mid=(l+r)/2; 
    build(l,mid,x*2); 
    build(mid+1,r,x*2+1);
    pushup(x);
}
void updata(int p,int q,int l,int r,int x)
{
    if (l==r)
     {
        data[x]=q;
        return;
     }
    int mid=(l+r)/2;
    if (p<=mid)updata(p,q,l,mid,x*2);
    else updata(p,q,mid+1,r,x*2+1);
    pushup(x); 
}
int query(int x,int y,int l,int r,int s)
{
    if (x>r||y<l)return 1e9;
    if (x<=l&&y>=r)return data[s];
    int mid=(l+r)/2;
    return min(query(x,y,l,mid,s*2),query(x,y,mid+1,r,s*2+1));
}
int find(int x,int y)
{
    int f1=top[x],f2=top[y],temp=0;
    while (f1!=f2)
     {
        if (deep[f1]<deep[f2])
         {
            swap(x,y);
            swap(f1,f2);
         }
        temp=max(temp,query(p[f1],p[x],1,n,1)); 
        x=fa[f1],f1=top[x]; 
     }
    if (x==y)return temp;
    if (deep[x]>deep[y])swap(x,y);
    return max(temp,query(p[son[x]],p[y],1,n,1)); 
}
void jb1(int x,int y,int z)
{
	ne[tot]=fi[x];
	fi[x]=tot;
	zz[tot]=y;
	sl[tot++]=z;
	ne[tot]=fi[y];
	fi[y]=tot;
	zz[tot]=x;
	sl[tot++]=z;
}
int bfs(int S,int T)
{
	memset(dis,0xff,sizeof dis);
	dis[S]=0;
	while (!Q.empty())Q.pop();
	Q.push(S);
	while (!Q.empty())
	 {
	 	int now=Q.front();
	 	Q.pop();
	 	for (int i=fi[now];i!=-1;i=ne[i])
	 	 if (sl[i]&&dis[zz[i]]==-1)
	 	  {
	 	  	dis[zz[i]]=dis[now]+1;
	 	  	Q.push(zz[i]);
	 	  	if (zz[i]==T)return 1;
	 	  }
	 }
	return 0; 
}
int dfs(int x,int y)
{
	if (x==T)return y;
	int b=0;
	for (int &i=pre[x];i!=-1;i=ne[i])
	 if (sl[i]&&dis[zz[i]]==dis[x]+1&&(b=dfs(zz[i],min(sl[i],y))))
	  {
	  	sl[i]-=b;
	  	sl[i^1]+=b;
	  	return b;
	  }
	return 0;  
}
void doit()
{
	memset(fi,-1,sizeof fi);
	int ans=0;
	while (m--)
	 {
	 	scanf("%d%d%d",&x,&y,&z);
	 	jb1(x,y,z);
	 }
	scanf("%d%d%d%d",&S,&S,&S,&T);
	while (bfs(S,T))
	 {
	 	for (int i=1;i<=n;i++)pre[i]=fi[i];
	 	ans+=dfs(S,1e9);
	 }
	printf("%lld",ans); 
	return;
}
int main()
{
	freopen("flow.in","r",stdin);
	freopen("flow.out","w",stdout);
    pos=1;tot=0;
    memset(son,-1,sizeof son);
    scanf("%d%d",&n,&m);
    if (m>=n)
     {
     	doit();
     	return 0;
     }
    for (int i=0;i<m;i++)
     {
        scanf("%d%d%d",&e[i][0],&e[i][1],&e[i][2]);
        jb(e[i][0],e[i][1]);jb(e[i][1],e[i][0]);
     }
    dfs1(1,0,0);
    dfs2(1,1);
    for (int i=0;i<m;i++)
     {
        if (deep[e[i][0]]<deep[e[i][1]])swap(e[i][0],e[i][1]);
        val[p[e[i][0]]]=e[i][2];
      }
    build(1,n,1); 
    scanf("%d",&QQ);
    while (QQ--)
     {   
	 	scanf("%s%d%d",&s,&x,&y);
        if (s[0]=='0')printf("%d\n",find(x,y));
        else updata(p[e[x-1][0]],y,1,n,1);
     }
    return 0; 
}
