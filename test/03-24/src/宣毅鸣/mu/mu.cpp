#include<bits/stdc++.h>
using namespace std;
const int N=1e6+5;
int miu[N],ans,flag[N],tot,num[N],p[N],n,m;
void init()
{
	num[1]=miu[1]=1;
	for (int i=2;i<N;i++)
	 {
	 	if (!flag[i])
	 	 {
	 	 	miu[i]=-1;
	 	 	p[++tot]=i;
	 	 }
	 	for (int j=1;j<=tot;j++)
		 {
		 	int k=p[j]*i;
		 	if (k>=N)break;
		 	flag[k]=1;
		 	if (i%p[j]==0)
		 	 {
		 	 	miu[k]=0;
		 	 	break;
		 	 }
		 	miu[k]-=miu[i]; 
		 }
	 } 
}
void dfs(int s,int x,int y,int z)
{
	if (s/x<x)return;
	ans+=(s/(x*x))*z;
	for (int i=y;i<=tot;i++)
	 {
	 	if (s/(x*p[i])<p[i]*x)break;
	 	dfs(s,x*p[i],i+1,z*(-1));
	 }
}
int pd(int x)
{
	ans=0;
	for (int i=1;i<=tot;i++)dfs(x,p[i],i+1,1);
	if (ans<m)return 1;
	return 0;
}
void doit()
{
	int l=0,r=1e9;
	while (l<r)
	 {
	 	int mid=(l+r)/2;
	 	if (pd(mid))l=mid+1;
	 	else r=mid;
	 }
	printf("%d",l); 
}
int main()
{
	freopen("mu.in","r",stdin);
	freopen("mu.out","w",stdout);
	scanf("%d%d",&n,&m);
	init();
	if (n==0)
	 {
	 	doit();
	 	return 0;
	 }
	for (int i=1;i;i++)
	 {
	 	if (miu[i]==n)m--;
	 	if (!m)
	 	 {
	 	 	printf("%d",i);
	 	 	return 0;
	 	 }
	 }
	return 0; 
}
