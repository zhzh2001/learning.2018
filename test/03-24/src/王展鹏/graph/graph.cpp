#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<set>
#include<map>
#include<queue>
#include<bitset>
#include<ctime>
using namespace std;
typedef long long ll;
#define sqr(x) ((x)*(x))
#define mp make_pair
#define uint unsigned
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(int a){
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(int a){
    if(a<0){
    	a=-a; putchar('-');
	}
	write(a); puts("");
}
const int N=100;
int vis[N],n,f[N],ans,quan;
void ddd(int p){
	if(vis[p])return;else vis[p]=1;
	if(p==quan)return;
	ddd(f[p]);
	for(int i=1;i<=n;i++)if(f[i]==p)ddd(i);
}
int calc(){
	int sum=0; memset(vis,0,sizeof(vis));
	for(int i=1;i<=n;i++)if(i!=quan&&!vis[i]){
		ddd(i); sum++;
	}
	return sum;
}
void dfs(int p){
	if(p>n){
		quan=0; int t=calc(); //if(t>1)return;
		for(int i=1;i<=n;i++){
			quan=i;
			//if(calc()>t)cout<<f[1]<<" "<<f[2]<<" "<<f[3]<<" "<<i<<" "<<calc()<<endl;
			if(calc()>t)ans++;
		}
		//cout<<f[1]<<" "<<f[2]<<" "<<f[3]<<" "<<f[4]<<" "<<ans<<endl;
		return;
	}
	for(int i=1;i<=n;i++){
		f[p]=i; dfs(p+1);
	}
}
int main(){
	freopen("graph.in","r",stdin); freopen("graph.out","w",stdout);
	int T=read();
	while(T--){
		n=read(); ans=0;
		dfs(1);
		writeln(ans);
	}
}
//15 220 3405 59886
//c(6,2) c(12,3)
