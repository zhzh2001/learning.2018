#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<set>
#include<map>
#include<queue>
#include<bitset>
#include<ctime>
using namespace std;
typedef long long ll;
#define sqr(x) ((x)*(x))
#define mp make_pair
#define uint unsigned
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(int a){
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(int a){
    if(a<0){
    	a=-a; putchar('-');
	}
	write(a); puts("");
}
const int N=5005,mod=998244353;
int n,fac[N],a[N],dp[N],ni[N],g[N],f[N];
inline int c(int a,int b){
	return (ll)fac[a]*ni[b]%mod*ni[a-b]%mod;
}
inline int ksm(int a,int b){
	int ans=1;
	for(;b;b>>=1){
		if(b&1)ans=(ll)ans*a%mod;
		a=(ll)a*a%mod;
	}
	return ans;
}
int main(){
	n=read();
	for(int i=1;i<=n;i++){
		for(int j=1;j+i<=n;j++){
			dp[i][j]=dp[i]
		}
	}
	fac[0]=ni[0]=1;
	for(int i=1;i<=n;i++){
		fac[i]=(ll)fac[i-1]*i%mod; ni[i]=ksm(fac[i],mod-2);
	}
	for(int i=0;i<=n;i++)g[i]=ksm(i,i);
	for(int i=1;i<=n;i++){
		for(int j=1;j<=i;j++){
			f[i]=(f[i]+(ll)(a[j]*g[i-j])*c(i,j))%mod;
		}
	}
	cout<<f[n]<<endl;
}
/*
5
0 0 15 220 3405
0 0 5 55 681
*/
