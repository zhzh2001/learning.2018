#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<set>
#include<map>
#include<queue>
#include<bitset>
#include<ctime>
using namespace std;
typedef long long ll;
#define int long long
#define sqr(x) ((x)*(x))
#define uint unsigned
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x = 0; char ch = gc(); bool positive = 1;
	for (; !isdigit(ch); ch = gc())	if (ch == '-')	positive = 0;
	for (; isdigit(ch); ch = gc())	x = x * 10 + ch - '0';
	return positive ? x : -x;
}
inline void write(int a){
    if(a>=10)write(a/10);
    putchar('0'+a%10);
}
inline void writeln(int a){
    if(a<0){
    	a=-a; putchar('-');
	}
	write(a); puts("");
}
const int N=200005,oo=4e18;
int q,n,m,nextt[N<<1],son[N],nedge,ed[N<<1],di[N<<1],cur[N];
int tree[N<<2],peo[N],wson[N],father[N],deep[N],zh[N],mp[N],pos[N],in[N],ti,top[N];
inline void aedge(int a,int b,int c){
	nextt[++nedge]=son[a]; son[a]=nedge; ed[nedge]=b; di[nedge]=c;
}
void dfs1(int p){
	peo[p]=1; wson[p]=-1;
	for(int i=son[p];i;i=nextt[i])if(ed[i]!=father[p]){
		deep[ed[i]]=deep[p]+1; father[ed[i]]=p; zh[ed[i]]=di[i];
		dfs1(ed[i]); peo[p]+=peo[ed[i]]; mp[(i+1)/2]=ed[i];
		if(wson[p]==-1||peo[ed[i]]>peo[wson[p]])wson[p]=ed[i];
	}
}
void dfs2(int p){
	pos[in[p]=++ti]=p; 
	if(p==wson[father[p]])top[p]=top[father[p]]; else top[p]=p;
	if(wson[p]!=-1)dfs2(wson[p]);
	for(int i=son[p];i;i=nextt[i])if(ed[i]!=father[p]&&ed[i]!=wson[p])dfs2(ed[i]);
}
void build(int l,int r,int nod){
	if(l==r){
		tree[nod]=zh[pos[l]]; return;
	}
	int mid=(l+r)>>1;
	build(l,mid,nod<<1); build(mid+1,r,nod<<1|1);
	tree[nod]=min(tree[nod<<1],tree[nod<<1|1]);
}
inline int ask(int l,int r,int i,int j,int nod){
	if(l==i&&r==j)return tree[nod];
	int mid=(l+r)>>1;
	if(j<=mid)return ask(l,mid,i,j,nod<<1); else if(i>mid)ask(mid+1,r,i,j,nod<<1|1);
	else return min(ask(l,mid,i,mid,nod<<1),ask(mid+1,r,mid+1,j,nod<<1|1));
}
inline void insert(int l,int r,int pos,int s,int nod){
	if(l==r){tree[nod]=s; return;}
	int mid=(l+r)>>1;
    if(pos<=mid)insert(l,mid,pos,s,nod<<1); else insert(mid+1,r,pos,s,nod<<1|1);
	tree[nod]=min(tree[nod<<1],tree[nod<<1|1]);
}
int query(int u,int v){
	int ans=2e9;
	while(top[u]!=top[v]){
		if(deep[top[u]]<deep[top[v]])swap(u,v);
		ans=min(ans,ask(1,n,in[top[u]],in[u],1));
		u=father[top[u]];
	}
	if(deep[u]>deep[v])swap(u,v);
	if(in[u]!=in[v])ans=min(ans,ask(1,n,in[u]+1,in[v],1));
	return ans;
}
inline bool bfs(int s,int t){
	for(int i=1;i<=n;i++)deep[i]=oo;
    for(int i=1;i<=n;i++)cur[i]=son[i];
    deep[s]=0;
    queue<int> q; while(!q.empty())q.pop();
    q.push(s);
    while(!q.empty()){
        int now=q.front();q.pop();
        for(int tmp=son[now];tmp;tmp=nextt[tmp])
            if(deep[ed[tmp]]>=oo&&di[tmp]){deep[ed[tmp]]=deep[now]+1,q.push(ed[tmp]);}
    }
    return deep[t]<oo;
}
int dfs(int now,int t,int limit){
    if(!limit||now==t) return limit;
    int flow=0,f;
    for(int tmp=cur[now];tmp;tmp=nextt[tmp]){
        cur[now]=tmp;
        if(deep[ed[tmp]]==deep[now]+1&&(f=dfs(ed[tmp],t,min(limit,di[tmp])))){
            flow+=f; limit-=f; di[tmp]-=f; di[((tmp-1)^1)+1]+=f; if(!limit)break;
        }
    }
    return flow;
}
inline int dinic(int s,int t){
    int ans=0;
    while(bfs(s,t))ans+=dfs(s,t,oo);
    return ans;
}
signed main(){
	freopen("flow.in","r",stdin); freopen("flow.out","w",stdout);
	n=read(); m=read();
	for(int i=1;i<=m;i++){
		int a=read(),b=read(),c=read(); aedge(a,b,c); aedge(b,a,c);
	}
	if(m!=n-1){
		read(); //read(); read();read();
		read();
		int s=read(),t=read();
		writeln(dinic(s,t));
		return 0;
	}
	dfs1(1); dfs2(1);
	build(1,n,1);
	q=read();
	for(int i=1;i<=q;i++){
		int op=read(),x=read(),y=read();
		if(op==1){
			insert(1,n,in[mp[x]],y,1);
		}else{
			writeln(query(x,y));
		}
	}
}

