#include <fstream>
#include <queue>
#include <algorithm>
using namespace std;
ifstream fin("flow.in");
ofstream fout("flow.out");
const int N = 100005, M = 400005, INF = 1e9;
int n, m, head[N], H[N], v[M], cap[M], nxt[M], e, s, t, dep[N], sz[N], son[N], f[N], id[N], rid[N], top[N], edge[N], redge[M];
inline void add_edge(int u, int v, int cap)
{
	::v[e] = v;
	::cap[e] = cap;
	nxt[e] = head[u];
	head[u] = e++;
}
bool bfs()
{
	queue<int> Q;
	Q.push(s);
	fill(dep + 1, dep + n + 1, 0);
	dep[s] = 1;
	while (!Q.empty())
	{
		int k = Q.front();
		Q.pop();
		if (k == t)
			return true;
		for (int i = head[k]; ~i; i = nxt[i])
			if (!dep[v[i]] && cap[i])
			{
				dep[v[i]] = dep[k] + 1;
				Q.push(v[i]);
			}
	}
	return false;
}
int dfs(int k, int pred)
{
	if (k == t || !pred)
		return pred;
	int flow = 0, now;
	for (int &i = H[k]; ~i; i = nxt[i])
		if (dep[v[i]] == dep[k] + 1 && (now = dfs(v[i], min(pred, cap[i]))) > 0)
		{
			flow += now;
			cap[i] -= now;
			cap[i ^ 1] += now;
			if (!(pred -= now))
				break;
		}
	if (!flow)
		dep[k] = -1;
	return flow;
}
void dfs1(int k, int fat)
{
	sz[k] = 1;
	f[k] = fat;
	for (int i = head[k]; ~i; i = nxt[i])
		if (v[i] != fat)
		{
			dep[v[i]] = dep[k] + 1;
			dfs1(v[i], k);
			sz[k] += sz[v[i]];
			edge[v[i]] = i;
			redge[i] = v[i];
			if (sz[v[i]] > sz[son[k]])
				son[k] = v[i];
		}
}
void dfs2(int k, int anc)
{
	id[k] = ++t;
	rid[t] = k;
	top[k] = anc;
	if (son[k])
		dfs2(son[k], anc);
	for (int i = head[k]; ~i; i = nxt[i])
		if (v[i] != son[k] && v[i] != f[k])
			dfs2(v[i], v[i]);
}
int tree[N * 4];
void build(int id, int l, int r)
{
	if (l == r)
		tree[id] = cap[edge[rid[l]]];
	else
	{
		int mid = (l + r) / 2;
		build(id * 2, l, mid);
		build(id * 2 + 1, mid + 1, r);
		tree[id] = min(tree[id * 2], tree[id * 2 + 1]);
	}
}
void modify(int id, int l, int r, int x, int val)
{
	if (l == r)
		tree[id] = val;
	else
	{
		int mid = (l + r) / 2;
		if (x <= mid)
			modify(id * 2, l, mid, x, val);
		else
			modify(id * 2 + 1, mid + 1, r, x, val);
		tree[id] = min(tree[id * 2], tree[id * 2 + 1]);
	}
}
int query(int id, int l, int r, int L, int R)
{
	if (L <= l && R >= r)
		return tree[id];
	int mid = (l + r) / 2;
	if (R <= mid)
		return query(id * 2, l, mid, L, R);
	if (L > mid)
		return query(id * 2 + 1, mid + 1, r, L, R);
	return min(query(id * 2, l, mid, L, R), query(id * 2 + 1, mid + 1, r, L, R));
}
int queryChain(int u, int v)
{
	int ans = INF;
	while (top[u] != top[v])
	{
		if (dep[top[u]] < dep[top[v]])
			swap(u, v);
		ans = min(ans, query(1, 1, n, id[top[u]], id[u]));
		u = f[u];
	}
	int l = id[u], r = id[v];
	if (l > r)
		swap(l, r);
	if (l < r)
		ans = min(ans, query(1, 1, n, l + 1, r));
	return ans;
}
int main()
{
	fin >> n >> m;
	fill(head + 1, head + n + 1, -1);
	while (m--)
	{
		int u, v, w;
		fin >> u >> v >> w;
		add_edge(u, v, w);
		add_edge(v, u, w);
	}
	int q;
	fin >> q;
	if (q == 1)
	{
		int u;
		fin >> u >> s >> t;
		int ans = 0;
		while (bfs())
		{
			copy(head + 1, head + n + 1, H + 1);
			ans += dfs(s, INF);
		}
		fout << ans << endl;
	}
	else if (e / 2 == n - 1)
	{
		dfs1(1, 0);
		dfs2(1, 1);
		build(1, 1, n);
		while (q--)
		{
			int opt, x, y;
			fin >> opt >> x >> y;
			if (opt)
			{
				if (redge[x * 2 - 2])
					modify(1, 1, n, id[redge[x * 2 - 2]], y);
				else
					modify(1, 1, n, id[redge[x * 2 - 1]], y);
			}
			else
				fout << queryChain(x, y) << endl;
		}
	}
	else
		fout << "dunno\n";
	return 0;
}