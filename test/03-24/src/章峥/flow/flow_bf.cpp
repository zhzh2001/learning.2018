#include <fstream>
#include <queue>
#include <algorithm>
using namespace std;
ifstream fin("flow.in");
ofstream fout("flow.ans");
const int N = 100005, M = 400005, INF = 1e9;
int n, m, head[N], H[N], v[M], cap[M], C[M], nxt[M], e, s, t, dep[N];
inline void add_edge(int u, int v, int cap)
{
	::v[e] = v;
	::cap[e] = cap;
	nxt[e] = head[u];
	head[u] = e++;
}
bool bfs()
{
	queue<int> Q;
	Q.push(s);
	fill(dep + 1, dep + n + 1, 0);
	dep[s] = 1;
	while (!Q.empty())
	{
		int k = Q.front();
		Q.pop();
		if (k == t)
			return true;
		for (int i = head[k]; ~i; i = nxt[i])
			if (!dep[v[i]] && C[i])
			{
				dep[v[i]] = dep[k] + 1;
				Q.push(v[i]);
			}
	}
	return false;
}
int dfs(int k, int pred)
{
	if (k == t || !pred)
		return pred;
	int flow = 0, now;
	for (int &i = H[k]; ~i; i = nxt[i])
		if (dep[v[i]] == dep[k] + 1 && (now = dfs(v[i], min(pred, C[i]))) > 0)
		{
			flow += now;
			C[i] -= now;
			C[i ^ 1] += now;
			if (!(pred -= now))
				break;
		}
	if (!flow)
		dep[k] = -1;
	return flow;
}
int main()
{
	fin >> n >> m;
	fill(head + 1, head + n + 1, -1);
	while (m--)
	{
		int u, v, w;
		fin >> u >> v >> w;
		add_edge(u, v, w);
		add_edge(v, u, w);
	}
	int q;
	fin >> q;
	while (q--)
	{
		int opt, x, y;
		fin >> opt >> x >> y;
		if (opt)
			cap[x * 2 - 2] = cap[x * 2 - 1] = y;
		else
		{
			s = x;
			t = y;
			copy(cap, cap + e, C);
			int ans = 0;
			while (bfs())
			{
				copy(head + 1, head + n + 1, H + 1);
				ans += dfs(s, INF);
			}
			fout << ans << endl;
		}
	}
	return 0;
}