#include <fstream>
#include <random>
#include <ctime>
using namespace std;
ofstream fout("flow.in");
const int n = 100000, q = 100000;
int f[n + 5];
int getf(int x)
{
	return f[x] == x ? x : f[x] = getf(f[x]);
}
int main()
{
	minstd_rand gen(time(NULL));
	fout << n << ' ' << n - 1 << endl;
	for (int i = 1; i <= n; i++)
		f[i] = i;
	uniform_int_distribution<> d(1, n), w(1, 1e9);
	for (int i = 1; i < n;)
	{
		int u = d(gen), v = d(gen);
		if (getf(u) != getf(v))
		{
			f[getf(u)] = getf(v);
			fout << u << ' ' << v << ' ' << w(gen) << endl;
			i++;
		}
	}
	fout << q << endl;
	for (int i = 1; i <= q; i++)
	{
		uniform_int_distribution<> opt(0, 1);
		if (opt(gen))
		{
			int s = d(gen), t = d(gen);
			while (s == t)
			{
				s = d(gen);
				t = d(gen);
			}
			fout << 0 << ' ' << s << ' ' << t << endl;
		}
		else
		{
			uniform_int_distribution<> de(1, n - 1);
			fout << 1 << ' ' << de(gen) << ' ' << w(gen) << endl;
		}
	}
	return 0;
}