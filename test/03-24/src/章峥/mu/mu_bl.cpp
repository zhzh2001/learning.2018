#include <fstream>
#include <ctime>
using namespace std;
ifstream fin("mu.in");
ofstream fout("mu.ans");
const int N = 32000;
int p[N + 5];
bool bl[N + 5];
int main()
{
	int pn = 0;
	for (int i = 2; i <= N; i++)
	{
		if (!bl[i])
			p[++pn] = i;
		for (int j = 1; j <= pn && i * p[j] <= N; j++)
		{
			bl[i * p[j]] = true;
			if (i % p[j] == 0)
				break;
		}
	}
	int l, r;
	fin >> l >> r;
	for (int i = l; i <= r; i++)
	{
		int x = i, mu = 1;
		for (int j = 1; p[j] * p[j] <= x; j++)
		{
			int cnt = 0;
			for (; x % p[j] == 0; x /= p[j])
				cnt++;
			if (cnt == 1)
				mu = -mu;
			else if (cnt > 1)
			{
				mu = 0;
				break;
			}
		}
		if (x > 1)
			mu = -mu;
		fout << mu << ' ';
	}
	fout << clock() << endl;
	return 0;
}