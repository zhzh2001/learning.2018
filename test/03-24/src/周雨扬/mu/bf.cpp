#include<bits/stdc++.h>
using namespace std;
const int N=10000005;
bool fl[N];
int pri[N/10],mu[N],tot;
void init(int n){
	mu[1]=1;
	for (int i=2;i<=n;i++){
		if (!fl[i]) pri[++tot]=i,mu[i]=-1;
		for (int j=1;i*pri[j]<=n;j++){
			fl[i*pri[j]]=1;
			if (i%pri[j]==0){
				mu[i*pri[j]]=0;
				break;
			}
			mu[i*pri[j]]=-mu[i];
		}
	}
}
int main(){
	freopen("mu.in","r",stdin);
	freopen("bf.out","w",stdout); 
	init(10000000);
	int v,rk;
	scanf("%d%d",&v,&rk);
	for (int i=1;i<N;i++){
		if (mu[i]==v) rk--;
		if (!rk){
			printf("%d\n",i);
			return 0;
		}
	}
}
