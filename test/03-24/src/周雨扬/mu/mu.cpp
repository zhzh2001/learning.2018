#include<bits/stdc++.h>
using namespace std;
const int N=1000005;
const int bas=3500;
bool fl[N];
int pri[80000],mu[N],tot;
int cnt[N],f[100005][66];
int jdb[100005][66][2];
int g[1000005][2];
void init(int n){
	mu[1]=1;
	for (int i=2;i<=n;i++){
		if (!fl[i]) pri[++tot]=i,mu[i]=-1;
		for (int j=1;i*pri[j]<=n;j++){
			fl[i*pri[j]]=1;
			if (i%pri[j]==0){
				mu[i*pri[j]]=0;
				break;
			}
			mu[i*pri[j]]=-mu[i];
		}
	}
	for (int i=2;i<=n;i++)
		cnt[i]=cnt[i-1]+(!fl[i]);
	for (int i=1;i<=100000;i++) f[i][0]=i;
	for (int i=1;i<=100000;i++)
		for (int j=1;j<=65;j++)
			f[i][j]=f[i][j-1]-f[i/pri[j]][j-1];
	for (int i=1;i<=100000;i++) jdb[i][0][1]=1;
	for (int i=1;i<=100000;i++)
		for (int j=1;j<=65;j++)
			for (int k=0;k<2;k++)
				jdb[i][j][k]=jdb[i][j-1][k]+jdb[i/pri[j]][j-1][1-k];
}

#define ll long long
map<ll,int> mp;
int times2=0;
int calc(int x,int y){
	if (!x||!y) return x;
	if (x<=100000&&y<=65) return f[x][y];
	ll tmp=1ll*x*bas+y;
	if (mp.find(tmp)!=mp.end()) return mp[tmp];
	times2++;
	return mp[tmp]=calc(x,y-1)-calc(x/pri[y],y-1);
}
int countpri(int x){
	if (x<=1000000) return cnt[x];
	int tmp=1;
	for (;1ll*pri[tmp]*pri[tmp]*pri[tmp]<=x;tmp++);
	tmp--;
	int ans=calc(x,tmp);
	for (int i=tmp+1;pri[i]*pri[i]<=x;i++)
		ans-=cnt[x/pri[i]]-i+1;
	return ans+tmp-1;
}


int times=0;
map<ll,int> mp1;
int calc2(int x,int y,int z){
	if (!x) return 0; 
	if (!y||x==1) return z==1;
	if (x<pri[y]) y=cnt[x];
	if (x<=100000&&y<=65) return jdb[x][y][z];
	ll tmp=7000ll*x+y*2+z;
	if (mp1.find(tmp)!=mp1.end()) return mp1[tmp];
	times++;
	return mp1[tmp]=calc2(x,y-1,z)+calc2(x/pri[y],y-1,1-z);
}
int cogito(int l){
	int ans=0;
	for (int i=2;i*i<=l;i++)
		ans+=(-mu[i])*(l/i/i);
	return ans;
}
int work(int v,int r){
	if (!v) return cogito(r);
	/*int u=clock();
	int tmp=1;
	for (;1ll*pri[tmp]*pri[tmp]<=r;tmp++);
	tmp--;
	int ans=calc2(r,tmp,v+(v==-1));
	printf("%d\n",clock()-u);
	int sum=0;
	for (int i=1;i*i<r;i++){
		int rbound=r/i,lbound=r/(i+1);
		lbound=max(lbound,i);
		sum=sum+(mu[i]==-v);
		ans+=(countpri(rbound)-countpri(lbound))*sum;
	}
	printf("%d\n",clock()-u);
	printf("%d %d\n",times,times2);
	return ans;
	*/
	int u=clock();
	int tmp=1;
	for (;1ll*pri[tmp]*pri[tmp]*pri[tmp]<=r;tmp++);
	tmp--;
	int ans=calc2(r,tmp,v+(v==-1));
	//printf("%d\n",clock()-u);
	int lim=r/pri[tmp+1];
	for (int i=tmp+1;pri[i]<=lim;i++)
		for (int j=pri[i];j<=lim;j+=pri[i])
			g[j][0]=g[j][1]=1;
	for (int i=1;i<=lim;i++){
		g[i][0]=g[i-1][0]+1-(g[i][0]||mu[i]!=-1);
		g[i][1]=g[i-1][1]+1-(g[i][1]||mu[i]!=1);
	}
	for (int i=tmp+1;pri[i]*pri[i]<=r;i++){
		int now=v+(v==-1);
		ans+=g[r/pri[i]][1-now];
		for (int j=i+1;pri[j]*pri[j]<=r;j++)
			ans+=g[r/pri[i]/pri[j]][now];
	}
	//printf("%d\n",clock()-u);
	int sum=0;
	for (int i=1;i*i<r;i++){
		int rbound=r/i,lbound=r/(i+1);
		lbound=max(lbound,i);
		sum=sum+(mu[i]==-v);
		ans+=(countpri(rbound)-countpri(lbound))*sum;
	}
	//printf("%d %d\n",times,times2);
	return ans;
}
int wzp(int x){
	int ans=1;
	for (int i=1;pri[i]*pri[i]<=x;i++)
		if (x%pri[i]==0){
			x/=pri[i];
			if (x%pri[i]==0)
				return 0;
			ans=-ans;
		}
	if (x!=1) ans=-ans;
	return ans;
}
void bf(int v,int rk){
	for (int i=1;i<N;i++){
		if (mu[i]==v) rk--;
		if (!rk){
			printf("%d\n",i);
			exit(0);
		}
	}
}
int main(){
	freopen("mu.in","r",stdin);
	freopen("mu.out","w",stdout); 
	init(1000000);
	int v,rk; 
	scanf("%d%d",&v,&rk);
	int except=(int)rk/(v==-1?0.30418:(v==0?0.3922:0.30418));
	//printf("%d\n",except);
	if (except<=950000){
		bf(v,rk);
		return 0;
	}
	rk-=work(v,except);
	//printf("%d\n",rk);
	for (int i=except+1;;i++){
		if (wzp(i)==v) rk--;
		if (!rk){
			printf("%d\n",i);
			return 0;
		}
	}
}
