#include<bits/stdc++.h>
#define N 100005
using namespace std;
int n,m,q;
struct edge1{
	int to,next,v,id;
};
struct edge2{
	int to,next,f,mxf;
};
struct LZHAKKING{
	edge1 e[N*2];
	int head[N],dep[N],id[N];
	int fa[N],sz[N],dfn[N],val[N];
	int mn[N*4],tot,T,Val[N],top[N];
	void add(int x,int y,int z,int id){
		e[++tot]=(edge1){y,head[x],z,id};
		head[x]=tot;
	}
	void dfs1(int x){
		sz[x]=1;
		for (int i=head[x];i;i=e[i].next)
			if (e[i].to!=fa[x]){
				fa[e[i].to]=x;
				dep[e[i].to]=dep[x]+1;
				val[e[i].to]=e[i].v;
				id[e[i].id]=e[i].to;
				dfs1(e[i].to);
				sz[x]+=sz[e[i].to];
			}
	}
	void dfs2(int x,int tp){
		dfn[x]=++T; Val[T]=val[x]; top[x]=tp;
		int k=0;
		for (int i=head[x];i;i=e[i].next)
			if (sz[e[i].to]>sz[k]&&e[i].to!=fa[x])
				k=e[i].to;
		if (k) dfs2(k,tp);
		for (int i=head[x];i;i=e[i].next)
			if (e[i].to!=k&&e[i].to!=fa[x])
				dfs2(e[i].to,e[i].to);
	}
	void build(int k,int l,int r){
		if (l==r){
			mn[k]=Val[l];
			return;
		}
		int mid=(l+r)/2;
		build(k*2,l,mid);
		build(k*2+1,mid+1,r);
		mn[k]=min(mn[k*2],mn[k*2+1]);
	}
	void change(int k,int l,int r,int p,int v){
		if (l==r){
			mn[k]=v;
			return;
		}
		int mid=(l+r)/2;
		if (p<=mid) change(k*2,l,mid,p,v);
		else change(k*2+1,mid+1,r,p,v);
		mn[k]=min(mn[k*2],mn[k*2+1]);
	}
	int ask(int k,int l,int r,int x,int y){
		if (l==x&&r==y) return  mn[k];
		int mid=(l+r)/2;
		if (y<=mid) return ask(k*2,l,mid,x,y);
		if (x>mid) return ask(k*2+1,mid+1,r,x,y);
		return min(ask(k*2,l,mid,x,mid),ask(k*2+1,mid+1,r,mid+1,y));
	}
	void qmn(int x,int y){
		int ans=2e9;
		for (;top[x]!=top[y];x=fa[top[x]]){
			if (dep[top[x]]<dep[top[y]]) swap(x,y);
			ans=min(ans,ask(1,1,n,dfn[top[x]],dfn[x]));
		}
		if (dep[x]<dep[y]) swap(x,y);
		if (dfn[x]!=dfn[y])
			ans=min(ans,ask(1,1,n,dfn[y]+1,dfn[x]));
		printf("%d\n",ans);
	}
	void solve(){
		for (int i=1;i<=m;i++){
			int x,y,z;
			scanf("%d%d%d",&x,&y,&z);
			add(x,y,z,i); add(y,x,z,i);
		}
		scanf("%d",&q);
		dfs1(1);
		dfs2(1,1);
		build(1,1,n);
		for (int i=1;i<=q;i++){
			int fl,x,y;
			scanf("%d%d%d",&fl,&x,&y);
			if (fl==0) qmn(x,y);
			else change(1,1,n,dfn[id[x]],y);
		}
	}
}Sol1;
struct SZBAKKING{
	edge2 e[N*4];
	int head[N],q[N],dis[N],cur[N];
	int tot,S,T;
	SZBAKKING(){
		tot=1;
	}
	void add(int x,int y,int z){
		e[++tot]=(edge2){y,head[x],z,z};
		head[x]=tot;
	}
	bool bfs(){
		for (int i=1;i<=n;i++) dis[i]=-1;
		int h=0,t=1; q[1]=S; dis[S]=1;
		while (h!=t){
			int x=q[++h];
			for (int i=head[x];i;i=e[i].next)
				if (dis[e[i].to]==-1&&e[i].f){
					dis[e[i].to]=dis[x]+1;
					if (e[i].to==T) return 1;
					q[++t]=e[i].to;
				}
		}
		return 0;
	}
	int dfs(int x,int flow){
		if (x==T) return flow;
		int k,rest=flow;
		for (int i=cur[x];i&&rest;i=e[i].next){
			cur[x]=i;
			if (dis[e[i].to]==dis[x]+1&&e[i].f){
				int k=dfs(e[i].to,min(rest,e[i].f));
				e[i].f-=k; e[i^1].f+=k; rest-=k;
			}
		}
		if (rest) dis[x]=-1;
		return flow-rest;
	}
	void work(int x,int y){
		S=x; T=y;
		int ans=0;
		while (bfs()){
			for (int i=1;i<=n;i++)
				cur[i]=head[i];
			ans+=dfs(S,2e9);
		}
		printf("%d\n",ans);
		for (int i=2;i<=tot;i++)
			e[i].f=e[i].mxf;
	}
	void solve(){
		for (int i=1;i<=m;i++){
			int x,y,z;
			scanf("%d%d%d",&x,&y,&z);
			add(x,y,z); add(y,x,z);
		}
		scanf("%d",&::q);
		int ans=0;
		for (int i=1;i<=::q;i++){
			int fl,x,y;
			scanf("%d%d%d",&fl,&x,&y);
			if (fl==0) work(x,y);
			else{
				e[x*2].mxf=e[x*2].f=y;
				e[x*2+1].mxf=e[x*2+1].f=y;
			}
		}
	}
}Sol2;
int main(){
	freopen("flow.in","r",stdin);
	freopen("flow.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (m==n-1) Sol1.solve();
	else Sol2.solve();
}
/*
6 6
1 2 1
4 5 8
4 3 2
6 5 5
1 6 4
2 3 1
6
0 4 5
0 1 3
0 1 2
1 1 5
1 6 5
0 1 2
*/
