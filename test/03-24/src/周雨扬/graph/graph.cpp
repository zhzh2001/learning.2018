#include<bits/stdc++.h>
#define mo 998244353
#define N 100005
using namespace std;
int fac[N],inv[N];
int power(int x,int y){
	if (y<=0) return 1;
	int s=1;
	for (;y;y/=2,x=1ll*x*x%mo)
		if (y&1) s=1ll*s*x%mo;
	return s;
}
int C(int x,int y){
	return 1ll*fac[x]*inv[y]%mo*inv[x-y]%mo;
}
void prepare(){
	fac[0]=inv[0]=inv[1]=1;
	for (int i=2;i<N;i++)
		inv[i]=1ll*(mo-mo/i)*inv[mo%i]%mo;
	for (int i=1;i<N;i++){
		fac[i]=1ll*fac[i-1]*i%mo;
		inv[i]=1ll*inv[i-1]*inv[i]%mo;
	}
}
void solve(int n){
	int ans=0;
	int ok=(power(n,n-1)+2ll*mo-power(n-1,n-1)-1ll*(n-1)*power(n-1,n-2)%mo)%mo;
	ans=(ans+1ll*ok*n)%mo;
	ok=1ll*(n-1)*(power(n,n-1)+mo-power(n-1,n-1))%mo;
	ans=(ans+1ll*ok*n)%mo;
	ok=1;
	for (int i=0;i<n-1;i++){
		int way=1ll*fac[n]*inv[i]%mo;
		ans=(ans+mo-1ll*way*ok%mo)%mo;
		ok=1ll*ok*(n-1)%mo;
	}
	printf("%d\n",ans);
}
int main(){
	freopen("graph.in","r",stdin);
	freopen("graph.out","w",stdout);
	int T;
	scanf("%d",&T);
	prepare();
	while (T--){
		int x;
		scanf("%d",&x);
		solve(x);
	} 
}

