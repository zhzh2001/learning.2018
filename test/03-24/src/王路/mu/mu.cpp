#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

namespace IO {
  char buf[1 << 20];
  inline char NextChar() {
    static char *S = buf, *T = buf;
    if (S == T) {
      T = (S = buf) + fread(buf, sizeof(char), 1 << 20, stdin);
      if (S == T)
        return EOF;
    }
    return *S++;
  }
  template <typename T>
  inline void Read(T &x) {
    x = 0;
    T flag = 1;
    char ch;
    for (ch = NextChar(); isspace(ch); ch = NextChar())
      ;
    if (ch == '-') {
      ch = NextChar();
      flag = -1;
    }
    for (; isdigit(ch); ch = NextChar())
      x = x * 10 + ch - '0';
    x *= flag;
  }
} // IO

using IO::Read;

const int S = 5e6;

int not_prime[S + 1];
int mu[S + 1], prime[S + 1];

int LinearShaker(int S, int ask, int kth) {
  int tot = 0;
  mu[1] = 1;
  for (int i = 2; i <= S; i++) {
    if (!not_prime[i]) {
      mu[i] = -1;
      prime[++tot] = i;
    }
    for (int j = 1; prime[j] * i <= S; j++) {
      not_prime[prime[j] * i] = 1;
      if (i % prime[j] == 0) {
        mu[prime[j] * i] = 0;
        break;
      }
      mu[prime[j] * i] = -mu[i];
    }
    if (mu[i] == ask && --kth == 0) {
      printf("%d\n", i);
      return 0;
    }
  }
  return 1;
}

const int kReq = 1e6 + 5;

int SubTask1(int ask, int kth) {
  return LinearShaker(5e6, ask, kth);
}

int main() {
  freopen("mu.in", "r", stdin);
  freopen("mu.out", "w", stdout);
  int ask, kth;
  Read(ask), Read(kth);
  if (kth <= 1e6) {
    return SubTask1(ask, kth); // 30 Points
  }
  return 0;
}
