#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <vector>
#include <queue>
using namespace std;

namespace IO {
  char buf[1 << 20];
  inline char NextChar() {
    static char *S = buf, *T = buf;
    if (S == T) {
      T = (S = buf) + fread(buf, sizeof(char), 1 << 20, stdin);
      if (S == T)
        return EOF;
    }
    return *S++;
  }
  template <typename T>
  inline void Read(T &x) {
    x = 0;
    T flag = 1;
    char ch;
    for (ch = NextChar(); isspace(ch); ch = NextChar())
      ;
    if (ch == '-') {
      ch = NextChar();
      flag = -1;
    }
    for (; isdigit(ch); ch = NextChar())
      x = x * 10 + ch - '0';
    x *= flag;
  }
} // IO

using IO::Read;

int n, m, q;

typedef long long ll;

const int kMaxN = 1e5, kMaxM = kMaxN << 1;
const ll kInf = 1LL << 62;

struct Edge { 
  int to, nxt;
  ll flow, cap;
} e[kMaxM << 1];
int ecnt = 1, head[kMaxN], cur[kMaxN], dep[kMaxN];

inline void AddEdge(int u, int v, int w) {
  e[++ecnt] = { v, head[u], 0, w };
  head[u] = ecnt;
  e[++ecnt] = { u, head[v], w, w };
  head[v] = ecnt;
}

bool DinicBFS(int st, int ed) {
  memset(dep, 0x00, sizeof dep);
  queue<int> q;
  q.push(st);
  dep[st] = 1;
  while (!q.empty()) {
    int now = q.front();
    q.pop();
    for (int i = head[now]; i; i = e[i].nxt) {
      if (e[i].flow && !dep[e[i].to]) {
        dep[e[i].to] = dep[now] + 1;
        q.push(e[i].to);
        if (e[i].to == ed)
          return true;
      }
    }
  }
  return false;
}

int DinicDFS(int now, int ed, ll flow) {
  if (now == ed)
    return flow;
  ll w, used = 0;
  for (int i = cur[now]; i; i = e[i].nxt) {
    if (dep[e[i].to] == dep[now] + 1) {
      w = flow - used;
      w = DinicDFS(e[i].to, ed, min(e[i].flow, w));
      e[i].flow -= w;
      e[i ^ 1].flow += w;
      if (e[i].flow)
        cur[now] = i;
      used += w;
      if (used == flow)
        return flow;
    }
  }
  if (!used)
    dep[now] = -1;
  return used; 
}

int DinicMaxFlow(int st, int ed) {
  int flow = 0;
  while (DinicBFS(st, ed)) {
    for (int i = 0; i <= n; ++i)
      cur[i] = head[i];
    flow += DinicDFS(st, ed, kInf);
  }
  return flow;
}

int main() {
  freopen("flow.in", "r", stdin);
  freopen("flow.out", "w", stdout);
  Read(n), Read(m);
  for (int i = 1, u, v, w; i <= m; ++i) {
    Read(u), Read(v), Read(w);
    AddEdge(u, v, w);
    AddEdge(v, u, w);
  }
  Read(q);
  if (q == 1) { // 10 Points (fake)
    int opt, a, b;
    Read(opt), Read(a), Read(b);
    printf("%d\n", DinicMaxFlow(a, b));
    return 0;
  }
  return 0;
}