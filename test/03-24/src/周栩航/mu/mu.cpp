#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<ctime>
#include<cstdlib>
#include<algorithm>
#include<queue>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define inf 1e9
#define pb push_back
using namespace std;
inline int read()
{
	int t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0)	{putchar('-');write(-x);return;}if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
int  mu[10000001],pri[2000001],tot,ask,kth,N,q[200001],qv[200001],top;
bool bj[10000001];
inline void Pre()
{
	mu[1]=1;

	For(i,2,N)
	{
		if(!bj[i])	pri[++tot]=i,mu[i]=-1;
		For(j,1,tot)
		{
			if(i*pri[j]>N)	break;
			if(i%pri[j]==0){bj[i*pri[j]]=1;mu[i*pri[j]]=0;break;}
			mu[i*pri[j]]=mu[i]*mu[pri[j]];bj[i*pri[j]]=1;
		}
	}
}
inline bool check(ll tep)
{
	ll tmp=0;
	For(i,1,top)	tmp+=((tep)/q[i])*qv[i];
	if(tmp>=kth)	return 1;else return 0;	
}
int main()
{
	freopen("mu.in","r",stdin);freopen("mu.out","w",stdout);
	ask=read();kth=read();
	if(ask==0)
	{
		N=sqrt(1e9)+1;
		Pre();
		For(i,2,N)
		{
			if(mu[i]==1)	q[++top]=i*i,qv[top]=-1;
			if(mu[i]==-1)	q[++top]=i*i,qv[top]=1;
		}
		
		ll l=1,r=1e10,ans=0;
		while(l<=r)
		{
			ll mid=l+r>>1;
			if(check(mid))	ans=mid,r=mid-1;else	l=mid+1;
		}
		writeln(ans);
	}
	else
	{
		N=5e6+5;
		Pre();
		For(i,1,N)	if(mu[i]==ask)	
		{
			kth--;
			if(kth==0)	{writeln(i);return 0;}
		}
	}
}