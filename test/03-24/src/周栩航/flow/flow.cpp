#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<ctime>
#include<cstdlib>
#include<algorithm>
#include<queue>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define inf 1e9
#define pb push_back
using namespace std;
inline int read()
{
	int t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(int x){if(x<0)	{putchar('-');write(-x);return;}if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(int x){write(x);puts("");}
inline void write_p(int x){write(x);putchar(' ');}
const int N=150100;
int poi[N*2],nxt[N*2],head[N*2],v[N*2],dep[N],siz[N],top[N],Fa[N],st[N][19],dfn[N],to[N],tim,cnt,S,T,dis[N],w[N*2];
int x[N],y[N],z[N],n,m,tr[N*4],cha[N],tv[N],son[N],p1,p2;
inline void add(int x,int y,int z){poi[++cnt]=y;nxt[cnt]=head[x];head[x]=cnt;v[cnt]=z;}
inline void add1(int x,int y,int z)
{
	poi[++cnt]=y;nxt[cnt]=head[x];head[x]=cnt;w[cnt]=z;
	poi[++cnt]=x;nxt[cnt]=head[y];head[y]=cnt;w[cnt]=0;
}
inline bool Bfs()
{
	For(i,1,n)	dis[i]=0;dis[S]=1;
	queue<int> Que;
	Que.push(S);
	while(!Que.empty())
	{
		int x=Que.front();Que.pop();
		for(int i=head[x];i;i=nxt[i])
		{
			if(w[i]&&!dis[poi[i]])	dis[poi[i]]=dis[x]+1,Que.push(poi[i]);
		}
	}
	return dis[T];
}
inline int Dfs(int x,int flow)
{
	if(x==T)	return flow;
	int used=0;
	for(int i=head[x];i;i=nxt[i])
	{
		if(dis[poi[i]]==dis[x]+1&&w[i])
		{
			int tmp=Dfs(poi[i],min(w[i],flow-used));
			w[i]-=tmp;w[i^1]+=tmp;used+=tmp;
			if(used==flow)	return used;
		}
	}
	dis[x]=-1;
	return used;
}
inline void Dinic()
{
	int flow=0;
	while(Bfs())
		flow+=Dfs(S,inf);
	writeln(flow);
}
inline void Dfs1(int x,int fa)
{
	Fa[x]=fa;siz[x]=1;
	for(int i=head[x];i;i=nxt[i])
	{
		if(poi[i]==fa)	continue;
		dep[poi[i]]=dep[x]+1;
		Dfs1(poi[i],x);
		siz[x]+=siz[poi[i]];
		if(siz[poi[i]]>siz[son[x]])	son[x]=poi[i];
	}
}
inline void Dfs2(int x,int aci)
{
	top[x]=aci;dfn[x]=++tim;to[tim]=x;
	// if(son[x])	Dfs2(son[x],aci),tv[son[x]]=v[];
	for(int i=head[x];i;i=nxt[i])	if(poi[i]==son[x])	
	{
		Dfs2(poi[i],aci);
		tv[poi[i]]=v[i];
		cha[(i+1)/2]=poi[i];
		break;
	}
	for(int i=head[x];i;i=nxt[i])
	{
		if(poi[i]==Fa[x]||poi[i]==son[x])	continue;
		Dfs2(poi[i],poi[i]);
		tv[poi[i]]=v[i];cha[(i+1)/2]=poi[i];//tv[i]表示i号点连向父亲的边的权 cha[i]第i条边的权在哪个点上
	}
}
inline int lca(int x,int y)
{
	while(top[x]!=top[y])
	{
		if(dep[top[x]]<dep[top[y]])	swap(x,y);
		x=Fa[top[x]];
	}
	return dep[x]<dep[y]?x:y;
}
inline void Build(int x,int l,int r)
{
	if(l==r){tr[x]=tv[to[l]];return;}
	int mid=l+r>>1;
	Build(x<<1,l,mid);Build(x<<1|1,mid+1,r);
	tr[x]=min(tr[x<<1],tr[x<<1|1]);
}
inline void Upd(int x,int l,int r,int to,int cv)
{
	if(l==r)	{tr[x]=cv;return;}
	int mid=l+r>>1;
	if(to<=mid)	Upd(x<<1,l,mid,to,cv);	else	Upd(x<<1|1,mid+1,r,to,cv);
	tr[x]=min(tr[x<<1],tr[x<<1|1]);
}
inline int Query(int x,int l,int r,int ql,int qr)
{
	if(ql<=l&&r<=qr)	return tr[x];
	int mid=l+r>>1,tmp=1e9;
	if(ql<=mid)	tmp=min(tmp,Query(x<<1,l,mid,ql,qr));
	if(qr>=mid+1)	tmp=min(tmp,Query(x<<1|1,mid+1,r,ql,qr));
	return tmp;
}
inline void Pre(){For(i,1,n)	st[i][0]=Fa[i];For(j,1,18)	For(i,1,n)	st[i][j]=st[st[i][j-1]][j-1];}
inline int up(int x,int del){Dow(i,0,18)	if(del>>i&1)	x=st[x][i];return x;}
inline int Solve(int tx,int ty)
{
	if(tx==ty)	return 1e9;
	int tmp=1e9;
	ty=up(tx,dep[tx]-dep[ty]-1);
	while(top[tx]!=top[ty])
	{
		
		tmp=min(tmp,Query(1,1,tim,dfn[top[tx]],dfn[tx]));
		tx=Fa[top[tx]];
	}
	// cout<<dfn[ty]<<' '<<dfn[tx]<<endl;
	tmp=min(tmp,Query(1,1,tim,dfn[ty],dfn[tx]));
	return tmp;
}
int opt;
inline void main2()
{
	For(i,1,m)	x[i]=read(),y[i]=read(),z[i]=read();
	int Q=read();
	For(tt,1,Q)
	{
		opt=read();S=read();T=read();
		if(opt==1)	z[S]=T;
		else
		{
			For(i,1,n)	head[i]=0;cnt=1;
			For(i,1,m)	add1(x[i],y[i],z[i]),add1(y[i],x[i],z[i]);
			Dinic();
		}		
	}
}
int main()
{
	freopen("flow.in","r",stdin);freopen("flow.out","w",stdout);
	n=read();m=read();
	if(m!=n-1)
		{main2();return 0;}
	For(i,1,m)	x[i]=read(),y[i]=read(),z[i]=read(),add(x[i],y[i],z[i]),add(y[i],x[i],z[i]);
	Dfs1(1,1);
	Dfs2(1,1);
	Pre();
	tv[1]=1e9;
	Build(1,1,tim);
	int Q=read();
	For(tt,1,Q)
	{
		opt=read();p1=read();p2=read();
		if(opt==1)
		{
			Upd(1,1,tim,dfn[cha[p1]],p2);
		}else
		{
			int t=lca(p1,p2);
			writeln(min(Solve(p1,t),Solve(p2,t)));
		}
	}
}
/*
20 19
2 1 24931
3 2 28423
4 3 24547
5 4 23341
6 2 4403
7 3 8145
8 6 30579
9 4 11566
10 8 19138
11 8 29565
12 6 11604
13 9 22448
14 1 26052
15 8 24675
16 13 5036
17 10 12048
18 16 4724
19 2 6530
20 1 32359
1
0 1 9


*/