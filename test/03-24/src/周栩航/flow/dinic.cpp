#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<ctime>
#include<cstdlib>
#include<algorithm>
#include<queue>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define inf 1e9
#define pb push_back
using namespace std;
inline int read()
{
	int t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(int x){if(x<0)	{putchar('-');write(-x);return;}if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(int x){write(x);puts("");}
inline void write_p(int x){write(x);putchar(' ');}
const int N=500001;
int cnt=1,head[N],poi[N],w[N],nxt[N],dis[N],x[N],y[N],z[N],opt,S,T,n,m;
inline void add(int x,int y,int z)
{
	poi[++cnt]=y;nxt[cnt]=head[x];head[x]=cnt;w[cnt]=z;
	poi[++cnt]=x;nxt[cnt]=head[y];head[y]=cnt;w[cnt]=0;
}
inline bool Bfs()
{
	For(i,1,n)	dis[i]=0;dis[S]=1;
	queue<int> Que;
	Que.push(S);
	while(!Que.empty())
	{
		int x=Que.front();Que.pop();
		for(int i=head[x];i;i=nxt[i])
		{
			if(w[i]&&!dis[poi[i]])	dis[poi[i]]=dis[x]+1,Que.push(poi[i]);
		}
	}
	return dis[T];
}
inline int Dfs(int x,int flow)
{
	if(x==T)	return flow;
	int used=0;
	for(int i=head[x];i;i=nxt[i])
	{
		if(dis[poi[i]]==dis[x]+1&&w[i])
		{
			int tmp=Dfs(poi[i],min(w[i],flow-used));
			w[i]-=tmp;w[i^1]+=tmp;used+=tmp;
			if(used==flow)	return used;
		}
	}
	dis[x]=-1;
	return used;
}
inline void Dinic()
{
	int flow=0;
	while(Bfs())
		flow+=Dfs(S,inf);
	writeln(flow);
}
int Q;
int main()
{
	freopen("flow.in","r",stdin);freopen("flow1.out","w",stdout);
	n=read();m=read();
	For(i,1,m)	x[i]=read(),y[i]=read(),z[i]=read();
	Q=read();
	For(tt,1,Q)
	{
		opt=read();S=read();T=read();
		if(opt==1)	z[S]=T;
		{
		else
			For(i,1,n)	head[i]=0;cnt=1;
			For(i,1,m)	add(x[i],y[i],z[i]),add(y[i],x[i],z[i]);
			Dinic();
		}		
	}

}
/*
6 6
1 2 1
4 5 8
4 3 2
6 5 5
1 6 4
2 3 1
6
0 4 5
0 1 3
0 1 2
1 1 5
1 6 5
0 1 2
*/