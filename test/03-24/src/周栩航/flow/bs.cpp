#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<ctime>
#include<cstdlib>
#include<algorithm>
#include<queue>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define inf 1e9
#define pb push_back
using namespace std;
inline int read()
{
	int t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(int x){if(x<0)	{putchar('-');write(-x);return;}if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(int x){write(x);puts("");}
inline void write_p(int x){write(x);putchar(' ');}
const int N=150100;
int poi[N*2],nxt[N*2],head[N*2],v[N*2],dep[N],siz[N],top[N],Fa[N],st[N][19],dfn[N],to[N],tim,cnt,S,T,dis[N],w[N*2];
int x[N],y[N],z[N],n,m,tr[N*4],cyc_tr[N*4],cha[N],tv[N],son[N],p1,p2,TIM,poi1[N*2],nxt1[N*2],head1[N*2],v1[N*2];
int Dfn[N],Low[N],tocyc[N],cyc_to[N],cyc_size,rt,cnt1,inq[N],q[N],cyc_tv[N],Top,num,fac[N],number[N],cyc;
inline void add(int x,int y,int z){poi1[++cnt1]=y;nxt1[cnt1]=head1[x];head1[x]=cnt1;v1[cnt1]=z;}
inline void Add(int x,int y,int z){poi[++cnt]=y;nxt[cnt]=head[x];head[x]=cnt;v[cnt]=z;}
inline void Tar(int x,int fa)
{
	Low[x]=Dfn[x]=++TIM;inq[x]=1;q[++Top]=x;
	for(int i=head1[x];i;i=nxt1[i])
	{
		if(poi1[i]==fa)	continue;
		if(!Dfn[poi1[i]])	{Tar(poi1[i],x);Low[x]=min(Low[x],Low[poi1[i]]);}
		else	if(inq[poi1[i]])	Low[x]=min(Low[x],Low[poi1[i]]);
	}
	if(Dfn[x]==Low[x])
	{
		++num;
		int tcnt=0;
		while(1)
		{
			fac[q[Top]]=num;number[q[Top]]=++tcnt;inq[q[Top]]=0;//点在环中的编号
			Top--;	
			if(q[Top+1]==x)	break;
		}
		if(tcnt!=1)	cyc=num,cyc_size=tcnt;
	}
}
inline void Dfs1(int x,int fa)
{
	Fa[x]=fa;siz[x]=1;
	for(int i=head[x];i;i=nxt[i])
	{
		if(poi[i]==fa)	continue;
		dep[poi[i]]=dep[x]+1;
		Dfs1(poi[i],x);
		siz[x]+=siz[poi[i]];
		if(siz[poi[i]]>siz[son[x]])	son[x]=poi[i];
	}
}
inline void Dfs2(int x,int aci)
{
	top[x]=aci;dfn[x]=++tim;to[tim]=x;
	// if(son[x])	Dfs2(son[x],aci),tv[son[x]]=v[];
	for(int i=head[x];i;i=nxt[i])	if(poi[i]==son[x])	
	{
		Dfs2(poi[i],aci);
		tv[poi[i]]=v[i];
		cha[(i+1)/2]=poi[i];
		break;
	}
	for(int i=head[x];i;i=nxt[i])
	{
		if(poi[i]==Fa[x]||poi[i]==son[x])	continue;
		Dfs2(poi[i],poi[i]);
		tv[poi[i]]=v[i];cha[(i+1)/2]=poi[i];//tv[i]表示i号点连向父亲的边的权 cha[i]第i条边的权在哪个点上
	}
}
inline int lca(int x,int y)
{
	while(top[x]!=top[y])
	{
		if(dep[top[x]]<dep[top[y]])	swap(x,y);
		x=Fa[top[x]];
	}
	return dep[x]<dep[y]?x:y;
}
inline void Build(int x,int l,int r)
{
	if(l==r){tr[x]=tv[to[l]];return;}
	int mid=l+r>>1;
	Build(x<<1,l,mid);Build(x<<1|1,mid+1,r);
	tr[x]=min(tr[x<<1],tr[x<<1|1]);
}
inline void Upd(int x,int l,int r,int to,int cv)
{
	if(l==r)	{tr[x]=cv;return;}
	int mid=l+r>>1;
	if(to<=mid)	Upd(x<<1,l,mid,to,cv);	else	Upd(x<<1|1,mid+1,r,to,cv);
	tr[x]=min(tr[x<<1],tr[x<<1|1]);
}
inline int Query(int x,int l,int r,int ql,int qr)
{
	if(ql<=l&&r<=qr)	return tr[x];
	int mid=l+r>>1,tmp=1e9;
	if(ql<=mid)	tmp=min(tmp,Query(x<<1,l,mid,ql,qr));
	if(qr>=mid+1)	tmp=min(tmp,Query(x<<1|1,mid+1,r,ql,qr));
	return tmp;
}
inline void Pre(){For(i,1,num)	st[i][0]=Fa[i];For(j,1,18)	For(i,1,num)	st[i][j]=st[st[i][j-1]][j-1];}
inline int up(int x,int del){Dow(i,0,18)	if(del>>i&1)	x=st[x][i];return x;}
inline int Solve(int tx,int ty)
{

	if(tx==ty)	return 1e9;
	int tmp=1e9;
	ty=up(tx,dep[tx]-dep[ty]-1);
	while(top[tx]!=top[ty])
	{
		tmp=min(tmp,Query(1,1,tim,dfn[top[tx]],dfn[tx]));
		tx=Fa[top[tx]];
	}	
	tmp=min(tmp,Query(1,1,tim,dfn[ty],dfn[tx]));
	return tmp;
}
inline void Build_cyc(int x,int l,int r)
{
	if(l==r){cyc_tr[x]=cyc_tv[l];return;}
	int mid=l+r>>1;
	Build_cyc(x<<1,l,mid);Build_cyc(x<<1|1,mid+1,r);
	cyc_tr[x]=min(cyc_tr[x<<1],cyc_tr[x<<1|1]);
}
inline void cyc_Upd(int x,int l,int r,int to,int cv)
{
	if(l==r)	{cyc_tr[x]=cv;return;}
	int mid=l+r>>1;
	if(to<=mid)	cyc_Upd(x<<1,l,mid,to,cv);	else	cyc_Upd(x<<1|1,mid+1,r,to,cv);
	cyc_tr[x]=min(cyc_tr[x<<1],cyc_tr[x<<1|1]);
}
inline int cyc_Query(int x,int l,int r,int ql,int qr)
{
	if(ql>qr)	return 1e9;
	if(ql<=l&&r<=qr)	return cyc_tr[x];
	int mid=l+r>>1,tmp=1e9;
	if(ql<=mid)	tmp=min(tmp,cyc_Query(x<<1,l,mid,ql,qr));
	if(qr>=mid+1)	tmp=min(tmp,cyc_Query(x<<1|1,mid+1,r,ql,qr));
	return tmp;
}
inline int Solve_cyc(int tx,int ty)
{
	if(tx>ty)	swap(tx,ty);
	int tmp=min(cyc_Query(1,1,cyc_size,1,tx),cyc_Query(1,1,cyc_size,ty+1,cyc_size))+cyc_Query(1,1,cyc_size,tx+1,ty);
	return tmp;
}
int main()
{
	freopen("flow.in","r",stdin);freopen("flow2.out","w",stdout);
	n=read();m=read();
	For(i,1,m)	x[i]=read(),y[i]=read(),z[i]=read(),add(x[i],y[i],z[i]),add(y[i],x[i],z[i]);
	For(i,1,n)	if(!Dfn[i])	Tar(i,i);
	For(i,1,n)	if(fac[i]==cyc)
		for(int t=head1[i];t;t=nxt1[t])
		{
			if(fac[poi1[t]]!=cyc)	tocyc[fac[poi1[t]]]=i;//点的父亲在环中位置
			else	if(number[i]<number[poi1[t]]||(number[i]==cyc_size&&number[poi1[t]]==1))	
				cyc_tv[number[poi1[t]]]=v1[t],cyc_to[t]=number[poi1[i]];
		}
	Build_cyc(1,1,cyc_size);	
	For(i,1,m)	if(fac[x[i]]!=fac[y[i]])	Add(fac[x[i]],fac[y[i]],z[i]),Add(fac[y[i]],fac[x[i]],z[i]);
	rt=cyc;
	Dfs1(rt,rt);
	Dfs2(rt,rt);
	Pre();
	tv[rt]=1e9;
	Build(1,1,tim);
	int Q=read();
	For(zyyakking,1,Q)
	{
		int opt=read();p1=read();p2=read();
		if(opt==0)
		{
			int t=lca(fac[p1],fac[p2]);
//			cout<<dfn[t]<<' '<<dfn[fac[p1]]<<' '<<rt<<endl;
			int ans=min(Solve(fac[p1],t),Solve(fac[p2],t));
			if(t==rt)
			{
				int t1=fac[p1],t2=fac[p2];
				if(t1!=rt)	t1=tocyc[up(t1,dep[t1]-1)]; else t1=p1;if(t2!=rt)	t2=tocyc[up(t2,dep[t2]-1)];else t2=p2;
				t1=number[t1];t2=number[t2];
				// cout<<t1<<'-'<<t2<<endl;
				ans=min(ans,Solve_cyc(t1,t2));
			}
			writeln(ans);
		}
		else
		{
			if(fac[x[p1]]!=fac[y[p1]])
			{
				Upd(1,1,tim,dfn[cha[p1]],p2);
			}	else
			{
				int l=number[x[p1]],r=number[y[p1]];
				if((l>r))swap(l,r);
				if(l==1&&r==cyc_size)	swap(l,r);
				cyc_Upd(1,1,cyc_size,r,p2);
			}
		}
	}
}
/*
6 6
1 2 1
4 5 8
4 3 2
6 5 5
1 6 4
2 3 1	
6
0 4 5
0 1 3
0 1 2
1 1 5
1 6 5
0 1 2
*/
