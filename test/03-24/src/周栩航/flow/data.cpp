#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<ctime>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<ctime>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define inf 1e9
#define pb push_back
using namespace std;
inline int read()
{
	int t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(int x){if(x<0)	{putchar('-');write(-x);return;}if(x>=10)	write(x/10);putchar(x%10+'0');}
inline void writeln(int x){write(x);puts("");}
inline void write_p(int x){write(x);putchar(' ');}
int n,m,x,y,opt,q;
int main()
{
	freopen("flow.in","w",stdout);
	srand(time(0));
	n=2000;m=n;
	cout<<n<<' '<<m<<endl;
	For(i,2,n)
	{
		write_p(i);write_p(rand()%(i-1)+1);writeln(rand());
	}
	write_p(rand()%n+1);write_p(rand()%n+1);writeln(rand());
	q=1000;
	writeln(q);
	For(i,1,q)
	{
		opt=rand()%2;
		write_p(opt);
		if(opt==1)
		{
			x=rand()%m+1;y=rand();
			write_p(x);writeln(y);
		}
		else
		{
			x=rand()%n+1,y=rand()%n+1;
			while(x==y)	x=rand()%n+1,y=rand()%n+1;
			write_p(x);writeln(y);
		}
	}
}