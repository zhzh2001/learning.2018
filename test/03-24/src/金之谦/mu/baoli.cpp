#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=5e6;
bool b[N+10];
int pri[N/5+10],u[N+10],cnt=0;
int u1[N/2],u0[N/2],ue[N/2],c1,c0,ce;
inline void get(){
	u[1]=1;
	for(int i=2;i<=N;i++){
		if(!b[i]){pri[++cnt]=i;u[i]=-1;}
		for(int j=1;j<=cnt;j++){
			if(i*pri[j]>N)break;
			b[i*pri[j]]=1;
			if(i%pri[j]==0)break;
			else u[i*pri[j]]=-u[i];
		}
	}
	for(int i=1;i<=N;i++){
		if(u[i]==1)u1[++c1]=i;
		if(u[i]==0)u0[++c0]=i;
		if(u[i]==-1)ue[++ce]=i;
	}
}
int main()
{
	freopen("mu.in","r",stdin);
	freopen("mu.out","w",stdout);
	get();
	int op=read(),x=read();
	if(op==0)writeln(u0[x]);
	if(op==1)writeln(u1[x]);
	if(op==-1)writeln(ue[x]);
	
	return 0;
}
