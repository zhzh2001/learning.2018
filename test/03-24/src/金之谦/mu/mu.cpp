#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
map<int,int>mmu;
const int N=5e6;
bool b[N+10];
int pri[N/5+10],mu[N+10],cnt=0;
inline void get(){
	mu[1]=1;
	for(int i=2;i<=N;i++){
		if(!b[i]){pri[++cnt]=i;mu[i]=-1;}
		for(int j=1;j<=cnt;j++){
			if(i*pri[j]>N)break;
			b[i*pri[j]]=1;
			if(i%pri[j]==0)break;
			else mu[i*pri[j]]=-mu[i];
		}
	}
	for(int i=2;i<=N;i++)mu[i]+=mu[i-1];
}
inline int getmu(int x){
	if(x<=N)return mu[x];
	if(mmu[x])return mmu[x];
	int sum=1;
	for(int i=2;i<=x;){
		int j=x/(x/i);
		sum-=getmu(x/i)*(j-i+1);
		i=j+1;
	}
	return mmu[x]=sum;
}
inline int sqr(int x){return x*x;}
int ans=0;
inline int dfs(int x,int s,int k,int v){
	if(s>1)ans=ans+k*(v/s);
	for(int i=x+1;i<=cnt;i++){
		if(s*sqr(pri[i])>v)break;
		dfs(i,s*sqr(pri[i]),-k,v);
	}
}
inline int check(int x){
	ans=0;dfs(0,1,-1,x);
	return ans;
}
signed main()
{
	freopen("mu.in","r",stdin);
	freopen("mu.out","w",stdout);
	get();
	int op=read(),x=read();
	if(op==0){
		int l=x,r=3*x,ans=0;
		while(l<=r){
			int mid=l+r>>1;
			if(check(mid)>=x)ans=mid,r=mid-1;
			else l=mid+1;
		}
		writeln(ans);
	}
	if(op==1){
		int l=x,r=4*x,ans=0;
		while(l<=r){
			int mid=l+r>>1;
			int p=mid-check(mid),q=getmu(mid);
			if(((p+q)>>1)>=x)ans=mid,r=mid-1;
			else l=mid+1;
		}
		writeln(ans);
	}
	if(op==-1){
		int l=x,r=4*x,ans=0;
		while(l<=r){
			int mid=l+r>>1;
			int p=mid-check(mid),q=getmu(mid);
			if(p-((p+q)>>1)>=x)ans=mid,r=mid-1;
			else l=mid+1;
		}
		writeln(ans);
	}
	return 0;
}
