#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=1e5+10;
struct edge{int x,y;}a[2*N];
int nedge=0,p[4*N],c[4*N],nex[4*N],head[4*N],n,m;
inline void addedge(int a,int b,int v){
	p[++nedge]=b;nex[nedge]=head[a];head[a]=nedge;c[nedge]=v;
}
namespace subtask1{
	int val[N];
	int deep[N],fa[N],top[N],s[N],son[N],sx[N],xs[N],cnt=0;
	inline void addedge(int a,int b){
		p[++nedge]=b;nex[nedge]=head[a];head[a]=nedge;
	}
	inline void dfs(int x,int Fa){
		deep[x]=deep[Fa]+1;fa[x]=Fa;s[x]=1;
		for(int k=head[x];k;k=nex[k])if(p[k]!=Fa){
			dfs(p[k],x);val[p[k]]=c[k];
			s[x]+=s[p[k]];
			if(s[son[x]]<s[p[k]])son[x]=p[k];
		}
	}
	inline void dfss(int x,int t){
		top[x]=t;sx[x]=++cnt;xs[cnt]=x;
		if(son[x])dfss(son[x],t);
		for(int k=head[x];k;k=nex[k])if(p[k]!=fa[x]&&p[k]!=son[x])
			dfss(p[k],p[k]);
	}
	int t[4*N];
	inline void build(int l,int r,int nod){
		if(l==r){t[nod]=val[xs[l]];return;}
		int mid=l+r>>1;
		build(l,mid,nod*2);build(mid+1,r,nod*2+1);
		t[nod]=min(t[nod*2],t[nod*2+1]);
	}
	inline void xg(int l,int r,int x,int v,int nod){
		if(l==r){t[nod]=v;return;}
		int mid=l+r>>1;
		if(x<=mid)xg(l,mid,x,v,nod*2);else xg(mid+1,r,x,v,nod*2+1);
		t[nod]=min(t[nod*2],t[nod*2+1]);
	}
	inline int smin(int l,int r,int i,int j,int nod){
		if(l>=i&&r<=j)return t[nod];
		int mid=l+r>>1,ans=1e9;
		if(i<=mid)ans=smin(l,mid,i,j,nod*2);
		if(j>mid)ans=min(ans,smin(mid+1,r,i,j,nod*2+1));
		return ans;
	}
	inline int fmin(int x,int y){
		int fx=top[x],fy=top[y],ans=1e9;
		while(fx!=fy){
			if(deep[fx]<deep[fy])swap(x,y),swap(fx,fy);
			ans=min(ans,smin(1,n,sx[fx],sx[x],1));
			x=fa[fx];fx=top[x];
		}
		if(deep[x]>deep[y])swap(x,y);
		ans=min(ans,smin(1,n,sx[x]+1,sx[y],1));
		return ans;
	}
}
namespace subtask3{
	bool vis[N];
	int dist[N],cur[4*N],s,t;
	inline int ff(int x){return (x&1)?x+1:x-1;}
	inline bool bfs(){
		memset(dist,-1,sizeof dist);dist[s]=1;
		queue<int>q;q.push(s);
		while(!q.empty()){
			int now=q.front();q.pop();
			for(int k=head[now];k;k=nex[k])if(c[k]&&dist[p[k]]==-1){
				dist[p[k]]=dist[now]+1;
				if(!vis[p[k]]){
					vis[p[k]]=1;q.push(p[k]);
				}
			}
			vis[now]=0;
		}
		return dist[t]>-1;
	}
	inline int dfs(int x,int low){
		if(x==t)return low;
		int used=0,a;
		for(int k=cur[x];k;k=nex[k]){
			cur[x]=k;
			if(c[k]&&dist[x]+1==dist[p[k]]){
				a=dfs(p[k],min(c[k],low-used));
				if(a)c[k]-=a,c[ff(k)]+=a,used+=a;
				if(used==low)break;
			}
		}
		if(!used)dist[x]=-1;
		return used;
	}
	inline int dinic(){
		int flow=0;
		while(bfs()){
			for(int i=s;i<=t;i++)cur[i]=head[i];
			flow+=dfs(s,1e9);
		}
		return flow;
	}	
}
int main()
{
	freopen("flow.in","r",stdin);
	freopen("flow.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=m;i++){
		int x=read(),y=read(),v=read();
		if(x==y){i--,m--;continue;}
		a[i]=(edge){x,y};
		addedge(x,y,v);addedge(y,x,v);
	}
	if(m==n-1){
		using namespace subtask1;
		dfs(1,0);dfss(1,1);build(1,n,1);
		for(int T=read();T;T--){
			int op=read(),x=read(),y=read();
			if(op==0)writeln(fmin(x,y));
			else{
				int lx=a[x].x,ly=a[x].y;
				if(fa[ly]==lx)swap(lx,ly);xg(1,n,sx[lx],y,1);
			}
		}return 0;
	}
	int Q=read();
	if(n<=2000&&Q==1){
		using namespace subtask3;
		s=0;t=n+1;read();
		int x=read(),y=read();
		addedge(s,x,1e9);addedge(x,s,0);
		addedge(y,t,1e9);addedge(t,y,0);
		writeln(dinic());
	}
	return 0;
}
