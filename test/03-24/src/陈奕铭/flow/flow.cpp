#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}
const int N = 100005,M = 2005;
int n,m,Q,st,ed;
struct edge{ int u,v,w; }a[N*2];
int Mn[N*4];
int head[N],to[N*2],nxt[N*2],w[N*2],cnt,cur[N];
int dfn[N],dep[N],DFN,son[N],fa[N],top[N],size[N],val[N],p[N];
bool vis[N];
inline int Min(int x,int y){
	if(x < y) return x;
	return y;
}

inline void insert(int x,int y,int z){
	to[++cnt] = y; nxt[cnt] = head[x]; head[x] = cnt; w[cnt] = z;
}

inline void dfs1(int x,int f){
	fa[x] = f; size[x] = 1; dep[x] = dep[f]+1;
	// bool flag = false;
	for(int i = head[x],v = to[i];i;i = nxt[i],v = to[i]){
		if(v == f) continue;
		dfs1(v,x); val[v] = w[i];
		size[x] += size[v];
		if((son[x] == 0) || size[v] > size[son[x]])
			son[x] = v;
	}
}
inline void dfs2(int x,int f){
	top[x] = f; dfn[x] = ++DFN; p[DFN] = x;
	if(son[x] != 0){
		dfs2(son[x],f);
		for(int i = head[x],v = to[i];i;i = nxt[i],v = to[i])
			if(v != fa[x] && v != son[x])
				dfs2(v,v);
	}
}

inline void build(int num,int l,int r){
	if(l == r) {Mn[num] = val[p[l]];return;}
	int mid = (l+r)>>1;
	build(num<<1,l,mid); build(num<<1|1,mid+1,r);
	Mn[num] = Min(Mn[num<<1],Mn[num<<1|1]);
}
inline void change(int num,int l,int r,int x){
	if(l == r){Mn[num] = val[p[l]]; return;}
	int mid = (l+r)>>1;
	if(x <= mid) build(num<<1,l,mid);
	else build(num<<1|1,mid+1,r);
	Mn[num] = Min(Mn[num<<1],Mn[num<<1|1]);
}
inline int query(int num,int l,int r,int x,int y){
	if(x <= l && r <= y) return Mn[num];
	int mid = (l+r)>>1,ans = 1e9;
	if(x <= mid) ans = Min(ans,query(num<<1,l,mid,x,y));
	if(y > mid) ans = Min(ans,query(num<<1|1,mid+1,r,x,y));
	return ans;
}

inline void getpath(int x,int y){
	int ans = 1e9;
	int fx = top[x],fy = top[y];
	while(fx != fy){
		if(dep[x] < dep[y]){
			swap(x,y); swap(fx,fy);
		}
		ans = Min(ans,query(1,1,n,dfn[fx],dfn[x]));
		x = fa[fx]; fx = top[x];
	}
	if(dep[x] < dep[y]) ans = Min(ans,query(1,1,n,dfn[x]+1,dfn[y]));
	else ans = Min(ans,query(1,1,n,dfn[y]+1,dfn[x]));
	printf("%d\n", ans);
}

inline bool bfs(){
	queue<int> q; while(!q.empty()) q.pop();
	memset(dep,0,sizeof dep); dep[st] = 1;
	q.push(st);
	do{
		int u = q.front(); q.pop();
		for(int i = head[u];i != -1;i = nxt[i])
			if(dep[to[i]] == 0 && (w[i] > 0))
				dep[to[i]] = dep[u]+1,q.push(to[i]);
	}while(!q.empty());
	if(dep[ed] > 0) return true;
	return false;
}
inline int dfs(int x,int flow){
	if(x == ed) return flow;
	for(int &i = cur[x];i != -1;i = nxt[i])
		if(dep[to[i]] == dep[x]+1 && w[i] != 0){
			int di = dfs(to[i],Min(flow,w[i]));
			if(di > 0){
				w[i] -= di;
				w[i^1] += di;
				return di;
			}
		}
	return 0;
}
inline void dinic(){
	int ans = 0;
	while(bfs()){
		for(int i = 1;i <= n;++i) cur[i] = head[i];
		while(int d = dfs(st,1e9)) ans += d;
	}
	printf("%d\n", ans);
}

signed main(){
	freopen("flow.in","r",stdin);
	freopen("flow.out","w",stdout);
	n = read(); m = read();
	for(int i = 1;i <= m;++i){
		a[i].u = read(); a[i].v = read(); a[i].w = read();
	}
	Q = read();
	if(Q == 1 && m != n-1){
		read(); st = read(); ed = read();
		memset(head,-1,sizeof head); cnt = -1;
		for(int i = 1;i <= m;++i){
			insert(a[i].u,a[i].v,a[i].w);
			insert(a[i].v,a[i].u,0);
			insert(a[i].v,a[i].u,a[i].w);
			insert(a[i].u,a[i].v,0);
		}
		dinic();
		return 0;
	}
	for(int i = 1;i <= m;++i){
		insert(a[i].u,a[i].v,a[i].w);
		insert(a[i].v,a[i].u,a[i].w);
	}
	val[1] = 1e9;
	dfs1(1,1);
	dfs2(1,1);
	build(1,1,n);
	while(Q--){
		int opt = read(),x = read(),y = read();
		if(opt == 1){
			a[x].w = y;
			if(dep[a[x].u] < dep[a[x].v]) swap(a[x].v,a[x].u);
			val[a[x].u] = y;
			change(1,1,n,dfn[a[x].u]);
		}else{
			getpath(x,y);
		}
	}
	return 0;
}