#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 2005,M = 4005;
int n,m,Q,st,ed;
int head[N],nxt[M*2],to[M*2],w[M*2],cnt;
int dep[N],cur[N];
struct edge{
	int u,v,w;
}a[N];
inline int Min(int x,int y) {if(x > y) return y; return x;}

inline void insert(int x,int y,int z){
	to[++cnt] = y; nxt[cnt] = head[x]; head[x] = cnt; w[cnt] = z;
}
inline bool bfs(){
	queue<int> q; while(!q.empty()) q.pop();
	memset(dep,0,sizeof dep); dep[st] = 1;
	q.push(st);
	do{
		int u = q.front(); q.pop();
		for(int& i = cur[u];i != -1;i = nxt[i])
			if(dep[to[i]] == 0 && (w[i] > 0))
				dep[to[i]] = dep[u]+1,q.push(to[i]);
	}while(!q.empty());
	if(dep[ed] > 0) return true;
	return false;
}
inline int dfs(int x,int flow){
	if(x == ed) return flow;
	for(int i = head[x];i != -1;i = nxt[i])
		if(dep[to[i]] == dep[x]+1 && w[i] != 0){
			int di = dfs(to[i],Min(flow,w[i]));
			if(di > 0){
				w[i] -= di;
				w[i^1] += di;
				return di;
			}
		}
	return 0;
}

inline void dinic(){
	int ans = 0,d;
	while(bfs()){
		for(int i = 1;i <= n;++i) cur[i] = head[i];
		while(d = dfs(st,1e9)) ans += d;
	}
	printf("%d\n", ans);
}

int main(){
	n = read(); m = read();
	for(int i = 1;i <= m;++i){
		a[i].u = read(); a[i].v = read(); a[i].w = read();
	}
	Q = read();
	if(m != n-1 && Q == 1){
		int opt = read(); st = read(); ed = read();
		memset(head,-1,sizeof head); cnt = -1;
		for(int i = 1;i <= m;++i){
			insert(a[i].u,a[i].v,a[i].w);
			insert(a[i].v,a[i].u,0);
			insert(a[i].v,a[i].u,a[i].w);
			insert(a[i].u,a[i].v,0);
		}
		dinic();
	}
	return 0;
}