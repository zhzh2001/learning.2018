#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}
const int N = 1000000;
int n,m,cnt;
int mu[N+5],pri[N+5];
int num[N+5];
bool vis[N+5];

inline void getpri(){
	mu[1] = 1; num[2]++;
	for(int i = 2;i <= N;++i){
		if(!vis[i]){
			pri[++cnt] = i;
			mu[i] = -1; num[0]++;
		}
		for(int j = 1;j <= cnt && pri[j]*i <= N;++j){
			vis[pri[j]*i] = true;
			if(i%pri[j] == 0){
				mu[pri[j]*i] = 0;
				num[1]++;
				break;
			}else{
				if(mu[i] == 0){
					mu[pri[j]*i] = 0;
					num[1]++;
				}else if(mu[i] == 1){
					mu[pri[j]*i] = -1;
					num[0]++;
				}else{
					mu[pri[j]*i] = 1;
					num[2]++;
				}
			}
		}
	}
	// printf("%d %d\n",cnt,num[0]+num[1]+num[2]);
	// for(int i = 1;i <= 50;++i) printf("%d ",pri[i]); puts("");
	// for(int i = 1;i <= 50;++i) printf("%d ",mu[i]); puts("");
}

signed main(){
	freopen("mu.in","r",stdin);
	freopen("mu.out","w",stdout);
	getpri();
	n = read(); m = read();
	for(int i = 1;i <= N;++i)
		if(mu[i] == n){
			m--;
			if(m == 0){
				printf("%d\n", i);
				break;
			}
		}
	return 0;
}
