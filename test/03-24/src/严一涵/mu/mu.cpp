#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstring>
using namespace std;
typedef long long LL;
const LL oo=1000000000ll;
#define dd c=getchar()
inline LL read(){
    LL a=0,b=1;
    char dd;
    while(!isdigit(c)&&c!='-')dd;
    if(c=='-'){b=-b;dd;}
    while(isdigit(c)){a=a*10+c-'0';dd;}
    return a*b;
}
#undef dd
LL u[1000003],n,m,f[1000003],x,q[100003];
#define N 1000000
LL check(LL v){
	LL ans=0,x;
	memset(q,0,sizeof(q));
	for(LL i=2;i*i<=v;i++){
		x=1-q[i];
		if(x==0)continue;
		ans+=v/i/i*x;
		for(LL j=i;j*j<=v;j+=i)q[j]+=x;
	}
	return ans;
}
void solve(){
	LL l=1,r=oo,mid;
	while(l<r){
		mid=(l+r+1)>>1;
		if(check(mid)>m)r=mid-1;else l=mid;
	}
	printf("%lld\n",l);
}
int main(){
	freopen("mu.in","r",stdin);
	freopen("mu.out","w",stdout);
	n=read();
	m=read();
	if(n==0){
		solve();
		return 0;
	}
	u[1]=f[1]=1;
	for(LL i=2;i<=N;i++){
		if(f[i]==0){
			for(LL j=i;j<=N;j+=i){
				f[j]=i;
			}
		}
		x=f[i];
		if(x==f[i/x]){
			u[i]=0;
		}else{
			u[i]=-u[i/x];
		}
	}
	for(LL i=2;i<=N;i++){
		if(u[i]==n)m--;
		if(m==0){
			printf("%lld\n",i);
			break;
		}
	}
	return 0;
}
