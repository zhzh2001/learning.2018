#include <iostream>
#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;
typedef long long LL;
const LL oo=1ll<<60;
#define dd c=getchar()
inline LL read(){
    LL a=0,b=1;
    char dd;
    while(!isdigit(c)&&c!='-')dd;
    if(c=='-'){b=-b;dd;}
    while(isdigit(c)){a=a*10+c-'0';dd;}
    return a*b;
}
#undef dd

struct edge{
    LL a,b,c;
    edge(){
        a=b=c=0;
    }
    edge(LL x,LL y,LL z){
        a=x;
        b=y;
        c=z;
    }
};

namespace myow{
    class ite;
}
class HLP;
class tree{
    vector<edge>e;
    vector<LL>en;
    LL et,n;
public:
    typedef myow::ite iterator;
    friend class myow::ite;
    friend class HLD;
    tree(){
        et=0;
        e.clear();
        en.clear();
        n=0;
    }
    void clr(LL);
    void adde(LL,LL,LL);
    myow::ite operator[](LL);
};
class myow::ite{
    int i;
    tree*e;
public:
    ite(){
        i=0;
        e=NULL;
    }
    ite(int p,tree*v){
        i=p;
        e=v;
    }
    void operator++(){
        i=e->e[i].a;
    }
    void operator++(int p){
        i=e->e[i].a;
    }
    bool end(){
        return i==0;
    }
    LL operator*(){
        return e->e[i].b;
    }
    LL operator~(){
    	return e->e[i].c;
    }
};
void tree::clr(LL N){
    n=N;
    e.clear();
    e.resize(2*n+3);
    en.clear();
    en.resize(n+3);
    et=0;
}
void tree::adde(LL a,LL b,LL c){
    e[++et]=edge(en[a],b,c);
    en[a]=et;
    e[++et]=edge(en[b],a,c);
    en[b]=et;
}
tree::iterator tree::operator[](LL x){
    return tree::iterator(en[x],this);
}

#define ls (nod<<1)
#define rs (nod<<1|1)
class segTree{
    vector<LL>tree;
    vector<LL>lazy;
    LL n;
    void pushup(LL,LL,LL);
    void pushdown(LL,LL,LL);
    void build(LL,LL,LL,LL);
    void build(LL,LL,LL,LL*);
    LL query(LL,LL,LL,LL,LL);
    void ins(LL,LL,LL,LL,LL,LL);
public:
    segTree();
    void init(LL,LL*);
    void init(LL,LL);
    LL query(LL,LL);
    LL query(LL);
    void ins(LL,LL,LL);
    void ins(LL,LL);
};
segTree::segTree(){
    n=0;
    tree.clear();
    lazy.clear();
}
void segTree::pushup(LL nod,LL l,LL r){
    tree[nod]=min(tree[ls],tree[rs]);
}
void segTree::pushdown(LL nod,LL l,LL r){
    LL m=r-l+1;
    LL m1=(m+1)>>1,m2=m>>1;
    LL v=lazy[nod];
    if(v!=0){
        tree[ls]=v;
        tree[rs]=v;
        lazy[ls]=v;
        lazy[rs]=v;
        lazy[nod]=0;
    }
}
void segTree::build(LL nod,LL l,LL r,LL v){
    lazy[nod]=0;
    if(l==r){
        tree[nod]=v;
        return;
    }
    LL mid=(l+r)>>1;
    build(ls,l,mid,v);
    build(rs,mid+1,r,v);
    pushup(nod,l,r);
}
void segTree::build(LL nod,LL l,LL r,LL*v){
    lazy[nod]=0;
    if(l==r){
        tree[nod]=v[l];
        return;
    }
    LL mid=(l+r)>>1;
    build(ls,l,mid,v);
    build(rs,mid+1,r,v);
    pushup(nod,l,r);
}
void segTree::init(LL N,LL*v){
    n=N;
    tree.resize(4*n+3);
    lazy.resize(4*n+3);
    build(1,1,n,v);
}
void segTree::init(LL N,LL v){
    n=N;
    tree.resize(4*n+3);
    lazy.resize(4*n+3);
    build(1,1,n,v);
}
LL segTree::query(LL nod,LL l,LL r,LL x,LL y){
    if(l==x&&r==y){
        return tree[nod];
    }
    pushdown(nod,l,r);
    LL mid=(l+r)>>1,ans=oo;
    if(x<=mid)ans=min(ans,query(ls,l,mid,x,min(y,mid)));
    if(y>mid)ans=min(ans,query(rs,mid+1,r,max(x,mid+1),y));
    return ans;
}
LL segTree::query(LL x,LL y){
    return query(1,1,n,x,y);
}
LL segTree::query(LL x){
    return query(1,1,n,x,x);
}
void segTree::ins(LL nod,LL l,LL r,LL x,LL y,LL v){
    if(l==x&&r==y){
        lazy[nod]=v;
        tree[nod]=v;
        return;
    }
    pushdown(nod,l,r);
    LL mid=(l+r)>>1;
    if(x<=mid)ins(ls,l,mid,x,min(y,mid),v);
    if(y>mid)ins(rs,mid+1,r,max(x,mid+1),y,v);
    pushup(nod,l,r);
}
void segTree::ins(LL x,LL y,LL v){
    ins(1,1,n,x,y,v);
}
void segTree::ins(LL x,LL v){
    LL y=x;
    ins(1,1,n,x,y,v);
}
#undef ls
#undef rs

class HLD{
    tree*e;
    segTree v;
    vector<LL>dep,low,hs,sz,tip,f[23],ed,va;
    LL n,q,root;
    void dfs(LL u,LL fa){
        f[0][u]=fa;
        dep[u]=dep[fa]+1;
        low[u]=u;
        sz[u]=1;
        hs[u]=0;
        LL v;
        for(tree::iterator i=(*e)[u];!i.end();i++){
            v=*i;
            if(v==fa)continue;
            va[v]=~i;
            dfs(v,u);
            if(hs[u]==0||sz[v]>sz[hs[u]]){
                hs[u]=v;
            }
            sz[u]+=sz[v];
        }
    }
    void mrk(LL u,LL fa,bool h){
        if(h){
            low[u]=low[fa];
        }
        tip[u]=++q;
        LL v;
        if(hs[u]>0)mrk(hs[u],u,1);
        for(tree::iterator i=(*e)[u];!i.end();i++){
            v=*i;
            if(v==fa||v==hs[u])continue;
            mrk(v,u,0);
        }
        ed[u]=q;
    }
    void lcainit(){
        for(LL i=1;i<=20;i++){
            for(LL j=1;j<=n;j++){
                f[i][j]=f[i-1][f[i-1][j]];
            }
        }
    }
    void lins(LL a,LL b,LL p){
        v.ins(tip[a],p);
        while(dep[a]>dep[b]){
            if(dep[low[a]]<=dep[b]){
                v.ins(tip[b],tip[f[0][a]],p);
                a=b;
            }else if(low[a]==a){
                a=f[0][a];
                v.ins(tip[a],p);
            }else{
                v.ins(tip[low[a]],tip[f[0][a]],p);
                a=low[a];
            }
        }
    }
    LL lque(LL a,LL b){
    	if(a==b)return oo;
        LL ans=v.query(tip[a]);
        while(dep[a]>dep[b]+1){
            if(dep[low[a]]<=dep[b]){
                ans=min(ans,v.query(tip[b]+1,tip[a]-1));
                break;
            }else if(low[a]==a){
                a=f[0][a];
                ans=min(ans,v.query(tip[a]));
            }else{
                ans=min(ans,v.query(tip[low[a]],tip[a]-1));
                a=low[a];
            }
        }
        return ans;
    }
public:
    LL lca(LL a,LL b){
        if(dep[a]<dep[b])return lca(b,a);
        for(LL i=20;i>=0;i--)if(dep[f[i][a]]>=dep[b])a=f[i][a];
        if(a==b)return a;
        for(LL i=20;i>=0;i--)if(f[i][a]!=f[i][b]){
            a=f[i][a];
            b=f[i][b];
        }
        return f[0][a];
    }
    void init(tree*D,LL rt){
        root=rt;
        e=D;
        n=D->n;
        dep.clear();
        dep.resize(n+3);
        low.clear();
        low.resize(n+3);
        hs.clear();
        hs.resize(n+3);
        sz.clear();
        sz.resize(n+3);
        tip.clear();
        tip.resize(n+3);
        ed.clear();
        ed.resize(n+3);
        va.clear();
        va.resize(n+3);
        for(LL i=0;i<=20;i++){
            f[i].clear();
            f[i].resize(n+3);
        }
        q=0;
        dfs(root,root);
        mrk(root,root,0);
        lcainit();
        LL*fss=new LL[n+3];
        for(LL i=1;i<=n;i++)fss[tip[i]]=va[i];
        v.init(n,fss);
        delete []fss;
    }
    void ins(LL a,LL b,LL p){
        LL c=lca(a,b);
        lins(a,c,p);
        lins(b,c,p);
        v.ins(tip[c],-p);
    }
    void ins(LL a,LL p){
    	//wrong
        v.ins(tip[a],tip[a],p);
    }
    LL query(LL a,LL b){
        LL c=lca(a,b);
        c=min(lque(a,c),lque(b,c));
        return c;
    }
    LL query(LL a){
        return v.query(tip[a],ed[a]);
    }
};

tree t;
HLD hld;
LL n,m,rt,a[100003],x,y,z;
edge rb[100003];
void swap(LL&a,LL&b){
	LL c=a;
	a=b;
	b=c;
}
int main(){
	freopen("flow.in","r",stdin);
	freopen("flow.out","w",stdout);
    n=read();
    read();
    rt=1;
    t.clr(n);
    for(LL i=1;i<n;i++){
        x=read();
        y=read();
        z=read();
        rb[i]=edge(x,y,z);
        t.adde(x,y,z);
    }
    hld.init(&t,rt);
    for(int i=1;i<n;i++){
    	if(hld.lca(rb[i].a,rb[i].b)==rb[i].b){
    		swap(rb[i].a,rb[i].b);
    	}
    }
    m=read();
    for(LL i=1;i<=m;i++){
        x=1-read();
        if(x==0){
            x=read();
            y=read();
            hld.ins(rb[x].b,y);
        }else if(x==1){
            x=read();
            y=read();
            printf("%lld\n",hld.query(x,y));
        }
    }
    return 0;
}

