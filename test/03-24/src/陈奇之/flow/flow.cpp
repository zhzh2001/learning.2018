#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int inf = 0x3f3f3f3f;
#define pc putchar
#define RG register
#define gc getchar
#define mp make_pair
typedef pair<int,int> pii;
//#define DEBUG
#define rep(i,a,b) for(RG int i=(a);i<(b);++i)
#define Rep(i,a,b) for(RG int i=(a);i<=(b);++i)
#define Down(i,a,b) for(RG int i=(a);i>=(b);--i)
#define rd read
inline ll read(){
	RG ll x=0,f=1;RG char c=gc();
	for(;!isdigit(c);c=gc())if(c=='-')f=-1;
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}
void write(ll x){
	if(x<0)x=-x,pc('-');
	if(x>=10)write(x/10);pc(x%10+'0');
}
void writeln(ll x){
	write(x);puts("");
}
#define mem(x,v) memset(x,v,sizeof(x))

const int maxn = 2e5+233;

#define mid ((l+r)>>1)
#define lson ((o<<1))
#define rson ((o<<1|1))
struct SegmentTree{
	int v[maxn*4];
	int query(int o,int l,int r,int x,int y){
//		printf("%d %d %d %d %d\n",o,l,r,x,y);
		if(x>y) return inf;
		if(l==x&&r==y){
			return v[o];
		}
		if(y<=mid) return query(lson,l,mid,x,y); else
		if(mid<x) return query(rson,mid+1,r,x,y); else
		return min(query(lson,l,mid,x,mid),query(rson,mid+1,r,mid+1,y));
	}
	void change(int o,int l,int r,int x,int y){
		if(l==r){
			v[o] = y;
//		printf("v[%d %d] = %d\n",l,r,v[o]);
			return ;
		}
		if(x<=mid) change(lson,l,mid,x,y); else
				   change(rson,mid+1,r,x,y);
		v[o] = min(v[lson],v[rson]);
//		printf("v[%d %d] = %d\n",l,r,v[o]);
	}
	void init(){
		mem(v,0x3f);
	}
}seg1,seg2,seg3;


struct Edge{
	int to,nxt,dist,id;
	Edge(){}
	Edge(int to,int nxt,int dist,int id):
			to(to),nxt(nxt),dist(dist),id(id){}
}edge[maxn*2];
int first[maxn],nume;
void Addedge(int a,int b,int c,int d){
	edge[nume] = Edge(b,first[a],c,d);first[a] = nume++;
}
//先考虑只有一个连通块！！ 
int fa[maxn],size[maxn],deep[maxn],top[maxn];
int in_v[maxn],out_v[maxn],up[maxn],son[maxn];
bool Isup[maxn];
int pos[maxn],ppos[maxn];
int Belong[maxn];
vector<pii> E[maxn];
//bool Instack[maxn];
int n,m,Q;
int s[maxn];
bool vis[maxn];
int pre[maxn];
void dfs(int u,int fae){
//#ifdef DEBUG
//	puts("U");writeln(u);
//#endif
	vis[u] = true;
	s[++*s] = u;//Instack[u] = true;
	for(int e=first[u];~e;e=edge[e].nxt){
		int v = edge[e].to;
		if(e == fae) continue;
		if(!vis[v]){
			dfs(v,e^1);
		} else{
			if(Belong[u]) continue;
			++*Belong;
//			ppp[*Belong] = u;
			while(s[*s] != v){
				Belong[s[*s]] = *Belong;
				pre[s[*s]] = s[*s-1];
//				suf[s[*s-1]] = s[*s];
				--*s;
			}
			Belong[v] = *Belong;
			--*s;
			up[*Belong] = e;
			Isup[e] = *Belong;
			Isup[e^1] = *Belong;
		}
	}
	if(!Belong[u]){
		Belong[u] = ++*Belong;
		up[*Belong] = first[u];
	}
}
void slpf();//树链剖分 
int start[maxn];
int ask(int a,int b){
	if(a==b) return inf;
//	printf("ask(%d %d)\n",a,b);
	int tmp = edge[up[Belong[a]]].dist;
//	writeln(tmp);
	if(pos[a] > pos[b]){
		swap(a,b);
	}
#ifdef DEBUG
	printf("query(%d %d) = %d\n",pos[a],pos[b]-1,seg1.query(1,1,n,pos[a],pos[b]-1));
	printf("query(%d %d) = %d\n",pos[b],start[Belong[a]+1]-1,seg1.query(1,1,n,pos[b],start[Belong[a]+1]-1));
#endif
	return seg1.query(1,1,n,pos[a],pos[b]-1)
		 + min(min(tmp,seg1.query(1,1,n,pos[b],start[Belong[a]+1]-1)),
					   seg1.query(1,1,n,start[Belong[a]],pos[a]-1));
}
int vfa[maxn],ufa[maxn];


int main(){
	freopen("flow.in","r",stdin);
	freopen("flow.out","w",stdout);
	seg1.init(),seg2.init(),seg3.init();
	n = rd(),m = rd();
//	if(m==n-1){
//		spjsolve();
//		return 0;
//	}
	mem(first,-1),nume = 0;
	Rep(i,1,m){
		int a = rd(),b = rd(),c = rd();
		Addedge(a,b,c,i);
		Addedge(b,a,c,i);
	}
	*s = 0;
	dfs(1,-1);
//	Rep(i,1,n){
//		printf("%d ",Belong[i]);
//	}
//#ifdef DEBUG
//	Rep(i,0,n){
//		printf("%d %d\n",i,pre[i]);
//	}puts("");
//	printf("%d %d %d\n",up[1],edge[up[1]].to,edge[up[1]^1].to);
//#endif
	//find circle 
	for(int i=0;i<nume;i+=2){
		int a = edge[i^1] . to;
		int b = edge[i] . to;
		if(Belong[a] != Belong[b]){
			E[Belong[a]] . push_back(mp(Belong[b],edge[i].dist));
			E[Belong[b]] . push_back(mp(Belong[a],edge[i].dist));
	//		printf("%d->%d\n",Belong[a],Belong[b]);
			//如果是外面的边 
			//6->4->5->7
		} else{
			if(Isup[i]) continue;
			if(pre[b] == a) swap(a,b);
			//使得pre[a] = b
			in_v[a] = edge[i] . dist;
		}
	}
#ifdef DEBUG
	Rep(i,0,n){
		printf("%d ",in_v[i]);
	}
#endif
	int tot = 0;
	for(int i=1;i<=*Belong;++i){
		start[i] = tot+1;
		int u = edge[up[i]^1].to;
		while(u){
			pos[u] = ++tot;
			if(pre[u])seg1.change(1,1,n,pos[u],in_v[u]);
#ifdef DEBUG
			printf("~~%d %d %d~~\n",u,pos[u],in_v[u]);
#endif
			u = pre[u];
		}
	}
	slpf();
#ifdef DEBUG
	printf("%d %d\n",vfa[1],ufa[1]);
	printf("%d %d\n",vfa[2],ufa[2]);
#endif

	//vfa表示穿过
	//ufa还没有穿过 
//	Rep(i,1,10){
//		printf("%d %d %d\n",ppos[i],top[i],fa[i]);
//	}
	Q = rd();
	while(Q--){
		int op = rd(),x = rd(),y = rd();
		if(op == 0){
			//query
			int ans = inf;
			int u = Belong[x],v = Belong[y];
			int preu=x,prev=y;
			while(top[u]!=top[v]){
				if(deep[top[u]] > deep[top[v]]){
					ans = min(ans,ask(preu,vfa[u]));
					if(u!=top[u])ans = min(ans,seg3.query(1,1,n,ppos[top[u]],ppos[fa[u]]));
					ans = min(ans,seg2.query(1,1,n,ppos[top[u]],ppos[u]));
					preu = ufa[u],u = fa[top[u]];
				} else{
					ans = min(ans,ask(preu,vfa[v]));
					if(v!=top[v])ans = min(ans,seg3.query(1,1,n,ppos[top[v]],ppos[fa[v]]));
					ans = min(ans,seg2.query(1,1,n,ppos[top[v]],ppos[v]));
					prev = ufa[v],v = fa[top[v]];
				}
			}
			if(u==v){
				ans = min(ans,ask(preu,prev));
			} else{
				if(deep[u] > deep[v]){
					swap(u,v);
					swap(preu,prev);
				}
				ans = min(ans,ask(prev,vfa[v]));
				//writeln(ans);
				ans = min(ans,seg2.query(1,1,n,ppos[u],ppos[Belong[ufa[v]]]));
//			printf("seg2.query(%d %d) = %d\n",ppos[u],ppos[Belong[ufa[v]]],seg2.query(1,1,n,ppos[u],ppos[Belong[ufa[v]]]));
//				writeln(seg2.query(1,1,n,ppos[u],ppos[Belong[ufa[v]]]));
//				writeln(ans);
				ans = min(ans,seg3.query(1,1,n,ppos[son[u]],ppos[Belong[ufa[v]]]));
//				writeln(seg3.query(1,1,n,ppos[son[u]],ppos[Belong[ufa[v]]]));
				//writeln(ans);
//				puts("Asking");writeln(ask(ufa[son[u]],preu));
//printf("%d->%d\n",ufa[son[u]],preu);
				ans = min(ans,ask(ufa[son[u]],preu));
			}
			writeln(ans);
//			if(deep[u] > deep[v]) swap(u,v),swap(preu,prev);
//			if(ppos[u]+1 <= ppos[v]) ans = min(ans,seg2.query(ppos[u]+1,ppos[v]));
//			u = Belong[x],v = Belong[y];//维护每个点从重儿子到父亲的权值。 
//			preu = x,prev = y;
			
		} else{
			//第x条边修改成y 
			edge[x*2].dist = y;
			edge[x*2-1].dist = y;
			int u = edge[x*2] . to,v = edge[x*2-1] . to;
			if(Isup[x*2]){
				continue;
//				up[Isup[x*2]] = y;
			} else
			if(Belong[u] == Belong[v]){
				if(pre[v] == u) swap(u,v);
				//pre[u] = v 
				seg1.change(1,1,n,pos[u],y);
				if(son[u] && vfa[u] != ufa[son[u]])
					seg3.change(1,1,n,ppos[u],ask(vfa[u],ufa[son[u]]));
			} else{
				u = Belong[u],v = Belong[v];
				if(deep[u] > deep[v]) swap(u,v);
//				printf("%d --- %d\n",u,ppos[u]);
				seg2.change(1,1,n,ppos[v],y);
			}
		}
	}

}
#define dfs ___dfs
void dfs(int u){
	size[u] = 1;
	for(int i=0;i<E[u].size();i++){
		int v = E[u][i].first;
		if(v == fa[u]) continue;
		deep[v] = deep[u] + 1;
		fa[v] = u;
//		printf("%d->%d\n",u,v);
		out_v[v]  = E[u][i].second;
		dfs(v);
		size[u] += size[v];
		if(size[v] > size[son[u]]) son[u] = v;
	}
}
int tot = 0;
void divide(int u,int chain){
	ppos[u] = ++tot;
	top[u] = chain;
	if(son[u]) divide(son[u],chain);
	for(int i=0;i<E[u].size();++i){
		int v = E[u][i].first;
		if(v == fa[u] || v==son[u]) continue;
		divide(v,v);
	}
	
	if(out_v[son[u]]){
		seg2.change(1,1,n,ppos[u],out_v[son[u]]);
//		printf("%d --> %d\n",ppos[u],out_v[son[u]]);
	}
	int vson;
	if(fa[u]){
		int w = edge[up[u]^1].to;
//		printf("%d:~W = %d\n",u,w);
		int vpa=0,vson=0;
		while(w){
			for(int e=first[w];~e;e=edge[e].nxt){
				if(Belong[edge[e].to] == fa[u]) vfa[u] = w,ufa[u] = edge[e].to;
//				if(Belong[edge[e].to] == son[u]) vson = w;
			}
//			printf("W = %d\n",w);
			w = pre[w];
		}
		//vson = ufa[son[u]]
		if(son[u] && vfa[u] != ufa[son[u]])
			seg3.change(1,1,n,ppos[u],ask(vfa[u],ufa[son[u]]));
	}
}
void slpf(){
	//先考虑一个连通块 
	dfs(1);
//	Rep(i,1,*Belong){
//		printf("%d %d %d %d\n",i,size[i],son[i],out_v[i]);
//	}
	divide(1,1);
}
#undef dfs
//segment tree
//if(x>y) return inf;
