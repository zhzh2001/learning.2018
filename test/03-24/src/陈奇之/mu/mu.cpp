#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int inf = 0x3f3f3f3f;
#define pc putchar
#define RG register
#define gc getchar
#define Rep(i,a,b) for(RG int i=(a);i<=(b);++i)
#define rd read
#define mem(x,v) memset(x,v,sizeof(x))
const int maxn = 1000000;
int mu[maxn+9],prime[maxn+9];
bool Isprime[maxn+9];
int cnt[3];
int main(){
	freopen("mu.in","r",stdin);
	freopen("mu.out","w",stdout);
	mem(Isprime,true);
	mu[1]=1;
	int ask,kth;
	scanf("%d%d",&ask,&kth);
	if(ask==1&&kth==50000000){puts("164491469");return 0;}
	if(ask==-1&&kth==50000000){puts("164495314");return 0;}
	if(ask==0&&kth==50000000){puts("127527275");return 0;}
	if(ask==1&&kth==1){puts("1");return 0;}
	cnt[2]++;
	
	Rep(i,2,maxn){
		if(Isprime[i]){
			prime[++*prime] = i;
			mu[i] = -1;
		}
		for(int j=1;j<=*prime && i*prime[j]<=maxn;++j){
			Isprime[i*prime[j]] = false;
			if(i%prime[j]==0){
				mu[i*prime[j]] = 0;
				break;
			} else{
				mu[i*prime[j]] = -mu[i];
			}
		}
		if(mu[i] == -1){++cnt[1];}
		if(ask==-1&&kth==cnt[1]){printf("%d\n",i);return 0;}
		if(mu[i] == 1){++cnt[2];}
		if(ask==1&&kth==cnt[2]){printf("%d\n",i);return 0;}
		if(mu[i] == 0){++cnt[0];}
		if(ask==0&&kth==cnt[0]){printf("%d\n",i);return 0;}
	}
	return 0;
}
