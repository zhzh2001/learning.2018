#include<bits/stdc++.h>
#define ll long long
#define N 200005
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
using namespace std;
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
int n,m,f[N],q,op,x,y;
int Next[N*2],head[N],to[N*2],nedge,lon[N*2];
#define V to[i]
void add(int a,int b,int c){Next[++nedge]=head[a];head[a]=nedge;to[nedge]=b;lon[nedge]=c;}
struct xx{
	int x,y,v;
}z[N];
void dfs(int x){
	for (int i=head[x];i;i=Next[i]){
		if (!f[V]){
			f[V]=min(f[x],lon[i]);dfs(V);
		}
	}
}
signed main(){
	freopen("flow.in","r",stdin);freopen("std.out","w",stdout);
	n=read();m=read();
	for (int i=1;i<=m;i++){
		z[i].x=read();z[i].y=read();z[i].v=read();
		add(z[i].x,z[i].y,z[i].v);add(z[i].y,z[i].x,z[i].v);
	} 
	q=read();
	for (int i=1;i<=q;i++){
		op=read();
		if (op==0){
			x=read();y=read();
			memset(f,0,sizeof(f));f[x]=inf;
			dfs(x);
			wrn(f[y]);
		}
		else{
			x=read();y=read();z[x].v=y;
			nedge=0;memset(head,0,sizeof(head));
			for (int i=1;i<=m;i++){
				add(z[i].x,z[i].y,z[i].v);add(z[i].y,z[i].x,z[i].v);
			} 
		}
	}
	return 0;
}
