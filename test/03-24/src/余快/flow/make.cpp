#include<bits/stdc++.h>
#define ll long long
#define N 200005
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
using namespace std;
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
int n,m,x,y,fa[N],q,op;
int gf(int x){return fa[x]=(fa[x]==x)?x:gf(fa[x]);}
signed main(){
	freopen("flow.in","w",stdout);
	srand(time(NULL));
	n=rand()%40+40;m=n-1;
	wri(n);wrn(m);
	for (int i=1;i<=n;i++) fa[i]=i;
	for (int i=1;i<=m;i++){
		x=rand()%n+1,y=rand()%n+1;
		while (gf(x)==gf(y)) x=rand()%n+1,y=rand()%n+1;
		fa[gf(x)]=gf(y);
		wri(x);wri(y);wrn(rand()%100+1);
	}
	q=rand()%2+2;wrn(q); 
	for (int i=1;i<=q;i++){
		op=rand()%2;wri(op);
		if (op==0){
			x=rand()%n+1;y=rand()%n+1;
			while (x==y) x=rand()%n+1,y=rand()%n+1;
			wri(x);wrn(y);
		}
		else{
			wri(rand()%m+1);wrn(rand()%100+1);
		}
	}
	return 0;
}

