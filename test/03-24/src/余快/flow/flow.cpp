#include<bits/stdc++.h>
#define ll long long
#define N 200005
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
using namespace std;
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
int t2,tot,op,q,t1,d,k,ans,s,n,m,dis[N],cur[N],dui[N],x,y,g2[N],b[N],a[N];
int nedge,Next[N*2],head[N],to[N*2],lon[N*2];
void add(int a,int b,int c){nedge++;Next[nedge]=head[a];head[a]=nedge;to[nedge]=b;lon[nedge]=c;}
void add2(int a,int b,int c){add(a,b,c);add(b,a,c);}
int g1(int x){return (x+1)/2;}
struct xx{
	int x,y,v;
}z[N];
struct xx1{
	int s,fa,top,sum,dis,num;
}tree[N];
struct tree1{
	int l,r,num,min;
}t[N*4];
void build(int x,int l,int r){
	t[x].l=l;t[x].r=r;
	if (l==r) {
		t[x].min=b[l];return;
	}
	int mid=(l+r)>>1;
	build(x*2,l,mid);build(x*2+1,mid+1,r);
	t[x].min=min(t[x*2].min,t[x*2+1].min);
}
void dfs(int x){
	tree[x].sum=1;
	for (int i=head[x];i;i=Next[i]){
		if (to[i]==tree[x].fa) continue;
		tree[to[i]].fa=x;tree[to[i]].dis=tree[x].dis+1;
		dfs(to[i]);
		tree[x].sum+=tree[to[i]].sum;
		if (tree[to[i]].sum>tree[tree[x].s].sum) tree[x].s=to[i],g2[x]=i;
	}
}
void dfs2(int x,int v){
	tot++;tree[x].num=tot;b[tot]=lon[v];a[g1(v)]=tot;
	if (tree[x].s>0){
	tree[tree[x].s].top=tree[x].top;
	dfs2(tree[x].s,g2[x]);
  }
  for (int i=head[x];i;i=Next[i]){
  	if (to[i]==tree[x].fa||to[i]==tree[x].s) continue;
  	tree[to[i]].top=to[i];
  	dfs2(to[i],i);
	}
}
void change(int x,int k,int p){
	if (t[x].l==t[x].r){
		t[x].min=p;return;
	}
	int mid=(t[x].l+t[x].r)>>1;
	if (k<=mid) change(x*2,k,p);
	else change(x*2+1,k,p);
	t[x].min=min(t[x*2].min,t[x*2+1].min);
}
int find(int x,int l,int r){
	if (l>r) return inf;
	if (t[x].l==l&&t[x].r==r) return t[x].min;
	int mid=(t[x].l+t[x].r)>>1;
	if (r<=mid) return find(x*2,l,r);
	else if (l>mid) return find(x*2+1,l,r);
	else return min(find(x*2,l,mid),find(x*2+1,mid+1,r));
}
int _find(int x,int y){
	int sum=inf;
	while (tree[x].top!=tree[y].top){
	if (tree[tree[x].top].dis<tree[tree[y].top].dis) swap(x,y);
	    sum=min(sum,find(1,tree[tree[x].top].num,tree[x].num));
	    x=tree[tree[x].top].fa;
    }
    if (tree[x].dis>tree[y].dis) swap(x,y);
    sum=min(sum,find(1,tree[x].num+1,tree[y].num));
    return sum;
}
void solve2(){
	//就当是一棵树
	for (int i=1;i<=m;i++) add2(z[i].x,z[i].y,z[i].v);
	dfs(1);tree[1].fa=1;tree[1].top=1;
	dfs2(1,0);
	build(1,1,n);
	for (int i=1;i<=q;i++){
		op=read();x=read();y=read();
		if (op==0){
			wrn(_find(x,y));	
		}
		else{
			change(1,a[x],y);
		}
	}
	return;
}
inline int g(int x){return x+(x&1?1:-1);}
bool bfs(){
	memset(dis,0,sizeof(dis));
	dui[1]=s;dis[s]=1;
	for (int l=1,r=1;l<=r;l++){
		t1=dui[l];
		for (int i=head[t1];i;i=Next[i]){
			if (!dis[to[i]]&&lon[i]){
				dis[to[i]]=dis[t1]+1;dui[++r]=to[i];
			}
		}
	}
	return dis[t2];
}
int dfs(int x,int s){
	if (x==t2) return s;
	for (int &i=cur[x];i;i=Next[i]){
		if (lon[i]&&dis[to[i]]==dis[x]+1){
			int t1=dfs(to[i],min(s,lon[i]));
			if (t1){
				lon[g(i)]+=t1;lon[i]-=t1;
				return t1;
			}
		}
	}
	return 0;
}
void solve1(){
	op=read();s=read();t2=read(); 
	for (int i=1;i<=m;i++) add2(z[i].x,z[i].y,z[i].v);
	while (bfs()){
		for (int i=1;i<=n;i++) cur[i]=head[i];//这边的上下界注意修改
		d=dfs(s,inf);
		while (d){
			ans+=d;d=dfs(s,inf);
		}
	}
	wrn(ans);
	return;
}
signed main(){
	freopen("flow.in","r",stdin);
	freopen("flow.out","w",stdout);
	n=read();m=read();
	//0位查询，1为修改 
	for (int i=1;i<=m;i++){
		z[i].x=read();z[i].y=read();z[i].v=read();
	}
	q=read();
	if (q==1){solve1();return 0;}
	if (m==n-1){solve2();return 0;}//希望不要闲着蛋疼弄些奇怪的图，给我弄出颗树来..
	//不弄出树不跟最后一档一样了吗。。 
	return 0;
}
