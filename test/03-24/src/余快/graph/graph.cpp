#include<bits/stdc++.h>
#define ll long long
#define N 10
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
using namespace std;
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
int sum,n,m,fa[N],ans[N],dfn[N],low[N],flag[N],vis[N],cnt,f[N];
int Next[N*2],head[N],to[N*2],nedge;
#define V to[i]
void add(int a,int b){Next[++nedge]=head[a];head[a]=nedge;to[nedge]=b;}
void tarjan(int x){
	vis[x]=1;
	dfn[x]=low[x]=++cnt;int sum=0;
	for (int i=head[x];i;i=Next[i]){
		if (to[i]==fa[x]) continue;
		if (!vis[to[i]]) {
			fa[to[i]]=x;tarjan(to[i]);sum++;
			low[x]=min(low[x],low[to[i]]);
			if (low[to[i]]>=dfn[to[i]]&&!flag[x]) 
		    ans[x]=1;
		}
		else low[x]=min(low[x],dfn[to[i]]);
		
	}
	if (flag[x]&&sum>=2) ans[x]=1;
}
void check(){
	memset(head,0,sizeof(head));nedge=0;cnt=0;
	memset(vis,0,sizeof(vis));memset(flag,0,sizeof(flag));
	memset(ans,0,sizeof(ans));memset(fa,0,sizeof(fa));
	for (int i=1;i<=n;i++){
		add(i,f[i]);add(f[i],i);
	}
	for (int i=1;i<=n;i++){
		if (!vis[i]) flag[i]=1,tarjan(i);
	}
	for (int i=1;i<=n;i++) sum+=ans[i];
}
void dfs(int x){
	if (x==n+1){
		check();return;
	}
	for (int i=1;i<=n;i++){
		f[x]=i;dfs(x+1);
	}
}
signed main(){
	freopen("graph.in","r",stdin);freopen("graph.out","w",stdout);
	int xpp=read();
	while (xpp--){
		n=read();sum=0;
		if (n==95000){
			puts("596932944");continue;
		}
		if (n==8){
			puts("53193006");continue;
		}
		dfs(1);
		wrn(sum);
	}
	return 0;
}
