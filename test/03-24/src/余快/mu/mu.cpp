#include<bits/stdc++.h>
#define ll long long
#define N 2000005
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
#include<ext/pb_ds/hash_policy.hpp>
#include<ext/pb_ds/assoc_container.hpp>
using namespace std;
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
int cnt,t,mu[N],vis[N],n,m,f1[4],tot,z[N],msum[N],f[200005],g[200005];
__gnu_pbds::gp_hash_table<int,int> mu1;
inline ll solve(int x){
	if (x<=2e6) return msum[x];
	if(mu1.find(x)!=mu1.end()) return mu1[x];  	
    register long long last,re=1;  
    for(register int i=1;i<=x;i=last+1)  {  
        last=x/(x/i);  
        if(x/i-1)re-=(ll)(solve(last)-solve(i-1))*(x/i-1);  
    }  
    mu1[x]=re;
    return re;   
}
int check(int x){
	int num=x;
	for (int i=1;i<=cnt&&f[i]<=x;i++) 
	num+=(x/f[i])*g[i];
	if (t==0) return x-num;
	int p=solve(x);
	if (t==1) return (num+p)/2;
	else return (num-p)/2; 
}
signed main(){
	freopen("mu.in","r",stdin);freopen("mu.out","w",stdout);
	t=read();n=read();
	mu[1]=1;
	for (int i=2;i<=2e6;i++){
		if (!vis[i]){
			mu[i]=-1;
			z[++tot]=i;
		}
		for (int j=1;j<=tot&&z[j]<=2e6/i;j++){
			vis[z[j]*i]=1;
			if (i%z[j]==0) {mu[i*z[j]]=0;break;}
			else mu[i*z[j]]=-mu[i];
		}
	}
	for (int i=1;i<=2e6;i++){
		msum[i]=msum[i-1]+mu[i];
	}
	for (int i=2;i<=1e9/i;i++){
		if (mu[i]){
		f[++cnt]=i*i;
		g[cnt]=mu[i];
		}
	}
	int mid,l=1,r=1e9;
	while (l<r){
		mid=(l+r)>>1;
		if (check(mid)<n) l=mid+1;
		else r=mid; 
	}
	wrn(l);
	return 0;
}
