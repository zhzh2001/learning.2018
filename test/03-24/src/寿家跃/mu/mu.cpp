#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<cmath>

#define pi acos(-1)

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}

typedef long long ll;

const int N=1000005;

int ask,kth;
int mu[N],Pri[N],B[N];
int main(){
	freopen("mu.in","r",stdin);
	freopen("mu.out","w",stdout);
	ask=read(),kth=read();
	mu[1]=1;
	Rep(i,2,(int)1e6){
		if (!B[i]) mu[Pri[++*Pri]=i]=-1;
		for (int j=1,k;j<=*Pri && (k=i*Pri[j])<=(int)1e6;j++){
			B[k]=true;
			if (i%Pri[j]==0){
				mu[k]=0;break;
			}
			mu[k]=mu[i]*-1;
		}
	}
//	Rep(i,2,20) if (!B[i]) printf("%d\n",i);
	int cnt=0;
	Rep(i,1,(int)1e6){
		if (mu[i]==ask) cnt++;
		if (cnt==kth) return printf("%d\n",i),0;
	}
}
