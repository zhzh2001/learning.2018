#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<cmath>

#define pi acos(-1)

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}

typedef long long ll;

const int N=100005;
const int M=N*2;
const int Ln=20;
const int INF=1e9+7;

int n,m;

int lines,front[N];
struct Edge{
	int next,to,wei,p;
}E[M*2];
inline void Addline(int u,int v,int w,int p){
	E[++lines]=(Edge){front[u],v,w,p};front[u]=lines;
}

struct Segment_Tree{
	#define mid (L+R>>1)
	int Val[N*4];
	void Modify(int L,int R,int p,int pos,int val){
		if (L==R){
			Val[p]=val;return;
		}
		if (pos<=mid) Modify(L,mid,p<<1,pos,val);
			else Modify(mid+1,R,p<<1|1,pos,val);
		Val[p]=min(Val[p<<1],Val[p<<1|1]);
	}
	int Query(int L,int R,int p,int x,int y){
		if (L==x && R==y) return Val[p];
		if (y<=mid) return Query(L,mid,p<<1,x,y);else
		if (x>mid) return Query(mid+1,R,p<<1|1,x,y);else{
			return min(Query(L,mid,p<<1,x,mid),Query(mid+1,R,p<<1|1,mid+1,y));
		}
	}
	#undef mid
}T;
int fa[N],deep[N],size[N],heavy[N];
int dep[N],top[N],Edgfa[N],Edgpos[N];

void Dfs(int u,int d,int f){
	fa[u]=f;
	deep[u]=d;
	size[u]=1;
	for (int i=front[u],v;i!=0;i=E[i].next){
		if ((v=E[i].to)!=f){
			Edgfa[v]=E[i].wei;
			Edgpos[E[i].p]=v;
			Dfs(v,d+1,u);
			size[u]+=size[v];
			if (size[v]>size[heavy[u]]) heavy[u]=v;
		}
	}
}

int Pos[N],cnt;
void Par(int u,int d,int t){
	dep[u]=d;
	top[u]=t;
	Pos[u]=++cnt;
	int son=heavy[u];
	if (son) Par(son,d,t);
	for (int i=front[u],v;i!=0;i=E[i].next){
		if ((v=E[i].to)!=fa[u] && v!=son){
			Par(v,d+1,v);
		}
	}
}
int Lca(int x,int y){
	while (top[x]!=top[y]){
		if (dep[x]<dep[y]) swap(x,y);
		x=fa[top[x]];
	}
	return deep[x]<deep[y]?x:y;
}
int Link_Query(int p,int f){
	int res=INF;
	while (top[p]!=top[f]){
		res=min(res,T.Query(1,n,1,Pos[top[p]],Pos[p]));p=fa[top[p]];
	}
	if (f!=p) res=min(res,T.Query(1,n,1,Pos[f]+1,Pos[p]));
	return res;
}
int Query(int x,int y){
	int lca=Lca(x,y);
//	printf("lca=%d val1=%d val2=%d\n",lca,Link_Query(x,lca),Link_Query(y,lca));
	return min(Link_Query(x,lca),Link_Query(y,lca));
}
int main(){
	freopen("flow.in","r",stdin);
	freopen("flow.out","w",stdout);
	n=read(),m=read();
	lines=1;
	memset(front,0,sizeof(front));
	static int u[M],v[M],w[M];
	Rep(i,1,m){
		u[i]=read(),v[i]=read(),w[i]=read();
		Addline(u[i],v[i],w[i],i),Addline(v[i],u[i],w[i],i);
	}
	Dfs(1,0,0);
	Par(1,0,1);
	Rep(i,1,n) T.Modify(1,n,1,Pos[i],Edgfa[i]);
	int q=read();
	Rep(i,1,q){
		int opt=read(),x=read(),y=read();
		if (opt==0) printf("%d\n",Query(x,y));
		if (opt==1) T.Modify(1,n,1,Pos[Edgpos[x]],y);
	}
}
/*
6 5
1 2 1
1 3 1
2 4 2
2 5 3
3 6 4

10
0 4 6
1 1 5
0 4 6
1 2 5
0 4 6
1 3 5
0 4 6
*/
