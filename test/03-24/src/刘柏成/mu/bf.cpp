#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 1
void init(){
	freopen("mu.in","r",stdin);
	freopen("bf.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
const int maxn=1000002;
int primes[maxn],cur;
bool ntprime[maxn];
int mu[maxn];
void sieve(int n){
	ntprime[1]=1;
	mu[1]=1;
	for(int i=2;i<=n;i++){
		if (!ntprime[i]){
			primes[++cur]=i;
			mu[i]=-1;
		}
		for(int j=1;j<=cur;j++){
			int k=i*primes[j];
			if (k>n)
				break;
			ntprime[k]=1;
			if (i%primes[j]==0){
				mu[k]=0;
				break;
			}
			mu[k]=-mu[i];
		}
	}
}
int check(int n){ //sigma i|x u(i)=[x=1]
	ll ans=0;
	for(int i=1;i*i<=n;i++)
		ans+=mu[i]*n/i/i;
	return n-ans;
}
int main(){
	init();
	int x=readint(),k=readint();
	sieve(1000000);
	int tot=0;
	for(int i=1;i<=1000000;i++)
		if (mu[i]==x){
			tot++;
			if (tot==k)
				return printf("%d",i),0;
		}
	// printf("%d\n",tot);
}