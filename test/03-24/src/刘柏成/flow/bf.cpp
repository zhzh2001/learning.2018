#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=30000020;
const int INF=0x3f3f3f3f;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("flow.in","r",stdin);
	freopen("bf.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
const int maxn=2002;
const int maxm=maxn*4;
struct graph{
	int n,m;
	graph(){
		m=1;
	}
	struct edge{
		int to,orz,cap,next;
	}e[maxm];
	int first[maxn];
	void addedge(int from,int to,int cap){
		e[++m]=(edge){to,cap,cap,first[from]};
		first[from]=m;
		e[++m]=(edge){from,cap,cap,first[to]};
		first[to]=m;
	}
	int t;
	int d[maxn],q[maxn];
	bool bfs(int s){
		int l=1,r=1;
		q[1]=s;
		memset(d,0x3f,(n+1)*4);
		d[s]=0;
		while(l<=r){
			int u=q[l++];
			for(int i=first[u];i;i=e[i].next){
				int v=e[i].to;
				if (!e[i].cap || d[v]!=INF)
					continue;
				d[v]=d[u]+1;
				if (v==t)
					return true;
				q[++r]=v;
			}
		}
		return d[t]!=INF;
	}
	int dfs(int u,int flow){
		if (u==t)
			return flow;
		for(int& i=cur[u];i;i=e[i].next){
			int v=e[i].to;
			if (!e[i].cap || d[v]!=d[u]+1)
				continue;
			int res=dfs(v,min(e[i].cap,flow));
			if (res){
				e[i].cap-=res;
				e[i^1].cap+=res;
				return res;
			}
		}
		return 0;
	}
	int cur[maxn];
	ll dinic(int s,int t){
		this->t=t;
		for(int i=2;i<=m;i++)
			e[i].cap=e[i].orz;
		ll ans=0;
		while(bfs(s)){
			for(int i=1;i<=n;i++)
				cur[i]=first[i];
			int flow=0;
			while(flow=dfs(s,INF))
				ans+=flow;
		}
		return ans;
	}
}g;
int main(){
	init();
	int n=readint(),m=readint();
	g.n=n;
	while(m--){
		int u=readint(),v=readint(),w=readint();
		g.addedge(u,v,w);
	}
	int q=readint();
	while(q--){
		int op=readint(),x=readint(),y=readint();
		if (!op)
			printf("%lld\n",g.dinic(x,y));
		else g.e[x*2].orz=g.e[x*2+1].orz=y;
	}
}