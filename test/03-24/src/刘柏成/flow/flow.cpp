#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=20000020;
const int INF=0x7f7f7f7f;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("flow.in","r",stdin);
	freopen("flow.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
const int maxn=100002;
struct segtree{
	int n;
	int t[maxn*4];
	void init(int n){
		this->n=n;
		memset(t,0x7f,sizeof(t));
	}
	int p,v;
	void update(int o,int l,int r){
		if (l==r){
			t[o]=v;
			return;
		}
		int mid=(l+r)/2;
		if (p<=mid)
			update(o*2,l,mid);
		else update(o*2+1,mid+1,r);
		t[o]=min(t[o*2],t[o*2+1]);
	}
	void update(int p,int v){
		this->p=p,this->v=v;
		update(1,1,n);
	}
	int ql,qr;
	int query(int o,int l,int r){
		if (ql<=l && qr>=r)
			return t[o];
		int mid=(l+r)/2,ans=INF;
		if (ql<=mid)
			ans=min(ans,query(o*2,l,mid));
		if (qr>mid)
			ans=min(ans,query(o*2+1,mid+1,r));
		return ans;
	}
	int query(int l,int r){
		if (l>r)
			return INF;
		ql=l,qr=r;
		return query(1,1,n);
	}
}t,t2;
const int maxm=maxn*4;
namespace bf
{
	struct graph{
		int n,m;
		graph(){
			m=1;
		}
		struct edge{
			int to,orz,cap,next;
		}e[maxm];
		int first[maxn];
		void addedge(int from,int to,int cap){
			e[++m]=(edge){to,cap,cap,first[from]};
			first[from]=m;
			e[++m]=(edge){from,cap,cap,first[to]};
			first[to]=m;
		}
		int t;
		int d[maxn],q[maxn];
		bool bfs(int s){
			int l=1,r=1;
			q[1]=s;
			memset(d,0x7f,(n+1)*4);
			d[s]=0;
			while(l<=r){
				int u=q[l++];
				for(int i=first[u];i;i=e[i].next){
					int v=e[i].to;
					if (!e[i].cap || d[v]!=INF)
						continue;
					d[v]=d[u]+1;
					if (v==t)
						return true;
					q[++r]=v;
				}
			}
			return d[t]!=INF;
		}
		int dfs(int u,int flow){
			if (u==t)
				return flow;
			for(int& i=cur[u];i;i=e[i].next){
				int v=e[i].to;
				if (!e[i].cap || d[v]!=d[u]+1)
					continue;
				int res=dfs(v,min(e[i].cap,flow));
				if (res){
					e[i].cap-=res;
					e[i^1].cap+=res;
					return res;
				}
			}
			return 0;
		}
		int cur[maxn];
		ll dinic(int s,int t){
			this->t=t;
			for(int i=2;i<=m;i++)
				e[i].cap=e[i].orz;
			ll ans=0;
			while(bfs(s)){
				for(int i=1;i<=n;i++)
					cur[i]=first[i];
				int flow=0;
				while(flow=dfs(s,INF))
					ans+=flow;
			}
			return ans;
		}
	}g;
	void work(int n,int m){
		g.n=n;
		while(m--){
			int u=readint(),v=readint(),w=readint();
			g.addedge(u,v,w);
		}
		int q=readint();
		// fprintf(stderr,"WTF\n");
		while(q--){
			int op=readint(),x=readint(),y=readint();
			if (!op)
				printf("%lld\n",g.dinic(x,y));
			else g.e[x*2].orz=g.e[x*2+1].orz=y;
		}
	}
}
struct graph{
	int n,m;
	graph(){
		m=1;
	}
	struct edge{
		int from,to,dist,next;
	}e[maxm];
	int first[maxn];
	void addedge(int from,int to,int dist){
		e[++m]=(edge){from,to,dist,first[from]};
		first[from]=m;
	}
};
struct forest : graph{
	int pl[maxn],sz[maxn],top[maxn],son[maxn],dep[maxn],fa[maxn],id[maxn],cl,now;
	int val[maxn];
	void dfs(int u){
		sz[u]=1;
		pl[u]=now;
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (sz[v])
				continue;
			fa[v]=u;
			dep[v]=dep[u]+1;
			val[v]=e[i].dist;
			dfs(v);
			sz[u]+=sz[v];
			if (sz[v]>sz[son[u]])
				son[u]=v;
		}
	}
	void dfs2(int u,int tp){
		top[u]=tp;
		id[u]=++cl;
		if (son[u])
			dfs2(son[u],tp);
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (!id[v])
				dfs2(v,v);
		}
	}
	void prepare(){
		for(int i=1;i<=n;i++)
			if (!pl[i]){
				now++;
				dfs(i);
				dfs2(i,i);
			}
		t.init(n);
		for(int i=1;i<=n;i++)
			t.update(id[i],val[i]);
	}
	void update(int u,int v){
		t.update(id[u],v);
	}
	int query(int u,int v){
		int ans=INF;
		for(;top[u]!=top[v];u=fa[top[u]]){
			if (dep[top[u]]<dep[top[v]])
				swap(u,v);
			ans=min(ans,t.query(id[top[u]],id[u]));
		}
		if (dep[u]>dep[v])
			swap(u,v);
		return min(ans,t.query(id[u]+1,id[v]));
	}
}f;
struct loop_graph : graph{
	int id[maxm];
	bool vis[maxn];
	int fa[maxn],rt[maxn];
	int cur,qwq;
	bool flag;
	void dfs(int u){
		vis[u]=1;
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (!vis[v]){
				fa[v]=i;
				dfs(v);
			}else if ((i^1)!=fa[u] && !flag){
				int j=u;
				// fprintf(stderr,"%d %d\n",u,v);
				while(j!=v){
					id[fa[j]]=id[fa[j]^1]=++cur;
					// fprintf(stderr,"%d\n",j);
					j=e[fa[j]].from;
					// fprintf(stderr,"%d\n",j);
				}
				fa[v]=i;
				id[i]=id[i^1]=++cur;
				flag=true;
			}
		}
	}
	void prepare(){
		dfs(1);
		for(int i=2;i<=m;i++)
			if (!id[i])
				f.addedge(e[i].from,e[i].to,e[i].dist);
		f.prepare();
		// fprintf(stderr,"WTF\n");
		for(int i=1;i<=n;i++)
			if (id[fa[i]] || i==qwq)
				rt[f.pl[i]]=i;
		t2.init(cur);
		for(int i=2;i<=m;i+=2)
			if (id[i])
				t2.update(id[i],e[i].dist);
	}
	void update(int x,int v){
		if (id[x])
			t2.update(id[x],v);
		else{
			int a=e[x].from,b=e[x].to;
			if (f.fa[b]==a)
				swap(a,b);
			f.update(a,v);
		}
	}
	int query(int u,int v){
		if (f.pl[u]==f.pl[v])
			return f.query(u,v);
		int s=rt[f.pl[u]],t=rt[f.pl[v]];
		int ans=min(f.query(u,s),f.query(v,t));
		if (id[fa[s]]>id[fa[t]])
			swap(s,t);
		int l=id[fa[s]],r=id[fa[t]]-1;
		int qwq=min(t2.query(1,l-1),t2.query(r+1,cur)),tat=t2.query(l,r);
		if (qwq==INF || tat==INF)
			return ans;
		return min(ans,qwq+tat);
	}
}g;
int main(){
	init();
	int n=readint(),m=readint(),flg=m==n-1;
	// fprintf(stderr,"%d",sizeof(bf::g)+sizeof(g)+sizeof(f)+sizeof(t)+sizeof(t2));
	if (m>n)
		return bf::work(n,m),0;
	g.n=f.n=n;
	while(m--){
		int u=readint(),v=readint(),w=readint();
		g.addedge(u,v,w);
		g.addedge(v,u,w);
	}
	if (flg)
		g.addedge(1,n,0),g.addedge(n,1,0);
	// fprintf(stderr,"WTF\n");
	g.prepare();
	// fprintf(stderr,"WTF\n");
	int q=readint();
	while(q--){
		int op=readint(),x=readint(),y=readint();
		if (!op)
			printf("%d\n",g.query(x,y));
		else g.update(x*2,y);
	}
}