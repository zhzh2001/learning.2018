#include<stdio.h>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=200100,P=1e9+7;
inline int read(){
	int x=0,c=getchar(),f=0;
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
int pr[100000000],sm;
char mu[1000000001];
int main(){
	freopen("mu.in","r",stdin);
	freopen("mu1.out","w",stdout);
	//t=read();m=read();
	register int i,j,k,n,s0,s1,ss;
	n=1000000000;s0=s1=ss=0;s1=1;
	for(i=1;i<=n;i++)
	mu[i]=2;
	for(i=2;i<=n;i++){
		if(mu[i]>1){
			pr[++sm]=i;
			mu[i]=-1;
		}
		for(j=1;j<=sm;j++){
			if(1ll*i*pr[j]>n)break;
			k=i*pr[j];mu[k]=-mu[i];
			if(i%pr[j]==0){mu[k]=0;break;}
		}
		if(mu[i]==-1)ss++;
		if(mu[i]==0)s0++;
		if(mu[i]==1)s1++;
		if(i%3679561==0){
			printf("%d,",ss);
		}
	}
	return 0;
}

