#include<ctime>
#include<map>
#include<stdio.h>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=500100,mod=1e9;
inline int read(){
    int x=0,c=getchar(),f=0;
    for(;c>'9'||c<'0';f=c=='-',c=getchar());
    for(;c>='0'&&c<='9';c=getchar())
    x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
void write(ll x){
    if(x>9)write(x/10);
    putchar(x%10+'0');
}
char s[N];
int n,m,o,w,T,P=97,x,y,a[2000100],b,x1,t,xx,yy,i,j,k;
ll rll(){
	return (ll)(rand()<<15)|rand();
}
map<int,bool>f[N];
int l[2001000],r[2001000];
int main(){
	freopen("flow.in","w",stdout);
	srand(unsigned(time(0)));
	n=100000;m=n-1;P=1e9;
	printf("%d %d\n",n,m);
	for(i=1;i<n;i++)printf("%d %d %d\n",rll()%i+1,i+1,rll()%P+1);
	printf("%d\n",n);
	for(i=1;i<=n;i++){
		x=rll()%n+1;
		y=rll()%n+1;
		while(x==y)y=rll()%n+1;
		printf("0 %d %d\n",x,y);
	}
	return 0;
}
