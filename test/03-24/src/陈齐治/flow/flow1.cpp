#include<stdio.h>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
const int N=400100;
inline int read(){
	int x=0,c=getchar(),f=0;
	for(;c>'9'||c<'0';f=c=='-',c=getchar());
	for(;c>='0'&&c<='9';c=getchar())
	x=(x<<1)+(x<<3)+c-'0';return f?-x:x;
}
inline void write(ll x){
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
struct cqz{int x,i;}mn[N],w[N],it;
inline bool operator<(cqz i,cqz j){return i.x<j.x;}
inline cqz operator+(cqz x,int k){x.x+=k;return x;}
int n,m,X[N],Y[N],W[N];
inline cqz min(cqz x,cqz y){return x<y?x:y;}
int c[N][2],rv[N],cv[N],dw[N],cl[N],fa[N];
inline bool isrt(int x){return c[fa[x]][0]!=x&&c[fa[x]][1]!=x;}
inline void up(int x){
	mn[x]=min(mn[c[x][0]],mn[c[x][1]]);
	mn[x]=min(mn[x],w[x]);
}
inline void cov(int x,int k){
	if(!x)return;
	//mn[x].x-=W[cv[x]];
	cv[x]=dw[x]=k;
	//mn[x].x+=W[cv[x]];
}
inline void add(int x,int k){
	if(!x)return;
	cl[x]+=k;
	mn[x].x+=k;w[x].x+=k;
}
inline void down(int x){
	if(rv[x]){
		swap(c[x][0],c[x][1]);rv[x]=0;
		rv[c[x][0]]^=1;rv[c[x][1]]^=1;
	}
	if(dw[x]){
		cov(c[x][0],dw[x]);cov(c[x][1],dw[x]);dw[x]=0;
	}
	if(cl[x]){
		add(c[x][0],cl[x]);add(c[x][1],cl[x]);cl[x]=0;
	}
}
inline void dataup(int x){
	if(!isrt(x))
	dataup(fa[x]);
	down(x);
}
inline void mie(int x){
	int y=fa[x],z=fa[y],l,r;
	if(!isrt(y))c[z][c[z][1]==y]=x;
	l=c[y][1]==x;r=l^1;fa[x]=z;fa[y]=x;
	if(c[x][r])fa[c[x][r]]=y;c[y][l]=c[x][r];
	c[x][r]=y;up(y);up(x);
}
inline void splay(int x){
	for(dataup(x);!isrt(x);mie(x)){
		int y=fa[x],z=fa[y];if(!isrt(y))
		mie((c[y][1]==x)^(c[z][1]==y)?x:y);
	}
}
inline void access(int x){
	for(int y=0;x;y=x,x=fa[x])
	splay(x),c[x][1]=y,up(x);
}
inline void mkrt(int x){
	access(x);splay(x);rv[x]^=1;
}
inline int gettp(int x){
	access(x);splay(x);while(c[x][0])x=c[x][0];return x;
}
inline cqz get(int x,int y){
	mkrt(x);access(y);splay(x);
	return mn[x];
}
inline void link(int i){
	mkrt(X[i]);
	splay(X[i]);
	fa[X[i]]=i;
	mkrt(Y[i]);
	splay(Y[i]);
	fa[Y[i]]=i;
}
inline void cut(int i){
	access(X[i]);splay(i);if(fa[i]==X[i])fa[i]=0;
	access(i);splay(X[i]);if(fa[X[i]]==i)fa[X[i]]=0;
	access(Y[i]);splay(i);if(fa[i]==Y[i])fa[i]=0;
	access(i);splay(Y[i]);if(fa[Y[i]]==i)fa[Y[i]]=0;
}
int in[N];
int main(){
	freopen("flow.in","r",stdin);
	freopen("flow1.out","w",stdout);
	int i,x,y;
	w[0].x=mn[0].x=2e9+1;
	n=read();m=read();
	for(i=1;i<=n;i++)
	w[i]=mn[i]=(cqz){(int)1e9+1,i};
	for(i=n+1;i<=n+m;i++){
		X[i]=read();Y[i]=read();W[i]=read();
		w[i].x=W[i];w[i].i=i;mn[i]=w[i];
		if(gettp(X[i])!=gettp(Y[i]))link(i),in[i]=1;
		else{
			it=get(X[i],Y[i]);
			if(W[it.i]<W[i]){
				cut(it.i);in[it.i]=2;
				link(i);in[i]=1;
				get(X[it.i],Y[it.i]);
				cov(X[it.i],it.i);
				add(X[it.i],W[it.i]);
			}else{
				cov(X[i],i);
				add(X[i],W[i]);
				in[i]=2;
			}
		}
	}
	for(m=read();m--;){
		i=read();x=read();y=read();
		if(i==0){
			it=get(x,y);
			printf("%d\n",it.x);
		}else{
			x=x+n;
			/*splay(x);
			W[x]=y;w[x].x=y;up(x);*/
			if(in[x]==2){
				it=get(X[x],Y[x]);
				add(X[x],-W[x]);
				W[x]=y;w[x].x=y;
				if(W[it.i]<W[x]){
					cut(it.i);in[it.i]=2;
					link(x);in[x]=1;
					get(X[it.i],Y[it.i]);
					cov(X[it.i],it.i);
					add(X[it.i],W[it.i]);
				}else{
					add(X[x],W[x]);
				}
			}else{
				splay(x);
				w[x].x+=y-W[x];
				W[x]=y;up(x);
				y=cv[x];
				if(W[x]<W[y]){
					get(X[y],Y[y]);
					add(X[y],-W[y]);
					cut(x);in[x]=2;
					link(y);in[y]=1;
					get(X[x],Y[x]);
					cov(X[x],x);
					add(X[x],W[x]);
				}
			}
		}
	}return 0;
}
