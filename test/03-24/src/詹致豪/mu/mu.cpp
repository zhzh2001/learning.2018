#include <cstdio>

int i,x,y;

inline int mu(int x)
{
	int t=1;
	for (int i=2;i*i<=x;i++)
		if (! (x%i))
		{
			if (! ((x/i)%i))
				return 0;
			x=x/i,t=-t;
		}
	if (x>1)
		t=-t;
	return t;
}

int main()
{
	freopen("mu.in","r",stdin);
	freopen("mu.out","w",stdout);
	scanf("%d%d",&x,&y);
	for (i=1;;i++)
	{
		if (mu(i)==x)
			y--;
		if (! y)
		{
			printf("%d",i);
			return 0;
		}
	}
}
