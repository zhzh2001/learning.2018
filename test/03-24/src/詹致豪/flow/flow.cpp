#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

const int inf=2000000000;

int edge[500000],next[500000],flow[500000],first[500000];
int g[500000],h[500000];
int i,m,n,s,x,y,z,head,tail,sum_edge;

inline void addedge(int x,int y,int f)
{
	sum_edge++,edge[sum_edge]=y,next[sum_edge]=first[x],flow[sum_edge]=f,first[x]=sum_edge;
	return;
}

inline int oppedge(int x)
{
	return ((x-1)^1)+1;
}

inline void buildgraph()
{
	freopen("flow.in","r",stdin);
	scanf("%d%d",&n,&m);
	for (i=1;i<=m;i++)
	{
		scanf("%d%d%d",&x,&y,&z);
		addedge(x,y,z),addedge(y,x,z);
	}
	scanf("%d",&z);
	scanf("%d%d%d",&z,&x,&y);
	addedge(0,x,inf),addedge(x,0,0);
	addedge(y,n+1,inf),addedge(n+1,y,0);
	return;
}

inline int dinic_bfs()
{
	memset(h,0,sizeof(h));
	tail=1,g[tail]=0,h[g[tail]]=1;
	for (head=1;head<=tail;head++)
		for (i=first[g[head]];i!=0;i=next[i])
			if ((flow[i]) && (! h[edge[i]]))
				tail++,g[tail]=edge[i],h[g[tail]]=h[g[head]]+1;
	return h[n+1];
}

inline int dinic_dfs(int now,int cap)
{
	if (now==n+1)
		return cap;
	int s=0,t=0;
	for (int i=first[now];i!=0;i=next[i])
		if ((flow[i]) && (h[edge[i]]==h[now]+1))
		{
			t=dinic_dfs(edge[i],min(cap,flow[i]));
			s=s+t,cap=cap-t;
			flow[i]=flow[i]-t,flow[oppedge(i)]=flow[oppedge(i)]+t;
			if (! cap)
				break;
		}
	if (! s)
		h[now]=0;
	return s;
}

inline void maxflow()
{
	freopen("flow.out","w",stdout);
	while (dinic_bfs())
		s=s+dinic_dfs(0,inf);
	printf("%d",s);
	return;
}

int main()
{
	buildgraph();
	maxflow();
	return 0;
}
