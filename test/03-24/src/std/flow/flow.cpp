#include <bits/stdc++.h>
#define fi first
#define se second
#define mk make_pair
#define pb push_back
#define CH (ch=getchar())
#define Exit(...)    printf(__VA_ARGS__),exit(0)
#define dprintf(...) fprintf(stderr,__VA_ARGS__)
#define rep(i,V)     for(__typeof(*V.begin()) i:  V)
#define For(i,a,b)   for(int i=(int)a;i<=(int)b;i++)
#define Rep(i,a,b)   for(int i=(int)a;i< (int)b;i++)
#define Forn(i,a,b)  for(int i=(int)a;i>=(int)b;i--)
#define pend(x)      ((x)=='\n'||(x)=='\r'||(x)=='\t'||(x)==' ')
#define MIN(x,y)     ((x)<(y)?(x):(y)) 
using namespace std;
typedef	double		db;
typedef	long long	ll;
const	int N=300005;
const	ll	Inf=(ll)1e10;
const	int inf=(int)2e9;
const	int	mo=ll(1e9+7);
 
int THIS_VARIBLE_NOT_UESD;
#define huan(x,y) {THIS_VARIBLE_NOT_UESD=x,x=y,y=THIS_VARIBLE_NOT_UESD;}
 
inline int IN(){
	char ch;CH; int f=0,x=0;
	for(;pend(ch);CH); if(ch=='-')f=1,CH;
	for(;!pend(ch);CH) x=x*10+ch-'0';
	return (f)?(-x):(x);
}
 
int Pow(int x,int y,int p){
	int A=1;
	for(;y;y>>=1,x=(ll)x*x%p) if(y&1) A=(ll)A*x%p;
	return A;
}
 
vector<pair<int,int> > e[N];
int n,m;
#define jia(x,y) (x->Min+=y,x->plu+=y,x->val+=y*x->spe)
#define rever(x) {x->rev^=1;swap(x->s[0],x->s[1]);}
struct node{
	node *s[2],*f;
	ll Min,plu,val;
	int rev,spe;
	void upd(){
		Min=val;
		For(i,0,1) if(s[i]) Min=MIN(Min,s[i]->Min);
	}
	bool isr(){return !f||(f->s[0]!=this&&f->s[1]!=this);}
	void rot(){
		node *y=f; int d=(f->s[1]==this);
		if(!y->f) f=NULL;else{
			if(y->f->s[0]==y) y->f->s[0]=this;
			if(y->f->s[1]==y) y->f->s[1]=this;
			f=y->f;
		}
		y->s[d]=s[!d];
		if(s[!d]) s[!d]->f=y;
		s[!d]=y,y->f=this;
		y->upd();
	}
}ex[N],*S[N]; int T;
 
#define isr(x) (!x->f||(x->f->s[0]!=x&&x->f->s[1]!=x))
 
void splay(node *x){
	for(node *k=x;;k=k->f){
		S[++T]=k;
		if(isr(k)) break;
	}
	for(;T;T--){
		node *p=S[T];
		For(i,0,1)if(p->s[i]){
			jia(p->s[i],p->plu);
			if(p->rev) rever(p->s[i]);
		}
		p->plu=p->rev=0;
	}
	while(!isr(x)){
		node *y=x->f;
		if(isr(y)){
			x->rot();
			break;
		}
		if((y->f->s[1]==y)==(x->f->s[1]==x)) y->rot();else x->rot();
		x->rot();
	}
	x->upd();
}
 
void access(node *x){
	node *y=NULL;
	for(;x;){
		splay(x);
		x->s[1]=y;
		x->upd();
		y=x;
		x=x->f;
	}
}
 
void evert(node *x){
	access(x);
	splay(x);
	rever(x);
}
 
int query(node *x,node *y){
	evert(x);
	access(y);
	splay(y);
	return y->Min;
}
 
void link(int x,int y){
	evert(ex+x);
	evert(ex+y);
	ex[y].f=ex+x;
}
 
void link(int x,int y,int z){
//	dprintf("link %d %d\n",x,y);
	z+=n;
	link(x,z);
	link(z,y);
}
 
void modify(int x,int y,ll val){
	if(!val) return;
//	dprintf("delta %d %d %d\n",x,y,val);
	evert(ex+x);
	access(ex+y);
	splay(ex+y);
	jia((ex+y),val);
}
 
void cut(int x,int y){
	evert(ex+x);
	access(ex+x);
	splay(ex+y);
	ex[y].f=NULL;
}
 
void cut(int x,int y,int z){
//	dprintf("cut %d %d\n",x,y);
	z+=n;
	cut(x,z);
	cut(z,y);
}
 
pair<int,int> E[N];
int dep[N],fa[N],dfn[N],tot,A[N],fr[N];
int cir,bel[N],cirmin[N];
void dfs(int x){
	dfn[x]=1;
	for(int i=0;i<(int)e[x].size();i++){
		pair<int,int> v=e[x][i];
		if(dfn[v.fi])continue;
		fa[v.fi]=x;
		dep[v.fi]=dep[x]+1;
		fr[v.fi]=v.se;
		dfs(v.fi);
	}
}
 
int lc[N],rc[N],tfa[N],rt[N];
 
int merge(int x,int y){
	if(!x||!y) return x^y;
	if(A[x]>A[y]) huan(x,y);
	int p=merge(rc[x],y);
	tfa[rc[x]=p]=x;
	huan(lc[x],rc[x]);
	return x;
}
 
void erase(int x){
	int p=merge(lc[x],rc[x]);
	if(!tfa[x]) rt[bel[x]]=p;
	if(p) tfa[p]=tfa[x];
	if(lc[tfa[x]]==x) lc[tfa[x]]=p;
	if(rc[tfa[x]]==x) rc[tfa[x]]=p;
	tfa[x]=lc[x]=rc[x]=0;
}
 
void insert(int x,int y){
	rt[x]=merge(rt[x],y);
}
 
int main(){
	freopen("flow.in","r",stdin);
	freopen("flow.out","w",stdout);
	n=IN(),m=IN();
	For(i,1,m){
		int x=IN(),y=IN(),w=IN();
		e[x].pb(mk(y,i));
		e[y].pb(mk(x,i));
		A[i]=w;
		E[i]=mk(x,y);
		ex[n+i].val=ex[n+i].Min=w;
	}
	For(i,1,n) ex[i].val=ex[i].Min=inf;
	For(i,n+1,n+m) ex[i].spe=1;
	dfs(1);
	For(i,1,m){
		int u=E[i].fi,v=E[i].se,fg=0;
		if(fa[E[i].fi]==E[i].se) u=E[i].fi,v=E[i].se,fg=1;
		if(fa[E[i].se]==E[i].fi) u=E[i].se,v=E[i].fi,fg=1;
		if(fg){
			ex[u].f=ex+i+n;
			ex[i+n].f=ex+v;
		}
	}
	For(i,1,m){
		int u=E[i].fi,v=E[i].se,fg=0;
		if(fa[E[i].fi]==E[i].se) fg=1;
		if(fa[E[i].se]==E[i].fi) fg=1;
		if(!fg){
			++cir;
			bel[i]=cir;
			insert(cir,i);
			for(int x=u,y=v;x!=y;x=fa[x]){
				if(dep[x]<dep[y]) swap(x,y);
				insert(cir,fr[x]);
				bel[fr[x]]=cir;
			}
			if(rt[cir]!=i){
				int u=rt[cir];
				cut(E[u].fi,E[u].se,u);
				link(E[i].fi,E[i].se,i);
			}
			int u=rt[cir];
			cirmin[cir]=u;
			modify(E[u].fi,E[u].se,A[u]);
			erase(u);
		}
	}
	
	int m=IN(),tQ=0;
	For(z,1,m){
		int ty=IN();
		if(!ty){
			int S=IN(),T=IN();
			printf("%d\n",query(ex+S,ex+T));
		}else{
			int x=IN(),y=IN();
			int u=E[x].fi,v=E[x].se;
			if(bel[x]){
				int &tmp=cirmin[bel[x]],cir=bel[x];
				if(tmp!=x&&A[tmp]>y){
					modify(E[tmp].fi,E[tmp].se,-A[tmp]);
					erase(x);
					cut(u,v,x);
					
					link(E[tmp].fi,E[tmp].se,tmp);
					insert(cir,tmp);
					modify(u,v,A[x]);
					tmp=x;
				}else
				if(tmp==x&&y>A[x]){
					int cu=rt[cir];
					if(y>A[cu]){
						modify(u,v,-A[x]);
						erase(cu);
						cut(E[cu].fi,E[cu].se,cu);
 
						link(u,v,x);
						modify(u,v,y-A[x]);
						A[x]=y;
						insert(cir,tmp);
						modify(E[cu].fi,E[cu].se,A[cu]);
						tmp=cu;
					}
				}else{
					if(tmp!=x){
						erase(x);
						modify(u,v,y-A[x]);
						A[x]=y;
						insert(cir,x);
					}
				}
				if(tmp==x) ex[x+n].val=y;
			}
			modify(u,v,y-A[x]);
			A[x]=y;
		}
	}
	return 0;
}
 
 
