#include <bits/stdc++.h>
#define PR pair
#define fi first
#define se second
#define mk make_pair
#define pb push_back
#define For(i,x,y)   for(int i=(int)(x);i<=(int)(y);i++)
#define Rep(i,x,y)   for(int i=(int)(x);i< (int)(y);i++)
#define Forn(i,x,y)  for(int i=(int)(x);i>=(int)(y);i--)
#define CH	         ch=getchar()
#define Exit(...)    printf(__VA_ARGS__),exit(0)
#define dprintf(...) fprintf(stderr,__VA_ARGS__)
using namespace std;
typedef double	  db;
typedef long long ll;
typedef PR<int,int> PII;
const	int inf=1e9;
const	ll Inf=1e10;
const	int P=1e9+7;
const	int N=1000005;

inline int IN(){
	int x=0;int ch=0,f=0;
	for(CH;ch!=-1&&(ch<48||ch>57);CH)  f=(ch=='-');
	for(;ch>=48&&ch<=57;CH) x=(x<<1)+(x<<3)+ch-'0';
	return f?(-x):x;
}

int Pow(int x,int y,int p){
	int a=1;
	for(;y;y>>=1,x=(ll)x*x%p) if(y&1) a=(ll)a*x%p;
	return a;
}

int pr[N],tot,vis[N],ask,kth;
int mu[N],c[N],lim=1e6;
int T(int n){
	int ans=0;
	for(int i=1;1ll*i*i<=1ll*n;i++) ans+=mu[i]*n/(i*i);
	return ans;
}

map<int,int> mps;
int S(int n){
	if(n<=1e6) return c[n];
	if(mps.count(n)) return mps[n];
	int res=1;
	for(int i=2,j;i<=n;i=j+1){
		j=n/(n/i);
		res-=(j-i+1)*S(n/i);
	}
	return mps[n]=res;
}

int A(int n){
	if(ask==0) return n-T(n);
	if(ask==1) return (T(n)+S(n))/2;
	return (T(n)-S(n))/2;
}

int main(){
	freopen("mu.in","r",stdin);
	freopen("mu.out","w",stdout);
	scanf("%d%d",&ask,&kth);
	mu[1]=1;
	For(i,2,lim){
		if(!vis[i]) mu[pr[++tot]=i]=-1;
		for(int j=1;j<=tot&&1ll*i*pr[j]<=lim;j++){
			vis[i*pr[j]]=1;
			if(i%pr[j]==0){
				mu[i*pr[j]]=0;
				break;
			}else{
				mu[i*pr[j]]=-mu[i];
			}
		}
	}
	For(i,1,lim) c[i]=mu[i]+c[i-1];
		
	int l=1,r=2e9,aim=0;
	while(l<=r){
		int md=(l+r)>>1;
		if(A(md)>=kth){
			aim=md;
			r=md-1;
		}else{
			l=md+1;
		}
	}
	printf("%d\n",aim);
	dprintf("%d\n",clock());
	return 0;
}
/*
#include <bits/stdc++.h>
#define PR pair
#define fi first
#define se second
#define mk make_pair
#define pb push_back
#define For(i,x,y)   for(int i=(int)(x);i<=(int)(y);i++)
#define Rep(i,x,y)   for(int i=(int)(x);i< (int)(y);i++)
#define Forn(i,x,y)  for(int i=(int)(x);i>=(int)(y);i--)
#define CH	         ch=getchar()
#define Exit(...)    printf(__VA_ARGS__),exit(0)
#define dprintf(...) fprintf(stderr,__VA_ARGS__)
using namespace std;
typedef double	  db;
typedef long long ll;
typedef PR<int,int> PII;
const	int inf=1e9;
const	ll Inf=1e10;
const	int P=1e9+7;
const	int N=1000005;

inline int IN(){
	int x=0;int ch=0,f=0;
	for(CH;ch!=-1&&(ch<48||ch>57);CH)  f=(ch=='-');
	for(;ch>=48&&ch<=57;CH) x=(x<<1)+(x<<3)+ch-'0';
	return f?(-x):x;
}

int Pow(int x,int y,int p){
	int a=1;
	for(;y;y>>=1,x=(ll)x*x%p) if(y&1) a=(ll)a*x%p;
	return a;
}

int pr[N],tot,vis[N],ask,kth;
int mu[N],c[N],lim=1e6;
int T(int n){
	int ans=0;
	for(int i=1;1ll*i*i<=1ll*n;i++) ans+=mu[i]*n/(i*i);
	return ans;
}

map<int,int> mps;
int S(int n){
	if(n<=1e6) return c[n];
	if(mps.count(n)) return mps[n];
	int res=1;
	for(int i=2,j;i<=n;i=j+1){
		j=n/(n/i);
		res-=(j-i+1)*S(n/i);
	}
	return mps[n]=res;
}

int A(int n){
	if(ask==0) return n-T(n);
	if(ask==1) return (T(n)+S(n))/2;
	return (T(n)-S(n))/2;
}

int main(){
	freopen("mu.in","r",stdin);
	freopen("mu.out","w",stdout);
	scanf("%d%d",&ask,&kth);
	mu[1]=1;
	For(i,2,lim){
		if(!vis[i]) mu[pr[++tot]=i]=-1;
		for(int j=1;j<=tot&&1ll*i*pr[j]<=lim;j++){
			vis[i*pr[j]]=1;
			if(i%pr[j]==0){
				mu[i*pr[j]]=0;
				break;
			}else{
				mu[i*pr[j]]=-mu[i];
			}
		}
	}
	For(i,1,lim) c[i]=mu[i]+c[i-1];
		
	int l=1,r=2e9,aim=0;
	while(l<=r){
		int md=(l+r)>>1;
		if(A(md)>=kth){
			aim=md;
			r=md-1;
		}else{
			l=md+1;
		}
	}
	printf("%d\n",aim);
	dprintf("%d\n",clock());
	return 0;
}
*/
/*
#include <bits/stdc++.h>
#define PR pair
#define fi first
#define se second
#define mk make_pair
#define pb push_back
#define For(i,x,y)   for(int i=(int)(x);i<=(int)(y);i++)
#define Rep(i,x,y)   for(int i=(int)(x);i< (int)(y);i++)
#define Forn(i,x,y)  for(int i=(int)(x);i>=(int)(y);i--)
#define CH	         ch=getchar()
#define Exit(...)    printf(__VA_ARGS__),exit(0)
#define dprintf(...) fprintf(stderr,__VA_ARGS__)
using namespace std;
typedef double	  db;
typedef long long ll;
typedef PR<int,int> PII;
const	int inf=1e9;
const	ll Inf=1e10;
const	int P=1e9+7;
const	int N=1000005;

inline int IN(){
	int x=0;int ch=0,f=0;
	for(CH;ch!=-1&&(ch<48||ch>57);CH)  f=(ch=='-');
	for(;ch>=48&&ch<=57;CH) x=(x<<1)+(x<<3)+ch-'0';
	return f?(-x):x;
}

int Pow(int x,int y,int p){
	int a=1;
	for(;y;y>>=1,x=(ll)x*x%p) if(y&1) a=(ll)a*x%p;
	return a;
}

int pr[N],tot,vis[N],ask,kth;
int mu[N],c[N],lim=1e6;
int T(int n){
	int ans=0;
	for(int i=1;1ll*i*i<=1ll*n;i++) ans+=mu[i]*n/(i*i);
	return ans;
}

map<int,int> mps;
int S(int n){
	if(n<=1e6) return c[n];
	if(mps.count(n)) return mps[n];
	int res=1;
	for(int i=2,j;i<=n;i=j+1){
		j=n/(n/i);
		res-=(j-i+1)*S(n/i);
	}
	return mps[n]=res;
}

int A(int n){
	if(ask==0) return n-T(n);
	if(ask==1) return (T(n)+S(n))/2;
	return (T(n)-S(n))/2;
}

int main(){
	freopen("mu.in","r",stdin);
	freopen("mu.out","w",stdout);
	scanf("%d%d",&ask,&kth);
	mu[1]=1;
	For(i,2,lim){
		if(!vis[i]) mu[pr[++tot]=i]=-1;
		for(int j=1;j<=tot&&1ll*i*pr[j]<=lim;j++){
			vis[i*pr[j]]=1;
			if(i%pr[j]==0){
				mu[i*pr[j]]=0;
				break;
			}else{
				mu[i*pr[j]]=-mu[i];
			}
		}
	}
	For(i,1,lim) c[i]=mu[i]+c[i-1];
		
	int l=1,r=2e9,aim=0;
	while(l<=r){
		int md=(l+r)>>1;
		if(A(md)>=kth){
			aim=md;
			r=md-1;
		}else{
			l=md+1;
		}
	}
	printf("%d\n",aim);
	dprintf("%d\n",clock());
	return 0;
}
*/
