#include <set>
#include <map>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <ctime>
#define PR pair
#define fi first
#define se second
#define mk make_pair
#define pb push_back
#define For(i,x,y)   for(int i=(int)(x);i<=(int)(y);i++)
#define Rep(i,x,y)   for(int i=(int)(x);i< (int)(y);i++)
#define Forn(i,x,y)  for(int i=(int)(x);i>=(int)(y);i--)
#define CH             ch=getchar()
#define Exit(...)    printf(__VA_ARGS__),exit(0)
#define dprintf(...) fprintf(stderr,__VA_ARGS__)
using namespace std;
typedef double      db;
typedef long long ll;
typedef PR<int,int> PII;
const    int inf=1e9;
const    ll Inf=1e10;
const    int P=998244353;

inline int IN(){
    int x=0;int ch=0,f=0;
    for(CH;ch!=-1&&(ch<48||ch>57);CH)  f=(ch=='-');
    for(;ch>=48&&ch<=57;CH) x=(x<<1)+(x<<3)+ch-'0';
    return f?(-x):x;
}

int Pow(int x,int y,int p){
    int a=1;
    for(;y;y>>=1,x=(ll)x*x%p) if(y&1) a=(ll)a*x%p;
    return a;
}

int f[100005],g[100005],fac[100005],facinv[100005],n,N;
int w[2][400005],rev[400005],invn;
int ff[400005],gg[400005];

int C(int x,int y){
    return 1ll*fac[x]*facinv[y]%P*facinv[x-y]%P;
}

void pret(int n){
	for(N=1;N<=n;N<<=1);N<<=1;
	int W=Pow(3,(P-1)/N,P),IW=Pow(W,P-2,P);
	w[0][0]=w[1][0]=1;
	Rep(i,1,N){
		w[0][i]=1ll*w[0][i-1]*W%P;
		w[1][i]=1ll*w[1][i-1]*IW%P;
	}
	Rep(i,0,N){
		int x=i,y=0;
		for(int k=1;k<N;k<<=1,x>>=1) (y<<=1)|=(x&1);
		rev[i]=y;
	}
	invn=Pow(N,P-2,P);
}

void fft(int *a,int f){
	Rep(i,0,N) if(i<rev[i]) swap(a[i],a[rev[i]]);
	for(int i=1;i<N;i<<=1)
	for(int j=0,t=N/(i<<1);j<N;j+=i<<1)
	for(int k=0,l=0;k<i;k++,l+=t){
		int x=1ll*w[f][l]*a[j+k+i]%P;
		int y=a[j+k];
		a[j+k]=(y+x)%P;
		a[j+k+i]=(y+P-x)%P;
	}
	if(f) Rep(i,0,N) a[i]=1ll*a[i]*invn%P;
}

int main(){
    freopen("graph.in","r",stdin);
    freopen("graph.out","w",stdout);
    n=100000;
    fac[0]=1;
    For(i,1,n) fac[i]=1ll*fac[i-1]*i%P;
    For(i,0,n) facinv[i]=Pow(fac[i],P-2,P);
    For(i,1,n) f[i]=1ll*Pow(i,i-1,P)*(i+1)%P;
    For(i,0,n) g[i]=Pow(i,i,P);

    
    pret(n);
    For(i,0,n) ff[i]=1ll*f[i]*facinv[i]%P;
    For(i,0,n) gg[i]=1ll*g[i]*facinv[i]%P;
    fft(ff,0);
    fft(gg,0);
    Rep(i,0,N) ff[i]=1ll*ff[i]*gg[i]%P;
    fft(ff,1);
    
    int T=IN(),cas=0;
    while(T--){
        n=IN();
        int ans=1ll*fac[n-1]*ff[n-1]%P;
        ans=(Pow(n,n,P)-(0ll+ans+1ll*n*g[n-1]%P)%P+P)%P;
        ans=1ll*ans*n%P;
        printf("%d\n",ans);
    }
    return 0;
}
