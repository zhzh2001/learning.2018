#include<bits/stdc++.h>
#define ll long long
#define N 100005
#define inf (1ll<<60)
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,T,tot=1,cnt,a[N],tmp[N],pos[N],num[N],id[N],top[N],size[N],son[N],deep[N],fa[N],head[N],minn[N<<2];
struct edge{ ll to,next,w; }e[N<<1];
void add(ll u,ll v,ll w){
	e[++tot]=(edge){v,head[u],w}; head[u]=tot;
}
void dfs1(ll u,ll last){
	size[u]=1;
	for (ll i=head[u],v=e[i].to;i;i=e[i].next,v=e[i].to){
		if (v==last) continue; fa[v]=u; deep[v]=deep[u]+1; dfs1(v,u); tmp[i/2]=v;
		if (size[son[u]]<size[v]) son[u]=v; size[u]+=size[v]; num[v]=e[i].w;
	}
}
void dfs2(ll u,ll last,ll now){
	id[u]=++cnt; top[u]=now; pos[cnt]=u;
	if (!son[u]) return; dfs2(son[u],u,now);
	for (ll i=head[u],v=e[i].to;i;i=e[i].next,v=e[i].to){
		if (v!=son[u]&&v!=last) dfs2(v,u,v);
	}
}
void build(ll l,ll r,ll p){
	if (l==r){
		minn[p]=num[pos[l]];
		return;
	}
	ll mid=l+r>>1;
	build(l,mid,p<<1); build(mid+1,r,p<<1|1);
	minn[p]=min(minn[p<<1],minn[p<<1|1]);
}
ll query(ll l,ll r,ll s,ll t,ll p){
	if (l==s&&r==t) return minn[p];
	ll mid=l+r>>1;
	if (t<=mid) return query(l,mid,s,t,p<<1);
	else if (s>mid) return query(mid+1,r,s,t,p<<1|1);
	else return min(query(l,mid,s,mid,p<<1),query(mid+1,r,mid+1,t,p<<1|1));
}
void modfiy(ll l,ll r,ll pos,ll v,ll p){
	if (l==r) { minn[p]=v; return; }
	ll mid=l+r>>1;
	if (pos<=mid) modfiy(l,mid,pos,v,p<<1);
	else modfiy(mid+1,r,pos,v,p<<1|1);
	minn[p]=min(minn[p<<1],minn[p<<1|1]);
}
void work(){
	rep(i,1,m){
		ll u=read(),v=read(),w=read();
		add(u,v,w); add(v,u,w);
	}
	num[1]=inf; dfs1(1,0); dfs2(1,0,1);
	build(1,cnt,1); T=read();
	while (T--){
		ll opt=read();
		if (opt){
			ll x=read(),y=read();
			modfiy(1,cnt,id[tmp[x]],y,1);
		}else{
			ll x=read(),y=read(),ans=inf;
			while (top[x]!=top[y]){
				if (deep[top[x]]>deep[top[y]]){
					ans=min(ans,query(1,cnt,id[top[x]],id[x],1));
					x=fa[top[x]];
				}else{
					ans=min(ans,query(1,cnt,id[top[y]],id[y],1));
					y=fa[top[y]];
				}
			}
			ans=min(ans,query(1,cnt,min(id[x],id[y]),max(id[x],id[y]),1));
			printf("%lld\n",ans);
		}
	}
}
int main(){
	freopen("flow.in","r",stdin);
	freopen("flow.out","w",stdout);
	n=read(); m=read();
	if (m==n-1) work();
	else puts("Szb Ak Zjoi!");
}
