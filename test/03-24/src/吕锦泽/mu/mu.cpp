#include<bits/stdc++.h>
#define ll int
#define N 4000005
#define M 300005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,num,u[N],pri[M];
bool flag[N];
int main(){
	freopen("mu.in","r",stdin);
	freopen("mu.out","w",stdout);
	u[1]=1;
	rep(i,2,N-5){
		if (!flag[i]) pri[++num]=i,u[i]=-1;
		for (ll j=1;j<=num&&i*pri[j]<N;++j){
			flag[i*pri[j]]=pri[j];
			if (i%pri[j]) u[i*pri[j]]=u[i]*u[pri[j]];
			else { u[i*pri[j]]=0; break; }
		}
	}
	n=read(); m=read();
	rep(i,1,N-5){
		if (u[i]==n) --m;
		if (!m){
			printf("%d",i);
			return 0;
		}
	}
}
