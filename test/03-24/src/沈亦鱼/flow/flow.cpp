#include<cstdio>
using namespace std;
int n,m,tot,tim,u,v,w,ques,opt,x,y,p[510000],ta[510000],ne[510000],he[110000],fa[110000],us[110000],l[110000],r[110000];
long long ans;
void adg(int u,int v,int w){
	tot++;
	p[tot]=v;
	ta[tot]=w;
	ne[tot]=he[u];
	he[u]=tot;
}
void dfs(int k,int fath){
	tim++;
	l[k]=tim;
	for(int i=he[k];i;i=ne[i])
		if(p[i]!=fath){
			fa[p[i]]=k;
//			dfs[p[i]]=dis[k]+1;
			us[p[i]]=i;
			dfs(p[i],k);
		}
	r[k]=tim;
}
void sea(int k,int s){
	if(l[k]<=l[s]&&r[s]<=r[k])return;
	else{
		if(ta[us[k]]<ans)ans=ta[us[k]];
		sea(fa[k],s);
	}
}
int main(){
	freopen("flow.in","r",stdin);
	freopen("flow.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		scanf("%d%d%d",&u,&v,&w);
		adg(u,v,w);
		adg(v,u,w);
	}
	dfs(1,0);
	scanf("%d",&ques);
	for(int p=1;p<=ques;p++){
		scanf("%d%d%d",&opt,&x,&y);
		if(opt==0){
			ans=2147483647;
			sea(x,y);
			sea(y,x);
			printf("%lld\n",ans);
		}
		else{
			ta[x*2-1]=y;
			ta[x*2]=y;
		}
	}
	return 0;
}
/*
����������1��
6 5
1 2 1
4 5 8
4 3 2
6 5 5
1 6 4
6
0 4 5
0 1 3
0 1 2
1 1 5
1 6 5
0 1 2
���������1��
9
3
2
7
*/
