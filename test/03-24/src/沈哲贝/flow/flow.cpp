#include<bits/stdc++.h>
using namespace std;
#define ll int
#define llu unsigned long long
#define ld double
#define llu unsigned long long
#define rep(i,x,y)  for(ll i=x;i<y;++i)
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
#define pi acos(-1)
#define mk make_pair<ll,ll>
#define pa pair<ll,ll>
#define lf else if
#define max(x,y)    ((x)<(y)?(y):(x))
#define min(x,y)    ((x)<(y)?(x):(y))
#define sqr(x)      ((x)*(x))
#define abs(x)      ((x)>0?(x):-(x))
#define Mul(x,y)    ((x)=1LL*(x)*(y)%mod)
#define Add(x,y)    ((x)=((x)+(y))%mod)
#define Max(x,y)    ((x)=((x)<(y)?(y):(x)))
#define Min(x,y)    ((x)=((x)>(y)?(y):(x)))
#define E(x)        return writeln(x),0
#define p(x)        printf("~%d~\n",x)
#define pp(x,y)     printf("~~%d %d~~\n",x,y)
#define ppp(x,y,z)  printf("~~~%d %d %d~~~\n",x,y,z)
#define pppp(a,b,c,d)	printf("~~~%d %d %d %d\n",a,b,c,d)
#define f_in(x)     freopen(x".in","r",stdin)
#define f_out(x)    freopen(x".out","w",stdout)
#define open(x)     f_in(x),f_out(x)
typedef complex<double> E;
namespace SHENZHEBEI{
#define NEG 1
    const int L=2333333;
    inline char gc(){	return getchar();	}
#if NEG
    inline ll read(){    ll x=0,f=1; char ch=gc();   for (;!isdigit(ch);ch=gc()) if (ch=='-') f=-1;  for (;isdigit(ch);ch=gc())  x=x*10-48+ch;   return x*f; }
    inline void write(ll x){    if (x<0) putchar('-'),x=-x;  if (x>=10)   write(x/10);    putchar(x%10+'0');  }
#else
    inline ll read(){    ll x=0; char ch=gc();   for (;!isdigit(ch);ch=gc());    for (;isdigit(ch);ch=gc())  x=x*10-48+ch;   return x;   }
    inline void write(ll x){    if (x>=10)   write(x/10);    putchar(x%10+'0');  }
#endif
    inline char readchar(){ char ch=gc();   for(;isspace(ch);ch=gc());  return ch;  }
    inline ll readstr(char *s){ char ch=gc();   int cur=0;  for(;isspace(ch);ch=gc());      for(;!isspace(ch);ch=gc())  s[cur++]=ch;    s[cur]='\0';    return cur; }
    inline void writeln(ll x){  write(x);   puts("");   }
}using namespace SHENZHEBEI;
const ll N=200010,inf=1e9;
ll head[N],nxt[N],vet[N],fa[N],dep[N],dfn[N],X[N],Y[N],mn[N],val[N],a[N],siz[N],son[N],q[N],top[N];
ll tot,n,m,cnt;
void insert(ll x,ll y,ll w){nxt[++tot]=head[x];head[x]=tot;vet[tot]=y;val[tot]=w;}
void dfs(ll x){
	for(ll i=head[x];i;i=nxt[i])if (vet[i]!=fa[x]){
		a[vet[i]]=val[i],fa[vet[i]]=x,dep[vet[i]]=dep[x]+1,dfs(vet[i]);
		siz[x]+=siz[vet[i]];if (siz[vet[i]]>siz[son[x]])son[x]=vet[i];
	}++siz[x];
}
void dfs1(ll x){
	dfn[q[++cnt]=x]=cnt;top[x]=top[x]?top[x]:x;
	if (son[x])top[son[x]]=top[x],dfs1(son[x]);
	for(ll i=head[x];i;i=nxt[i])if (vet[i]!=fa[x]&&vet[i]!=son[x])dfs1(vet[i]);
}
void build(ll l,ll r,ll p){
	if (l==r)return mn[p]=a[q[l]],void(0);
	ll mid=(l+r)>>1;build(l,mid,p<<1),build(mid+1,r,p<<1|1);
	mn[p]=min(mn[p<<1],mn[p<<1|1]);
}
void change(ll l,ll r,ll p,ll pos,ll v){
	if (l==r)return mn[p]=v,void(0);
	ll mid=(l+r)>>1;pos<=mid?change(l,mid,p<<1,pos,v):change(mid+1,r,p<<1|1,pos,v);
	mn[p]=min(mn[p<<1],mn[p<<1|1]);
}
ll Qmin(ll l,ll r,ll p,ll s,ll t){
	if (s<=l&&r<=t)return mn[p];
	ll mid=(l+r)>>1,ans=inf;if (s<=mid)Min(ans,Qmin(l,mid,p<<1,s,t));if (t>mid)Min(ans,Qmin(mid+1,r,p<<1|1,s,t)); 
	return ans;
}
ll Query(ll x,ll y){
	ll ans=inf;
	for(;top[x]!=top[y];){
		if (dep[top[x]]<dep[top[y]])swap(x,y);
		Min(ans,Qmin(1,n,1,dfn[top[x]],dfn[x]));
		x=fa[top[x]];
	}if (x^y){if(dep[x]<dep[y])swap(x,y);Min(ans,Qmin(1,n,1,dfn[y]+1,dfn[x]));}
	return ans;
}
void work2(){
	rep(i,1,n){X[i]=read(),Y[i]=read();ll w=read();insert(X[i],Y[i],w),insert(Y[i],X[i],w);}
	dfs(1);dfs1(1);build(1,n,1);
	rep(i,1,n)if (dep[X[i]]<dep[Y[i]])swap(X[i],Y[i]);
	for(ll Q=read();Q--;){
		ll opt=read();
		if (opt==1){ll x=read(),y=read();change(1,n,1,dfn[X[x]],y);}
		else writeln(Query(read(),read()));
	}
}
int main(){
	open("flow");
	n=read(),m=read();
	if (m==n-1)work2();
}
