#include<bits/stdc++.h>
using namespace std;
#define ll int
#define llu unsigned long long
#define ld double
#define llu unsigned long long
#define rep(i,x,y)  for(ll i=x;i<y;++i)
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
#define pi acos(-1)
#define mk make_pair<ll,ll>
#define pa pair<ll,ll>
#define lf else if
#define max(x,y)    ((x)<(y)?(y):(x))
#define min(x,y)    ((x)<(y)?(x):(y))
#define sqr(x)      ((x)*(x))
#define abs(x)      ((x)>0?(x):-(x))
#define Mul(x,y)    ((x)=1LL*(x)*(y)%mod)
#define Add(x,y)    ((x)=((x)+(y))%mod)
#define Max(x,y)    ((x)=((x)<(y)?(y):(x)))
#define Min(x,y)    ((x)=((x)>(y)?(y):(x)))
#define E(x)        return writeln(x),0
#define p(x)        printf("~%d~\n",x)
#define pp(x,y)     printf("~~%d %d~~\n",x,y)
#define ppp(x,y,z)  printf("~~~%d %d %d~~~\n",x,y,z)
#define pppp(a,b,c,d)	printf("~~~%d %d %d %d\n",a,b,c,d)
#define f_in(x)     freopen(x".in","r",stdin)
#define f_out(x)    freopen(x".out","w",stdout)
#define open(x)     f_in(x),f_out(x)
typedef complex<double> E;
namespace SHENZHEBEI{
#define NEG 1
    const int L=2333333;
    inline char gc(){	return getchar();	}
#if NEG
    inline ll read(){    ll x=0,f=1; char ch=gc();   for (;!isdigit(ch);ch=gc()) if (ch=='-') f=-1;  for (;isdigit(ch);ch=gc())  x=x*10-48+ch;   return x*f; }
    inline void write(ll x){    if (x<0) putchar('-'),x=-x;  if (x>=10)   write(x/10);    putchar(x%10+'0');  }
#else
    inline ll read(){    ll x=0; char ch=gc();   for (;!isdigit(ch);ch=gc());    for (;isdigit(ch);ch=gc())  x=x*10-48+ch;   return x;   }
    inline void write(ll x){    if (x>=10)   write(x/10);    putchar(x%10+'0');  }
#endif
    inline char readchar(){ char ch=gc();   for(;isspace(ch);ch=gc());  return ch;  }
    inline ll readstr(char *s){ char ch=gc();   int cur=0;  for(;isspace(ch);ch=gc());      for(;!isspace(ch);ch=gc())  s[cur++]=ch;    s[cur]='\0';    return cur; }
    inline void writeln(ll x){  write(x);   puts("");   }
}using namespace SHENZHEBEI;
const ll N=2000010,mod=1e9+7,M=N*23;
struct dt{ll x,y,id;	}p[N];
ll from[M],tr[M],head[M],nxt[M],vet[M],mark[M],vis[M],n,tot,node_size,answ=1,_L,_R;
bool cmp1(dt a,dt b){return a.x<b.x;}
bool cmp2(dt a,dt b){return a.y>b.y;}
void insert(ll x,ll y){if (!x||!y)return;nxt[++tot]=head[x];head[x]=tot;from[tot]=x;vet[tot]=y;}
void Insert(ll l,ll r,ll p,ll s,ll t,ll id){
	if (!tr[p])return;
	if (s<=l&&r<=t)insert(id,tr[p]),void(0);
	ll mid=(l+r)>>1;
	if (s<=mid)	Insert(l,mid,p<<1,s,t,id);
	if (t>mid)	Insert(mid+1,r,p<<1|1,s,t,id);
}
void NewNode(ll l,ll r,ll p,ll s,ll t,ll id){
	if (s<l||t>r)return;
	insert(++node_size,tr[p]);
	insert(tr[p]=node_size,id);
	if (l==r)return;
	ll mid=(l+r)>>1;
	NewNode(l,mid,p<<1,s,t,id);
	NewNode(mid+1,r,p<<1|1,s,t,id);
}
void dfs(ll x,ll col){
	if (mark[x]){if ((x<=n)&&mark[x]!=col){puts("0");exit(0);}return;}
	mark[x]=col;
	for(ll i=head[x];i;i=nxt[i])dfs(vet[i],vet[i]<=n?3-col:col);
}
int main(){
	f_in("ernd");
	n=read();
	For(i,1,n){ll x=read(),y=read();p[i]=(dt){x,y,i};}
	sort(p+1,p+n+1,cmp1);node_size=n;
	memset(tr,0,sizeof tr);
	For(i,1,n)	Insert(1,2*n,1,p[i].x,p[i].y,p[i].id),
				NewNode(1,2*n,1,p[i].y,p[i].y,p[i].id);
	sort(p+1,p+n+1,cmp2);
	memset(tr,0,sizeof tr);
	For(i,1,n)	Insert(1,2*n,1,p[i].x,p[i].y,p[i].id),
				NewNode(1,2*n,1,p[i].x,p[i].x,p[i].id);
	For(i,1,n)if (!mark[i])dfs(i,1),Mul(answ,2);
	writeln(answ);
}
/*
如果包含这个集合的补集则可以包含这个集合
*/
