#include <bits/stdc++.h>

using namespace std;

namespace program
{
	typedef long long big;
	
	#define int big
	
	const int MAXN = 100000, MAXM = (MAXN << 1) - 2;
	const int MAXC = MAXN << 2, INF = 0x3f3f3f3f3f3f3f3fLL;
	struct input_edge { int u, v, w; } E[MAXM + 10];
	int n, m, q;
	int Fa[MAXN + 10], Dep[MAXN + 10];
	int Siz[MAXN + 10], Son[MAXN + 10];
	int Dfn[MAXN + 10], Top[MAXN + 10];
	int Val[MAXN + 10], Min0[MAXC + 10];
	vector<int> G[MAXN + 10];
	
	void dfs1(int u, int fa)
	{
		Fa[u] = fa;
		Dep[u] = Dep[fa] + 1;
		Siz[u] = 1;
		Son[u] = 0;
		for(vector<int>::iterator p = G[u].begin(); p != G[u].end(); p++)
			if(*p != fa)
			{
				dfs1(*p, u);
				Siz[u] += Siz[*p];
				if(Siz[*p] > Siz[Son[u]])
					Son[u] = *p;
			}
	}
	
	void dfs2(int u, int top)
	{
		static int tot = 0;
		Dfn[u] = ++tot;
		Top[u] = top;
		if(Son[u])
			dfs2(Son[u], top);
		for(vector<int>::iterator p = G[u].begin(); p != G[u].end(); p++)
			if(*p != Fa[u] && *p != Son[u])
				dfs2(*p, *p);
	}
	
	void build(int p, int a, int b, int *Min = Min0)
	{
		if(a < b)
		{
			int mid = (a + b) >> 1;
			build(p << 1, a, mid, Min);
			build(p << 1 | 1, mid + 1, b, Min);
			Min[p] = min(Min[p << 1], Min[p << 1 | 1]);
		}
		else
			Min[p] = Val[a];
	}
	
	void update(int p, int a, int b, int pos, int val, int *Min = Min0)
	{
		if(a < b)
		{
			int mid = (a + b) >> 1;
			if(pos <= mid)
				update(p << 1, a, mid, pos, val, Min);
			else
				update(p << 1 | 1, mid + 1, b, pos, val, Min);
			Min[p] = min(Min[p << 1], Min[p << 1 | 1]);
		}
		else
			Min[p] = val;
	}
	
	int query(int p, int a, int b, int x, int y, int *Min = Min0)
	{
		if(x <= b && a <= y)
		{
			if(x <= a && b <= y)
				return Min[p];
			else
			{
				int mid = (a + b) >> 1;
				return min(query(p << 1, a, mid, x, y, Min),
					query(p << 1 | 1, mid + 1, b, x, y, Min));
			}
		}
		else
			return INF;
	}
	
	inline void to_min(int &x, int y) { if(x > y) x = y; }
	
	void tree_work()
	{
		for(int i = 0; i < m; i++)
		{
			G[E[i].u].push_back(E[i].v);
			G[E[i].v].push_back(E[i].u);
		}
		dfs1(1, 0);
		dfs2(1, 1);
		for(int i = 0; i < m; i++)
		{
			if(E[i].v == Fa[E[i].u])
				swap(E[i].u, E[i].v);
			Val[Dfn[E[i].v]] = E[i].w;
		}
		build(1, 1, n);
		for(int i = 0; i < q; i++)
		{
			int opt;
			scanf("%lld", &opt);
			if(opt == 0)
			{
				int u, v, ans = INF;
				scanf("%lld%lld", &u, &v);
				while(Top[u] != Top[v])
				{
					if(Dep[Top[u]] < Dep[Top[v]])
						swap(u, v);
					to_min(ans, query(1, 1, n, Dfn[Top[u]], Dfn[u]));
					u = Fa[Top[u]];
				}
				if(Dep[u] < Dep[v])
					swap(u, v);
				printf("%lld\n", min(ans, query(1, 1, n, Dfn[v] + 1, Dfn[u])));
			}
			else
			{
				int i, x;
				scanf("%lld%lld", &i, &x);
				update(1, 1, n, Dfn[E[i - 1].v], x);
			}
		}
	}
	
	int cnt = 0, C[MAXN + 10], Bel[MAXN + 10], Pos[MAXN + 10], Min1[MAXN + 10];
	bool Vis[MAXN + 10];
	
	void dfs3(int u, int fa)
	{
		static bool f = 0;
		int tt = 0;
		Vis[u] = 1;
		Fa[u] = fa;
		Dep[u] = Dep[fa] + 1;
		for(vector<int>::iterator p = G[u].begin(); p != G[u].end() && !f; p++)
			if(*p != fa || tt++)
			{
				if(Vis[*p])
				{
					for(int i = u; i != Fa[*p]; i = Fa[i])
						C[Pos[i] = cnt++] = i;
					f = 1;
				}
				else
					dfs3(*p, u);
			}
	}
	
	void dfs4(int u, int b)
	{
		Bel[u] = b;
		for(vector<int>::iterator p = G[u].begin(); p != G[u].end(); p++)
			if(*p != Fa[u])
				dfs4(*p, b);
	}
	
	void one_work()
	{
		for(int i = 0; i < m; i++)
		{
			G[E[i].u].push_back(E[i].v);
			G[E[i].v].push_back(E[i].u);
		}
		memset(Pos + 1, -1, sizeof(int) * n);
		dfs3(1, 0);
		for(int u = 1; u <= n; u++)
			if(Pos[u] != -1)
			{
				for(vector<int>::iterator p = G[u].begin(); p != G[u].end(); p++)
					if(Pos[*p] != -1)
						*p = 0;
				dfs1(u, 0);
				dfs2(u, u);
				dfs4(u, u);
			}
		memset(Val + 1, 0x3f, sizeof(int) * n);
		for(int i = 0; i < m; i++)
			if(Pos[E[i].u] == -1 || Pos[E[i].v] == -1)
			{
				if(E[i].v == Fa[E[i].u])
					swap(E[i].u, E[i].v);
				Val[Dfn[E[i].v]] = E[i].w;
			}
		build(1, 1, n);
		memset(Val, 0x3f, sizeof(int) * n);
		for(int i = 0; i < m; i++)
			if(Pos[E[i].u] != -1 && Pos[E[i].v] != -1)
			{
				if(Val[Pos[E[i].v]] != INF || (Pos[E[i].u] == Pos[E[i].v] + 1 && Val[Pos[E[i].u]] == INF))
					swap(E[i].u, E[i].v);
				Val[Pos[E[i].v]] = E[i].w;
			}
		build(1, 0, cnt - 1, Min1);
		for(int i = 0; i < q; i++)
		{
			int opt;
			scanf("%lld", &opt);
			if(opt == 0)
			{
				int u, v, ans = INF;
				scanf("%lld%lld", &u, &v);
				if(Bel[u] == Bel[v])
				{
					while(Top[u] != Top[v])
					{
						if(Dep[Top[u]] < Dep[Top[v]])
							swap(u, v);
						to_min(ans, query(1, 1, n, Dfn[Top[u]], Dfn[u]));
						u = Fa[Top[u]];
					}
					if(Dep[u] < Dep[v])
						swap(u, v);
					printf("%lld\n", min(ans, query(1, 1, n, Dfn[v] + 1, Dfn[u])));
				}
				else
				{
					if(Pos[Bel[u]] > Pos[Bel[v]])
						swap(u, v);
					ans = query(1, 0, cnt - 1, Pos[Bel[u]] + 1, Pos[Bel[v]], Min1) +
						min(query(1, 0, cnt - 1, 0, Pos[Bel[u]], Min1), query(1, 0, cnt - 1, Pos[Bel[v]] + 1, cnt - 1, Min1));
					while(u)
					{
						to_min(ans, query(1, 1, n, Dfn[Top[u]], Dfn[u]));
						u = Fa[Top[u]];
					}
					while(v)
					{
						to_min(ans, query(1, 1, n, Dfn[Top[v]], Dfn[v]));
						v = Fa[Top[v]];
					}
					printf("%lld\n", ans);
				}
			}
			else
			{
				int i, x;
				scanf("%lld%lld", &i, &x);
				if(Pos[E[i - 1].u] == -1 || Pos[E[i - 1].v] == -1)
					update(1, 1, n, Dfn[E[i - 1].v], x);
				else
					update(1, 0, cnt - 1, Pos[E[i - 1].v], x, Min1);
			}
		}
	}
	
	namespace flow
	{
		int s, t, Head[MAXN + 10], Cur[MAXN + 10], Dis[MAXN + 10], Q[MAXN + 10];
		struct edge { int v, f, next; } E[(MAXM << 1) + 10];
		
		void add_edge(int u, int v, int f)
		{
			static int ec = 0;
			E[ec] = (edge){v, f, Head[u]};
			Head[u] = ec++;
			E[ec] = (edge){u, f, Head[v]};
			Head[v] = ec++;
		}
		
		bool bfs()
		{
			int *head = Q, *tail = Q;
			memset(Dis + 1, -1, sizeof(int) * n);
			Dis[*tail++ = t] = 0;
			while(head < tail)
				for(int u = *head++, p = Head[u]; p != -1; p = E[p].next)
					if(E[p ^ 1].f && Dis[E[p].v] == -1)
						Dis[*tail++ = E[p].v] = Dis[u] + 1;
			return Dis[s] != -1;
		}
		
		int dfs(int u, int f)
		{
			if(u == t)
				return f;
			else
			{
				int tot = 0;
				for(int &p = Cur[u]; p != -1; p = E[p].next)
					if(E[p].f && Dis[u] == Dis[E[p].v] + 1)
					{
						int tmp = dfs(E[p].v, min(E[p].f, f - tot));
						E[p].f -= tmp;
						E[p ^ 1].f += tmp;
						tot += tmp;
						if(tot >= f)
							break;
					}
				return tot;
			}
		}
		
		int dinic()
		{
			int tot = 0;
			while(bfs())
			{
				memcpy(Cur + 1, Head + 1, sizeof(int) * n);
				tot += dfs(s, INF);
			}
			return tot;
		}
		
		void work()
		{
			memset(Head + 1, -1, sizeof(int) * n);
			for(int i = 0; i < m; i++)
				add_edge(program::E[i].u, program::E[i].v, program::E[i].w);
			scanf("%*d%lld%lld", &s, &t);
			printf("%lld\n", dinic());
		}
	}
	
	void work()
	{
		scanf("%lld%lld", &n, &m);
		for(int i = 0; i < m; i++)
			scanf("%lld%lld%lld", &E[i].u, &E[i].v, &E[i].w);
		scanf("%lld", &q);
		if(m == n - 1)
			tree_work();
		else if(n == m)
			one_work();
		else
			flow::work();
	}
	#undef int
}

int main()
{
	freopen("flow.in", "r", stdin);
	freopen("flow.out", "w", stdout);
	program::work();
	return 0;
}
