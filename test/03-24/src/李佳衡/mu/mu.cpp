#include <bits/stdc++.h>

using namespace std;

namespace program
{
	const int MAXN = 1000000000, B = 10000000;
	int pc, cur, Prime[B + 10], Pre[B + 10], Sum[MAXN / B + 10];
	bool IsPrime[B + 10], Vis[MAXN / B + 10];
	
	void init_prime()
	{
		pc = 0;
		memset(IsPrime, 1, sizeof(IsPrime));
		Pre[1] = 1;
		for(int i = 2; i <= B; i++)
		{
			if(IsPrime[i])
			{
				Prime[pc++] = i;
				Pre[i] = -1;
			}
			for(int j = 0; j < pc; j++)
			{
				int t = i * Prime[j];
				if(t > B)
					break;
				IsPrime[t] = 0;
				if(i % Prime[j])
					Pre[t] = -Pre[i];
				else
				{
					Pre[t] = 0;
					break;
				}
			}
			Pre[i] += Pre[i - 1];
		}
	}
	
	int dfs(int i, int t, int s, int n)
	{
		if(s <= n / (Prime[i] * Prime[i]))
			return dfs(i + 1, t, s, n) + dfs(i + 1, t + 1, s * Prime[i] * Prime[i], n);
		else
			return (t & 1) ? -n / s : n / s;
	}
	
	int sum(int n)
	{
		if(n <= B)
			return Pre[n];
		else
		{
			int &tot = Sum[cur / n];
			if(!Vis[cur / n])
			{
				Vis[cur / n] = 1;
				tot = 1;
				for(int i = 2, j; i <= n; i = j + 1)
				{
					j = n / (n / i);
					tot -= sum(n / i) * (j - i + 1);
				}
			}
			return tot;
		}
	}
	
	int calc(int n, int t)
	{
		int s0 = n - dfs(0, 0, 1, n);
		if(t == 0)
			return s0;
		else
		{
			memset(Vis + 1, 0, sizeof(bool) * n / B);
			cur = n;
			if(t == 1)
				return (n - s0 + sum(n)) >> 1;
			else
				return (n - s0 - sum(n)) >> 1;
		}
	}
	
	int find(int t, int k)
	{
		int a = 1, b = MAXN;
		while(a < b)
		{
			int mid = (a + b) >> 1;
			if(calc(mid, t) >= k)
				b = mid;
			else
				a = mid + 1;
		}
		return a;
	}
	
	void work()
	{
		int t, k;
		scanf("%d%d", &t, &k);
		init_prime();
		printf("%d\n", find(t, k));
	}
}

int main()
{
	freopen("mu.in", "r", stdin);
	freopen("mu.out", "w", stdout);
	program::work();
	return 0;
}
