#include <bits/stdc++.h>
typedef long long ll;
typedef double db;
using namespace std;
inline int rd() {
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) if(ch=='-') f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
char s[1000100];
int N;
string ans;
int lowz;
//defs========================
ll get_num(int L,int R);
bool is_zero(int L,int R);
bool abs_1(int L,int R);
void deal_int(int L,int R);
void deal_float(int L,int R);
void deal_mother(int L,int R);
string part(int L,int R);
void deal_int_float(int L,int R);
//main========================

int main() {
	freopen("read.in","r",stdin);
	freopen("read.out","w",stdout);
	scanf("%s",s+1);N=strlen(s+1);
	for(int i=1;i<=N;++i) {
		if(s[i]=='/') {
			int L1=1,R1=i-1,L2=i+1,R2=N;
			if(L1<=R1&&s[L1]=='-') {
				++L1;
				lowz^=1;
			}
			if(L2<=R2&&s[L2]=='-') {
				++L2;
				lowz^=1;
			}
			if(is_zero(L1,R1)) {
				puts("0");
				exit(0);
			} else {
				if(lowz) putchar('F');
				if(!abs_1(L2,R2)) {
					deal_int_float(L2,R2);
					printf("fz");
				} 		
				deal_int_float(L1,R1);
			}
			return 0;
		}
	}
	deal_int_float(1,N);
	return 0;
}
//other functions================================ 
string help(int bb) {
	if(bb==0) return string("");
	if(bb==1) return string("S");
	if(bb==2) return string("B");
	if(bb==3) return string("Q");
	if(bb==4) return string("W");
	if(bb==8) return string("Y");
	return string("");
}
bool is_zero(int L,int R) {
	if(L>R) return true;
	for(int i=L;i<=R;++i) if(isdigit(s[i])){
		if(s[i]!='0') return false;
	}
	return true;
}
bool abs_1(int L,int R) {
	if(L>R) return false;
	while(s[L]=='-') ++L;
	if(L>R) return false;
	int divpos=-1;
	for(int i=L;i<=R;++i) {
		if(s[i]=='.') divpos=i;
	}
	if(divpos!=-1) {
		bool flag_1=1;
		if(L==divpos) flag_1=0;
		else for(int i=divpos-1;i>=L;--i) if(s[i]!='-'){
			if(i==divpos-1&&s[i]!='1') flag_1=false;
			if(i!=divpos-1&&s[i]!='0') flag_1=false;
		}
		return flag_1&&is_zero(divpos+1,R);
	} else {
		bool flag_1=1;
		for(int i=R;i>=L;--i) if(s[i]!='-'){
			if(i==R&&s[i]!='1') flag_1=false;
			if(i!=R&&s[i]!='0') flag_1=false;
		}
		return flag_1;
	}
}
bool is_under_zero(int L,int R) {
	if(is_zero(L,R)) return false;
	for(int i=L;i<=R;++i) if(s[i]=='-') return true;
	return  false;
}
void deal_int(int L,int R) {
	if(L>R) {
		printf("0");return ;
	}
	if(s[L]=='-') {
		printf("F");
		++L;
	}
	while(s[L]=='0'&&L<=R) ++L;
	if(L>R) {
		printf("0"); return ;
	}
	string tmp;
	int pos=R,zr=1;
	for(int i=R,j=0;i>=L;i-=4,j+=4) {
		zr&=is_zero(max(L,i-3),i);
		if(is_zero(max(L,i-3),i)) {
			if(!zr) {
				if(tmp[0]=='0') tmp=tmp;
				else tmp='0'+tmp;				
			}
			else {
				
			}
		} else if(is_zero(i,i)&&tmp.size()&&tmp[0]!='0')tmp=part(max(L,i-3),i)+help(j)+'0'+tmp;
		else
		tmp=part(max(L,i-3),i)+help(j)+tmp;
	}
	printf("%s",tmp.c_str());
}
void deal_float(int L,int R) {
	while(s[R]=='0'&&R>=L) --R;
	for(int i=L;i<=R;++i) putchar(s[i]);
}
string part(int L,int R) {
	bool zr=true;
	string ret;
	for(int i=R;i>=L;--i) {
		zr&=(s[i]=='0');
		if(!zr) {
			if(s[i]!='0') {
				ret=s[i]+help(R-i)+ret;
			} else {
				if(i==L||s[i-1]!='0') {
					ret='0'+ret;
				} 
			} 
		}
	}
	return ret;
}
void deal_int_float(int L,int R) {
	for(int i=L;i<=R;++i) {
		if(s[i]=='.') {
			if(is_zero(L,i-1)&&is_zero(i+1,R)) {
				printf("0\n");
				return;
			} else {
				deal_int(L,i-1);
				if(!is_zero(i+1,R)) {
					putchar('D'); 	
					deal_float(i+1,R);
				}
				return;
			}
		}
	}
	if(is_zero(L,R)) puts("0");
	else {
		deal_int(L,R);
	}	
}
