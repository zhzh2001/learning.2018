#include <bits/stdc++.h>
typedef long long ll;
typedef double db;
using namespace std;
inline int rd() {
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) if(ch=='-') f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
const ll mod=1e9+7;
ll BLOCK;
struct node {
	int x,y,nxt;
}E[1010*1010];
int head[1010*1010],cnt;
int but[1010*1010];
bool vis[1010*1010];
void add(int x,int y,int k) {
	E[++cnt].x=x;E[cnt].y=y,E[cnt].nxt=head[k],head[k]=cnt;
	++but[k];
} 
int N,M,K,a[1010][1010];
ll f[1010][1010],g[1010][1010];
ll h[1010][1010],ht[1010][1010];
int fnd[1010*1010],fcnt;
int prep[101000];
//main========================
int main() {
	freopen("dp.in","r",stdin);
	freopen("dp.out","w",stdout);
	N=rd(),M=rd(),K=rd();BLOCK=sqrt(K);
	for(int i=1;i<=N;++i) {
		for(int j=1;j<=M;++j) {
			a[i][j]=rd();
			add(i,j,a[i][j]);
		}
	}
	for(int i=1;i<=N*M;++i) if(but[i]>=BLOCK) vis[i]=1,fnd[i]=++fcnt;
	f[1][1]=g[1][1]=1;
	if(fnd[a[1][1]]) {
		ht[fnd[a[1][1]]][1]+=1;
		prep[fnd[a[1][1]]]=1;
	}
	//cerr<<fnd[20]<<"   "<<fnd[17]<<endl;
	for(int i=1;i<=N;++i) {
		//cerr<<prep[]
		for(int j=1;j<=M;++j) if(i!=1||j!=1) {
			if(!vis[a[i][j]]) {
				f[i][j]=g[i-1][j-1];
				for(int k=head[a[i][j]];k;k=E[k].nxt) {
					//printf("k=%d (%d,%d)\n",k,i,j);
					if(E[k].x<i&&E[k].y<j)
						f[i][j]=(f[i][j]+mod-f[E[k].x][E[k].y])%mod;
				}

			} else {
				f[i][j]=(g[i-1][j-1]-h[fnd[a[i][j]]][j-1]+mod)%mod;
				(ht[fnd[a[i][j]]][j]+=f[i][j])%=mod;
				if(f[i][j])
					prep[fnd[a[i][j]]]=1;
			}
		//	printf("f[%d][%d]=%lld\n",i,j,f[i][j]);
			g[i][j]=(g[i-1][j]+g[i][j-1]-g[i-1][j-1]+f[i][j]+mod)%mod;
		}
		for(int i=1;i<=fcnt;++i) if(prep[i]) {
			
			for(int j=1;j<=M;++j) {
				(ht[i][j]+=ht[i][j-1])%=mod;
			//	printf("ht[%d][%d]=%lld\n",i,j,ht[i][j]);
				(h[i][j]+=ht[i][j])%=mod;
			//					printf("h[%d][%d]=%lld\n",i,j,h[i][j]);
			}
			for(int j=1;j<=M;++j) ht[i][j]=0;
			prep[i]=0;
		}
	}
	printf("%lld\n",(f[N][M]%mod+mod)%mod);
	return 0;
}
