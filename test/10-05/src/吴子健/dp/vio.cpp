#include <bits/stdc++.h>
typedef long long ll;
typedef double db;
using namespace std;
inline int rd() {
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) if(ch=='-') f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
const ll mod=1e9+7;
ll BLOCK;
struct node {
	int x,y,nxt;
}E[1010*1010];
int head[1010*1010],cnt;
int but[1010*1010];
bool vis[1010*1010];
void add(int x,int y,int k) {
	E[++cnt].x=x;E[cnt].y=y,E[cnt].nxt=head[k],head[k]=cnt;
	++but[k];
} 
int N,M,K,a[1010][1010];
ll f[1010][1010],g[1010][1010];
ll h[1010][1010],ht[1010][1010];
int fnd[1010*1010],fcnt;
int prep[1010];
//main========================
int main() {
	freopen("1.in","r",stdin);
	N=rd(),M=rd(),K=rd();BLOCK=sqrt(K);
	for(int i=1;i<=N;++i) {
		for(int j=1;j<=M;++j) {
			a[i][j]=rd();
			add(i,j,a[i][j]);
		}
	}
	for(int i=1;i<=N*M;++i) if(but[i]>=BLOCK) vis[i]=1,fnd[i]=++fcnt;
	f[1][1]=1;
	for(int i=1;i<=N;++i) {
		for(int j=1;j<=M;++j) if(i!=1||j!=1) {
			for(int k=1;k<i;++k) {
				for(int l=1;l<j;++l) {
					if(a[i][j]!=a[k][l]) f[i][j]=(f[i][j]+f[k][l])%mod;
				}
			}
		//	printf("f[%d][%d]=%lld\n",i,j,f[i][j]);
		}
	}
	printf("%lld\n",f[N][M]);
	return 0;
}
