#pragma GCC optimize 2
#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <fstream>
using namespace std;

ifstream fin("read.in");
ofstream fout("read.out");

typedef long long ll;
typedef pair<int, int> pii;
const int kMaxL = 20005, kInf = 0x3f3f3f3f;
string buf;
int n;

inline int QueryFlag(int pos) {
  if (buf[pos] == '-') return -1;
  else return 1;
}
inline pair<int, int> TrimTailZero(int l, int r) {
  bool tail = true;
  for (int i = r; i >= l; --i) {
    tail &= buf[i] == '0';
    if (!tail)
      return make_pair(l, i);
  }
  return make_pair(l, -1);
}
inline pair<int, int> TrimLeadZero(int l, int r) {
  bool lead = true;
  for (int i = l; i <= r; ++i) {
    lead &= (buf[i] == '0' || buf[i] == '+' || buf[i] == '-');
    if (!lead)
      return make_pair(i, r);
  }
  return make_pair(kInf, r);
}
const char sp[] = " SBQ";
string SolveSubInteger(int l, int r, bool get_lead) {
  ll x = atoi(buf.substr(l, r - l + 1).c_str());
  string ret("");
  if (x < 10)
    ret = ret + (get_lead ? "0" : "") + buf[r];
  else {
    // int c1 = x / 1000 % 10, c2 = x / 100 % 10,
      // c3 = x / 10 % 10, c4 = x % 10;
    // bool fl = false;
    for (int i = l; i < r; ++i) {
      if (buf[i] != '0')
        ret = ret + buf[i] + sp[r - i];
    }
    if (buf[r] != '0') {
      if (r - 1 >= l && buf[r - 1] == '0')
        ret += "0";
      ret += buf[r];
    }
  }
  return ret;
}
inline string GetOnes(int cnt) {
  if (cnt < 0)
    abort();
  switch (cnt) {
    case 0:
      return "";
    case 1:
      return "W";
    case 2:
      return "Y";
  }
  return "swwind is akking";
}
inline pair<string, ll> SolveInteger(int l, int r) {
  int fg = (r - l + 1) / 4;
  int low = r - fg * 4;
  string ret;
  bool fl = true;
  if (low >= l) {
    ret += SolveSubInteger(l, low, false) + GetOnes(fg);
    fl = false;
  }
  for (int i = low + 1; i <= r; i += 4) {
    if (fl) {
      ret += SolveSubInteger(i, i + 3, false) + GetOnes(--fg);
      fl = false;
    } else ret += SolveSubInteger(i, i + 3, true) + GetOnes(--fg);
  }
  ll x = atoi(buf.substr(l, r - l + 1).c_str());
  return make_pair(ret, x);
}
inline pair<string, ll> TrimInteger(int l, int r) {
  pair<int, int> np(TrimLeadZero(l, r));
  if (np.second < np.first)
    return make_pair("0", 0);
  else {
    return SolveInteger(np.first, np.second);
  }
}
/*
 * @return 0 if fraction is zero
 */
inline pair<string, ll> TrimFrac(int l, int r) {
  pair<int, int> np(TrimTailZero(l, r));
  if (np.second < np.first)
    return make_pair("0", 0);
  else
    return make_pair(buf.substr(np.first, np.second - np.first + 1), -1);
}
/*
 * @return (str, 0) if value equals 0
 *         (str, 1) if value equals 1
 *         otherwise, (str, -1)
 */
pair<string, ll> TrimReal(int l, int r) {
  int pos = -1;
  for (int i = l; i <= r; ++i) {
    if (buf[i] == '.') {
      pos = i;
      break;
    }
  }
  if (pos == -1) {
    return TrimInteger(l, r);
  } else if (pos == l) {
    pair<string, ll> p1 = TrimFrac(l + 1, r);
    if (p1.second != 0)
      return make_pair("0D" + p1.first, -1);
    else if (p1.second == 0)
      return make_pair("0", 0);
  } else if (pos == r) {
    return TrimInteger(l, r - 1);
  } else {
    pair<string, ll> p1 = TrimInteger(l, pos - 1);
    pair<string, ll> p2 = TrimFrac(pos + 1, r);
    // printf("%d\n", l);
    if (p2.second == 0) {
      return make_pair(p1.first, p1.second);
    } else {
      return make_pair(p1.first + "D" + p2.first, -1);
    }
  }
  return make_pair("swwind is akking", -1);
}
string TrimFZ(int l, int r) { // base on the only one "fz"
  string ret;
  int pos = -1;
  for (int i = r - 1; i > l; --i) {
    if (buf[i] == '/') {
      pos = i;
      break;
    }
  }
  if (~pos) {
    int flag = QueryFlag(l) * QueryFlag(pos + 1);
    pair<string, ll> f1(TrimReal(l, pos - 1));
    pair<string, ll> f2(TrimReal(pos + 1, r));
    if (f1.second == 0) {
      ret += "0";
    } else if (f2.second == 1) {
      ret += (flag < 0 ? "F" : "") + f1.first;
    } else {
      ret += (flag < 0 ? "F" : "") + f2.first + "fz" + f1.first;
    }
    return ret;
  } else {
    int flag = QueryFlag(l);
    return (flag < 0 ? "F" : "") + TrimReal(l, r).first;
  }
}
int main() {
  fin >> buf;
  fout << TrimFZ(0, buf.length() - 1) << endl;
  return 0;
}
