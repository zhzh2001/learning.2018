#pragma GCC optimize 2
#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <vector>
using namespace std;

namespace IO {
static const int kMegaBytes = 1 << 20;
static char buf[kMegaBytes], *S = buf, *T = buf;
inline char NextChar() {
  if (S == T) {
    T = (S = buf) + fread(buf, sizeof(char), sizeof(buf), stdin);
    if (S == T)
      return EOF;
  }
  return *S++;
}
template <typename Int> inline void Read(Int &x) {
  x = 0;
  Int flag = 1;
  char ch;
  for (ch = NextChar(); isspace(ch); ch = NextChar())
    ;
  if (ch == '-')
    flag = -1, ch = NextChar();
  for (; isdigit(ch); ch = NextChar())
    x = x * 10 + ch - '0';
  x *= flag;
}
} // namespace IO
using IO::Read;

typedef long long ll;
typedef pair<int, int> pii;

const int kMaxN = 1005, kMod = 1e9 + 7;
int n, m, k, a[kMaxN][kMaxN];

namespace SubTask1 {
const int kSubMaxN = 105;
int f[kSubMaxN][kSubMaxN];
int Solve() {
  for (int i = 1; i <= n; ++i)
    for (int j = 1; j <= m; ++j) {
      Read(a[i][j]);
    }
  f[1][1] = 1;
  for (int i = 1; i < n; ++i) {
    for (int j = 1; j < m; ++j) {
      for (int pi = i + 1; pi <= n; ++pi)
        for (int pj = j + 1; pj <= m; ++pj)
          if (a[pi][pj] != a[i][j])
            f[pi][pj] = (1LL * f[pi][pj] + 1LL * f[i][j]) % kMod;
    }
  }
  printf("%d\n", f[n][m]);
  return 0;
}
} // namespace SubTask1

namespace Solution {
const int kMaxK = 1e6 + 5, kSubMaxN = kMaxN;
ll f[kSubMaxN][kSubMaxN], g[kSubMaxN][kSubMaxN];
vector<pii> pa[kMaxK];
int Solve() {
  for (int i = 1; i <= n; ++i)
    for (int j = 1; j <= m; ++j) {
      Read(a[i][j]);
      pa[a[i][j]].push_back(make_pair(i, j));
    }
  for (int i = 0; i < kMaxK; ++i) {
    if (pa[i].size())
      sort(pa[i].begin(), pa[i].end());
  }
  f[1][1] = 1;
  g[1][1] = 1;
  for (int i = 1; i <= m; ++i)
    g[1][i] = g[1][i - 1] + g[1][i];
  for (int i = 1; i <= n; ++i)
    g[i][1] = g[i - 1][1] + g[i][1];
  for (int i = 2; i <= n; ++i)
    for (int j = 2; j <= m; ++j) {
      int c = a[i][j];
      f[i][j] = g[i - 1][j - 1];
      for (int k = 0; k < pa[c].size(); ++k) {
        const pii &now = pa[c][k];
        if (now.first < i && now.second < j)
          f[i][j] -= f[now.first][now.second];
      }
      g[i][j] = (f[i][j] + g[i - 1][j] + g[i][j - 1] - g[i - 1][j - 1]) % kMod;
    }
  printf("%d\n", (f[n][m] % kMod + kMod) % kMod);
  return 0;
}
} // namespace Solution

int main() {
  freopen("dp.in", "r", stdin);
  freopen("dp.out", "w", stdout);
  Read(n), Read(m), Read(k);
  if (n <= 100 && m <= 100)
    return SubTask1::Solve();
  return Solution::Solve();
}