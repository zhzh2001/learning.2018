#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
using namespace std;

namespace IO {
static const int kMegaBytes = 1 << 20;
static char buf[kMegaBytes], *S = buf, *T = buf;
inline char NextChar() {
  if (S == T) {
    T = (S = buf) + fread(buf, sizeof(char), sizeof(buf), stdin);
    if (S == T)
      return EOF;
  }
  return *S++;
}
template <typename Int> inline void Read(Int &x) {
  x = 0;
  Int flag = 1;
  char ch;
  for (ch = NextChar(); isspace(ch); ch = NextChar())
    ;
  if (ch == '-')
    flag = -1, ch = NextChar();
  for (; isdigit(ch); ch = NextChar())
    x = x * 10 + ch - '0';
  x *= flag;
}
} // namespace IO
using IO::Read;

typedef long long ll;
typedef pair<int, int> pii;

int cnt[2];

int main() {
  freopen("pubg.in", "r", stdin);
  freopen("pubg.out", "w", stdout);
  ll x1, y1, x2, y2;
  while (~scanf("%lld%lld%lld%lld", &x1, &y1, &x2, &y2)) {
    ll dx = abs(x2 - x1), dy = abs(y2 - y1);
    if (__gcd(dx, dy) == 1) {
      puts("YE5");
      ++cnt[0];
    } else {
      puts("NO");
      ++cnt[1];
    }
  }
  if (cnt[0] == cnt[1])
    puts("Friend Ship.");
  else if (cnt[0] > cnt[1])
    puts("Poor xshen!");
  else puts("Yahoo!");
  return 0;
}