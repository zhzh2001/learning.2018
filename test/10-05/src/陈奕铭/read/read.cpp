#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-') f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 10005,M = 20005;
char s[M];
int lens;
bool flagy1,flagx1;
bool flagx2,flagy2;
bool flagf;
struct node{
	char a[M];
	int len;
	node(){
		memset(a,0,sizeof a);
		len = 0;
	}
}x1,y1,x2,y2,ansx1,ansy1,ansx2,ansy2;

inline void solvex1(){
	bool flag = false;
	if(x1.a[1] == '-'){flag = true; x1.a[1] = '0';}
	int i = 1,top = x1.len+1;
	for(i = 1;i <= x1.len;++i)
		if(x1.a[i] != '0'){
			top = i;
			break;
		}
	if(top == x1.len+1){	//整数是0
		ansx1.a[1] = '0';
		ansx1.a[2] = 0;
		flagx1 = true;
		return;
	}
	if(flag) ansx1.a[++ansx1.len] = 'F';
	flag = false;		//是否是连续的一段0
	for(i = top;i <= x1.len;++i){
		if(x1.a[i] != '0'){	//	不是0
			ansx1.a[++ansx1.len] = x1.a[i];
			int x = (x1.len-i)%4;
			int y = (x1.len-i)/4;
			if(x == 1) ansx1.a[++ansx1.len] = 'S';
			else if(x == 2) ansx1.a[++ansx1.len] = 'B';
			else if(x == 3) ansx1.a[++ansx1.len] = 'Q';
			if(y > 0){
				bool flap = false;
				for(int j = i+1;j <= i+x;++j)
					if(x1.a[j] != '0'){
						flap = true;
						break;
					}
				if(!flap){
					if(y == 1) ansx1.a[++ansx1.len] = 'W';
					else if(y == 2) ansx1.a[++ansx1.len] = 'Y';
				}
			}
			flag = false;
		}else{
			if(flag) continue;
			else{
				ansx1.a[++ansx1.len] = '0';
				flag = true;
			}
		}
	}
	if(flag){	//末尾有0则删之
		ansx1.a[ansx1.len] = 0;
	}
	ansx1.a[ansx1.len+1] = 0;
}


inline void solvex2(){
	bool flag = false;
	if(x2.a[1] == '-'){flag = true; x2.a[1] = '0';}
	int i = 1,top = x2.len+1;
	for(i = 1;i <= x2.len;++i)
		if(x2.a[i] != '0'){
			top = i;
			break;
		}
	if(top == x2.len+1){	//整数是0
		ansx2.a[1] = '0';
		ansx2.a[2] = 0;
		return;
	}
	if(flag) ansx2.a[++ansx2.len] = 'F';
	flag = false;		//是否是连续的一段0
	for(i = top;i <= x2.len;++i){
		if(x2.a[i] != '0'){	//	不是0
			ansx2.a[++ansx2.len] = x2.a[i];
			int x = (x2.len-i)%4;
			int y = (x2.len-i)/4;
			if(x == 1) ansx2.a[++ansx2.len] = 'S';
			else if(x == 2) ansx2.a[++ansx2.len] = 'B';
			else if(x == 3) ansx2.a[++ansx2.len] = 'Q';
			if(y > 0){
				bool flap = false;
				for(int j = i+1;j <= i+x;++j)
					if(x2.a[j] != '0'){
						flap = true;
						break;
					}
				if(!flap){
					if(y == 1) ansx2.a[++ansx2.len] = 'W';
					else if(y == 2) ansx2.a[++ansx2.len] = 'Y';
				}
			}
			flag = false;
		}else{
			if(flag) continue;
			else{
				ansx2.a[++ansx2.len] = '0';
				flag = true;
			}
		}
	}
	if(flag){	//末尾有0则删之
		ansx2.a[ansx2.len] = 0;
	}
	ansx2.a[ansx2.len+1] = 0;
	if(ansx2.a[1] == '1' && ansx2.a[2] == 0){//判断是不是1
		flagx2 = true;
	}
}

inline void solvey1(){
	int i = y1.len; y1.len = 0;
	for(;i >= 1;--i)
		if(y1.a[i] != '0'){
			y1.len = i;
			break;
		}
	if(y1.len == 0){
		ansy1.a[1] = 0;	//没有小数部分
		flagy1 = true;
		return;
	}
	ansy1.a[++ansy1.len] = 'D';
	for(i = 1;i <= y1.len;++i)
		ansy1.a[++ansy1.len] = y1.a[i];
	ansy1.a[ansy1.len+1] = 0;
	return;
}

inline void solvey2(){
	int i = y2.len; y2.len = 0;
	for(;i >= 1;--i)
		if(y2.a[i] != '0'){
			y2.len = i;
			break;
		}
	if(y2.len == 0){
		ansy2.a[1] = 0;	//没有小数部分
		flagy2 = true;
		return;
	}
	ansy2.a[++ansy2.len] = 'D';
	for(i = 1;i <= y2.len;++i)
		ansy2.a[++ansy2.len] = y2.a[i];
	ansy2.a[ansy2.len+1] = 0;
	return;
}


signed main(){
	freopen("read.in","r",stdin);
	freopen("read.out","w",stdout);
	scanf("%s",s+1); lens = strlen(s+1);
	int p = 1;
	while(p <= lens && s[p] != '.' && s[p] != '/'){
		x1.a[++x1.len] = s[p++];
	}
	if(p > lens){
		solvex1();
		printf("%s",ansx1.a+1);
		puts("");
		return 0;
	}
	if(s[p] == '.'){
		++p;
		while(p <= lens && s[p] != '/') y1.a[++y1.len] = s[p++];
		if(p > lens){
			solvex1();
			solvey1();
			printf("%s",ansx1.a+1);
			printf("%s\n",ansy1.a+1);
			return 0;
		}
	}
	if(s[p] == '/'){
		++p;
		while(p <= lens && s[p] != '.'){
			x2.a[++x2.len] = s[p++];
		}
		if(x1.a[1] == '-' && x2.a[1] != '-'){
			flagf = true;
			x1.a[1] = '0';
		}else if(x1.a[1] != '-' && x2.a[1] == '-'){
			flagf =  true;
			x2.a[1] = '0';
		}else if(x1.a[1] == '-' && x2.a[1] == '-'){x1.a[1] = x2.a[1] = '0';}
		solvex1();
		solvey1();
		if(flagx1 && flagy1) {puts("0"); return 0;}	//分子是0
		++p;
		while(p <= lens) y2.a[++y2.len] = s[p++];
		solvex2();
		solvey2();
		if(flagx2 && flagy2){ //分母是1
			if(flagf) putchar('-');
			printf("%s",ansx1.a+1);
			printf("%s",ansy1.a+1);
			return 0;
		}else{
			if(flagf) putchar('-');
			printf("%s",ansx2.a+1);
			printf("%s",ansy2.a+1);
			printf("fz");
			printf("%s",ansx1.a+1);
			printf("%s\n",ansy1.a+1);
			return 0;
		}
	}
	return 0;
}