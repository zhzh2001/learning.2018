#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-') f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

#define lowbit(x) ((x)&(-x))
const int N = 1005,mod = 1e9+7;
const int M = 1000005;
int n,m,k;
int c[M];
int C[M];
int L[M],R[M];	//每个数字的开头
int mp[N][N];
int a[M],cnt,d[M];
int dd[M],b[M],cc[M];
bool vis[M];
int dp[N][N];

inline int erfen(int v){
	int l = 1,r = cnt,mid;
	while(l <= r){
		mid = (l+r)>>1;
		if(d[mid] == v) return mid;
		if(d[mid] < v) l = mid+1;
		else r = mid-1;
	}
	return l;
}
inline int Erfen(int k,int v){
	int l = L[k],r = R[k],mid;
	while(l <= r){
		mid = (l+r)>>1;
		if(dd[mid] == v) return mid;
		if(dd[mid] < v) l = mid+1;
		else r = mid-1;
	}
	return l;
}

inline void updateC(int x,int v){
	for(;x <= m;x += lowbit(x))
		C[x] = (C[x]+v)%mod;
}
inline void update(int k,int x,int v){
	int p = Erfen(k,x); p = p-L[k]+1;
	int num = R[k]-L[k]+1;
	for(;p <= num;p += lowbit(p))
		c[L[k]+p-1] = (c[L[k]+p-1]+v)%mod;
}
inline int getsumC(int x){
	int ans = 0;
	for(;x;x -= lowbit(x))
		ans = (ans+C[x])%mod;
	return ans;
}
inline int getsum(int k,int x){
	int p = Erfen(k,x); p = p-L[k];
	int ans = 0;
	for(;p;p -= lowbit(p))
		ans = (c[L[k]+p-1]+ans)%mod;
	return ans;
}

signed main(){
	freopen("dp.in","r",stdin);
	freopen("dp.out","w",stdout);
	n = read(); m = read(); k = read();
	for(int i = 1;i <= n;++i){
		for(int j = 1;j <= m;++j){
			mp[i][j] = read();
			if(i == 1 && j == 1) ++a[mp[i][j]];
			else if(i != 1 && j != 1) ++a[mp[i][j]];
		}
	}
	for(int i = 1;i <= k;++i)
		if(a[i] > 1) d[++cnt] = i;
		else vis[i] = true;

	if(vis[mp[1][1]]){
		mp[1][1] = 0;
	}else{
		int p = erfen(mp[1][1]);
		mp[1][1] = p;
		++b[p];
	}		
	
	for(int j = 2;j <= m;++j)
		for(int i = 2;i <= n;++i){
			if(vis[mp[i][j]]){
				mp[i][j] = 0;
				continue;
			}
			int p = erfen(mp[i][j]);
			mp[i][j] = p;
			++b[p];
		}

	for(int i = 1;i <= cnt;++i) b[i] += b[i-1];

	if(mp[1][1] != 0){
		int p = mp[1][1];
		dd[b[p-1]+cc[p]+1] = 1;			//cc存的是这个数j的个数
		++cc[p];
	}

	for(int j = 2;j <= m;++j)
		for(int i = 2;i <= n;++i){
			if(mp[i][j] == 0) continue;
			int p = mp[i][j];
			if(cc[p] > 0 && dd[b[p-1]+cc[p]] == j) continue;	//如果上次已经是，就没必要重复加入
			dd[b[p-1]+cc[p]+1] = j;			//cc存的是这个数j的个数
			++cc[p];
		}
	int p = 0;
	for(int i = 1;i <= cnt;++i){
		L[i] = p+1;		//这些数的开头
		for(int j = 1;j <= cc[i];++j){
			dd[++p] = dd[b[i-1]+j];
		}
		R[i] = p;
	}
	dp[1][1] = 1;
	updateC(1,1);
	if(mp[1][1] != 0)
		update(mp[1][1],1,1);
	for(int i = 2;i <= n;++i){
		if(i != 2){
			for(int j = 2;j <= m;++j){
				updateC(j,dp[i-1][j]);
				if(mp[i-1][j] != 0) update(mp[i-1][j],j,dp[i-1][j]);
			}
		}
		for(int j = 2;j <= m;++j){
			if(mp[i][j] == 0){
				dp[i][j] = getsumC(j-1);
			}else{
				int sum1 = getsumC(j-1);
				int sum2 = getsum(mp[i][j],j);
				dp[i][j] = ((sum1-sum2)%mod+mod)%mod;				
			}
		}
	}
	printf("%d\n",(dp[n][m]%mod+mod)%mod);
	return 0;
}