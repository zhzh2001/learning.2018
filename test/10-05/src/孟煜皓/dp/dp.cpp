#include <cstdio>
#include <cctype>
int read(){
	register int x = 0;
	register char f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = !f;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
#define N 1005
#define P 1000000007
int n, m, k, a[N][N];
long long dp[N][N];
int main(){
	freopen("dp.in", "r", stdin);
	freopen("dp.out", "w", stdout);
	n = read(), m = read(), k = read();
	for (register int i = 1; i <= n; ++i)
		for (register int j = 1; j <= m; ++j)
			a[i][j] = read();
	dp[n][n] = 1;
	for (register int i = n - 1; i; --i)
		for (register int j = n - 1; j; --j){
			for (register int x = i + 1; x <= n; ++x)
				for (register int y = j + 1; y <= m; ++y)
					if (a[i][j] != a[x][y]) dp[i][j] += dp[x][y];
			dp[i][j] %= P;
		}
	printf("%lld", dp[1][1]);
}