#include <cstdio>
#include <cctype>
unsigned long long read(){
	register unsigned long long x = 0;
	register char f = 1, ch = getchar();
	for (; !isdigit(ch) && ch != EOF; ch = getchar()) if (ch == '-') f = !f;
	if (ch == EOF) return -1;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
long long x1, y1, x2, y2;
int s1, s2;
unsigned long long gcd(unsigned long long a, unsigned long long b){
	return b ? gcd(b, a % b) : a;
}
int main(){
	freopen("pubg.in", "r", stdin);
	freopen("pubg.out", "w", stdout);
	while (~(x1 = read(), y1 = read(), x2 = read(), y2 = read())){
		unsigned long long dx = x1 > x2 ? x1 - x2 : x2 - x1;
		unsigned long long dy = y1 > y2 ? y1 - y2 : y2 - y1;
		// printf("%llu %llu\n", dx, dy);
		if (gcd(dx, dy) <= 1) printf("YE5\n"), ++s1; else printf("NO\n"), ++s2;
	}
	if (s1 > s2) printf("Poor xshen!");
	else if (s1 == s2) printf("Friend Ship.");
	else printf("Yahoo!");
	return 0;
}