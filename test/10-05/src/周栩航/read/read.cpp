#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define pb push_back


using namespace std;

inline ll read()
{
	ll t=0,dp=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	dp=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-48,c=getchar();
	return t*dp;
}
inline void write(ll x){if(x<0)	{putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+48);}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
/*------------------------------------------------------------------------------------------------------*/

const int N=30000000;
char s[N];
int n,tag;
inline bool Out_zs(int l,int r,int zero)
{	
	if(l>r)	return 0;
	int len=r-l+1;
	bool hav_y=0,hav_w=0;
	if(l<=r-8)
	{
		Out_zs(l,r-8,0);
		putchar('Y');
		hav_y=1;
	}
	bool p_zero=0;
	if(l<=r-4)
	{
		bool ned=Out_zs(max(l,r-7),r-4,hav_y);
		if(ned)	putchar('W');
		hav_w=1;
	}
	bool ret=0,can=hav_w;
	For(i,max(l,r-3),r)
	{
		if(s[i]!='0')
		{
			if(s[i-1]=='0'&&can)	putchar('0'),p_zero=0;
			putchar(s[i]);ret=1;can=1;
			if(i==r-1)	putchar('S');else	if(i==r-2)	putchar('B');else	if(i==r-3)	putchar('Q');
		}
	
	}
	return ret;
}
inline void Out_xs(int l,int r)
{
	tag=r+1;
	For(i,l,r)	if(s[i]=='.')	tag=i;
	int zs=l;
	int zero=0;
	For(i,l,r)	if(s[i]!='-'&&s[i]!='.'&&s[i]!='0')	zero=1;
	zero^=1;
	if(zero)
	{
		puts("0");return;
	}
	if(s[zs]=='-')	zs++,putchar('F');
	while(s[zs]=='0'&&zs<tag)	zs++;
	Out_zs(zs,tag-1,0);
	int xs=r;
	while(xs>tag)
		if(s[xs]=='0')xs--;else	break;
	if(tag<=zs)	putchar('0');	
	if(xs>tag)	
	{
		putchar('D');
		For(i,tag+1,xs)	putchar(s[i]);
	}
}
inline void Out_fs(int mid)
{
	bool f=0;
	if(s[1]=='-')	f^=1;if(s[mid+1]=='-')	f^=1;
	bool no_fm=0;
	int l=mid+1;if(s[l]=='-')	l++;
	while(s[l]=='0')	l++;
	if(l==n&&s[n]=='1')	no_fm=1;
	int tl=1;if(s[tl]=='-')	tl++;
	while(s[tl]=='0'&&tl<=mid-1)	tl++;
	if(tl>mid-1)	{putchar('0');return;}
	
	bool fm=0;
	
	int tag=n+1;
	For(i,mid+1,n)	if(s[i]=='.')	tag=i;
	For(j,tag+1,n)	if(s[j]!='0')	fm=1;
	int ttl=mid+1;if(s[ttl]=='-')	ttl++;
	if(s[ttl]!='1')	fm=1;if(tag-ttl!=1)	fm=1;
	
	int zero=0;
	For(i,1,mid-1)	if(s[i]!='-'&&s[i]!='.'&&s[i]!='0')	zero=1;
	zero^=1;
	
	if(zero)
	{
		puts("0");return;
	}
	if(f)	putchar('F');
	if(fm)	
	{
		Out_xs(l,n);putchar('f'),putchar('z');
	}	
	Out_xs(tl,mid-1);
}
int main()
{
	freopen("read.in","r",stdin);
	freopen("read.out","w",stdout);
	scanf("%s",s+1);
	n=strlen(s+1);tag=n+1;
	For(i,1,n)	if(s[i]=='/')	{Out_fs(i);return 0;}
	Out_xs(1,n);
}
