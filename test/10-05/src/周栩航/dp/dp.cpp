#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define pb push_back


using namespace std;

inline ll read()
{
	ll t=0,dp=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	dp=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-48,c=getchar();
	return t*dp;
}
inline void write(ll x){if(x<0)	{putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+48);}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
/*------------------------------------------------------------------------------------------------------*/
const int N=1005,mo=1e9+7;
int n,m,k,a[N][N],dp[N][N],rt[N*N],tr[N*N*41],tot,lson[N*N*41],rson[N*N*41];
inline void Insert(int &x,int l,int r,int to,int val)
{
	if(!x)	x=++tot;
	if(l==r){tr[x]+=val;tr[x]%=mo;return;}
	int mid=l+r>>1;
	if(to<=mid)	Insert(lson[x],l,mid,to,val);	else	Insert(rson[x],mid+1,r,to,val);
	tr[x]=tr[lson[x]]+tr[rson[x]];tr[x]%=mo;
}
inline ll Ask(int x,int l,int r,int ql,int qr)
{
	if(!x)	return 0;
	if(ql<=l&&r<=qr)	return tr[x];
	int mid=l+r>>1,tmp=0;
	if(ql<=mid)	tmp=Ask(lson[x],l,mid,ql,qr);
	if(qr>mid)	tmp=(tmp+Ask(rson[x],mid+1,r,ql,qr))%mo;
	return tmp;
}
int main()
{
	freopen("dp.in","r",stdin);
	freopen("dp.out","w",stdout);
	n=read();m=read();k=read();
	For(i,1,n)	
		For(j,1,m)	a[i][j]=read();
	Insert(rt[0],1,m,1,1);
	Insert(rt[a[1][1]],1,m,1,1);
	For(i,2,n)
	{
		For(j,2,m-(i!=n))
			dp[i][j]=((Ask(rt[0],1,m,1,j-1)-Ask(rt[a[i][j]],1,m,1,j-1))%mo+mo)%mo;
		if(i!=n)	For(j,2,m)	Insert(rt[a[i][j]],1,m,j,dp[i][j]),Insert(rt[0],1,m,j,dp[i][j]);
	}
	writeln(dp[n][m]);
}
/*
4 4 4
1 1 1 1 
1 3 2 1 
1 2 4 1 
1 1 1 1
*/
