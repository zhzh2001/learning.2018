#include<bits/stdc++.h>
#define ll long long
using namespace std;
ll n,m,k;
const int N=1005;
ll a[N][N],f[N][N];
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const ll mo=1e9+7;
int main()
{
	freopen("dp.in","r",stdin);
	freopen("dp.out","w",stdout);
	n=read();m=read();k=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			a[i][j]=read();
	f[1][1]=1;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			for(int k=i+1;k<=n;k++)
				for(int l=j+1;l<=m;l++)
					if(a[i][j]!=a[k][l]){
						f[k][l]+=f[i][j];
						f[k][l]%=mo;
					}
	writeln(f[n][m]);
	return 0;
}
