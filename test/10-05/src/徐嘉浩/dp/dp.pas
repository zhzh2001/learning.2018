var i,j:longint; n,m,k:int64;
    a,f:array[0..1000,0..1000]of int64;
function dfs(x,y:int64):int64;
var i,j:longint;
begin
 if f[x,y]>0 then exit(f[x,y]);
 for i:=x+1 to n-1 do
  for j:=y+1 to m-1 do
   if a[x,y]<>a[i,j] then f[x,y]:=(f[x,y]+dfs(i,j))mod 1000000007;
 if a[x,y]<>a[n,m] then f[x,y]:=(f[x,y]+1)mod 1000000007;
 exit(f[x,y]);
end;
begin
 assign(input,'dp.in');
 assign(output,'dp.out');
 reset(input);
 rewrite(output);
 read(n,m,k);
 for i:=1 to n do
  for j:=1 to m do read(a[i,j]);
 writeln(dfs(1,1));
 close(input);
 close(output);
end.