#include<bits/stdc++.h>
using namespace std;
const int N=100005;
char s[2][N],c[N],xs[N];
int len[2],f;
inline void get(int x,int k)
{
	int qw,bw,sw,gw;
	gw=x%10;
	sw=x/10%10;
	bw=x/100%10;
	qw=x/1000;
	if (qw!=0)
	{
		s[k][++len[k]]=qw+'0';
		s[k][++len[k]]='Q';
	}
	if (bw!=0)
	{
		s[k][++len[k]]=bw+'0';
		s[k][++len[k]]='B';
	}
	if (qw!=0&&bw==0&&sw!=0)
		s[k][++len[k]]='0';
	if (sw!=0)
	{
		s[k][++len[k]]=sw+'0';
		s[k][++len[k]]='S';
	}
	if ((sw==0&&gw!=0)&&(bw!=0||(bw==0&&qw!=0)))
		s[k][++len[k]]='0';
	if (gw!=0)
		s[k][++len[k]]=gw+'0';
}
inline void work(int l,int r,int k)
{
	if (c[l]=='-')
	{
		++l;
		f=-f;
	}
	int zs=0,cnt=0;
	for (int i=l;i<=r;++i)
		if (c[i]!='.')
			zs=zs*10+c[i]-'0';
		else
		{
			for (int j=i+1;j<=r;++j)
				xs[++cnt]=c[j]-'0';
			break;
		}
	if (zs!=0)
	{
		int yw=zs/(int)1e8,ww=zs/10000%10000,gw=zs%10000;
		if (yw!=0)
		{
			get(yw,k);
			s[k][++len[k]]='Y';
			if (ww!=0)
			{
				if (ww<1000)
					s[k][++len[k]]='0';
				get(ww,k);
				s[k][++len[k]]='W';
			}
			if (gw!=0)
			{
				if (gw<1000)
					s[k][++len[k]]='0';
				get(gw,k);
			}
		}
		else
		{
			if (ww!=0)
			{
				get(ww,k);
				s[k][++len[k]]='W';
				if (gw!=0)
				{
					if (gw<1000)
						s[k][++len[k]]='0';
					get(gw,k);
				}
			}
			else
				get(gw,k);
		}
	}
	else
		s[k][++len[k]]='0';
	xs[0]=1;
	while (xs[cnt]==0)
		--cnt;
	if (cnt!=0)
	{
		s[k][++len[k]]='D';
		for (int i=1;i<=cnt;++i)
			s[k][++len[k]]=xs[i]+'0';
	}
}
int main()
{
	freopen("read.in","r",stdin);
	freopen("read.out","w",stdout);
	scanf("%s",c+1);
	int l=strlen(c+1);
	f=1;
	for (int i=2;i<l;++i)
	if (c[i]=='/')
		{
			work(1,i-1,0);
			work(i+1,l,1);
			if (len[0]==1&&s[0][1]=='0')
			{
				putchar('0');
				return 0;
			}
			if (len[1]==1&&s[1][1]=='1')
			{
				if (f==-1)
					putchar('F');
				for (int i=1;i<=len[0];++i)
					putchar(s[0][i]);
				return 0;
			}
			if (f==-1)
				putchar('F');
			for (int i=1;i<=len[1];++i)
				putchar(s[1][i]);
			putchar('f');
			putchar('z');
			for (int i=1;i<=len[0];++i)
				putchar(s[0][i]);
			return 0;
		}
	work(1,l,0);
	if (len[0]==1&&s[0][1]=='0')
	{
		putchar('0');
		return 0;
	}
	if (f==-1)
		putchar('F');
	for (int i=1;i<=len[0];++i)
		putchar(s[0][i]);
	return 0;
}
/*
起来，饥寒交迫的奴隶
起来，全世界受苦的人
满腔的热血已经沸腾
要为真理而斗争
旧世界打个落花流水
奴隶们起来，起来
不要说我们一无所有
我们要做天下的主人

这是最后的斗争
团结起来到明天
英特纳雄耐尔
就一定要实现
这是最后的斗争
团结起来到明天
英特纳雄耐尔
就一定要实现


从来就没有什么救世主
也不需要神仙皇帝
创造全人类的幸福
就只能靠我们自己
我们要夺回劳动果实
把思想冲破牢笼
快把那炉火烧得通红
趁热打铁才能成功

这是最后的斗争
团结起来到明天
英特纳雄耐尔
就一定要实现
这是最后的斗争
团结起来到明天
英特纳雄耐尔
就一定要实现
*/

