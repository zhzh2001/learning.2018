#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int mod=1e9+7;
const int N=1005,K=10000005;
int a[N][N],f[N][N],g[N][N];
int s[K],ls[K],rs[K],rt;
int n,m,k,cnt;
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void add(int &x,int l,int r,int pos,int sum)
{
	if (!x)
		x=++cnt;
	(s[x]+=sum)%=mod;
	if (l==r)
		return;
	int mid=(l+r)/2;
	if (pos<=mid)
		add(ls[x],l,mid,pos,sum);
	else
		add(rs[x],mid+1,r,pos,sum);
}
inline int ask(int x,int l,int r,int L,int R)
{
	if (x==0)
		return 0;
	if (l==L&&r==R)
		return s[x];
	int mid=(l+r)/2;
	if (R<=mid)
		return ask(ls[x],l,mid,L,R);
	if (L>mid)
		return ask(rs[x],mid+1,r,L,R);
	return (ask(ls[x],l,mid,L,mid)+
		   ask(rs[x],mid+1,r,mid+1,R))%mod;
}
int main()
{
	freopen("dp.in","r",stdin);
	freopen("dp.out","w",stdout);
	read(n);
	read(m);
	read(k);
	cnt=k;
	for (int i=1;i<=n;++i)
		for (int j=1;j<=m;++j)
			read(a[i][j]);
	f[1][1]=1;
	for (int i=1;i<=n;++i)
		g[i][1]=1;
	for (int i=1;i<=m;++i)
		g[1][i]=1;
	add(rt=a[1][1],1,m,1,1);
	for (int i=2;i<=n;++i)
	{
		for (int j=2;j<=m;++j)
		{
			f[i][j]=(g[i-1][j-1]-ask(a[i][j],1,m,1,j-1)+mod)%mod;
			g[i][j]=((ll)g[i-1][j]+g[i][j-1]+f[i][j]-g[i-1][j-1])%mod;
			if (g[i][j]<0)
				g[i][j]+=mod;
		}
		for (int j=2;j<=m;++j)
			add(rt=a[i][j],1,m,j,f[i][j]);
	}
	cout<<f[n][m];
	return 0;
}