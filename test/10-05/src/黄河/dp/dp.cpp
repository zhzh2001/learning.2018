#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<ctime>
#include<cstdlib>
#include<string>
#include<queue>
#include<cmath>
#include<set>
#include<map>
#include<bitset>
#include<vector>
#define rep(x,a,b) for (int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (int x=hd[a];~x;x=nx[x])
#define LL long long
#define mo 1000000007
#define N 1010
LL f[N][N],s[N*N];
int a[N][N],vis[N*N],n,m,k,T;
using namespace std;
int read(){
int x=0,fh=1; char c=getchar();
  for(;!isdigit(c); c=getchar()) fh=(c=='-')?-1:fh;
  for(;isdigit(c); c=getchar()) x=(x<<3)+(x<<1)+(c^'0');
  return x*fh;
}
inline void check(int x){
	if (vis[x]!=T) s[x]=0;
	vis[x]=T;
}
inline void CDQ(int l,int r){
	if (l==r) return;
	int mid=l+r>>1;
	CDQ(l,mid);
	LL sum=0;T++;
	rep(i,2,n){
		rep(j,l,mid){
            (sum+=f[i-1][j])%=mo;
            check(a[i-1][j]);
            (s[a[i-1][j]]+=f[i-1][j])%=mo;
        }
		rep(j,mid+1,r){
            check(a[i][j]);
            (f[i][j]+=sum-s[a[i][j]]+mo)%=mo;
        }	
	}	
	CDQ(mid+1,r);
}
int main(){
	freopen("dp.in","r",stdin);
	freopen("dp.out","w",stdout);
	n=read();m=read();k=read();
	rep(i,1,n)
		rep(j,1,n) a[i][j]=read();
	f[1][1]=1;
	CDQ(1,m);
	printf("%lld\n",f[n][m]);
	return 0;
}

