#include<bits/stdc++.h>
using namespace std;
#define mod 1000000007
inline int Mod(int x){return x%mod;/*while(x>=mod) x-=mod;return x;*/}
int f[1001][1001],a[1001][1001],sum[1001][1001],Sum[651][2][1001];
int n,m,k;
int head[1000001],nxt[1000001],x[1000001],y[1000001],tot,cnt[1000001],tmp[1000001];int id[1000001],id_cnt;
int luangao[1000001],luangaocnt;
inline void add(int _x,int _y,int val){nxt[++tot]=head[val];x[tot]=_x;y[tot]=_y;head[val]=tot;cnt[val]++;tmp[val]++;}
inline void init()
{
	scanf("%d%d%d",&n,&m,&k);
	for(int i=1;i<=n;i++) for(int j=1;j<=m;j++)
	{
		int k;scanf("%d",&k);
		if(id[k]==0) id[k]=++id_cnt;
		a[i][j]=id[k];
	}
	for(int i=n;i;i--) for(int j=m;j;j--) add(i,j,a[i][j]);
	sort(tmp+1,tmp+id_cnt+1,greater<int>());
	int Mx=650;if(id_cnt>=100000) Mx=10;
	for(int i=1;i<=id_cnt;i++) if(luangaocnt+1<=Mx&&cnt[i]>=tmp[Mx]) luangao[i]=++luangaocnt;
}
int main()
{
	freopen("dp.in","r",stdin);
//	freopen("dp.out","w",stdout);
	init();f[1][1]=1;sum[1][1]=1;if(luangao[a[1][1]]) Sum[luangao[a[1][1]]][1][1]=1;
	for(int i=1;i<=n;i++) for(int j=1;j<=m;j++) if(i!=1||j!=1)
	{
		f[i][j]=sum[i-1][j-1];
		if(!luangao[a[i][j]])for(int k=head[a[i][j]];k;k=nxt[k])
		{
			if(x[k]>=i) break;
			if(x[k]<i&&y[k]<j) f[i][j]=Mod(f[i][j]-f[x[k]][y[k]]+mod);
		}
		else f[i][j]=Mod(f[i][j]-Sum[luangao[a[i][j]]][(i-1)&1][j-1]+mod);
		sum[i][j]=Mod(Mod(Mod(sum[i-1][j]+sum[i][j-1])-sum[i-1][j-1]+mod)+f[i][j]);
		for(int k=1;k<=luangaocnt;k++) Sum[k][i&1][j]=Mod(Mod(Sum[k][(i-1)&1][j]+Sum[k][i&1][j-1])-Sum[k][(i-1)&1][j-1]+mod);
		if(luangao[a[i][j]]) Sum[luangao[a[i][j]]][i&1][j]=Mod(Sum[luangao[a[i][j]]][i&1][j]+f[i][j]);
	}
	cout<<f[n][m];return 0;
}
