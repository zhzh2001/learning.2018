#include<bits/stdc++.h>
using namespace std;
inline void printsub(string s,bool isbegin)
{
	static const char p[]={'Q','B','S','W'};
	int beg=-1;while(s.length()<4) s='0'+s;
	while(beg+1<(int)s.length()&&s[beg+1]=='0') beg++;
	if(isbegin==0&&beg!=-1) putchar('0');
	for(int i=beg+1;i<4;i++)
	{
		if(s[i]!='0')
		{
			putchar(s[i]);
			if(i!=3) putchar(p[i]);
		}
		else
		{
			bool flag=0;
			for(int j=i+1;j<4;j++)
			{
				if(s[j]!='0'){flag=1;break;}
				if(s[j]=='0'){flag=0;break;}
			}
			if(flag) putchar('0');
		}
	}
}
inline void printint(string s)
{
	while(s.length()&&s[0]=='0') s.erase(0,1);
	int len=s.length();
	if(len==0) return (void)putchar('0');
	if(len<=4) return (void)printsub(s,1);
	if(len<=8)
	{
		printsub(s.substr(0,(len-1)%4+1),1);putchar('W');
		s.erase(0,(len-1)%4+1);if(s!="0000") printsub(s,0);
		return;
	}
	if(len<=12)
	{
		printsub(s.substr(0,(len-1)%4+1),1);putchar('Y');
		s.erase(0,(len-1)%4+1);
		if(s.substr(0,4)!="0000"){printsub(s.substr(0,4),0);putchar('W');}
		s.erase(0,4);
		if(s!="0000") printsub(s,0);
		return;
	}
	puts("read failed");
}
inline bool checkzero(string s)
{
	for(int i=0;i<(int)s.length();i++) if(isdigit(s[i])&&s[i]!='0') return 0;
	return 1;
}
inline void print(string s)
{
	if(checkzero(s)) return (void)putchar('0');
	bool flag=0;
	while(s.length()&&(s[0]=='+'||s[0]=='-'))
	{
		if(s[0]=='-') flag^=1;
		s.erase(0,1);
	}
	if(flag) putchar('F');
	for(int i=0;i<(int)s.length();i++) if(s[i]=='.')
	{
		printint(s.substr(0,i));
		while(s[s.length()-1]=='0') s.erase(s.length()-1,1);
		if(s[s.length()-1]=='.') return;
		putchar('D');
		for(int j=i+1;j<(int)s.length();j++) putchar(s[j]);
		return;
	}
	printint(s);return;
}
inline bool checkone(string s)
{
	int getint=0;
	for(int i=0;i<(int)s.length();i++)
	{
		if(s[i]=='.')
		{
			if(getint!=1) return 0;
			for(int j=i+1;j<(int)s.length();j++) if(s[j]!='0') return 0;
			return 1;
		}
		getint=10*getint+s[i]-'0';
	}
	if(getint==1) return 1;
	return 0;
}
inline void printwithfrac(string s)
{
	for(int i=0;i<(int)s.length();i++) if(s[i]=='/')
	{
		string s1=s.substr(i+1,(int)s.length()-i-1);
		string s2=s.substr(0,i);
		bool flag=0;
		while(s1.length()&&(s1[0]=='+'||s1[0]=='-')){if(s1[0]=='-') flag^=1;s1.erase(0,1);}
		while(s2.length()&&(s2[0]=='+'||s2[0]=='-')){if(s2[0]=='-') flag^=1;s2.erase(0,1);}
		if(checkzero(s2)) return (void)putchar('0');
		if(flag) putchar('F');if(checkone(s1)) return (void)print(s2);
		print(s1);printf("fz");print(s2);
		return;
	}
	print(s);return;
}
string s;
int main()
{
	freopen("read.in","r",stdin);
	freopen("read.out","w",stdout);
	cin>>s;printwithfrac(s);return 0;
}
