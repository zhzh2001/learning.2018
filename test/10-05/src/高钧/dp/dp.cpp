#include <cstdio>
#include <cstring>
using namespace std;
#define int long long
const int N=1005,md=1e9+7;
int n,m,k,mp[N][N],f[N][N];
inline void Ad(int &x,int y){x+=y;if(x>md)x-=md;}
signed main()
{
	freopen("dp.in","r",stdin);
	freopen("dp.out","w",stdout);
	int i,j,x,y; scanf("%lld%lld%lld",&n,&m,&k); memset(f,0,sizeof f);
	for(i=1;i<=n;i++)
	{
		for(j=1;j<=m;j++)
		{
			scanf("%lld",&mp[i][j]);
		}
	}f[1][1]=1;
	for(i=2;i<=n;i++)
	{
		for(j=2;j<=m;j++)
		{
			for(x=1;x<i;x++)
			{
				for(y=1;y<j;y++)
				{
					if(mp[x][y]!=mp[i][j])Ad(f[i][j],f[x][y]);
				}
			}
		}
	}printf("%lld\n",f[n][m]);
}
