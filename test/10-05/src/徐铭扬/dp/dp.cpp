#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int M=1e9+7;
struct node{
	int x,y;
};
vector<node>v[1000002];
int i,j,x,n,m,k;
ll f[1002][1002],sum[1002][1002];
int rd(){
	int x=0,f=1;char c;
	do{c=getchar();if(c=='-')f=-1;}while(c<'0'||c>'9');
	while('0'<=c&&c<='9')x=(x<<3)+(x<<1)+(c^48),c=getchar();
	return x*f;
}
int main(){
	freopen("dp.in","r",stdin);
	freopen("dp.out","w",stdout);
	n=rd();m=rd();k=rd();
	f[1][1]=1;
	for (i=1;i<=n;i++){
		for (j=1;j<=m;j++){
			x=rd();
			for (k=0;k<v[x].size();k++)
				if (v[x][k].x<i && v[x][k].y<j) f[i][j]-=f[v[x][k].x][v[x][k].y];
			(f[i][j]+=sum[i-1][j-1])%=M;
			(sum[i][j]=f[i][j]+sum[i-1][j]+sum[i][j-1]-sum[i-1][j-1])%=M;
			v[x].push_back((node){i,j});
		}
	}
	printf("%lld",(f[n][m]+M)%M);
}
