#include <iostream>
#include <algorithm>
#include <cstdio>
#include <map>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline LL read(){
	LL a=0,b=1;int dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
const LL md=1000000007;
LL n,m,k,a[1003][1003],f[1003][1003];
map<LL,LL>p[1003];
int main(){
	freopen("dp.in","r",stdin);
	freopen("dp.out","w",stdout);
	n=read();
	m=read();
	k=read();
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			a[i][j]=read();
		}
	}
	if(n<=100){
		f[1][1]=1;
		for(int i=2;i<=n;i++){
			for(int j=2;j<=m;j++){
				f[i][j]=0;
				for(int ii=1;ii<i;ii++){
					for(int jj=1;jj<j;jj++){
						if(a[ii][jj]!=a[i][j]){
							f[i][j]+=f[ii][jj];
							f[i][j]%=md;
						}
					}
				}
			}
		}
		printf("%lld\n",(f[n][m]+md)%md);
		return 0;
	}
	for(int i=1;i<=m;i++)f[1][i]=1;
	p[1][a[1][1]]=1;
	for(int i=2;i<=n;i++){
		for(int j=2;j<=m;j++){
			f[i][j]=f[i-1][j-1];
			LL x=a[i][j];
			for(int l=1;l<j;l++){
				if(p[l].count(x))f[i][j]-=p[l][x];
			}
			f[i][j]%=md;
		}
		if(i==n)break;
		for(int j=1;j<=m;j++){
			LL x=a[i][j];
			if(f[i][j]>0){
				p[j][x]=(p[j][x]+f[i][j])%md;
			}
			f[i][j]+=f[i-1][j]+f[i][j-1]-f[i-1][j-1];
			f[i][j]%=md;
		}
	}
	printf("%lld\n",(f[n][m]+md)%md);
	return 0;
}
