#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstring>
#include <string>
using namespace std;
int n;
char c[100003];
bool f=0;
bool pd0(int l,int r){
	for(int i=l;i<=r;i++)if(c[i]=='.'){
		while(c[r]=='0')r--;
		if(c[r]=='.')return pd0(l,r-1);
		return 0;
	}
	while(l<=r&&c[l]=='0')l++;
	return l>r;
}
bool pd1(int l,int r){
	for(int i=l;i<=r;i++)if(c[i]=='.'){
		while(c[r]=='0')r--;
		if(c[r]=='.')return pd0(l,r-1);
		return 0;
	}
	while(l<=r&&c[l]=='0')l++;
	return l==r&&c[l]=='1';
}
void read(int l,int r){
	for(int i=l;i<=r;i++)if(c[i]=='/'){
		if(pd0(l,i-1)){
			putchar('0');
			return;
		}
		if(!pd1(i+1,r)){
			read(i+1,r);
			putchar('f');
			putchar('z');
		}
		read(l,i-1);
		return;
	}
	for(int i=l;i<=r;i++)if(c[i]=='.'){
		read(l,i-1);
		while(c[r]=='0')r--;
		if(c[r]=='.')return;
		putchar('D');
		for(int j=i+1;j<=r;j++)putchar(c[j]);
		return;
	}
	while(c[l]=='0'&&l<=r)l++;
	if(l>r){
		putchar('0');
		return;
	}
	if(r-l>=8){
		read(l,r-8);
		putchar('Y');
		l=r-7;
	}
	if(c[l]=='0'){
		while(c[l]=='0'&&l<=r)l++;
		if(l>r)return;
		putchar('0');
	}
	if(r-l>=4){
		read(l,r-4);
		putchar('W');
		l=r-3;
	}
	if(c[l]=='0'){
		while(c[l]=='0'&&l<=r)l++;
		if(l>r)return;
		putchar('0');
	}
	if(r-l>=3){
		read(l,r-3);
		putchar('Q');
		l=r-2;
	}
	if(c[l]=='0'){
		while(c[l]=='0'&&l<=r)l++;
		if(l>r)return;
		putchar('0');
	}
	if(r-l>=2){
		read(l,r-2);
		putchar('B');
		l=r-1;
	}
	if(c[l]=='0'){
		while(c[l]=='0'&&l<=r)l++;
		if(l>r)return;
		putchar('0');
	}
	if(r-l>=1){
		read(l,r-1);
		putchar('S');
		l=r;
	}
	if(c[l]=='0'){
		while(c[l]=='0'&&l<=r)l++;
		if(l>r)return;
		putchar('0');
	}
	putchar(c[l]);
}
int main(){
	freopen("read.in","r",stdin);
	freopen("read.out","w",stdout);
	gets(c+1);
	n=0;
	for(int i=1;c[i];i++){
		if(c[i]=='-'){
			f=!f;
		}else{
			c[++n]=c[i];
		}
	}
	if(f)putchar('F');
	read(1,n);
	return 0;
}
