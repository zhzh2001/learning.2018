#include<bits/stdc++.h>
using namespace std;
int n,m,k;
const int mo=1000000007;
const int N=1005,LIM=50;
int a[N][N],cnt[N*N],id[N*N];
vector<pair<int,int> > vec[N*N];
int s[N][N],f[N][N];
int tr[20005][N];
void add(int id,int y,int v){
	for (;y<=m;y+=y&(-y))
		tr[id][y]=(tr[id][y]+v)%mo;
}
int ask(int id,int y){
	int ans=0;
	for (;y;y-=y&(-y))
		ans=(ans+tr[id][y])%mo;
	return ans;
}
inline int read(){
	int x=0;
	char ch=getchar();
	for (;ch<'0'||ch>'9';ch=getchar());
	for (;ch>='0'&&ch<='9';ch=getchar())
		x=x*10-48+ch;
	return x;
}
int main(){
	freopen("dp.in","r",stdin);
	freopen("dp.out","w",stdout);
	scanf("%d%d%d",&n,&m,&k);
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++)
			cnt[a[i][j]=read()]++;
	for (int i=1;i<=k;i++)
		if (cnt[i]>LIM) id[i]=++*id;
	f[1][1]=1;
	if (cnt[a[1][1]]>LIM)
		add(id[a[1][1]],1,f[1][1]);
	else vec[a[1][1]].push_back(pair<int,int>(1,1));
	for (int i=2;i<=n;i++){
		for (int j=1;j<=m;j++)
			s[i-1][j]=(f[i-1][j]+s[i-1][j-1])%mo;
		for (int j=1;j<=m;j++)
			s[i-1][j]=(s[i-1][j]+s[i-2][j])%mo;
		for (int j=1;j<=m;j++){
			f[i][j]=s[i-1][j-1];
			if (cnt[a[i][j]]>LIM) f[i][j]=(f[i][j]+mo-ask(id[a[i][j]],j-1))%mo;
			else{
				for (int k=0;k<vec[a[i][j]].size();k++){
					pair<int,int> at=vec[a[i][j]][k];
					if (j>at.second)
						f[i][j]=(f[i][j]+mo-f[at.first][at.second])%mo;
				}
			}
		}
		for (int j=1;j<=m;j++)
			if (cnt[a[i][j]]>LIM) 
				add(id[a[i][j]],j,f[i][j]);
			else vec[a[i][j]].push_back(pair<int,int>(i,j));
	}
	printf("%d\n",f[n][m]);
}
