#include<bits/stdc++.h>
using namespace std;
const int mo=1000000007;
const int N=1005;
int a[N][N],f[N][N],n,m,k;
inline int read(){
	int x=0;
	char ch=getchar();
	for (;ch<'0'||ch>'9';ch=getchar());
	for (;ch>='0'&&ch<='9';ch=getchar())
		x=x*10-48+ch;
	return x;
}
int main(){
	freopen("dp.in","r",stdin);
	freopen("bf.out","w",stdout);
	scanf("%d%d%d",&n,&m,&k);
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++)
			a[i][j]=read();
	f[1][1]=1;
	for (int i=2;i<=n;i++)
		for (int j=2;j<=m;j++)
			for (int k=1;k<i;k++)
				for (int l=1;l<j;l++)
					if (a[i][j]!=a[k][l])
						f[i][j]=(f[i][j]+f[k][l])%mo;
	printf("%d\n",f[n][m]);
}
