#include<bits/stdc++.h>
using namespace std;
int n,m,k;
const int mo=1000000007;
const int N=1005,LIM=50;
int a[N][N],cnt[N*N],id[N*N];
vector<pair<int,int> > vec[N*N];
int s[N][N],f[N][N];
int tr[20005][N];
void add(int id,int y,int v){
	for (;y<=m;y+=y&(-y))
		tr[id][y]=(tr[id][y]+v)%mo;
}
int ask(int id,int y){
	int ans=0;
	for (;y;y-=y&(-y))
		ans=(ans+tr[id][y])%mo;
	return ans;
}
inline int read(){
	int x=0;
	char ch=getchar();
	for (;ch<'0'||ch>'9';ch=getchar());
	for (;ch>='0'&&ch<='9';ch=getchar())
		x=x*10-48+ch;
	return x;
}
int main(){
	freopen("dp.in","w",stdout);
	printf("50 46 50\n");
	srand(time(NULL));
	for (int i=1;i<=50;i++,puts(""))
		for (int j=1;j<=46;j++)
			printf("%d ",rand()%50+1);
}
