/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
#define N 1000055
#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int n,m,tot;
char str[N],ans[N*5];
char p[5];
//出题人找阿 
void rc_x(int l,int r){
	F(i,l,r){
		ans[++tot]=str[i];
	}
}
void rc_zg(int l,int r){
	int len=r-l+1;
	D(i,r,l){
		if (str[i]!='0'){r=i;break;}
	}
	F(i,l,r){
		if (str[i]>'0'){
			ans[++tot]=str[i];
			if (len>1) ans[++tot]=p[len];
		}
		else{
			if (str[i+1]!='0') ans[++tot]='0';
		}
		len--;
	}
}
void rc_zw(int l,int r){
	int len=r-l+1;
	if (len>=5){
		rc_zg(l,r-4);ans[++tot]='W';
		if (str[r-3]=='0') ans[++tot]='0';
	}
	int l1=0;
	F(i,max(l,r-3),r){
		if (str[i]!='0'){
			l1=i;break;
		}
	}
	if (l1!=0) rc_zg(l1,r);
	else{
		if (len>=5) tot--;
	}
}
int rc_z(int l,int r){
	F(i,l,r){
		if (str[i]!='0'){
			l=i;break;
		} 
	}
	int num=0;
	F(i,l,r) num=num*10+str[i]-'0';
	if (num==0){
		ans[++tot]='0';return 0;
	}
	int len=r-l+1;
	if (len>=9){
		rc_zw(l,r-8);
		ans[++tot]='Y';
		if (str[r-7]=='0') ans[++tot]='0';
	}
	int l1=0;
	F(i,max(l,r-7),r){
		if (str[i]!='0'){
			l1=i;break;
		}
	}
	if (l1!=0) rc_zw(l1,r);
	else{
		if (len>=9) tot--;
	}
	return num;
}
int rc(int l,int r){
	int pd=0;
	F(i,l,r) if (str[i]=='.') pd=i;
	if (pd){
		int pd2=0;
		F(i,pd+1,r) if (str[i]!='0') pd2=i;
		if (pd2>0){
			rc_z(l,pd-1);
			ans[++tot]='D';
			rc_x(pd+1,pd2);
			return -1;
		}
		else{
			return rc_z(l,pd-1);
		}
	}
	else{
		return rc_z(l,r);
	}
}
signed main(){
	//freopen("read.in","r",stdin);freopen("read.out","w",stdout);
	p[4]='Q';p[3]='B';p[2]='S';
	scanf("%s",str+1);n=strlen(str+1);
	int pdf=0,yg=0;
	F(i,1,n) if (str[i]=='/') pdf=i;
	if (pdf>0){
		//读分数
		int fh=1,num=pdf;
		for (int i=pdf+1;str[i]=='-'||str[i]=='+';i++){
			if (str[i]=='-') fh=-fh;num=i;
		}
		yg=rc(num+1,n);
		ans[++tot]='f';ans[++tot]='z';
		if (yg==1) tot=0;
		num=0;
		for (int i=1;str[i]=='-'||str[i]=='+';i++){
			if (str[i]=='-') fh=-fh;num=i;
		}
		yg=rc(num+1,pdf-1);
		if (yg==0){puts("0");return 0;}
		if (fh==-1) putchar('F');
	}
	else{
		int fh=1,num=0;
		for (int i=1;str[i]=='-'||str[i]=='+';i++){
			if (str[i]=='-') fh=-fh;num=i;
		}
		yg=rc(num+1,n);
		if (yg==0){
			puts("0");return 0;
		}
		if (fh==-1) putchar('F');
	}
	F(i,1,tot) putchar(ans[i]);
	return 0;
}
