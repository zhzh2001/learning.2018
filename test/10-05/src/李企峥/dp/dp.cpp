#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for(int i=a;i<=b;i++)
#define Rep(i,a,b) for(int i=b;i>=a;i--)
using namespace std;
ll mo,n,m,k;
ll a[1010][1010],f[1010][1010];
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
int main()
{
	freopen("dp.in","r",stdin);
	freopen("dp.out","w",stdout);
	mo=1e9+7;
	n=read();m=read();k=read();
	For(i,1,n)For(j,1,m)a[i][j]=read();
	f[1][1]=1;
	For(i,1,n)For(j,1,m)
		For(ii,i+1,n)For(jj,j+1,m)
		{
			if(a[i][j]!=a[ii][jj])
			{
				f[ii][jj]+=f[i][j];
				f[ii][jj]%=mo;
			}
		}
	cout<<f[n][m]<<endl;
	return 0;
}

