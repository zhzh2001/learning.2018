#include<bits/stdc++.h>
using namespace std;
#define mo 1000000007
int f[1001][1001]={0},a[1001][1001],sum[1001][1001];
int i,j,n,m,k,l,h,t;
vector<int> x[250001],y[250001];
int main()
{
	freopen("dp.in","r",stdin);
	freopen("dp.out","w",stdout);
	scanf("%lld%lld%lld",&n,&m,&k);
	for(i=1;i<=n;i++)
	 for(j=1;j<=n;j++)
	  scanf("%lld",&a[i][j]);
	f[1][1]=1;
	sum[1][1]=1;
	x[a[1][1]].push_back(1);
	y[a[1][1]].push_back(1);
	for(i=1;i<=n;i++)
	{
	 for(j=1;j<=m;j++)
	  if(i!=1||j!=1)
	   {
	   	t=0;
	    l=x[a[i][j]].size();
	    for(k=0;k<l;k++)
	    if (x[a[i][j]][k]<i&&y[a[i][j]][k]<j) t+=f[x[a[i][j]][k]][y[a[i][j]][k]];
	    f[i][j]=(sum[i-1][j-1]-t)% mo;
	    x[a[i][j]].push_back(i);
	    y[a[i][j]].push_back(j);
		sum[i][j]=(f[i][j]+sum[i-1][j]+sum[i][j-1]-sum[i-1][j-1])% mo; 	
	   }  
    }
    /*for(i=1;i<=n;i++)
    {
     for(j=1;j<=m;j++)
      printf("%d ",f[i][j]);
     cout<<endl;  
    }
    for(i=1;i<=n;i++)
    {
     for(j=1;j<=m;j++)
      printf("%d ",sum[i][j]);
     cout<<endl;  
    }*/
	printf("%d",f[n][m]% mo);   
	fclose(stdin);   
	fclose(stdout);   
}
