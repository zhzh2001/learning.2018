#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 1010, mod = 1000000007;
int n,m,k,a[N][N],dp[N][N];
inline void Mod(int &x){if (x>=mod) x-=mod;}
int main(){
	freopen("dp.in","r",stdin);
	freopen("b.out","w",stdout);
	n=read(),m=read(),k=read();
	For(i,1,n) 
		For(j,1,m) a[i][j]=read();
	dp[1][1]=1;
	For(i,2,n)
		For(j,2,m)
			For(I,1,i-1)
				For(J,1,j-1) if (a[i][j]!=a[I][J]) Mod(dp[i][j]+=dp[I][J]);
	printf("%d",dp[n][m]);
}
