#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 1010, mod = 1000000007;
int n,m,k,cnt,a[N][N];
ll Sum,sum[N*N],dp[N][N];
inline void Add(int x,ll y){sum[x]+=y;Sum+=y;}
inline void Del(int x,ll y){sum[x]-=y;Sum-=y;}
inline void CDQ(int l,int r){
	if (l==r) return;
	int mid=l+r>>1;
	CDQ(l,mid);
	For(i,2,n){
		For(j,l,mid) Add(a[i-1][j],dp[i-1][j]);
		For(j,mid+1,r) (dp[i][j]+=Sum-sum[a[i][j]])%=mod;
	}
	For(i,1,n-1)
		For(j,l,mid) Del(a[i][j],dp[i][j]);
	CDQ(mid+1,r);
}
int main(){
	freopen("dp.in","r",stdin);
	freopen("dp.out","w",stdout);
	n=read(),m=read(),k=read();
	For(i,1,n) 
		For(j,1,m) a[i][j]=read();
	dp[1][1]=1;
	CDQ(1,m);
	printf("%lld",dp[n][m]);
}
