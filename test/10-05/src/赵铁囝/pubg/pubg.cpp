#include <iostream>
#include <cstdio>
#include <cmath>

using namespace std;

int a,b,x1,yy,x2,yyy;

inline int gcd(int x,int y){
	return x%y?gcd(y,x%y):y;
}

int main(){
	freopen("pubg.in","r",stdin);
	freopen("pubg.out","w",stdout);
	ios::sync_with_stdio(false);
	while(cin>>x1>>yy>>x2>>yyy){
		x1=abs(x1-x2);
		yy=abs(yy-yyy);
		if(x1==0||yy==0){
			if(x1==yy){
				b++;
				cout<<"YES"<<endl;
			}else{
				if(max(x1,yy)>1){
					a++;
					cout<<"NO"<<endl;
				}else{
					b++;
					cout<<"YES"<<endl;
				}
			}
			continue;
		}
		if(gcd(x1,yy)>1){
			a++;
			cout<<"NO"<<endl;
		}else{
			b++;
			cout<<"YES"<<endl;
		}
	}
	if(a==b){
		cout<<"Friend Ship."<<endl;
	}else{
		if(a>b){
			cout<<"Yahoo!"<<endl;
		}else{
			cout<<"Poor xshen!"<<endl;
		}
	}
	return 0;
}
