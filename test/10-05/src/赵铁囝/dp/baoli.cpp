#include <iostream>
#include <cstdio>

#define Max 1005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

const int wzp=1e9+7;

int n,m,k,a[Max][Max],f[Max][Max];

int main(){
	freopen("dp.in","r",stdin);
	freopen("dp.out","w",stdout);
	n=read();m=read();k=read();
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			a[i][j]=read();
		}
	}
	f[1][1]=1;
	for(int i=2;i<=n;i++){
		for(int j=2;j<=m;j++){
			for(int l=1;l<i;l++){
				for(int r=1;r<j;r++){
					if(a[i][j]!=a[l][r]){
						f[i][j]=(f[i][j]+f[l][r])%wzp;
					}
				}
			}
		}
	}
	/*for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			cout<<f[i][j]<<" ";
		}
		cout<<endl;
	}*/
	writeln(f[n][m]);
	return 0;
}
