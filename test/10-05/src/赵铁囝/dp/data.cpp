#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <ctime>

#define Max 1005

using namespace std;

int n,m,k,a[Max][Max];

int main(){
	freopen("dp.in","w",stdout);
	srand((int)time(NULL));
	n=20;m=20;k=n*m;
	cout<<n<<" "<<m<<" "<<k<<endl;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			a[i][j]=rand()%k+1;
			cout<<a[i][j]<<" ";
		}
		cout<<endl;
	}
	return 0;
}
