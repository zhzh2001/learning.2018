#include <iostream>
#include <cstring>
#include <cstdio>

using namespace std;

int n;
char s[100000];
bool vis[100000];

inline void solve1(int x,int y);
inline void solve2(int x,int y);
inline void solve3(int x,int y);
inline void solve4(int x,int y);
inline bool check1(int x,int y);
inline bool check2(int x,int y);
inline bool check3(int x,int y);

inline void solve1(int x,int y){
	for(int i=x;i<=y;i++){
		if(s[i]=='.'){
			solve2(x,i-1);
			if(check3(i+1,y))return;
			putchar('D');
			solve3(i+1,y);
			return;
		}
	}
	solve2(x,y);
}

inline void solve2(int x,int y){
	if(check1(x,y)){
		putchar('0');
		return;
	}
	int now,k;
	for(int i=x;i<=y;i++){
		if(s[i]!='0'){
			now=i;
			break;
		}
	}
	k=now+(y-now)%4;
	solve4(now,k);
	if(k==y-4)putchar('W');
	if(k==y-8)putchar('Y');
	vis[k]=false;
	for(int i=k+1;i<y;i+=4){
		solve4(i,i+3);
		if(check1(i,i+3)){
			vis[i+3]=true;
		}else{
			if(i+3==y-4)putchar('W');
			if(i+3==y-8)putchar('Y');
			vis[i+3]=false;
		}
	}
	return;
}

inline void solve3(int x,int y){
	int now=y;
	for(int i=y;i>=x;i--){
		if(s[i]!='0'){
			now=i;
			break;
		}
	}
	for(int i=x;i<=now;i++){
		putchar(s[i]);
	}
	return;
}

inline void solve4(int x,int y){
	int now=y;
	for(int i=y;i>=x;i--){
		if(s[i]!='0'){
			now=i;
			break;
		}
	}
	for(int i=x;i<=now;i++){
		if(s[i]=='0'){
			if(s[i-1]=='0'&&vis[i-1]){
				vis[i]=true;
				continue;
			}
			putchar('0');
			vis[i]=true;
			continue;
		}
		putchar(s[i]);
		if(i==y)continue;
		if(i+1==y)putchar('S');
		if(i+2==y)putchar('B');
		if(i+3==y)putchar('Q');
	}
	return;
}

inline bool check1(int x,int y){
	for(int i=x;i<=y;i++){
		if(s[i]!='0'&&s[i]!='.')return false;
	}
	return true;
}

inline bool check2(int x,int y){
	int now=y;
	for(int i=x;i<=y;i++){
		if(s[i]=='.'){
			now=i;
			break;
		}
	}
	for(int i=x;i<now;i++){
		if(s[i]!='0')return false;
	}
	if(s[now]!='1')return false;
	for(int i=now+1;i<=y;i++){
		if(s[i]!='0')return false;
	}
	return true;
}

inline bool check3(int x,int y){
	int now=y;
	for(int i=y;i>=x;i--){
		if(s[i]!='0'){
			now=i;
			break;
		}
	}
	if(s[y]=='0'&&now==y)return true;
	if(now<x)return true;else return false;
}

int main(){
	freopen("read.in","r",stdin);
	freopen("read.out","w",stdout);
	scanf("%s",s+1);
	n=strlen(s+1);
	for(int i=2;i<n;i++){
		if(s[i]=='/'){
			int k=0,now,now2;
			now=1;
			for(int j=1;j<=i-1;j++){
				if(s[j]=='-'||s[j]=='+'){
					if(s[j]=='-')k^=1;
				}else{
					now=j;
					break;
				}
			}
			now2=i+1;
			for(int j=i+1;j<=n;j++){
				if(s[j]=='-'||s[j]=='+'){
					if(s[j]=='-')k^=1;
				}else{
					now2=j;
					break;
				}
			}
			if(check1(now,i-1)){
				puts("0");
				return 0;
			}
			if(k)putchar('F');
			solve1(now,i-1);
			if(check2(now2,n)){
				return 0;
			}
			putchar('f');putchar('z');
			solve1(now2,n);
			return 0;
		}
	}
	int k=0,now;
	for(int i=1;i<=n;i++){
		if(s[i]=='-'||s[i]=='+'){
			if(s[i]=='-')k^=1;
		}else{
			now=i;
			break;
		}
	}
	if(k)putchar('F');
	solve1(now,n);
	return 0;
}
