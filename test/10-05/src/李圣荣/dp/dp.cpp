#include<bits/stdc++.h>
using namespace std;
const int mod=1e9+7;
int a[1005][1005];
long long dp[1005][1005];
inline int read(){
	int k=0; int f=1; char ch=getchar();
	while(!isdigit(ch)){if(ch=='-') f=-1; ch=getchar();}
	while(isdigit(ch)){k=(k<<1)+(k<<3)+(ch^48);ch=getchar();}
	return k*f;
}
int main(){
	freopen("dp.in","r",stdin);
	freopen("dp.out","w",stdout);
	int n,m,k; 
	n=read(); m=read(); k=read();
	for(int i=1;i<=n;i++)
	    for(int j=1;j<=m;j++) a[i][j]=read();
    dp[1][1]=1;
    for(int i=2;i<=n;i++){
        for(int j=2;j<=m;j++){
        	long long sum=0;
        	for(int ii=1;ii<i;ii++){
        		for(int jj=1;jj<j;jj++){
        			if(a[i][j]==a[ii][jj]) continue;
        			sum=(sum+dp[ii][jj])%mod;
				}
			}
			dp[i][j]=sum%mod;
		}	
	}
	cout<<dp[n][m]%mod;
}
/*#include<bits/stdc++.h>
using namespace std;
#define lb(x) x&-x
int dp[1005][1005],c[501*501][501],a[1001][1001];
int sum[1005][1005];
int n,m,k;
void add(int x,int co,int v){
	for(;x<=m;x+=lb(x)) c[co][x]+=v;
}
int query(int x,int co){
	int ans=0;
	for(;x;x-=lb(x)) ans+=c[co][x];
	return ans;
}
int main(){
	 cin>>n>>m>>k;
	for(int i=1;i<=n;i++)  
        for(int j=1;j<=m;j++) cin>>a[i][j];
    add(1,a[1][1],1); sum[1][1]=1; //dp[2][2]=1;
    // sum[2][2]=2; 
    dp[1][1]=1;
    for(int i=2;i<=n;i++){
    	for(int j=2;j<=m;j++){
    		int res=0;
    		//for(int kk=1;kk<j;kk++){
    			//if(kk==a[i][j]) continue;
    			res+=query(kk,a[i][j]);
			} res=query(j-1,a[i][j]);
			cout<<res<<" ";
			dp[i][j]=max(sum[i-1][j-1]-res,0);
		//	add(j,a[i][j],dp[i][j]);
		//	if(i==2&&j==3) cout<<dp[i][j]<<endl;
			sum[i][j]=sum[i-1][j-1]+dp[i][j];
		}
		for(int j=2;j<=m;j++) add(j,a[i][j],dp[i][j]);
	}
	cout<<endl;
	   	for(int i=1;i<=n;i++){
	   for(int j=1;j<=m;j++) cout<<sum[i][j]<<" ";
	   cout<<endl;
	}
	cout<<sum[n][m];
}
*/
