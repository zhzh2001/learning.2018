program dp;
 var
  //k:array[0..10001,0..101] of int64;
  a:array[0..101,0..101] of longint;
  f:array[0..101,0..101] of int64;
  i,j,k,l,m,n,mot:longint;
  p,ssum:int64;
 begin
  assign(input,'dp.in');
  assign(output,'dp.out');
  reset(input);
  rewrite(output);
  mot:=1000000007;
  readln(n,m);
  for i:=1 to n do
   begin
    for j:=1 to m do
     read(a[i,j]);
    readln;
   end;
  //fillqword(k,sizeof(k)>>3,0);
  ssum:=0;
  f[1,1]:=1;
  for i:=1 to n do
   for j:=1 to n do
    for k:=1 to i-1 do
     for l:=1 to j-1 do
      if a[i,j]<>a[k,l] then f[i,j]:=(f[i,j]+f[k,l]) mod mot;
  writeln(f[n,m]);
  close(input);
  close(output);
 end.
