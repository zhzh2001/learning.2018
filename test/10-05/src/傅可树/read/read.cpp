#include<cstdio>
#include<cstring>
using namespace std;
const int N=200000;
char s[N],ch[20];
int cho,F,be,ed,flag,len,point;
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
inline void init(){
	scanf("%s",s+1); len=strlen(s+1);
}
inline bool isnum(char c){
	return (c<='9'&&c>='0');
}
inline void pre(int l,int r){
	int tmp;
	tmp=point-1; be=point;
	for (int i=l;i<=tmp;i++){ //去前导0 
		if (s[i]!='0') {
			be=i;
			break;
		}
	}
	tmp=point+1;
	ed=point;
	for (int i=r;i>=tmp;i--){ //去后导 
		if (s[i]!='0'){
			ed=i;
			break;
		}
	}
}
inline bool all_zero(int l,int r){
	for (int i=l;i<=r;i++){
		if (s[i]!='0') return 0; 
	}
	return 1;
}
inline void zheng(int l,int r){//整数 
	if (l==r) {
		putchar(s[l]);
		return;
	}
	int las=l; bool f;
	for (int i=l;i<=r;i++){
		int w=r-i+1;
		if (ch[w]!='A'){
			if (all_zero(las,i)) continue;
			f=0;
			for (int j=las;j<=i;j++){
				if (s[j]=='0') {
					f=1;
				}else{
					las=j; break;
				}
			}
			if (f) putchar('0');
			zheng(las,i);
			if (ch[w]!='Z') putchar(ch[w]);
			las=i+1;
		}
	}
}
void xiao(int l,int r){
	if (l>r) return;
	putchar('D');
	for (int i=l;i<=r;i++) putchar(s[i]);
}
inline void read(int l,int r){
	if (l>r) return;
	flag=1;
	if (s[l]=='-') flag=0,l++; // 判断正负 
		else if (s[l]=='+') l++;
	point=r+1;
	for (int i=l;i<=r;i++){ //找小数点 
		if (s[i]=='.') point=i; 
	}
	pre(l,r);
	l=be; r=ed;
	if (l==r&&((s[l]=='.')||(s[l]=='0')||(s[l]=='/'))) {
		putchar('0');
		F=1;
		return;
	}
	if (!flag) putchar('F');
	if (isnum(s[point-1])) {
		if (all_zero(l,point-1)) putchar('0');
			else zheng(l,point-1);
	}
		else putchar('0');
	xiao(point+1,r);
}
int chu;
inline void solve(){
	for (int i=0;i<20;i++) ch[i]='A';
	ch[1]='Z'; ch[2]='S'; ch[3]='B'; ch[4]='Q'; ch[5]='W'; ch[9]='Y'; chu=len+1;
	for (int i=1;i<=len;i++){
		if (s[i]=='/') {
			chu=i; break;
		}
	}
	read(1,chu-1);
	if (F&&chu!=len+1) {
		return;
	}
	if (chu!=len+1) {
		putchar('f'); putchar('z');
		read(chu+1,len);
	}
}
int main(){
	freopen("read.in","r",stdin); freopen("read.out","w",stdout);
	init(); solve();
	return 0;
}
