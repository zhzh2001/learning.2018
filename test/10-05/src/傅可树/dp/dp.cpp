#include<cstdio>
#include<cstring>
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=1e3+5,mod=1e9+7;
int cnt,n,m,K,sum[N][N],num[N][N],dp[N][N],root[N*N],S[N*N];
inline void init(){
	n=read(); m=read(); K=read();
	for (int i=1;i<=n;i++){
		for (int j=1;j<=m;j++){
			num[i][j]=read();
		}
	}
}
struct node{
	int son[2],sum;
}a[N*N*20];
int query(int k,int l,int r,int x,int y){
	if (!x||!y||!k) return 0;
	if (l==x&&r==y) {
		return a[k].sum;
	}
	int mid=(l+r)>>1;
	if (mid>=y) return query(a[k].son[0],l,mid,x,y);
		else if (mid<x) return query(a[k].son[1],mid+1,r,x,y);
			else return (query(a[k].son[0],l,mid,x,mid)+query(a[k].son[1],mid+1,r,mid+1,y))%mod;
}
inline void pushup(int k){
	a[k].sum=(a[a[k].son[0]].sum+a[a[k].son[1]].sum)%mod;
}
void update(int &k,int l,int r,int x,int v){
	if (!k) k=++cnt;
	if (l==r) {
		(a[k].sum+=v)%=mod;
		return;
	}
	int mid=(l+r)>>1;
	if (mid>=x) update(a[k].son[0],l,mid,x,v);
		else update(a[k].son[1],mid+1,r,x,v);
	pushup(k);
}
inline void solve(){
	for (int i=1;i<=n;i++){
		memset(S,0,sizeof S);
		for (int j=1;j<=m;j++){
			if (i==1&&j==1) dp[i][j]=1; 
				else {
					dp[i][j]=(sum[i-1][j-1]-query(root[num[i][j]],1,n,1,j-1)+mod)%mod;
					(dp[i][j]+=S[num[i][j]])%=mod;
				}
			update(root[num[i][j]],1,n,j,dp[i][j]);
			sum[i][j]=(sum[i-1][j]+sum[i][j-1])%mod; sum[i][j]=(sum[i][j]-sum[i-1][j-1]+mod)%mod; (sum[i][j]+=dp[i][j])%=mod;
			(S[num[i][j]]+=dp[i][j])%=mod;
		}
	}
	writeln(dp[n][m]);
}
int main(){
	freopen("dp.in","r",stdin); freopen("dp.out","w",stdout);
	init(); solve();
	return 0;
}
