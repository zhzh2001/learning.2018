/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
#define N 500055
//#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int n,m,ans,cnt,root[1005*1005],k,a[1005][1005],g[1005],sum[1005],f[1005][1005];
struct xx{
	int l,r,le,ri,sum;
}t[1005*1005*18];
inline void add(int &x,int k){
	x+=k;x-=(x>=mod)?mod:0;
}
void newp(int &x,int l,int r){
	x=++cnt;t[x].l=l;t[x].r=r;
}
int query(int x,int l,int r){
	if (x==0) return 0;
	if (t[x].l==l&&t[x].r==r) return t[x].sum;
	int mid=(t[x].l+t[x].r)>>1;
	if (r<=mid) return query(t[x].le,l,r);
	else if (l>mid) return query(t[x].ri,l,r);
	else return (query(t[x].le,l,mid)+query(t[x].ri,mid+1,r))%mod;
}
void change(int x,int k,int p){
	if (t[x].l==t[x].r){
		add(t[x].sum,p);return;
	}
	int mid=(t[x].l+t[x].r)>>1;
	if (k<=mid){
		if (!t[x].le) newp(t[x].le,t[x].l,mid);
		change(t[x].le,k,p);
	}
	else{
		if (!t[x].ri) newp(t[x].ri,mid+1,t[x].r);
		change(t[x].ri,k,p);
	}
	t[x].sum=(t[t[x].le].sum+t[t[x].ri].sum)%mod;
}
int t_query(int x,int k){
	if (root[x]==0) return 0;
	else return query(root[x],1,k);
}
void t_add(int x,int k,int p){
	if (!root[x]){newp(root[x],1,m);};
	change(root[x],k,p);
}
signed main(){
	freopen("dp.in","r",stdin);
	freopen("dp.out","w",stdout);
	n=read();m=read();k=read();
	if (n==1&&m==1){
		wrn(1);return 0;
	}
	if (n==1){
		wrn(0);return 0;
	}
	F(i,1,n) F(j,1,m) a[i][j]=read();
	f[1][1]=1;t_add(a[1][1],1,1);
	F(i,1,m-1) sum[i]=1;
	F(i,2,n-1){
		g[1]=0;
		F(j,2,m-1){
			f[i][j]=sum[j-1];
			f[i][j]=(f[i][j]+mod-t_query(a[i][j],j-1))%mod;
			g[j]=(g[j-1]+f[i][j])%mod;
		}
		F(j,2,m-1) add(sum[j],g[j]);
		F(j,2,m-1) 
		t_add(a[i][j],j,f[i][j]);
	}
	ans=sum[m-1];
	ans=(ans+mod-t_query(a[n][m],m-1))%mod;
	wrn(ans);
	return 0;
}
