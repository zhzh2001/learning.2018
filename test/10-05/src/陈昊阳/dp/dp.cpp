#include<iostream>
#include<cstdio>
#include<algorithm>
#include<string>
#include<cstring>
#include<map>
#include<set>
#include<cmath>
#include<queue>
#define rep(a,b,c) for(int a=b;a<=c;++a)
#define drp(a,b,c) for(int a=b;a>=c;--a)
#define crs(a,b) for(int a=hd[b];a;a=ne[a])
#define HY 1009
#define lo long long
#define XJ Iloveyou
#define qword unsigned long long
#define mod 1000000007 
#define maxlonglo 2147483647
#define PI 3.141592653589793238462643383279
#define INF 0x3f3f3f3f
using namespace std;

int n,m,k;
int a[HY][HY],dp[HY][HY];

int read(){
int x=0,fh=1; char c=getchar();
  for(;!isdigit(c); c=getchar()) fh=(c=='-')?-1:fh;
  for(;isdigit(c); c=getchar()) x=(x<<3)+(x<<1)+(c^'0');
return x*fh;
}

int dfs(int x,int y){
 if(dp[x][y]) return dp[x][y];
 if(x==n&&y==m) return 1;
 
  rep(i,x+1,n)
   rep(j,y+1,m)
   if(a[i][j]!=a[x][y])
   dp[x][y]=(dp[x][y]+dfs(i,j))%mod;
   
 return dp[x][y]%mod;
}

int main(){
  freopen("dp.in","r",stdin); freopen("dp.out","w",stdout);
  
  n=read(); m=read(); k=read();
  
   rep(i,1,n)
    rep(j,1,m)
	 a[i][j]=read();
    
    cout<<dfs(1,1)%mod;
return 0;
}













