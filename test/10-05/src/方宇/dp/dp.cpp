#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cctype>
#define LL long long
#define QW unsigned long long
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define fy 9223372036854775808
#define N 1000010
using namespace std;

LL read()
{
	LL x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
int main()
{
	freopen("dp.in","r",stdin);
	freopen("dp.out","w",stdout);
	
	return 0;
}
