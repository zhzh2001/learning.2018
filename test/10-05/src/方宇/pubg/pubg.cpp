#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cctype>
#define LL long long
#define QW unsigned long long
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define fy 9223372036854775808
#define N 1000010
using namespace std;
int l,r;
LL x1,x2,y1,y2;
QW a,b,c;
LL read()
{
	LL x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
QW gcd(QW a,QW b)
{
	QW r=a%b;
	if (r==0) return b;
	return gcd(b,r);
}
int main()
{
	freopen("pubg.in","r",stdin);
	freopen("pubg.out","w",stdout);
	while (~scanf("%lld%lld%lld%lld",&x1,&y1,&x2,&y2))
	{
		if(x1>x2) a=x1-x2; else a=x2-x1;
		if(y1>y2) b=y1-y2; else b=y2-y1;
		if(b==0&&a>1||a==0&&b>1)
		{
			printf("NO\n");
			r++;
			continue;
		} 
		if(b==0||a==0)
		{
			printf("YE5\n");
			l++;
			continue;
		}
		c=gcd(a,b);
		if (c>1) 
		{
			printf("NO\n");
			r++;
		}
		  else
		{
		  	printf("YE5\n");
			l++;
		}
	}
	if (l>r) printf("Poor xshen!\n");
	if (l==r) printf("Friend Ship.\n");
	if (l<r) printf("Yahoo!\n");
	return 0;
}
