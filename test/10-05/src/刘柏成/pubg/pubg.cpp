#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
ull gcd(ull a,ull b){
	return b?gcd(b,a%b):a;
}
ull sub(ll x,ll y){
	if (x<0 && y<0)
		x=-x,y=-y;
	if (x<y)
		swap(x,y);
	ull ans=x;
	if (y<0){
		y=-y;
		ans=ans+(ull)y;
	}else ans=ans-(ull)y;
	return ans;
}
int main(){
	freopen("pubg.in","r",stdin);
	freopen("pubg.out","w",stdout);
	ll a,b,c,d;
	int cntr=0,cntx=0;
	while(scanf("%lld%lld%lld%lld",&a,&b,&c,&d)!=EOF){
		ull x=sub(a,c),y=sub(b,d);
		bool flag=((!x) && (!y));
		if (flag || gcd(x,y)==1){
			cntr++;
			puts("YE5");
		}else{
			cntx++;
			puts("NO");
		}
	}
	if (cntr>cntx)
		puts("Poor xshen!");
	else if (cntr==cntx)
		puts("Friend Ship.");
	else puts("Yahoo!");
}
