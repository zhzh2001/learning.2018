#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const ll INF=0x3f3f3f3f3f3f3f3fLL;
char b[233333],s[233333];
ll geti(int l,int r){
	ll res=0;
	bool after=false;
	for(int i=l;i<=r;i++){
		if (s[i]=='.'){
			after=true;
			continue;
		}
		if (after){
			if (s[i]!='0')
				return -INF;
		}else res=res*10+s[i]-'0';
	}
	return res;
}
void readi(ll x){
	static char ans[233],s[233];
	if (!x){
		putchar('0');
		return;
	}
	int cur=0;
	while(x){
		s[cur++]=x%10+'0';
		x/=10;
	}
	int idx=0;
	for(int i=cur-cur%4;i>=0;i-=4){
		int r=min(i+4,cur)-1;
		bool az=true;
		for(int j=r;j>=i;j--)
			if (s[j]!='0')
				az=false;
		if (az){
			bool flag=true;
			for(int j=r;j>=0;j--)
				if (s[j]!='0')
					flag=false;
			if ((!flag) && idx && ans[idx-1]!='0')
				ans[idx++]='0';
			continue;
		}
		for(int j=r;j>=i;j--){
			if (s[j]=='0'){
				bool flag=true;
				for(int k=i;k<=j;k++)
					if (s[k]!='0')
						flag=false;
				if ((!flag) && idx && ans[idx-1]!='0')
					ans[idx++]='0';
				continue;
			}
			ans[idx++]=s[j];
			if (j-i==3)
				ans[idx++]='Q';
			else if (j-i==2)
				ans[idx++]='B';
			else if (j-i==1)
				ans[idx++]='S';
		}
		if (i==8)
			ans[idx++]='Y';
		else if (i==4)
			ans[idx++]='W';
	}
	ans[idx]=0;
	printf("%s",ans);
}
void readde(int l,int r){
	ll x=geti(l,r);
	if (x!=-INF){
		readi(x);
		return;
	}
	for(int i=l;i<=r;i++)
		if (s[i]=='.'){
			readi(geti(l,i-1));
			putchar('D');
			int to=r;
			for(int j=r;j>i;j--)
				if (s[j]!='0'){
					to=j;
					break;
				}
			for(int j=i+1;j<=to;j++)
				putchar(s[j]);
			return;
		}
}
int main(){
	freopen("read.in","r",stdin);
	freopen("read.out","w",stdout);
	scanf("%s",b+1);
	int n=strlen(b+1);
	int sgn=1,cur=0;
	for(int i=1;i<=n;i++)
		if (b[i]=='-')
			sgn*=-1;
		else if (b[i]!='+')
			s[++cur]=b[i];
	n=cur;
	for(int i=1;i<=n;i++)
		if (s[i]=='/'){
			if (!geti(1,i-1)){
				puts("0");
				return 0;
			}
			if (sgn<0)
				putchar('F');
			if (geti(i+1,n)==1){
				readde(1,i-1);
				return 0;
			}
			readde(i+1,n);
			printf("fz");
			readde(1,i-1);
			return 0;
		}
	if (!geti(1,n)){
		puts("0");
		return 0;
	}
	if (sgn<0)
		putchar('F');
	readde(1,n);
}
