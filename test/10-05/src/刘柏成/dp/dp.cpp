#include <bits/stdc++.h>
using namespace std;
const int maxn=1000002;
int read(){
	int x=0;
	char c=getchar();
	while(c<'0' || c>'9')
		c=getchar();
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x;
}
const int mod=1000000007;
struct segtree{
	struct node{
		int sum,ls,rs;
	}t[maxn*30];
	int n,cur;
	int p,v;
	void update(int& o,int l,int r){
		if (!o)
			o=++cur;
		t[o].sum+=v;
		if (t[o].sum>=mod)
			t[o].sum-=mod;
		if (l==r)
			return;
		int mid=(l+r)/2;
		if (p<=mid)
			update(t[o].ls,l,mid);
		else update(t[o].rs,mid+1,r);
	}
	void add(int &o,int p,int v){
		this->p=p,this->v=v;
		update(o,1,n);
	}
	int ql,qr;
	int query(int o,int l,int r){
		if (!o)
			return 0;
		if (ql<=l && qr>=r)
			return t[o].sum;
		int mid=(l+r)/2,ans=0;
		if (ql<=mid)
			ans+=query(t[o].ls,l,mid);
		if (qr>mid)
			ans+=query(t[o].rs,mid+1,r);
		if (ans>=mod)
			ans-=mod;
		return ans;
	}
	int get(int o,int l,int r){
		if (l>r)
			return 0;
		ql=l,qr=r;
		return query(o,1,n);
	}
}t;
int a[1003][1003],dp[1003][1003],sum[1003][1003],rt[maxn];
int main(){
	freopen("dp.in","r",stdin);
	freopen("dp.out","w",stdout);
	int n,m,k;
	n=read();
	m=read();
	k=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			a[i][j]=read();
	dp[1][1]=sum[1][1]=1;
	t.n=m;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			if (i==1 && j==1)
				continue;
			dp[i][j]=sum[i-1][j-1];
			dp[i][j]=dp[i][j]-t.get(rt[a[i][j]],1,j-1);
			if (dp[i][j]<0)
				dp[i][j]+=mod;
			sum[i][j]=((sum[i-1][j]+sum[i][j-1]-sum[i-1][j-1])%mod+dp[i][j])%mod;
			if (sum[i][j]<0)
				sum[i][j]+=mod;
		}
		for(int j=1;j<=m;j++)
			t.add(rt[a[i][j]],j,dp[i][j]);
	}
	printf("%d",(dp[n][m]%mod+mod)%mod);
}
