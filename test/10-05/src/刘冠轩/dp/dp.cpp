#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){
	int x=0;char c=getchar();bool p=1;
	for(;!isdigit(c);c=getchar())if(c=='-')p=0;
	for(;isdigit(c);c=getchar())x=(x<<1)+(x<<3)+(c^48);
	return p?x:-x;
}
const int N=1005;
const int P=1e9+7;
int n,m,k,f[N][N],a[N][N];
signed main(){
	freopen("dp.in","r",stdin);
	freopen("dp.out","w",stdout);
	n=read();m=read();k=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)a[i][j]=read();
	f[1][1]=1;
	for(int i=2;i<=n;i++)
		for(int j=2;j<=m;j++){
			for(int k=1;k<i;k++)
				for(int l=1;l<j;l++)
					if(a[i][j]!=a[k][l])f[i][j]+=f[k][l];
			f[i][j]%=P;
		}
	cout<<f[n][m];
	return 0;
}
