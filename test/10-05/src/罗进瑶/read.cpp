#include<bits/stdc++.h>

using namespace std;
typedef long long ll;
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}while(ch >= '0' && ch <= '9') {
x = x * 10 + ch - '0';ch = getchar();}return w * x;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen(".in", "r", stdin);
	freopen(".out", "w", stdout);
}

int main() {
	return 0;
}
