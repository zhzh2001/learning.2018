#include<bits/stdc++.h>

using namespace std;
typedef long long ll;
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}while(ch >= '0' && ch <= '9') {
x = x * 10 + ch - '0';ch = getchar();}return w * x;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("pubg.in", "r", stdin);
	freopen("pubg.out", "w", stdout);
}
ll t1, t2;
ll x[5], y[5];
ll gcd(ll a, ll b) {
	if(!b) return a;
	return gcd(b, a % b);
}
bool solve() {
	ll d1 = abs(x[2] - x[1]);
	ll d2 = abs(y[2] - y[1]);
	return gcd(d1, d2) == 1;
}
int main() {
	setIO();
	while(~scanf("%lld%lld%lld%lld", &x[1], &y[1], &x[2], &y[2])) {
		if(solve()) {++t1;puts("YE5");}
		else {++t2;puts("NO");}
	}
	if(t1 > t2) {puts("Poor xshen!");}
	else if(t1 == t2) puts("Friend Ship.");
	else puts("Yahoo!");
	return 0;
}
