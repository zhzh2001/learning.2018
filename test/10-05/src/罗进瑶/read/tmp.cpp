#include<bits/stdc++.h>

using namespace std;
typedef long long ll;
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}while(ch >= '0' && ch <= '9') {
x = x * 10 + ch - '0';ch = getchar();}return w * x;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
const int N = 1234;
void setIO() {
	freopen("read.in", "r", stdin);
	freopen("read.out", "w", stdout);
}
int m, L[N], R[N];
char tran[] = " SBQWY";
char know[] = {' ', 'W', 'Y'};
#define show cout<<t; if(tmp.length()) cout<<"D"<<tmp; cout<<endl;
ll getit(int l, int r, int x, string t) {
	for(int i = r; i >= l; --i) {
		bool fg = 0;
		for(int j = R[i]; j >= L[i]; --j) {
			int k = j - L[i];
			if(t[j] != '0') {
				putchar(t[j]);
				fg = 1;
				if(k > 0)
					putchar(tran[k]);
			} else if(j - L[i] >= 0){
				while(j - L[i] >= 0 && t[j] == '0') --j;
				if(j - L[i] < 0) break;
				++j;
				putchar('0');
			}
		}
		if(fg && i - x > 0) putchar(know[i - x]);
	}
}
void solve(string t) {
	int p = 0;
	string tmp;
	if(t[0] == '-') {
		t = t.substr(1, t.length() - 1);
		putchar('F');
	}
	if(t[0] == '+') 
		t = t.substr(1, t.length() - 1);
	for(int i = 0 ; i < t.length(); ++i)
		if(t[i] == '.') p = i;
	if(t[p] == '.') {
		tmp = t.substr(p + 1, t.length() - 1 - p - 1 + 1);
		t = t.substr(0, p);
	}
	while(tmp.length() > 1 && tmp[tmp.length() - 1] == '0') tmp = tmp.substr(0, tmp.length() - 1);
	for(int i = 0; i < t.length() / 2; ++i)
		swap(t[i], t[t.length() - i - 1]);
	while(t.length() > 1 && t[t.length() - 1] == '0') t = t.substr(0, t.length() - 1);
	if(t.length() == 0) t = "0";
	m = 0;
	for(int i = 0; i < t.length(); i += 4) {
		L[++m] = i;
		R[m] = min((int)t.length() - 1, i + 3);
	}
	if(t.length() == 1 && t[0] == '0')
		printf("0");
	else {
		int tot = m / 3, i = m;
		for(; i >= 4; i -= 3) {
			getit(i - 2, i, i - 2, t);
			for(int j = 1; j <= tot; ++j)
				putchar('Y');
			--tot;
		}
		if(i > 1) {
			getit(1, i, 1, t);
		}
	}
	if(tmp.length() && !(tmp.length() == 1 && tmp[0] == '0'))
		cout<<"D"<<tmp;
}
bool check(string t) {
	int p = 0;
	string tmp;
	for(int i = 0 ; i < t.length(); ++i)
		if(t[i] == '.') p = i;
	if(t[p] == '.') {
		tmp = t.substr(p + 1, t.length() - 1 - p - 1 + 1);
		t = t.substr(0, p);
	}
	while(tmp.length() > 1 && tmp[tmp.length() - 1] == '0') tmp = tmp.substr(0, tmp.length() - 1);
	for(int i = 0; i < t.length() / 2; ++i)
		swap(t[i], t[t.length() - i - 1]);
	if(t.length() == 0) t = "0";
	while(t.length() > 1 && t[t.length() - 1] == '0') t = t.substr(0, t.length() - 1);
	return t.length() == 1 && t[0] == '1' && (tmp.length() == 0 || tmp.length() == 1 && tmp[0] == '0');
}
string s;
int main() {
//	setIO();
	int p = 0;
	ll num = 0;
	cin>>s;
	for(int i = 0; i < s.length(); ++i)
		if(s[i] == '/') p = i;
	if(p) {
		bool zero = 1;
		for(int i = 0; i < p; ++i) if(s[i] > '0' && s[i] <= '9') zero = 0;
		if(zero) {puts("0");}
		else {
			if(check(s.substr(p + 1, s.length() - 1 - p - 1 + 1))) {
				solve(s.substr(0, p));
			} else 
				solve(s.substr(p + 1, s.length() - 1 - p - 1 + 1)), printf("fz"), solve(s.substr(0, p));	
		}
	}
	else solve(s.substr(0, s.length()));
	return 0;
}
