#include<bits/stdc++.h>

using namespace std;
typedef long long ll;
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}while(ch >= '0' && ch <= '9') {
x = x * 10 + ch - '0';ch = getchar();}return w * x;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("dp.in", "r", stdin);
	freopen("dp.out", "w", stdout);
}
const int N = 1100;
int n, m, a[N][N], f[N][N];
ll val[N * N];
const ll p = 1000000007;
void solve(int l, int r) {
	if(l == r) return;
	int mid = (l + r) >> 1;
	if(l <= mid) solve(l, mid);
	ll res = 0;
	for(int i = 1; i <= m; ++i)
		for(int j = l; j <= r; ++j) val[a[j][i]]= 0;
	for(int i = 1; i <= m; ++i) {
		for(int j = r; j >= l; --j) if(j <= mid){
			(val[a[j][i]] += f[j][i]) %= p;
			(res += f[j][i]) %= p;
		} else {
			(f[j][i] += res - val[a[j][i]]) %= p;
		}
	}
	if(r > mid) solve(mid + 1, r);
}
int main() {
	setIO();
	n = read(), m = read(); read();
	for(int i = 1; i <= n; ++i)
		for(int j = 1; j <= m; ++j)
			a[i][j] = read();
	f[1][1] = 1;
	solve(1, n);
	writeln((f[n][m] + p) % p);
	return 0;
}
