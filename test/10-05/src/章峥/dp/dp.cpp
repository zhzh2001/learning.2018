#include<fstream>
using namespace std;
ifstream fin("dp.in");
ofstream fout("dp.out");
const int N=1005,MOD=1e9+7;
int a[N][N],root[N*N],cc,sum[N][N],sum2[N*N],dp[N][N];
struct node
{
	int sum,ls,rs;
}tree[N*N*12];
void modify(int& id,int l,int r,int x,int val)
{
	if(!id)
		id=++cc;
	if(l==r)
		tree[id].sum=(tree[id].sum+val)%MOD;
	else
	{
		int mid=(l+r)/2;
		if(x<=mid)
			modify(tree[id].ls,l,mid,x,val);
		else
			modify(tree[id].rs,mid+1,r,x,val);
		tree[id].sum=(tree[tree[id].ls].sum+tree[tree[id].rs].sum)%MOD;
	}
}
int query(int id,int l,int r,int L,int R)
{
	if(!id)
		return 0;
	if(L<=l&&R>=r)
		return tree[id].sum;
	int mid=(l+r)/2,ans=0;
	if(L<=mid)
		ans=query(tree[id].ls,l,mid,L,R);
	if(R>mid)
		ans=(ans+query(tree[id].rs,mid+1,r,L,R))%MOD;
	return ans;
}
int main()
{
	int n,m,k;
	fin>>n>>m>>k;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			fin>>a[i][j];
	dp[1][1]=1;
	modify(root[a[1][1]],1,m,1,1);
	for(int i=1;i<=n;i++)
		sum[i][1]=1;
	for(int i=1;i<=m;i++)
		sum[1][i]=1;
	for(int i=2;i<=n;i++)
	{
		for(int j=2;j<=m;j++)
		{
			dp[i][j]=((sum[i-1][j-1]-query(root[a[i][j]],1,m,1,j-1)+MOD)%MOD+sum2[a[i][j]])%MOD;
			sum2[a[i][j]]=(sum2[a[i][j]]+dp[i][j])%MOD;
			modify(root[a[i][j]],1,m,j,dp[i][j]);
			sum[i][j]=(((sum[i-1][j]+sum[i][j-1])%MOD-sum[i-1][j-1]+MOD)%MOD+dp[i][j])%MOD;
		}
		for(int j=2;j<=m;j++)
			sum2[a[i][j]]=0;
	}
	fout<<dp[n][m]<<endl;
	return 0;
}
