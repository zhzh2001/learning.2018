#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("pubg.in");
ofstream fout("pubg.out");
inline unsigned long long dabs(long long a,long long b)
{
	if(a<b)
		swap(a,b);
	if((a>=0&&b>=0)||(a<=0&&b<=0))
		return a-b;
	else
		return (unsigned long long)a+(unsigned long long)(-b);
}
unsigned long long gcd(unsigned long long a,unsigned long long b)
{
	return b?gcd(b,a%b):a;
}
int main()
{
	long long x1,y1,x2,y2;
	int win=0,lost=0;
	while(fin>>x1>>y1>>x2>>y2)
	{
		if(gcd(dabs(x1,x2),dabs(y1,y2))<=1)
		{
			fout<<"YE5\n";
			lost++;
		}
		else
		{
			fout<<"NO\n";
			win++;
		}
	}
	if(win>lost)
		fout<<"Yahoo!\n";
	else if(win==lost)
		fout<<"Friend Ship.\n";
	else
		fout<<"Poor xshen!\n";
	return 0;
}
