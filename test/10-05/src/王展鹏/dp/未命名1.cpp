#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline int read(){
	bool pos=1; int x=0; char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')pos=0;
	for(;isdigit(ch);ch=getchar())x=x*10+ch-'0';
	return pos?x:-x;
}
const int N=1005,M=20000005,mod=1000000007;
int n,m,k,rt[N*N],dp[N][N],sum[N][N],a[N][N],tree[M],lson[M],rson[M],nodecnt;
void insert(int l,int r,int pos,int de,int &nod){
	if(!nod)nod=++nodecnt; 
	if(l==r){
		tree[nod]=(tree[nod]+de)%mod; return;
	}
	int mid=(l+r)>>1;
	if(pos<=mid)insert(l,mid,pos,de,lson[nod]);
	else insert(mid+1,r,pos,de,rson[nod]);
	tree[nod]=(tree[lson[nod]]+tree[rson[nod]])%mod;
}
int ask(int l,int r,int i,int j,int nod){
	if(!nod)return 0;
	if(l==i&&r==j)return tree[nod];
	int mid=(l+r)>>1;
	if(j<=mid)return ask(l,mid,i,j,lson[nod]);
	else if(i>mid)return ask(mid+1,r,i,j,rson[nod]);
	else return (ask(l,mid,i,mid,lson[nod])+ask(mid+1,r,mid+1,j,rson[nod]))%mod;
}
int main(){
	//freopen("dp.in","r",stdin); freopen("dp.out","w",stdout);
	srand(time(0));
	n=1e3; m=1e3; k=1e4;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++)a[i][j]=(rand()<<15^rand())%k+1;
	}
	dp[1][1]=1;
	for(int i=1;i<n;i++){
		for(int j=1;j<m;j++){
			sum[i][j]=((ll)sum[i-1][j]+sum[i][j-1]-sum[i-1][j-1]+dp[i][j])%mod;
			insert(1,m,j,dp[i][j],rt[a[i][j]]);
			dp[i+1][j+1]=(sum[i][j]-ask(1,k,1,j,rt[a[i+1][j+1]]))%mod;
		}
	}
	cout<<nodecnt<<endl;
	cout<<(dp[n][n]+mod)%mod<<endl;
}
