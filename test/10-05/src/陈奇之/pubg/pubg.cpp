#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int inf = 0x3f3f3f3f;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define gc getchar
#define pc putchar
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
#define pp(x,y) cout << "pp: " << x << " " << y << endl;
inline ll read(){
	register ll x=0,f=1;register char c=gc();
	for(;!isdigit(c);c=gc())if(c=='-')f=-1;
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}
#define rd read
void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);puts("");}
#define y1 __y1
ll x1,y1,x2,y2;
inline ll gcd(ll a,ll b){
	return b ? gcd(b,a%b) : a;
}
int main(){
	freopen("pubg.in","r",stdin);
	freopen("pubg.out","w",stdout);
	int ans1 = 0,ans2 = 0;
	while(~scanf("%lld%lld%lld%lld",&x1,&y1,&x2,&y2)){
		int a = abs(x1 - x2);
		int b = abs(y1 - y2);
		if(a==0&&b==0 || gcd(a,b) == 1){
			ans1++;
			puts("YE5");
		} else{
			ans2++;
			puts("NO");
		}
	}
	if(ans1 > ans2){
		puts("Poor xshen!");
	} else
	if(ans1 == ans2){
		puts("Friend Ship.");
	} else{
		puts("Yahoo!");
	}
	return 0;
}
