#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int inf = 0x3f3f3f3f;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define gc getchar
#define pc putchar
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
#define pp(x,y) cout << "pp: " << x << " " << y << endl;
inline ll read(){
	register ll x=0,f=1;register char c=gc();
	for(;!isdigit(c);c=gc())if(c=='-')f=-1;
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}
#define rd read
void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);puts("");}
const int maxn = 1005;
const int maxm = 1005 * 1005 * 15;
const int mod = 1e9+7;
#define mid ((l+r)>>1)
int T[maxm],lc[maxm],rc[maxm],tot;
int a[maxn][maxn],f[maxn],sum[maxn];
int n,m,k;
int root[maxn*maxn];
inline void insert(int &o,int l,int r,int x,int v){
	if(!o){
		o = ++tot;
		lc[o] = rc[o] = T[o] = 0;
	}
	if(l==r){
		T[o] = (T[o] + v) % mod;
		return ;
	}
	if(x<=mid) insert(lc[o],l,mid,x,v); else
	if(mid+1<=x) insert(rc[o],mid+1,r,x,v);
	T[o] = (T[lc[o]] + T[rc[o]]) % mod;
}
inline int query(int o,int l,int r,int x,int y){
	if(!o) return 0;
	if(l==x&&r==y) return T[o];
	if(y<=mid) return query(lc[o],l,mid,x,y); else
	if(mid+1<=x) return query(rc[o],mid+1,r,x,y); else
	return (query(lc[o],l,mid,x,mid) + query(rc[o],mid+1,r,mid+1,y)) % mod;
}
int main(){
	freopen("dp.in","r",stdin);
	freopen("dp.out","w",stdout);
	n = rd(),m = rd(),k = rd();
	Rep(i,1,k) root[i] = 0;tot = 0;
	Rep(i,1,n) Rep(j,1,m) a[i][j] = rd();
	insert(root[a[1][1]],1,m,1,1);
	Rep(i,1,m) sum[i] = f[i] = 1;
	Rep(i,2,n){
		Rep(j,1,m) f[j] = 0;
		Rep(j,2,m){
			f[j] = (sum[j-1] - query(root[a[i][j]],1,m,1,j-1) + mod) % mod;
		}
		Rep(j,1,m) if(f[j]) insert(root[a[i][j]],1,m,j,f[j]);
		Rep(j,2,m) f[j] = (f[j] + f[j-1]) % mod;
		Rep(j,1,m) sum[j] = (sum[j] + f[j]) % mod;
	}
	writeln((f[m]-f[m-1]+mod)%mod);
	return 0;
}
