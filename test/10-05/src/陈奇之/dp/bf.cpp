#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int inf = 0x3f3f3f3f;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define gc getchar
#define pc putchar
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
#define pp(x,y) cout << "pp: " << x << " " << y << endl;
inline ll read(){
	register ll x=0,f=1;register char c=gc();
	for(;!isdigit(c);c=gc())if(c=='-')f=-1;
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}
#define rd read
void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);puts("");}
const int maxn = 1005;
const int maxm = 1005 * 1005 * 15;
const int mod = 1e9+7;
#define lson (o<<1)
#define rson (o<<1|1)
#define mid ((l+r)>>1)
int a[maxn][maxn],f[maxn][maxn],sum[maxn];
int n,m,k;
int main(){
	freopen("dp.in","r",stdin);
	freopen("bf.out","w",stdout);
	n = rd(),m = rd(),k = rd();
	Rep(i,1,n) Rep(j,1,m) a[i][j] = rd();
	f[1][1] = 1;
	Rep(i,2,n){
		Rep(j,2,m){
			f[i][j] = 0;
			Rep(k,1,i-1){
				Rep(l,1,j-1){
					if(a[i][j] != a[k][l])
						f[i][j] = (f[i][j] + f[k][l]) % mod;
				}
			}
		}
	}
	writeln(f[n][m]);
	return 0;
}
