#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int inf = 0x3f3f3f3f;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define gc getchar
#define pc putchar
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
#define pp(x,y) cout << "pp: " << x << " " << y << endl;
inline ll read(){
	register ll x=0,f=1;register char c=gc();
	for(;!isdigit(c);c=gc())if(c=='-')f=-1;
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}
#define rd read
void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);puts("");}
const int maxn = 20000;

bool Is_zero(char s[],int n){
	if(!n) return true;
	int pos=n+1;
	Rep(i,1,n) if(s[i]=='.'){pos=i;break;}
	Rep(i,1,pos-1) if(s[i] != '0') return false;
	Rep(i,pos+1,n) if(s[i] != '0') return false;
	return true;
}
bool Is_one(char s[],int n){
	int pos=n+1;
	Rep(i,1,n) if(s[i]=='.'){pos=i;break;}
	if(!(1<=pos-1)) return false;
	Rep(i,1,pos-2) if(s[i] != '0') return false;
	if(s[pos-1] != '1') return false;
	Rep(i,pos+1,n) if(s[i] != '0') return false;
	return true;
}

inline bool f(char c){
	if(c=='Y' || c=='W' || c=='Q' || c=='B' || c=='S') return true;
	return false;
}
inline void read_W(char s[],int n){
	if(n==1){
		printf("%c",s[1]);
	} else
	if(n==2){
		if(s[2] == '0')	printf("%cS",s[1]);
		else printf("%cS%c",s[1],s[2]);
	} else
	if(n==3){
		if(s[2]=='0'&&s[3]=='0') printf("%cB",s[1]);
		else if(s[2]=='0') printf("%cB0%c",s[1],s[3]);
		else if(s[3]=='0') printf("%cB%cS",s[1],s[2]);
		else printf("%cB%cS%c",s[1],s[2],s[3]);
	} else
	if(n==4){
		if(s[2]=='0'&&s[3]=='0'&&s[4]=='0') printf("%cQ",s[1]);//1000
		else if(s[2]=='0'&&s[3]=='0') printf("%cQ0%c",s[1],s[4]);//1001
		else if(s[2]=='0'&&s[4]=='0') printf("%cQ0%cS",s[1],s[3]);//1010
		else if(s[3]=='0'&&s[4]=='0') printf("%cQ%cB",s[1],s[2]);//1100
		else if(s[2]=='0') printf("%cQ0%cS%c",s[1],s[3],s[4]);//1011
		else if(s[3]=='0') printf("%cQ%cB0%c",s[1],s[2],s[4]);//1101
		else if(s[4]=='0') printf("%cQ%cB%cS",s[1],s[2],s[3]);//1110
		else printf("%cQ%cB%cS%c",s[1],s[2],s[3],s[4]);//1111
	} else{
		puts("WzpAkKing");
		exit(0);
	}
}
inline void read_Z(char s[],int n){
//	printf("read_Z(%s %d)\n",s+1,n);
	if(!n) return ;
	if(s[1] == '0'){
		read_Z(s+1,n-1);
		return ;
	}
	if(n>=9){
		read_W(s,n-8);
		printf("Y");
		if(!Is_zero(s+(n-8),8)){
			if(s[n-8+1] == '0') printf("0");
			read_Z(s+(n-8),8);
		}
		return ;
	}
	if(n>=5){
		read_W(s,n-4);
		printf("W");
		if(!Is_zero(s+(n-4),4)){
			if(s[n-4+1] == '0') printf("0");
			read_Z(s+(n-4),4);
		}
		return ;
	}
	if(n>=1){
		read_W(s,n);
		return ;
	}
}

void read_X(char s[],int n){
//	printf("read_X(%s %d)\n",s+1,n);
	while(s[n] == '0') n--;
	Rep(i,1,n){
		printf("%c",s[i]);
	}
}
void read_real(char s[],int n){
	int pos = n+1;
	Rep(i,1,n){
		if(s[i] == '.'){
			pos = i;
			break;
		}
	}
	char a[maxn],b[maxn];
	mem(a,0);mem(b,0);
	int len1 = 0,len2 = 0;
	Rep(i,1,pos-1) a[++len1] = s[i];
	Rep(i,pos+1,n) b[++len2] = s[i];
	if(Is_zero(b,len2)){
		if(!len1 || Is_zero(a,len1)){
			printf("0");
		} else{
			read_Z(a,len1);
		}
	} else{
		if(!len1 || Is_zero(a,len1)){
			printf("0");
		} else{
			read_Z(a,len1);
		}
		printf("D");	
		read_X(b,len2);
	}
}

void read_frac(char t[],int m){
	char s[maxn];int n = 0;mem(s,0);
	int opt=1;
	Rep(i,1,m) if(t[i] == '-') opt*=-1; else if(t[i]!='+') s[++n] = t[i];

	int pos = -1;
	Rep(i,1,n){
		if(s[i] == '/'){
			pos = i;
			break;
		} 
	}
	if(pos == -1){
		if(opt==-1) printf("F");
		read_real(s,n);
	} else{
		char a[maxn],b[maxn];
		mem(a,0);mem(b,0);
		int len1 = 0,len2 = 0;
		Rep(i,1,pos-1) a[++len1] = s[i];
		Rep(i,pos+1,n) b[++len2] = s[i];
		if(Is_zero(a,len1)){
			printf("0");
		} else
		if(Is_one(b,len2)){
			if(opt==-1) printf("F");
			read_real(a,len1);
		} else{
			if(opt==-1) printf("F");
			read_real(b,len2);
			printf("fz");
			read_real(a,len1);
		}
	}
}
int main(){
	//freopen("read.in","r",stdin);	freopen("read.out","w",stdout);
	char s[maxn];int n;
	scanf("%s",s+1);n = strlen(s+1);
	read_frac(s,n);
	return 0;
}
