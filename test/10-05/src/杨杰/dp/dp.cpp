#include <cstdio>
#include <map>
using namespace std;
const int N=1005,P=1e9+7;
int mp[N][N],f[N][N],s[N][N],rt[N*N];
int n,m,K,T;
struct S_tree {
	int s,ls,rs;
}tr[N*N*20];
void add(int &ts,int l,int r,int pos,int v) {
	if (!ts) ts=++T;
	tr[ts].s=(tr[ts].s+v)%P;
	if (l==r) return;
	int mid=(l+r)>>1;
	if (pos<=mid) add(tr[ts].ls,l,mid,pos,v);
	else add(tr[ts].rs,mid+1,r,pos,v);
}
int query(int ts,int l,int r,int L,int R) {
	if (!ts || L>R) return 0;
	if (l==L && r==R) return tr[ts].s;
	int mid=(l+r)>>1;
	if (R<=mid) return query(tr[ts].ls,l,mid,L,R);
	else if (L>mid) return query(tr[ts].rs,mid+1,r,L,R);
	else return (query(tr[ts].ls,l,mid,L,mid)+query(tr[ts].rs,mid+1,r,mid+1,R))%P;
}
int main() {
	freopen("dp.in","r",stdin);
	freopen("dp.out","w",stdout);
	scanf("%d%d%d",&n,&m,&K);
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++) scanf("%d",&mp[i][j]);
	for (int i=1;i<=n;i++) {	
		for (int j=1;j<=m;j++) {
			if (i==1 && j==1) f[i][j]=1;
			else {
				f[i][j]=s[i-1][j-1]-query(rt[mp[i][j]],1,m,1,j-1);
				f[i][j]=(f[i][j]+P)%P;
			}
			s[i][j]=s[i-1][j]+s[i][j-1]-s[i-1][j-1]+f[i][j];
			s[i][j]=(s[i][j]+P)%P;
		}
		for (int j=1;j<=m;j++) {
			add(rt[mp[i][j]],1,m,j,f[i][j]);
		}
	}
	printf("%d\n",f[n][m]);
}
