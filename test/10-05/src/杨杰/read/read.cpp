#include <cstdio>
#include <cstring>
using namespace std;
const int N=2e4+7;
char s[N],c[14][3]={"","S","B","Q","W","S","B","Q","Y","S","B","Q","W"};
void readz(int l,int r) {
	int x=l;
	if (s[l]=='+' || s[l]=='-') l++;
	while (s[l]=='0' && l<=r) l++;
	if (l>r) {putchar('0');return;}
	x=r;
	while (s[x]=='0' && x>l) x--;
	while (l<=x) {
		if (s[l]!='0' || s[l-1]!='0') putchar(s[l]);
		if(s[l]!='0')printf("%s",c[r-l]);
		l++;
	}
	if (r-l>=8) putchar('Y');
	else if (r-l>=4) putchar('W');
}
void reads(int l,int r) {
	while (s[r]=='0' && r>=l) r--;
	if (l+1>=r) return;
	putchar('D');
	for (int i=l+1;i<=r;i++) putchar(s[i]);
}
bool check0(int l,int r) {
	for (int i=l;i<=r;i++) if (s[i]!='0') return 0;
	return 1;
}
bool check1(int l,int r) {
	int mid=r+1;
	if (s[l]=='+' || s[l]=='-') l++; 
	for (int i=l;i<=r;i++) {
		if (s[i]=='.') {
			mid=i;
			for (int i=mid+1;i<=r;i++) if (s[i]!='0') return 0;
		}
	}
	for (int i=l;i<mid-1;i++) if (s[i]!='0') return 0;
	if (s[mid-1]!='1') return 0;
	return 1;
}
void read(int l,int r) {
	int p=0;
	for (int i=l;i<=r;i++) {
		if (s[i]=='.') {
			if (s[l]=='-') {
				if (!check0(l,i-1) || !check0(i+1,r)) putchar('F');
			}
			readz(l,i-1);
			reads(i,r),p=1;
		}
	}
	if (!p) {
		if (s[l]=='-' && !check0(l,r)) putchar('F');
		readz(l,r);
	}
}
int main() {
	freopen("read.in","r",stdin);
	freopen("read.out","w",stdout);
	scanf("%s",s+1);
	int ln=strlen(s+1),p=0;
	for (int i=1;i<=ln;i++) {
		if (s[i]=='/') {
			int l=1;
			if (s[l]=='-' || s[l]=='+') l++;
			if (check0(1,i-1)) return puts("0"),0;
			p=1;
			int pp=0;
			if (!check1(i+1,ln)) read(i+1,ln),printf("fz"),pp=1;
			if (s[i+1]=='-' && !pp){
			 	if (s[1]=='-') read(2,i-1);
			 	else s[0]='-',read(0,i-1);
			}
			else read(1,i-1);
		}
	}
	if (!p) read(1,ln);
}
