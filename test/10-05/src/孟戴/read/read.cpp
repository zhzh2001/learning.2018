//#include<iostream>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
using namespace std;
ifstream fin("read.in");
ofstream fout("read.out");
char ch,wei[1003];
bool mdndz,mdndm,fu,b[1003];
long long s[1003],fenmu,fenzi;
string ss;
inline long long init(){
	long long x=0,f=1;
	ch=getchar();
	if(ch=='.')return 0;
	fu=false;
	for(;ch<'0'||ch>'9';ch=getchar()){
		if(ch=='-'){
			f=-1,fu=true;
		}
	}
	for(;ch>='0'&&ch<='9';ch=getchar()){
		x=x*10+ch-'0';
	}
	return x*f;
}
inline void solve(long long fenzi){
	long long t=1;
	while(fenzi){
		s[t]=fenzi%10;
		fenzi/=10;
		t++;
	}
	long long wu=0;
	for(int i=1;i<t;i++){
		if(s[i]!=0){
			wu=i;
		}
	}
	if(wu==0){
		fout<<"0";
	}else{
		if(wu>=5){
			for(int i=5;i<=min(8ll,wu);i++){
				if(s[i]!=0){
					b[5]=true,b[i]=true;
				}
			}
			bool you=false;
			if(wu>8){
				you=true;
			}
			for(int i=min(wu,8ll);i>=5;i--){
				if(s[i]!=0){
					if(you==true&&s[i+1]==0){
						b[i+1]=true;
					}
					you=true;
				}
			}
		}
		if(wu>=9){
			for(int i=9;i<=wu;i++){
				if(s[i]!=0){
					b[9]=true,b[i]=true;
				}
			}
			bool you=false;
			for(int i=wu;i>=9;i--){
				if(s[i]!=0){
					if(you==true&&s[i+1]==0){
						b[i+1]=true;
					}
					you=true;
				}
			}
		}
		for(int i=1;i<=4;i++){
			if(s[i]!=0){
				b[i]=true;
			}
		}
		bool you;
		if(wu>4){
			you=true;
		}else{
			you=false;
		}
		for(int i=4;i>=1;i--){
			if(s[i]!=0){
				if(you==true&&s[i+1]==0){
					b[i+1]=true;
				}
				you=true;
			}
		}
		for(int i=wu;i;i--){
			if(b[i]){
				if((i!=9&&i!=5)||(s[i]!=0)){
					fout<<(char)('0'+s[i]);
				}
				if(i!=1&&s[i]!=0||(i==5||i==9)){
					fout<<wei[i];
				}
			}
		}
	}
}
int main(){
	freopen("read.in","r",stdin);
	freopen("read.out","w",stdout);
	wei[1]='!',wei[2]='S',wei[3]='B',wei[4]='Q';
	wei[5]='W',wei[6]='S',wei[7]='B',wei[8]='Q';
	wei[9]='Y',wei[10]='S',wei[11]='B',wei[12]='Q';
	fenzi=init();
	if(fenzi==0){
		fout<<0<<endl;
		return 0;
	}
	mdndz=fu;
	if(ch=='/'){
		fenmu=init();
		mdndm=fu;
		if(mdndz^mdndm){
			fout<<"F";
			fenmu=abs(fenmu),fenzi=abs(fenzi);
		}else if(mdndz&&mdndm){
			fenzi=-fenzi,fenmu=-fenmu;
		}
		if(abs(fenmu)!=1){
			solve(fenmu),fout<<"fz",solve(fenzi);
		}else{
			solve(fenzi);
		}
	}else if(ch=='.'){
		fin>>ss;
		long long len=ss.length();
		if(mdndz){
			fenzi=-fenzi;
			fout<<"F";
		}
		solve(fenzi);
		long long e=-1;
		for(int i=0;i<len;i++){
			if(ss[i]!='0'){
				e=i;
			}
		}
		if(e!=-1){
			fout<<"D";
		}
		for(int i=0;i<=e;i++){
			fout<<ss[i];
		}
	}else{
		if(mdndz){
			fenzi=-fenzi;
			fout<<"F";
		}
		solve(fenzi);
	}
	return 0;
}
