#include<iostream>
#include<stdio.h>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
using namespace std;
//ifstream fin("dp.in");
//ofstream fout("dp.out");
const long long mod=1000000007;
long long n,m,k;
long long num[1003][1003];
long long dp[1003][1003];
int main(){
	cin>>n>>m>>k;
	for(register int i=1;i<=n;i++){
		for(register int j=1;j<=m;j++){
			cin>>num[i][j];
		}
	}
	dp[1][1]=1;
	for(register int i=1;i<=n;i++){
		for(register int j=1;j<=m;j++){
			for(register int x=1;x<=i-1;x++){
				for(register int y=1;y<=j-1;y++){
					if(num[x][y]!=num[i][j]){
						dp[i][j]=dp[i][j]+dp[x][y];
						if(dp[i][j]>1e16){
							dp[i][j]=dp[i][j]%mod;
						}
					}
				}
			}
		}
	}
	cout<<dp[n][m]%mod<<endl;
	return 0;
}
/*

in:
4 4 4
1 1 1 1
1 3 2 1
1 2 4 1
1 1 1 1


*/
