#include<cstdio>
using namespace std;
inline int read(){
	int ret=0,p=1;
	char c=getchar();
	while((c>'9')||(c<'0')){
		if(c=='-')p=-1;
		c=getchar();
	}
	while((c>='0')&&(c<='9')){
		ret=(ret<<1)+(ret<<3)+c-'0';
		c=getchar();
	}
	return ret*p;
}
int mo=1000000007,n,m,k,size,su1,su2,sl[1005],ge[1000005],co[1005][1005],su[1005][1005],f[1005][1005],ls[36000000],rs[36000000],tr[36000000];
void upd(int &rt,int val,int p,int l,int r){
	if(!rt){
		size++;
		rt=size;
	}
	if(l==r){
		tr[rt]=(tr[rt]+val)%mo;
		return;
	}
	int mid=(l+r)>>1;
	if(p<=mid)upd(ls[rt],val,p,l,mid);
	else upd(rs[rt],val,p,mid+1,r);
	tr[rt]=(tr[ls[rt]]+tr[rs[rt]])%mo;
}
int ask(int rt,int L,int R,int l,int r){
	if(!rt)return 0;
	if(L<=l&&r<=R){
		return tr[rt];
	}
	int sum=0,mid=(l+r)>>1;
	if(L<=mid)sum=(sum+ask(ls[rt],L,R,l,mid))%mo;
	if(R>mid)sum=(sum+ask(rs[rt],L,R,mid+1,r))%mo;
	return sum;
}
int main(){
	freopen("dp.in","r",stdin);
	freopen("dp.out","w",stdout);
	n=read();
	m=read();
	k=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			co[i][j]=read();
	for(int i=1;i<=m;i++)
		su[1][i]=1;
	f[1][1]=1;
	upd(ge[co[1][1]],1,1,1,m);
	for(int i=2;i<=n;i++){
		for(int j=m;j>=1;j--){
			f[i][j]=((su[i-1][j-1]-ask(ge[co[i][j]],1,j-1,1,m))%mo+mo)%mo;
			upd(ge[co[i][j]],f[i][j],j,1,m);
		}
		for(int j=1;j<=m;j++){
			sl[j]=(sl[j-1]+f[i][j])%mo;
			su[i][j]=(su[i-1][j]+sl[j])%mo;
		}
	}
	printf("%d",f[n][m]);
}/*
4 4 4
1 1 1 1
1 3 2 1
1 2 4 1
1 1 1 1
*/
