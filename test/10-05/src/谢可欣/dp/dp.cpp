#include <iostream>
#include <cstdio>
using namespace std;
inline int read(){int tot=1;char c=getchar();while(c!='-'&&(c<'0'||c>'9'))c=getchar();if(c=='-')tot=-1,c=getchar();
int sum=0;while(c<='9'&&c>='0')sum=sum*10+c-'0',c=getchar();return sum*tot;}
int ans,n,m,k,a[1005][1005],f[1005][1005];
void dfs(int x,int y){
	if(x==n&&y==m){
		ans++;
		return;
	}
	for(int i=x+1;i<=n;i++)
	    for(int j=y+1;j<=m;j++){
	    	if(a[i][j]!=a[x][y]) dfs(i,j);
		}
}
int main()
{
	freopen("dp.in","r",stdin);
	freopen("dp.out","w",stdout);
	n=read(),m=read(),k=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++) a[i][j]=read();
//	dfs(1,1);
//	cout<<ans<<endl;
	f[1][1]=1;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++){
			for(int k=1;k<i;k++)
				for(int q=1;q<j;q++){
					if(a[i][j]!=a[k][q])f[i][j]+=f[k][q];
				}
		}
	cout<<f[n][m]<<endl;
	return 0;
}
