#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 510
#define MOD 1000000007
#define int long long
using namespace std;
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9) write(x/10);putchar(x%10+'0');}
int read(){int d=0,w=1;char c=getchar();for(;c<'0'||c>'9';c=getchar())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=getchar())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
int a[maxn][maxn],f[maxn][maxn],n,m,kk;
signed main()
{
	freopen("dp.in","r",stdin);
	freopen("dp.out","w",stdout);
	n=read();
	m=read();
	kk=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;a[i][j++]=read());
	f[1][1]=1;
	for(int i=2;i<=n;i++)
		for(int j=2;j<=n;j++)
			for(int k=1;k<i;k++)
				for(int l=1;l<j;l++)
					if(a[i][j]!=a[k][l]) (f[i][j]+=f[k][l])%=MOD;
	write(f[n][m]);
	return 0;
}
