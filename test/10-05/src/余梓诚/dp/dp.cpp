#include <cstdio>
#include <cstring>
#include <iostream>
using namespace std;

const int maxn=1100,mo=1000000007;
int n,m,k,f[maxn][maxn],a[maxn][maxn];

void judge(){
	freopen("dp.in","r",stdin);
	freopen("dp.out","w",stdout);
}

int main(){
	judge();
	scanf("%d%d%d",&n,&m,&k);
	for (int i=1;i<=n;i++){
		for (int j=1;j<=m;j++) scanf("%d",&a[i][j]);
	}
	f[1][1]=1;
	for (int i=1;i<=n;i++){
		for (int j=1;j<=m;j++){
			for (int k=i+1;k<=n;k++){
				for (int t=j+1;t<=m;t++){
					if (a[i][j]!=a[k][t]) f[k][t]=(f[k][t]+f[i][j])%mo;
				}
			}
		}
	}
	printf("%d\n",f[n][m]);
	return 0;
}
