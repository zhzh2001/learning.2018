#include <cstdio>
#include <cstring>
#include <iostream>
using namespace std;

const int maxn=110;

void judge(){
	freopen("read.in","r",stdin);
	freopen("read.out","w",stdout);
}

string s;

//读取小数点的位置 
int get_p(string s){
	int len=s.length();
	int loc=0;
	for (int i=0;i<len;i++){
		if (s[i]=='.') return loc;
		loc=i;
	}
	return loc;
}
//取出s l到r部分的字符串 
string get_s(string s,int l,int r){
	string res="";
	for (int i=l;i<=r;i++) res=res+s[i];
	return res;
}
//读整数
struct info{
	bool pre,suf;
	string s;
};
info read_int(string s){
	string res="";
	int locpre=-1,locsuf=-1;
	for (int i=0;i<4;i++){
		if (s[i]!='0'){
			locpre=i;
			break;
		}
	}
	for (int i=3;i>=0;i--){
		if (s[i]!='0'){
			locsuf=i;
			break;
		}
	}
	if (locpre==-1){
		info t;
		t.s=res; t.pre=t.suf=0;
		return t;
	}
	for (int i=locpre;i<=locsuf;i++){
		if (s[i]=='0'&&s[i-1]=='0') continue;
		res=res+s[i];
		if (s[i]=='0') continue;
		if (i==0) res=res+'Q';
		if (i==1) res=res+'B';
		if (i==2) res=res+'S';
	}
	info t;
	t.s=res; t.pre=(locpre>0); t.suf=(locsuf<3);
	return t;
}
void read_int(string s,bool d){
	int len=s.length(),loc=-1;
	if (s[0]=='-') printf("F"); 
	for (int i=0;i<len;i++){
		if (s[i]=='-') continue;
		if (s[i]!='0'){
			loc=i;
			break;
		}
	}
	if (loc==-1) return (void) (printf("0"));
	string now="";
	int opt=12-len+(s[0]=='-')-1;
	for (int i=0;i<=opt;i++) now=now+'0';
	for (int i=0;i<len;i++){
		if (s[i]=='-') continue;
		now=now+s[i];
	}
	info t1=read_int(get_s(now,0,3));
	info t2=read_int(get_s(now,4,7));
	info t3=read_int(get_s(now,8,11));
	string s1=t1.s,s2=t2.s,s3=t3.s;
	if (s1==""){
		if (s2==""){
			if (s3=="") printf("0");
			else cout<<s3;
		}else{
			if (s3=="") cout<<s2<<"W";
			else if (t2.suf||t3.pre) cout<<s2<<"W0"<<s3;
			else cout<<s2<<"W"<<s3;
		}
	}else{
		if (s2==""){
			if (s3=="") cout<<s1<<"Y";
			else cout<<s1<<"Y0"<<s3;
		}else{
			if (s3==""){
				if (t1.suf||t2.pre) cout<<s1<<"Y0"<<s2<<"W";
				else cout<<s1<<"Y"<<s2<<"W";
			}else{
				if (t1.suf||t2.pre){
					cout<<s1<<"Y0"<<s2;
					if (t2.suf||t3.pre) cout<<"0"<<s3;
					else cout<<s3;
				}else{
					cout<<s1<<"Y"<<s2<<"W";
					if (t2.suf||t3.pre) cout<<"0"<<s3;
					else cout<<s3;
				}
			}
		}
	}
}
//读小数
void read_p(string s){
	int len=s.length(),loc=-1;
	for (int i=len-1;i>=0;i--){
		if (s[i]!='0'){
			loc=i;
			break;
		}
	}
	if (loc!=-1) printf("D");
	for (int i=0;i<=loc;i++) printf("%c",s[i]);
}

long long change(string s){
	int len=s.length();
	long long res[2];
	res[0]=res[1]=0;
	int now=0;
	for (int i=0;i<len;i++){
		if (s[i]=='-') continue;
		if (s[i]=='.'){
			now++;
			continue;
		}
		if (isdigit(s[i])) res[now]=res[now]*10+s[i]-'0';
	}
	if (res[1]!=0) return 100000;
	return res[0];
}

int main(){
	judge();
	cin>>s;
	int locf=-1,len=s.length();
	bool flag=0;
	for (int i=0;i<len;i++){
		if (s[i]=='/'){
			flag=1; locf=i;
			break;
		}
	}
	if (flag){//分数
		if (change(get_s(s,0,locf-1))==0) return printf("0"),0;
		if (change(get_s(s,locf+1,len-1))==1){
			string ss=get_s(s,0,locf-1);
			int locp=get_p(ss);
			int lenn=ss.length();
			if (locp!=lenn-1){//有小数
				read_int(get_s(ss,0,locp),1);
				read_p(get_s(ss,locp+2,lenn-1));
			}else read_int(get_s(ss,0,locp),1);
			return 0;
		}
		
		string ss=get_s(s,0,locf-1);
		int locp=get_p(ss),lenn=ss.length();
		if (locp!=len-1){
			read_int(get_s(ss,0,locp),1);
			read_p(get_s(ss,locp+2,lenn-1));
		}else read_int(get_s(ss,0,locp),1);
		printf("fz");
		ss=get_s(s,locf+1,len-1);
		locp=get_p(ss);
		lenn=ss.length();
		if (locp!=lenn-1){
			read_int(get_s(ss,0,locp),1);
			read_p(get_s(ss,locp+2,lenn-1));
		}else read_int(get_s(ss,0,locp),1);
		return 0;
	}
	int locp=get_p(s);
	if (locp!=len-1){//有小数
		read_int(get_s(s,0,locp),1);
		read_p(get_s(s,locp+2,len-1));
	}else read_int(get_s(s,0,locp),1);
	return 0;
}
//QAQ
