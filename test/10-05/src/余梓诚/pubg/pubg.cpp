#include <cstdio>
#include <cstring>
#include <iostream>
using namespace std;

long long x,y,xx,yy;
int sum1,sum2;

void judge(){
	freopen("pubg.in","r",stdin);
	freopen("pubg.out","w",stdout);
}

unsigned long long gcd(unsigned long long x,unsigned long long y){
	return y==0?x:gcd(y,x%y);
}

long long abs(long long x){
	return x<0?-x:x;
}

int main(){
	judge();
	while (scanf("%lld%lld%lld%lld",&x,&y,&xx,&yy)!=EOF){
		unsigned long long X,Y;
		if (xx<0&&x<0){
			if (abs(xx)>abs(x)) X=abs(xx)+x;
			else X=abs(x)+xx;
		}else if (x>0&&xx>0){
			if (xx>x) X=xx-x;
			else X=x-xx;
		}else X=abs(x)+abs(xx);
		if (yy<0&&y<0){
			if (abs(yy)>abs(y)) Y=abs(yy)+y;
			else Y=abs(y)+yy;
		}else if (y>0&&yy>0){
			if (yy>y) Y=yy-y;
			else Y=y-yy;
		}else Y=abs(y)+abs(yy);
		if (gcd(X,Y)==1) sum1++,puts("YE5");
		else sum2++,puts("NO");
	}
	if (sum1>sum2) puts("Poor xshen!");
	else if (sum1==sum2) puts("Friend Ship.");
	else puts("Yahoo!");
	return 0;
}
