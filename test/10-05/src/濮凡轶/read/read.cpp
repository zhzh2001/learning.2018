#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <stack>
#include <string>
#include <fstream>

using std::string;
using std::max;
using std::min;

//#include <iostream>
//using std::cin;
//using std::cout;
//using std::endl;

std::ifstream fin("read.in");
std::ofstream fout("read.out");

namespace fu // ����
{
	inline void read(string s)
	{
		bool flag = false;
		for(unsigned i = 0; i < s.length(); ++i)
			if(s[i] == '-')
				flag ^= 1;
		if(flag)
			fout << "F";
	}
}

namespace fen_yu
{
	inline string chun(string s)
	{
		int l = 0;
		while(s[l] == '-' || s[l] == '/' || s[l] == '+')
			l++;
		return s.substr(l);
	}

	inline bool is_zero(string s) // zheng
	{
		for(int i = 0; i < (int) s.length() && s[i] != '/'; ++i)
			if(isdigit(s[i]) && s[i] != '0')
				return false;
		return true;
	}

	inline bool is_1(string s)
	{
		bool flag = false;
		int dot = s.length();
		for(int i = 0; i < (int) s.length(); ++i)
		{
			if(s[i] == '.')
			{
				dot = i;
				break;
			}
		}
		for(int i = s.length() - 1; i > dot; --i)
			if(isdigit(s[i]) && s[i] != '0')
				return false;
		for(int i = dot - 1; ~i; --i)
		{
			if(isdigit(s[i]))
			{
				if(!flag)
				{
					flag = true;
					if(s[i] != '1')
						return false;
				}
				else if(s[i] != '0')
					return false;
			}
		}
		return true;
	}
}

namespace zheng
{
	inline string cut_zero(string s)
	{
		int now = 0;
		while(s[now] == '0' && now < (int) s.length() - 1)
			now++;
		s = s.substr(now);
		return s;
	}

	inline string read_wan_nei(string s)
	{
		string ans = "";
		int now = s.length();
		int wz = now - 1;
		while(s[wz] == '0')
			wz--;
		s = s.substr(0, wz + 1);
		for(int i = 0, j = now - 1; i < (int) s.length(); ++i, --j)
		{
			if(s[i] != '0' || !i || s[i-1] != '0')
				ans.push_back(s[i]);
			if(s[i] != '0')
			{
				switch(j)
				{
					case 1:
						ans.push_back('S');
						break;
					case 2:
						ans.push_back('B');
						break;
					case 3:
						ans.push_back('Q');
						break;
				}
			}
		}
		return ans;
	}

	inline void read(string s)
	{
		s = cut_zero(s);
		if(fen_yu::is_zero(s))
		{
			fout << "0";
			return;
		}
		std::stack<string> tmp;
		while(!tmp.empty())
			tmp.pop();
		int llen = s.length() % 4;
		if(llen)
		{
			fout << read_wan_nei(s.substr(0, llen));
			if(s.length() > 8)
				fout << "Y";
			else if(s.length() > 4)
				fout << "W";
			s = s.substr(llen);
		}
		while(s.length())
		{
			string tmp = read_wan_nei(s.substr(0, 4));
			if(tmp != "")
			{
				fout << tmp;
				if(s.length() > 8)
					fout << "Y";
				else if(s.length() > 4)
					fout << "W";
				if(s.length() <= 4)
					break;
			}
			s = s.substr(4);
		}
	}
}

namespace xiao
{
	inline string cut_zero(string s)
	{
		int now = s.length() - 1;
		while(~now && s[now] == '0')
			now--;
		return s.substr(0, now + 1);
	}

	inline void read_xiao(string s)
	{
		s = cut_zero(s);
		if(s != "")
		{
			fout << "D";
			fout << s;
		}
	}

	inline void read(string s)
	{
		string zb = "";
		int now = 0;
		while(now < (int) s.length() && s[now] != '.' && !isdigit(s[now]))
			++now;
//		cout << s << endl;
//		cout << now << endl;
		if(s[now] == '.')
			zb = "0";
		else
		{
			for(; isdigit(s[now]); ++now)
				zb.push_back(s[now]);
		}
//		cout << now << endl;
//		cout << "zb = " << zb << endl;
		zheng::read(zb);
		zb = "";
		while(s[now] != '.' && now < (int) s.length())
			now++;
		now++;
//		cout << s[now] << endl;
		for(; now < (int) s.length() && isdigit(s[now]); ++now)
			zb.push_back(s[now]);
//		cout << zb << endl;
		read_xiao(zb);
	}
}

namespace fen
{
	inline int get_fen(string s)
	{
		for(int i = 0; i < (int) s.length(); ++i)
			if(s[i] == '/')
				return i;
		return 0;
	}

	inline void read(string s)
	{
		if(fen_yu::is_zero(s))
		{
			fout << "0";
			return;
		}
		fu::read(s);
		int fen = get_fen(s);
//		cout << "fen = " << fen << endl;
		if(fen)
		{
			string tmpp = s.substr(0, fen);
			string tmp = s.substr(fen);
			if(!fen_yu::is_1(tmp))
			{
				xiao::read(fen_yu::chun(tmp));
				fout << "fz";
			}
			xiao::read(fen_yu::chun(tmpp));
		}
		else
			xiao::read(fen_yu::chun(s));
	}
}

string s;

int main()
{
	fin >> ::s;
	fen::read(s);
	return 0;
}
