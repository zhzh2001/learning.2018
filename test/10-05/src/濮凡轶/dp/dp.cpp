#include <cstdio>
#include <algorithm>

typedef long long LL;

const int maxn = 505;
const int mod = 1000000007;

LL dp[maxn][maxn];
int mp[maxn][maxn];

int main()
{
	freopen("dp.in", "r", stdin);
	freopen("dp.out", "w", stdout);
	int n, m, k;
	scanf("%d%d%d", &n, &m, &k);
	dp[1][1] = 1;
	for(int i = 1; i <= n; ++i)
	{
		for(int j = 1; j <= m; ++j)
		{
			scanf("%d", &mp[i][j]);
			if(i == 1 && j == 1)
				continue;
			for(int ki = 1; ki < i; ++ki)
				for(int kj = 1; kj < j; ++kj)
					if(mp[i][j] != mp[ki][kj])
						(dp[i][j] += dp[ki][kj]) %= mod;
		}
	}
	printf("%lld\n", dp[n][m] % mod);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
