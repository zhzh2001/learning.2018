#include <cstdio>
#include <cmath>
#include <algorithm>

using namespace std;

typedef long long LL;

inline LL gcd(LL a, LL b)
{
	while(b)
	{
		a %= b;
		swap(a, b);
	}
	return a;
}

inline bool pan(LL x1, LL y1, LL x2, LL y2)
{
	x1 -= x2;
	y1 -= y2;
//	printf("%lld %lld\n", x1, y1);
	if(x1 && y1)
		return abs(gcd(x1, y1)) == 1;
	else
		return x1 <= 1 && y1 <= 1;
}

int main()
{
	freopen("pubg.in", "r", stdin);
	freopen("pubg.out", "w", stdout);
	LL x1, y1, x2, y2;
	LL ri = 0, xshen = 0;
	while(scanf("%lld%lld%lld%lld", &x1, &y1, &x2, &y2) == 4)
	{
		if(pan(x1, y1, x2, y2))
		{
			puts("YE5");
			ri++;
		}
		else
		{
			puts("NO");
			xshen++;
		}
	}
	if(ri == xshen)
		puts("Friend Ship.");
	else if(ri > xshen)
		puts("Poor xshen!");
	else
		puts("Yahoo!");
	fclose(stdin);
	fclose(stdout);
	return 0;
}
