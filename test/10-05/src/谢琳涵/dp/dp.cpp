#include<bits/stdc++.h>
#define ll long long
using namespace std;
int n,m,n1,cnt,a[1010][1010],le[22000001],ri[22000001],rt[22000001];
ll g[22000001],f[1010][1010],sum[1010][1010],p=1e9+7;
void insert(int &x,int l,int r,int t,ll k){
	if(!x)x=++cnt;
	(g[x]+=k)%=p;
	if(l==r)return;
	int mid=(l+r)/2;
	if(t<=mid)insert(le[x],l,mid,t,k);
	 else insert(ri[x],mid+1,r,t,k); 
}
ll query(int x,int l,int r,int t,int k){
	if(!x)return 0ll;
	if(l==t&&r==k)return g[x];
	int mid=(l+r)/2;
	if(mid>=k)return query(le[x],l,mid,t,k);
	 else if(t>mid)return query(ri[x],mid+1,r,t,k);
	  else return (query(le[x],l,mid,t,mid)+query(ri[x],mid+1,r,mid+1,k))%p;
}
int main(){
freopen("dp.in","r",stdin);
freopen("dp.out","w",stdout);	
	int i,j;
	scanf("%d%d%d",&n,&m,&n1);
	for(i=1;i<=n;i++)
	 for(j=1;j<=m;j++){
	 	scanf("%d",&a[i][j]);
	 }
	f[1][1]=1ll;sum[1][1]=1ll;
	for(i=1;i<=m;i++)sum[1][i]=1ll,sum[i][1]=1ll;
	insert(rt[a[1][1]],1,n1,1,1ll);
	for(i=2;i<=n;i++){
		sum[i][1]=sum[i-1][1];
	 for(j=2;j<=m;j++){
	 	f[i][j]=(sum[i-1][j-1]-query(rt[a[i][j]],1,n1,1,j-1)+p)%p;
	 	//printf("%d %d %lld\n",i,j,f[i][j]);
	 	sum[i][j]=(sum[i-1][j]+sum[i][j-1]-sum[i-1][j-1]+f[i][j])%p;
	 }  
	for(j=2;j<=m;j++)insert(rt[a[i][j]],1,n1,j,f[i][j]);
}
    printf("%lld",f[n][m]);
}

/*
4 4
1 1 1 1
1 3 2 1
1 2 4 1
1 1 1 1

10 10 20
17 20 18 14 10 20 9 5 11 14
2 20 3 2 6 12 20 14 7 20
4 16 18 20 11 20 7 6 19 4
17 18 20 1 8 20 3 16 10 20
5 20 17 20 10 20 1 12 16 20
20 15 2 19 20 20 3 20 13 18
14 6 10 2 12 1 18 19 3 19
17 20 11 15 12 20 2 14 20 1
12 17 10 20 17 16 20 13 5 10
20 11 20 6 1 1 6 17 8 17
*/
