#include<bits/stdc++.h>
#define ll long long
using namespace std;
inline int read(){
  int x = 0, f = 0;char ch = getchar();
  for (; ch <'0' || ch>'9'; ch = getchar()) if (ch == '-') f = 1;
  for (; ch >= '0'&& ch<='9'; ch = getchar()) x = (x<<1) + (x<<3) + ch - 48;
  return f ?-x :x;
}
#define N 505
const int mod = 1e9+7;
int n, m, k, a[N][N], g[N][N*N], p[N*N];
ll f[N][N];
int main() {
  n = read();
  m = read();
  k = read();
  for (int i = 1; i <= n; i++)
    for (int j = 1; j <= m; j++)
      a[i][j] = read();
  f[0][0] = 1;
  for (int i = 1; i < n; i++) {
    memset(p, 0, sizeof p);
    for (int j = 1; j < m; j++) {
      f[i][j] = (f[i-1][j] + f[i][j-1] + f[i-1][j-1])%mod;
      f[i][j] -= g[j-1][a[i][j]];
      f[i][j] += p[a[i][j]];
      f[i][j] %= mod;
      if (f[i][j] < 0) f[i][j] += mod;
      for (int K = j; K < m; K++) g[K][a[i][j]] = (g[K][a[i][j]]+f[i-1][j-1])%mod;
      p[a[i][j]] = (p[a[i][j]]+f[i-1][j-1])%mod;
    }
  }
/*  for (int i = 1; i <= n; i++) {
    for (int j = 1; j <= m; j++) cout << f[i][j] <<" ";
    cout << endl;
  }*/
  printf("%d\n",(f[n-1][m-1] - g[m-1][a[n][m]] + mod)%mod);
  return 0;
}