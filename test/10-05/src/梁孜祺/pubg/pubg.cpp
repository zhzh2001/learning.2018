#include<bits/stdc++.h>
#define ll long long
using namespace std;
inline ll read(){
  ll x = 0, f = 0;char ch = getchar();
  for (; ch <'0' || ch>'9'; ch = getchar()) if (ch == '-') f = 1;
  for (; ch >= '0'&& ch<='9'; ch = getchar()) x = (x<<1) + (x<<3) + ch - 48;
  return f ?-x :x;
}
ll x, y, X, Y;
int win, lost;
inline ll gcd(ll x, ll y) {
  if (!y) return x;
  return gcd(y, x%y);
}
int main() {
  freopen("pubg.in","r",stdin);
  freopen("pubg.out","w",stdout);
  while(scanf("%lld%lld%lld%lld", &x, &y, &X, &Y) != EOF) {
    ll disx = abs(x - X), disy = abs(y - Y);
    if (disx == 1 || disy == 1) lost++, puts("YE5");
    else if (disx == 0 && disy == 0) lost++, puts("YE5");
    else if (disx == 0 || disy == 0) win++, puts("NO");
    else {
      if (gcd(disx, disy) == 1) lost++, puts("YE5");
      else win++, puts("NO");
    }
  }
  if (win == lost) puts("Friend Ship");
  else if (win > lost) puts("Yahoo!");
  else puts("Poor xshen!");
  return 0;
}