#include <bits/stdc++.h>
#define int long long
using namespace std;
int a,b,c,d,xwin,ywin;
inline int read() {
	int X=0,w=0;
	char ch=0;
	while(!isdigit(ch)) {
		w|=ch=='-';
		ch=getchar();
	}
	while(isdigit(ch)) X=(X<<3)+(X<<1)+(ch^48),ch=getchar();
	return w?-X:X;
}
int gcd(int a,int b) {
	if(a<b) swap(a,b);
	if(a==0 && b==0) return 1;
	if(a==1 && b==0) return 1;
	if(a>=2 && b==0) return 0;
	int c=a%b;
	while(c) {a=b;b=c;c=a%b;}
	return b;
}
signed main() {
	freopen("pudg.in","r",stdin);
	freopen("pudg.out","w",stdout);
	while(scanf("%lld",&a)==1) {
		b=read();
		c=read();
		d=read();
		if(gcd(abs(a-c),abs(b-d))==1) {puts("YES");xwin++;}
		else {puts("NO");ywin++;}
	}
	if(xwin==ywin) { puts("Friend Ship."); } 
	else if(xwin<ywin) { puts("Yahoo!");} 
	else if(xwin>ywin) { puts("Poor xshen!");}
	return 0;
}