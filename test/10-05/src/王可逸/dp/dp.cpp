#include <bits/stdc++.h>
#define int long long
using namespace std;
int n,m,k,a[1200][1200],f[1200][1200];
inline int read() {
	int X=0,w=0;
	char ch=0;
	while(!isdigit(ch)) {
		w|=ch=='-';
		ch=getchar();
	}
	while(isdigit(ch)) X=(X<<3)+(X<<1)+(ch^48),ch=getchar();
	return w?-X:X;
}
void make(int x,int y) {
	for(int i=x+1;i<=n;i++)
		for(int j=y+1;j<=m;j++) {
			if(a[i][j]!=a[x][y]) {
				f[i][j]++;
				make(i,j);
			}
		}
}
signed main() {	
	freopen("dp.in","r",stdin);
	freopen("dp.out","w",stdout);
	n=read();
	m=read();
	k=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			a[i][j]=read();
	make(1,1);
	cout<<f[n][m]<<'\n';
	return 0;
}







// #include <bits/stdc++.h>
// using namespace std;
// const int mod=1e9+7;
// int dp[1200][1200],n,m,k;
// map <pair <int,int>,int> mp;
// inline int read() {
// 	int X=0,w=0;
// 	char ch=0;
// 	while(!isdigit(ch)) {
// 		w|=ch=='-';
// 		ch=getchar();
// 	}
// 	while(isdigit(ch)) X=(X<<3)+(X<<1)+(ch^48),ch=getchar();
// 	return w?-X:X;
// }
// int dis(int a,int b,int c,int d) {
	// if(mp [make_pair((a-1)*n+b,(c-1)*n+d)]) return mp [make_pair((a-1)*n+b,(c-1)*n+d)];
// 	int ans=0;
// 	for(int i=a+1;i<=c-1;i++)
// 		for(int j=b+1;j<=d-1;j++){
// 			ans=(ans+dis(a,b,i,j)*dis(i,j,c,d))%mod;
// 		}
// 	if(dp[a][b]!=dp[c][d]) ans++;
// 	mp[make_pair((a-1)*n+b,(c-1)*n+d)]=ans;
// 	cout<<a<<" "<<b<<" "<<c<<" "<<d<<" "<<ans<<'\n';
// 	return ans;
// }
// int main() {
// 	n=read();
// 	m=read();
// 	k=read();
// 	for(int i=1;i<=n;i++)
// 		for(int j=1;j<=m;j++)
// 			dp[i][j]=read();
// 	cout<<dis(1,1,n,m);

// }