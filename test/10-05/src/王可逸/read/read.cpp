#include <bits/stdc++.h>
using namespace std;
char a[120000];
int n;
void out_shu(int l, int r, int use) {
	int ll = l;
	if (use) {
		while (a[l] == '0') l++;
		if (l == r + 1) return;
		if (l != ll) cout << "0";
	} else {
		while (a[l] == '0') l++;
		if (l == r + 1) return;
	}

	int wei = r - l + 1;

	if (wei == 1) {//5
		cout << a[l] - '0';
	} else if (wei == 2) {
		cout << a[l] - '0';
		cout << "S";//50
		if (a[r] != '0') cout << a[r] - '0'; //55
	} else if (wei == 3) {
		cout << a[l] - '0';
		cout << "B";
		if (a[l + 1] == '0' && a[l + 2] == '0') return;//500
		if (a[l + 1] == '0') {
			cout << a[l + 1] - '0';//505
		} else {
			cout << a[l + 1] - '0';
			cout << "S";
		}//550
		if (a[l + 2] != '0') {
			cout << a[l + 2] - '0';
		}//555
	} else if (wei == 4) {
		cout << a[l] - '0';
		cout << "Q";
		if (a[l + 1] == '0' && a[l + 2] == '0' && a[l + 3] == '0') return;//5000
		if (a[l + 1] == '0' && a[l + 2] == '0' && a[l + 3] != '0') {
			cout << "0";
			cout << a[l + 3] - '0';//5005
		}
		if (a[l + 1] == '0' && a[l + 2] != '0' ) {
			cout << "0";
			cout << a[l + 2] - '0';
			cout << "S";//5050
			if (a[l + 3] != '0') {
				cout << a[l + 3] - '0';//5055
			}
		}
		if (a[l + 1] != '0' && a[l + 2] == '0' && a[l + 3] == '0') {
			cout << a[l + 1] - '0';
			cout << "B";
		}//5500
		if (a[l + 1] != '0' && a[l + 2] == '0' && a[l + 3] != '0') {
			cout << a[l + 1] - '0';
			cout << "B";
			cout << "0";
			cout << a[l + 3] - '0';
		}//5505
		if (a[l + 1] != '0' && a[l + 2] != '0' && a[l + 3] != '0') {
			cout << a[l + 1] - '0';
			cout << "B";
			cout << a[l + 2] - '0';
			cout << "S";
			cout << a[l + 3] - '0';
		}//5555
	}
}

void out_zheng(int l, int r) {
	int wei = r - l + 1;
	if (wei <= 4) out_shu(l, r, 0);
	else if (wei <= 8) {
		out_shu(l, r - 4, 0);
		cout << "W";
		out_shu(r - 3, r, 1);
	} else {
		out_shu(l, r - 8, 0);
		cout << "Y";
		bool flag = true;
		for (int i = r - 7; i <= r - 4; i++)
			if (a[i] != '0') flag = false;
		if (flag) {
			cout << "0";
			out_shu(r - 3, r, 0);
		} else {
			out_shu(r - 7, r - 4, 1);
			cout << "W";
			out_shu(r - 3, r, 1);
		}
	}
}

int main() {
	freopen("read.in","r",stdin);
	freopen("read.out","w",stdout);

	scanf("%s", a + 1);
	n = strlen(a + 1);
	bool fen = false;
	int fen_at;
	for (int i = 1; i <= n; i++)
		if (a[i] == '/') {
			fen = true;
			fen_at = i;
			break;
		}

	if (!fen) {
		bool dian = false;
		int dian_at;
		for (int i = 1; i <= n; i++) {
			if (a[i] == '.') {
				dian = true;
				dian_at = i;
				break;
			}
		}
		if (!dian) {
			int l = 1;
			bool fu=false;
			if (a[l] == '-') {
				fu=true;
				l++;
			}
			while (a[l] == '0') l++;
			if(l==n+1) cout<<"0";
			else {
				if(fu) cout<<"F";
			}
			out_zheng(l, n);
		} else {
			bool zero = true,fu=false;
			int l = 1;
			bool ss=true;
			for(int i=2;i<=n;i++) {
				if(a[i]!='0' && a[i]!='.'){
					ss=false;	
					break;
				}
			}
			if(ss && a[1]=='-') {
				cout<<"0";
				return 0;	
			}
			if (a[l] == '-') {
				cout<<"F";
				l++;
			}
			for (int i = l; i <= dian_at - 1; i++)
				if (a[i] != '0') zero = false;
			if (zero) cout << "0";
			else {
				while (a[l] == '0') l++;
				out_zheng(l, dian_at - 1);
			}
			zero = true;
			for (int i = dian_at + 1; i <= n; i++)
				if (a[i] != '0') zero = false;
			if (!zero) {
				cout << "D";
				for (int i = dian_at + 1; i <= n; i++) cout << a[i] - '0';
			}
		}
	} else {
		//分数特殊情况
		bool zi_zero = true;
		for (int i = 2; i <= fen_at - 1; i++)
			if (a[i] != '0' && a[i] != '.') {
				zi_zero = false;
				break;
			}
		if(a[1]!='-' && a[1]!='0') zi_zero=false; 
		if (zi_zero) {
			cout << "0";
			return 0;
		}
		bool mu_one = true;
		bool dd = false;
		int dd_at = 0;
		for (int i = fen_at + 1; i <= n; i++) {
			if (a[i] == '.') {
				dd = true;
				dd_at = i;
				break;
			}
		}
		if (dd) {
			for (int i = fen_at + 1; i <= dd_at - 2; i++)
				if (a[i] != '0') {
					mu_one = false;
					break;
				}
			if (a[dd_at - 1] != '1') {
				mu_one = false;
			}
			for (int i = dd_at + 1; i <= n; i++) {
				if (a[i] != '0') {
					mu_one = false;
					break;
				}
			}
		} else {
			for (int i = fen_at + 1; i <= n - 1; i++) {
				if (a[i] != '0') {
					mu_one = false;
					break;
				}
			}
			if (a[n] != '1') {
				mu_one = false;
			}
		}
		
		bool fuyi=true,ddd;
		int at;
		for(int i=fen_at+1;i<=n;i++)
			if(a[i]=='.') {
				ddd=true;
				at=i;
				break;
			}
		if(ddd) {
			for(int i=fen_at+2;i<=at-2;i++)
				if(a[i]!='0') {
					fuyi=false;
					break;
				}
			if(a[fen_at+1]!='-' || a[at-1]!='1') fuyi=false;
			for(int i=at+1;i<=n;i++) {
				if(a[i]!='0') {
					fuyi=false;
					break;
				}
			}
		} else {
			for(int i=fen_at+2;i<=n-1;i++)
				if(a[i]!='0') {
					fuyi=false;
					break;
				}
			if(a[fen_at+1]!='-' || a[n]!='1') {
				fuyi=false;
			}
		}
		
		if (mu_one || fuyi) {
			bool dian = false;
			int dian_at = 0;
			for (int i = 1; i <= fen_at - 1; i++) {
				if (a[i] == '.') {
					dian = true;
					dian_at = i;
					break;
				}
			}
			if (!dian) {
				int l = 1;
				bool fu=false;
				if (a[l] == '-') {
					if(!fuyi) cout<<"F";
					l++;
				} else if(fuyi) cout<<"F";
				while (a[l] == '0') l++;
				if(l==fen_at) cout<<"0";
				out_zheng(l, fen_at - 1);
			} else {
				bool zero = true,fu=false;
				int l = 1;
				if (a[l] == '-') {
					if(!fuyi) cout<<"F";
					l++;
				} else if(fuyi) cout<<"F";
				for (int i = l; i <= dian_at - 1; i++)
					if (a[i] != '0') zero = false;
				if (zero) cout << "0";
				else {
					while (a[l] == '0') l++;
					out_zheng(l, dian_at - 1);
				}
				zero = true;
				for (int i = dian_at + 1; i <= fen_at - 1; i++)
					if (a[i] != '0') zero = false;
				if (!zero) {
					cout << "D";
					for (int i = dian_at + 1; i <= fen_at - 1; i++) cout << a[i] - '0';
				}
			}
			return 0;
		}







		//
		bool dian = false;
		int dian_at = 0;
		for (int i = fen_at + 1; i <= n; i++) {
			if (a[i] == '.') {
				dian = true;
				dian_at = i;
				break;
			}
		}

		if (!dian) {
			int l = fen_at + 1;
			bool fu=false;
			if (a[l] == '-') {
				fu=true;
				l++;
			}
			while (a[l] == '0') l++;
			if(l==n+1) {
				cout<<"0";
			} else {
				if(fu) cout<<"F";
			}
			out_zheng(l, n);
		} else {
			bool zero = true,fu=false;
			int l = fen_at + 1;
			if (a[l] == '-') {
				cout<<"F";
				l++;
			}
			for (int i = l; i <= dian_at - 1; i++)
				if (a[i] != '0') zero = false;
			if (zero) cout << "0";
			else {
				while (a[l] == '0') l++;
				out_zheng(l, dian_at - 1);
			}
			zero = true;
			for (int i = dian_at + 1; i <= n; i++)
				if (a[i] != '0') zero = false;
			if (!zero) {
				cout << "D";
				for (int i = dian_at + 1; i <= n; i++) cout << a[i] - '0';
			}
		}

		cout << "FZ";

		dian = false;
		dian_at = 0;
		for (int i = 1; i <= fen_at - 1; i++) {
			if (a[i] == '.') {
				dian = true;
				dian_at = i;
				break;
			}
		}
		if (!dian) {
			int l = 1;
			bool fu=false;
			if (a[l] == '-') {
				fu=true;
				l++;
			}
			while (a[l] == '0') l++;
			if(l==fen_at) {
				cout<<"0";
			} else {
				if(fu) cout<<"F";
			}
			out_zheng(l, fen_at - 1);
		} else {
			bool zero = true,fu=false;
			int l = 1;
			if (a[l] == '-') {
				cout<<"F";
				l++;
			}
			for (int i = l; i <= dian_at - 1; i++)
				if (a[i] != '0') zero = false;
			if (zero) {
				cout << "0";
			} else {
				while (a[l] == '0') l++;
				out_zheng(l, dian_at - 1);
			}
			
			zero = true;

			for (int i = dian_at + 1; i <= fen_at - 1; i++)
				if (a[i] != '0') zero = false;
			if (!zero) {
				cout << "D";
				for (int i = dian_at + 1; i <= fen_at - 1; i++) cout << a[i] - '0';
			}
		}
	}
}
