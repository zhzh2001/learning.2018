#include<bits/stdc++.h>
#define LL long long
#define GG long long 
#define Ull unsigned long long
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
using namespace std;
inline GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
inline void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
inline void writeln(GG x) {
    write(x); puts("");
}

inline Ull gcd(Ull x, Ull y) {
	if(x==0 || y==0) return 1;  
	if(x%y==0) return y; 
	else return gcd(y, x%y); 
}

LL xs, ys, xt, yt, a, b; 

int main() {
	freopen("pubg.in","r",stdin); 
	freopen("pubg.out","w",stdout); 
	while(~scanf("%lld%lld%lld%lld", &xs, &ys, &xt, &yt)) {
		if(xt<xs) swap(xs, xt);
		if(yt<ys) swap(ys, yt);
		Ull x = xt-xs; 
		Ull y = yt-ys; 
		if(gcd(x, y)==1) { puts("YE5"); ++a; }
		else{ puts("NO"); ++b; }
	}
	if(a>b) puts("Poor xshen!"); 
	if(a==b) puts("Friend Ship."); 
	if(a<b) puts("Yahoo!"); 
}


