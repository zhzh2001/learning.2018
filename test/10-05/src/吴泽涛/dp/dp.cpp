#include<bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
using namespace std;
inline GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
inline void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
inline void writeln(GG x) {
    write(x); puts("");
}

const int N = 101, Mod = 1000000007; 
int n, m, K, ans; 
int a[N][N], f[7000][N][N]; 

int main() {
	freopen("dp.in","r",stdin); 
	freopen("dp.out","w",stdout); 
	n = read(); m = read(); K = read(); 
	For(i, 1, n) 
		For(j, 1, m) a[i][j] = read(); 
	f[0][1][1] = 1; 
	f[a[1][1]][1][1] = 1; 
	For(i, 1, n) 
	  For(j, 1, m) {
	  	if(i==1 &&j==1) continue; 
		For(k, 1, K) 
		  f[k][i][j] = (1ll*f[k][i-1][j]+f[k][i][j-1]-f[k][i-1][j-1]) %Mod; 
	    int tot = (1ll*f[0][i-1][j-1]-f[a[i][j]][i-1][j-1]) %Mod;  	  
		f[a[i][j]][i][j] = (1ll*f[a[i][j]][i][j] + tot) % Mod;   
		f[0][i][j] = (1ll*f[0][i-1][j]+f[0][i][j-1]-f[0][i-1][j-1]+tot) %Mod;     
	  }
	ans = (1ll*f[0][n][m]-f[0][n-1][m]-f[0][n][m-1]+f[0][n-1][m-1]) %Mod; 
	writeln(ans); 
}


/*

4 4 4
1 1 1 1
1 3 2 1
1 2 4 1
1 1 1 1


1000000007



*/
