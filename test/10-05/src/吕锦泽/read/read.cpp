#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 1000005
#define mod 1000000007
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n; char s[N];
ll get(ll l,ll r){
	ll num=0; if (s[l]=='-') ++l;
	rep(i,l,r) num=num*10+s[i]-'0';
	return abs(num);
}
void doit(ll x,ll tmp){
	ll flag=0;
	for (ll i=1000;i;i/=10){
		if (x/i==0) flag=1;
		else {
			if (tmp) flag=tmp=0;
			if (flag) putchar('0');
			flag=0;
			putchar(x/i+'0'); x-=x/i*i;
			if (i==1000) putchar('Q');
			else if (i==100) putchar('B');
			else if (i==10) putchar('S');
		}
	}
}
void print(ll x){
	if (x>=100000000){
		doit(x/100000000,1);
		putchar('Y'); x%=100000000;
		if (x>=10000){
			doit(x/10000,0),putchar('W');
			x%=10000;
			if (x>=1) doit(x,0);
			else putchar('0');
		}
		else{
			if (x>=1){
				putchar('0');
				doit(x,1);
			}
		}
	}else if (x>=10000){
		doit(x/10000,1); putchar('W');
		x%=10000;
		if (x>=1) doit(x,0);
	}else doit(x,1);
}
void work2(ll l,ll n){
	ll pos=0;
	rep(i,l,n) if (s[i]=='.') pos=i;
	while (pos!=0&&s[n]=='0'&&n) --n;
	if (pos==0) pos=n+1;
	ll le=get(l,pos-1); le=abs(le);
	if (le==0) putchar('0');else print(le);
	if (n>pos) putchar('D');
	if (n>pos) rep(i,pos+1,n) putchar(s[i]);
}

void work1(){
	ll pos=0,posl=0,posr=0;
	rep(i,1,n) if (s[i]=='/') pos=i;
	rep(i,pos+1,n) if (s[i]=='.') posr=i;
	rep(i,1,pos-1) if (s[i]=='.') posl=i;
	ll boo=0;
	rep(i,pos+1,n)  if (s[i]>='1'&&s[i]<='9') ++boo;
	if ((posr==0&&boo==1&&s[n]=='1')||(posr!=0&&s[posr-1]=='1'&&boo==1)){
		work2(1,pos-1);
		return;
	}
	boo=0;
	if (posl!=0){
		rep(i,1,pos-1) if (s[i]>='1'&&s[i]<='9') ++boo;
		if (boo==0){
			putchar('0');
			return;
		}
	}
	work2(pos+1,n); printf("fz"); work2(1,pos-1);
}
int main(){
	freopen("read.in","r",stdin);
	freopen("read.out","w",stdout);
	scanf("%s",s+1); n=strlen(s+1); ll num=0;
	rep(i,1,n) if (s[i]=='-') ++num;
	if (num%2) putchar('F');
	rep(i,1,n) if (s[i]=='/'){
		work1();
		return 0;
	}
	work2(1,n);
}
