#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 1005
#define mod 1000000007
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,k,a[N][N],f[N][N];
int main(){
	freopen("dp.in","r",stdin);
	freopen("dp.out","w",stdout);
	n=read(); m=read(); k=read();
	rep(i,1,n) rep(j,1,m) a[i][j]=read();
	f[1][1]=1;
	rep(i,2,n) rep(j,2,m){
		rep(k,1,i-1) rep(l,1,j-1)
			if (a[i][j]!=a[k][l]) f[i][j]+=f[k][l];
		f[i][j]%=mod;
	}
	printf("%lld",f[n][m]);
}
