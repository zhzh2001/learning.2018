#include <map>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
using namespace std;
int rest;
string s, ans, Ans;
char c[4] = {'\0', 'S', 'B', 'Q'};
map<char, int>level;
bool f;
void solve(string s, int len){
	for (int i = 0; i < len; i++) {
		ans += s[i];
		if (s[i] ^ '0' && (len - i - 1) % 4) ans += c[(len - i - 1) % 4];
		rest--;
		if (rest == 8) ans += 'Y';
		if (rest == 4) ans += 'W';
	}
	for(int i = 0; i < (int)ans.size() - 1;) {
		if(ans[i] == '0' && ans[i + 1] == '0') ans.erase(i, 1), i--;
		else i++;
	}
	for(int i = 0; i < (int)ans.size(); i++)
		if((ans[i] == 'Y' || ans[i] == 'W') && ans[i - 1] == '0') ans.erase(i - 1, 1);
	for(int i = 1; i < (int)ans.size(); i++)
		if(!isdigit(ans[i]) && !isdigit(ans[i - 1]) && level[ans[i]] < level[ans[i - 1]]) ans.erase(i, 1);
	while(ans.size() && ans[ans.size() - 1] == '0') ans.erase(ans.size() - 1, 1);
}
void Work(string s){
	int cnt = 0;
	ans.clear();
	int hasdot = s.find('.');
	while (!isdigit(s[0]) && s[0] != '.') {
		if (s[0] == '-') cnt++;
		s.erase(0, 1);
	}
	while (s.size() && s[0] == '0') s.erase(0, 1);
	while (~hasdot && s.size() && s[s.size() - 1] == '0') s.erase(s.size() - 1, 1);
	if ((~hasdot && s.size() == 1) || !s.size()) {
		puts("0");
		exit(0);
	}
	if (cnt & 1) f ^= 1;
	int p = s.find('.');
	if(~p) rest = p;
	else rest = s.size();
	if(~p) solve(s, p);
	else{
		solve(s, s.size());
		return;
	}
	if (p == (int)s.size() - 1) return;
	if(s[0] == '.') ans += '0';
	ans += 'D';
	for (int i = p + 1; i < (int)s.size(); i++) ans += s[i];
}
int main() {
	freopen("read.in", "r", stdin);
	freopen("read.out", "w", stdout);
	level['S'] = 0; level['B'] = 2; level['Q'] = 3; level['W'] = 4; level['Y'] = 5;
	cin >> s;
	if(s.find('/') == string::npos) {
		Work(s);
		if(f) ans = 'F' + ans;
		cout << ans << endl;
	}else{
		int pp = s.find('/');
		Work(s.substr(0, pp));
		Ans = ans;
		Work(s.substr(pp + 1, s.size()));
		if(ans != "1") {
			Ans = "fz" + Ans;
			Ans = ans + Ans;
		}
		if(f) Ans = 'F' + Ans;
		cout << Ans;
	}
}


