#include <iostream>
#include <cstdio>
#include <queue>
#include <algorithm>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
#define dd c=gc()
inline ll read(){
	ll x=0,f=1;char dd;
	for(;!isdigit(c);dd){if(c=='-')f=-1;if(c==EOF)return EOF;}
	for(;isdigit(c);dd)x=(x<<1)+(x<<3)+(c^48);return x*f;
}
#undef dd
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=1010,P=1e9+7;
ll a[N][N],f[N][N],tot,n,m,k,t[N*N],s[N*N];
inline void solve(ll l,ll r){
	if (l==r) return;
	ll mid=(l+r)>>1;
	solve(l,mid);
	++tot;
	ll sum=0;
	for (ll i=1;i<=m;++i){
		for (ll j=r;j>mid;--j){
			if (t[a[i][j]]<tot) {t[a[i][j]]=tot;s[a[i][j]]=0;}
			f[i][j]=(f[i][j]+sum-s[a[i][j]])%P;
		}
		for (ll j=l;j<=mid;++j){
			if (t[a[i][j]]<tot) {t[a[i][j]]=tot;s[a[i][j]]=0;}
			(s[a[i][j]]+=f[i][j])%=P;
			(sum+=f[i][j])%=P;
		}
	}
	solve(mid+1,r);
}
int main(){
	freopen("dp.in","r",stdin);
	freopen("dp.out","w",stdout);
	n=read(),m=read(),k=read();
	for (ll i=1;i<=n;++i) for (ll j=1;j<=m;++j) a[i][j]=read();
	f[1][1]=1;
	solve(1,n);
	wr((f[n][m]+P)%P);
	return 0;
}
