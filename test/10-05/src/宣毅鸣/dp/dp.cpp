#include<bits/stdc++.h>
using namespace std;
const int N=1005,M=1e9+7;
vector<int >v1[N*N],v2[N*N];
int a[N][N],cnt[N*N],n,m,b[N][N],sum[N],k,fff[N*N],ff[N*N],dp2[N][N],dp[N][N],f[N][N];
int cmp(int x,int y){
	return cnt[x]>cnt[y];
}
int read(){
	int x=0;char c;
	for (;c<'0'||c>'9';c=getchar());
	for (;c>='0'&&c<='9';c=getchar())x=x*10+c-48;
	return x;
}
int main(){
	freopen("dp.in","r",stdin);
	freopen("dp.out","w",stdout);
	scanf("%d%d%d",&n,&m,&k);
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++)a[i][j]=read(),cnt[a[i][j]]++;
	for (int i=1;i<=k;i++)ff[i]=i;
	sort(ff+1,ff+k+1,cmp);
	for (int i=1;i<=min(k,100);i++)fff[ff[i]]=i;
	for (int i=1;i<=n;i++)
	 for (int j=1;j<=m;j++)b[i][j]=fff[a[i][j]];
	sum[1]=f[1][1]=1;
	if (b[1][1])
		for (int i=1;i<=m;i++)dp[1][b[1][1]]=dp2[i][b[1][1]]=1;
	else v1[a[1][1]].push_back(1),v2[a[1][1]].push_back(1);	
	k=min(100,k);
	for (int i=2;i<=n;i++){
		int num=0;
		for (int j=1;j<=m;j++)
			if (b[i][j]!=0){
				f[i][j]=(num-dp2[j-1][b[i][j]]+M)%M;
				num+=sum[j];
				sum[j]+=f[i][j];
			} 
			else {
				int s=0;
				for (int kk=0;kk<v1[a[i][j]].size();kk++)
					if (v1[a[i][j]][kk]<=j-1)(s+=v2[a[i][j]][kk])%=M;
				f[i][j]=(num-s+M)%M;
				num+=sum[j];
				sum[j]+=f[i][j];
			}
		for (int j=1;j<=m;j++){
			if (b[i][j]!=0)(dp[j][b[i][j]]+=f[i][j])%=M;
			else {
				v1[a[i][j]].push_back(j);
				v2[a[i][j]].push_back(f[i][j]);
			}
			for (int l=1;l<=k;l++)dp2[j][l]=(dp2[j-1][l]+dp[j][l])%M;			
		}
	}	
	printf("%d",f[n][m]);
	return 0;
}
