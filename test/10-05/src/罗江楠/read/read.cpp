#include <bits/stdc++.h>
#define N 1000020
#define ll long long
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}

const bool ___debug = false;

char res[N];
int top;
void _putchar(char c) {
  res[++ top] = c;
}

char str[N];

int find(int l, int r, char c) {
  for (int i = l; i <= r; ++ i) {
    if (str[i] == c) {
      return i;
    }
  }
  return -1;
}
bool allZero(int l, int r) {
  for (int i = l; i <= r; ++ i) {
    if (str[i] != '0' && str[i] != '-') {
      return false;
    }
  }
  return true;
}
bool isZero(int l, int r) {
  int dot = find(l, r, '.');
  if (dot == -1) {
    return allZero(l, r);
  } else {
    return allZero(l, dot - 1) && allZero(dot + 1, r);
  }
}
bool is1(int l, int r) {
  if (str[l] == '-') return false;
  int dot = find(l, r, '.');
  if (dot == -1) {
    return allZero(l, r - 1) && str[r] == '1';
  } else {
    return allZero(l, dot - 2) && str[dot - 1] == '1' && allZero(dot + 1, r);
  }
}

void speakInteger(int start, int end) {

  if (allZero(start, end)) {
    _putchar('0');
    return;
  }

  int len = end - start + 1;
  if (len > 8) {
    // 有亿位
    // 可以保证前面不会都是 0
    speakInteger(start, end - 8);
    _putchar('Y');
    start = end - 8 + 1;
    len = 8;
  }

  bool still_nothing = false;

  if (len > 4) {
    // 有万位，可能都是 0
    bool nothing = allZero(start, end - 4);
    if (!nothing) {
      speakInteger(start, end - 4);
      _putchar('W');
    } else {
      still_nothing = true;
    }
    start = end - 4 + 1;
    len = 4;
  }

  if (len > 3) {
    bool nothing = str[start] == '0';
    if (!nothing) {
      if (still_nothing) {
        _putchar('0');
        still_nothing = false;
      }
      _putchar(str[start]);
      _putchar('Q');
    } else {
      still_nothing = true;
    }
    ++ start;
    len = 3;
  }

  if (len > 2) {
    bool nothing = str[start] == '0';
    if (!nothing) {
      if (still_nothing) {
        _putchar('0');
        still_nothing = false;
      }
      _putchar(str[start]);
      _putchar('B');
    } else {
      still_nothing = true;
    }
    ++ start;
    len = 2;
  }

  if (len > 1) {
    bool nothing = str[start] == '0';
    if (!nothing) {
      if (still_nothing) {
        _putchar('0');
        still_nothing = false;
      }
      _putchar(str[start]);
      _putchar('S');
    } else {
      still_nothing = true;
    }
    ++ start;
    len = 1;
  }

  if (str[start] != '0') {
    if (still_nothing) {
      _putchar('0');
    }
    _putchar(str[start]);
  }

}

void speak(int start, int end) {

  if (isZero(start, end)) {
    _putchar('0');
    return;
  }

  if (str[start] == '-') {
    _putchar('F');
    ++ start;
  }

  int dot = find(start, end, '.');

  if (dot > -1) {

    while (str[start] == '0' && start < dot - 1)
      ++ start;

    speakInteger(start, dot - 1);

    if (!allZero(dot + 1, end)) {

      _putchar('D');

      while (str[end] == '0' && end > dot + 1)
        -- end;

      for (int i = dot + 1; i <= end; ++ i) {
        _putchar(str[i]);
      }
    }

  } else {

    while (str[start] == '0' && start < end)
      ++ start;
    speakInteger(start, end);

  }

}

void work() {

  int len = strlen(str + 1);
  int fs = find(1, len, '/');
  if (___debug) printf("fs = %d\n", fs);

  if (fs > -1) {

    if (isZero(1, fs - 1)) {
      _putchar('0');
    } else if (is1(fs + 1, len)) {
      speak(1, fs - 1);
    } else {
      speak(fs + 1, len);
      _putchar('f');
      _putchar('z');
      speak(1, fs - 1);
    }

  } else {

    speak(1, len);

  }

}
/*
bool it(const char* s, const char* res) {
  int len = strlen(s);
  for (int i = 0; i < len; ++ i) {
    str[i + 1] = s[i];
  }
  str[len + 1] = '\0';
  top = 0;
  work();
  ::res[top + 1] = '\0';

  int res_len = strlen(res);
  if (top != res_len) {
    return false;
  }
  for (int i = 1; i <= top; ++ i) {
    if (::res[i] != res[i - 1]) {
      return false;
    }
  }
  return true;
}
int _psd, _all;
void describe(const char* s, const char* res) {
  ++ _all;
  printf("Input:  %s\n", s);
  printf("Answer: %s\n", res);
  bool passed = it(s, res);
  printf("Output: %s\n", ::res + 1);
  if (!passed) {
    puts("Error!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
  } else {
    ++ _psd;
    puts("Passing");
  }
  puts("");
}

void run_test() {
  describe("-000.89", "F0D89");
  describe("-0020030004.56700", "F2Q03W04D567");
  describe("10/10", "1Sfz1S");
  describe("-165.000", "F1B6S5");
  describe("-0000000000.000", "0");
  describe(".2333", "0D2333");
  describe("-.2333", "F0D2333");
  describe("-.000", "0");
  describe("-.000/-566.000", "0");
  describe("-854/0001.000", "F8B5S4");
  describe("100000000", "1Y");
  describe("1000000000", "1SY");
  describe("1100010000", "1S1Y01W");
  describe("1101010000", "1S1Y01B01W");
  describe("1101010101", "1S1Y01B01W01B01");
  describe("1101011101", "1S1Y01B01W1Q1B01");
  describe("-1101011101", "F1S1Y01B01W1Q1B01");
  describe("-1101011101.000", "F1S1Y01B01W1Q1B01");
  describe("-10000000001.000", "F1BY01");
  describe("-10000100001.000", "F1BY01SW01");
  describe("-12345678910.1415926535", "F1B2S3Y4Q5B6S7W8Q9B1SD1415926535");
  describe("-12345608910.1415926535", "F1B2S3Y4Q5B6SW8Q9B1SD1415926535");
  printf("%d / %d passed\n", _psd, _all);
}
*/
int main(int argc, char const *argv[]) {
  freopen("read.in", "r", stdin);
  freopen("read.out", "w", stdout);

  // run_test();
  scanf("%s", str + 1);
  work();
  puts(::res + 1);

  return 0;
}