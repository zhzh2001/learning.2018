#pragma GCC optimize("Ofast")
#include <bits/stdc++.h>
#define N 1020
#define mod 1000000007
#define ll long long
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}
int col[N][N];
int f[N][N];
int res[N*N];
int row[N];
int main(int argc, char const *argv[]) {
  freopen("dp.in", "r", stdin);
  freopen("dp.out", "w", stdout);
  int n = read(), m = read(), k = read();
  for (int i = 1; i <= n; ++ i) {
    for (int j = 1; j <= m; ++ j) {
      col[i][j] = read();
    }
  }

  f[1][1] = 1;
  row[1] = 1;

  for (int i = 2; i <= n; ++ i) {
    int sum = 0;
    memset(res, 0, sizeof res);
    for (int j = 2; j <= m; ++ j) {
      for (int k = 1; k < i; ++ k) {
        res[col[k][j-1]] = (res[col[k][j-1]] + f[k][j-1]) % mod;
      }
      sum = (sum + row[j - 1]) % mod;
      f[i][j] = (sum - res[col[i][j]] + mod) % mod;
    }
    for (int j = 1; j <= m; ++ j) {
      row[j] = (row[j] + f[i][j]) % mod;
      // res[col[i][j]] = (res[col[i][j]] + f[i][j]) % mod;
    }
  }

  printf("%d\n", f[n][m]);

  return 0;
}