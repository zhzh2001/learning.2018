#include <bits/stdc++.h>
#define N 1020
#define mod 1000000007
#define ll long long
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}
int col[N][N];
int f[N][N];
int res[N*N];
int row[N];

ll frac[N<<1];

ll fast_pow(ll x, ll y) {
  ll z = 1;
  for (; y; y >>= 1, x = x * x % mod) {
    if (y & 1) {
      z = z * x % mod;
    }
  }
  return z;
}

ll C(ll n, ll m) {
  return frac[n] * fast_pow(frac[n - m], mod - 2) % mod
    * fast_pow(frac[m], mod - 2) % mod;
}

ll calc(int x1, int y1, int x2, int y2) {
  ll a = abs(x1 - x2);
  ll b = abs(y1 - y2);
  return C(a + b - 2, a - 1);
}

int main(int argc, char const *argv[]) {
  freopen("dp.in", "r", stdin);
  // freopen("dp.out", "w", stdout);

  for (int i = frac[0] = 1; i <= 2e3; ++ i) {
    frac[i] = frac[i - 1] * i % mod;
  }

  int n = read(), m = read(), k = read();
  for (int i = 1; i <= n; ++ i) {
    for (int j = 1; j <= m; ++ j) {
      col[i][j] = read();
    }
  }

  ll ans = calc(1, 1, n, m);

  printf("%d\n", ans);

  for (int i = 2; i < n; ++ i) {
    for (int j = 2; j < m; ++ j) {
      f[i][j] = col[i][j] == col[1][1];
      for (int k = 2; k < i; ++ k) {
        for (int l = 2; l < j; ++ l) {
          if (col[i][j] == col[k][l]) {
            ++ f[i][j];
          }
        }
      }
      ll res = f[i][j] * calc(i, j, n, m) % mod;
      ans = (ans - res + mod) % mod;
    }
  }

  ll rsk = col[n][m] == col[1][1];
  for (int i = 2; i < n; ++ i) {
    for (int j = 2; j < m; ++ j) {
      if (col[i][j] == col[n][m]) {
        rsk += f[i][j];
      }
    }
  }
  ans = (ans - rsk + mod) % mod;

  printf("%d\n", ans);

  return 0;
}