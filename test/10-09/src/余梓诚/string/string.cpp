#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

void judge(){
    freopen("string.in","r",stdin);
    freopen("string.out","w",stdout);
}

const int maxn=5100;
int n,ans;
bool f[30][30],vis[maxn],used[maxn];
string s[maxn];

int main(){
	judge();
	scanf("%d",&n);
	for (int i=1;i<=n;i++) cin>>s[i];
	for (int t=1;t<=n;t++){
		int len=s[t].length();
		bool flag=1;
		memset(vis,0,sizeof(vis));
		memset(f,0,sizeof(f));
		for (int i=0;i<len;i++){
			for (int j=1;j<=n;j++){
				if (i==j||vis[j]) continue;
				int Len=s[j].length();
				//if (s[t]=="dkcniodq"&&s[j]=="dkcn") cout<<"#"<<i<<" "<<Len<<endl;
				if (i>=Len||f[s[j][i]-'a'][s[t][i]-'a']){
					flag=0;
					break;
				}
				if (s[t][i]!=s[j][i]) vis[j]=1,f[s[t][i]-'a'][s[j][i]-'a']=1;
			}
			if (!flag) break;
		}
		used[t]=flag; ans+=flag;
	}
	printf("%d\n",ans);
	for (int i=1;i<=n;i++){
		if (used[i]) cout<<s[i]<<endl;
	}
	return 0;
}
