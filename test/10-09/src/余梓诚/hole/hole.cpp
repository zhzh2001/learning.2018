#include <queue>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

void judge(){
    freopen("hole.in","r",stdin);
    freopen("hole.out","w",stdout);
}
inline char read(){
    static const int IN_LEN=1000000;
    static char buf[IN_LEN],*s,*t;
    return (s==t?t=(s=buf)+fread(buf,1,IN_LEN,stdin),(s==t?-1:*s++):*s++);
}
template<class T>
inline void read(T &x){
    static bool iosig;
    static char c;
    for (iosig=false,c=read();!isdigit(c);c=read()){
        if (c=='-') iosig=true;
        if (c==-1) return;
    }
    for (x=0;isdigit(c);c=read()) x=((x+(x<<2))<<1)+(c^'0');
    if (iosig) x=-x;
}
const int OUT_LEN=10000000;
char obuf[OUT_LEN],*ooh=obuf;
inline void print(char c){
    if (ooh==obuf+OUT_LEN) fwrite(obuf,1,OUT_LEN,stdout),ooh=obuf;
    *ooh++=c;
}
template<class T>
inline void print(T x){
    static int buf[30],cnt;
    if (x==0) print('0');
    else{
        if (x<0) print('-'),x=-x;
        for (cnt=0;x;x/=10) buf[++cnt]=x%10+48;
        while (cnt) print((char)buf[cnt--]);
    }
}
inline void flush(){
	fwrite(obuf,1,ooh-obuf,stdout);
}

const int maxn=210000,inf=1<<30;
int n,m,pre[maxn],s,t,loc[maxn];
long long dis[maxn],Min,flow[maxn],v[maxn];
bool used[maxn];
struct info{
	int to,nxt,w;
}e[maxn<<1];
int head[maxn],opt;
void add(int x,int y,int z,int val){
	e[opt].w=val; e[opt].to=y; e[opt].nxt=head[x]; v[opt]=z; head[x]=opt++;
}

struct thing{
	int pos,val;
}a[maxn];

bool spfa(int s,int t){
	queue<int> q;
	while (q.size()) q.pop();
	for (int i=0;i<=t;i++) dis[i]=inf,pre[i]=-1,used[i]=0;
	q.push(s); dis[s]=0; used[s]=1; flow[s]=inf;
	while (q.size()){
		int u=q.front(); q.pop();
		for (int i=head[u];i!=-1;i=e[i].nxt){
			int k=e[i].to,w=e[i].w;
			if (dis[k]>dis[u]+w&&v[i]>0){
				dis[k]=dis[u]+w;
				pre[k]=i; flow[k]=min(flow[u],v[i]);
				if (!used[k]) used[k]=1,q.push(k);
			}
		}
		used[u]=0;
	}
	return dis[t]!=inf;
}

void run(){
	while (spfa(s,t)){
		Min+=flow[t]*dis[t];
		int now=t;
		while (now!=s){
			int u=pre[now];
			v[u]-=flow[t];
			v[u^1]+=flow[t];
			now=e[u^1].to;
		}
	}
}

int abs(int x){
	return x<0?-x:x;
}

int main(){
	judge();
	memset(head,-1,sizeof(head));
	read(n),read(m);
	int tot=n+m+1;
	for (int i=1;i<=n;i++) read(loc[i]);
	for (int i=1;i<=m;i++) read(a[i].pos),read(a[i].val);
	for (int i=1;i<=n;i++){
		for (int j=1;j<=m;j++){
			add(i,n+j,inf,abs(a[j].pos-loc[i]));
			add(n+j,i,0,-abs(a[j].pos-loc[i]));
		}
	}
	for (int i=1;i<=n;i++){
		add(0,i,1,0);
		add(i,0,0,0);
	}
	for (int i=1;i<=m;i++){
		add(i+n,tot,a[i].val,0);
		add(tot,i+n,0,0);
	}
	s=0; t=tot;
	run();
	print(Min),print('\n');
	return flush(),0;
}
