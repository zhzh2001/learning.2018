#include <cstdio>
#include <vector>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

#define pb push_back

inline char read(){
    static const int IN_LEN=1000000;
    static char buf[IN_LEN],*s,*t;
    return (s==t?t=(s=buf)+fread(buf,1,IN_LEN,stdin),(s==t?-1:*s++):*s++);
}
template<class T>
inline void read(T &x){
    static bool iosig;
    static char c;
    for (iosig=false,c=read();!isdigit(c);c=read()){
        if (c=='-') iosig=true;
        if (c==-1) return;
    }
    for (x=0;isdigit(c);c=read()) x=((x+(x<<2))<<1)+(c^'0');
    if (iosig) x=-x;
}
const int OUT_LEN=10000000;
char obuf[OUT_LEN],*ooh=obuf;
inline void print(char c){
    if (ooh==obuf+OUT_LEN) fwrite(obuf,1,OUT_LEN,stdout),ooh=obuf;
    *ooh++=c;
}
template<class T>
inline void print(T x){
    static int buf[30],cnt;
    if (x==0) print('0');
    else{
        if (x<0) print('-'),x=-x;
        for (cnt=0;x;x/=10) buf[++cnt]=x%10+48;
        while (cnt) print((char)buf[cnt--]);
    }
}
inline void flush(){
	fwrite(obuf,1,ooh-obuf,stdout);
}

void judge(){
    freopen("balance.in","r",stdin);
    freopen("balance.out","w",stdout);
}

const int maxn=110000;
const long long inf=1e12;
int b[maxn],a[maxn],opt,n,s[maxn];
long long ans=inf,tot[maxn],cnt,sum[maxn];

struct info{
	int pos,num;
	inline bool operator <(const info &a)const{
		return num<a.num;
	}
}c[maxn];

int abs(int x){
	return x<0?-x:x;
}

vector<int> v[maxn];

void add(int x,int y){
	int t=s[x];
	int id=t/2,now=v[x].size();
	if (t&1) id++;
	if (now<id) tot[x]-=y;
	else if (now>id) tot[x]+=y;
}

int main(){
	judge();
	read(n);
	for (int i=1;i<=n;i++) read(b[i]);
	a[opt=1]=b[1];
	for (int i=2;i<=n;i++){
		if (b[i]!=b[i-1]) a[++opt]=b[i];
	}
	for (int i=1;i<=opt;i++){
		c[i].pos=i,c[i].num=a[i];
		if (i==1||i==opt) s[a[i]]++;
		else s[a[i]]+=2;
		if (i!=1) cnt+=abs(a[i]-a[i-1]),sum[a[i-1]]+=abs(a[i]-a[i-1]),sum[a[i]]+=abs(a[i]-a[i-1]);
	}
	sort(c+1,c+opt+1);
	for (int i=1;i<=opt;i++){
		if (c[i].pos!=1){
			v[a[c[i].pos-1]].pb(c[i].num);
			add(a[c[i].pos-1],c[i].num);
		}
		if (c[i].pos!=opt){
			v[a[c[i].pos+1]].pb(c[i].num);
			add(a[c[i].pos+1],c[i].num);
		}
	}
	/*for (int i=1;i<=opt;i++){
		int t=v[a[i]].size();
		for (int j=0;j<t;j++) print(v[a[i]][j]),print(' ');
		print('\n');
	}*/
	for (int i=1;i<=opt;i++){
		int t=v[a[i]].size()-1;
		int t1=t/2,numb;
		numb=v[a[i]][t1];
	//	print(s[a[i]]),print(' '),print(tot[a[i]]),print(' '),print(numb),print('\n');
	//	print(sum[a[i]]),print('\n');
		cnt-=sum[a[i]];
		ans=min(ans,cnt+abs(tot[a[i]]-(t-t1-t1)*numb));
		cnt+=sum[a[i]];
	//	print(tot[a[i]]-(t-t1-1)*numb),print('\n');
	}
	ans=min(ans,cnt);
	print(ans),print('\n');
	return flush(),0;
}
