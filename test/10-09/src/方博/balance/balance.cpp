#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=100005;
vector<int>c[N];
bool vis[N];
int a[N],sz[N],sum[N];
int n,ans,ret;
int main()
{
	freopen("balance.in","r",stdin);
	freopen("balance.out","w",stdout); 
	n=read();
	for(int i=1;i<=n;i++)
		a[i]=read();
	for(int i=n;i>=2;i--){
		ans+=abs(a[i]-a[i-1]);
		if(a[i]!=a[i-1]){
			sz[a[i]]++;
			c[a[i]].push_back(a[i-1]);
			sum[a[i]]+=a[i-1];
		}
	}
	for(int i=1;i<n;i++)
		if(a[i]!=a[i+1]){
			sz[a[i]]++;
			c[a[i]].push_back(a[i+1]);
			sum[a[i]]+=a[i+1];
		}
	ret=ans;
	memset(vis,false,sizeof(vis));
	for(int i=1;i<=n;i++){
		if(vis[a[i]])continue;
		vis[a[i]]=true;
		int g1,g2,g3,g4,g5,p0,p1,p2,p3,p4;
		g1=g2=g3=g4=g5=0;
		sort(c[a[i]].begin(),c[a[i]].end());
		p0=a[i];p1=c[a[i]][sz[p0]/2];
		if(sz[p0]>1)p2=c[a[i]][sz[p0]/2-1];
		else p2=p0;
		p3=sum[a[i]]/sz[p0];p4=p3+1;
		for(int j=0;j<=sz[a[i]]-1;j++){
			g1+=abs(c[p0][j]-p0);
			g2+=abs(c[p0][j]-p1);
			g3+=abs(c[p0][j]-p2);
			g4+=abs(c[p0][j]-p3);
			g5+=abs(c[p0][j]-p4);
		}
		g2=min(g2,g3);
		g2=min(g2,g4);
		g2=min(g2,g5);
		ret=min(ans-(g1-g2),ret);
	}
	writeln(ret);
	return 0;
}
