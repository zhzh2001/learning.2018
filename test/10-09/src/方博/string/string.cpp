#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=50005;
struct trie{
	int son[27];
	int t;
}a[N*20];
int c[255];
int n,cnt;
string s[N];
int len[N];
int mp[27][27];
bool can;
int ans;
void build(int k,int t,int l)
{
	if(l==len[t]){
//		cout<<l<<endl;
		a[k].t++;
		return;
	}
	l++;
	int v=c[s[t][l-1]];
	if(!a[k].son[v])a[k].son[v]=++cnt;
	if(l<=len[t])build(a[k].son[v],t,l);
}
void getmap(int k,int t,int l)
{
//	cout<<k<<'!'<<t<<' '<<l<<endl;
	if(l==len[t])return;
	l++;
	int v=c[s[t][l-1]];
	for(int i=0;i<26;i++){
		if(i==v)continue;
//		if(a[k].son[i])cout<<'!'<<i<<endl;
		if(a[k].son[i])mp[v][i]=1;
	}
	if(a[k].t&&l<=len[t])can=false;
	if(l<=len[t])getmap(a[k].son[v],t,l);
	return;
}
bool vis[27];
int in[27];
int out[27];
int q[27];
bool anst[N];
void dfs(int k)
{
//	cout<<k<<' '<<can<<endl;
	if(!can)return;
	vis[k]=true;
	for(int i=0;i<26;i++)
		if(mp[k][i])
			if(vis[i])can=false;
			else dfs(i);
	vis[k]=false;
}
int main()
{
//	cout<<(sizeof(a)/1024/1024)<<endl;
	freopen("string.in","r",stdin);
	freopen("string.out","w",stdout);
	for(int i=0;i<26;i++)
		c[i+'a']=i;
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>s[i];
	for(int i=1;i<=n;i++)
		len[i]=s[i].length();
	cnt=1;
	for(int i=1;i<=n;i++)
		build(1,i,0);
	for(int i=1;i<=n;i++){
		memset(mp,0,sizeof(mp));
		can=true;
		getmap(1,i,0);
		if(!can)continue;
		memset(vis,0,sizeof(vis));
		memset(in,0,sizeof(in));
		memset(out,0,sizeof(out));
		for(int k=0;k<26;k++)
			for(int j=0;j<26;j++)
				if(mp[k][j])in[j]++,out[k]++;
		/*if(i==38){
			cout<<' ';
			for(int i=0;i<26;i++)
				cout<<' '<<(char)('a'+i);
			cout<<endl;
			for(int i=0;i<26;i++){
				cout<<(char)('a'+i)<<' ';
				for(int j=0;j<26;j++)
					cout<<mp[i][j]<<' ';
				cout<<endl;
			}
			system("pause");
		}*/
		for(int j=0;j<26;j++)
			dfs(j);
		if(!can)continue;
		if(can)ans++,anst[i]=true;
//		system("pause");
	}
	writeln(ans);
	for(int i=1;i<=n;i++)
		if(anst[i])cout<<s[i]<<endl;
}
