#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=5005;
ll n,m;
ll a[N];
ll sum[N];
ll f[N][N];//f[i][j]表示前j个球进前i个洞并且后面的j+1~n个球不会再进这i个洞的最小代价
ll sh[N];
struct xxx{
	ll b,c;
}h[N];
inline bool com(xxx a,xxx b){return a.b<b.b;}
int main()
{
	freopen("hole.in","r",stdin);
	freopen("hole.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=n;i++)
		a[i]=read();
	sort(a+1,a+n+1);
	for(int i=1;i<=m;i++)
		h[i].b=read(),h[i].c=read();
	sort(h+1,h+m+1,com);
	for(int i=1;i<=m;i++){
		sh[i]=sh[i-1]+h[i].c;
		f[i][0]=1e15;
	}
	if(sh[m]<n){
		puts("Stupid game!");
		return 0;
	}
	f[0][0]=0;
	for(int i=1;i<=m;i++){
		for(int j=1;j<=n;j++){
			sum[j]=sum[j-1]+abs(a[j]-h[i].b);
			f[i][j]=1e15;
		}
		for(int j=0;j<=min(sh[i-1],n);j++)
			for(int k=j;k<=min(j+h[i].c,n);k++)
				f[i][k]=min(f[i-1][j]+sum[k]-sum[j],f[i][k]);
	}
	cout<<f[m][n]<<endl;
	return 0;
}
