#include <cstdio>
#include <queue>
#include <algorithm>
using namespace std;
const int N=5005;
int n,m,S;
int a[N],L[N],R[N],ci[N];
#define ll long long
ll ans=1e14;
struct node {
	int b,c;
	bool operator <(node a) const {
		return b<a.b;
	}
}p[N];
int aabs(int x) {return x<0?-x:x;}
int findl(int x) {return x==L[x]?x:L[x]=findl(L[x]);}
int findr(int x) {return x==R[x]?x:R[x]=findr(R[x]);}
int main() {
	freopen("hole.in","r",stdin);
	freopen("hole.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=0;i<n;i++) scanf("%d",&a[i]);
	for (int i=1;i<=m;i++) scanf("%d%d",&p[i].b,&p[i].c),S+=p[i].c;
	if (S<n) return puts("Stupid game!"),0;
	sort(a,a+n);sort(p+1,p+m+1);
	for (int k=0;k<(1<<n);k++) {
		for (int i=0;i<=m+1;i++) L[i]=i,R[i]=i,ci[i]=p[i].c;
		int ps=1,ck=1;
		ll res=0;
		for (int i=0;i<n;i++) {
			while (ps<m && p[ps+1].b<i) ps++;
			int l=findl(ps),r=findr(ps);
			if (k>>i&1) {
				if (!l) {ck=0;break;}
				res+=aabs(a[i]-p[l].b);
				if ((--ci[l])==0) L[l]=l-1,R[l]=l+1;
			}
			else {
				if (r>m) {ck=0;break;}
				res+=aabs(p[r].b-a[i]);
				if ((--ci[r])==0) L[r]=r-1,R[r]=r+1;
			}
		}
		if (ck) ans=min(ans,res);
	}
	printf("%lld\n",ans);
}
