#include <cstdio>
#include <cmath>
#include <algorithm>
using namespace std;
const int N=1e5+7;
int a[N],b[N],vis[N];
int n,ans=1e9,mx,mi=1e5,now;
int check(int x) {
	int ans=0;
	for (int i=1;i<=n;i++) if (a[i]==now) b[i]=x;else b[i]=a[i];
	for (int i=2;i<=n;i++) ans+=abs(b[i]-b[i-1]);
	return ans;
}
int main() {
	freopen("balance.in","r",stdin);
	freopen("balance.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;++i) scanf("%d",&a[i]),vis[a[i]]=1,mx=max(mx,a[i]),mi=min(mi,a[i]);
	for (int i=mi;i<=mx;i++) 
		if (vis[i]) {
			now=i;
			for (int j=mi;j<=mx;j++) ans=min(ans,check(j));
		}
	printf("%d\n",ans);
}
