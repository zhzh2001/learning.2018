#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
const int N=50005;
struct node {
	char c[25];
	int id,ln;
}s[N],ans[N];
int n,cnt,now;
int you[27],flg[27];
bool operator <(node a,node b) {
	if (you[a.c[now]-'a']!=you[b.c[now]-'a']) return you[a.c[now]-'a']<you[b.c[now]-'a'];
	return a.c[now]<b.c[now];
}
bool operator !=(node a,node b) {return you[a.c[now]-'a']!=you[b.c[now]-'a'];}
bool cmp2(node a,node b) {return a.id<b.id;}
void solve (int l,int r,int dep) {
	if (l==r) {ans[++cnt]=s[l];return;}
	now=dep;
	sort(s+l,s+r+1);
//	puts("----------------------------------------");
//	for (int i=0;i<26;i++) printf("%d ",you[i]);
//	puts("");
//	for (int i=l;i<=r;i++) printf("%s\n",s[i].c+1);
//	puts("----------------------------------------");
	int la=l;
	bool vis[27];
	memset(vis,0,sizeof vis);
	int nyou[27];
	for (int i=0;i<26;i++) nyou[i]=you[i];
	vis[s[l].c[dep]-'a']=1;
	for (int i=l+1;i<=r;i++) {
		if (s[i-1]!=s[i]) break;
		if (s[i].c[dep]!=s[i-1].c[dep]) vis[s[i].c[dep]-'a']=1;
	}
	for (int i=l+1;i<=r+1;i++) {
		now=dep;
		int id=s[i-1].c[dep]-'a',x=s[i].c[dep]-'a';
		if (id!=x || i==r+1) {
			for (int j=0;j<26;j++) if (vis[j] && j!=id && !flg[id]) you[j]=dep+1;
			if (!you[id]) you[id]=dep;
			flg[id]++;
			solve(la,i-1,dep+1);
			for (int j=0;j<26;j++) you[j]=nyou[j];
			la=i;
			flg[id]--;
		}
		if (i<=r && s[i-1]!=s[i] && you[id] && you[x]) break;
	}
}
int main() {
	freopen("string.in","r",stdin);
	freopen("string.out","w",stdout);
	scanf("%d",&n);
	you[26]=-1;
	for (int i=1;i<=n;i++) scanf("%s",s[i].c+1),s[i].ln=strlen(s[i].c+1),s[i].id=i;
	solve(1,n,1);
	printf("%d\n",cnt);
	sort(ans+1,ans+cnt+1,cmp2);
	for (int i=1;i<=cnt;i++) printf("%s\n",ans[i].c+1);
}
