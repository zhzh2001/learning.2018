#include <iostream>
#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=1e5+10,M=1e5;
ll n,a[N],b[N],ans;
inline ll work(){
	ll ret=0;
	for (ll i=2;i<=n;++i) ret+=abs(a[i]-a[i-1]);
	return ret;
}
int main(){
	freopen("balance.in","r",stdin);
	freopen("baoli.out","w",stdout);
	read(n);
	for (ll i=1;i<=n;++i) read(a[i]),b[i]=a[i];
	ans=work();
	for (ll i=1,x,y;i<=n;++i)
		for (ll j=1;j<=n;++j) if ((x=a[i])^(y=a[j])){
			for (ll k=1;k<=n;++k) if (a[k]==x) a[k]=y;
			ans=min(ans,work());
//		bool flag=0;
//		if (work()==30247) flag=1;
			for (ll k=1;k<=n;++k) a[k]=b[k];
//		if (flag) cout<<a[i]<<' '<<a[j]<<'\n';
		}
	wr(ans);
	return 0;
}
