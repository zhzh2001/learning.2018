#include <iostream>
#include <cstdio>
#include <vector>
#include <algorithm>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=1e5+10,M=1e5;
ll n,a[N],t[N],ans,start;
vector<ll>V[N];
int main(){
	freopen("balance.in","r",stdin);
	freopen("balance.out","w",stdout);
	read(n);
	for (ll i=1;i<=n;++i) read(a[i]);
	for (ll i=1;i<=n;++i){
		if (i>1&&a[i]!=a[i-1]) V[a[i]].push_back(a[i-1]),t[a[i]]+=abs(a[i]-a[i-1]);
		if (i<n&&a[i]!=a[i+1]) V[a[i]].push_back(a[i+1]),t[a[i]]+=abs(a[i]-a[i+1]);
		if (i>1) start+=abs(a[i]-a[i-1]);
	}
	ans=start;
	for (ll i=0;i<=M;++i){
		if (!V[i].size()) continue;
		sort(V[i].begin(),V[i].end());
		ll now=start-t[i];
		for (ll l=0,r=(ll)V[i].size()-1;l<=r;++l,--r){
			now+=V[i][r]-V[i][l];
		}
		ans=min(ans,now);
	}
	wr(ans);
	return 0;
}
/*
209 48 30247
32 87
42 58
49
*/
