#include <iostream>
#include <cstdio>
#include <cstring>
#include <queue>
using namespace std;
typedef int ll;
const ll N=5e4+10;
ll n,net[N*21][27],tot=1,ans[N],cnt,ins[30];
bool ed[N*21],e[30][30];
char a[N][21];
inline void add(ll x,char *s){
	ll len=strlen(s),k=1;
	for (ll i=0,t;i<len;++i){
		t=s[i]-'a';
		if (!net[k][t]) net[k][t]=++tot;
		k=net[k][t];
	}ed[k]=1;
}
inline bool check(char *s){
	ll len=strlen(s),k=1;
	memset(e,0,sizeof e);
	for (ll i=0,t;i<len;++i){
		t=s[i]-'a';
		if (!net[k][t]) net[k][t]=++tot;
		for (ll i=0;i<26;++i) if (net[k][i]&&i!=t) e[i][t]=1;
		if (ed[k]) return 0;
		k=net[k][t];
	}
	memset(ins,0,sizeof ins);
//	for (ll k=0;k<26;++k)
	for (ll i=0;i<26;++i)
		for (ll j=0;j<26;++j) if (e[i][j]){
			if (e[j][i]) return 0;
			++ins[j];
		}
	queue<ll>Q;
	for (ll i=0;i<26;++i) if (!ins[i]) Q.push(i);
	while (!Q.empty()){
		ll x=Q.front();Q.pop();
		for (ll i=0;i<26;++i)
			if (e[x][i]) if (!(--ins[i])) Q.push(i);
	}
	for (ll i=0;i<26;++i) if (ins[i]) return 0;
	return 1;
}
/*inline void dfs(ll now,ll already){
	if (!ed[now].empty()){
		for (unsigned i=0;i<ed[now].size();++i)
			ans[++cnt]=ed[now][i];
		return;
	}
	for (ll i=1;i<=already;++i)
		if (net[now][S[i]]){
			dfs(net[now][S[i]],already);
			return;
		}
	for (ll i=0;i<26;++i){
		if (vis[i]||!net[now][i]) continue;
		vis[i]=1;
		S[already+1]=i;
		dfs(net[now][i],already+1);
		vis[i]=0;
	}
}*/
int main(){
	freopen("string.in","r",stdin);
	freopen("string.out","w",stdout);
	scanf("%d",&n);
	for (ll i=1;i<=n;++i){
		scanf("%s",a[i]);
		add(i,a[i]);
	}
	for (ll i=1;i<=n;++i) if (check(a[i])) ans[++cnt]=i;
	printf("%d\n",cnt);
	for (ll i=1;i<=cnt;++i) printf("%s\n",a[ans[i]]);
	return 0;
}
