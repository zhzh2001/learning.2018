#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=5010;
ll n,m,a[N],f[N][N];
struct node{
	ll x,c;
	bool operator <(const node&res)const{
		return x<res.x;
	}
}b[N];
int main(){
	freopen("hole.in","r",stdin);
	freopen("hole.out","w",stdout);
	read(n),read(m);
	for (ll i=1;i<=n;++i) read(a[i]);
	sort(a+1,a+1+n);
	for (ll i=1;i<=m;++i) read(b[i].x),read(b[i].c);
	sort(b+1,b+1+m);
	memset(f,0x3f,sizeof f);
	f[0][0]=0;
	for (ll i=0;i<=n;++i)
		for (ll j=1;j<=m;++j){
			ll D=0;
			for (ll k=i;k<=min(n,i+b[j].c);++k){
				f[k][j]=min(f[k][j],f[i][j-1]+D);
				D+=abs(a[k+1]-b[j].x);
			}
		}
	wr(f[n][m]);
	return 0;
}
/*	
	for (ll j=1;j<=m;++j)
		for (ll i=0;i<=n;++i){
			ll D=0;
			for (ll k=i;k<=min(n,i+b[j].c);++k){
				f[k][j]=min(f[k][j],f[i][j-1]+D);
				D+=abs(a[k+1]-b[j].x);
			}
		}
*/
