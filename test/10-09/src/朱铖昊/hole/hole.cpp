#include<bits/stdc++.h>
using namespace std;
#define fi first
#define se second
#define ll long long
const int inf=2e9+7;
const int N=5005;
multiset<int> s[N];
ll ans;
int a[N];
pair<int,int> b[N];
int n,m,sum,pos,wz;
inline void read(int &x)
{
	char c=getchar();
	int y=1;
	while (c>'9'||c<'0')
	{
		if (c=='-')
			y=-1;
		c=getchar();
	}
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
	x*=y;
}
inline ll calc(int x,int y)
{
	if (y==0||y>m)
		return inf;
	if (s[y].size()<b[y].se)
		return abs(x-b[y].fi);
	int p=*s[y].begin();
	if (p>=b[y].fi)
		return calc(x,y-1);
	else
		return calc(p,y-1)+abs(x-b[y].fi)-abs(p-b[y].fi);
}
inline void work(int x,int y)
{
	if (y==0||y>m)
		return;
	if (s[y].size()<b[y].se)
	{
		s[y].insert(x);
		return;
	}
	int p=*s[y].begin();
	if (p>=b[y].fi)
		work(x,y-1);
	else
	{
		s[y].erase(s[y].begin());
		s[y].insert(x);
		work(p,y-1);
	}
}
int main()
{
	freopen("hole.in","r",stdin);
	freopen("hole.out","w",stdout);
	read(n);
	read(m);
	for (int i=1;i<=n;++i)
		read(a[i]);
	for (int i=1;i<=m;++i)
	{
		read(b[i].fi);
		read(b[i].se);
		sum+=b[i].se;
	}
	if (sum<n)
	{
		puts("Stupid game!");
		return 0;
	}
	sort(a+1,a+1+n);
	sort(b+1,b+1+m);
	int tmp=m;
	m=1;
	for (int i=2;i<=tmp;++i)
		if (b[m].fi!=b[i].fi)
			b[++m]=b[i];
		else
			b[m].se+=b[i].se;
	wz=pos=1;
	for (int i=1;i<=n;++i)
	{
		while ((a[i]>b[pos].fi||s[pos].size()==b[pos].se)&&pos<=m)
			++pos;
		while (a[i]>b[wz].fi&&wz<=m)
			++wz;
		ll p=calc(a[i],wz-1),q=calc(a[i],wz);
		if (pos<=m&&p>b[pos].fi-a[i]&&q>b[pos].fi-a[i])
		{
			ans+=b[pos].fi-a[i];
			s[pos].insert(a[i]);
		}
		else
		{
			if (p<q)
			{
				ans+=p;
				work(a[i],wz-1);
			}
			else
			{
				ans+=q;
				work(a[i],wz);
			}
		}
		// puts("");
		// cout<<i<<'\n';
		// for (int j=1;j<=m;++j)
		// {
		// 	cout<<j<<": ";
		// 	for (auto k=s[j].begin();k!=s[j].end();k++)
		// 		cout<<*k<<' ';
		// 	puts("");
		// }
	}
	cout<<ans;
	return 0;
}