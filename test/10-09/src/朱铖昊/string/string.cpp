#include<bits/stdc++.h>
using namespace std;
const int N=50005,M=27;
int e[N*20][M],f[N*20];
int vis[M],in[M],a[M][M];
char c[N][M];
bool b[N];
int ans,n,cnt;
inline void add(int x)
{
	int pos=1,len=strlen(c[x]+1);
	for (int i=1;i<=len;++i)
	{
		if (!e[pos][c[x][i]-'a'])
			e[pos][c[x][i]-'a']=++cnt;
		pos=e[pos][c[x][i]-'a'];
	}
	f[pos]=1;
}
inline bool check(int x)
{
	int pos=1,len=strlen(c[x]+1);
	for (int i=1;i<=len;++i)
	{
		if (f[pos])
			return false;
		pos=e[pos][c[x][i]-'a'];
	}
	return true;
}
inline void line(int x)
{
	int pos=1,len=strlen(c[x]+1);
	for (int i=1;i<=len;++i)
	{
		for (int j=0;j<=25;++j)
			if (e[pos][j]&&j!=c[x][i]-'a')
				a[c[x][i]-'a'][j]=1;
		pos=e[pos][c[x][i]-'a'];
	}
}
inline bool dfs(int x)
{
	vis[x]=in[x]=1;
	for (int i=0;i<=25;++i)
		if (a[x][i]&&x!=i)
		{
			if (in[i])
				return false;
			if (!vis[i])
			{
				if (!dfs(i))
					return false;
			}
		}
	in[x]=0;
	return true;
}
inline bool work(int x)
{
	for (int i=0;i<=25;++i)
		vis[i]=in[i]=0;
	memset(a,0,sizeof(a));
	line(x);
	for (int i=0;i<=25;++i)
		if (!vis[i])
		{
			if (!dfs(i))
				return false;
		}
	return true;
}
int main()
{
	freopen("string.in","r",stdin);
	freopen("string.out","w",stdout);
	scanf("%d",&n);
	cnt=1;
	for (int i=1;i<=n;++i)
	{
		scanf("%s",c[i]+1);
		add(i);
	}
	for (int i=1;i<=n;++i)
	{
		b[i]=check(i)&&work(i);
		if (b[i])
			ans++;
	}
	printf("%d\n",ans);
	for (int i=1;i<=n;++i)
		if (b[i])
			puts(c[i]+1);
	return 0;
}