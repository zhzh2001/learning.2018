#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline void read(int &x)
{
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	x=0;
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
const int N=100005;
int t,n,mid,x,a[N];
ll sum,tot,ans,s[N];
vector<int> v[N];
int main()
{
	freopen("balance.in","r",stdin);
	freopen("balance.out","w",stdout);
	read(t);
	while (t--)
	{
		read(x);
		if (x!=a[n])
			a[++n]=x;
	}
	for (int i=1;i<n;++i)
	{
		sum+=abs(a[i+1]-a[i]);
		v[a[i]].push_back(a[i+1]);
		v[a[i+1]].push_back(a[i]);
		s[a[i]]+=abs(a[i+1]-a[i]);
		s[a[i+1]]+=abs(a[i+1]-a[i]);
	}
	ans=sum;
	for (int i=1;i<=100000;++i)
		if (v[i].size())
		{
			sort(v[i].begin(),v[i].end());
			tot=0;
			mid=v[i][v[i].size()/2];
			for (unsigned j=0;j<v[i].size();++j)
				tot+=abs(mid-v[i][j]);
			ans=min(ans,sum+tot-s[i]);
		}
	cout<<ans;
	return 0;
}