#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int k=0; int f=1; char ch=getchar();
	while(!isdigit(ch)){if(ch=='-') f=-1; ch=getchar();}
	while(isdigit(ch)){k=(k<<1)+(k<<3)+(ch^48); ch=getchar();}
	return k*f;
}
bool vis[100010];
vector<int> ve[100010];
vector<int> vv[100010];
long long cx[100010],xc[100010],sum[100010],sum1[100010];
int gs[100010],gs1[100010],a[100010];
int main(){
	freopen("balance.in","r",stdin);
	freopen("balance.out","w",stdout);
	int n; n=read(); long long res=0;
	for(int i=1;i<=n;i++){
		a[i]=read(); 
	}  
	for(int i=2;i<=n;i++) res+=abs(a[i]-a[i-1]);
	long long ans=res; //cout<<ans<<endl;
	for(int i=1;i<=n;i++){
		if(i>1){
			if(a[i-1]<a[i]) ve[a[i]].push_back(a[i-1]),cx[a[i]]+=a[i]-a[i-1],gs[a[i]]++,sum[a[i]]+=a[i-1];
		else vv[a[i]].push_back(a[i-1]),xc[a[i]]+=a[i-1]-a[i],gs1[a[i]]++,sum1[a[i]]+=a[i-1];	
		}
		if(i==n) continue;
		if(a[i+1]<a[i]) ve[a[i]].push_back(a[i+1]),cx[a[i]]+=a[i]-a[i+1],gs[a[i]]++,sum[a[i]]+=a[i+1];
		else vv[a[i]].push_back(a[i+1]),xc[a[i]]+=a[i+1]-a[i],gs1[a[i]]++,sum1[a[i]]+=a[i+1];
	}
	for(int i=1;i<=n;i++){
		if(vis[a[i]]) continue;
		vis[a[i]]=1;
		if(cx[a[i]]>xc[a[i]]){
			long long now=sum[a[i]]/gs[a[i]]; 
			//cout<<now<<endl;
			long long ress=res;
			ress-=cx[a[i]];
			for(int j=0;j<ve[a[i]].size();j++){
			//	cout<<ve[a[i]][j]<<endl;
				ress+=abs(now-ve[a[i]][j]);
			}
			if(ress<ans) ans=ress;
		}else{
			long long now=sum1[a[i]]/gs1[a[i]]; 
			long long ress=res;
			ress-=xc[a[i]];
			for(int j=0;j<vv[a[i]].size();j++){
				ress+=abs(now-vv[a[i]][j]);
			}
			if(ress<ans) ans=ress;
		}
	//	cout<<ans<<endl;
	}
	cout<<ans<<endl;
}
