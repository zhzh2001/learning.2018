//gtmdjzq
#include<bits/stdc++.h>
#define N 100005
#define ll long long
using namespace std;
inline int read(){
  int x = 0, f = 0;char ch = getchar();
  for (; ch <'0' || ch>'9'; ch = getchar()) if (ch == '-') f = 1;
  for (; ch >= '0'&& ch<='9'; ch = getchar()) x = (x<<1) + (x<<3) + ch - 48;
  return f ?-x :x;
}
int n, a[N], nxt[N], pre[N], vis[N];
int b[N], ha[N], tot;
ll ans;
int main() {
  freopen("balance.in","r",stdin);
  freopen("balance.out","w",stdout);
  n = read();
  for (int i = 1; i <= n; i++){
    a[i] = read();
    nxt[i] = pre[a[i]];
    pre[a[i]] = i;
  }
  for (int i = n; i; i--)
    if (!vis[i]) {
      tot = 0;
      ll sum = 0;
      for (int j = i; j; j = nxt[j]) {
        vis[j] = 1;
        if (j<n && a[j] != a[j+1]) ha[++tot] = a[j+1], sum += abs(a[j+1] - a[j]);
        if (j>1 && a[j] != a[j-1]) ha[++tot] = a[j-1], sum += abs(a[j-1] - a[j]);
      }
      sort(ha+1,ha+1+tot);
      int t = ha[(tot+1)/2];
      ll sum1 = 0;
      for (int j = 1; j <= tot; j++)
        sum1 += abs(t-ha[j]);
      if (sum - sum1 > ans) ans = sum - sum1;
    }
  ll pyop = 0;
  for (int i = 1; i < n; i++)
    pyop += abs(a[i]-a[i+1]);
  printf("%lld\n", pyop - ans);
  return 0;
}