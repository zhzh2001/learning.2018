//gtmdjzq
#include<bits/stdc++.h>
#define N 50005
#define ll long long
using namespace std;
inline int read(){
  int x = 0, f = 0;char ch = getchar();
  for (; ch <'0' || ch>'9'; ch = getchar()) if (ch == '-') f = 1;
  for (; ch >= '0'&& ch<='9'; ch = getchar()) x = (x<<1) + (x<<3) + ch - 48;
  return f ?-x :x;
}
int n, ind, ch[1000005][26], ed[1000005], tot[26];
char s[N][25];
inline void insert(int I) {
  scanf("%s", s[I]+1);
  int len = strlen(s[I]+1), x = 0;
  for (int i = 1; i <= len; i++) {
    if (!ch[x][s[I][i]-'a']) ch[x][s[I][i]-'a'] = ++ind;
    x = ch[x][s[I][i]-'a'];
  }ed[x] = 1;
}
int yx[27], vis[27];
bitset <26> f[26];
inline void dfs(int x) {
  if (vis[x]) return;
  for (int i = 0; i < 26; i++) 
    if (f[x][i]) {
      dfs(i);
      f[x] |= f[i];
    }
}
inline bool pdfy() {
  memset(vis, -1, sizeof vis);
  for (int i = 0; i < 26; i++)
    if (vis[i] < 0) {
      vis[i] = 1;
      dfs(i);
    }
  for (int i = 0; i < 26; i++)
    for (int j = 0; j < 26; j++)
      if (f[i][j] && f[j][i]) return 1;
  return 0;
}
inline bool query(int X) {
  int len = strlen(s[X]+1), x = 0;
  memset(f, 0, sizeof f);
  memset(tot, 0, sizeof tot);
  for (int i = 1; i <= len; i++) {
    if (ed[x]) return 0;
    for (int j = 0; j < 26; j++)
      if (j != s[X][i]-'a' && ch[x][j]) f[s[X][i]-'a'][j] = 1, tot[s[X][i]-'a'] = 1;
    x = ch[x][s[X][i]-'a'];
  }
  if (pdfy()) return 0;
  return 1;
}
int ans[N];
int main() {
  freopen("string.in","r",stdin);
  freopen("string.out","w",stdout);
  n = read();
  for (int i = 1; i <= n; i++)
    insert(i);
  for (int i = 1; i <= n; i++)
    if (query(i)) ans[++ans[0]] = i;
  printf("%d\n", ans[0]);
  for (int i = 1; i <= ans[0]; i++)
    printf("%s\n",s[ans[i]]+1);
  return 0;
}
