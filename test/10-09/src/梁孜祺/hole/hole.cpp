//gtmdjzq
#include<bits/stdc++.h>
#define N 5005
#define ll long long
using namespace std;
inline int read(){
  int x = 0, f = 0;char ch = getchar();
  for (; ch <'0' || ch>'9'; ch = getchar()) if (ch == '-') f = 1;
  for (; ch >= '0'&& ch<='9'; ch = getchar()) x = (x<<1) + (x<<3) + ch - 48;
  return f ?-x :x;
}
int n, m, a[N], tot, l[N];
struct data {
  int x, y;
  bool operator <(const data & a) const{
    return x < a.x;
  }
}b[N];
struct Data{
  int a,c;
  bool operator <(const Data & b) {
    return a < b.a;
  }
}t[N][N];
int vis[N];
int main() {
  freopen("hole.in","r",stdin);
  freopen("hole.out","w",stdout);
  n = read();
  m = read();
  for (int i = 1; i <= n; i++)
    a[i] = read();
  int py = 0;
  for (int i = 1; i <= m; i++)
    b[i] = (data){read(), read()}, py += b[i].y;
  if (py < n) return puts("Stupid game!")&0;
  sort(a+1,a+1+n);
  sort(b+1,b+1+m);
  for (int i = 2; i <= m; i++)
    if (b[i].x == b[tot].x) b[tot].y += b[i].y;
    else b[++tot] = b[i];
  m = tot;
  for (int i = 1; i <= n; i++){
    for (int j = 1; j <= m; j++)
      t[i][j].a = abs(b[j].x-a[i]), t[i][j].c = j;
  }
  for (int i = 1; i <= n; i++)
    sort(t[i]+1,t[i]+1+m);
  for (int i = 1; i <= n; i++) l[i] = 1;
  ll ans = 0;
  for (int i = 1; i <= n; i++) {
    int mx = 0, mxx;
    for (int j = 1; j <= n; j++) 
      if (!vis[j]) {
        while (l[j] <= m && !b[t[j][l[j]].c].y) l[j]++;
      }
    for (int j = 1; j <= n; j++)
      if (!vis[j] && mx > t[j][l[j]].a)
        mx = t[j][l[j]].a, mxx = j;
    vis[mxx] = 1;
    b[t[mxx][l[mxx]].c].y--;
    ans += mxx;
  }
  printf("%lld\n", ans);
  return 0;
}