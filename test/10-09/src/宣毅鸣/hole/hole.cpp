#include<bits/stdc++.h>
using namespace std;
#define int long long
const int N=5005;
int a[N],b[N],c[N],f[N],n,m,dp[2][N],g[N],p[N],q[N];
int cmp(int x,int y){
	return b[x]<b[y];
}
signed main(){
	freopen("hole.in","r",stdin);
	freopen("hole.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	for (int i=1;i<=n;i++)scanf("%lld",&a[i]);
	sort(a+1,a+n+1);
	for (int i=1;i<=m;i++)scanf("%lld%lld",&b[i],&c[i]),f[i]=i;
	sort(f+1,f+m+1,cmp);
	for (int i=1;i<=n;i++)dp[0][i]=1e18;
	dp[0][0]=0;
	for (int i=1;i<=m;i++){
		for (int j=1;j<=n;j++)g[j]=g[j-1]+abs(b[f[i]]-a[j]);
		p[1]=0;q[1]=0;
		int l=1,r=1,t=i&1;
		for (int j=1;j<=n;j++){
			while (l<=r&&p[l]+c[f[i]]<j)l++;
			while (l<=r&&q[l]>=dp[t^1][j]-g[j])r--;
			r++;
			p[r]=j;q[r]=dp[t^1][j]-g[j];
			dp[t][j]=g[j]+q[l];
		}
	}
	if (dp[m&1][n]>1e16)puts("Stupid game!");
	else printf("%lld",dp[m&1][n]);
	return 0;
}
