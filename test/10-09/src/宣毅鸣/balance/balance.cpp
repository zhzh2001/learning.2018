#include<bits/stdc++.h>
using namespace std;
#define int long long
const int N=2e5+5;
int a[N],n,sum,p[N],s[N],b[N];
vector<int > v[N];
signed main(){
	freopen("balance.in","r",stdin);
	freopen("balance.out","w",stdout);
	scanf("%lld",&n);
	for (int i=1;i<=n;i++)scanf("%lld",&a[i]);
	for (int i=1;i<n;i++)sum+=abs(a[i+1]-a[i]);
	for (int i=1;i<=n;i++){
		if (i>1&&a[i-1]!=a[i])v[a[i]].push_back(a[i-1]),s[a[i]]+=abs(a[i]-a[i-1]),p[a[i]]+=a[i-1];
		if (i<n&&a[i+1]!=a[i])v[a[i]].push_back(a[i+1]),s[a[i]]+=abs(a[i]-a[i+1]),p[a[i]]+=a[i+1];
	}
	int ans=sum;
	for (int i=1;i<=1e5;i++)
		if (v[i].size()){
			int tot=v[i].size(),now=0;
			for (int j=0;j<tot;j++)b[j]=v[i][j];
			sort(b,b+tot);
			for (int j=0;j<tot;j++){
				ans=min(ans,sum-s[i]+(b[j]*j-now)+(p[i]-now-b[j]*(tot-j)));
				now+=b[j];
			}
		}
	printf("%lld",ans);	
	return 0;
}
