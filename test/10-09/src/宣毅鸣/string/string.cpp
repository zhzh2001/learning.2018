#include<bits/stdc++.h>
using namespace std;
const int N=50005,M=26;
char s[N][M];
int fi[M],fflag,zhan[M],v[N*M],bb[M],nex[N],zz[N],tot,ne[N*M][M],n,ans,f[N];
void jb(int x,int y){
	nex[++tot]=fi[x];
	fi[x]=tot;
	zz[tot]=y;
}
void insert(char s[]){
	int p=0;
	for (int i=0;s[i];i++){
		int x=s[i]-'a';
		if (!ne[p][x])ne[p][x]=++tot;
		p=ne[p][x];
	}
	v[p]=1;
}
void dfs(int x){
	zhan[x]=1;bb[x]=1;
	for (int i=fi[x];i;i=nex[i]){
		if (zhan[zz[i]])fflag=0;
		if (!bb[zz[i]])dfs(zz[i]);
	}
	zhan[x]=0;
}
int pd(){
	fflag=1;
	memset(zhan,0,sizeof zhan);
	memset(bb,0,sizeof bb);
	for (int i=0;i<26;i++)
		if (!bb[i])dfs(i);
	return fflag;
}
int main(){
	freopen("string.in","r",stdin);
	freopen("string.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++)scanf("%s",&s[i]),insert(s[i]);
	for (int i=1;i<=n;i++){
		memset(fi,0,sizeof fi);
		tot=0;
		int p=0,flag=0;
		for (int j=0;s[i][j];j++){
			int x=s[i][j]-'a';
			if (v[p]){
				flag=1;
				break;
			}
			for (int k=0;k<26;k++)
				if (k!=x&&ne[p][k])jb(x,k);
			p=ne[p][x];
		}
		if (flag)continue;
		if (pd())ans++,f[i]=1;
	}
	printf("%d\n",ans);
	for (int i=1;i<=n;i++)
		if (f[i])puts(s[i]);
	return 0;	
}
