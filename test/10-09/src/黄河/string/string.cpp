#include<cstdio>
#include<cstring>
#include<algorithm>
#include<iostream>
#include<cctype>
#include<ctime>
#include<cstdlib>
#include<string>
#include<queue>
#include<cmath>
#include<set>
#include<map>
#include<bitset>
#include<vector>
#define re register
#define rep(x,a,b) for (re int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (re int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (re int x=hd[a];~x;x=nx[x])
#define LL long long
#define mo 1000000007
#define N 5100
using namespace std;
const int maxn=5100;
int n,ans;
bool f[30][30],vis[N],used[N];
string Str[N];

int main(){
    freopen("string.in","r",stdin);
    freopen("string.out","w",stdout);
	scanf("%d",&n);
	rep(i,1,n) cin>>Str[i];
	rep(t,1,n){
		int len=Str[t].length();
		bool flag=1;
		memset(vis,0,sizeof(vis));
		memset(f,0,sizeof(f));
		rep(i,0,len-1){
			rep(j,1,n){
				if (i==j||vis[j]) continue;
				int Len=Str[j].length();
				if (i>=Len||f[Str[j][i]-'a'][Str[t][i]-'a']){
					flag=0;
					break;
				}
				if (Str[t][i]!=Str[j][i]) vis[j]=1,f[Str[t][i]-'a'][Str[j][i]-'a']=1;
			}
			if (!flag) break;
		}
		used[t]=flag; ans+=flag;
	}
	printf("%d\n",ans);
	rep(i,1,n){
		if (used[i]) cout<<Str[i]<<endl;
	}
	return 0;
}
