#include<cstdio>
#include<cctype>
#include<cmath>
#include<cstring>
#include<iostream>
#include<algorithm>
#define LL long long
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define drp(i,r,l) for(LL i=r;i>=l;i--)
#define inf 20021218020021218
#define N 1000010
using namespace std;
LL a[N],b[N],c[N],fl[N],val[N];
LL Q[N],v[N],pre[N],path[N],f[N];
LL x[N],y[N],h[N];
LL n,m,i,j,k,l,r,ans,cnt;
LL read(){
	LL x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
LL check(LL a){
	if(a<0) return -a;
	return a;
}
void add(LL l,LL r,LL k,LL v){
	a[++cnt]=r; b[cnt]=c[l]; c[l]=cnt;
	fl[cnt]=k; val[cnt]=v;
	a[++cnt]=l; b[cnt]=c[r]; c[r]=cnt;
	fl[cnt]=0; val[cnt]=-v;
}
bool SPFA(){
	rep(i,0,n+m+1) f[i]=inf,v[i]=0;
	LL l=1,r=1; Q[1]=0; f[0]=0;
	while(l<=r){
		for(LL j=c[Q[l]];j!=-1;j=b[j])
		  if(fl[j]&&f[a[j]]>f[Q[l]]+val[j]){
		  	f[a[j]]=f[Q[l]]+val[j];
		  	pre[a[j]]=Q[l];
		  	path[a[j]]=j;
		  	if(!v[a[j]]) Q[++r]=a[j],v[a[j]]=1;
		  }
		v[Q[l++]]=0;
	}
	if(f[n+m+1]==inf) return 0;
	return 1;
}
LL min_cost(){
	LL ans=0;
	while(SPFA()){
		LL Min=inf;
		for(LL i=n+m+1;i;i=pre[i])
		  if(Min>fl[path[i]]) Min=fl[path[i]];
		ans+=Min*f[n+m+1];
		for(LL i=n+m+1;i;i=pre[i])
			fl[path[i]]-=Min,fl[path[i]^1]+=Min;
	}
	return ans;
}
int main()
{
	freopen("hole.in","r",stdin);
	freopen("hole.out","w",stdout);
	n=read(); m=read(); cnt=-1;
	rep(i,0,n+m+1) c[i]=-1;
	rep(i,1,n)
		x[i]=read(),add(0,i,1,0);
	rep(i,1,m)
		y[i]=read(),h[i]=read(),add(n+i,n+m+1,h[i],0);
	rep(i,1,n)
	  rep(j,1,m) add(i,n+j,inf,check(x[i]-y[j]));
	ans=min_cost();
	printf("%lld",ans);
	return 0;
}
