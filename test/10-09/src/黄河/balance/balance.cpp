#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<ctime>
#include<cstdlib>
#include<string>
#include<queue>
#include<cmath>
#include<set>
#include<map>
#include<bitset>
#include<vector>
#define re register
#define rep(x,a,b) for (re int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (re int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (re int x=hd[a];~x;x=nx[x])
#define LL long long
#define mo 1000000007
#define N 100010
using namespace std;
int read(){
int x=0,fh=1; char c=getchar();
  for(;!isdigit(c); c=getchar()) fh=(c=='-')?-1:fh;
  for(;isdigit(c); c=getchar()) x=(x<<3)+(x<<1)+(c^'0');
  return x*fh;
}
vector<int>b[N];
int n,a[N],c[N],hh,sum,ans,cha;
bool vis[N];
int main(){
	freopen("balance.in","r",stdin);
	freopen("balance.out","w",stdout);
	n=read();
	rep(i,1,n)
		a[i]=read();
	rep(i,1,n){
		if (i!=1) {
			hh+=abs(a[i]-a[i-1]);
			if (a[i]!=a[i-1])b[a[i]].push_back(a[i-1]);
		}
		if (i!=n) if (a[i]!=a[i+1])b[a[i]].push_back(a[i+1]);
	}
	cha=ans=hh;
	memset(vis,0,sizeof(vis));
	rep(i,1,n){
		if (vis[a[i]]) continue;
		vis[a[i]]=1;
		int m=b[a[i]].size();
		rep(j,1,m) c[j]=b[a[i]][j-1];
		sort(c+1,c+m+1);
	//	rep(j,1,m)printf("%d ",c[j]);
	//	printf("\n");
		int zh;
		if (m&1) zh=c[m/2+1];
			else zh=(int)((c[m/2]+c[m/2+1])/2);
		sum=cha;
		rep(j,1,m) sum+=abs(c[j]-zh)-abs(c[j]-a[i]);
		if (sum<ans) ans=sum;
	}
	printf("%d",ans);
	return 0;
}
