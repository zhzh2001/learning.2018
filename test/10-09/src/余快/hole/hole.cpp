/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000005
#define iinf 1000000000000005
#define mod 1000000007
#define put putchar('\n')
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
#define N 5055
//#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(ll x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(ll x){wr(x);put;}inline void wri(ll x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int n,m,a[N],tot,l,r,dui[5005],e;
ll ans,f[2][5005],sum[5005];
struct xx{
	int x,c;
}z1[N],z[N];
bool cmp(xx a,xx b){return a.x<b.x;}
inline int absa(int x){return (x<0)?-x:x;}
signed main(){
	freopen("hole.in","r",stdin);freopen("hole.out","w",stdout);
	n=read();m=read();
	F(i,1,n) a[i]=read();sort(a+1,a+n+1);
	F(i,1,m){
		z1[i].x=read();z1[i].c=min(read(),n);
	}
	sort(z1+1,z1+m+1,cmp);z1[0].x=inf;
	F(i,1,m){
		if (z1[i].x!=z1[i-1].x) z[++tot].x=z1[i].x;
		z[tot].c+=z1[i].c;
		z[tot].c=min(z[tot].c,n);
	}
	//f[j][i]=min(f[j-1][k]-sum[k][j])+sum[i][j];
	//i-z[j].c<=k<=i
	//�������� 
	ans=iinf;e=0;
	F(i,1,n) sum[i]=sum[i-1]+absa(a[i]-z[1].x); 
	F(j,1,tot){
		l=r=1;dui[1]=0;
		F(i,1,n){
			while (dui[l]<i-z[j].c&&l<=r) l++;
			while (l<=r&&f[e^1][i]<=f[e^1][dui[r]]) r--;
			dui[++r]=i;
			f[e][i]=f[e^1][dui[l]]+sum[i];
		}
		ans=min(ans,f[e][n]);
		if (j!=tot){
			F(i,1,n) sum[i]=sum[i-1]+absa(a[i]-z[j+1].x);
			F(i,1,n) f[e][i]=f[e][i]-sum[i];
			e^=1;
		}
	}
	wrn(ans);
	return 0;
}
