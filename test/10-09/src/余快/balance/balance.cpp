/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
#define N 100055
//#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(ll x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(ll x){wr(x);put;}inline void wri(ll x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int n,m,a[N];
ll sum,ans;
vector <int> z[N];
signed main(){
	freopen("balance.in","r",stdin);
	freopen("balance.out","w",stdout);
	n=read();
	F(i,1,n) a[i]=read();
	F(i,1,n){
		if (i!=n&&a[i+1]!=a[i]) z[a[i]].pb(a[i+1]),sum+=abs(a[i+1]-a[i]);
		if (i!=1&&a[i-1]!=a[i]) z[a[i]].pb(a[i-1]);
	}
	ans=sum;
	F(i,1,100000){
		if (!z[i].empty()){
			sort(z[i].be,z[i].en);
			int t=z[i].size()-1;
			ll num=0;
			int mid=t/2;
			F(j,0,t){
				num+=abs(z[i][j]-i)-abs(z[i][j]-z[i][mid]);
			}
			ans=min(ans,sum-num);
		}
	}
	wrn(ans);
	return 0;
}
