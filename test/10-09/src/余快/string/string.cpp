/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
//#define N 500055
//#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int n,m,ans,dui[100],vis[100],len[50005],cnt,z[50005],tot;
int Next[1000*2],head[1000],to[1000*2],nedge,in1[1000];
#define V to[i]
void add(int a,int b){Next[++nedge]=head[a];head[a]=nedge;to[nedge]=b;in1[b]++;}
char str[50005][25];
struct xx{
	int num,p;
	int Next[26];
}c[1000005];
void add_s(int x){
	c[0].num++;
	for (int i=0,j=1;j<=len[x];j++){
		if (!c[i].Next[str[x][j]-'a']) c[i].Next[str[x][j]-'a']=++cnt;
		i=c[i].Next[str[x][j]-'a'];c[i].num++;if (j==len[x]) c[i].p++;
	}
}
int query(int x){
	for (int i=0,j=1;j<=len[x];j++){
		if (c[i].p) return 0;
		int t=str[x][j]-'a';
		F(k,0,25){
			if (k==t) continue;
			if (c[i].Next[k]) add(t,k);
		}
		i=c[i].Next[t];
	}
	int r=0;
	F(i,0,25){
		if (in1[i]==0) dui[++r]=i,vis[i]=1;
	}
	F(l,1,r){
		int t=dui[l];
		go(i,t){
			in1[V]--;
			if (in1[V]==0&&vis[V]==0){
				vis[V]=1;dui[++r]=V;
			}
		}
	}
	return (r==26);
}
signed main(){
	freopen("string.in","r",stdin);freopen("string.out","w",stdout);
	n=read();
	F(i,1,n) scanf("%s",str[i]+1),len[i]=strlen(str[i]+1),add_s(i);
	F(i,1,n){
		F(j,0,25) in1[j]=head[j]=vis[j]=0;nedge=0;
		if (query(i)) z[++tot]=i;
	}
	wrn(tot);
	F(i,1,tot) printf("%s\n",str[z[i]]+1);
	return 0;
}
