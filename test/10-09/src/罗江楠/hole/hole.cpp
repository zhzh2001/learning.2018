#include <bits/stdc++.h>
#define N 5020
#define ll long long
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}

pair<int, int> b[N];
int n, m;
long long a[N];

/*
long long check(int offset) {
  long long res = 0;
  for (int i = 0; i < n; ++ i) {
    res += llabs((ll) p[i+offset] - a[i + 1]);
  }
  return res;
}
*/
long long f[N];
int main(int argc, char const *argv[]) {
  freopen("hole.in", "r", stdin);
  freopen("hole.out", "w", stdout);

  n = read(); m = read();
  for (int i = 1; i <= n; ++ i) {
    a[i] = read();
  }
  int sum = 0;
  for (int i = 1; i <= m; ++ i) {
    b[i].first = read();
    b[i].second = read();
    sum += b[i].second;
  }
  if (sum < n) {
    return puts("Stupid game!"), 0;
  }
  sort(a + 1, a + n + 1);
  sort(b + 1, b + m + 1);
  // for (int i = 1; i <= m; ++ i) {
  //   for (int j = 1; j <= b[i].second; ++ j) {
  //     p[++ cnt] = b[i].first;
  //   }
  // }
  /*
  int l = 1, r = sum;

  while (l < r) {
    int mid1 = (r-l+1)/3+l;
    int mid2 = r-(r-l+1)/3;

    if (check(mid1) < check(mid2)) {
      r = mid2 - 1;
    } else {
      l = mid1 + 1;
    }
  }

  long long res = min(min(check(l - 1), check(l)), check(l + 1));
  printf("%lld\n", res);
  */

  memset(f, 0x3f, sizeof f);
  f[0] = 0;
  for (int i = 1; i <= m; ++ i) {
    for (int k = 1; k <= b[i].second; ++ k) {
      for (int j = n; j; -- j) {
        f[j] = min(f[j], f[j - 1] + abs(a[j] - b[i].first));
      }
    }
  }

  printf("%lld\n", f[n]);

  return 0;
}
/*

直接 f[i][j] -> 第 i 个洞，放了 j 个球的最小值

f[i][j] = min(f[i-1][j], f[i-1][j-1]+abs(a[j]-p[i]))

滚存即可，复杂度 O(n^3)

考虑优化转移，发现可以从后往前转移，这样只需要一个 O(n) 数组即可。

for (int i = 1; i <= cnt; ++ i) {
  for (int j = n; j; -- j) {
    f[j] = min(f[j], f[j - 1] + abs(a[j] - p[i]));
  }
}

复杂度仍为 O(n^3)，无法继续优化。

f[i][j] -> 第 i 个球，最后一个用的是第 j 个洞的最小值。

f[i][j] = min{ f[i-1][k] + min[k+1,j]{ abs(a[i] - p[j]) } }

f[i+1][j+k] = min{  }

for (int i = 1; i <= n; ++ i) {
  for (int j = i; j <= cnt; ++ j) {
    ll min_value = 1ll << 60;
    for (int k = j-1; k >= i-1; ++ k) {
      min_value = min(min_value, abs(a[i] - p[j]));
      f[i][j] = min(f[i][j], f[i-1][k] + min_value);
    }
  }
}

你们啊，还是 too young too simple，sometimes naive

*/