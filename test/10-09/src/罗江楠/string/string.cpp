#pragma GCC optimize 2
#include <bits/stdc++.h>
#define N 50020
#define ll long long
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}
char str[N][25];
int ch[N*20][26], cnt = 1;
bool imp[N*20], isend[N*20];

void insert(char *s) {
  int n = strlen(s + 1);
  int now = 1;
  for (int i = 1; i <= n; ++ i) {
    int to = s[i] - 'a';
    if (!ch[now][to]) ch[now][to] = ++ cnt;
    now = ch[now][to];
  }
  isend[now] = true;
}
bool query(char *s) {
  int n = strlen(s + 1);
  int now = 1;
  for (int i = 1; i <= n; ++ i) {
    int to = s[i] - 'a';
    if (imp[ch[now][to]]) return false;
    now = ch[now][to];
  }
  return true;
}

namespace __greater_map {

int small[25][26];
int WORK_DEP = 0;

// x > S
inline int greater(int x, int S) {
  small[WORK_DEP][x] |= S;
  for (int i = 0; i < 26; ++ i) {
    if (small[WORK_DEP][x] >> i & 1) {
      small[WORK_DEP][x] |= small[WORK_DEP][i];
    }
  }
}
// 返回 S 中小于其他元素的集合(不可能被选择)
inline int getSmaller(int S) {
  int T = 0;
  for (int i = 0; i < 26; ++ i) {
    if (S >> i & 1) {
      for (int j = 0; j < 26; ++ j) {
        if (S >> j & 1) {
          if (small[WORK_DEP][i] >> j & 1) {
            T |= (1 << j);
          }
        }
      }
    }
  }
  return T;
}
// 这些元素的大小关系是不确定的
inline int getGreater(int S) {
  return S - getSmaller(S);
}

} // namespace __greater_map

// 搜到哪个点，大小关系
void dfs(int x) {
  // printf("%d\n", x); gm.output();
  if (isend[x]) {
    for (int i = 0; i < 26; ++ i) {
      if (ch[x][i]) {
        imp[ch[x][i]] = true;
      }
    }
    return;
  }
  int status = 0;
  for (int i = 0; i < 26; ++ i) {
    if (ch[x][i]) {
      status |= (1 << i);
    }
  }
  int greater = __greater_map::getGreater(status);
  if (greater) {
    for (int i = 0; i < 26; ++ i) {
      if (greater >> i & 1) {
        memcpy(__greater_map::small[__greater_map::WORK_DEP + 1],
                   __greater_map::small[__greater_map::WORK_DEP],
            sizeof __greater_map::small[__greater_map::WORK_DEP]);
        ++ __greater_map::WORK_DEP;
        __greater_map::greater(i, status ^ (1 << i));
        dfs(ch[x][i]);
        -- __greater_map::WORK_DEP;
      }
    }
    int bad = status ^ greater;
    for (int i = 0; i < 26; ++ i) {
      if (bad >> i & 1) {
        imp[ch[x][i]] = true;
      }
    }
  } else {
    // the fastest way is to do nothing here
    // %%% timber ak king
  }
}

int main(int argc, char const *argv[]) {
  freopen("string.in", "r", stdin);
  freopen("string.out", "w", stdout);

  int n = read();

  for (int i = 1; i <= n; ++ i) {
    scanf("%s", str[i] + 1);
    insert(str[i]);
  }

  dfs(1);

  vector<int> ans;
  for (int i = 1; i <= n; ++ i) {
    if (query(str[i])) {
      ans.push_back(i);
    }
  }
  printf("%u\n", ans.size());
  for (size_t i = 0; i < ans.size(); ++ i) {
    puts(str[ans[i]] + 1);
  }

  // fprintf(stderr, "%.4lf\n", (&__a - &__b) / 1024.0 / 1024.0);
  // 25.5806
  return 0;
}