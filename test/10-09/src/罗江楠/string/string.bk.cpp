#pragma GCC optimize 2
#include <bits/stdc++.h>
#define N 50020
#define ll long long
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}
char str[N][25];

int ch[N*20][26], cnt = 1;
bool imp[N*20], isend[N*20];
void insert(char *s) {
  int n = strlen(s + 1);
  int now = 1;
  for (int i = 1; i <= n; ++ i) {
    int to = s[i] - 'a';
    if (!ch[now][to]) ch[now][to] = ++ cnt;
    now = ch[now][to];
  }
  isend[now] = true;
}
bool query(char *s) {
  int n = strlen(s + 1);
  int now = 1;
  for (int i = 1; i <= n; ++ i) {
    int to = s[i] - 'a';
    if (imp[ch[now][to]]) return false;
    now = ch[now][to];
  }
  return true;
}
struct greater_map {
  int small[26];
  greater_map(){memset(small,0,sizeof small);}
  // x > S
  inline int greater(int x, int S) {
    small[x] |= S;
    for (int i = 0; i < 26; ++ i) {
      if (small[x] >> i & 1) {
        small[x] |= small[i];
      }
    }
  }
  // x > y ?
  inline int isGreater(int x, int y) {
    return small[x] >> y & 1;
  }
  // 返回 S 中小于其他元素的集合(不可能被选择)
  inline int getSmaller(int S) {
    int T = 0;
    for (int i = 0; i < 26; ++ i) {
      if (S >> i & 1) {
        for (int j = 0; j < 26; ++ j) {
          if (S >> j & 1) {
            if (isGreater(i, j)) {
              T |= (1 << j);
            }
          }
        }
      }
    }
    return T;
  }
  // 这些元素的大小关系是不确定的
  inline int getGreater(int S) {
    return S - getSmaller(S);
  }
};
// 搜到哪个点，大小关系
void dfs(int x, greater_map &gm) {
  // printf("%d\n", x); gm.output();
  if (isend[x]) {
    for (int i = 0; i < 26; ++ i) {
      if (ch[x][i]) {
        imp[ch[x][i]] = true;
      }
    }
    return;
  }
  int status = 0;
  for (int i = 0; i < 26; ++ i) {
    if (ch[x][i]) {
      status |= (1 << i);
    }
  }
  int greater = gm.getGreater(status);
  if (greater) {
    for (int i = 0; i < 26; ++ i) {
      if (greater >> i & 1) {
        greater_map _gm = gm;
        _gm.greater(i, status ^ (1 << i));
        dfs(ch[x][i], _gm);
      }
    }
    int bad = status ^ greater;
    for (int i = 0; i < 26; ++ i) {
      if (bad >> i & 1) {
        imp[ch[x][i]] = true;
      }
    }
  } else {
    // ???
  }
}

int main(int argc, char const *argv[]) {
  freopen("string.in", "r", stdin);
  freopen("string.out", "w", stdout);

  int n = read();

  for (int i = 1; i <= n; ++ i) {
    scanf("%s", str[i] + 1);
    insert(str[i]);
  }

  greater_map _;
  dfs(1, _);

  vector<int> ans;
  for (int i = 1; i <= n; ++ i) {
    if (query(str[i])) {
      ans.push_back(i);
    }
  }
  printf("%u\n", ans.size());
  for (size_t i = 0; i < ans.size(); ++ i) {
    puts(str[ans[i]] + 1);
  }

  return 0;
}