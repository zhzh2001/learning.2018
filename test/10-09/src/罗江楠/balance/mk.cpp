#include <bits/stdc++.h>
#define N 100020
#define ll long long
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}
int main(int argc, char const *argv[]) {
  srand(time(0));
  freopen("balance.in", "w", stdout);
  int n = 100000;
  printf("%d\n", n);
  for (int i = 1; i <= n; ++ i) {
    printf("%d ", rand() * rand() % (100001) + 1);
  }
  return 0;
}