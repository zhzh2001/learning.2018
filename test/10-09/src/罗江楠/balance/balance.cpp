#pragma GCC optimize 2
#include <bits/stdc++.h>
#define N 100020
#define ll long long
using namespace std;
inline int read(){
  int x=0,f=1;char ch=getchar();
  while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
  while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
  return f?x:-x;
}

vector<int> b[N];
bool vis[N];
long long a[N], c[N];

int main(int argc, char const *argv[]) {
  freopen("balance.in", "r", stdin);
  freopen("balance.out", "w", stdout);

  int n = read();

  for (int i = 1; i <= n; ++ i) {
    a[i] = read();
    vis[a[i]] = true;
  }

  long long sum = 0;
  for (int i = 2; i <= n; ++ i) {
    if (a[i] != a[i - 1]) {
      b[a[i]].push_back(a[i-1]);
      b[a[i-1]].push_back(a[i]);
      c[a[i]]   += llabs(a[i] - a[i - 1]);
      c[a[i-1]] += llabs(a[i] - a[i - 1]);
    }
    sum += llabs(a[i] - a[i - 1]);
  }

  if (!sum) {
    // all equal
    return puts("0"), 0;
  }

  long long ans = 1ll << 60;
  for (int i = 0; i <= 1e5; ++ i) {
    if (vis[i] && b[i].size()) {
      sort(b[i].begin(), b[i].end());
      long long mid = b[i][(b[i].size() + 1) / 2];
      long long res = sum - c[i];
      for (size_t j = 0; j < b[i].size(); ++ j) {
        res += llabs(b[i][j] - mid);
      }
      // printf("res = %lld\n", res);
      ans = min(ans, res);
    }
  }

  printf("%lld\n", ans);

  return 0;
}