#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 100005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
struct data{ ll id,num; }a[N];
ll n,m,now,ans,x[N],b[N];
bool cmp(data x,data y) { return x.num<y.num; }
int main(){
	freopen("balance.in","r",stdin);
	freopen("balance.out","w",stdout);
	n=read(); m=0;
	rep(i,1,n){
		ll t=read();
		if (m&&t==x[m]) continue;
		x[++m]=t;
	}
	n=m;
	rep(i,2,n) now+=abs(x[i]-x[i-1]);
	rep(i,1,n){
		a[i].id=i;
		a[i].num=x[i];
	}
	sort(a+1,a+1+n,cmp); ans=now;
	for (ll l=1,r=1;l<=n;l=++r){
		while (r<=n&&a[r+1].num==a[l].num) ++r;
		ll len=0,tmp=now;
		rep(i,l,r){
			if (a[i].id!=1){
				b[++len]=x[a[i].id-1];
				tmp-=abs(x[a[i].id-1]-a[i].num);
			}
			if (a[i].id!=n){
				b[++len]=x[a[i].id+1];
				tmp-=abs(x[a[i].id+1]-a[i].num);
			}
		}
		sort(b+1,b+1+len);
		rep(i,1,len) tmp+=abs(b[(len+1)/2]-b[i]);
		ans=min(ans,tmp);
	}
	printf("%lld",ans);
}
