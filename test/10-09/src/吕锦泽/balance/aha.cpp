#include<bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
using namespace std;
inline GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
inline void write(LL x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
inline void writeln(LL x) {
    write(x); puts("");
}

const int N = 5011; 
int n, m, all; 
int a[N], where[N]; 
struct hole{
	int len, rest; 
}b[N]; 
LL sum, ans; 

void dfs(int x) {
	if(x > n) {
		if(sum < ans) ans = sum; 
		return; 
	}
	For(i, 1, m) {
		if(b[i].rest) {
			--b[i].rest; 
			where[x] = i; 
			LL tmp = abs(1ll*a[x]-b[i].len); 
			sum += tmp;
			if(sum < ans) dfs(x+1); 
			sum -= tmp; 
			++b[i].rest; 
		}
	}
}

inline bool cmp_x(int a, int b) {
	return a > b; 
} 

inline bool cmp_len(hole a, hole b) {
	return a.len > b.len; 
} 

int main() {
/*	freopen("hole.in","r",stdin); 
	freopen("hole.out","w",stdout); */
	n = read(); m = read(); 
	ans = 1e15; sum = 0; 
	For(i, 1, n) 
		a[i] = read(); 
//	sort(a+1, a+n+1, cmp_x); 
	For(i, 1, m) {
		b[i].len = read(); 
		b[i].rest = read(); 
		all += b[i].rest; 
	}
//	sort(b+1, b+m+1, cmp_len); 
	if(n>all) {
		puts("Stupid game!"); 
		return 0; 
	}
	dfs(1); 
	writeln(ans); 
}

/*


4 5 
6 2 8 9
3 6 
2 1 
3 6 
4 7
4 7

7 2 
10 20 30 40 50 45 35 
-1000000000 10 
1000000000 1 

*/
