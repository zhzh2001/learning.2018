#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define inf (1ll<<50)
#define N 5005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,a[N],sum[N],sumh[N],f[N][N];
struct data{ ll b,c; }h[N];
bool cmp(data x,data y) { return x.b<y.b; }
int main(){
	freopen("hole.in","r",stdin);
	freopen("hole.out","w",stdout);
	n=read(); m=read();
	rep(i,1,n) a[i]=read();
	sort(a+1,a+n+1);
	rep(i,1,m) h[i].b=read(),h[i].c=read();
	sort(h+1,h+m+1,cmp);
	rep(i,1,m){
		sumh[i]=sumh[i-1]+h[i].c;
		f[i][0]=inf;
	}
	if(sumh[m]<n) return puts("Stupid game!")&0;
	rep(i,1,m){
		rep(j,1,n){
			sum[j]=sum[j-1]+abs(a[j]-h[i].b);
			f[i][j]=inf;
		}
		rep(j,0,min(sumh[i-1],n)) rep(k,j,min(j+h[i].c,n))
			f[i][k]=min(f[i-1][j]+sum[k]-sum[j],f[i][k]);
	}
	printf("%lld",f[m][n]);
}
