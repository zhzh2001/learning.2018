#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 1000005
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=x*10+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,num,son[N][30],end[N],vis[N],ans,f[30][30]; char s[50005][30];
void dfs(ll now){
	if (end[now]) { ans+=end[now]; vis[now]=1; return; }
	rep(i,0,25){
		if (!son[now][i]) continue;
		ll boo=0;
		rep(j,0,25){
			if (!son[now][j]||i==j) continue;
			if (f[j][i]) boo=1;
		}
		if (boo) continue;
		rep(j,0,25){
			if (!son[now][j]||i==j) continue;
			++f[i][j];
		}
		dfs(son[now][i]);
		rep(j,0,25){
			if (!son[now][j]||i==j) continue;
			--f[i][j];
		}
	}
}
int main(){
	freopen("string.in","r",stdin);
	freopen("string.out","w",stdout);
	n=read();
	rep(i,1,n){
		scanf("%s",s[i]+1);
		ll len=strlen(s[i]+1);
		ll now=0;
		rep(j,1,len){
			if (!son[now][s[i][j]-'a']) son[now][s[i][j]-'a']=++num;
			now=son[now][s[i][j]-'a'];
		}
		++end[now];
	}
	dfs(0); printf("%d\n",ans);
	rep(i,1,n){
		ll len=strlen(s[i]+1);
		ll now=0;
		rep(j,1,len) now=son[now][s[i][j]-'a'];
		if (vis[now]){
			rep(j,1,len) putchar(s[i][j]);
			puts("");
		}
	}
}
