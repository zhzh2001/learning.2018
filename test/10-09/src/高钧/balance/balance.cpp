#include <cstdio>
#include <vector>
#include <cstring>
#include <algorithm>
using namespace std;
#define int long long
const int N=100005,inf=0x7ffffffffffff;
int n,a[N],b[N];
struct node{int l,r;};
vector<node>G[N];
signed main()
{
	freopen("balance.in","r",stdin);
	freopen("balance.out","w",stdout);
	int tmp,i,j,k,x,cnt=0,re,ma=0; scanf("%lld",&n); for(i=1;i<=n;i++){scanf("%lld",&x);ma=max(ma,x);if((!cnt)||a[cnt]!=x)a[++cnt]=x;} n=cnt;
	for(i=2;i<n;i++)G[a[i]].push_back((node){a[i-1],a[i+1]}); for(i=2;i<=n;i++)re+=abs(a[i]-a[i-1]); memmove(b,a,sizeof a);
	for(i=1;i<=ma;i++)
	{
		for(j=1;j<=ma;j++)
		{
			tmp=0; for(k=1;k<=n;k++){if(a[k]==i)a[k]=j;if(k!=1)tmp+=abs(a[k]-a[k-1]);} for(k=1;k<=n;k++)a[k]=b[k]; re=min(re,tmp);
		}
	}printf("%lld\n",re);
}
