#include<bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
using namespace std;
inline GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
inline void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
inline void writeln(GG x) {
    write(x); puts("");
}

const int N = 1000011; 
int n, val,ans; 
char s[50005][30];
int son[N][30], end[N], vis[N], f[30][30]; 
void dfs(int root){
	if (end[root]) { 
		ans+=end[root]; 
		vis[root]=1; 
		return; 
	}
	For(i, 0, 25){
		if (!son[root][i]) continue;
		int boo=0;
		For(j, 0, 25){
			if (!son[root][j] || i==j) continue;
			if (f[j][i]) boo=1;
		}
		if (boo) continue;
		For(j, 0, 25){
			if (!son[root][j] || i==j) continue;
			++f[i][j];
		}
		dfs(son[root][i]);
		For(j, 0, 25){
			if (!son[root][j]||i==j) continue;
			--f[i][j];
		}
	}
}
int main(){
	freopen("string.in","r",stdin);
	freopen("string.out","w",stdout);
	n = read();
	For(i, 1, n){
		scanf("%s",s[i]+1);
		int len=strlen(s[i]+1);
		int root=0;
		For(j,1,len){
			if (!son[root][s[i][j]-'a']) son[root][s[i][j]-'a'] = ++val;
			root = son[root][s[i][j]-'a'];
		}
		++end[root];
	}
	dfs(0); 
	writeln(ans); 
	For(i, 1, n){
		int len=strlen(s[i]+1);
		int root=0;
		For(j, 1, len) root = son[root][s[i][j]-'a'];
		if (vis[root]) {
			For(j, 1, len) putchar(s[i][j]); 
			puts("");
		}
	}
}
