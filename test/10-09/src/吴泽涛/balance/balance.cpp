#include<bits/stdc++.h>
#define LL long long
#define GG long long 
#define For(i, j, k) for(int i=j; i<=k; i++)
#define Dow(i, j, k) for(int i=j; i>=k; i--)
using namespace std;
inline GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
inline void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
inline void writeln(GG x) {
    write(x); puts("");
}

const int N = 100011; 
struct data{ 
	GG lin,val; 
}a[N];
GG n, m, root, ans;
GG x[N], b[N]; 
bool cmp(data x,data y) { 
	return x.val < y.val; 
}
/*
inline void work(int val) {
	tot = 0; 
	For(i, 1, n) 
		if(a[i]==val) {
//			if(i!=1 && a[i-1]!=val) b[++tot] = a[i-1]; 
//			if(i!=n && a[i+1]!=val) b[++tot] = a[i+1]; 
			if(i!=1) b[++tot] = a[i-1]; 
			if(i!=n) b[++tot] = a[i+1]; 
		} 
	sort(b+1, b+tot+1); 
	int mid = (1+tot)/2; 
	LL sum = 0; 
	For(i, 1, tot) sum = sum+abs(b[mid]-b[i]); 
	if(sum>ans) ans = sum; 
}
*/

int main(){
	freopen("balance.in","r",stdin);
	freopen("balance.out","w",stdout);
	n = read(); m=0;
	For(i, 1, n){
		GG t = read();
		if (m!=0&&t==x[m]) continue;
		x[++m]=t;
	}
	n = m;
	/*
	For(i, 1, n) {
		if(a[i-1]>a[i]) up[a[i]] += a[i-1]-a[i]; 
			else down[a[i]] += a[i]-a[i-1]; 
		if(a[i+1]>a[i]) up[a[i]] += a[i+1]-a[i]; 
			else down[a[i]] += a[i]-a[i+1];
	}
	int x = N-2, y = N-2; 
	down[N-2] = up[N-2] = 0; 
	For(i, 0, 100000) {
		if(down[x] < down[i]) x = i; 
		if(up[y] < up[i]) y = i; 
	}
	For(i, 2, n) all = all+abs(a[i]-a[i-1]); 
	
	work(x); 
	work(y); 
//	writeln(ans); 
//	writeln(all); 
//	writeln(x); 
//	writeln(y); 
	writeln(all-ans);	
	*/
	For(i, 2, n) 
		root += abs(x[i]-x[i-1]);
	For(i, 1, n){
		a[i].lin = i;
		a[i].val = x[i];
	}
	sort(a+1, a+1+n, cmp); ans=root;
	for (GG l=1,r=1;l<=n;l=++r){
		while (r<=n&&a[r+1].val==a[l].val) ++r;
		GG len=0,tmp=root;
		For(i, l, r){
			if (a[i].lin!=1){
				b[++len]=x[a[i].lin-1];
				tmp -= abs(x[a[i].lin-1]-a[i].val);
			}
			if (a[i].lin!=n){
				b[++len]=x[a[i].lin+1];
				tmp -= abs(x[a[i].lin+1]-a[i].val);
			}
		}
		sort(b+1, b+1+len);
		For(i, 1, len) tmp += abs(b[(len+1)/2]-b[i]);
		ans = min(ans, tmp);
	}
	writeln(ans); 
}
