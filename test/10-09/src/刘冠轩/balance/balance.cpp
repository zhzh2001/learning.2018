#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){
	int x=0;char c=getchar();bool p=1;
	for(;!isdigit(c);c=getchar())if(c=='-')p=0;
	for(;isdigit(c);c=getchar())x=(x<<1)+(x<<3)+(c^48);
	return p?x:-x;
}
const int N=100005;
int n,a[N],s[N],c[N],ans,mx,mid,k=0;
vector<int>b[N];
signed main(){
	freopen("balance.in","r",stdin);
	freopen("balance.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<n;i++)
		if(a[i+1]!=a[i]){
			ans+=abs(a[i+1]-a[i]);
			b[a[i]].push_back(a[i+1]);
			b[a[i+1]].push_back(a[i]);
			s[a[i]]+=abs(a[i+1]-a[i]);
			s[a[i+1]]+=abs(a[i+1]-a[i]);
		}
	for(int i=0;i<=100000;i++)if(s[i]){
		for(int j=0;j<b[i].size();j++)c[j+1]=b[i][j];
		sort(c+1,c+b[i].size()+1);
		k=0;mid=(b[i].size()+1)/2;
		for(int l=1;l<=b[i].size();l++)k+=abs(c[l]-c[mid]);
		mx=max(mx,s[i]-k);
	}
	cout<<ans-mx;
	return 0;
}
