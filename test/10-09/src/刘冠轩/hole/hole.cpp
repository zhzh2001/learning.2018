#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){
	int x=0;char c=getchar();bool p=1;
	for(;!isdigit(c);c=getchar())if(c=='-')p=0;
	for(;isdigit(c);c=getchar())x=(x<<1)+(x<<3)+(c^48);
	return p?x:-x;
}
const int N=5005;
const int inf=200000000000000ll;
int n,m,Z,X,u[N],ans,S,SS,l,r,sum;
struct node{int b,c;}p[N];
struct nooe{int p,s;}a[N];
bool cmp(node x,node y){return x.b<y.b;}
signed main(){
	freopen("hole.in","r",stdin);
	freopen("hole.out","w",stdout);
	srand(time(NULL));
	n=read();m=read();
	for(int i=1;i<=n;i++)a[i].p=read();
	for(int i=1;i<=m;i++)p[i].b=read(),Z+=(p[i].c=read());
	if(Z<n){puts("Stupid game!");return 0;}
	sort(p+1,p+m+1,cmp);
	X=222222222/n/m;ans=inf;
	for(int i=1;i<=n;i++)
		a[i].s=lower_bound(p+1,p+m+1,(node){a[i].p,0},cmp)-p;
	for(int T=1;T<X;T++){
		sum=0;
		for(int i=1;i<=m;i++)u[i]=p[i].c;
		for(int i=1;i<=n;i++){
			if(a[i].s>m)r=inf;else{
				S=a[i].s;
				while(S<=m)if(u[S])break;else S++;
				if(S>m)r=inf;
				else r=p[S].b-a[i].p;
			}
			if(a[i].s==1)l=inf;else{
				SS=a[i].s-1;
				while(SS)if(u[SS])break;else SS--;
				if(SS<1&&a[i].p<p[a[i].s].b)l=inf;
				else l=a[i].p-p[SS].b;
			}
			if(l<=r){u[SS]--;sum+=l;}
			else{u[S]--;sum+=r;}
			if(sum>=ans)break;
		}
		ans=min(ans,sum);
		random_shuffle(a+1,a+n+1);
	}
	cout<<ans;
	return 0;
}
