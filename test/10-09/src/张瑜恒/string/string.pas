var
  n,i,j,m,l,sum,mm,k,t:longint;
  min:string;
  a:array [0..2010] of string;
  b:array [0..2010,'a'..'z'] of longint;
  c:array [0..50000] of longint;
function fmin(l,r:longint):longint;
  begin
    if (l<r) then exit(l) else exit(r);
  end;
procedure f(l,r:longint);
  var
    x,y,mid,t:longint;
  begin
    x:=l;y:=r;mid:=c[(l+r) div 2];
    repeat
      while (c[x]<mid) do inc(x);
      while (c[y]>mid) do dec(y);
      if (x<=y) then
        begin
          t:=c[x];c[x]:=c[y];c[y]:=t;
          inc(x);dec(y);
        end;
    until x>y;
    if (l<y) then f(l,y);
    if (x<r) then f(x,r);
  end;
begin
  assign(input,'string.in');
  reset(input);
  assign(output,'string.out');
  rewrite(output);
    readln(n);
    for i:=1 to n do
      begin
        readln(a[i]);
        k:=0;
        for j:=1 to length(a[i]) do
          if (b[i][a[i][j]]=0) then begin k:=k+1;b[i][a[i][j]]:=k; end;
      end;
    m:=0;l:=1;
    for i:=1 to n do
      begin
        min:=a[i];m:=m+1;c[m]:=i;
        for j:=1 to n do
          if (i<>j) then
            begin
              sum:=fmin(length(min),length(a[j]));t:=1;
              for k:=1 to sum+1 do
                begin
                  if (k=sum+1) then
                    begin
                      if (length(min)<length(a[j])) then t:=0;
                      if (length(min)=length(a[j])) then t:=2;
                      break;
                    end;
                  if (b[i][a[j][k]]=0) then begin t:=0;break; end;
                  if (b[i][min[k]]<b[i][a[j][k]]) then begin t:=0;break; end;
                  if (b[i][min[k]]>b[i][a[j][k]]) then break;
                end;
              if (t=2) then begin m:=m+1;c[m]:=j; end;
              if (t=1) then begin m:=l;c[m]:=j;min:=a[j]; end;
            end;
        l:=m+1;
      end;
    f(1,m);
    mm:=0;c[0]:=c[1]-1;
    for i:=1 to m do
      if (c[i]<>c[i-1]) then mm:=mm+1;
    writeln(mm);
    for i:=1 to m do
      if (c[i]<>c[i-1]) then writeln(a[c[i]]);
  close(input);
  close(output);
end.