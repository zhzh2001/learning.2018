#include<cstdio>
#include<algorithm> 
#include<cstring>
#include<vector>
#define re register
#define rep(x,a,b) for (re int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (re int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (re int x=hd[a];~x;x=nx[x])
#define N 100010
using namespace std;
vector<int>b[N];
int n,a[N],c[N],t,sum,ans,s;
bool d[N];
inline double Abs(double x){
	if (x<0.0) return -1.0*x;
}
int main(){
	freopen("balance.in","r",stdin);
	freopen("balance.out","w",stdout);
	scanf("%d",&n);
	rep(i,1,n) scanf("%d",&a[i]);
	rep(i,1,n){
		if (i!=1) {
			t+=abs(a[i]-a[i-1]);
			if (a[i]!=a[i-1])b[a[i]].push_back(a[i-1]);
		}
		if (i!=n) if (a[i]!=a[i+1])b[a[i]].push_back(a[i+1]);
	}
	s=ans=t;
	memset(d,0,sizeof(d));
	rep(i,1,n){
		if (d[a[i]]) continue;
		d[a[i]]=1;
		int m=b[a[i]].size();
		rep(j,1,m) c[j]=b[a[i]][j-1];
		sort(c+1,c+m+1);
		int zh;
		if (m&1) zh=c[m/2+1];
			else zh=(int)((c[m/2]+c[m/2+1])/2);
		sum=s;
		rep(j,1,m) sum+=abs(c[j]-zh)-abs(c[j]-a[i]);
		if (sum<ans) ans=sum;
	}
	printf("%d",ans);
	return 0;
}
