#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

const int kInf = 0x3f3f3f3f;

ll n, m, a[5005];
ll b[5005], c[5005];

ll cho[11], arp[11];
ll ans = 1LL << 60, CNT = 0;

void Work() {
  CNT++;
  ll tmp = 0;
  memset(arp, 0x00, sizeof arp);
  for (int i = 1; i <= n; ++i) {
    tmp += llabs(a[i] - b[cho[i]]);
    ++arp[cho[i]];
  }
  for (int i = 1; i <= m; ++i)
    if (arp[i] > c[i])
      return;
  if (tmp < ans) {
    ans = tmp;
    // fprintf(stderr, "Hello, World\n");
  }
  return;
}

void DFS(int dep) {
  if (dep == n) {
    Work();
    return;
  }
  for (int i = 1; i <= m; ++i) {
    cho[dep + 1] = i;
    DFS(dep + 1);
  }
}

int Solve1() {
  DFS(0);
  printf("%lld\n", ans);
  // fprintf(stderr, "%lld\n", CNT);
  return 0;
}

int main() {
  freopen("hole.in", "r", stdin);
  freopen("hole.out", "w", stdout);
  scanf("%lld%lld", &n, &m);
  for (int i = 1; i <= n; ++i)
    scanf("%lld", a + i);
  for (int i = 1; i <= m; ++i) {
    scanf("%lld%lld", b + i, c + i);
  }
  if (n <= 10)
    return Solve1();
  puts("Stupid game!");
  return 0;
}