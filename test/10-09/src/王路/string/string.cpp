#pragma GCC optimize 2
#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <set>
#include <vector>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

const int kMaxN = 5e5 + 5, kLen = 25;

int n;
char ss[kMaxN][kLen];
int len[kMaxN];

int main() {
  freopen("string.in", "r", stdin);
  freopen("string.out", "w", stdout);
  scanf("%d", &n);
  for (int i = 1; i <= n; ++i) {
    scanf("%s", ss[i]);
    len[i] = strlen(ss[i]);
  }
  vector<int> ANS;
  for (int i = 1; i <= n; ++i) {
    set<int> p[26];
    bool fl = true;
    for (int j = 1; j <= n; ++j) {
      if (i == j)
        continue;
      int m = min(len[i], len[j]);
      bool same = true;
      for (int k = 0; k < m; ++k) {
        if (p[(int)ss[j][k] - 'a'].count(ss[i][k])) {
          fl = false;
          break;
        }
        if (ss[i][k] != ss[j][k]) {
          p[(int)ss[i][k] - 'a'].insert(ss[j][k]);
          same = false;
          break;
        }
      }
      if (!same)
        continue;
      if (len[i] <= len[j])
        continue;
      if (len[i] > len[j])
        fl = false;
    }
    if (fl) ANS.push_back(i);
  }
  printf("%d\n", (int)ANS.size());
  for (int j = 0; j < (int)ANS.size(); ++j)
    puts(ss[ANS[j]]);
  return 0;
}