#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <set>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

const int kMaxN = 5e5 + 5, kLen = 25;

int a[kLen], val[256], n;

struct Node {
  char s[kLen];
  int len, id;
  inline bool operator<(const Node &other) const {
    int m = min(len, other.len);
    for (int i = 0; i < m; ++i) {
      if (val[(int)s[i]] < val[(int)other.s[i]])
        return true;
      if (val[(int)s[i]] > val[(int)other.s[i]])
        return false;
    }
    if (len < other.len)
      return true;
    if (len > other.len)
      return true;
    return false;
  }
} c[kMaxN];

// 对拍造数据保证不完全相同

inline bool cmp(const Node &a, const Node &b) { return a.id < b.id; }

int main() {
  freopen("string.in", "r", stdin);
  freopen("bf.out", "w", stdout);
  scanf("%d", &n);
  for (int i = 1; i <= n; ++i) {
    scanf("%s", c[i].s);
    c[i].len = strlen(c[i].s);
    c[i].id = i;
  }
  for (int i = 0; i < 10; ++i)
    a[i] = i + 'a';
  set<int> S;
  do {
    for (int i = 0; i < 10; ++i)
      val[a[i]] = i;
    sort(c + 1, c + n + 1);
    S.insert(c[1].id);
  } while (next_permutation(a, a + 10));
  printf("%d\n", (int)S.size());
  sort(c + 1, c + n + 1, cmp);
  for (int i = 1; i <= n; ++i) {
    if (S.count(i))
      puts(c[i].s);
  }
  return 0;
}