// #pragma GCC optimize 2
#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <queue>
#include <set>
#include <string>
#include <vector>
using namespace std;

typedef long long ll;
typedef pair<int, int> pii;

const int kMaxN = 5e5 + 5, kLen = 22, kAlpha = 26, kLow = 'a';

int n, ans, pacnt;
char buf[kLen];
set<int> S[kAlpha];
vector<int> pa[kMaxN];
vector<int> ANS;

struct Trie {
  int ch[kMaxN * kLen][kAlpha];
  int lk[kMaxN * kLen], val[kMaxN * kLen], vl[kMaxN * kLen], rt, cnt;
  inline Trie() : rt(0), cnt(0) {}
  inline void Insert(const char *s, int id) {
    int now = rt;
    for (int i = 0; s[i]; ++i) {
      int c = s[i] - kLow;
      if (!ch[now][c]) {
        ++cnt;
        ch[now][c] = cnt;
      }
      now = ch[now][c];
    }
    if (!val[now])
      vl[now] = ++pacnt;
    ++val[now];
    pa[vl[now]].push_back(id);
  }
  void DFS(int x, string dbg) {
    bool vis[26], add[26];
    for (int i = 0; i < kAlpha; ++i) {
      if (ch[x][i])
        vis[i] = true;
    }
    if (val[x]) {
      ans += val[x];
      for (vector<int>::iterator j = pa[vl[x]].begin(); j != pa[vl[x]].end(); ++j)
        ANS.push_back(*j);
      return;
    }
    // // sgpcmtizkztbunrvxm
    // if (dbg == "sgpcmti") {

    //   fprintf(stderr, "%s\n", "Hello, World!");
    // }
    for (int i = 0; i < kAlpha; ++i) {
      if (ch[x][i]) {
        for (set<int>::iterator it = S[i].begin(); it != S[i].end(); ++it)
          vis[*it] = false;
      }
    }
    for (int i = 0; i < kAlpha; ++i) {
      if (ch[x][i] && vis[i]) {
        for (int j = 0; j < kAlpha; ++j)
          if (ch[x][j] && i != j && S[i].find(j) == S[i].end())
            S[i].insert(j), add[j] = true;
        char c = 'a' + i;
        DFS(ch[x][i], dbg + c);
        for (int j = 0; j < kAlpha; ++j)
          if (add[j]) {
            S[i].erase(j), add[j] = false;
          }
      }
    }
  }
} T;

char ss[kMaxN][kLen];
bool vis[kMaxN];

// 70 pts

int main() {
  freopen("string.in", "r", stdin);
  freopen("string.out", "w", stdout);
  scanf("%d", &n);
  for (int i = 1; i <= n; ++i) {
    scanf("%s", ss[i]);
    T.Insert(ss[i], i);
  }
  T.DFS(T.rt, "");
  printf("%d\n", ans);
  for (int j = 0; j < (int)ANS.size(); ++j)
    vis[ANS[j]] = true;
  for (int i = 1; i <= n; ++i)
    if (vis[i])
      puts(ss[i]);
  return 0;
}