#include <algorithm>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <vector>
using namespace std;

namespace IO {
static const int kMegaBytes = 1 << 20;
static char buf[kMegaBytes], *S = buf, *T = buf;
inline char NextChar() {
  if (S == T) {
    T = (S = buf) + fread(buf, sizeof(char), sizeof(buf), stdin);
    if (S == T)
      return EOF;
  }
  return *S++;
}
template <typename Int> inline void Read(Int &x) {
  x = 0;
  Int flag = 1;
  char ch;
  for (ch = NextChar(); isspace(ch); ch = NextChar())
    ;
  if (ch == '-')
    flag = -1, ch = NextChar();
  for (; isdigit(ch); ch = NextChar())
    x = x * 10 + ch - '0';
  x *= flag;
}
} // namespace IO
using IO::Read;

typedef long long ll;
typedef pair<int, int> pii;

const int kMaxN = 3005, kInf = 0x3f3f3f3f;
int n, a[kMaxN];
vector<int> v;

int main() {
  freopen("balance.in", "r", stdin);
  freopen("bf.out", "w", stdout);
  Read(n);
  for (int i = 1; i <= n; ++i) {
    Read(a[i]);
    v.push_back(a[i]);
  }
  sort(v.begin(), v.end());
  v.resize(unique(v.begin(), v.end()) - v.begin());
  int ans = kInf;
  for (int i = 0; i < (int)v.size(); ++i) {
    int c = v[i];
    for (int j = 0; j < (int)v.size(); ++j) {
      int rp = v[j];
      int tmp = 0;
      int last = (a[1] == c) ? rp : a[1], t;
      for (int k = 2; k <= n; ++k) {
        t = (a[k] == c) ? rp : a[k];
        tmp += abs(t - last);
        last = t;
      }
      // fprintf(stderr, "%d %d %d\n", c, rp, tmp);
      if (tmp < ans) {
        ans = tmp;
      }
    }
  }
  printf("%d\n", ans);
  return 0;
}