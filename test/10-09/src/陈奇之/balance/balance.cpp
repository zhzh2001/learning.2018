#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int inf = 0x3f3f3f3f;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define gc getchar
#define pc putchar
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
#define pp(x,y) cout << "pp: " << x << " " << y << endl;
inline int read(){
	register int x=0,f=1;register char c=gc();
	for(;!isdigit(c);c=gc())if(c=='-')f=-1;
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}
#define rd read
void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);puts("");}
const int maxn = 1e5+233;
int n;
vector<int> V[maxn];
int a[maxn],pos[maxn];
ll ans,answ,res;
bool mark[maxn];
int main(){
	freopen("balance.in","r",stdin);
	freopen("balance.out","w",stdout);
	n = rd();
	mem(mark,false);
	Rep(i,1,n){
		a[i] = rd();
		V[a[i]] . push_back(i);
	}
	ans = 0,answ = 0x3f3f3f3f3f3f3f3fll;
	Rep(i,2,n) ans += abs(a[i] - a[i-1]);
	Rep(i,1,n){
		if(!mark[a[i]]){
			res = ans;
			pos[0] = 0;
			for(unsigned j=0;j<V[a[i]].size();++j){
				if(V[a[i]][j] != 1 && a[i]!=a[V[a[i]][j]-1]) res -= abs(a[i] - a[V[a[i]][j]-1]),pos[++pos[0]] = a[V[a[i]][j]-1];
				if(V[a[i]][j] != n && a[i]!=a[V[a[i]][j]+1]) res -= abs(a[i] - a[V[a[i]][j]+1]),pos[++pos[0]] = a[V[a[i]][j]+1];
			}
			nth_element(pos+1,pos+(pos[0]+1)/2,pos+pos[0]+1);
			Rep(j,1,pos[0]) res += abs(pos[(pos[0]+1)/2] - pos[j]);
			mark[a[i]] = true;
			answ = min(answ,res);
		}
	}writeln(answ);
	return 0;
}
