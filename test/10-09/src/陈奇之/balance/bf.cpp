#include<bits/stdc++.h>
#include<algorithm>
using namespace std;
typedef long long ll;
const int inf = 0x3f3f3f3f;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define gc getchar
#define pc putchar
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
#define pp(x,y) cout << "pp: " << x << " " << y << endl;
inline int read(){
	register int x=0,f=1;register char c=gc();
	for(;!isdigit(c);c=gc())if(c=='-')f=-1;
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}
#define rd read
void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);puts("");}
const int maxn = 2333;
ll n,res,a[maxn],b[maxn];
int main(){
	freopen("balance.in","r",stdin);
	freopen("bf.out","w",stdout);
	n = read();
	res=0;
	Rep(i,1,n) a[i] = rd();
	Rep(i,2,n)res+=abs(a[i]-a[i-1]);
	Rep(c,1,300){
		rep(d,1,300){
			ll ans = 0;
			Rep(i,1,n) b[i] = (a[i]==c) ? d : a[i];
			Rep(i,2,n) ans+=abs(b[i]-b[i-1]);
			res=min(res,ans);
		}
	}writeln(res);
	return 0;
}
