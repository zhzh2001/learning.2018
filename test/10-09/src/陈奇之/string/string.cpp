#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
const int inf = 0x3f3f3f3f;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define gc getchar
#define pc putchar
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
#define pp(x,y) cout << "pp: " << x << " " << y << endl;
inline int read() {
	register int x=0,f=1;
	register char c=gc();
	for(; !isdigit(c); c=gc())if(c=='-')f=-1;
	for(; isdigit(c); c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}
#define rd read
void write(ll x) {
	if(x<0)x=-x,pc('-');
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x);
	puts("");
}
#define hash1 Hash1
const int maxn = 50050;
const int base1 = 29;const int mod1 = 1e9+7;
const int base2 = 31;const int mod2 = 998244353;
map<pair<int,int> ,int> table;
int hash1[maxn][22];
int hash2[maxn][22];
int front,rear;
char s[maxn][22];
int len[maxn],n,f[maxn][22],deg[233],q[233];
bool mark[maxn];
int answ;
struct Edge {
	int to,nxt;
} edge[26*21+233];
int first[26+5],nume;
void Addedge(int a,int b) {
	edge[nume] . to = b;
	edge[nume] . nxt = first[a];
	first[a] = nume++;
}
int pos[maxn],rk[maxn],cnt[27];
bool cmp(int x,int y){
	Rep(i,1,20){
		if(s[x][i] < s[y][i]) return true;
		if(s[x][i] > s[y][i]) return false;
	}
	return false;
}
int main() {
	freopen("string.in","r",stdin);
	freopen("string.out","w",stdout);
	n = rd();
	Rep(i,1,n) {
		scanf("%s",s[i]+1);
		len[i] = strlen(s[i]+1);
		hash1[i][0] = hash2[i][0] = 0;
		Rep(j,1,20) {
			hash1[i][j] = (1ll * hash1[i][j-1] * base1 + s[i][j] - 'a') % mod1;
			hash2[i][j] = (1ll * hash2[i][j-1] * base2 + s[i][j] - 'a') % mod2;
		}//puts("");
	}
	Rep(i,1,n) pos[i] = i;
	sort(pos+1,pos+1+n,cmp);
//	Rep(i,1,n){
//		debug(pos[i]);
//	}
	Rep(j,1,20){
		for(int i=1,k;i<=n;i=k){
			int e = 0;
			for(k=i;k<=n && hash1[pos[i]][j-1]==hash1[pos[k]][j-1] && hash2[pos[i]][j-1]==hash2[pos[k]][j-1];k++)
				e |= (1 << (s[pos[k]][j] - 'a'));
			rep(t,i,k) f[pos[t]][j] = e;
		}
	}
	Rep(i,1,n){
		rep(k,0,26)first[k]=-1,deg[k]=0;
		nume=0;
		Rep(j,1,len[i]) {
			rep(k,0,26)
			if((f[i][j]>>k&1) && (k!=s[i][j]-'a')){
				Addedge(s[i][j]-'a',k);
				++deg[k];
			}
		}//puts("");
		front=rear=0;
		rep(k,0,26)if(!deg[k])q[rear++]=k;
		while(front<rear) {
			int u=q[front++];
			for(int e=first[u]; ~e; e=edge[e].nxt) {
				int v=edge[e].to;
				if(--deg[v]==0) q[rear++]=v;
			}
		}
		//debug(rear);
		if(rear==26) mark[i]=true;
	}

	Rep(j,1,20){
		for(int i=1,k;i<=n;i=k){
			int e = 0;
			for(k=i;k<=n && hash1[pos[i]][j]==hash1[pos[k]][j] && hash2[pos[i]][j]==hash2[pos[k]][j];k++)
				e |= len[pos[k]] == j;
			if(e) rep(t,i,k) if(len[pos[t]] != j) mark[pos[t]] = false;
		}
	}
	Rep(i,1,n) answ+=mark[i];
	writeln(answ);
	Rep(i,1,n) {
		if(mark[i]) printf("%s\n",s[i]+1);
	}
	return 0;
}
/*
5
bca
ddadbc
cbbd
acabddd
aabcadd
*/
