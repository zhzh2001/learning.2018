#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int inf = 0x3f3f3f3f;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define gc getchar
#define pc putchar
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
#define pp(x,y) cout << "pp: " << x << " " << y << endl;
inline int read(){
	register int x=0,f=1;register char c=gc();
	for(;!isdigit(c);c=gc())if(c=='-')f=-1;
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}
#define rd read
void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);puts("");}
inline int ran(){
	return rand() << 15 | rand();
}
int n,m;
int main(){
	freopen("hole.in","w",stdout);
	srand(time(NULL));
	n = ran() % 300 + 1,m = ran() % 300 + 1;
	printf("%d %d\n",n,m);
	Rep(i,1,n){
		printf("%d ",2*rand() - (32768));
	}puts("");
	Rep(i,1,m){
		printf("%d %d\n",2*rand() - (32768),rand()%20+1);
	}
}
