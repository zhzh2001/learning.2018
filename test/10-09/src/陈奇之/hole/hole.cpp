#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int inf = 0x3f3f3f3f;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define gc getchar
#define pc putchar
#define fi first
#define se second
#define debug(x) cout << #x" = " << x << endl;
#define pp(x,y) cout << "pp: " << x << " " << y << endl;
inline int read(){
	register int x=0,f=1;register char c=gc();
	for(;!isdigit(c);c=gc())if(c=='-')f=-1;
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}
#define rd read
void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);puts("");}
const int maxn = 5005;
pair<int,int> b[maxn];
int a[maxn],n,m;
ll f[2][maxn],sum[maxn];
int front,rear,q[maxn];
int main(){
	freopen("hole.in","r",stdin);
	freopen("hole.out","w",stdout);
	n = rd(),m = rd();
	int flag = 0;
	Rep(i,1,n) a[i] = rd(),flag++;
	sort(a+1,a+1+n);
	Rep(i,1,m) b[i].fi = rd(),b[i].se = rd(),flag-=b[i].se;
	if(flag>0){puts("Stupid game!");return 0;}
	sort(b+1,b+1+m);
	mem(f[0&1],0x3f);
	f[0][0] = 0;
	Rep(i,1,m){
		mem(f[i&1],0x3f);
		sum[0] = 0;Rep(j,1,n) sum[j] = sum[j-1] + abs(a[j] - b[i].fi);
		front = rear = 0;
		q[rear++] = 0;f[(i-1)&1][0] - sum[0];
		Rep(j,1,n){
			while(front < rear && (f[(i-1)&1][q[rear-1]] - sum[q[rear-1]] > f[(i-1)&1][j] - sum[j])) rear--;
			q[rear++]=j;
			while(front < rear && (q[front] < j-b[i].se)) front++;
			f[i&1][j] = f[(i-1)&1][q[front]] - sum[q[front]] + sum[j];
		}
	}writeln(f[m&1][n]);
	return 0;
}
/*前i个洞，放了j个球，最小代价是多少
f[i][j] = f[i-1][k] + 这一段的代价。


  2   				6	8	9
(2,1) (3,6)  (3,6) (4,7)(4,7)
容易发现决策单调性。

4 5
6 2 8 9
3 6
2 1
3 6
4 7
4 7
j < k
f[j] + calc(j,i) < f[k] + calc(k,i)
calc(j,i) = calc(j,k-1) + calc(k,i)


*/
/*
f[j] + calc(j,k-1) < f[k]

要让
*/
