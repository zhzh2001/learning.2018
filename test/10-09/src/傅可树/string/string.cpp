#pragma GCC optimize ("O2")
#include<cstdio>
#include<cstring>
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=50005,L=25;
int n,nod;
struct node{
	int tran[26];
	bool end;
}a[26*N];
char s[N][L];
bool flag[N];
inline void insert(char S[]){
	int len=strlen(S+1),now=0;
	for (int i=1;i<=len;i++){
		int c=S[i]-'a';
		if (!a[now].tran[c]){
			a[now].tran[c]=++nod;
		}
		now=a[now].tran[c];
	}
	a[now].end=1;
}
inline void init(){
	n=read();
	for (int i=1;i<=n;i++){
		scanf("%s",s[i]+1); 
		insert(s[i]);
	}
}
struct edge{
	int link,next;
}e[26*17];
int cnt,tot,head[26],du[26],q[30],h,t;
inline void add_edge(int u,int v){
	e[++tot]=(edge){v,head[u]}; head[u]=tot; du[v]++;
}
inline bool topsort(){
	h=t=0;
	for (int i=0;i<26;i++){
		if (!du[i]) {
			q[++t]=i;
		}
	}
	while (h!=t){
		int u=q[++h];
		for (int i=head[u];i;i=e[i].next){
			int v=e[i].link; du[v]--;
			if (!du[v]) q[++t]=v;
		}
	}
	return t==26;
}
inline bool travel(char S[]){
	memset(head,0,sizeof head); memset(du,0,sizeof du);
	int len=strlen(S+1),now=0; tot=0;
	for (int i=1;i<=len;i++){
		if (i<len&&a[now].end) return 0;
		int c=S[i]-'a';
		for (int j=0;j<26;j++){
			if (c!=j&&a[now].tran[j]){
				add_edge(c,j);
			}
		}
		now=a[now].tran[c];
	}
	return topsort();
}
inline void solve(){
	for (int i=1;i<=n;i++){
		flag[i]=travel(s[i]); cnt+=flag[i];
	}
	writeln(cnt);
	for (int i=1;i<=n;i++){
		if (flag[i]) printf("%s\n",s[i]+1);
	}
}
int main(){
	freopen("string.in","r",stdin); freopen("string.out","w",stdout);
	init(); solve();
	return 0;
}
