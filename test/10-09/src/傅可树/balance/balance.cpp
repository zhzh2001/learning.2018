#include<cstdio>
#include<algorithm>
using namespace std;
const int INF=1e9,N=1e5+5;
int n,a[N];
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
inline void init(){
	n=read();
	for (int i=1;i<=n;i++) a[i]=read();
}
inline int abs(int x){
	return (x<0)?-x:x;
}
bool flag[N];
int nxt[N],S,ans,las[N],b[N<<1],tot,sum; 
inline void solve(){
	for (int i=1;i<=n;i++){
		nxt[las[a[i]]]=i; las[a[i]]=i;
		if (i!=1) S+=abs(a[i]-a[i-1]);
	} 
	ans=S;
	for (int i=1;i<=n;i++){
		if (!flag[i]){
			int now=i; sum=tot=0;
			while (now){
				if (now>1&&a[now-1]!=a[now]) {
					b[++tot]=a[now-1];
					sum+=abs(a[now-1]-a[now]);
				}
				if (now<n&&a[now+1]!=a[now]){
					b[++tot]=a[now+1];
					sum+=abs(a[now+1]-a[now]);
				}
				flag[now]=1;
				now=nxt[now];
			}
			int mid=(1+tot)>>1;
			nth_element(b+1,b+mid,b+tot+1);
			int tmp=b[mid],res=0;
			for (int j=1;j<=tot;j++){
				res+=abs(tmp-b[j]);
			}
			ans=min(ans,S-sum+res);
		}
	}
	writeln(ans);
}
int main(){
	freopen("balance.in","r",stdin); freopen("balance.out","w",stdout);
	init(); solve();
	return 0;
}
