#include<cstdio>
#include<algorithm>
#include<cstring>
#define int long long
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=305,INF=1e18;
struct node{
	int b,c;
}hole[N];
int n,m,ball[N],S;
inline bool cmp(node A,node B){
	return A.b<B.b;
}
inline void init(){
	n=read(); m=read();
	for (int i=1;i<=n;i++){
		ball[i]=read();
	}
	sort(ball+1,ball+1+n);
	for (int i=1;i<=m;i++){
		hole[i]=(node){read(),read()};
		S+=hole[i].c;
	}
	sort(hole+1,hole+1+m,cmp);
}
inline void upd(int &x,int w){
	x=min(x,w);
}
inline int Abs(int x){
	return (x<0)?-x:x;
}
int ans,dp[N][N];
inline void solve(){
	if (S<n) {
		puts("Stupid game!");
	}
	for (int i=0;i<=n;i++){
		for (int j=0;j<=n;j++){
			dp[i][j]=INF;
		}
	}
	dp[0][0]=0; ans=INF;
	for (int i=0;i<m;i++){
		for (int j=0;j<=n;j++){
			int sum=0;
			for (int k=0;k<=hole[i+1].c&&j+k<=n;k++){
				int tmp=ball[j+k];
				if (k){
					sum+=Abs(tmp-hole[i+1].b);
				}
				upd(dp[i+1][j+k],dp[i][j]+sum);
			}
		}
	}
	for (int i=0;i<=m;i++) upd(ans,dp[i][n]);
	writeln(ans);
}
signed main(){
	freopen("hole.in","r",stdin); freopen("hole.out","w",stdout);
	init(); solve();
	return 0;
}
