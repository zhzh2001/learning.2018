#include<cstdio>
#include<algorithm>
#include<cstring>
#define int long long
using namespace std;
inline int read(){int x=0,f=1,ch=getchar(); while(ch<'0'||ch>'9'){if(ch=='-') f=-1; ch=getchar();} while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();} return x*f;}
inline void write(int x){if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');}
inline void writeln(int x){write(x); puts("");}
const int N=5005,INF=1e18;
struct node{
	int b,c;
}hole[N];
int n,m,ball[N],S;
inline bool cmp(node A,node B){
	return A.b<B.b;
}
inline void init(){
	n=read(); m=read();
	for (int i=1;i<=n;i++){
		ball[i]=read();
	}
	sort(ball+1,ball+1+n);
	for (int i=1;i<=m;i++){
		hole[i]=(node){read(),read()};
		S+=hole[i].c;
	}
	sort(hole+1,hole+1+m,cmp);
}
int sum[N],pos[N];
inline int find(int x){
	int l=1,r=n+1;
	while (l<r){
		int mid=(l+r)>>1;
		if (hole[mid].b>=x) r=mid;
			else l=mid+1; 
	}
	return l;
}
struct Node {
	int id,v;
}q[N];
int h,t,cur,dp[2][N];
inline void solve(){
	if (S<n) {
		puts("Stupid game!");
	}
	ball[n+1]=INF;
	for (int i=1;i<=n+1;i++){
		sum[i]=sum[i-1]+ball[i];
	}
	for (int i=1;i<=m;i++){
		pos[i]=find(hole[i].b);
	}
	S=0;
	for (int j=1;j<=n;j++) dp[cur][j]=INF;
	for (int i=1;i<=m;i++){
		S+=hole[i].c;
		for (int j=0;j<=n;j++) dp[cur^1][j]=INF;
		h=1; t=0; memset(q,0,sizeof q);
		for (int j=0;j<=S;j++){
			while (h<=t&&q[h].id<j-hole[i].c) h++;
			int tmp=dp[cur][j]-hole[i].b*j+sum[j];
			while (h<=t&&tmp<=q[t].v) t--;
			q[++t]=(Node){j,tmp}; 
			dp[cur^1][j]=(sum[j]-sum[pos[i]-1])+hole[i].b*(2*pos[i]-j-2)-sum[pos[i]-1]+q[h].v;
		}
		cur^=1;
	}
	writeln(dp[cur][n]);
}
signed main(){
	init(); solve();
	return 0;
}
