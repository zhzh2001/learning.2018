#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 5e4+10;
int n,sum,len[N];
char c[N][21];
bool ans[N],flag[26][26];
namespace Subtask1{
	const int N = 2010;
	bool flag[26][26];
	int top,Top,q[N],Q[N];
	int e[26][26],d[26],rd[26],h,t,s[100];
	inline bool check(){
		For(i,0,25) rd[i]=0;
		For(i,0,25){
			d[i]=0;
			For(j,0,25) if (flag[i][j]) e[i][++d[i]]=j,rd[j]++;
		}
		h=t=0;
		For(i,0,25) if (!rd[i]) s[++t]=i;
		while (h<t){
			int u=s[++h];
			For(i,1,d[u]) if (--rd[e[u][i]]==0) s[++t]=e[u][i];
		}
		return t==26;
	}
	inline void Main(){
		For(i,1,n){
			memset(flag,0,sizeof flag);
			top=0;bool f=1;
			For(j,1,n) 
				if (i!=j)
					if (c[j][1]==c[i][1]){
						q[++top]=j;
						if (len[j]==1){f=0;break;}
					}
					else flag[c[i][1]-'a'][c[j][1]-'a']=1;
			if (!f) continue;
			For(j,2,len[i]){
				For(k,1,top) if (flag[c[q[k]][j]-'a'][c[i][j]-'a']){f=0;break;}
				if (!f) break;
				Top=0;
				For(k,1,top) 
					if (c[q[k]][j]==c[i][j]){
						Q[++Top]=q[k];
						if (len[q[k]]==j){f=0;break;}
					}
					else flag[c[i][j]-'a'][c[q[k]][j]-'a']=1;
				top=Top;
				For(k,1,top) q[k]=Q[k];
				if (!f) break;
			}
			f&=check(),ans[i]=f,sum+=f;
		}
		printf("%d\n",sum);
		For(i,1,n) if (ans[i]) printf("%s\n",c[i]+1);
	}
};
int tot,son[1000010][26];
bool f[1000010];
inline void insert(char *s){
	int len=strlen(s+1),u=0;
    For(i,1,len)
        if (!son[u][s[i]-'a']) son[u][s[i]-'a']=++tot,u=tot;
            else u=son[u][s[i]-'a'];
    f[u]=1;
} 
int e[26][26],d[26],rd[26],h,t,q[100];
inline bool check(){
	For(i,0,25) rd[i]=d[i]=0;
	For(i,0,25) For(j,0,25) if (flag[i][j]) e[i][++d[i]]=j,rd[j]++;
	h=t=0;
	For(i,0,25) if (!rd[i]) q[++t]=i;
	while (h<t){
		int u=q[++h];
		For(i,1,d[u]) if (--rd[e[u][i]]==0) q[++t]=e[u][i];
	}
	return t==26;
}
int main(){
	freopen("string.in","r",stdin);
	freopen("string.out","w",stdout);
	n=read();
	For(i,1,n) scanf("%s",c[i]+1),len[i]=strlen(c[i]+1);
	//if (n<=2000) return Subtask1::Main(),0;
	For(i,1,n) insert(c[i]);
	For(i,1,n){
		memset(flag,0,sizeof flag);int u=0,F=1;
		For(j,1,len[i]){
			For(k,0,25) 
				if (c[i][j]-'a'!=k&&son[u][k])
					if (flag[k][c[i][j]-'a']){F=0;break;}
						else flag[c[i][j]-'a'][k]=1;
			//printf("%d ",j);
			if (!F) break;
			if (f[u=son[u][c[i][j]-'a']]&&j!=len[i]){F=0;break;}
		}//puts("");
		F&=check(),ans[i]=F,sum+=F;
	}
	printf("%d\n",sum);
	For(i,1,n) if (ans[i]) printf("%s\n",c[i]+1);
}
