#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 1e5+10;
int n,m,x,Max;
ll ans,sum,a[N],sum1[N];
vector<int>q[N],sum2[N];
int main(){
	freopen("balance.in","r",stdin);
	freopen("balance.out","w",stdout);
	m=read();
	For(i,1,m){
		x=read();
		if (x!=a[n]||!n) a[++n]=x;
		Max=max(Max,x);	
	}
	For(i,2,n) sum+=abs(a[i]-a[i-1]);
	For(i,1,n){
		if (i>1) q[a[i]].push_back(a[i-1]),sum1[a[i]]+=abs(a[i]-a[i-1]);
		if (i<n) q[a[i]].push_back(a[i+1]),sum1[a[i]]+=abs(a[i]-a[i+1]);
	}
	For(i,0,Max) sort(q[i].begin(),q[i].end());
	For(i,0,Max) 
		if (!q[i].empty()){
			sum2[i].push_back(q[i][0]);
			For(j,1,q[i].size()-1) sum2[i].push_back(sum2[i][sum2[i].size()-1]+q[i][j]);
		}
	ans=sum;
	For(i,0,Max){
		if (sum2[i].empty()) continue;
		ll Size=sum2[i].size(),Mid=q[i].size()>>1,mid=q[i][Mid];
		ll x=sum-sum1[i];
		x+=mid*(Mid+1)-sum2[i][Mid]+(sum2[i][Size-1]-sum2[i][Mid])-(Size-Mid-1)*mid;
		ans=min(ans,x);
	}
	printf("%lld",ans);
}
