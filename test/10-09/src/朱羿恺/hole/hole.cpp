#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 5e3+10;
int n,m,a[N],b[N],c[N];
namespace Mcmf{
	const int N = 1e4+10, M = 1e6+10;
	ll MaxFlow,MinCost;
	int tot=-1,first[N],to[M<<1],last[M<<1];
	ll flow[M<<1],cost[M<<1];
	inline void Add(int x,int y,int w,int f){to[++tot]=y,flow[tot]=w,cost[tot]=f,last[tot]=first[x],first[x]=tot;}
	int head,tail,q[1000000];
	ll dis[N];
	bool vis[N];
	inline bool spfa(int s,int t){
	    For(i,1,n) vis[i]=0,dis[i]=1e15;
	    head=0,dis[q[tail=1]=s]=0;
	    while (head<tail){
	        int u=q[++head];vis[u]=0;
	        for (int i=first[u];i!=-1;i=last[i]){
	            int v=to[i];
	            if (dis[u]+cost[i]<dis[v]&&flow[i]){
	                dis[v]=dis[u]+cost[i];
	                if (!vis[v]) vis[q[++tail]=v]=1;
	            }
	        }
	    }
	    return dis[t]!=1e15;
	}
	inline int dfs(int u,int t,ll dist){
	    if (u==t){vis[t]=1,MinCost+=dist*dis[t];return dist;}
	    ll Flow=0,f;vis[u]=1;
	    for (int i=first[u];i!=-1;i=last[i]){
	        if (!vis[to[i]]&&flow[i]&&dis[to[i]]==dis[u]+cost[i]&&(f=dfs(to[i],t,min(dist,flow[i])))){
	            dist-=f,Flow+=f,flow[i]-=f,flow[i^1]+=f;
	            if (!dist) break;
	        }
	    }
	    return Flow;
	}
	inline void MCMF(int s,int t){
	    while (spfa(s,t))
	        for(vis[t]=1;vis[t];) memset(vis,0,sizeof vis),MaxFlow+=dfs(s,t,1e9);
	}
	inline void Main(){
		memset(first,-1,sizeof first);
		For(i,1,n) Add(1,i+1,1,0),Add(i+1,1,0,0);
		For(i,1,n) For(j,1,m) Add(i+1,j+n+1,1,abs(a[i]-b[j])),Add(j+n+1,i+1,0,-abs(a[i]-b[j]));
		For(j,1,m) Add(j+n+1,n+m+2,c[j],0),Add(n+m+2,j+n+1,0,0);
		n=n+m+2,MCMF(1,n);
		printf("%lld",MinCost);
	}
};
int main(){
	freopen("hole.in","r",stdin);
	freopen("hole.out","w",stdout);
	n=read(),m=read();int sumc=0;
	For(i,1,n) a[i]=read();
	For(i,1,m) b[i]=read(),c[i]=read(),sumc+=c[i];
	if (sumc<n) return puts("Stupid game!"),0;
	Mcmf::Main();
}
