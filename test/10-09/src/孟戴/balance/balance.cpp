//#include<iostream>
#include<algorithm>
#include<stdio.h>
#include<string.h>
#include<math.h>
#include<fstream>
#include<vector>
using namespace std;
ifstream fin("balance.in");
ofstream fout("balance.out");
vector<long long>c[100003];
bool vis[100003];
long long n,ans,ret,a[100003],sz[100003],sum[100003];
int main(){
	fin>>n;
	for(int i=1;i<=n;i++){
		fin>>a[i];
	}
	for(int i=n;i>=2;i--){
		ans+=abs(a[i]-a[i-1]);
		if(a[i]!=a[i-1]){
			sz[a[i]]++;
			c[a[i]].push_back(a[i-1]);
			sum[a[i]]+=a[i-1];
		}
	}
	for(int i=1;i<n;i++){
		if(a[i]!=a[i+1]){
			sz[a[i]]++;
			c[a[i]].push_back(a[i+1]);
			sum[a[i]]+=a[i+1];
		}
	}
	ret=ans;
	for(int i=1;i<=n;i++){
		if(vis[a[i]]){
			continue;
		}
		vis[a[i]]=true;
		long long g1=0,g2=0,g3=0,g4=0,g5=0,p1,p2,p3,p4;
		sort(c[a[i]].begin(),c[a[i]].end());
		p1=c[a[i]][sz[a[i]]/2];
		if(sz[a[i]]>1){
			p2=c[a[i]][sz[a[i]]/2-1];
		}else{
			p2=a[i];
		}
		p3=sum[a[i]]/sz[a[i]];
		p4=p3+1;
		for(int j=0;j<=sz[a[i]]-1;j++){
			g1+=abs(c[a[i]][j]-a[i]);
			g2+=abs(c[a[i]][j]-p1);
			g3+=abs(c[a[i]][j]-p2);
			g4+=abs(c[a[i]][j]-p3);
			g5+=abs(c[a[i]][j]-p4);
		}
		g2=min(g2,min(g3,min(g4,g5)));
		ret=min(ans-(g1-g2),ret);
	}
	fout<<ret<<endl;
	return 0;
}
