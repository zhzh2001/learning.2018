//#include<iostream>
#include<algorithm>
#include<stdio.h>
#include<string.h>
#include<math.h>
#include<fstream>
using namespace std;
ifstream fin("string.in");
ofstream fout("string.out");
struct trie{
	int son[26],t;
}a[1000000];
int n,ans,cnt=1,len[50003],mp[27][27],q[27];
string s[50003];
bool yes,vis[27],anss[50003];
void dfs(int k){
	if(!yes){
		return;
	}
	vis[k]=true;
	for(int i=0;i<=25;i++){
		if(mp[k][i]){
			if(vis[i]){
				yes=false;
			}else{
				dfs(i);
			}
		}
	}
	vis[k]=false;
}
void build(int rt,int x,int l){
	if(l==len[x]){
		return;
	}
	l++;
	int v=s[x][l-1]-'a';
	for(int i=0;i<=25;i++){
		if(i!=v&&a[rt].son[i]){
			mp[v][i]=1;
		}
	}
	if(a[rt].t&&l<=len[x]){
		yes=false;
	}
	if(l<=len[x]){
		build(a[rt].son[v],x,l);
	}
}
void insert(int rt,int x,int l){
	if(l==len[x]){
		a[rt].t++;
		return;
	}
	l++;
	int v=s[x][l-1]-'a';
	if(!a[rt].son[v]){
		a[rt].son[v]=++cnt;
	}
	if(l<=len[x]){
		insert(a[rt].son[v],x,l);
	}
}
int main(){
	fin>>n;
	for(int i=1;i<=n;i++){
		fin>>s[i];
		len[i]=s[i].length();
	}
	for(int i=1;i<=n;i++){
		insert(1,i,0);
	}
	for(int i=1;i<=n;i++){
		memset(mp,0,sizeof(mp));
		memset(vis,0,sizeof(vis));
		yes=true;
		build(1,i,0);
		for(int j=0;j<=25;j++){
			if(!vis[j]){
				dfs(j);
			}
		}
		if(yes){
			ans++;
			anss[i]=true;
		}
	}
	fout<<ans<<endl;
	for(int i=1;i<=n;i++){
		if(anss[i]){
			fout<<s[i]<<endl;
		}
	}
	return 0;
}
