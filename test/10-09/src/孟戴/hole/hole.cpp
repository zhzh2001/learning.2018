//#include<iostream>
#include<stdio.h>
#include<algorithm>
#include<math.h>
#include<string.h>
#include<fstream>
using namespace std;
ifstream fin("hole.in");
ofstream fout("hole.out");
long long n,m;
long long a[5003],ss[5003];
long long sum[5003],f[5003][5003];
struct hole{
	long long b,c;
}h[5003];
inline bool cmp(hole a,hole b){
	return a.b<b.b;
}
int main(){
	fin>>n>>m;
	for(int i=1;i<=n;i++){
		fin>>a[i];
	}
	sort(a+1,a+n+1);
	for(int i=1;i<=m;i++){
		fin>>h[i].b>>h[i].c;
	}
	sort(h+1,h+m+1,cmp);
	for(int i=1;i<=m;i++){
		ss[i]=ss[i-1]+h[i].c;
		for(int j=1;j<=n;j++){
			f[i][j]=1e17;
		}
		f[i][0]=1e17;
	}
	if(ss[m]<n){
		fout<<"Stupid game!"<<endl;
		return 0;
	}
	if(n<=300){
		f[0][0]=0;
		for(int i=1;i<=m;i++){
			for(int j=1;j<=n;j++){
				sum[j]=sum[j-1]+abs(a[j]-h[i].b);
			}
			for(int j=0;j<=n;j++){
				f[i-1][j]-=sum[j];
			}
			for(int j=0;j<=min(ss[i-1],n);j++){
				for(int k=j;k<=min(j+h[i].c,n);k++){
					f[i][k]=min(f[i-1][j]+sum[k],f[i][k]);
				}
			}
		}
		fout<<f[m][n]<<endl;
		return 0;
	}
	f[0][0]=0;
	int q[5003],head,tail,now;
	for(long long i=1;i<=m;i++){
		for(int j=1;j<=n;j++){
			sum[j]=sum[j-1]+abs(a[j]-h[i].b);
		}
		for(long long j=0;j<=n;j++){
			f[i-1][j]-=sum[j];
		}
		for(long long k=0;k<=min(ss[i],n);k++){
			if(k==0){
				head=1,tail=1,q[1]=0,now=0;
			}
			while(q[head]<max(0ll,k-h[i].c)){
				head++;
			}
			while(now<min(ss[i-1],k)){
				while(head<=tail&&f[i-1][q[tail]]>=f[i-1][now+1]){
					tail--;
				}
				q[++tail]=(++now);
			}
			f[i][k]=min(f[i][k],f[i-1][q[head]]+sum[k]);
		}
	}
	fout<<f[m][n]<<endl;
	return 0;
}
/*
7 2
10 20 30 40 50 45 35
-1000000000 10
1000000000 1
in:
4 5
2 6 8 9
2 1
3 6
3 6
4 7
4 7

out:
11

*/
