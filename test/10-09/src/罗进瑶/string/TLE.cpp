#include<bits/stdc++.h>
#define mk make_pair
#define fi first
#define nd second
using namespace std;
typedef long long ll;
void setIO() {
	freopen("string.in", "r", stdin);
	freopen("string.out", "w", stdout);
}
const int N = 51000;
int n;
string s[N];
bool ans[N];
bool vis[30];
int pos[30], v[30];
void solve() {
	for(int i = 0; i < 10; ++i)
		pos[v[i]] = i;
	for(int i = 1; i <= n; ++i) if(!ans[i]){
		bool fg = 1;
		for(int j = 1; j <= n; ++j)
			if(i != j)
				for(int k = 0; k < min(s[i].length(), s[j].length()); ++k)
					if(pos[s[i][k] - 'a'] > pos[s[j][k] - 'a']) {
						fg = 0;
						break;
					}
		ans[i] = fg;
	}
}
int main() {
	ios::sync_with_stdio(false);
	cin.tie(0); cout.tie(0);
	cin>>n;
	for(int i = 1; i <= n; ++i)
		cin>>s[i];
	int res = 0;
	for(int i = 0; i < 10; ++i)
		v[i] = i;
	do{
		solve();
	}while(next_permutation(v, v + 10));
	for(int i = 1; i <= n; ++i)
		res += ans[i];
	for(int i = 1; i <= n; ++i)
		if(ans[i]) cout<<s[i]<<endl;
	return 0;
}
