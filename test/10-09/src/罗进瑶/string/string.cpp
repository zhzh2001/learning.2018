#include<bits/stdc++.h>
#define mk make_pair
#define fi first
#define nd second
using namespace std;
typedef long long ll;
void setIO() {
	freopen("string.in", "r", stdin);
	freopen("string.out", "w", stdout);
}
const int N = 51000;
int n;
string s[N];
bool ans[N];
bool g[27][27];
int ind[27];
bool solve(int x) {
	memset(g, 0, sizeof g);
	memset(ind, 0, sizeof ind);
	for(int i = 1; i <= n; ++i)
		if(i != x) {
			bool fg = 0;
			if(s[i].length() < s[x].length()) {
				for(int k = 0; k < s[i].length(); ++k)
					if(s[x][k] != s[i][k]) {
						fg = 1;
						break;
					}
				if(!fg) return 0;
			}
			for(int k = 0; k < min(s[i].length(), s[x].length()); ++k)
				if(s[i][k] != s[x][k]) {
					int u = s[x][k] - 'a', v = s[i][k] - 'a';
					if(g[u][v]) break;
					ind[v] += 1;
					g[u][v] = 1;
					break;
				}			
		}
	queue<int>q;
	for(int i = 0; i < 26; ++i) if(!ind[i]) q.push(i);
	while(!q.empty()) {
		int x = q.front(); q.pop();
		for(int i = 0; i < 26; ++i)
			if(g[x][i] && !(--ind[i]))
				q.push(i);
	}
	for(int i = 0; i < 26; ++i) if(ind[i])
		return 0;
	return 1;
}
int main() {
	setIO();
	ios::sync_with_stdio(false);
	cin.tie(0); cout.tie(0);
	cin>>n;
	for(int i = 1; i <= n; ++i)
		cin>>s[i];
	int res = 0;
	for(int i = 1; i <= n; ++i) {
		ans[i] = solve(i);
		res += ans[i];
	}
	cout<<res<<endl;
	for(int i = 1; i <= n; ++i)
		if(ans[i]) cout<<s[i]<<endl;
	return 0;
}
