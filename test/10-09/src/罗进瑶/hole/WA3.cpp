#include<bits/stdc++.h>
#define mk make_pair
#define fi first
#define nd second
using namespace std;
typedef long long ll;
inline ll read() {ll x = 0; char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;
ch = getchar();}while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("hole.in", "r", stdin);
	freopen("hole.out", "w", stdout);
} 
const int N = 30000, M = 500*500;
int n, m;
ll a[N];
struct H {
	ll x, f;
}b[N];
bool cmp(H _x, H _y) {
	return _x.x < _y.x;
}
int find(int x) {
	int l = -1, r = m + 1;
	while(r - l > 1) {
		int mid = (l + r) >> 1;
		if(b[mid].x >= x) r = mid;
		else l = mid;
	}
	return r;
}
struct Edge {
	int u, v, nxt;
	ll f, c;
}e[M];
int head[N], en;
void add(int x, int y, ll f, ll c) {
	e[++en].u = x, e[en].v = y, e[en].nxt = head[x], e[en].f = f, e[en].c = c, head[x] = en;;
	e[++en].v = x, e[en].u = y, e[en].nxt = head[y], e[en].f = 0, e[en].c = -c, head[y] = en;
}
const ll inf = 0x3f3f3f3f3f3f3f3f; 

int pre[N], s, t;
ll d[N];
bool inq[N];
bool spfa() {
	memset(d, 0x3f, sizeof d);
	memset(inq, 0, sizeof inq);
	d[s] = 0;
	queue<int>q;
	q.push(s);
	while(!q.empty()) {
		int x = q.front(); q.pop();
		inq[x] = 0;
		for(int i = head[x]; i;i = e[i].nxt) if(e[i].f){
			int y = e[i].v;
			if(d[y] > d[x] + e[i].c) {
				d[y] = d[x] + e[i].c;
				if(!inq[y]) inq[y] = 1, q.push(y);
				pre[y] = i;
			}
		}
	}
	return d[t] < 0x3f3f3f3f;
}
ll ans;
void update() {
	int x = t;
	ll Min = 1e17;
	while(x != s) {
		Min = min(Min, e[pre[x]].f);
		x = e[pre[x]].u;
	}
	x = t;
	while(x != s) {
		e[pre[x]].f -= Min;
		e[pre[x] ^ 1].f += Min;
		x = e[pre[x]].u;
	}
	ans += d[t] * Min;
}
int main() {
	n = read(), m = read();
	for(int i = 1; i <= n; ++i)
		a[i] = read();
	for(int i = 1; i <= m; ++i)
		b[i].x = read(), b[i].f = read();
	en = 1;
	sort(a + 1, a + 1 + n);
	sort(b + 1, b + 1 + m, cmp);
	s = n + m + 1;
	t = n + m + 2;
	for(int i = 1; i <= n; ++i) {
		for(int j = 1; j <= m; ++j)
			add(i, n + j, inf, abs(b[j].x - a[i]));
		add(s, i, 1, 0);
	}
	for(int i = 1; i <= m; ++i) add(n + i, t, b[i].f, 0);
	while(spfa())
		update();
	writeln(ans);
	return 0;
}
