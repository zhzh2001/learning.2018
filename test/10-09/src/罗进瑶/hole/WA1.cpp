#include<bits/stdc++.h>
#define mk make_pair
#define fi first
#define nd second
using namespace std;
typedef long long ll;
inline ll read() {ll x = 0; char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;
ch = getchar();}while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("string.in", "r", stdin);
	freopen("string.out", "w", stdout);
}
const int N = 5100;
int n, m;
ll a[N];
struct H {
	ll x, f;
	ll c;
}b[N];
bool vis[N];
ll ans;
bool cmp(H _x, H _y) {
	return _x.x < _y.x;
}
int main() {
	n = read(), m = read();
	for(int i = 1; i <= n; ++i)
		a[i] = read();
	sort(a + 1, a + 1 + n);
	for(int i = 1; i <= m; ++i)
		b[i].x = read(), b[i].f = read();
	sort(b + 1, b + 1 + m, cmp);
	int q = 0;
	for(int i = 1; i <= n; ++i) {
		while(q <= m && b[q + 1].x <= a[i]) ++q;
		int p = q;
		while(p >= 1 && b[p].c == b[p].f) --p;
		if(p) {
			ans += a[i] - b[p].x;
			++b[p].c;
			vis[i] = 1;
		}
	}
	q = m + 1;
	for(int i = n; i >= 1; --i) if(!vis[i]){
		while(q >= 1 && b[q - 1].x >= a[i]) --q;
		int p = q;
		while(p <= m && b[p].c == b[p].f) ++p;
		if(p <= m) {
			ans += b[p].x - a[i];
			++b[p].c;
			vis[i] = 1;
		}
	}
	writeln(ans);
	return 0;
}
