#include<bits/stdc++.h>
#define mk make_pair
#define fi first
#define nd second
using namespace std;
typedef long long ll;
inline ll read() {ll x = 0; char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;
ch = getchar();}while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("hole.in", "r", stdin);
	freopen("hole.out", "w", stdout);
}
const int N = 5100;
int n, m;
ll a[N];
struct H {
	ll x, f;
	ll c;
}b[N];
bool vis[N];
ll ans = 1e17;
bool cmp(H _x, H _y) {
	return _x.x < _y.x;
}
int find(ll x) {
	int l = 0, r = m + 1;
	while(r - l > 1) {
		int mid = (l + r) >> 1;
		if(b[mid].x >= x) r = mid;
		else l = mid;
	}
	return r;
}
void dfs(int x, ll sum) {
	if(sum >= ans) return;
	if(x == n + 1) {
		ans = sum;
		return ;
	}
	int q = find(a[x]);
	int p = q;
	while(q <= m && b[q].c == b[q].f) ++q;
	while(p >= 1 && (b[p].c == b[p].f || b[p].x > a[x]))--p;
	if(q <= m) {
		++b[q].c;
		dfs(x + 1, sum + b[q].x - a[x]);
		--b[q].c;
	}	
	if(p >= 1) {
		++b[p].c;
		dfs(x + 1, sum + a[x] - b[p].x);
		--b[p].c;
	}
}
int main() {
	setIO();
	n = read(), m = read();
	for(int i = 1; i <= n; ++i)
		a[i] = read();
	sort(a + 1, a + 1 + n);
	ll sum = 0;
	for(int i = 1; i <= m; ++i)
		b[i].x = read(), b[i].f = read(), sum += b[i].f;
	if(sum < n) {
		puts("Stupid game!");
		return 0;
	}
	if(n > 10 ){
		puts("0");
		return 0;
	}
	sort(b + 1, b + 1 + m, cmp);
	dfs(1, 0);
	writeln(ans);
	return 0;
}
