#include<bits/stdc++.h>
#define mk make_pair
#define fi first
#define nd second
using namespace std;
typedef long long ll;
inline ll read() {ll x = 0; char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;
ch = getchar();}while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) putchar('-'), x = -x;if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
void setIO() {
	freopen("balance.in", "r", stdin);
	freopen("balance.out", "w", stdout);
}
const int N = 1e5 + 111;
int n;
int a[N], mx;
vector<int>b[N];
ll ans, c[N], sum;
int main() {
	setIO();
	n = read();
	for(int i = 1; i <= n; ++i) {
		a[i] = read();
		mx = max(mx, a[i]);
		if(i > 1) {
			ans += abs(a[i] - a[i - 1]);
			b[a[i]].push_back(a[i - 1]);
			c[a[i]] += abs(a[i - 1] - a[i]);
			b[a[i - 1]].push_back(a[i]);
			c[a[i - 1]] += abs(a[i - 1] - a[i]);
		}
	}
	sum = ans;
	for(int i = 0; i <= mx; ++i) if(b[i].size()){
		sort(b[i].begin(), b[i].end());
		ll res = 0, mid = b[i][b[i].size() / 2];
		for(int j = 0; j < b[i].size(); ++j)
			res += abs(mid - b[i][j]);
		ans = min(ans, sum - c[i] + res);
	}
	writeln(ans);
	return 0;
}
