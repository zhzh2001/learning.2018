#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define pb push_back


using namespace std;

inline ll read()
{
	ll t=0,dp=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	dp=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-48,c=getchar();
	return t*dp;
}
inline void write(ll x){if(x<0)	{putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+48);}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
/*------------------------------------------------------------------------------------------------------*/

ll dp[5001],n,m,x[5001],y[5001],tot,c[5001],q[5001],tdp[5001],sum[5001],tep[5001];
struct node{int y,c;}	p[5001];
inline bool cmp(node x,node y){return x.y<y.y;}
inline ll Get_sum(int l,int r)
{
	if(l>r)	return 0;
	return sum[r]-sum[l-1];
}
inline ll Get(int l,int r,int to)
{
	ll tmp=0;
	int t=tep[to];
	if(l>t)	tmp+=Get_sum(l,r)-(r-l+1)*y[to];
	else	
	if(r<=t)	tmp+=(r-l+1)*y[to]-Get_sum(l,r);
	else
	{
		if(l<=t)tmp+=max(0,t-l+1)*y[to]-Get_sum(l,t);
		if(t<r)tmp+=Get_sum(t+1,r)-max(0,r-t)*y[to];
	}
	ll ttmp=0;
	For(i,l,r)	ttmp+=abs(x[i]-y[to]);
	return ttmp;
}
int main()
{
	freopen("hole.in","r",stdin);
	freopen("hole.out","w",stdout);
	n=read();
	m=read();
	For(i,1,n)	x[i]=read();
	sort(x+1,x+n+1);
	For(i,1,m)	p[i].y=read(),p[i].c=read(),tot+=p[i].c;
	sort(p+1,p+m+1,cmp);
	For(i,1,m)	y[i]=p[i].y,c[i]=p[i].c;
	
	For(i,1,n)	sum[i]=sum[i-1]+x[i];
	int las=0;
	For(i,1,m)
	{
		while(x[las+1]<=y[i])	las++;
		tep[i]=las;
	}
	memset(dp,0x3f,sizeof dp);memset(tdp,0x3f,sizeof tdp);
	dp[0]=0;
	ll ans=1e15;
	For(p,1,m)
	{
		int l=1,r=1;
		q[r]=0;
		For(i,0,n)
		{
			while(l<=r&&i-(q[l]+1)>=c[p])	++l;
			tdp[i]=min(dp[i],dp[q[l]]+Get(q[l]+1,i,p));
			while(l<=r&&dp[i]<=dp[q[r]]+Get(q[r]+1,i,p))	--r;
			q[++r]=i;
		}
		ans=min(ans,tdp[n]);
		swap(dp,tdp);
	}
	writeln(ans);
}
