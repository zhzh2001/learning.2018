#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define pb push_back


using namespace std;

inline ll read()
{
	ll t=0,dp=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	dp=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-48,c=getchar();
	return t*dp;
}
inline void write(ll x){if(x<0)	{putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+48);}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
/*------------------------------------------------------------------------------------------------------*/

const int N=100006;
ll ans,mx,sum[N];
int n,a[N];
bool vis[N];
vector<int> vec[N];
inline ll Get(int x)
{
	if(vis[x])	return 0;
	vis[x]=1;
	sort(vec[x].begin(),vec[x].end());
	int mid=(vec[x].size()+1)/2-1;
	ll tmp=0;
	For(i,0,vec[x].size()-1)	tmp+=1LL*abs(vec[x][i]-vec[x][mid]);
	return sum[x]-tmp;
}
int main()
{
	freopen("balance.in","r",stdin);
	freopen("balance.out","w",stdout);
	n=read(); 
	For(i,1,n)	a[i]=read();
	For(i,1,n-1)
	{
		int delta=abs(a[i]-a[i+1]);
		if(a[i]==a[i+1])	continue;
		sum[a[i]]+=delta;sum[a[i+1]]+=delta;
		vec[a[i]].pb(a[i+1]);vec[a[i+1]].pb(a[i]);
		ans+=delta;
	}
	For(i,1,n)	mx=max(mx,Get(a[i]));
	writeln(ans-mx);
}
/*
5
9 4 3 8 8 
*/
