#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")
#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define pb push_back


using namespace std;

inline ll read()
{
	ll t=0,dp=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	dp=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-48,c=getchar();
	return t*dp;
}
inline void write(ll x){if(x<0)	{putchar('-');x=-x;}	if(x>=10)	write(x/10);putchar(x%10+48);}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
/*------------------------------------------------------------------------------------------------------*/
int son[1000555][26],val[1000555],vis[26],to[26],tot;
char s[50005][26];
inline void Insert(char s[],int x)
{
	int len=strlen(s+1),now=0;
	For(i,1,len)
	{
		if(!son[now][s[i]-'a'])	son[now][s[i]-'a']=++tot;
		now=son[now][s[i]-'a'];
	}
	val[now]=x;
}
const int N=30;
int poi[N*N],low[N],dfn[N],que[N],flag,inq[N],tmp[N],q_top,tim,cnt,nxt[N*N],head[N];
inline void add(int x,int y){poi[++cnt]=y;nxt[cnt]=head[x];head[x]=cnt;}
inline void Tar(int x)
{
	que[++q_top]=x;dfn[x]=++tim;low[x]=dfn[x];inq[x]=1;
	for(int i=head[x];i;i=nxt[i])
	{
		if(!dfn[poi[i]])	Tar(poi[i]),low[x]=min(low[x],low[poi[i]]);
		else	if(inq[poi[i]])	low[x]=min(low[x],low[poi[i]]);
	}
	if(low[x]==dfn[x])
	{
		int sz=0;
		while(1)
		{
			sz++;inq[que[q_top]]=0;
			q_top--;
			if(que[q_top+1]==x)	break;
		}
		if(sz>=2)	flag=1;
	}
}
inline bool Check(int len,int mb)
{
	int now=0;
	For(i,0,25)	head[i]=inq[i]=dfn[i]=low[i]=0;cnt=0;
	flag=0;q_top=0;
	For(i,1,len)
	{
		int tt=0;
		For(c,0,25)	if(son[now][c])	tmp[++tt]=c;
		For(c,1,tt)	if(tmp[c]!=s[mb][i]-'a')	add(s[mb][i]-'a',tmp[c]);
		now=son[now][s[mb][i]-'a'];
		if(val[now]&&i!=len)	return 0;
	}
	For(i,0,25)	if(!dfn[i])	Tar(i);
	if(flag)	return 0;else	return 1;
}
int top,q[1000555],n;
inline void Work(int x)
{
	int num=0;
	For(i,0,25)	vis[i]=0,to[i]=-1;
	int len=strlen(s[x]+1);
	if(Check(len,x))	q[++top]=x;
}
int main()
{
	freopen("string.in","r",stdin);
	freopen("string.out","w",stdout);
	n=read();
	For(i,1,n)
	{
		scanf("\n%s",s[i]+1);
		Insert(s[i],i);
	}
	For(i,1,n)	Work(i);
	writeln(top);
	For(i,1,top)	
	{
		int l=strlen(s[q[i]]+1);
		For(j,1,l)	putchar(s[q[i]][j]);
		puts("");
	}
}
/*
a b c d e f g h i j 
3 
aba
aac
bac
*/
