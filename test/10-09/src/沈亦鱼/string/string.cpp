#include<cstdio>
#include<cstring>
using namespace std;
char s[51000][25];
int n,k,x,ans,tot,len,b[51000],he[1000005],to[51000],ne[51000],p[1000005][27],lj[27][27],ru[27];
inline int read(){
	int ret=0,p=1;
	char c=getchar();
	while((c>'9')||(c<'0')){
		if(c=='-')p=-1;
		c=getchar();
	}
	while((c>='0')&&(c<='9')){
		ret=(ret<<1)+(ret<<3)+c-'0';
		c=getchar();
	}
	return ret*p;
}
void adg(int u,int v){
	tot++;
	to[tot]=v;
	ne[tot]=he[u];
	he[u]=tot;
}
void dfs(int k){//printf("%d %d\n",k,ru[1]);
	if(he[k]>0){
		for(int i=he[k];i;i=ne[i]){
			ans++;
			b[to[i]]=1;
		}
		return;
	}
	int h=0,t=0,d[27],dr[27];
	for(int i=0;i<26;i++){
		if(ru[i]==0){
			t++;
			d[t]=i;
		}
		dr[i]=ru[i];
	}
	while(h<t){
		h++;
		if(p[k][d[h]]==0){
			for(int i=0;i<26;i++)
				if(lj[d[h]][i]>0){
					dr[i]--;
					if(dr[i]==0){
						t++;
						d[t]=i;
					}
				}
		}
		else{
			for(int i=0;i<26;i++)
				if(p[k][i]>0&&lj[d[h]][i]==0&&d[h]!=i){
					lj[d[h]][i]=k;
					ru[i]++;
				}
			dfs(p[k][d[h]]);
			for(int i=0;i<26;i++)
				if(p[k][i]>0&&lj[d[h]][i]==k&&d[h]!=i){
					lj[d[h]][i]=0;
					ru[i]--;
				}
		}
	}
}
int main(){
	freopen("string.in","r",stdin);
	freopen("string.out","w",stdout);
	scanf("%d\n",&n);
	k=1;
	for(int i=1;i<=n;i++){
		scanf("%s",s[i]);
		len=strlen(s[i]);
		x=1;
		for(int j=0;j<len;j++){
			if(p[x][s[i][j]-'a']>0)x=p[x][s[i][j]-'a'];
			else{
				k++;
				p[x][s[i][j]-'a']=k;
				x=k;
			}//printf("%d\n",x);
		}
		adg(x,i);//printf("%d\n",k);
	}
	dfs(1);
	printf("%d\n",ans);
	for(int i=1;i<=n;i++)
		if(b[i]>0)puts(s[i]);
	return 0;
}
