#include<cstdio>
#include<algorithm>
using namespace std;
int n,tot,a[110000],b[110000];
long long l,mid,r,l1,r1,x,y,ans,p[210000],ne[210000],he[110000];
inline int read(){
	int ret=0,p=1;
	char c=getchar();
	while((c>'9')||(c<'0')){
		if(c=='-')p=-1;
		c=getchar();
	}
	while((c>='0')&&(c<='9')){
		ret=(ret<<1)+(ret<<3)+c-'0';
		c=getchar();
	}
	return ret*p;
}
long long ab(long long x){
	if(x<0)return -x;
	else return x;
}
void adg(int u,int v){
	tot++;
	p[tot]=v;
	ne[tot]=he[u];
	he[u]=tot;
}
//long long cha(int i){
//	long long sum=0;
//	for(int j=he[i];j;j=ne[j])
//		sum+=ab(i-p[j]);
//	return sum;
//}
long long che(long long i,long long nu){
	long long s1=0,s2=0;
	for(int j=he[i];j;j=ne[j]){
		s1+=ab(i-p[j]);
		s2+=ab(nu-p[j]);
	}
	return s1-s2;
}
int main(){
	freopen("balance.in","r",stdin);
	freopen("balance.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)
		a[i]=read();
	for(int i=1;i<=n;i++){
		b[a[i]]=1;
		if(i>1&&a[i-1]!=a[i])adg(a[i],a[i-1]);
		if(i<n&&a[i+1]!=a[i])adg(a[i],a[i+1]);
	}
	for(int i=0;i<=100000;i++)
		if(b[i]>0){
			l=-1;
			r=100001;
//			s=cha(i);
			while(l+1<r){
				mid=(r-l+1)/3;
				l1=l+mid;
				r1=r-mid;
				x=che(i,l1);
				y=che(i,r1);//printf("%lld %lld %d %d\n",x,y,l,r);
				if(x>y){
					r=r1;
					ans=max(ans,x);
				}
				else{
					l=l1;
					ans=max(ans,y);
				}
			}
		}
	ans=-ans;//printf("%d\n",ans);
	for(int i=1;i<n;i++)
		ans+=ab(a[i]-a[i+1]);
	printf("%lld",ans);
	return 0;
}
