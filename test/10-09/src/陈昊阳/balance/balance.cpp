#include<iostream>
#include<cstdio>
#include<cstring>
#include<string>
#include<cmath>
#include<algorithm>
#include<map>
#include<vector>
#include<cstdlib>
#include<ctime>
#define rep(a,b,c) for(int a=b;a<=c;++a)
#define drp(a,b,c) for(int a=b;a>=c;--a)
#define N 100009
#define M 200009
#define mod 1000000007
#define LL long long
#define XJ I love you
using namespace std;

LL n,a[N],sum,cnt,Max,W,Q,xj,ans;
LL st[N];
vector<int>b[N];

LL read(){
 LL x=0,fh=0; char c=getchar();
  while(!isdigit(c)) fh=(c=='-')?1:fh,c=getchar();
  while(isdigit(c)) x=(x<<3)+(x<<1)+(c^'0'),c=getchar();
 return fh?-x:x;
}

int main(){
freopen("balance.in","r",stdin); freopen("balance.out","w",stdout);
	n=read();
	rep(i,1,n) a[i]=read(),Max=max(Max,a[i]);
	
	for(int i=2;i<n;++i){
	 sum+=abs(a[i]-a[i-1]);
	 if(a[i-1]!=a[i]) b[a[i]].push_back(a[i-1]);
	 if(a[i+1]!=a[i]) b[a[i]].push_back(a[i+1]);
	}
	sum+=abs(a[n]-a[n-1]); 
	if(a[1]!=a[2]) b[a[1]].push_back(a[2]);
	if(a[n]!=a[n-1]) b[a[n]].push_back(a[n-1]);
	
	rep(i,0,Max){
	 cnt=b[i].size(); W=xj=0;
	 if(!cnt) continue;
	 
	 rep(j,1,cnt) 
	  st[j]=b[i][j-1],W+=abs(i-st[j]);
	 sort(st+1,st+cnt+1);
	 
	 Q=st[(1+cnt)>>1];
	 rep(j,1,cnt) xj+=abs(Q-st[j]);
	 ans=max(ans,W-xj);
	}
	
	cout<<sum-ans;
return 0;
}







