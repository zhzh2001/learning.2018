#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
int read(){
	int x=0,sgn=1;
	char c=getchar();
	while(c<'0' || c>'9'){
		if (c=='-')
			sgn=-1;
		c=getchar();
	}
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*sgn;
}
struct trie{
	static const int maxt=1000003;
	int sz;
	trie(){
		sz=1;
	}
	int ch[maxt][26],fa[maxt];
	bool is[maxt];
	int ins(char *s){
		int l=strlen(s);
		int u=1;
		for(int i=0;i<l;i++){
			int &v=ch[u][s[i]-'a'];
			if (!v){
				v=++sz;
				fa[v]=u;
			}
			u=v;
		}
		is[u]=1;
		return u;
	}
	int a[26];
	bool check(int u){
		memset(a,0,sizeof(a));
		for(;;){
			int t=u,w=0;
			u=fa[u];
			if (!u)
				break;
			if (is[u])
				return false;
			for(int i=0;i<26;i++){
				if (t==ch[u][i]){
					w=i;
					break;
				}
			}
			for(int i=0;i<26;i++)
				if (ch[u][i]){
					a[w]|=(1<<i);
					// fprintf(stderr,"addedge %d %d\n",w,i);
				}
		}
		// fprintf(stderr,"WTF");
		for(int k=0;k<26;k++)
			for(int i=0;i<26;i++)
				if (a[i]>>k&1)
					a[i]|=a[k];
		for(int i=0;i<26;i++)
			for(int j=i+1;j<26;j++)
				if ((a[i]>>j&1) && (a[j]>>i&1))
					return false;
		return true;
	}
}t;
const int maxn=50002;
int id[maxn];
char s[maxn][25];
bool flag[maxn];
int main(){
	freopen("string.in","r",stdin);
	freopen("string.out","w",stdout);
	int n=read();
	for(int i=1;i<=n;i++){
		scanf("%s",s[i]);
		id[i]=t.ins(s[i]);
	}
	int ans=0;
	for(int i=1;i<=n;i++)
		if (t.check(id[i]))
			flag[i]=1,ans++;
	printf("%d\n",ans);
	for(int i=1;i<=n;i++)
		if (flag[i])
			puts(s[i]);
}