#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
int read(){
	int x=0,sgn=1;
	char c=getchar();
	while(c<'0' || c>'9'){
		if (c=='-')
			sgn=-1;
		c=getchar();
	}
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*sgn;
}
const int maxn=300005;
int a[maxn];
pair<int,int> b[maxn];
int c[maxn];
inline int sub(int x,int y){
	if (x>y)
		return x-y;
	else return y-x;
}
int main(){
	freopen("balance.in","r",stdin);
	freopen("balance.out","w",stdout);
	int n=read();
	if (n<=1)
		return puts("0"),0;
	for(int i=1;i<=n;i++){
		a[i]=read();
		b[i]=make_pair(a[i],i);
	}
	sort(b+1,b+n+1);
	int cur=0;
	ll sum=0;
	for(int i=2;i<=n;i++)
		sum+=sub(a[i],a[i-1]);
	ll ans=sum,now=0;
	for(int i=1;i<=n;i++){
		int p=b[i].second;
		if (p!=1 && a[p]!=a[p-1]){
			now-=sub(a[p],a[p-1]);
			c[++cur]=a[p-1];
		}
		if (p!=n && a[p]!=a[p+1]){
			now-=sub(a[p],a[p+1]);
			c[++cur]=a[p+1];
		}
		if (i==n || b[i].first!=b[i+1].first){
			sort(c+1,c+cur+1);
			int x=c[(cur+1)/2];
			for(int j=1;j<=cur;j++)
				now+=sub(c[j],x);
			ans=min(ans,sum+now);
			cur=0;
			now=0;
		}
	}
	printf("%lld",ans);
}