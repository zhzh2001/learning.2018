#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
int read(){
	int x=0,sgn=1;
	char c=getchar();
	while(c<'0' || c>'9'){
		if (c=='-')
			sgn=-1;
		c=getchar();
	}
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*sgn;
}
int a[233],b[233],c[233];
int n,m;
const ll INF=0x3f3f3f3f3f3f3f3fLL;
ll ans=INF;
void dfs(int d,ll now){
	if (d==n+1){
		// fprintf(stderr,"now=%lld\n",now);
		ans=min(ans,now);
		return;
	}
	for(int i=1;i<=m;i++){
		if (!c[i])
			continue;
		ll to=now;
		if (a[d]>b[i])
			to+=a[d]-b[i];
		else to+=b[i]-a[d];
		c[i]--;
		dfs(d+1,to);
		c[i]++;
	}
}
int main(){
	freopen("hole.in","r",stdin);
	freopen("bf.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)
		scanf("%d",a+i);
	for(int i=1;i<=m;i++)
		scanf("%d%d",b+i,c+i);
	dfs(1,0);
	if (ans==INF)
		puts("Stupid game!");
	else printf("%lld",ans);
}