#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int mod=2000000001;
const int shift=1000000000;
ll get(){
	ll res=0;
	for(int i=1;i<=10;i++)
		res=res*10+rand()%10;
	return res;
}
int main(){
	freopen("hole.in","w",stdout);
	srand(time(0));
	int n=5000,m=5000;
	printf("%d %d\n",n,m);
	for(int i=1;i<=n;i++)
		printf("%lld ",(get()%mod)-shift);
	puts("");
	for(int i=1;i<=m;i++)
		printf("%lld %d\n",(get()%mod)-shift,rand()%5000+1);
}