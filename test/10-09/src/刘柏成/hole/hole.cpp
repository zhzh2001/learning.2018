#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
int read(){
	int x=0,sgn=1;
	char c=getchar();
	while(c<'0' || c>'9'){
		if (c=='-')
			sgn=-1;
		c=getchar();
	}
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*sgn;
}
const int maxn=5005;
const ll INF=0x3f3f3f3f3f3f3f3fLL;
int a[maxn];
struct hole{
	int b,c;
	bool operator <(const hole& rhs)const{
		return b<rhs.b;
	}
}h[maxn];
ll ff[maxn],gg[maxn],*f=ff,*g=gg;
ll sum[maxn];
inline int sub(int x,int y){
	if (x>y)
		return x-y;
	else return y-x;
}
int q[maxn];
int main(){
	freopen("hole.in","r",stdin);
	freopen("hole.out","w",stdout);
	int n=read(),m=read();
	for(int i=1;i<=n;i++)
		a[i]=read();
	sort(a+1,a+n+1);
	int tot=0;
	for(int i=1;i<=m;i++){
		h[i].b=read(),h[i].c=read();
		tot+=h[i].c;
	}
	if (tot<n){
		puts("Stupid game!");
		return 0;
	}
	sort(h+1,h+m+1);
	for(int i=1;i<=n;i++){
		if (i>h[1].c)
			g[i]=INF;
		else g[i]=g[i-1]+sub(a[i],h[1].b);
	}
	for(int o=2;o<=m;o++){
		for(int i=1;i<=n;i++)
			sum[i]=sum[i-1]+sub(a[i],h[o].b);
		int l=1,r=1;
		q[1]=0;
		for(int i=1;i<=n;i++){
			while(l<=r && g[q[r]]-sum[q[r]]>=g[i]-sum[i])
				r--;
			q[++r]=i;
			f[i]=min(sum[i]+g[q[l]]-sum[q[l]],INF);
			if (l<=r && q[l]==i-h[o].c)
				l++;
		}
		swap(f,g);
	}
	printf("%lld",g[n]);
}