#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstring>
using namespace std;
int n,cnt=0,p[26],q[26],qt,d[26][50000*21],dt=0;
char c[50003][23];
bool ans[50003],e[26][26],ed[50000*21];
int root=-1;
int adp(){
	for(int i='a';i<='z';i++){
		d[i-'a'][dt]=-1;
	}
	return dt++;
}
void add(int&v,char*c){
	if(*c==0){
		ed[v]=1;
		return;
	}
	if(d[*c-'a'][v]==-1)d[*c-'a'][v]=adp();
	add(d[*c-'a'][v],c+1);
}
void te(int&v,char*c){
	if(*c==0)return;
	if(ed[v]){
		e[0][0]=1;
		return;
	}
	for(int i='a';i<='z';i++)if(i!=*c&&d[i-'a'][v]!=-1){
		e[i-'a'][*c-'a']=1;
	}
	te(d[*c-'a'][v],c+1);
}
bool ck(char*c){
	memset(e,0,sizeof(e));
	te(root,c);
	qt=0;
	for(int i=0;i<26;i++){
		p[i]=0;
		for(int j=0;j<26;j++)p[i]+=e[j][i];
		if(p[i]==0)q[qt++]=i;
	}
	for(int i=0;i<26;i++){
		if(i>=qt)return 0;
		for(int j=0;j<26;j++)if(e[q[i]][j]){
			p[j]--;
			if(p[j]==0)q[qt++]=j;
		}
	}
	return 1;
}
int main(){
	freopen("string.in","r",stdin);
	freopen("string.out","w",stdout);
	scanf("%d",&n);
	root=adp();
	for(int i=1;i<=n;i++){
		scanf("%s",c[i]);
		add(root,c[i]);
	}
	for(int i=1;i<=n;i++){
		if(ck(c[i])){
			cnt++;
			ans[i]=1;
		}
	}
	printf("%d\n",cnt);
	for(int i=1;i<=n;i++)if(ans[i]){
		puts(c[i]);
	}
	return 0;
}
