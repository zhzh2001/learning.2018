#include <iostream>
#include <algorithm>
#include <cstdio>
#include <queue>
#include <cmath>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline LL read(){
	LL a=0,b=1;int dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
LL n,a[100003],x,y,z,sum,ans,c[100003],d[100003];
priority_queue<LL>b[100003];
int main(){
	freopen("balance.in","r",stdin);
	freopen("balance.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){
		a[i]=read();
		if(i>1){
			sum+=abs(a[i]-a[i-1]);
			c[a[i-1]]+=abs(a[i]-a[i-1]);
			c[a[i]]+=abs(a[i]-a[i-1]);
		}
	}
	ans=sum;
	for(int i=1;i<=n;i++){
		if(i!=1&&a[i-1]!=a[i])b[a[i]].push(a[i-1]);
		if(i!=n&&a[i+1]!=a[i])b[a[i]].push(a[i+1]);
	}
	for(int i=0;i<=100000;i++)if(!b[i].empty()){
		x=0;
		while(!b[i].empty()){
			d[++x]=b[i].top();
			b[i].pop();
		}
		y=-c[i];
		z=(x+1)/2;
		for(int i=1;i<=x;i++){
			y+=abs(d[i]-d[z]);
		}
		ans=min(ans,sum+y);
	}
	printf("%lld\n",ans);
	return 0;
}
