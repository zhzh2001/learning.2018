#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline LL nextLong(){
	LL a=0,b=1;int dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
inline int nextInt(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
const LL oo=1ll<<60;
int n,m,x,y;
LL a[5003],f[2][5003],sum,ans;
pair<LL,int>b[5003];
LL mv[5003],zv=0;
int mt[5003],ml,mr;
void init(){
	ml=1;
	mr=0;
	zv=0;
}
void ct(int tt){
	while(ml<=mr&&mt[ml]<tt)ml++;
}
void add(LL p,int t){
	p-=zv;
	while(ml<=mr&&mv[mr]>=p)mr--;
	mv[++mr]=p;
	mt[mr]=t;
}
LL top(){
	return mv[ml]+zv;
}
int main(){
	freopen("hole.in","r",stdin);
	freopen("hole.out","w",stdout);
	n=nextInt();
	m=nextInt();
	f[0][0]=0;
	for(int i=1;i<=n;i++){
		a[i]=nextLong();
		f[0][i]=oo;
	}
	sum=0;
	for(int i=1;i<=m;i++){
		b[i].first=nextLong();
		b[i].second=nextInt();
		sum+=b[i].second;
	}
	if(sum<n){
		puts("Stupid game!");
		return 0;
	}
	sort(a+1,a+n+1);
	sort(b+1,b+m+1);
	ans=oo;
	for(int i=1;i<=m;i++){
		x=i&1;
		y=x^1;
		init();
		add(f[y][0],0);
		f[x][0]=0;
		for(int j=1;j<=n;j++){
			ct(j-b[i].second);
			zv+=abs(a[j]-b[i].first);
			add(f[y][j],j);
			f[x][j]=top();
		}
		ans=min(ans,f[x][n]);
	}
	printf("%lld\n",ans);
	return 0;
}
