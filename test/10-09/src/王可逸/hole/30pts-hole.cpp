#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read() {
	int X = 0, w = 0;
	char ch = 0;
	while (!isdigit(ch)) {
		w |= ch == '-';
		ch = getchar();
	}
	while (isdigit(ch)) X = (X << 3) + (X << 1) + (ch ^ 48), ch = getchar();
	return w ? -X : X;
}
int n,m,ball[5600],where[5600],v[5600],tot,used[5600],ww=0,ans=1e18;
bool vis[5600];
void dfs(int at,int num) {
	if(at==n+1) {
		ww++;
		if(ww%1000000==0) cout<<ww<<'\n';
		ans=min(ans,num);
		return;
	}
	for(int i=1;i<=m;i++) 
		if(used[i]+1<=v[i]) {
			used[i]++;
			dfs(at+1,num+abs(ball[at]-where[i]));
			used[i]--;
		}
}
signed main() {
	n=read();
	m=read();
	for(int i=1;i<=n;i++) ball[i]=read();
	for(int i=1;i<=m;i++) {
		where[i]=read(),v[i]=read();
		tot+=v[i];
	}
	if(tot<n) {
		puts("Stupid game!");
		return 0;
	}
	dfs(1,0);
	cout<<ans;
}
