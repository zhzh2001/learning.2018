#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read() {
	int X = 0, w = 0;
	char ch = 0;
	while (!isdigit(ch)) {
		w |= ch == '-';
		ch = getchar();
	}
	while (isdigit(ch)) X = (X << 3) + (X << 1) + (ch ^ 48), ch = getchar();
	return w ? -X : X;
}
int n,m,a[5600];
struct h {
	int at,v;
} sxd[5600];
struct node {
	int num,sec,secdis;
	friend bool operator < (node a,node b) {
		return a.secdis>b.secdis;
	}
};
bool cmp(h a,h b) {
	return a.at<b.at;
}
priority_queue <node> q[5600];
int have[5600],tot,ans,used[5600],ans1;
void dfs(int at,int num) {
	if(at==n+1) {
		ans1=min(ans1,num);
		return;
	}
	for(int i=1; i<=m; i++) {
		if(used[i]+1<=sxd[i].v) {
			used[i]++;
			dfs(at+1,num+abs(a[at]-sxd[i].at));
			used[i]--;
		}
	}
}
signed main() {
	freopen("hole.in","r",stdin);
	freopen("hole.out","w",stdout);
	n=read();
	m=read();
	for(int i=1; i<=n; i++) a[i]=read();
	sort(a+1,a+n+1);
	for(int i=1; i<=m; i++) {
		sxd[i].at=read();
		sxd[i].v=read();
		tot+=sxd[i].v;
	}
	if(tot<n) {
		puts("Stupid game!");
		return 0;
	}
	ans=0;
	ans1=1e18;
	if(n<=10) {
		dfs(1,0);
		cout<<ans1;
		return 0;
	} else {
		for(int i=1; i<=n; i++) {
			int mn=0x3f3f3f3f,se=0x3f3f3f3f,mnat,seat;
			for(int j=1; j<=m; j++) {
				if(abs(a[i]-sxd[j].at)<mn) {
					se=mn;
					seat=mnat;
					mn=abs(a[i]-sxd[j].at);
					mnat=j;
				} else if(abs(a[i]-sxd[j].at)<se) {
					se=abs(a[i]-sxd[j].at);
					seat=j;
				}
			}
			have[mnat]++;
			q[mnat].push((node) {
				i,seat,se
			});
		}
		for(int i=1; i<=m; i++) {
			if(have[i]>sxd[i].v) {
				while(have[i]>sxd[i].v) {
					while(!q[i].empty()) {
						node xd=q[i].top();
						q[i].pop();
						if(have[xd.sec]+1<=sxd[xd.sec].v) {
							int mn=0x3f3f3f3f,mnat=0;
							for(int j=1; j<=m; j++) {
								if(have[j]+1<=sxd[j].v && abs(a[xd.num]-sxd[j].at)<mn) {
									mn=abs(a[xd.num]-sxd[j].at);
									mnat=j;
								}
							}
							q[xd.sec].push((node) {
								xd.num,mnat,mn
							});
						} else {
							int mn=0x3f3f3f3f,mnat=0,se=0x3f3f3f3f,seat;
							for(int j=1; j<=m; j++) {
								if(have[j]+1<=sxd[j].v && abs(a[xd.num]-sxd[j].at)<mn) {
									se=mn;
									seat=mnat;
									mn=abs(a[xd.num]-sxd[j].at);
									mnat=j;
								} else if(have[j]+1<=sxd[j].v && abs(a[xd.num]-sxd[j].at)<se) {
									se=abs(a[xd.num]-sxd[j].at);
									seat=j;
								}
							}
							q[mnat].push((node) {
								xd.num,seat,se
							});
						}
						have[i]--;
						if(have[i]==sxd[i].v) goto L1;
					}
				}
L1:
				;
			}
		}
		ans=0;
		for(int i=1; i<=m; i++) {
			while(!q[i].empty()) {
				node w=q[i].top();
				q[i].pop();
				ans+=abs(a[w.num]-sxd[i].at);
			}
		}
		cout<<ans;
	}
	return 0;
}
