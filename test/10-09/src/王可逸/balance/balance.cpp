#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read() {
	int X = 0, w = 0;
	char ch = 0;
	while (!isdigit(ch)) {
		w |= ch == '-';
		ch = getchar();
	}
	while (isdigit(ch)) X = (X << 3) + (X << 1) + (ch ^ 48), ch = getchar();
	return w ? -X : X;
}
int n, a[120000], now, cha[120000],mx,mxat;
set <int> q;
vector <int> v[120000];
int count(int x) {
	int sz=v[x].size();
	sort(v[x].begin(),v[x].end());
	int ans=0,w=v[x][sz/2];
	for(int i=0;i<sz;i++)
		ans+=abs(v[x][i]-w);
	return ans;
}
signed main() {
	freopen("balance.in","r",stdin);
	freopen("balance.out","w",stdout);
	n = read();
	for (int i = 1; i <= n; i++) {
		a[i] = read();
		q.insert(a[i]);
		if(i!=1) now+=abs(a[i]-a[i-1]);
	}
	for (int i = 1; i < n; i++)
		if (a[i] != a[i + 1]) {
			v[a[i]].push_back(a[i + 1]);
			v[a[i + 1]].push_back(a[i]);
			cha[a[i]] += abs(a[i] - a[i + 1]);
			cha[a[i + 1]] += abs(a[i] - a[i + 1]);
		}
	for (set <int> :: iterator it = q.begin(); it != q.end(); it++) {
		int sxd=*it;
		if(cha[sxd]-count(sxd)>mx) mx=cha[sxd]-count(sxd);
	}
	cout<<now-mx;
	return 0;
}
