#include<bits/stdc++.h>
#define int long long
using namespace std;
inline void read(int &q){
	int x=0,f=1;
	char ch;
	for(ch=getchar();(ch<'0'||ch>'9')&&ch!='-';ch=getchar());
	if(ch=='-'){
		f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=(x<<3)+(x<<1)+ch-'0';
		ch=getchar();
	}
	q=x*f;
}
int n,tot,sum,x,y,maxx,minn=998244353,ans=998244353;
int a[100001],num[100001];
map<int,int>mp;
signed main(){
	freopen("balance.in","r",stdin);
	freopen("balance.out","w",stdout);
	read(n);
	for(int i=1;i<=n;i++){
		read(a[i]);
		if(!mp[a[i]]){
			tot++;
			num[tot]=a[i];
			mp[a[i]]=1;
		}
		maxx=max(maxx,a[i]);
		minn=min(minn,a[i]);
	}
	for(int i=1;i<=tot;i++)
		for(int j=minn;j<=maxx;j++){
			sum=0;
			for(int k=1;k<n;k++){
				x=a[k];
				y=a[k+1];
				if(x==num[i])
					x=j;
				if(y==num[i])
					y=j;
				sum+=abs(x-y);
			}
			ans=min(sum,ans);
		}
	cout<<ans;
	return 0;
}
