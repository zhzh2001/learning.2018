#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("balance.in");
ofstream fout("balance.out");
const int N=100005,M=200005;
int a[N],head[N],v[M],nxt[M],e,tmp[N];
inline void add_tag(int u,int v)
{
	::v[++e]=v;
	nxt[e]=head[u];
	head[u]=e;
}
int main()
{
	int n;
	fin>>n;
	long long sum=0;
	for(int i=1;i<=n;i++)
	{
		fin>>a[i];
		if(i>1)
			sum+=abs(a[i]-a[i-1]);
	}
	if(a[1]!=a[2])
		add_tag(a[1],a[2]);
	for(int i=2;i<n;i++)
	{
		if(a[i]!=a[i-1])
			add_tag(a[i],a[i-1]);
		if(a[i]!=a[i+1])
			add_tag(a[i],a[i+1]);
	}
	if(a[n]!=a[n-1])
		add_tag(a[n],a[n-1]);
	long long ans=sum;
	for(int i=0;i<N;i++)
		if(head[i])
		{
			long long now=sum;
			int cc=0;
			for(int j=head[i];j;j=nxt[j])
			{
				now-=abs(v[j]-i);
				tmp[++cc]=v[j];
			}
			int mid=(1+cc)/2;
			nth_element(tmp+1,tmp+mid,tmp+cc+1);
			for(int j=1;j<=cc;j++)
				now+=abs(tmp[j]-tmp[mid]);
			ans=min(ans,now);
		}
	fout<<ans<<endl;
	return 0;
}
