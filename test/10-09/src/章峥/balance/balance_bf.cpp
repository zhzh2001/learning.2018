#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("balance.in");
ofstream fout("balance.ans");
const int N=100005,INF=1e9;
int a[N],b[N],tmp[N];
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	copy(a+1,a+n+1,b+1);
	int ans=INF;
	for(int i=1;i<=n;i++)
		for(int j=0;j<=2333;j++)
		{
			for(int k=1;k<=n;k++)
				if(a[k]==b[i])
					tmp[k]=b[j];
				else
					tmp[k]=a[k];
			int now=0;
			for(int k=1;k<n;k++)
				now+=abs(tmp[k+1]-tmp[k]);
			ans=min(ans,now);
		}
	fout<<ans<<endl;
	return 0;
}
