#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("hole.in");
ofstream fout("hole.ans");
const int N=5005;
const long long INF=1e18;
int a[N];
pair<int,int> hole[N];
long long dp[N][N];
int main()
{
	int n,m;
	fin>>n>>m;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	sort(a+1,a+n+1);
	for(int i=1;i<=m;i++)
		fin>>hole[i].first>>hole[i].second;
	sort(hole+1,hole+m+1);
	fill_n(&dp[0][0],sizeof(dp)/sizeof(long long),INF);
	dp[0][0]=0;
	for(int i=1;i<=m;i++)
	{
		dp[i][0]=0;
		for(int j=1;j<=n;j++)
		{
			dp[i][j]=dp[i-1][j];
			long long sum=abs(hole[i].first-a[j]);
			for(int k=j-1;k>=0&&j-k<=hole[i].second;k--)
			{
				dp[i][j]=min(dp[i][j],dp[i-1][k]+sum);
				sum+=abs(hole[i].first-a[k]);
			}
		}
	}
	if(dp[m][n]==INF)
		fout<<"Stupid game!\n";
	else
		fout<<dp[m][n]<<endl;
	return 0;
}
