#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("hole.in");
ofstream fout("hole.out");
const int N=5005;
const long long INF=1e18;
int a[N];
pair<int,int> hole[N];
long long dp[N][N];
pair<long long,int> q[N];
int main()
{
	int n,m;
	fin>>n>>m;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	sort(a+1,a+n+1);
	for(int i=1;i<=m;i++)
		fin>>hole[i].first>>hole[i].second;
	sort(hole+1,hole+m+1);
	for(int i=1;i<=m;i++)
	{
		int l=1,r=1;
		q[1]=make_pair(0,0);
		long long sum=0;
		for(int j=1;j<=n;j++)
		{
			if(l<=r&&j-q[l].second>hole[i].second)
				l++;
			if(l>r)
				dp[i][j]=INF;
			else
				dp[i][j]=q[l].first+sum+abs(hole[i].first-a[j]);
			sum+=abs(hole[i].first-a[j]);
			if(i>1)
			{
				dp[i][j]=min(dp[i][j],dp[i-1][j]);
				if(dp[i-1][j]<INF)
				{
					for(;l<=r&&q[r].first>=dp[i-1][j]-sum;r--);
					q[++r]=make_pair(dp[i-1][j]-sum,j);
				}
			}
		}
	}
	if(dp[m][n]==INF)
		fout<<"Stupid game!\n";
	else
		fout<<dp[m][n]<<endl;
	return 0;
}
