#include<fstream>
#include<string>
#include<algorithm>
using namespace std;
ifstream fin("string.in");
ofstream fout("string.ans");
const int N=50005;
string dict[N];
bool ans[N];
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
		fin>>dict[i];
	string alpha="abcdefghij";
	do
	{
		int mn=1;
		for(int i=2;i<=n;i++)
		{
			bool flag=false;
			for(int j=0;;j++)
			{
				int x=alpha.find(dict[i][j]),y=alpha.find(dict[mn][j]);
				if(x==string::npos)
				{
					flag=true;
					break;
				}
				if(y==string::npos)
					break;
				if(x<y)
				{
					flag=true;
					break;
				}
				if(x>y)
					break;
			}
			if(flag)
				mn=i;
		}
		for(int i=1;i<=n;i++)
			if(dict[i]==dict[mn])
				ans[i]=true;
	}while(next_permutation(alpha.begin(),alpha.end()));
	fout<<count(ans+1,ans+n+1,true)<<endl;
	for(int i=1;i<=n;i++)
		if(ans[i])
			fout<<dict[i]<<endl;
	return 0;
}
