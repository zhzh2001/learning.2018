#include<fstream>
#include<string>
#include<cstring>
using namespace std;
ifstream fin("string.in");
ofstream fout("string.out");
const int N=50005,C=1000005,A=26;
int trie[C][A],cnt[C],ind[A],q[A+5];
string dict[N];
bool mat[A][A],ans[N],ed[C];
int main()
{
	int n;
	fin>>n;
	int cc=1;
	for(int i=1;i<=n;i++)
	{
		fin>>dict[i];
		int id=1;
		for(int j=0;j<dict[i].length();j++)
		{
			if(!trie[id][dict[i][j]-'a'])
				trie[id][dict[i][j]-'a']=++cc;
			id=trie[id][dict[i][j]-'a'];
			cnt[id]++;
		}
		ed[id]=true;
	}
	int tot=0;
	for(int i=1;i<=n;i++)
	{
		memset(mat,0,sizeof(mat));
		memset(ind,0,sizeof(ind));
		bool prefix=false;
		for(int j=0,id=1;j<dict[i].length();j++)
		{
			for(int k=0;k<A;k++)
				if(k!=dict[i][j]-'a'&&cnt[trie[id][k]]&&!mat[dict[i][j]-'a'][k])
				{
					mat[dict[i][j]-'a'][k]=true;
					ind[k]++;
				}
			id=trie[id][dict[i][j]-'a'];
			if(ed[id]&&j<dict[i].length()-1)
			{
				prefix=true;
				break;
			}
		}
		if(prefix)
			continue;
		int l=1,r=0;
		for(int j=0;j<A;j++)
			if(!ind[j])
				q[++r]=j;
		int cnt=0;
		while(l<=r)
		{
			cnt++;
			for(int j=0;j<A;j++)
				if(mat[q[l]][j]&&!--ind[j])
					q[++r]=j;
			l++;
		}
		if(cnt==A)
		{
			tot++;
			ans[i]=true;
		}
	}
	fout<<tot<<endl;
	for(int i=1;i<=n;i++)
		if(ans[i])
			fout<<dict[i]<<endl;
	return 0;
}
