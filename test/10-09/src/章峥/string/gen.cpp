#include<fstream>
#include<random>
#include<windows.h>
using namespace std;
ofstream fout("string.in");
const int n=50000,l=20;
int main()
{
	minstd_rand gen(GetTickCount());
	fout<<n<<endl;
	for(int i=1;i<=n;i++)
	{
		uniform_int_distribution<> dl(l,l);
		int l=dl(gen);
		for(int j=1;j<=l;j++)
		{
			uniform_int_distribution<> dc('a','z');
			fout.put(dc(gen));
		}
		fout<<endl;
	}
	return 0;
}
