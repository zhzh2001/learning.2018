#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define PI pair<int,int>
#define mp make_pair
inline ll read(){
	bool pos=1; ll x=0; char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')pos=0;
	for(;isdigit(ch);ch=getchar())x=x*10+ch-'0';
	return pos?x:-x;
}
void write(ll x){
	if(x<0){
		x=-x; putchar('-');
	}
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x); puts("");
}
const int N=50005,K=27;
int n,ans,vis[N],cnt[N*20],nodecnt=1,q[K],rd[K],e[K][K],go[N*20][K];
char ch[N][K];
void insert(char *ch){
	int len=strlen(ch),p=1;
	for(int i=0;i<len;i++){
		int t=ch[i]-'a';
		if(!go[p][t])go[p][t]=++nodecnt;
		p=go[p][t];
	}
	cnt[p]++;
}
bool ask(char *ch){
	memset(e,0,sizeof(e)); memset(rd,0,sizeof(rd));
	int len=strlen(ch),p=1;
	for(int i=0;i<len;i++){
		int t=ch[i]-'a';
		if(cnt[p])return 0;
		for(int j=0;j<26;j++)if(j!=t&&go[p][j]){e[t][j]++; rd[j]++;}
		p=go[p][t];
	}
	//cout<<ch<<endl;
	//for(int i=0;i<26;i++)for(int j=0;j<26;j++)if(e[i][j])cout<<i<<" "<<j<<endl;
	int l=0,r=0;
	for(int i=0;i<26;i++)if(!rd[i])q[++r]=i;
	while(l<r){
		int k=q[++l];
		for(int j=0;j<26;j++)if(e[k][j]){
			rd[j]-=e[k][j]; if(rd[j]==0)q[++r]=j;
		}
	}
	if(l==26)return 1;
	return 0;
}
int main(){
	freopen("string.in","r",stdin); freopen("string.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++){scanf("%s",ch[i]); insert(ch[i]);}
	for(int i=1;i<=n;i++){
		if(ask(ch[i])){vis[i]=1; ans++;}
	}
	writeln(ans);
	for(int i=1;i<=n;i++)if(vis[i])puts(ch[i]);
}
/*
aa
ab
b比a小即可 
有公共的前缀说明 cha<chb
很多形如cha<chb的东西 
3
aba
aac
bac

*/
