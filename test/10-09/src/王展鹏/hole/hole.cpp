#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define int long long
#define PI pair<int,int>
#define mp make_pair
inline ll read(){
	bool pos=1; ll x=0; char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')pos=0;
	for(;isdigit(ch);ch=getchar())x=x*10+ch-'0';
	return pos?x:-x;
}
void write(ll x){
	if(x<0){
		x=-x; putchar('-');
	}
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x); puts("");
}
const int N=5005;
int n,m,a[N],q[N];
PI b[N];
ll sum[N],dp[N][N];
signed main(){
	freopen("hole.in","r",stdin); freopen("hole.out","w",stdout);
	n=read(); m=read();
	for(int i=1;i<=n;i++)a[i]=read();
	for(int i=1;i<=m;i++){
		b[i].first=read(); b[i].second=read();
	}
	sort(&b[1],&b[m+1]);
	sort(&a[1],&a[n+1]);
	//for(int i=1;i<=n;i++)cout<<a[i]<<" "; puts("");
	//for(int i=1;i<=m;i++)cout<<b[i].first<<" wzp "<<b[i].second<<endl;
	for(int i=1;i<=n;i++)dp[0][i]=1e18;
	for(int i=1;i<=m;i++){
		for(int j=1;j<=n;j++)sum[j]=sum[j-1]+abs(b[i].first-a[j]); 
		for(int j=0;j<=n;j++)dp[i-1][j]-=sum[j];
		int l=1,r=1; q[1]=0;
		for(int j=1;j<=n;j++){
			while(l<=r&&j-q[l]>b[i].second)l++;
			while(l<=r&&dp[i-1][j]<=dp[i-1][q[r]])r--; q[++r]=j;
			dp[i][j]=dp[i-1][q[l]]+sum[j];
			//if(i==2&&j==1)cout<<l<<" "<<r<<" "<<q[l]<<" "<<dp[1][1]<<" "<<dp[i][j]<<" "<<i<<" "<<j<<endl;
		}
	}
	if(dp[m][n]>=1000000000000000)puts("Stupid game!"); else cout<<dp[m][n]<<endl;
}
/*
dp[i][j]=min(dp[i-1][k]+cal(k+1,j,i))
cal(i,j,k) 表示[i,j]的球放到k中 
*/
