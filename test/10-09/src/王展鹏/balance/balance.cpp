#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define int long long
#define PI pair<int,int>
#define mp make_pair
inline ll read(){
	bool pos=1; ll x=0; char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')pos=0;
	for(;isdigit(ch);ch=getchar())x=x*10+ch-'0';
	return pos?x:-x;
}
void write(ll x){
	if(x<0){
		x=-x; putchar('-');
	}
	if(x>=10)write(x/10);
	putchar(x%10+'0');
}
void writeln(ll x){
	write(x); puts("");
}
const int N=100005;
int n,a[N],dq,ans,sum;
signed main(){
	freopen("balance.in","r",stdin); freopen("balance.out","w",stdout);
	n=read();
	a[0]=-1e18; 
	for(int i=1;i<=n;i++){
		int t=read();
		if(t!=a[dq]){
			a[++dq]=t;
		}
	}
	if(dq==1){
		puts("0"); return 0;
	}
	for(int i=1;i<dq;i++)sum+=abs(a[i]-a[i+1]);
	ans=sum;
	ans=min(sum-abs(a[1]-a[2]),sum-abs(a[dq]-a[dq-1])); 
	for(int i=2;i<dq;i++){
		ans=min(ans,sum-(abs(a[i]-a[i-1])+abs(a[i]-a[i+1]))+abs(a[i+1]-a[i-1]));
	}
	cout<<ans<<endl;
}
