#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for(int i=a;i<=b;i++)
#define Rep(i,a,b) for(int i=b;i>=a;i--)
#define pb push_back
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
vector<int>c[100100];
bool f[100100];
ll a[100100],b[100100],d[100100],e[100100];
ll n,ans,mx;
int main()
{
	freopen("balance.in","r",stdin);
	freopen("balance.out","w",stdout); 
	n=read();
	For(i,1,n)a[i]=read();
	For(i,2,n)
	{
		ans+=abs(a[i]-a[i-1]);
		if(a[i]==a[i-1])continue;
		b[a[i]]++;
		c[a[i]].pb(a[i-1]);
		d[a[i]]+=a[i-1];
	}
	//cout<<ans<<endl;
	For(i,1,n-1)
	{
		if(a[i]==a[i+1])continue;
		b[a[i]]++;
		c[a[i]].pb(a[i+1]);
		d[a[i]]+=a[i+1];
	}
	mx=0;
	memset(f,false,sizeof(f));
	For(i,1,n)
	{
		if(f[a[i]])continue;
		f[a[i]]=true;
		ll x,y,z,xx,yy,zz;
		x=0;y=0;z=0;
		xx=a[i];yy=d[xx]/b[xx];zz=yy+1;
		For(j,0,b[a[i]]-1)
		{
			e[j+1]=c[xx][j];
		}
		sort(e+1,e+b[xx]+1);
		if(b[xx]%2==1)yy=zz=e[b[xx]/2+1];
			else yy=(e[b[xx]/2+1]+e[b[xx]/2])/2,zz=yy+1;
		For(j,1,b[xx])
		{
			x+=abs(xx-e[j]);
			y+=abs(yy-e[j]);
			z+=abs(zz-e[j]);
		}
		y=min(y,z);
		mx=max(mx,x-y);
		//cout<<x-y<<endl;
	}
	cout<<ans-mx<<endl;
	return 0;
}

