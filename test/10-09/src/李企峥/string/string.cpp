#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for(int i=a;i<=b;i++)
#define Rep(i,a,b) for(int i=b;i>=a;i--)
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
struct D{
	int son[26];
	int t;
}a[1000100];
int c[255];
int n,cnt;
string s[50100];
int len[50100];
int mp[30][30];
bool can;
ll ans;
void build(int k,int t,int l)
{
	if(l==len[t])
	{
		a[k].t++;
		return;
	}
	l++;
	int v=c[s[t][l-1]];
	if(!a[k].son[v])a[k].son[v]=++cnt;
	if(l<=len[t])build(a[k].son[v],t,l);
}
void getmap(int k,int t,int l)
{
	if(l==len[t])return;
	l++;
	int v=c[s[t][l-1]];
	For(i,0,25)
	{
		if(i==v)continue;
		if(a[k].son[i])mp[v][i]=1;
	}
	if(a[k].t&&l<=len[t])can=false;
	if(l<=len[t])getmap(a[k].son[v],t,l);
	return;
}
bool vis[27];
int in[27];
int out[27];
int q[27];
bool anst[50100];
void dfs(int k)
{
	if(!can)return;
	vis[k]=true;
	For(i,0,25)
		if(mp[k][i])
			if(vis[i])can=false;
				else dfs(i);
	vis[k]=false;
}
int main()
{
	freopen("string.in","r",stdin);
	freopen("string.out","w",stdout);
	For(i,0,25)c[i+'a']=i;
	n=read();
	For(i,1,n)cin>>s[i];
	For(i,1,n)len[i]=s[i].length();
	cnt=1;
	For(i,1,n)build(1,i,0);
	For(i,1,n)
	{
		memset(mp,0,sizeof(mp));
		can=true;
		getmap(1,i,0);
		if(!can)continue;
		memset(vis,0,sizeof(vis));
		memset(in,0,sizeof(in));
		memset(out,0,sizeof(out));
		For(k,0,25)For(j,0,25)if(mp[k][j])in[j]++,out[k]++;
		For(j,0,25)if(!vis[j])dfs(j);
		if(!can)continue;
		if(can)ans++,anst[i]=true;
	}
	cout<<ans<<endl;
	For(i,1,n)if(anst[i])cout<<s[i]<<endl;
}
