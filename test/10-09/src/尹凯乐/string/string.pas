program strings;
 type
  wb=record
   n:^wb;
   t:char;
  end;
  wwb=^wb;
 const
  maxlan=20;
 var
  b:array[0..50001] of wb;
  a:array[0..50001,0..21,'a'..'z'] of longint;
  smove,s:array[0..50001] of string;
  lan,z,w:array[0..50001] of longint;
  h,d,f:array['a'..'z'] of longint;
  i,j,n,o,p,oc,pc,pinc,upass,ssum:longint;
  k,jc:char;
 procedure haha(l,r:longint);
  var
   o,p,t1:longint;
   m,t:string;
  procedure hahaswap(var x,y:string);
   begin
    t:=x;
    x:=y;
    y:=t;
   end;
  procedure hahaswap2(var x,y:longint);
   begin
    t1:=x;
    x:=y;
    y:=t1;
   end;
  function hahacmp(x,y:string):boolean;
   begin
    exit(x<y);
   end;
  begin
   o:=l;
   p:=r;
   m:=s[(l+r)>>1];
   repeat
    while hahacmp(s[o],m) do inc(o);
    while hahacmp(m,s[p]) do dec(p);
    if o>p then break;
    hahaswap(s[o],s[p]);
    hahaswap2(lan[o],lan[p]);
    hahaswap2(z[o],z[p]);
    inc(o);
    dec(p);
   until o>p;
   if o<r then haha(o,r);
   if l<p then haha(l,p);
  end;
 function hahafize(k,x,y:longint;z:char):longint;
  var
   m:longint;
  begin
   while x<=y do
    begin
     m:=(x+y)>>1;
     if s[m,k]<>z then y:=m-1
                  else x:=m+1;
    end;
   if y and 1=1 then exit(y)
                else exit(x-1);
  end;
 function hahafize2(k,x,y:longint;z:char):longint;
  var
   m:longint;
  begin
   while x<=y do
    begin
     m:=(x+y)>>1;
     if s[m,k]<>z then x:=m+1
                  else y:=m-1;
    end;
   if y and 1=1 then exit(y+1)
                else exit(x);
  end;
 procedure hahainc(x,y:char);
  begin
   inc(pinc);
   b[pinc].n:=@b[h[x]];
   b[pinc].t:=y;
   h[x]:=pinc;
  end;
 procedure hahaDFS(k:char);
  var
   i:char;
   j:wwb;
  begin
   f[k]:=1;
   d[k]:=1;
   j:=@b[h[k]];
   while j<>@b[0] do
    begin
     i:=j^.t;
     j:=j^.n;
     if d[i]=1 then upass:=1;
     if f[i]=0 then hahaDFS(i);
    end;
   d[k]:=0;
  end;
 procedure hahacheck;
  var
   i,j,k:longint;
   jc:char;
  begin
   {for i:=1 to n do
    begin
     for j:=1 to 3 do
      write(a[i,j],' ');
     writeln(a[i,4]);
    end; }
   {for i:=1 to n do
    writeln(s[i],' ',lan[i],' ',z[i]);}
   {for i:=1 to pinc do
    writeln(b[i].t);
   for jc:='a' to 'z' do
    write(h[jc],' ');
   writeln(h['z']);}
  end;
 begin
  assign(input,'string.in');
  assign(output,'string.out');
  reset(input);
  rewrite(output);
  randomize;
  readln(n);
  for i:=1 to n do
   readln(smove[i]);
  filldword(a,sizeof(a)>>2,0);
  for i:=1 to n do
   begin
    s[i]:=smove[i];
    lan[i]:=length(s[i]);
    z[i]:=i;
   end;
  //hahacheck;
  haha(1,n);
  //hahacheck;
  for i:=1 to n do
   for j:=1 to maxlan do
    begin
     a[i,j]:=a[i-1,j];//or (1<<(ord(s[i,j])-97));
     if j<=lan[i] then inc(a[i,j,s[i,j]]);
    end;
  filldword(w,sizeof(w)>>2,0);
  for i:=1 to n do
   begin
    o:=1;
    p:=n;
    filldword(h,sizeof(h)>>2,0);
    pinc:=0;
    for j:=1 to lan[i] do
     begin
      oc:=hahafize2(j,o,i,s[i,j]);
      pc:=hahafize(j,i,p,s[i,j]);
      for k:='a' to 'z' do
       if (a[oc-1,j,k]-a[o-1,j,k]>0) or (a[p,j,k]-a[pc,j,k]>0) then hahainc(s[i,j],k);
      //writeln(o,' ',p,' ',oc,' ',pc,' ',pinc);
      //writeln(a[0,2,'a'],' ',a[2,2,'a'],' ',a[1,2,'a']);
      o:=oc;
      p:=pc;
     end;
    //writeln;
    filldword(d,sizeof(d)>>2,0);
    filldword(f,sizeof(f)>>2,0);
    upass:=0;
    //hahacheck;
    for jc:='a' to 'z' do
     if f[jc]=0 then hahaDFS(jc);
    if upass=0 then w[z[i]]:=1;
   end;
  for i:=2 to n do
   if (pos(s[i-1],s[i])=1) and (lan[i]>lan[i-1]) then w[z[i]]:=0;
  ssum:=0;
  for i:=1 to n do
   inc(ssum,w[i]);
  writeln(ssum);
  for i:=1 to n do
   if w[i]=1 then writeln(smove[i]);
  close(input);
  close(output);
 end.
