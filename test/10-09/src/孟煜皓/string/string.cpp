#include <cstdio>
#include <cstring>
#include <algorithm>
int n, len[2005], tot, ans[2005], d[30];
char a[2005][25];
bool less[30][30];
int h, t, q[35];
bool toposort(){
	h = 0, t = 0;
	for (register int i = 0; i <= 25; ++i)
		for (register int j = 0; j <= 25; ++j)
			if (less[i][j]) ++d[j];
	for (register int i = 0; i <= 25; ++i) if (!d[i]) q[++t] = i;
	while (h < t)
		for (register int u = q[++h], v = 0; v <= 25; ++v)
			if (less[u][v] && !(--d[v])) q[++t] = v;
	return t == 26;
}
int main(){
	freopen("string.in", "r", stdin);
	freopen("string.out", "w", stdout);
	scanf("%d", &n);
	for (register int i = 1; i <= n; ++i)
		scanf("%s", a[i] + 1), len[i] = strlen(a[i] + 1);
	for (register int i = 1; i <= n; ++i)
		for (register int j = 1; j <= len[i]; ++j)
			a[i][j] -= 'a';
	for (register int i = 1; i <= n; ++i){
		memset(less, 0, sizeof less), memset(d, 0, sizeof d);
		int flag = 1;
		for (register int j = 1; j <= n; ++j)
			if (j != i){
				int k = 1;
				while (k <= len[i] && k <= len[j] && a[i][k] == a[j][k]) ++k;
				if (k > len[j] && k <= len[i]) { flag = 0; break; }
				if (k <= len[i] && k <= len[j]) less[a[i][k]][a[j][k]] = 1;
			}
		if (flag && toposort()) ans[++tot] = i;
	}
	printf("%d\n", tot);
	for (register int i = 1; i <= tot; ++i){
		for (register int j = 1; j <= len[ans[i]]; ++j) a[ans[i]][j] += 'a';
		printf("%s\n", a[ans[i]] + 1);
	}
}
