#include <cstdio>
#include <cstring>
#include <algorithm>
#define N 10005
#define M 1000005
#define INF (long long)(0x3f3f3f3f3f3f3f3f)
int n, m, sum, a[N], b[N], c[N], S = 0, T;
int edge, to[M], cap[M], cost[M], pr[M], hd[N];
void addedge(int u, int v, int w, int c){
	to[edge] = v, pr[edge] = hd[u], cap[edge] = w, cost[edge] = c, hd[u] = edge++;
	to[edge] = u, pr[edge] = hd[v], cap[edge] = 0, cost[edge] = -c, hd[v] = edge++;
}
int h, t, Q[M << 3], vis[N], mn[N], pre[N];
long long dis[N];
void spfa(){
	memset(dis, 0x3f, sizeof dis), memset(vis, 0, sizeof vis);
	h = 0, t = 1, Q[t] = S, dis[S] = 0, vis[S] = 1, pre[S] = 0, mn[S] = 1000000000;
	while (h < t){
		int u = Q[++h]; vis[u] = 0;
		for (register int i = hd[u], v; ~i; i = pr[i])
			if (cap[i] && dis[u] + cost[i] < dis[v = to[i]]){
				dis[v] = dis[u] + cost[i];
				mn[v] = std :: min(mn[u], cap[i]), pre[v] = i;
				if (!vis[v]) Q[++t] = v, vis[v] = 1;
			}
	}
}
long long MCMF(){
	long long ans = 0;
	while (spfa(), dis[T] != INF){
		ans += dis[T] * mn[T];
		for (register int i = T; i != S; i = to[pre[i] ^ 1])
			cap[pre[i]] -= mn[T], cap[pre[i] ^ 1] += mn[T];
	}
	return ans;
}
int abs(int a){
	return a > 0 ? a : -a;
}
int main(){
	freopen("hole.in", "r", stdin);
	freopen("hole.out", "w", stdout);
	scanf("%d%d", &n, &m), T = n + m + 1;
	for (register int i = 1; i <= n; ++i) scanf("%d", a + i);
	for (register int i = 1; i <= m; ++i) scanf("%d%d", b + i, c + i), sum += c[i];
	if (sum < n) return printf("Stupid game!"), 0;
	memset(hd, -1, sizeof hd);
	for (register int i = 1; i <= n; ++i)
		for (register int j = 1; j <= m; ++j)
			addedge(i, j + n, 1, abs(a[i] - b[j]));
	for (register int i = 1; i <= n; ++i) addedge(S, i, 1, 0);
	for (register int i = 1; i <= m; ++i) addedge(i + n, T, c[i], 0);
	printf("%lld", MCMF());
}
