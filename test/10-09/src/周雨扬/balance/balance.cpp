#include<bits/stdc++.h>
#define ll long long
using namespace std;
map<int,int> mp;
const int N=100005;
int a[N],n,nd;
ll val[N];
vector<int> vec[N];
bool vis[N];
int main(){
	freopen("balance.in","r",stdin);
	freopen("balance.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++) scanf("%d",&a[i]);
	for (int i=1;i<=n;i++) if (!mp[a[i]]) mp[a[i]]=++nd;
	ll ans=0,delta=0;
	for (int i=1;i<n;i++){
		ans+=abs(a[i]-a[i+1]);
		val[mp[a[i]]]+=abs(a[i]-a[i+1]);
		val[mp[a[i+1]]]+=abs(a[i]-a[i+1]);
		if (a[i]!=a[i+1]){
			vec[mp[a[i]]].push_back(a[i+1]);
			vec[mp[a[i+1]]].push_back(a[i]);
		}
	}
	for (int i=1;i<=n;i++) if (!vis[mp[a[i]]]){
		int id=mp[a[i]]; vis[id]=1;
		sort(vec[id].begin(),vec[id].end());
		ll v=-val[id];
		int md=vec[id].size()/2;
		for (int j=0;j<vec[id].size();j++)
			v+=abs(vec[id][j]-vec[id][md]);
		delta=min(delta,v);
	}
	printf("%lld\n",delta+ans);
}
