#include<bits/stdc++.h>
using namespace std;
int ch[1000005][26],ND=1;
int ed[1000005];
void insert(char *s,int len){
	int x=1;
	for (int i=1;i<=len;i++){
		int to=s[i]-'a';
		if (!ch[x][to])
			ch[x][to]=++ND;
		x=ch[x][to];
	}
	ed[x]=1;
}
int e[26][26],deg[26],q[30];
bool check(char *s,int len){
	memset(e,0,sizeof(e));
	memset(deg,0,sizeof(deg));
	int x=1;
	for (int i=1;i<=len;i++){
		if (ed[x]) return 0;
		for (int j=0;j<26;j++)
			if (ch[x][j]&&j!=s[i]-'a')
				if (!e[s[i]-'a'][j])
					deg[j]++,e[s[i]-'a'][j]=1;
		x=ch[x][s[i]-'a'];
	}
	int h=0,t=0;
	for (int i=0;i<26;i++)
		if (!deg[i]) q[++t]=i;
	while (h!=t){
		int x=q[++h];
		for (int i=0;i<26;i++) if (e[x][i])
			if (!(--deg[i])) q[++t]=i;
	}
	return t==26;
}
int n,cnt;
int ok[50005],len[50005];
char s[50005][22];
int main(){
	freopen("string.in","r",stdin);
	freopen("string.out","w",stdout);
	scanf("%d",&n);
	for (int i=1;i<=n;i++){
		scanf("%s",s[i]+1);
		len[i]=strlen(s[i]+1);
		insert(s[i],len[i]);
	}
	for (int i=1;i<=n;i++)
		if (check(s[i],len[i]))
			 cnt++,ok[i]=1;
	printf("%d\n",cnt);
	for (int i=1;i<=n;i++)
		if (ok[i]) printf("%s\n",s[i]+1);
}
/*
3
aba
aac
bac
*/
