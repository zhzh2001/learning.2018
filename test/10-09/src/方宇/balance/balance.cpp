#include<cstdio>
#include<cctype>
#include<cmath>
#include<cstring>
#include<iostream>
#include<algorithm>
#define LL long long
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define fy 20021218020021218
#define N 500010
using namespace std;
LL a[N],b[N],c[N],v[N],f[N],h[N];
LL n,m,i,j,k,l,r,ans,sum;
LL read()
{
	LL x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
void add(LL l,LL r){m++; a[m]=r; b[m]=c[l]; c[l]=m;}
LL check(LL a){if(a<0) return -a; return a;}
int main()
{
	freopen("balance.in","r",stdin);
	freopen("balance.out","w",stdout);
	n=read(); m=0; l=fy; r=0;
	rep(i,1,n) 
	{
		v[i]=read();
		if(l>v[i]) l=v[i];
		if(r<v[i]) r=v[i];
	}
	rep(i,1,n-1)
		if(v[i]!=v[i+1])
			add(v[i],v[i+1]),add(v[i+1],v[i]);
	ans=fy;
	rep(i,l,r)
		if(c[i])
		{
			m=0; sum=0;
			for(j=c[i];j;j=b[j]) f[++m]=a[j];
			sort(f+1,f+m+1);
			rep(j,1,n) 
				if(v[j]==i) h[j]=f[m/2+1];
				  else h[j]=v[j];
			rep(j,1,n-1) sum+=check(h[j]-h[j+1]);
			if(sum<ans) ans=sum;
		}
	printf("%lld",ans);
	return 0;
}
