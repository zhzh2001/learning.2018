#include<cstdio>
#include<cctype>
#include<cmath>
#include<cstring>
#include<iostream>
#include<algorithm>
#define LL long long
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define fy 20021218020021218
#define N 100010
#define M 50
using namespace std;
char a[N][M];
LL b[N],pre[N],nex[N],ans[N],h[N];
LL f[M][M];
LL n,m,i,j,k,l,r,sum;
LL read()
{
	LL x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
bool check()
{
	rep(i,1,26) h[i]=0;
	rep(i,1,26)
		rep(j,1,26) h[i]+=f[i][j];
	rep(i,1,26)
	{
		LL k=0;
		rep(j,1,26) if(!h[j]) k=j;
		if(k==0) return 0;
		h[k]=-1;
		rep(j,1,26)
			if(f[j][k]==1) h[j]--;
	}
	return 1;
}
int main()
{
	freopen("string.in","r",stdin);
	freopen("string.out","w",stdout);
	n=read(); 
	rep(i,1,n) scanf("%s",a[i]+1);
	rep(i,1,n) b[i]=strlen(a[i]+1);
	rep(i,1,n)
	{
		rep(j,0,30)
		  rep(k,0,30) f[j][k]=0;
		rep(j,0,n+1)
		{
			pre[j]=j-1;
			nex[j]=j+1;
		}
		nex[pre[i]]=nex[i];
		pre[nex[i]]=pre[i];
		rep(k,1,b[i])
		{
			if(nex[0]==n+1) break;
			for(j=nex[0];j!=n+1;j=nex[j])
				if(a[j][k]!=a[i][k])
				{
					f[a[i][k]-'a'+1][a[j][k]-'a'+1]=1;
					nex[pre[j]]=nex[j];
					pre[nex[j]]=pre[j];
				}
		}
		if(check()) ans[++sum]=i;
	}
	printf("%lld\n",sum);
	rep(i,1,sum)
	{
		rep(j,1,b[ans[i]]) printf("%c",a[ans[i]][j]);
		printf("\n");
	}
	return 0;
}
