#include<bits/stdc++.h>
using namespace std;
#define maxn 50010
struct node
{
	int cnt,size;
	int son[26];
	node(){cnt=0;size=0;memset(son,0,sizeof(son));}
}a[maxn*20];int root=1,tot=1;
inline int getnew(){tot++;a[tot]=node();return tot;}
inline void insert(string s)
{
	int cur=root;a[root].size++;
	for(int i=0;i<(int)s.length();i++)
	{
		if(a[cur].son[s[i]-'a']==0) a[cur].son[s[i]-'a']=getnew();
		cur=a[cur].son[s[i]-'a'];a[cur].size++;
	}
	a[cur].cnt++;
}
int id[26],perm[26],id_cnt;
//id->place in the permutation
//perm->constructed permutation
int ind[26];bool Edge[26][26];
queue<int> q;
inline bool toposort()
{
	while(q.size()) q.pop();
	for(int i=0;i<26;i++) if(ind[i]==0) q.push(i);
	while(q.size())
	{
		int x=q.front();q.pop();
		for(int i=0;i<26;i++) if(Edge[x][i])
		{
			ind[i]--;
			if(ind[i]==0) q.push(i);
		}
	}
	for(int i=0;i<26;i++) if(ind[i]) return 0;
	return 1;
}
inline bool check(string s)
{
	int cur=root;
	for(int i=0;i<(int)s.length();i++)
	{
		if(a[cur].cnt) return 0;
		cur=a[cur].son[s[i]-'a'];
	}
	memset(ind,0,sizeof(ind));memset(Edge,0,sizeof(Edge));
	cur=root;
	for(int i=0;i<(int)s.length();i++)
	{
		for(int j=0;j<26;j++) if(j!=s[i]-'a'&&a[a[cur].son[j]].size&&Edge[s[i]-'a'][j]==0){ind[j]++;Edge[s[i]-'a'][j]=1;}
		cur=a[cur].son[s[i]-'a'];
	}
	return toposort();
}
string s[maxn];int n;
string ans[maxn];int cnt;
int main()
{
	freopen("string.in","r",stdin);
	freopen("string.out","w",stdout);
	cin>>n;for(int i=1;i<=n;i++) cin>>s[i],insert(s[i]);
	for(int i=1;i<=n;i++) if(check(s[i])) ans[++cnt]=s[i];
	cout<<cnt<<endl;
	for(int i=1;i<=cnt;i++) cout<<ans[i]<<endl;
}
