#include<bits/stdc++.h>
using namespace std;
#define maxn 100001
#define int long long
inline int Abs(int x){return x<0?-x:x;}
int a[maxn],n;
int head[maxn],nxt[maxn<<1],val[maxn<<1],tot;
inline void init()
{
	scanf("%lld",&n);for(int i=1;i<=n;i++) scanf("%lld",a+i);
	for(int i=1;i<=n;i++)
	{
		if(i>1&&a[i]!=a[i-1]){nxt[++tot]=head[a[i]];val[tot]=a[i-1];head[a[i]]=tot;}
		if(i<n&&a[i]!=a[i+1]){nxt[++tot]=head[a[i]];val[tot]=a[i+1];head[a[i]]=tot;}
	}
}
int t[maxn<<1],cnt,sum[maxn<<1];
inline int solve(int x)
{
	cnt=0;for(int i=head[x];i;i=nxt[i]) t[++cnt]=val[i];
	sort(t+1,t+cnt+1);for(int i=1;i<=cnt;i++) sum[i]=sum[i-1]+t[i];
	int cur=0;
	for(int i=head[x];i;i=nxt[i]) cur+=Abs(x-val[i]);
	int res=1e18;
	for(int i=1;i<=cnt;i++)
	{
		int calleft=t[i]*i-sum[i];
		int calright=(sum[cnt]-sum[i])-t[i]*(cnt-i);
		res=min(res,calleft+calright);
	}
	return res-cur;
}
signed main()
{
	freopen("balance.in","r",stdin);
	freopen("balance.out","w",stdout);
	init();
	int kkk=0;
	for(int i=2;i<=n;i++) kkk+=Abs(a[i]-a[i-1]);
	int ans=kkk;
	for(int i=1;i<=100000;i++) ans=min(ans,kkk+solve(i));
	cout<<ans;
	return 0;
}
