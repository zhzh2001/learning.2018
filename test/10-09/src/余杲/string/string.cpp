#include<bits/stdc++.h>
using namespace std;
const int MAXN=500005;
bool can[MAXN],vis[30],a[30][30];
int n,tot,scc,dfn[30],low[30];
int top,ss[30];
char s[MAXN][30];
void dfs(int k){
	dfn[k]=low[k]=++tot;vis[k]=1;ss[++top]=k;
	for(int i=0;i<26;i++)
		if(a[k][i]){
			if(!dfn[i]){
				dfs(i);
				low[k]=min(low[k],low[i]);
			}else if(vis[i])low[k]=min(low[k],dfn[i]);
		}
	if(low[k]==dfn[k]){
		scc++;
		int u;
		do{
			u=ss[top--];
			vis[u]=0;
		}while(k!=u);
	}
}
bool check(int now){
	int len1=strlen(s[now]);
	for(int i=1;i<=n;i++)
		if(now^i){
			bool flag=0;
			int len2=strlen(s[i]);
			for(int j=0;j<min(len1,len2);j++)
				if(s[now][j]!=s[i][j]){
					flag=1;
					a[s[now][j]-'a'][s[i][j]-'a']=1;
					break;
				}
			if(!flag&&len1>len2)return 0;
		}
	tot=scc=top=0;
	memset(dfn,0,sizeof(dfn));
	memset(low,0,sizeof(low));
	memset(vis,0,sizeof(vis));
	for(int i=0;i<26;i++)
		if(!dfn[i])dfs(i);
	return scc==26;
}
int main(){
	freopen("string.in","r",stdin);
	freopen("string.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)scanf("%s",s[i]);
	int ans=0;
	for(int i=1;i<=n;i++){
		memset(a,0,sizeof(a));
		if(check(i)){
			ans++;
			can[i]=1;
		}
	}
	printf("%d\n",ans);
	for(int i=1;i<=n;i++)
		if(can[i])puts(s[i]);
}
