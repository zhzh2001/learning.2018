#include<bits/stdc++.h>
using namespace std;
#define gc c=getchar()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
const int MAXN=1e5;
int a[MAXN],b[MAXN],c[MAXN],num[MAXN];
int Abs(int x){
	return x<0?-x:x;
}
int main(){
	freopen("balance.in","r",stdin);
	freopen("balance.out","w",stdout);
	int n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	int ans=0;
	for(int i=2;i<=n;i++)ans+=Abs(a[i]-a[i-1]);
	for(int i=1;i<=n;i++){
		int Mx=0;
		if(a[i+1]!=a[i])Mx=max(Mx,a[i+1]);
		if(a[i-1]!=a[i])Mx=max(Mx,a[i-1]);
		if(Mx>=a[i])continue;
		num[a[i]]=max(num[a[i]],Mx);
		c[a[i]]=a[i]-num[a[i]];
	}
	int Mxid=0;
	for(int i=1;i<=MAXN;i++)
		if(c[i]>c[Mxid])Mxid=i;
	for(int i=1;i<=n;i++)
		if(a[i]==Mxid)a[i]=num[Mxid];
	int tmp=0;
	for(int i=2;i<=n;i++)tmp+=Abs(a[i]-a[i-1]);
	printf("%d",min(ans,tmp));
}
