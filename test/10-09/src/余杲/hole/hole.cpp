#include<bits/stdc++.h>
using namespace std;
#define gc c=getchar()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
#define int long long
const int MAXN=5005;
int Abs(int x){
	return x<0?-x:x;
}
int n,m,T,ans;
int a[MAXN],b[MAXN],c[MAXN],p[MAXN];
void dfs(int k,int dis){
	if(clock()-T>1500)return;
	if(dis>ans)return;
	if(k>n){
		ans=dis;
		return;
	}
	int Mn=1LL<<60,id=0;
	for(int i=1;i<=m;i++)
		if(c[i]&&Abs(a[k]-b[i])<Mn){
			Mn=Abs(a[k]-b[i]);
			id=i;
		}
	c[id]--;
	dfs(k+1,dis+Mn);
	c[id]++;
	for(int i=1;i<=m;i++)
		if(i!=id&&c[i]){
			c[i]--;
			dfs(k+1,dis+Abs(a[k]-b[i]));
			if(clock()-T>1500)return;
			c[i]++;
		}
}
signed main(){
	freopen("hole.in","r",stdin);
	freopen("hole.out","w",stdout);
	T=clock();
	n=read(),m=read();
	for(int i=1;i<=n;i++)a[i]=read();
	int tot=0;
	for(int i=1;i<=m;i++){
		b[i]=read(),c[i]=read();
		tot+=c[i];
	}
	if(tot<n)return puts("Stupid game!"),0;
	ans=1LL<<60;
	dfs(1,0);
	printf("%lld",ans);
}
