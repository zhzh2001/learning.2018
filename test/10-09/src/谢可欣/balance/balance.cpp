#include <iostream>
#include <cstdio>
#include <vector>
using namespace std;
inline int read(){int tot=1;char c=getchar();while(c!='-'&&(c<'0'||c>'9'))c=getchar();if(c=='-')tot=-1,c=getchar();
int sum=0;while(c<='9'&&c>='0')sum=sum*10+c-'0',c=getchar();return sum*tot;}
int a[100005],exi[100005],cl[100005],st[100005],vis[100005];
vector<int> v[100005];
int Abs(int x){
	if(x<0)return -x; return x;
}
int main()
{
	freopen("balance.in","r",stdin);
	freopen("balance.out","w",stdout);
	int n=read(),sum=0;
	for(int i=1;i<=n;i++){
		a[i]=read();
		if(i!=1)sum+=Abs(a[i]-a[i-1]);
	}
	int ans=sum;
//	cout<<ans<<endl;
	for(int i=1;i<=n;i++){
		v[a[i]].push_back(i);
		exi[a[i]]++;
	}
//	for(int i=1;i<=n;i++)cout<<exi[a[i]]<<" ";puts("");
	for(int i=1;i<=n;i++){
		int tmp=sum;
		if(cl[a[i]])continue;
		if(exi[a[i]]==1){
			if(i==1)tmp-=Abs(a[1]-a[2]);
			else if(i==n)tmp-=Abs(a[n]-a[n-1]);
			else{
				int pre=Abs(a[i]-a[i-1])+Abs(a[i]-a[i+1]);
				tmp=tmp+Abs(a[i-1]-a[i+1])-pre;
			}
			cl[a[i]]=1;
			ans=min(ans,tmp);
		}
		else{
			int top=0;
			for(int j=0;j<v[a[i]].size();j++){
				int k=v[a[i]][j];
				if(k-1>=1&&!vis[a[k-1]])st[++top]=a[k-1],vis[a[k-1]]=1;
				if(k+1<=n&&!vis[a[k+1]])st[++top]=a[k+1],vis[a[k+1]]=1;
			}
			for(int j=1;j<=top;j++){
				int pre=0,now=0;
				for(int k=0;k<v[a[i]].size();k++){
					int q=v[a[i]][k];
					if(q-1>=1)pre+=Abs(a[q-1]-a[q]),now+=(a[q-1]-j);
					if(q+1<=n)pre+=Abs(a[q+1]-a[q]),now+=(a[q+1]-j);
				}
				ans=min(ans,tmp+now-pre);
				vis[a[i]]=0;
			}
			cl[a[i]]=1;
		}
	}
	cout<<ans<<endl;
	return 0;
}
