#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=5010;
int n,m,a[N],f[N][N],s[N],q[N];
struct ppap{int x,v;}p[N];
inline bool cmp(ppap a,ppap b){return a.x<b.x;}
signed main()
{
	freopen("hole.in","r",stdin);
	freopen("hole.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=n;i++)a[i]=read();
	sort(a+1,a+n+1);int sum=0;
	for(int i=1;i<=m;i++){
		p[i].x=read();p[i].v=read();
		sum+=p[i].v;
	}
	sort(p+1,p+m+1,cmp);
	if(sum<n)return puts("Stupid game!")&0;
	for(int i=1;i<=n;i++)f[0][i]=1e18;
	for(int i=1;i<=m;i++){
		for(int j=1;j<=n;j++)s[j]=s[j-1]+abs(a[j]-p[i].x);
		int l=1,r=0;
		for(int j=0;j<=n;j++){
			while(l<=r&&q[l]<j-p[i].v)l++;
			while(l<=r&&f[i-1][q[r]]-s[q[r]]>=f[i-1][j]-s[j])r--;
			q[++r]=j;
			f[i][j]=f[i-1][q[l]]-s[q[l]]+s[j];
		}
	}
	writeln(f[m][n]);
	return 0;
}
