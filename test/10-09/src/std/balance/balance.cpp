#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#include <complex>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=1e5+10;
int a[N],n,ma;
vector<int>e[N];
signed main()
{
	freopen("balance.in","r",stdin);
	freopen("balance.out","w",stdout);
	n=read();
	int ans,sum=0;
	for(int i=1;i<=n;i++){
		a[i]=read();
		ma=max(ma,a[i]);
		if(i>1&&a[i]!=a[i-1]){
			sum+=abs(a[i]-a[i-1]);
			e[a[i-1]].push_back(a[i]);
			e[a[i]].push_back(a[i-1]);
		}
	}
	ans=sum;
	for(int i=0;i<=ma;i++)if(e[i].size()){
		int s=e[i].size();
		sort(e[i].begin(),e[i].end());
		int p=e[i][s/2],Sum=sum;
		for(int j=0;j<s;j++)Sum=Sum-abs(i-e[i][j])+abs(p-e[i][j]);
		ans=min(ans,Sum);
	}
	writeln(ans);
	return 0;
}
