#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
bool b[500010];
int n,nedge=0,p[2010],nex[2010],head[2010],rd[2010];
int nxt[500010][26],ans=0,cnt=0,anss[500010];
char s[50010][30];
inline void pre(){
	nedge=0;memset(nex,0,sizeof nex);
	memset(head,0,sizeof head);memset(rd,0,sizeof rd);
}
inline void addedge(int a,int b){
	p[++nedge]=b;nex[nedge]=head[a];head[a]=nedge;
}
inline void insert(int x){
	int len=strlen(s[x]+1),now=0;
	for(int i=1;i<=len;i++){
		int p=s[x][i]-'a';
		if(!nxt[now][p])nxt[now][p]=++cnt;
		now=nxt[now][p];
	}
	b[now]=1;
}
inline bool check(int x){
	int len=strlen(s[x]+1),now=0;
	for(int i=1;i<=len;i++){
		int p=s[x][i]-'a';
		if(b[now])return 0;
		for(int i=0;i<26;i++)if(p!=i&&nxt[now][i])addedge(p+1,i+1),rd[i+1]++;
		now=nxt[now][p];
	}
	return 1;
}
inline bool topo(){
	queue<int>q;for(int i=1;i<=26;i++)if(!rd[i])q.push(i);
	while(!q.empty()){
		int now=q.front();q.pop();
		for(int k=head[now];k;k=nex[k])if(rd[p[k]]){
			rd[p[k]]--;if(!rd[p[k]])q.push(p[k]);
		}
	}
	for(int i=1;i<=26;i++)if(rd[i])return 0;
	return 1;
}
int main()
{
	freopen("string.in","r",stdin);
	freopen("string.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++){
		scanf("%s",s[i]+1);
		insert(i);
	}
	for(int i=1;i<=n;i++){
		pre();
		if(check(i)&&topo())anss[++ans]=i;
	}
	printf("%d\n",ans);
	for(int i=1;i<=ans;i++)printf("%s\n",s[anss[i]]+1);
	return 0;
}