#include <iostream>
#include <cstdio>
#include <cmath>

#define ll long long
#define Max 100005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(ll x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(ll x){
	write(x);puts("");
}

ll n,ans,a[Max];

inline ll calc(ll x,ll y){
	ll sum=0,l,r;
	for(ll i=1;i<=n;i++){
		if(i!=1){
			if(a[i]==x)l=y;else l=a[i];
			if(a[i-1]==x)r=y;else r=a[i-1];
			sum+=abs(l-r);
		}
	}
	return sum;
}

int main(){
	freopen("balance.in","r",stdin);
	freopen("balance1.out","w",stdout);
	n=read();
	for(ll i=1;i<=n;i++){
		a[i]=read();
		if(i!=1)ans+=abs(a[i]-a[i-1]);
	}
//	cout<<ans<<endl;
	for(ll i=1;i<=n;i++){
		if(i!=1){
			ans=min(ans,calc(a[i],a[i-1]));
//			cout<<calc(a[i],a[i-1])<<endl;
		}
		if(i!=n){
			ans=min(ans,calc(a[i],a[i+1]));
//			cout<<calc(a[i],a[i+1])<<endl;
		}
	}
	writeln(ans);
	return 0;
}
