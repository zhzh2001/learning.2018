#include <algorithm>
#include <iostream>
#include <cstring>
#include <cstdio>
#include <cmath>

#define ll long long
#define Max 100005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(ll x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(ll x){
	write(x);puts("");
}

ll n,l,r,s,mid,ans,a[Max],b[Max*2],c[Max],f[Max],num[Max],sum[Max*2];

inline ll calc(ll x,ll y){
	ll now=0,ans=0;
	l=f[x];r=f[x+1]-1;
	ans=l-1;
	while(l<=r){
		mid=(l+r)>>1;
		if(b[mid]<=y){
			ans=mid;
			l=mid+1;
		}else{
			r=mid-1;
		}
	}
//	cout<<x<<" "<<y<<" "<<ans<<endl;
	now=s-c[x]+y*(ans-f[x]+1)-(sum[ans]-sum[f[x]-1])+(sum[f[x+1]-1]-sum[ans])-y*(f[x+1]-1-ans);
//	if(now==158572139)cout<<ans<<" ";
	return now;
}

int main(){
	freopen("balance.in","r",stdin);
	freopen("balance.out","w",stdout);
	n=read();
	for(ll i=1;i<=n;i++){
		a[i]=read();
		if(i!=1){
			ans+=abs(a[i]-a[i-1]);
			c[a[i]]+=abs(a[i]-a[i-1]);
			c[a[i-1]]+=abs(a[i]-a[i-1]);
		}
	}
	s=ans;
//	cout<<s<<endl;
	for(ll i=1;i<=n;i++){
		if(i!=1){
			if(a[i-1]!=a[i])num[a[i]]++;
		}
		if(i!=n){
			if(a[i+1]!=a[i])num[a[i]]++;
		}
	}
	f[0]=1;
	for(ll i=1;i<=100000;i++)f[i]=f[i-1]+num[i-1];
	memset(num,0,sizeof num);
	for(ll i=1;i<=n;i++){
		if(i!=1){
			if(a[i-1]!=a[i]){
				b[f[a[i]]+num[a[i]]]=a[i-1];
				num[a[i]]++;
			}
		}
		if(i!=n){
			if(a[i+1]!=a[i]){
				b[f[a[i]]+num[a[i]]]=a[i+1];
				num[a[i]]++;
			}
		}
	}
//	for(ll i=1;i<=n;i++){
//		cout<<a[i]<<" "<<f[a[i]]<<" "<<num[a[i]]<<endl;
//	}
	for(ll i=0;i<=100000;i++){
		sort(b+f[i],b+f[i]+num[i]);
	}
//	for(ll i=1;i<=f[100000];i++){
//		cout<<b[i]<<" ";
//	}
	for(ll i=0;i<=f[100000];i++){
		sum[i]=sum[i-1]+b[i];
	}
	for(ll i=1;i<=n;i++){
		if(i!=1){
			ans=min(ans,calc(a[i],a[i-1]));
//			if(calc(a[i],a[i-1])==109063122)cout<<a[i]<<" "<<a[i-1]<<" ";
//			cout<<calc(a[i],a[i-1])<<endl;
		}
		if(i!=n){
			ans=min(ans,calc(a[i],a[i+1]));
//			if(calc(a[i],a[i+1])==109063122)cout<<a[i]<<" "<<a[i+1]<<" ";
//			cout<<calc(a[i],a[i+1])<<endl;
		}
	}
	writeln(ans);
	return 0;
}
