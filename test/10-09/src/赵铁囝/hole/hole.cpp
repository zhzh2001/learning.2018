#include <algorithm>
#include <iostream>
#include <cstdio>
#include <cmath>

#define Max 5005

#define ll long long

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(ll x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(ll x){
	write(x);puts("");
}

struct Hole{
	ll s,h;
	inline bool operator<(const Hole&x)const{
		return s<x.s;
	}
}b[Max],c[Max];

ll n,m,ans,sum,now,num,a[Max];

int main(){
	freopen("hole.in","r",stdin);
	freopen("hole.out","w",stdout);
	n=read();m=read();
	for(ll i=1;i<=n;i++)a[i]=read();
	sort(a+1,a+n+1);
	for(ll i=1;i<=m;i++){
		b[i].s=read();
		b[i].h=read();
	}
	sort(b+1,b+m+1);
	ans=1e18;
	c[0].s=1e18;
	for(ll i=1;i<=m;i++){
		for(ll j=1;j<=m;j++)c[j]=b[j];
		num=1;
		for(ll j=1;j<=n;j++){
			if(a[j]>=b[i].s){
				num=j;
				break;
			}
		}
		sum=0;
		for(ll j=num;j<=n;j++){
			now=0;
			for(ll k=1;k<=m;k++){
				if(c[k].h&&abs(c[k].s-a[j])<abs(c[now].s-a[j])){
					now=k;
				}
			}
			c[now].h--;
			sum+=abs(c[now].s-a[j]);
		}
		for(ll j=1;j<num;j++){
			for(ll k=1;k<=m;k++){
				if(c[k].h&&abs(c[k].s-a[j])<abs(c[now].s-a[j])){
					now=k;
				}
			}
			c[now].h--;
			sum+=abs(c[now].s-a[j]);
		}
		ans=min(ans,sum);
	}	
	for(ll i=1;i<=m;i++){
		for(ll j=1;j<=m;j++)c[j]=b[j];
		num=1;
		for(ll j=1;j<=n;j++){
			if(a[j]>=b[i].s){
				num=j;
				break;
			}
		}
		sum=0;
		for(ll j=n;j>=num;j--){
			now=0;
			for(ll k=1;k<=m;k++){
				if(c[k].h&&abs(c[k].s-a[j])<abs(c[now].s-a[j])){
					now=k;
				}
			}
			c[now].h--;
			sum+=abs(c[now].s-a[j]);
		}
		for(ll j=num-1;j>=1;j--){
			for(ll k=1;k<=m;k++){
				if(c[k].h&&abs(c[k].s-a[j])<abs(c[now].s-a[j])){
					now=k;
				}
			}
			c[now].h--;
			sum+=abs(c[now].s-a[j]);
		}
		ans=min(ans,sum);
	}
	if(ans>1e12){
		puts("Stupid game!");
	}else{
		writeln(ans);
	}
	return 0;
}
