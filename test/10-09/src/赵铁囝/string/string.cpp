#include <iostream>
#include <cstring>
#include <cstdio>

#define Max 50005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

struct Tree{
	char now;
	bool flag;
	int a[26];
}t[Max*21];

int n,sum,now,cnt,root,size,vis[26];
char s[Max][21],b[200];
bool ans[Max];

inline void dfs(int k,int x,int&node){
	if(!node)node=++cnt;
	if(x==now){
		t[node].flag=true;
		return;
	}
	dfs(k,x+1,t[node].a[(int)(s[k][x+1]-'a')]);
}

/*inline void dfs2(int now){
	if(t[now].flag){
		for(int i=1;i<=size;i++){
			putchar(b[i]);
		}
		puts("");
	}
	for(int i=0;i<=25;i++){
		if(t[now].a[i]){
			b[++size]=i+'a';
			dfs2(t[now].a[i]);
			--size;
		}
	}
}*/

inline bool check(){
	for(int i=0;i<26;i++){
		for(int j=0;j<26;j++){
			if((vis[i]^(1<<j))<vis[i]){
				vis[i]|=vis[j];
			}
		}
	}
	for(int i=0;i<26;i++){
		for(int j=0;j<26;j++){
			if((vis[i]^(1<<j))<vis[i]){
				if((vis[j]^(1<<i))<vis[j]){
					return false;
				}
			}
		}
	}
	return true;
}

inline bool dfs3(int k,int x,int now){
	if(x==size)return true;
	if(t[now].flag)return false;
	for(int i=0;i<26;i++){
		if(t[now].a[i]&&i!=(int)(s[k][x+1]-'a')){
			vis[(int)(s[k][x+1]-'a')]|=1<<i;
		}
	}
	if(!check())return false;
	return dfs3(k,x+1,t[now].a[(int)(s[k][x+1]-'a')]);
}

int main(){
	freopen("string1.in","r",stdin);
	freopen("string.out","w",stdout);
	n=read();
	root=++cnt;
	for(int i=1;i<=n;i++){
		scanf("%s",s[i]+1);
		now=strlen(s[i]+1);
		dfs(i,0,root);
	}
	for(int i=1;i<=n;i++){
		memset(vis,0,sizeof vis);
		size=strlen(s[i]+1);
		if(dfs3(i,0,root)){
			ans[i]=true;
			sum++;
		}
	}
	writeln(sum);
	for(int i=1;i<=n;i++){
		if(ans[i]){
			puts(s[i]+1);
		}
	}
//	dfs2(root);
	return 0;
}
