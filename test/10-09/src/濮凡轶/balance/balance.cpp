#include <cmath>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>

using namespace std;

const int maxn = 100005;

typedef long long LL;

LL a[maxn];
LL sj[maxn];
LL xj[maxn];
LL gx[maxn];
bool vis_sj[maxn];
bool vis_xj[maxn];

int main()
{
	freopen("balance.in", "r", stdin);
	freopen("balance.out", "w", stdout);
	int n;
	scanf("%d", &n);
	LL maxx = 0;
	for(int i = 1; i <= n; ++i)
	{
		scanf("%lld", &a[i]);
		maxx = max(a[i], maxx);
	}
	a[0] = a[1];
	a[n + 1] = a[n];
	memset(sj, 0x3f, sizeof(sj));
	memset(xj, 0xff, sizeof(xj));
	memset(gx, 0, sizeof(gx));
	LL ans = 0;
	for(int i = 1; i <= n; ++i)
	{
		if(a[i-1] != a[i])
		{
			if(a[i-1] < a[i])
			{
				gx[a[i]]--;
				vis_xj[a[i]] = true;
				xj[a[i]] = max(xj[a[i]], a[i-1]);
			}
			else
			{
				gx[a[i]]++;
				vis_sj[a[i]] = true;
				sj[a[i]] = min(sj[a[i]], a[i-1]);
			}
		}
		if(a[i+1] != a[i])
		{
			if(a[i+1] < a[i])
			{
				gx[a[i]]--;
				vis_xj[a[i]] = true;
				xj[a[i]] = max(xj[a[i]], a[i+1]);
			}
			else
			{
				gx[a[i]]++;
				vis_sj[a[i]] = true;
				sj[a[i]] = min(sj[a[i]], a[i+1]);
			}
		}
		ans += (LL) abs(a[i] - a[i-1]);
	}
	LL Ans = ans;
	for(int i = 0; i <= maxx; ++i)
	{
//		cout << abs(gx[i]) * abs(i - xj[i]) << endl;
		if(vis_xj[i])
			Ans = min(Ans, ans - abs(gx[i]) * abs(i - xj[i]));
		if(vis_sj[i])
			Ans = min(Ans, ans - abs(gx[i]) * abs(sj[i] - i));
//		printf("%d %lld %lld %lld\n", i, gx[i], sj[i], xj[i]);
	}
//	puts("");
	printf("%lld", Ans);
//	fclose(stdin);
//	fclose(stdout);
	return 0;
}
