#include <cstring>
#include <string>
#include <vector>
#include <fstream>
#include <iostream>

using namespace std;

const int maxn = 50005;

int mp[30][30];
string ss[maxn];
//string sss[maxn];

struct Trie
{
	struct Node
	{
		Node* son[27];
		bool flag;

		Node()
		{
			flag = false;
			for(int i = 0; i < 27; ++i)
				son[i] = NULL;
		}
	};

	Node* root;
	Trie()
	{
		root = new Node();
	}

	inline void insert(string s)
	{
		Node* now = root;
		for(int i = 0; i < (int) s.length(); ++i)
		{
			int c = s[i] - 'a';
			if(now->son[c] == NULL)
				now->son[c] = new Node();
			now = now->son[c];
		}
		now->flag = true;
	}

	inline bool build(string s)
	{
		Node* now = root;
		memset(mp, 0, sizeof(mp));
		for(int i = 0; i < (int) s.length(); ++i)
		{
			if(now->flag)
				return false;
			int c = s[i] - 'a';
			for(int j = 0; j < 27; ++j)
				if(c != j && now->son[j] != NULL)
					mp[c][j] = true;
			now = now->son[c];
		}
		return true;
	}
} tr;

//bool in[maxn];

int dfn[27];
int low[27];
bool ins[27];
int sta[27];
int top;
int tim;

inline bool dfs(int now)
{
	dfn[now] = low[now] = ++tim;
	sta[++top] = now;
	ins[now] = true;
	for(int to = 0; to < 27; ++to)
	{
		if(mp[now][to])
		{
			if(!dfn[to])
			{
				if(dfs(to))
					return true;
				low[now] = min(low[to], low[now]);
				if(low[now] < dfn[now])
					return true;
			}
			else if(ins[to])
				return true;
//				low[now] = min(low[now], dfn[to]);
		}
	}
	ins[now] = false;
	top--;
	return false;
}

//inline void pan()
//{
//	for(int i = 0; i < 27; ++i)
//		for(int j = 0; j < 27; ++j)
//			for(int k = 0; k < 27; ++k)
//				mp[i][j] |= mp[i][k] & mp[k][j];
//}

bool flag[maxn];

int main()
{
	ifstream fin("string.in");
	ofstream fout("string.out");
	int n;
	fin >> n;
//	int minlen = 233;
	for(int i = 1; i <= n; ++i)
	{
		fin >> ss[i];
//		sss[i] = ss[i];
//		ss[i].push_back('z'+1);
		tr.insert(ss[i]);
//		minlen = min(minlen, (int) ss[i].length());
	}
//	for(int i = 1; i <= n; ++i)
//	{
//		if((int) ss[i].length() == minlen)
//		{
//			in[i] = true;
//		}
//	}
	int Ans = 0;
	for(int i = 1; i <= n; ++i)
	{
//		if(in[i])
//		{
		if(!tr.build(ss[i]))
			continue;
//		pan();
		if(ss[i] == "dkcniodq")
		{
			for(int i = 1; i <= 27; ++i)
			{
				for(int j = 1; j <= 27; ++j)
					cout << mp[i][j] << ' ';
				cout << '\n';
			}
		}
		memset(dfn, 0, sizeof(dfn));
		memset(low, 0, sizeof(low));
		memset(ins, 0, sizeof(ins));
		top = tim = 0;
		bool fff = true;
		for(int j = 0; j < 27; ++j)
		{
			if(!dfn[j] && dfs(j))
			{
				fff = false;
				break;
			}
		}
		if(fff)
		{
			flag[i] = true;
			Ans++;
		}
//		}
	}
	fout << Ans << '\n';
	for(int i = 1; i <= n; ++i)
		if(flag[i])
			fout << ss[i] << '\n';
	fin.close();
	fout.close();
	return 0;
}
