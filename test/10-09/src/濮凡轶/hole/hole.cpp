#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>

using namespace std;

typedef long long LL;

const LL maxn = 305;

int n, m;
LL qzh[maxn];
int a[maxn];
int b[maxn];
int c[maxn];
LL dp[maxn][maxn];

struct QD
{
	int b, c;
	inline bool operator < (const QD& other) const
	{ return this->b < other.b; }
} qd[maxn];

inline LL js(int l, int r, int k)
{
	if(l > r)
		return 0;
	int mid = lower_bound(a + 1, a + n + 1, qd[k].b) - a;
//	cout << "js = " << l << ' ' << r << ' ' << mid << endl;
	LL ans = 0LL;
	if(l <= mid)
		ans += (mid - l + 1) * a[mid] - (qzh[mid] - qzh[l-1]);
	if(r > mid)
		ans += qzh[r] - qzh[mid] - (r - mid) * a[mid];
	return ans;
}

int main()
{
	freopen("hole.in", "r", stdin);
	freopen("hole.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for(int i = 1; i <= n; ++i)
		scanf("%d", &a[i]);
	for(int i = 1; i <= m; ++i)
		scanf("%d%d", &qd[i].b, &qd[i].c);
	sort(a + 1, a + n + 1);
	sort(qd + 1, qd + m + 1);
//	cout << endl;
//	for(int i = 1; i <= n; ++i)
//		cout << a[i] << ' ';
//	cout << endl;
//	for(int i = 1; i <= m; ++i)
//		cout << qd[i].b << ' ' << qd[i].c << endl;
//	cout << endl;
	for(int i = 1; i <= n; ++i)
	{
		qzh[i] = a[i];
		qzh[i] += qzh[i-1];
	}
	memset(dp, 0x3f, sizeof(dp));
	dp[0][0] = 0;
	for(int i = 1; i <= m; ++i)
	{
		for(int j = 0; j <= n; ++j)
		{
			for(int k = 0; k <= min(j, qd[i].c); ++k)
			{
				dp[i][j] = min(dp[i][j], dp[i-1][j-k] + js(j - k + 1, j, i));
			}
//			cout << i << ' ' << j << ' ' << dp[i][j] << endl;
		}
	}
	printf("%lld", dp[m][n]);
	fclose(stdin);
	fclose(stdout);
	return 0;
}
