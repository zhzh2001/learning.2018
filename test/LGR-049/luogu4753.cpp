#include <iostream>
using namespace std;
const int N = 100005;
int a[N], ans[N];
bool vis[N];
int main()
{
	ios::sync_with_stdio(false);
	int n, m, s;
	cin >> n >> m >> s;
	for (int i = 1; i <= m; i++)
		cin >> a[i];
	a[++m] = n;
	int cc = 0;
	for (int i = 0, j = 0; i < m; i = j)
	{
		for (; j <= m && a[j] - a[i] < s; j++)
			;
		if (j > m)
		{
			cout << "NO\n";
			return 0;
		}
		ans[++cc] = j;
		vis[j] = true;
	}
	for (int i = m, j = m; i; i = j)
	{
		for (; j >= 0 && (a[i] - a[j] < s || vis[j]); j--)
			;
		if (j < 0)
		{
			cout << "NO\n";
			return 0;
		}
		ans[++cc] = j;
	}
	if (cc == m + 1)
	{
		cout << "YES\n";
		for (int i = 1; i <= cc; i++)
			cout << ans[i] << ' ';
		cout << endl;
	}
	else
		cout << "NO\n";
	return 0;
}