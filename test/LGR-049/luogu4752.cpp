#include <iostream>
#include <map>
using namespace std;
bool prime(long long p)
{
	for (long long i = 2; i * i <= p; i++)
		if (p % i == 0)
			return false;
	return true;
}
int main()
{
	ios::sync_with_stdio(false);
	int t;
	cin >> t;
	while (t--)
	{
		int n, m;
		cin >> n >> m;
		map<long long, int> mp;
		while (n--)
		{
			long long x;
			cin >> x;
			mp[x]++;
		}
		while (m--)
		{
			long long x;
			cin >> x;
			if (!--mp[x])
				mp.erase(x);
		}
		mp.erase(1);
		if (mp.size() == 1 && mp.begin()->second == 1 && prime(mp.begin()->first))
			cout << "YES\n";
		else
			cout << "NO\n";
	}
	return 0;
}