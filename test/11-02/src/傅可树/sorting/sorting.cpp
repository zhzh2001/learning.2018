#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
inline int read(){
	int x=0,f=1; char c=getchar();
	while (c<'0'||c>'9') {
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
void write(int x){
	if (x<0){
		putchar('-');
		x=-x;
	}
	if (x>=10) write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x); puts("");
}
const int N=1e5+5;
struct node{
	int c,id;
}ch[N],c[N],co[N],CH[N];
char s[N];
int m,K,D,n,tmp;
inline void init(){
	scanf("%s",s+1); n=strlen(s+1);
	for (int i=1;i<=n;i++) ch[i].c=s[i];
}
inline bool cmp(node A,node B){
	int tmp1=A.id%D,tmp2=B.id%D;
	if (tmp1==tmp2) return A.id<B.id;
		else return tmp1<tmp2;
}
inline void sol(){
	for (int i=1;i+K-1<=n;i++){
		tmp=i;
		for (int j=0;j<K;j++){
			c[j].id=j; c[j].c=ch[i+j].c;
		}
		sort(c,c+K,cmp);
		for (int j=0;j<K;j++){
			ch[i+j].c=c[j].c;
		}
	}
	for (int i=1;i<=n;i++){
		putchar(ch[i].c);
	} puts("");
}
inline void sub1(){
	for (int i=1;i<=m;i++){
		K=read(),D=read();
		sol();
	}
}
inline void sub2(){
	K=read(); D=read();
	for (int i=1;i<=n;i++){
		co[i].c=i;
	}
	for (int i=1;i+K-1<=n;i++){
		tmp=i;
		for (int j=0;j<K;j++){
			c[j].id=j; c[j].c=co[i+j].c;
		}
		sort(c,c+K,cmp);
		for (int j=0;j<K;j++){
			co[i+j].c=c[j].c;
		}
	}
	for (int i=1;i<=m;i++){
		for (int j=1;j<=n;j++){
			CH[j].c=ch[co[j].c].c;
			putchar(CH[j].c);
		} puts("");
		for (int j=1;j<=n;j++){
			ch[j].c=CH[j].c;
		}
		if (i!=m) K=read(),D=read();
	}
}
inline void solve(){
	m=read();
	if (m<=100) sub1();
		else sub2();
}
int main(){
	freopen("sorting.in","r",stdin); freopen("sorting.out","w",stdout);
	init(); solve();
	return 0;
}
