#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
typedef long long ll;
inline int read(){
	int x=0,f=1; char c=getchar();
	while (c<'0'||c>'9') {
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
void write(ll x){
	if (x>=10) write(x/10);
	putchar(x%10+'0');
}
inline void writeln(ll x){
	write(x); puts("");
}
const int N=1e5+5,INF=1e9;
const ll inf=1e18;
struct node{
	int v,r,t;
}b[N];
struct nod{
	ll mn;
	int son[2];
}a[N*30];
int T,root,tot,n,m;
ll dp[N],ans;
inline bool cmp(node A,node B){
	return A.v<B.v;
}
inline void init(){
	n=read(); m=read();
	for (int i=1;i<=n;i++){
		b[i]=(node){read(),read(),read()};
	}
	sort(b+1,b+1+n,cmp);
}
inline void clean(){
	ans=inf;
	root=tot=0; 
}
void update(int &k,int l,int r,int x,ll v){
	if (!k) k=++tot,a[k].mn=INF,a[k].son[0]=a[k].son[1]=0; a[k].mn=min(a[k].mn,v);
	if (l==r) return;
	int mid=(l+r)>>1;
	if (mid>=x) update(a[k].son[0],l,mid,x,v);
		else update(a[k].son[1],mid+1,r,x,v);
}
ll query(int k,int l,int r,int x,int y){
	if (!k) return inf;
	if (l==x&&r==y) {
		return a[k].mn;
	}
	int mid=(l+r)>>1;
	if (mid>=y) return query(a[k].son[0],l,mid,x,y);
		else if (mid<x) return query(a[k].son[1],mid+1,r,x,y);
			else return min(query(a[k].son[0],l,mid,x,mid),query(a[k].son[1],mid+1,r,mid+1,y));
}
inline void solve(){
	clean();
	for (int i=1;i<=n;i++){
		if (b[i].r==1) dp[i]=b[i].t;
			else dp[i]=b[i].t+query(root,1,INF,b[i].r,INF);
		update(root,1,INF,b[i].v,dp[i]);
		if (b[i].v>=m) ans=min(ans,dp[i]);
	}
	if (ans>=inf) puts("-1");
		else writeln(ans);
}
int main(){
	freopen("gift.in","r",stdin); freopen("gift.out","w",stdout);
	T=read();
	while (T--) init(),solve();
	return 0;
}
