#include<cstdio>
#include<algorithm>
#define int unsigned long long
using namespace std;
inline int read(){
	int x=0,f=1; char c=getchar();
	while (c<'0'||c>'9') {
		if (c=='-') f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
void write(int x){
	if (x<0){
		putchar('-');
		x=-x;
	}
	if (x>=10) write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x); puts("");
}
const int N=20,INF=1e9;
int T,ans,n,a[N],b[N];
inline void init(){
	n=read();
	for (int i=1;i<=n;i++){
		a[i]=read();
	}
}
void dfs(int x,int s){
	if (x==n+1) {
		ans=min(ans,s);
		return;
	}
	dfs(x+1,s^(b[x]));
	dfs(x+1,s|(b[x]));
	dfs(x+1,s&(b[x]));
}
int tmp;
inline void solve(){
	ans=0;
	for (int i=0;i<64;i++) {
		ans|=(1ll<<i);
	}
	tmp=ans;
	for (int i=0;i<(1<<n);i++){
		for (int j=1;j<=n;j++){
			if (i&(1<<j-1)){
				b[j]=tmp^a[j];
			}else{
				b[j]=a[j];
			}
		}
		dfs(2,b[1]);
	}
	writeln(ans);
}
signed main(){
	freopen("lucky.in","r",stdin); freopen("lucky.out","w",stdout);
	T=read();
	while (T--) init(),solve();
	return 0;
}
