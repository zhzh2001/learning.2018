#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<ctime>
#include<cstdlib>
#include<string>
#include<queue>
#include<cmath>
#include<set>
#include<map>
#include<bitset>
#include<vector>
#define re register
#define rep(x,a,b) for (re int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (re int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (re int x=hd[a];~x;x=nx[x])
#define LL long long
#define mo 1000000007
#define N 3100
using namespace std;
typedef unsigned long long ull;
inline LL read(){
    LL x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
int T,n;
ull a[10],ans;
vector<ull>t1,t2;
void solve(ull x){
	for(vector<ull>::iterator i=t1.begin();i!=t1.end();i++){
		t2.push_back((*i)|x);
		t2.push_back((*i)&x);
		t2.push_back((*i)^x);
	}
}
int main(){
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
	T=read();
	while(T--){
		n=read();
		rep(i,1,n)scanf("%llu",a+i);
		t1.clear();
		t1.push_back(a[1]);
		t1.push_back(~a[1]);
		rep(i,2,n){
			t2.clear();
			solve(a[i]);
			solve(~a[i]);
			t1.swap(t2);
		}
		ans=*t1.begin();
		for(vector<ull>::iterator i=t1.begin();i!=t1.end();i++){
			ans=min(ans,*i);
		}
		printf("%llu\n",ans);
	}
	return 0;
}
