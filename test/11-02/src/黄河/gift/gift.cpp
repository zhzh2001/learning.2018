#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<ctime>
#include<cstdlib>
#include<string>
#include<queue>
#include<cmath>
#include<set>
#include<map>
#include<bitset>
#include<vector>
#define re register
#define rep(x,a,b) for (re int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (re int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (re int x=hd[a];~x;x=nx[x])
#define LL long long
#define mo 1000000007
#define N 4500
#define inf 0x3f3f3f3f
using namespace std;
inline LL read(){
    LL x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
int T,n,m,tot,dian[N],dist[N],len[N][N],pos;
bool vis[N];
void Dijkstra(int s){
	memset(vis,0,sizeof vis);
	rep(i,1,tot) dist[i]=len[s][i];
	vis[s]=1;
	rep(i,1,tot-1){
		int v,Min=1<<30;
		rep(j,1,tot) if (!vis[j]&&dist[j]<Min)  
			Min=dist[j],v=j;
		vis[v]=1;                            
		rep(j,1,tot) if (!vis[j])        
			dist[j]=min(dist[j],dist[v]+len[v][j]);
	}
}
int main(){
	freopen("gift.in","r",stdin);
	freopen("gift.out","w",stdout);
	T=read();
	rep(_i,1,T){
		n=read(),m=read();
		tot=1;dian[tot]=1;
		memset(len,inf,sizeof(len));
		memset(dist,inf,sizeof(dist));
		pos=0;
		rep(i,1,n){
			int v=read(),r=read(),t=read();
		//	add(r[i],v[i],t[i]);
			int x=0,y=0;
			rep(j,1,tot) if (dian[j]==r) x=j;
			rep(j,1,tot) if (dian[j]==v) y=j;
			if (r==m&&!pos) pos=tot+1;
			if (v==m&&!pos) {
				if (!x) pos=tot+2;else pos=tot+1;
			}
			//if (x) pos--;
			if (!x) dian[++tot]=r,x=tot;
			if (!y) dian[++tot]=v,y=tot;
			len[x][y]=t;
		//	printf("%d %d %d\n",x,y,len[x][y]);
		}
		if (!pos) dian[++tot]=m,pos=tot;
		rep(i,1,tot)
			rep(j,1,tot)
			if (i!=j&&dian[i]>=dian[j]) len[i][j]=0;
	//	rep(i,1,tot){
	////		rep(j,1,tot) printf("%d ",len[i][j]);
	//		printf("\n");
	//	}
		Dijkstra(1);
		if (dist[pos]==inf) printf("-1\n");else printf("%d\n",dist[pos]);
	}
	return 0;
}
