#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<ctime>
#include<cstdlib>
#include<string>
#include<queue>
#include<cmath>
#include<set>
#include<map>
#include<bitset>
#include<vector>
#define re register
#define rep(x,a,b) for (re int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (re int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (re int x=hd[a];~x;x=nx[x])
#define LL long long
#define mo 1000000007
#define N 3100
using namespace std;
inline LL read(){
    LL x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
int n,m,k,d,to[N];
char s[N],_s[N];
struct Data{
	char ch;
	int pos,id;
}a[N];
struct node{
	char ch;
	int pos;
}b[N];
bool cmp(Data a,Data b){
	return (a.pos%d==b.pos%d)?(a.pos<b.pos):(a.pos%d<b.pos%d);
}
int main(){
	freopen("sorting.in","r",stdin);
	freopen("sorting.out","w",stdout);
	cin>>s; n=strlen(s);
	m=read();
	if (n>=200&&m>=200){
		k=read(),d=read();
		rep(i,0,n-1)b[i].ch=s[i],b[i].pos=i;
		rep(i,0,n-k){
			int len=0;
			rep(j,i,i+k-1)a[++len].ch=b[j].ch,a[len].pos=j-i,a[len].id=b[j].pos;
			sort(a+1,a+len+1,cmp);
			len=0;
			rep(j,i,i+k-1)s[j]=b[j].ch=a[++len].ch,b[j].pos=a[len].id;
		}
		printf("%s\n",s);
		rep(i,0,n-1)to[i]=b[i].pos;
		rep(i,1,m-1){
			rep(j,0,n-1)s[j]=b[to[j]].ch;
			printf("%s\n",s);
			rep(j,0,n-1)b[j].ch=s[j];
		}
		return 0;
	}
	rep(T,1,m){
		k=read(),d=read();
		rep(i,0,n-k){
			int len=0;
			rep(j,i,i+k-1)a[++len].ch=s[j],a[len].pos=j-i;
			sort(a+1,a+len+1,cmp);
			len=0;
			rep(j,i,i+k-1)s[j]=a[++len].ch;
		}
		printf("%s\n",s);
	}
	return 0;
}
