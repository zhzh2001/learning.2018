#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
#include<cctype>
#include<ctime>
#include<cstdlib>
#include<string>
#include<queue>
#include<cmath>
#include<set>
#include<map>
#include<bitset>
#include<vector>
#define re register
#define rep(x,a,b) for (re int x=int(a);x<=(int)(b);x++)
#define drp(x,a,b) for (re int x=int(a);x>=(int)(b);x--)
#define cross(x,a) for (re int x=hd[a];~x;x=nx[x])
#define LL long long
#define mo 1000000007
#define N 1000010
using namespace std;
inline LL read(){
    LL x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
int n,k,d;
char s[N],t[N];
int main(){
	freopen("sorting.in","r",stdin);
	freopen("sorting.out","w",stdout);
	scanf("%s",s+1);
	int len=strlen(s+1);
	n=read();
	rep(i,1,n){
		k=read(),d=read();
		int p=1;
		while(k+p-1<=len){
			int cnt=0;
			rep(j,p,p+d-1){
				int q=j;
				while (q<=k+p-1) 
					t[++cnt]=s[q],q+=d; 
			}
			rep(j,p,p+k-1) s[j]=t[j-p+1];
			++p;
		}
		printf("%s\n",s+1);
	}
	return 0;
}
