#include<cstdio>
#include<iostream>
using namespace std;
typedef unsigned long long ull;
#define gc c=getchar()
ull read(){
	ull x=0,f=1;char gc;
	for(;!isdigit(c);gc);
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
int n;
ull ans,tmp,a[105];
void dfs(int x){
	if(x>n){
		ans=min(ans,tmp);
		return;
	}
	ull now=tmp;
	tmp=now&a[x];
	dfs(x+1);
	tmp=now&(~a[x]);
	dfs(x+1);
	tmp=now;
}
int main(){
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
	for(int T=read();T;T--){
		ans=1LL<<63;
		n=read();
		for(int i=1;i<=n;i++)a[i]=read();
		tmp=a[1];dfs(2);
		tmp=~a[1];dfs(2);
		cout<<ans<<'\n';
	}
}
