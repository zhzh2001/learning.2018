#include<queue>
#include<cstdio>
#include<iostream>
#include<algorithm>
using namespace std;
#define gc c=getchar()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
const int MAXN=1e5+5;
struct Person{
	int v,r,t;
	void input(){
		v=read(),r=read(),t=read();
	}
	bool operator<(const Person b)const{
		return r<b.r;
	}
}p[MAXN];
long long f[MAXN];
int main(){
	freopen("gift.in","r",stdin);
	freopen("bf.out","w",stdout);
	for(int T=read();T;T--){
		int n=read(),m=read();
		for(int i=1;i<=n;i++)p[i].input();
		sort(p+1,p+n+1);
		p[0].v=1;
		for(int i=1;i<=n;i++)f[i]=1e18;
		for(int i=1;i<=n;i++)
			for(int j=0;j<i;j++)
				if(p[j].v>=p[i].r)f[i]=min(f[i],f[j]+p[i].t);
		long long ans=1e18;
		for(int i=1;i<=n;i++)
			if(p[i].v>=m)ans=min(ans,f[i]);
		if(ans==1e18)puts("-1");
		else printf("%lld\n",ans);
	}
}
