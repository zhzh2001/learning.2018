#include<queue>
#include<cstdio>
#include<iostream>
#include<algorithm>
using namespace std;
inline char _gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
#define gc c=_gc()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
const int MAXN=1e5+5;
struct Person{
	int v,r,t;
	void input(){
		v=read(),r=read(),t=read();
	}
	bool operator<(const Person b)const{
		return r<b.r;
	}
}p[MAXN];
long long f[MAXN];
struct cmp{
	bool operator()(int a,int b){
		return f[a]>f[b];
	}
};
priority_queue<int,vector<int>,cmp>q;
int main(){
	freopen("gift.in","r",stdin);
	freopen("gift.out","w",stdout);
	p[0].v=1;
	for(int T=read();T;T--){
		int n=read(),m=read();
		for(int i=1;i<=n;i++)p[i].input();
		sort(p+1,p+n+1);
		for(int i=1;i<=n;i++)f[i]=1e18;
		while(!q.empty())q.pop();
		q.push(0);
		for(int i=1;i<=n;i++){
			while(!q.empty()&&p[q.top()].v<p[i].r)q.pop();
			if(q.empty())break;
			f[i]=f[q.top()]+p[i].t;
			q.push(i);
		}
		long long ans=1e18;
		for(int i=1;i<=n;i++)
			if(p[i].v>=m)ans=min(ans,f[i]);
		if(ans==1e18)puts("-1");
		else printf("%lld\n",ans);
	}
}
