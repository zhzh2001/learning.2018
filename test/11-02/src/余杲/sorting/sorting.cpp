#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
#define gc c=getchar()
int read(){
	int x=0,f=1;char gc;
	for(;!isdigit(c);gc)if(c=='-')f=-1;
	for(;isdigit(c);gc)x=x*10+c-'0';
	return x*f;
}
#undef gc
const int MAXN=1e6;
char s[MAXN];
int k,d;
struct Data{
	int id,p;
}a[MAXN];
bool cmp(Data x,Data y){
	int a=x.p%d,b=y.p%d;
	if(a!=b)return a<b;
	return x.p<y.p;
}
int main(){
	freopen("sorting.in","r",stdin);
	freopen("sorting.out","w",stdout);
	scanf("%s",s);
	int n=strlen(s);
	for(int i=0;i<n;i++)a[i].id=i;
	for(int m=read();m;m--){
		k=read(),d=read();
		for(int i=0;i<n-k+1;i++){
			for(int j=i;j<=k+i;j++)a[j].p=j-i;
			sort(a+i,a+k+i,cmp);
		}
		for(int i=0;i<n;i++)putchar(s[a[i].id]);
		putchar('\n');
	}
}
