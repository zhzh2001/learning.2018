#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int N=100002;
struct node{
	int v,r,t;
}a[N];
ll mn[N<<2],ans,lz[N<<2];
int n,m,i,T,r,v,b[N<<1],c[N<<1],tot,cnt;
#define gc getchar
int rd(){
	int x=0,f=1;char c;
	do{c=gc();if(c=='-')f=-1;}while(c<'0'||c>'9');
	while('0'<=c&&c<='9')x=(x<<3)+(x<<1)+(c^48),c=gc();
	return x*f;
}
bool cmp(node a,node b){return a.r<b.r;}
#define mid ((l+r)>>1)
void up(int t){mn[t]=min(mn[t<<1],mn[t<<1|1]);}
void down(int t){
	if (lz[t]<1e17){
		mn[t<<1]=min(mn[t<<1],lz[t]);
		mn[t<<1|1]=min(mn[t<<1|1],lz[t]);
		lz[t<<1]=min(lz[t<<1],lz[t]);
		lz[t<<1|1]=min(lz[t<<1|1],lz[t]);
		lz[t]=1e17;
	}
}
void build(int t,int l,int r){
	if (l==r){
		mn[t]=(l==1?0:1e17);
		return;
	}
	build(t<<1,l,mid);
	build(t<<1|1,mid+1,r);
	up(t);
}
void Min(int t,int l,int r,int x,int y,ll val){
	if (x<=l && r<=y){
		mn[t]=min(mn[t],val);
		lz[t]=min(lz[t],val);
		return;
	}
	down(t);
	if (x<=mid) Min(t<<1,l,mid,x,y,val);
	if (mid<y) Min(t<<1|1,mid+1,r,x,y,val);
	up(t);
}
ll query(int t,int l,int r,int pos){
	if (l==r) return mn[t];
	down(t);
	if (pos<=mid) return query(t<<1,l,mid,pos);
	return query(t<<1|1,mid+1,r,pos);
}
int main(){
	freopen("gift.in","r",stdin);
	freopen("gift.out","w",stdout);
	for (T=rd();T--;){
		n=rd();m=rd();memset(lz,63,sizeof(lz));
		b[cnt=1]=m;
		for (i=1;i<=n;i++) a[i].v=b[++cnt]=rd(),a[i].r=b[++cnt]=rd(),a[i].t=rd();
		sort(b+1,b+cnt+1);
		c[tot=1]=b[1];
		for (i=2;i<=cnt;i++)
			if (b[i]!=b[i-1]) c[++tot]=b[i];
		sort(a+1,a+n+1,cmp);
		build(1,1,tot);
		for (i=1;i<=n;i++){
			r=lower_bound(c+1,c+tot+1,a[i].r)-c;
			v=lower_bound(c+1,c+tot+1,a[i].v)-c;
			Min(1,1,tot,1,v,query(1,1,tot,r)+a[i].t);
		}
		m=lower_bound(c+1,c+tot+1,m)-c;
		ans=query(1,1,tot,m);
		printf("%lld\n",ans<1e17?ans:-1);
	}
}
