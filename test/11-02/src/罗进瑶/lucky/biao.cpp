#include<bits/stdc++.h>
typedef long long ll;
typedef unsigned long long ull;
void setIO () {
	freopen("lucky.in", "r", stdin);
	freopen("lucky.out", "w", stdout);
}
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}
while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) x = -x, putchar('-');if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
using namespace std;
const int N = 123;
int n;
ull a[N], ans;
string num(ull x) {
	string res;
	while(x) {
		res = res + (char)(x % 10 + '0');
		x /= 10;
	}
	reverse(res.begin(), res.end());
	return res;
}
void dfs(int x,ull sum, string t) {
	if(x == n + 1) {
		if(sum == 0) cout<<t<<endl;
		ans = min(ans, sum);
		return;
	}
	if(x == 1) {
		dfs(x + 1, a[x], num(a[x]));
		dfs(x + 1, ~a[x], num(a[x]));
		return;
	}
	for(int i = 0; i < 2; ++i) {
		dfs(x + 1, a[x] ^ sum, t + "��" + num(a[x]));
		dfs(x + 1, a[x] | sum, t + "|" + num(a[x]));
		dfs(x + 1, a[x] & sum, t + "&" + num(a[x]));
		a[x] = ~a[x];		
	}
}
int main() {
//	setIO();
	int T = read();
	ull sa = 0;
	for(int i = 0; i < 64; ++i)
		sa += (1ll<<i);
	while(T--) {
		cin>>n;
		for(int i = 1; i <=n; ++i)
			cin>>a[i];
		ans = sa;
		dfs(1, 0, "");
		cout<<ans<<endl;
	}
	return 0;
}
