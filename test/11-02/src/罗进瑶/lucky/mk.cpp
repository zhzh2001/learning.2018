#include<bits/stdc++.h>
typedef long long ll;
typedef unsigned long long ull;
void setIO () {
	freopen("lucky.in", "w", stdout);
//	freopen("lucky.out", "w", stdout);
}
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}
while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) x = -x, putchar('-');if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
using namespace std;
const int N = 123;
int n;
ull random(ull x) {
	return (ull) rand() * rand() * rand() % x;
} 
int main() {
	setIO();
	srand(time(NULL));
	int T = 100;
	writeln(T);
	while(T--) {
		int n = 9;
		writeln(n);
		for(int i = 1; i <= n; ++i)
			printf("%llu ", random(1e9+7));
		puts("");
	}
	return 0;
}
