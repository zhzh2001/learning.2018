#include<bits/stdc++.h>
#define pb push_back
typedef long long ll;
void setIO () {
	freopen("sorting.in", "r", stdin);
	freopen("sorting.out", "w", stdout);
}
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}
while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) x = -x, putchar('-');if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
using namespace std;
const int N = 1e6 + 7;
char s[N];
int r[N], n, b[N], tot, t[N], tmp[N];
int op1[N], op2[N], Q;
inline void print() {
	for(int i = 0; i < n; ++i)
		putchar(s[r[i]]);
	puts("");
} 
void solve(int x, int d) {
	for(register int i = 0; i <= n - x; ++i) {
		tot = 0;
		for(register int j = i; j < i + d; ++j)
			for(register int k = j; k < i + x; k += d)
				b[tot++] = r[k];
		for(register int j = 0; j < tot; ++j)
			r[i + j] = b[j];
	}
}
namespace G1 {
	void main() {
		for(int i = 1; i <= Q; ++i) {
			solve(op1[i], op2[i]);
			print();
		}
	}
}
namespace G2 {
	void main() {
		solve(op1[1], op2[1]);
		print();
		for(int i = 0; i < n; ++i) t[i] = r[i];
		for(int i = 2; i <= Q; ++i) {
			for(int j = 0; j < n; ++j) tmp[j] = r[j];
			for(int j = 0; j < n; ++j) r[j] = tmp[t[j]];
			print();
		}
	}
}

int main() {
	setIO();
	scanf("%s", s);
	n = strlen(s);
	Q = read();
	bool fg2 = 1;
	for(register int i = 0; i < n; ++i) r[i] = i;
	for(int i = 1; i <= Q; ++i) {
		op1[i] = read(), op2[i] = read();
		if(i > 1) fg2 &= (op1[i] == op1[i - 1] && op2[i] == op2[i - 1]);
	}
	if(fg2 && Q) G2::main();
	else if(n <= 3000) G1::main();
	else puts("0");
	return 0;
}
