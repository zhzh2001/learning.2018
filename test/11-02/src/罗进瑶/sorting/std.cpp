#include<bits/stdc++.h>
#define pb push_back
typedef long long ll;
void setIO () {
	freopen("sorting.in", "r", stdin);
	freopen("sorting.out", "w", stdout);
}
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}
while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) x = -x, putchar('-');if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
using namespace std;
const int N = 1e6 + 7;
char s[N];
int r[N], n, b[N], tot;
void print() {
	for(int i = 0; i < n; ++i)
		putchar(s[r[i]]);
	puts("");
} 
void solve(int x, int d) {
	for(int i = 0; i <= n - x; ++i) {
		tot = 0;
		for(int j = i; j <= i + d - 1; ++j)
			for(int k = j; k <= i + x - 1; k += d)
				b[tot++] = r[k];
		for(int j = 0; j < tot; ++j)
			r[i + j] = b[j];
	}
}
int main() {
	setIO();
	scanf("%s", s);
	n = strlen(s);
	int Q = read();
	for(int i = 0; i < n; ++i) r[i] = i;
	while(Q--) {
		int x = read(), y = read();
		solve(x, y);
		print();
	}
	return 0;
}
