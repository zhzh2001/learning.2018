#include<bits/stdc++.h>
typedef long long ll;
void setIO () {
	freopen("gift.in", "r", stdin);
	freopen("gift.out", "w", stdout);
}
inline ll read() {ll x = 0;char ch = getchar(), w = 1;while(ch < '0' || ch > '9') {if(ch == '-') w = -1;ch = getchar();}
while(ch >= '0' && ch <= '9') {x = x * 10 + ch - '0';ch = getchar();}return x * w;}
void write(ll x) {if(x < 0) x = -x, putchar('-');if(x > 9) write(x / 10);putchar(x % 10 + '0');}
inline void writeln(ll x) {write(x);puts("");}
using namespace std;
const int N = 200000;
int n, m, b[N], tot;
struct Edge {
	int r, v, t;
}e[N];
namespace G1 {
	bool cmp(Edge _x, Edge _y) {
		return _x.r < _y.r;
	}
	void main() {
		sort(e + 1,e + 1 + n, cmp);
		int now = 1, j = 0, ans = 0;
		while(now < m) {
			int mx = 0;
			while(j < n && e[j + 1].r <= now) mx = max(mx, e[j + 1].v), ++j;
			if(mx == 0) {
				puts("-1");
				return;
			}
			now = mx;
			++ans;
		}
		writeln(ans);
	}
}
const ll inf = 0x3f3f3f3f3f3f3f3f;
namespace G2 {
	bool vis[N];
	ll d[N];
	#define pii pair<int,int>
	#define mk make_pair
	#define fi first
	#define nd second
	bool cmpv(Edge _x, Edge _y) {
		return _x.v > _y.v;
	}
	void main() {
		sort(e + 1,e + 1 + n, cmpv);
		b[++tot] = m;
		sort(b + 1, b + 1 + tot);
		tot = unique(b + 1, b  +1 + tot) - b - 1;
		if(b[1] != 1) {puts("-1"); return;}
		for(int i = 1; i <= n; ++i) e[i].r = lower_bound(b + 1, b + 1 + tot, e[i].r) - b;
		priority_queue<pii, vector<pii>, greater<pii> >q;
		memset(vis, 0, sizeof vis);
		for(int i = 1; i <= tot; ++i) d[i] = inf;
		int S = lower_bound(b + 1, b + 1 + tot, m) - b;
		q.push(mk(0, S));
		d[S] = 0;
		while(!q.empty()) {
			pii now = q.top(); q.pop();
			if(vis[now.nd]) continue;
			vis[now.nd] = 1;
			for(int i = 1; i <= n; ++i)
				if(e[i].v >= b[now.nd]) {
					if(d[e[i].r] > now.fi + e[i].t) {
						d[e[i].r] = now.fi + e[i].t;
						q.push(mk(d[e[i].r], e[i].r));
					}
				} else break;
		}
		if(d[1] == inf) puts("-1");
		else writeln(d[1]);
	}
}
int main() {
	setIO();
	int T = read();
	while(T--) {
		n = read(), m = read();
		tot = 0;
		bool fg1 = 1;
		for(int i = 1; i <= n; ++i)
			e[i].v = read(), e[i].r = read(), e[i].t = read(), fg1 &= (e[i].t == 1), b[++tot] = e[i].r;
		if(fg1) G1::main();
		else if(n <= 1500) G2::main();
		else puts("0");
	}
	return 0;
}
