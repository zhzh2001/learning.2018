#include <bits/stdc++.h>
#define int unsigned long long
using namespace std;
inline int read(){
	int t=0;bool p=0;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
	for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
	return p?-t:t;
}
const int N=105;
const int inf=((1ll<<64)-1ll);
int n,a[N][2],ans;
void dfs(int x,int y){
	if(x>n){ans=min(ans,y);return;}
	dfs(x+1,y|a[x][0]);
	dfs(x+1,y&a[x][0]);
	dfs(x+1,y|a[x][1]);
	dfs(x+1,y&a[x][1]);
}
signed main(){
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
	int T=read();
	while(T--){
		ans=inf;
		n=read();
		for(int i=1;i<=n;i++){
			a[i][0]=read();
			a[i][1]=inf^a[i][0];
		}
		dfs(2,a[1][0]);
		dfs(2,a[1][1]);
		cout<<ans<<'\n';
	}
	return 0;
}
