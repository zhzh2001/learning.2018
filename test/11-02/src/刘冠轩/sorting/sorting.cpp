#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int t=0;bool p=0;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
	for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
	return p?-t:t;
}
const int N=10005;
int n,m,k,d;
char str[N],s[N];
void sol(int l,int r){
	int K=k%d,P=k/d;
	for(int i=0;i<k;i++)s[i%d*P+i/d]=str[i+l];
	for(int i=0;i<k;i++)str[i+l]=s[i];
}
int main(){
	freopen("sorting.in","r",stdin);
	freopen("sorting.out","w",stdout);
	scanf("%s",str+1);
	m=read();n=strlen(str+1);
	while(m--){
		k=read();d=read();
		for(int i=k;i<=n;i++)sol(i-k+1,i);
		printf("%s\n",str+1);
	}
	return 0;
}
