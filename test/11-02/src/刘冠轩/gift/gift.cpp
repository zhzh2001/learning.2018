#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int t=0;bool p=0;char ch=getchar();
	for(;!isdigit(ch);ch=getchar())if(ch=='-')p=1;
	for(;isdigit(ch);ch=getchar())t=t*10+ch-48;
	return p?-t:t;
}
typedef long long ll;
const int N=100005;
const ll inf=1ll<<60;
int n,m,s[N*2],head[N*2],cnt,tot,ed;
ll dis[N*2];
bool vis[N*2];
struct node{int x,y;ll t;}E[N];
struct edge{int to,nt;ll t;}e[N*4];
map<int,int> mp;
void add(int u,int v,ll t){
	e[++cnt]=(edge){v,head[u],t};
	head[u]=cnt;
}
struct pt{
	int u;ll t;
	bool operator<(const pt &b)const{
		return t>b.t;
	}
}now;
priority_queue<pt> Q;
int main(){
	freopen("gift.in","r",stdin);
	freopen("gift.out","w",stdout);
	int T=read();
	while(T--){
		n=read();m=read();
		s[1]=1;s[2]=m;
		for(int i=1;i<=n;i++){
			s[i*2+1]=E[i].x=read();
			s[i*2+2]=E[i].y=read();
			E[i].t=read();
		}
		sort(s+1,s+n+n+3);
		for(int i=1;i<=n+n+2;i++)if(s[i]!=s[i-1]){
			mp[s[i]]=++tot;dis[tot]=inf;
		}
		for(int i=2;i<=tot;i++)add(i,i-1,0);
		for(int i=1;i<=n;i++)add(mp[E[i].y],mp[E[i].x],E[i].t);
		dis[1]=0;Q.push((pt){1,0});ed=mp[m];
		while(!Q.empty()){
			now=Q.top();Q.pop();
			if(now.t!=dis[now.u])continue;
			if(vis[now.u])continue;
			vis[now.u]=1;
			if(now.u==ed){while(!Q.empty())Q.pop();continue;}
			for(int i=head[now.u];i;i=e[i].nt)
				if(dis[e[i].to]>dis[now.u]+e[i].t){
					dis[e[i].to]=dis[now.u]+e[i].t;
					Q.push((pt){e[i].to,dis[e[i].to]});
				}
		}
		if(dis[mp[m]]<inf)cout<<dis[ed]<<'\n';else puts("-1");
		if(T){
			memset(head,0,sizeof(head));
			memset(vis,0,sizeof(vis));cnt=tot=0;
		}
	}
	return 0;
}
