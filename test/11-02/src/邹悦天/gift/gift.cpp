#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cctype>
#include <vector>
#include <set>
using namespace std;

namespace zuiyt
{
	template<typename T>
	inline void read(T &x)
	{
		char c;
		bool f = false;
		x = 0;
		do
			c = getchar();
		while (c != '-' && !isdigit(c));
		if (c == '-')
			f = true, c = getchar();
		do
			x = x * 10 + c - '0', c = getchar();
		while (isdigit(c));
		if (f)
			x = -x;
	}
	template<typename T>
	inline void write(T x)
	{
		static char buf[20];
		char *pos = buf;
		if (x < 0)
			putchar('-'), x = -x;
		do
			*pos++ = x % 10 + '0';
		while (x /= 10);
		while (pos > buf)
			putchar(*--pos);	
	}
	typedef long long ll;
	const int N = 1e5 + 10, INF = 0x3f3f3f3f;
	const ll LINF = 0x3f3f3f3f3f3f3f3fLL;
	struct fri
	{
		int v, r, t;
	}arr[N];
	struct edge
	{
		int to, w;	
	};
	vector<edge> g[N];
	int n, m, id[N];
	bool cmpv(const int i, const int j)
	{
		return arr[i].v < arr[j].v;
	}
	struct cmpr
	{
		bool operator () (const int i, const int j)
		{
			return arr[i].r == arr[j].r ? arr[i].v < arr[j].v : arr[i].r > arr[j].r;	
		}
	};
	ll f[N];
	ll dfs(const int u)
	{
		if (~f[u])
			return f[u];
		if (u == n + 1)
			return f[u] = 0;
		f[u] = LINF;
		for (int i = 0; i < g[u].size(); i++)
		{
			int v = g[u][i].to;
			f[u] = min(f[u], dfs(v) + g[u][i].w);
		}
		return f[u];
	}
	int work()
	{
		int T;
		read(T);
		while (T--)
		{
			static set<int, cmpr> s;
			read(n), read(m);
			s.clear();
			memset(f, -1, sizeof(ll[n + 2]));
			g[0].clear(), g[n + 1].clear();
			arr[0] = (fri){1, 0, 0}, arr[n + 1] = (fri){INF, m, 0};
			id[0] = 0, id[n + 1] = n + 1;
			for (int i = 1; i <= n; i++)
			{
				g[i].clear(), id[i] = i;
				read(arr[i].v), read(arr[i].r), read(arr[i].t);
			}
			sort(id, id + n + 2, cmpv);
			for (int i = n + 1; i >= 0; i--)
			{
				set<int, cmpr>::iterator it;
				arr[n + 2].r = arr[id[i]].v, arr[n + 2].v = -1;//The element for comparing
				for (it = s.lower_bound(n + 2); it != s.end(); it++)
					if (arr[id[i]].v < arr[*it].v)
						g[id[i]].push_back((edge){*it, arr[id[i]].t});
				s.insert(id[i]);
			}
			ll ans = dfs(0);
			if (ans < LINF)
				write(ans);
			else
				write(-1);
			putchar('\n');
		}
		return 0;	
	}	
}
int main()
{
	freopen("gift.in", "r", stdin);
	freopen("gift.out", "w", stdout);
	return zuiyt::work();	
}
