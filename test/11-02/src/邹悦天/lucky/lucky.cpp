#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cctype>
#include <climits>
using namespace std;

namespace zuiyt
{
	typedef unsigned long long ull;
	template<typename T>
	inline void read(T &x)
	{
		char c;
		bool f = false;
		x = 0;
		do
			c = getchar();
		while (c != '-' && !isdigit(c));
		if (c == '-')
			f = true, c = getchar();
		do
			x = x * 10 + c - '0', c = getchar();
		while (isdigit(c));
		if (f)
			x = -x;
	}
	inline void read(ull &x)
	{
		scanf("%llu", &x);
	}
	template<typename T>
	inline void write(T x)
	{
		static char buf[20];
		char *pos = buf;
		if (x < 0U)
			putchar('-'), x = -x;
		do
			*pos++ = x % 10U + '0';
		while (x /= 10U);
		while (pos > buf)
			putchar(*--pos);	
	}
	inline void write(const ull x)
	{
		printf("%llu", x);
	}
	const int N = 110;
	int n;
	ull arr[N], ans;
	bool rev[N];
	void dfs2(const int pos, const ull sum)
	{
		if (pos == n)
			ans = min(ans, sum);
		else
		{
			ull x = (rev[pos] ? ~arr[pos] : arr[pos]);
			dfs2(pos + 1, sum & x);
			dfs2(pos + 1, sum | x);
			dfs2(pos + 1, sum ^ x);
		}
	}
	void dfs1(const int pos)
	{
		if (pos == n)
			dfs2(1, rev[0] ? ~arr[0] : arr[0]);
		else
		{
			rev[pos] = true;
			dfs1(pos + 1);
			rev[pos] = false;
			dfs1(pos + 1);	
		}
	}
	bool check(const ull a, const int p)
	{
		return a & (1ULL << (ull)p);	
	}
	int work()
	{
		int T;
		read(T);
		while (T--)
		{
			ans = ULLONG_MAX;
			read(n);
			for (int i = 0; i < n; i++)
				read(arr[i]);
			if (n > 31)
			{
				write(0), putchar('\n');
				continue;	
			}
			if (n < 10)
			{
				dfs1(0);
				write(ans), putchar('\n');
				continue;
			} 
			for (ull i = 0; i < (1ULL << n); i++) // reverse or not
			{//wrong greed
				ull tmp = (check(i, 0) ? ~arr[0] : arr[0]);
				for (int j = 1; j < n; j++)
				{
					ull x = (check(i, j) ? ~arr[j] : arr[j]);
					tmp = min(tmp & x, tmp ^ x);
				}
				ans = min(ans, tmp);
			}
			write(ans), putchar('\n');
		}
		return 0;
	}
}
int main()
{
	freopen("lucky.in", "r", stdin);
	freopen("lucky.out", "w", stdout);
	return zuiyt::work();	
}
