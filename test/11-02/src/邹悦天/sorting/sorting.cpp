#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cctype>
using namespace std;

namespace zuiyt
{
	template<typename T>
	inline void read(T &x)
	{
		char c;
		bool f = false;
		x = 0;
		do
			c = getchar();
		while (c != '-' && !isdigit(c));
		if (c == '-')
			f = true, c = getchar();
		do
			x = x * 10 + c - '0', c = getchar();
		while (isdigit(c));
		if (f)
			x = -x;
	}
	inline void read(char *const s)
	{
		scanf("%s", s);
	}
	template<typename T>
	inline void write(T x)
	{
		static char buf[20];
		char *pos = buf;
		if (x < 0)
			putchar('-'), x = -x;
		do
			*pos++ = x % 10 + '0';
		while (x /= 10);
		while (pos > buf)
			putchar(*--pos);	
	}
	inline void write(char *const s)
	{
		printf("%s", s);
	}
	const int N = 1e6 + 10;
	char s[N], t[N];
	int nowd, id[N];
	bool cmpd(const int a, const int b)
	{
		return a % nowd == b % nowd ? a < b : a % nowd < b % nowd;
	}
	int work()
	{
		int n, m;
		read(s), read(m);
		n = strlen(s);
		while (m--)
		{
			int k, d;
			read(k), read(d);
			nowd = d;
			for (int i = 0; i <= n - k; i++)
			{
				memcpy(t, s + i, sizeof(char[k]));
				for (int j = 0; j < k; j++)
					id[j] = j;
				sort(id, id + k, cmpd);
				for (int j = 0; j < k; j++)
					s[i + j] = t[id[j]];
			}
			write(s), putchar('\n');
		}
		return 0;	
	}	
}
int main()
{
	freopen("sorting.in", "r", stdin);
	freopen("sorting.out", "w", stdout);
	return zuiyt::work();	
}
