#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
const int inf = 0x3f3f3f3f;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define gc getchar
#define pc putchar
#define fi first
#define rank Rankkk
#define se second
#define debug(x) cout << #x" = " << x << endl;
inline ll read(){
	register ll x=0,f=1;register char c=gc();
	for(;!isdigit(c);c=gc())if(c=='-')f=-1;
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}
#define rd read
void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);puts("");}
const int maxn = 1e5+233;
priority_queue<pair<ll,int> > Q;
struct Edge{
	int to,nxt,dist;
	Edge(){}
	Edge(int to,int nxt,int dist):
		to(to),nxt(nxt),dist(dist){}
}edge[maxn*2];
int first[maxn],nume;
int n,m,v[maxn],r[maxn],t[maxn];
int y[maxn];
ll dis[maxn];bool vis[maxn];
void Addedge(int a,int b,int c){
	edge[nume] = Edge(b,first[a],c);
	first[a] = nume++;
}
ll dijkstra(int S,int T,int n){
	Rep(i,1,n) dis[i] = 2e18,vis[i] = false;
	dis[S] = 0;
	Q.push(make_pair(-dis[S],S));
	while(!Q.empty()){
		int u = Q.top().se;Q.pop();
		if(vis[u]) continue;
		vis[u] = true;
		for(int e=first[u];~e;e=edge[e].nxt){
			int v=edge[e].to;
			if(dis[v] > dis[u] + edge[e].dist){
				dis[v] = dis[u] + edge[e].dist;
				Q . push(make_pair(-dis[v],v));
			}
		}
	}
	if(dis[T]==2e18) return -1; else return dis[T];
}
void solve(){
	mem(first,-1);nume = 0;
	n = read(),m = read();
	*y = 0;
	y[++*y] = 1;
	Rep(i,1,n){
		v[i] = read();
		r[i] = read();
		t[i] = read();
		y[++*y] = r[i];
	}
	y[++*y] = m;
	sort(y+1,y+1+*y);
	*y = unique(y+1,y+1+*y) - y - 1;
	Rep(i,1,n){
		v[i] = upper_bound(y+1,y+1+*y,v[i]) - y - 1;
		r[i] = lower_bound(y+1,y+1+*y,r[i]) - y;
		Addedge(r[i],v[i],t[i]);
	}
	Dep(i,*y,2) Addedge(i,i-1,0);
	int S,T;
	S = lower_bound(y+1,y+1+*y,1) - y;
	T = lower_bound(y+1,y+1+*y,m) - y;
	ll ans = dijkstra(S,T,*y);
	writeln(ans);
}
int main(){
	freopen("gift.in","r",stdin);
	freopen("gift.out","w",stdout);
	int T = read();
	while(T--) solve();
	return 0;
}
