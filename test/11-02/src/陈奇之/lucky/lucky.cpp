#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
const int inf = 0x3f3f3f3f;
#define Rep(i,a,b) for(register int i=(a);i<=int(b);++i)
#define Dep(i,a,b) for(register int i=(a);i>=int(b);--i)
#define rep(i,a,b) for(register int i=(a);i<int(b);++i)
#define mem(x,v) memset(x,v,sizeof(x))
#define gc getchar
#define pc putchar
#define fi first
#define rank Rankkk
#define se second
#define debug(x) cout << #x" = " << x << endl;
inline ll read(){
	register ll x=0,f=1;register char c=gc();
	for(;!isdigit(c);c=gc())if(c=='-')f=-1;
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);
	return x*f;
}
#define rd read
void write(ll x){if(x<0)x=-x,pc('-');if(x>=10)write(x/10);putchar(x%10+'0');}
void writeln(ll x){write(x);puts("");}
typedef unsigned long long ull;
const int maxn = 205;
int n;
ull a[maxn],x[maxn];
vector<ull> f,g;
void solve(){
	scanf("%d",&n);
	Rep(i,1,n) scanf("%llu",&a[i]);
	f.push_back(a[1]),f.push_back(~a[1]);
	Rep(i,2,n){
		g.clear();
		for(unsigned j=0;j<f.size();++j){
			ull w=f[j];
			g.push_back(w&a[i]);
			g.push_back(w&(~a[i]));
		}
		sort(g.begin(),g.end());
		g.erase(unique(g.begin(),g.end()),g.end());
		f = g;
	}
	writeln(f[0]);
}
int main(){
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
	int T;scanf("%d",&T);
	while(T--) solve();
	return 0;
}
