#include <bits/stdc++.h>
using namespace std;
#define int unsigned long long
inline int read(){int s=0; char c; while(!isdigit(c))c=getchar(); while(isdigit(c))s=s*10+c-'0',c=getchar();return s;}
inline void write(int x){char st[105],tp=0; if(x==0){putchar('0');return;}while(x){st[++tp]=(x%10+'0');x/=10;} while(tp){putchar(st[tp]);tp--;}}
const int N=105,B=20000005,inf=0x7fffffffffffffff;
int T,n,op[N],a[N],re,yy[B],Bin[N];
inline int inv(int x){int j,tp=0LL;for(j=64;~j;j--){if((x&Bin[j])==0)tp+=1LL*Bin[j];}return tp;}
inline void run(int x,int t,int la){if(x==t+1){yy[++*yy]=la;return;}int tmp=(op[x])?(inv(a[x])):a[x];if(la==-1)run(x+1,t,tmp);else{run(x+1,t,(tmp&la));run(x+1,t,(tmp|la));run(x+1,t,(tmp^la));}}
inline void dfs(int x,int s,int t){if(x==t+1){run(s,t,-1);return;} op[x]=1; dfs(x+1,s,t); op[x]=0; dfs(x+1,s,t);}
inline void run1(int x,int t,int la){if(x==t+1){int i;for(i=1;i<=*yy;i++)re=1LL*min(1LL*re,1LL*min((la&yy[i]),(la^yy[i])));return;}int tmp=(op[x])?(inv(a[x])):a[x];if(la==-1)run1(x+1,t,tmp);else{run1(x+1,t,(tmp&la));run1(x+1,t,(tmp|la));run1(x+1,t,(tmp^la));}}
inline void dfs1(int x,int s,int t){if(x==t+1){run1(s,t,-1);return;} op[x]=1; dfs1(x+1,s,t); op[x]=0; dfs1(x+1,s,t);}
inline void qcls(){sort(yy+1,yy+*yy+1); *yy=unique(yy+1,yy+*yy+1)-yy-1;}
inline void solve1(){dfs(1,1,n/2); qcls(); dfs1(n/2+1,n/2+1,n); write(re); putchar('\n');}
inline void solve2(){puts("0");}
signed main()
{
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
	int i; T=read(); Bin[0]=1LL;
	for(i=1;i<=64;i++)Bin[i]=(Bin[i-1]<<1LL);
	while(T--)
	{
		re=inf; *yy=0; n=read(); for(i=1;i<=n;i++)a[i]=read(); if(n<=12)solve1();else solve2();
	}
}
