#include <bits/stdc++.h>
#define LB lower_bound
#define int long long
inline int read(){int s=0; char c; while(!isdigit(c))c=getchar(); while(isdigit(c))s=s*10+c-'0',c=getchar();return s;}
using namespace std;
const int N=200005,inf=0x7ffffffffff,M=6000005;
int T,n,m,has[N],tot=0,Next[M],to[M],val[M],head[M],dis[N],bo[N];
struct node{int v,r,t;}a[N],b[N];
inline bool cmp(node a,node b){return a.r<b.r;}
inline void add(int x,int y,int z){Next[++tot]=head[x];to[tot]=y;val[tot]=z;head[x]=tot;}
struct rec{int x,w;};
inline bool operator<(const rec &p,const rec &q){return p.w>q.w;}
inline void Dijkstra(int s)
{
	int i,x; priority_queue<rec>q; q.push((rec){s,0}); memset(bo,0,sizeof bo); memset(dis,63,sizeof dis); dis[s]=0;
	while(!q.empty())
	{
		x=q.top().x; q.pop(); if(bo[x])continue; bo[x]=1;
		for(i=head[x];i;i=Next[i])
		{
			if(dis[to[i]]>dis[x]+val[i]){dis[to[i]]=dis[x]+val[i];q.push((rec){to[i],dis[to[i]]});}
		}
	}
}
inline void solve1()
{
	int i,j,re=inf;
	for(i=2;i<=*has;i++)
	{
		for(j=1;j<i;j++)add(i,j,0);
	}for(i=1;i<=n;i++)add(a[i].r,a[i].v,a[i].t);
	Dijkstra(1); for(i=*has;~i;i--)if(has[i]>=m)re=min(re,dis[i]);
	if(re==inf)re=-1;printf("%lld\n",re);
}
inline void solve2()
{
	int la=0,ma=1,tmp=1,re=0,laa=-1,maa=-1; sort(b+1,b+n+1,cmp);
	while(la<n)
	{
		if(ma>=m)break; if(ma==maa&&la==laa){re=-1;break;}
		while(b[la+1].r<=ma)
		{
			la++; tmp=max(tmp,b[la].v);
		}re++; laa=la; maa=ma; ma=max(ma,tmp); tmp=ma;
	}printf("%lld\n",re);
}
signed main()
{
	freopen("gift.in","r",stdin);
	freopen("gift.out","w",stdout);
	int i; T=read();
	while(T--)
	{
		n=read(); m=read(); tot=0; memset(head,0,sizeof head);
		for(i=1;i<=n;i++)
		{
			a[i].v=read(); a[i].r=read(); a[i].t=read(); has[++*has]=a[i].v; has[++*has]=a[i].r; b[i]=a[i];
		}has[++*has]=1; has[++*has]=m; sort(has+1,has+*has+1); *has=unique(has+1,has+*has+1)-has-1;
		for(i=1;i<=n;i++)
		{
			a[i].v=LB(has+1,has+*has+1,a[i].v)-has,a[i].r=LB(has+1,has+*has+1,a[i].r)-has;
		}if(n<=1500)solve1();else solve2();
	}
}
