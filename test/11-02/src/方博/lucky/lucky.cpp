#include<bits/stdc++.h>
#define ll unsigned long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x>9)write(x/10);
	putchar('0'+x%10);
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
using namespace std;
const int N=105;
ll a[N];
int f[65];
int n;
ll ans;
inline ll check()
{
	ll ret=a[1];
	for(int i=2;i<=n;i++)
		ret&=a[i];
	return ret;
}
void dfs(int k)
{
	if(k>n){
		ans=min(ans,check());
		return;
	}
	dfs(k+1);
	a[k]=~(a[k]);
	dfs(k+1);
	a[k]=~(a[k]);
}
signed main()
{
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
	srand(time(0));
	int T=read();
	while(T--){
		memset(f,0,sizeof(f));
		n=read();
		for(int i=1;i<=n;i++)
			a[i]=read();
		if(n>=64){
			puts("0");
			continue;
		}
		ans=1e18;
		if(n<=17)dfs(1);
		ll tim=2;
		while(tim--){
			if(tim==0)
				for(int i=1;i<=n/2;i++)
					swap(a[i],a[n-i+1]);
			memset(f,0,sizeof(f));
			for(int i=1;i<=n;i++)
				for(int j=0;j<=63;j++)
					if((1LL<<j)&a[i])
						f[j]++;
			for(int j=63;j>=0;j--){
				if(f[j]==n){
					for(int i=1;i<=n;i++){
						bool b=true;
						for(int k=63;k>j;k--){
							if(f[k]==n-1&&(a[i]&(1LL<<k))==0){
								b=false;
								break;
							}
						}
						if(b){
							for(int k=63;k>=0;k--)
								if(a[i]&(1LL<<k))f[k]--;
							a[i]=(~a[i]);
							for(int k=63;k>=0;k--)
								if(a[i]&(1LL<<k))f[k]++;
							break;
						}
					}
				}
			}
			ll ret=0;
			for(int j=0;j<=63;j++)
				if(f[j]==n)ret+=(1LL<<j);
			ans=min(ans,ret);
		}
		writeln(ans);
	}
}
/*
1
3
45285
61914
6892

1
*/
