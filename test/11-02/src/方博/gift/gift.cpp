#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar('0'+x%10);
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
ll T,n,m;
const ll M=1e15;
const int N=200005;
struct xxx{
	ll v,r,t;
}a[N];
ll p[N],f[N];
map<int,int>mmp;
ll cnt,tot;
inline bool com(xxx a,xxx b){if(a.r==b.r)return a.v<b.v;return a.r<b.r;}
struct tree{
	ll mn;
}t[1000003];
void build(int k,int l,int r)
{
	if(l==r){
		t[k].mn=M;
		return;
	}
	int mid=(l+r)>>1;
	build(k<<1,l,mid);
	build(k<<1|1,mid+1,r);
	t[k].mn=min(t[k<<1].mn,t[k<<1|1].mn);
}
ll query(int k,int l,int r,int x,int y)
{
	if(l==x&&r==y)return t[k].mn;
	int mid=(l+r)>>1;
	if(y<=mid)return query(k<<1,l,mid,x,y);
	else if(x>mid)return query(k<<1|1,mid+1,r,x,y);
	else return min(query(k<<1,l,mid,x,mid),query(k<<1|1,mid+1,r,mid+1,y));
}
void change(int k,int l,int r,int x,ll v)
{
	if(l==r){
		t[k].mn=min(v,t[k].mn);
		return;
	}
	int mid=(l+r)>>1;
	if(x<=mid)change(k<<1,l,mid,x,v);
	else change(k<<1|1,mid+1,r,x,v);
	t[k].mn=min(t[k<<1].mn,t[k<<1|1].mn);
}
int main()
{
	freopen("gift.in","r",stdin);
	freopen("gift.out","w",stdout);
	T=read();
	while(T--){
		n=read(),m=read();
		cnt=0,tot=0;
		for(register int i=1;i<=n;++i){
			a[i].v=read();a[i].r=read();a[i].t=read();
			p[++tot]=a[i].r;p[++tot]=a[i].v;
		}
		p[++tot]=m,p[++tot]=1;
		sort(p+1,p+tot+1);
		for(register int i=1;i<=tot;++i)
			if(i==1||p[i]!=p[i-1])mmp[p[i]]=++cnt;
		sort(a+1,a+n+1,com);
		build(1,1,cnt);
		change(1,1,cnt,1,0);
		for(register int i=1;i<=n;++i){
			int x=mmp[a[i].r];
			f[i]=query(1,1,cnt,x,cnt)+a[i].t;
			change(1,1,cnt,mmp[a[i].v],f[i]);
		}
		ll ans=query(1,1,cnt,mmp[m],cnt);
		if(ans<M)writeln(ans);
		else writeln(-1);
	}
	return 0;
}
