#include <iostream>
#include <algorithm>
#include <cstdio>
#include <vector>
using namespace std;
typedef unsigned long long ull;
int T,n;
ull a[10],ans;
vector<ull>t1,t2;
void solve(ull x){
	for(vector<ull>::iterator i=t1.begin();i!=t1.end();i++){
		t2.push_back((*i)|x);
		t2.push_back((*i)&x);
		t2.push_back((*i)^x);
	}
}
int main(){
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
	scanf("%d",&T);
	while(T-->0){
		scanf("%d",&n);
		for(int i=1;i<=n;i++)scanf("%llu",a+i);
		t1.clear();
		t1.push_back(a[1]);
		t1.push_back(~a[1]);
		for(int i=2;i<=n;i++){
			t2.clear();
			solve(a[i]);
			solve(~a[i]);
			t1.swap(t2);
		}
		ans=*t1.begin();
		for(vector<ull>::iterator i=t1.begin();i!=t1.end();i++){
			ans=min(ans,*i);
		}
		printf("%llu\n",ans);
	}
	return 0;
}
