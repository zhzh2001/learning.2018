#include <iostream>
#include <algorithm>
#include <cstdio>
using namespace std;
typedef long long LL;
#define dd c=getchar()
inline int read(){
	int a=0,b=1,dd;
	while(!isdigit(c)){if(c=='-')b=-b;dd;}
	while(isdigit(c)){a=a*10+c-'0';dd;}
	return a*b;
}
#undef dd
const LL oo=1LL<<60;
int T,n,m;
LL tot,ans;
struct st{
	int r,v,t;
	st(){r=v=t=0;}
	st(int _r,int _v,int _t){
		r=_r;
		v=_v;
		t=_t;
	}
}a[100003];
bool operator<(st a,st b){
	return a.r<b.r;
}
void Min(LL&a,const LL&b){
	a=min(a,b);
}
#define ls (nod<<1)
#define rs (nod<<1|1)
LL tree[400003];
void pushup(int nod){
	tree[nod]=min(tree[ls],tree[rs]);
}
void build(int nod,int l,int r){
	if(l==r){
		tree[nod]=oo;
		return;
	}
	int mid=(l+r)/2;
	build(ls,l,mid);
	build(rs,mid+1,r);
	pushup(nod);
}
void ins(int nod,int l,int r,int x,LL y){
	if(l==r){
		Min(tree[nod],y);
		return;
	}
	int mid=(l+r)/2;
	if(x<=mid)ins(ls,l,mid,x,y);
	else ins(rs,mid+1,r,x,y);
	pushup(nod);
}
LL query(int nod,int l,int r,int x,int y){
	if(l==x&&r==y){
		return tree[nod];
	}
	int mid=(l+r)/2;
	LL ans=oo;
	if(x<=mid)Min(ans,query(ls,l,mid,x,min(y,mid)));
	if(y>mid)Min(ans,query(rs,mid+1,r,max(x,mid+1),y));
	return ans;
}
#undef ls
#undef rs
int lh(int x){
	if(x>=a[n].r)return n;
	return upper_bound(a+1,a+n+1,st(x,0,0))-a-1;
}
int main(){
	freopen("gift.in","r",stdin);
	freopen("gift.out","w",stdout);
	T=read();
	while(T-->0){
		n=read();
		m=read();
		if(m<=1){
			puts("0");
			continue;
		}
		for(int i=1;i<=n;i++){
			a[i].v=read();
			a[i].r=read();
			a[i].t=read();
		}
		sort(a+1,a+n+1);
		build(1,1,n);
		if(a[1].r==1){
			ins(1,1,n,lh(1),0);
		}else{
			puts("-1");
			continue;
		}
		ans=oo;
		for(int i=1;i<=n;i++){
			tot=query(1,1,n,i,n)+a[i].t;
			if(a[i].v>=m)Min(ans,tot);
			ins(1,1,n,lh(a[i].v),tot);
		}
		if(ans>=oo)puts("-1");
		else printf("%lld\n",ans);
	}
	return 0;
}
