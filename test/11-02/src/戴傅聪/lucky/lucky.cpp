#include<bits/stdc++.h>
using namespace std;
#define ull unsigned long long
ull a[110];int n;
const ull inf=18446744073709551615u;
ull f[110][2];
inline void solve()
{
	scanf("%d",&n);
	for(int i=1;i<=n;i++) scanf("%llu",a+i);
	f[1][1]=a[1]^inf;f[1][0]=a[1];
	for(int i=2;i<=n;i++)
	{
		f[i][1]=min(f[i-1][0]&(a[i]^inf),f[i-1][1]&(a[i]^inf));
		f[i][0]=min(f[i-1][0]&a[i],f[i-1][1]&a[i]);
	}
	printf("%llu\n",min(f[n][0],f[n][1]));
}
int main()
{
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
	int T;scanf("%d",&T);
	while(T--) solve();
	return 0;
}
