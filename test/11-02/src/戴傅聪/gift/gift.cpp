#include<bits/stdc++.h>
using namespace std;
#define int long long
#define maxn 500100
#define maxm 3000100
inline char gc()
{
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline int read(int &x)
{
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
int head[maxn],ver[maxm],val[maxm],nxt[maxm],tot;
inline void clear(){tot=0;memset(head,0,sizeof(head));}
inline void addedge(int a,int b,int c){nxt[++tot]=head[a];ver[tot]=b;val[tot]=c;head[a]=tot;}
int lsh[maxn],lcnt,Tlsh[maxn],Tcnt;
int n,m;
int v[maxn],r[maxn],t[maxn];
inline void init()
{
	read(n);read(m);lcnt=1;lsh[1]=1;
	for(register int i=1;i<=n;i++) read(v[i]),read(r[i]),read(t[i]),lsh[++lcnt]=v[i];
	sort(lsh+1,lsh+lcnt+1);Tcnt=0;
	for(register int i=1;i<=lcnt;i++) if(i==1||lsh[i]!=lsh[i-1]) Tlsh[++Tcnt]=lsh[i];
	lcnt=Tcnt;
	for(register int i=1;i<=lcnt;i++) lsh[i]=Tlsh[i];
	for(register int i=1;i<=n;i++) r[i]=lower_bound(lsh+1,lsh+lcnt+1,r[i])-lsh,v[i]=lower_bound(lsh+1,lsh+lcnt+1,v[i])-lsh;
	m=lower_bound(lsh+1,lsh+lcnt+1,m)-lsh;
}
#define ls (p<<1)
#define rs (p<<1|1)
int vcnt=0,id[maxn];
inline void build_tree(int p,int l,int r)
{
	id[p]=++vcnt;if(l==r) return (void)addedge(l,id[p],0);
	int mid=(l+r)>>1;
	build_tree(ls,l,mid);addedge(id[ls],id[p],0);
	build_tree(rs,mid+1,r);addedge(id[rs],id[p],0);
}
inline void Addedge(int p,int l,int r,int ql,int qr,int to,int val)
{
	if(ql<=l&&qr>=r) return (void)addedge(id[p],to,val);
	int mid=(l+r)>>1;
	if(ql<=mid) Addedge(ls,l,mid,ql,qr,to,val);
	if(qr>mid) Addedge(rs,mid+1,r,ql,qr,to,val);
}
int dis[maxn];bool vis[maxn];
priority_queue<pair<int,int>,vector<pair<int,int> >,greater<pair<int,int> > > q;
inline void dijkstra()
{
	memset(vis,0,sizeof(vis));
	memset(dis,0x3f,sizeof(dis));
	while(q.size()) q.pop();
	dis[1]=0;q.push(make_pair(0,1));
	while(q.size())
	{
		register int x=q.top().second,d=q.top().first;q.pop();
		vis[x]=1;if(d!=dis[x]) continue;
		for(register int i=head[x];i;i=nxt[i])
		{
			register int y=ver[i],ndis=d+val[i];
			if(vis[y]||ndis>=dis[y]) continue;
			dis[y]=ndis;
			q.push(make_pair(ndis,y));
		}
	}
	int ans=1e18;
	for(int i=m;i<=lcnt;i++) ans=min(ans,dis[i]);
	if(ans==1e18) puts("-1");
	else printf("%lld\n",ans);
}
inline void solve()
{
	clear();init();
	vcnt=lcnt;
	build_tree(1,1,lcnt);
	for(register int i=1;i<=n;i++) Addedge(1,1,lcnt,r[i],lcnt,v[i],t[i]);
	dijkstra();
}
signed main()
{
	freopen("gift.in","r",stdin);
	freopen("gift.out","w",stdout);
	int T;read(T);
	while(T--) solve();
	return 0;
}
/*
3
5 9
5 1 1
10 4 10
8 1 10
11 6 1
7 3 8
4 5
2 1 1
3 2 1
4 3 1
8 4 1
3 10
5 1 3
8 2 5
10 9 2
*/
