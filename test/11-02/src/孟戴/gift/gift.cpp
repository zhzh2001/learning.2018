#include<algorithm>
//#include<iostream>
#include<string.h>
#include<fstream>
#include<stdio.h>
#include<math.h>
#include<map>
using namespace std;
#define cin fin
#define cout fout
ifstream fin("gift.in");
ofstream fout("gift.out");
inline long long read(){
	long long x=0,f=0;char ch=getchar();
	while(ch<'0'||ch>'9')f|=(ch=='-'),ch=getchar();
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+(ch-48),ch=getchar();
	return f?-x:x;
}
map<int,int>mp;
long long tim,tot;
long long T,n,m;
struct person{
	long long v,r,t;
}p[400003];
long long rr[400003],f[400003];
inline bool cmp(person a,person b){
	if(a.r==b.r) return a.v<b.v;
	return a.r<b.r;
}
struct tree{
	long long mn;
}t[1000003];
inline long long min(long long &a,long long &b){
	if(a<b) return a;
	return b;
}
void build(int rt,int l,int r){
	t[rt].mn=1e15;
	if(l==r){
		return;
	}
	int mid=l+r>>1;
	build(rt<<1,l,mid),build(rt<<1|1,mid+1,r);
}
long long query(int rt,int l,int r,int x,int y){
	if(l==x&&r==y) return t[rt].mn;
	int mid=l+r>>1;
	if(y<=mid) return query(rt<<1,l,mid,x,y);
	else if(x>mid) return query(rt<<1|1,mid+1,r,x,y);
	else return min(query(rt<<1,l,mid,x,mid),query(rt<<1|1,mid+1,r,mid+1,y));
}
void update(int rt,int l,int r,int x,long long val){
	if(l==r){
		t[rt].mn=min(val,t[rt].mn);
		return;
	}
	int mid=l+r>>1;
	if(x<=mid) update(rt<<1,l,mid,x,val);
	else update(rt<<1|1,mid+1,r,x,val);
	t[rt].mn=min(t[rt<<1].mn,t[rt<<1|1].mn);
}
int main(){
	freopen("gift.in","r",stdin);
	freopen("gift.out","w",stdout);
	T=read();
	while(T--){
		tim=0,tot=0;
		n=read(),m=read();
		for(register int i=1;i<=n;++i){
			p[i].v=read(),p[i].r=read(),p[i].t=read();
			rr[++tot]=p[i].r,rr[++tot]=p[i].v;
		}
		rr[++tot]=m,rr[++tot]=1;
		sort(rr+1,rr+tot+1);
		for(register int i=1;i<=tot;++i){
			if(i==1||rr[i]!=rr[i-1]){
				mp[rr[i]]=++tim;
			}
		}
		sort(p+1,p+n+1,cmp);
		build(1,1,tim);
		update(1,1,tim,1,0);
		for(register int i=1;i<=n;++i){
			f[i]=query(1,1,tim,mp[p[i].r],tim)+p[i].t;
			update(1,1,tim,mp[p[i].v],f[i]);
		}
		long long ans=query(1,1,tim,mp[m],tim);
		if(ans<1e15){
			cout<<ans<<endl;
		}else{
			cout<<-1<<endl;
		}
	}
	return 0;
}
/*

in:
3
5 9
5 1 1
10 4 10
8 1 10
11 6 1
7 3 8
4 5
2 1 1
3 2 1
4 3 1
8 4 1
3 10
5 1 3
8 2 5
10 9 2

out:
10
4
-1

*/
