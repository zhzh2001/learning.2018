#include<algorithm>
#include<iostream>
#include<string.h>
#include<fstream>
#include<stdio.h>
#include<math.h>
#include<time.h>
#include<map>
using namespace std;
ifstream fin("lucky.in");
ofstream fout("lucky.out");
inline unsigned long long read(){
	unsigned long long x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
using namespace std;
unsigned long long a[103];
int f[65];
int n,T;
unsigned long long ans;
inline void solve(){
	random_shuffle(a+1,a+n+1);
	memset(f,0,sizeof(f));
	for(register int i=1;i<=n;++i){
		for(register int j=0;j<=63;++j){
			if((1LL<<j)&a[i]){
				f[j]++;
			}
		}
	}
	for(register int j=63;j>=0;--j){
		if(f[j]==n){
			for(register int i=1;i<=n;++i){
				bool b=true;
				for(register int k=63;k>j;--k){
					if(f[k]==n-1&&(a[i]&(1LL<<k))==0){
						b=false;
						break;
					}
				}
				if(b){
					for(register int k=63;k>=0;--k){
						if(a[i]&(1LL<<k)){
							--f[k];
						}
					}
					a[i]=(~a[i]);
					for(register int k=63;k>=0;--k){
						if(a[i]&(1LL<<k)){
							++f[k];
						}
					}
					break;
				}
			}
		}
	}
}
void dfs(int k){
	if(k>n){
		unsigned long long ret=a[1];
		for(register int i=2;i<=n;++i){
			ret&=a[i];
		}
		ans=min(ans,ret);
		return;
	}
	dfs(k+1),a[k]=~(a[k]);
	dfs(k+1),a[k]=~(a[k]);
}
int main(){
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
	srand(time(0));
	T=read();
	while(T--){
		memset(f,0,sizeof(f));
		ans=1e18;
		n=read();
		int tim=0;
		if(n>=64){
			puts("0");
			continue;
		}
		for(register int i=1;i<=n;++i){
			a[i]=read();
		}
		if(n<=17){
			dfs(1);
			cout<<ans<<endl;
			continue;
		}
		(n<=30)?(tim=9):(tim=6);
		while(tim--){
			solve();
			unsigned long long ret=0;
			for(register int j=0;j<=63;++j){
				if(f[j]==n){
					ret+=(1LL<<j);
				}
			}
			ans=min(ans,ret);
		}
		cout<<ans<<endl;
	}
	return 0;
}
/*
1
3
45285
61914
6892

0
*/
