#include <bits/stdc++.h>
#define N 1000020
using namespace std;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}

char str[N];
int k[N], d[N];
int a[N], n, m;

namespace baoli {

int tmp[N];

// O(n)
void calc_reverse(int *b, int k, int d) {
  int ak = k % d, noip = k / d;
  for (int i = 0; i < k; ++ i) {
    int s = i % d, r = i / d;
    if (s < ak) {
      b[s * (noip + 1) + r] = i;
    } else {
      b[ak * (noip + 1) + (s - ak) * noip + r] = i;
    }
  }
}

// O(n)
void reverse_by(int *a, int *b, int len) {
  for (int i = 0; i < len; ++ i) {
    tmp[i] = a[b[i]];
  }
  for (int i = 0; i < len; ++ i) {
    a[i] = tmp[i];
  }
}

// O(n)
void print_solution() {
  for (int i = 0; i < n; ++ i) {
    putchar(str[a[i]]);
  }
  putchar('\n');
}

} // namespace baoli

int b[N], c[N];

namespace subtask1 {
// O(m*n^2)
void solve() {
  for (int i = 0; i < m; ++ i) {
    int k = ::k[i], d = ::d[i];
    baoli::calc_reverse(b, k, d);
    for (int i = 0; i < n - k + 1; ++ i) {
      baoli::reverse_by(a + i, b, k);
    }
    baoli::print_solution();
  }
}
} // namespace subtask1

namespace subtask2 {
// O(n^2+n*m)
void solve() {
  int k = ::k[1], d = ::d[1];
  baoli::calc_reverse(b, k, d);
  for (int i = 0; i < n; ++ i) {
    c[i] = i;
  }
  for (int i = 0; i < n - k + 1; ++ i) {
    baoli::reverse_by(c + i, b, k);
  }
  // baoli::get_ni(c, n);
  for (int i = 0; i < m; ++ i) {
    baoli::reverse_by(a, c, n);
    baoli::print_solution();
  }
}
} // namespace subtask2

namespace subtask3 {
// links to subtask1
void solve() {
  // puts("%%% lzq ak noip %%%");
  subtask1::solve();
}
} // namespace subtask3

int main(int argc, char const *argv[]) {
  freopen("sorting.in", "r", stdin);
  freopen("sorting.out", "w", stdout);

  scanf("%s", str);
  n = strlen(str);
  m = read();
  for (int i = 0; i < m; ++ i) {
    k[i] = read();
    d[i] = read();
  }
  for (int i = 0; i < n; ++ i) {
    a[i] = i;
  }

  if (n <= 100 && m <= 100) {
    subtask1::solve();
  } else if (n <= 3000) {
    subtask2::solve();
  } else {
    subtask3::solve();
  }

  return 0;
}