#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
inline int read() {
  int x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}

const int N = 100020;

struct node {
  int v, r, t;
  node(){}
  node(int r):r(r),v(0){}
} cm[N];

inline bool cmp(const node &a, const node &b) {
  return a.r == b.r ? a.v < b.v : a.r < b.r;
}

namespace segment_tree {

ll mn[N<<2];

void push_up(int x) {
  mn[x] = min(mn[x << 1], mn[x << 1 | 1]);
}

void update(int x, int k, int l, int r, ll v) {
  if (l == r) {
    mn[x] = v;
    return;
  }
  int mid = (l + r) >> 1;
  if (k <= mid) update(x << 1, k, l, mid, v);
  else update(x << 1 | 1, k, mid + 1, r, v);
  push_up(x);
}

ll query(int x, int l, int r, int L, int R) {
  if (l > r) return 1ll << 60;
  if (l == L && r == R) {
    return mn[x];
  }
  int mid = (L + R) >> 1;
  if (r <= mid) return query(x << 1, l, r, L, mid);
  if (l > mid) return query(x << 1 | 1, l, r, mid + 1, R);
  return min(query(x << 1, l, mid, L, mid), query(x << 1 | 1, mid + 1, r, mid + 1, R));
}

} // namespace segment_tree

int main(int argc, char const *argv[]) {
  freopen("gift.in", "r", stdin);
  freopen("gift.out", "w", stdout);

  int T = read();
  while (T --) {
    int n = read(), m = read();
    for (int i = 1; i <= n; ++ i) {
      cm[i].v = read();
      cm[i].r = read();
      cm[i].t = read();
    }
    sort(cm + 1, cm + n + 1, cmp);
    memset(segment_tree::mn, 0, sizeof segment_tree::mn);
    // for (int i = 1; i <= n; ++ i) {
    //   printf("[%d, %d]\n", cm[i].r, cm[i].v);
    // }
    for (int i = n; i; -- i) {
      int to = cm[i].v;
      if (cm[i].r < m) {
        if (to >= m) {
          ll res = cm[i].t;
          segment_tree::update(1, i, 1, n, res);
          // printf("f[%d] = %lld\n", i, res);
        } else {
          int right_pos = lower_bound(cm + 1, cm + n + 1, node(to+1), cmp) - cm - 1;
          ll res = segment_tree::query(1, i + 1, right_pos, 1, n) + cm[i].t;
          segment_tree::update(1, i, 1, n, res);
          // printf("f[%d] = %lld (updated from [%d, %d])\n", i, res, i + 1, right_pos);
        }
      }
    }
    int right_pos = lower_bound(cm + 1, cm + n + 1, node(2), cmp) - cm - 1;
    ll res = segment_tree::query(1, 1, right_pos, 1, n);
    // printf("[1, %d] = %lld\n", right_pos, res);
    if (res > (1ll << 55)) res = -1;
    printf("%lld\n", res);
  }

  return 0;
}