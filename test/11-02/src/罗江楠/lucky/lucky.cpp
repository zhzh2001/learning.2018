#include <bits/stdc++.h>
using namespace std;
typedef unsigned long long ull;
inline ull read() {
  ull x=0,f=1;char ch=getchar();
  while(ch<'0'||ch>'9')ch=='-'&&(f=0)||(ch=getchar());
  while(ch>='0'&&ch<='9')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
  return f?x:-x;
}

const ull max_value = - 1ll;

ull f[105][105];

#define check_min(ans, ls, rs) \
  ans = min(ans, (ls) | (rs)); \
  ans = min(ans, (ls) & (rs)); \
  ans = min(ans, (ls) ^ (rs))

ull dfs(int l, int r) {
  if (~ f[l][r]) return f[l][r];
  ull &ans = f[l][r];

  for (int i = l; i < r; ++ i) {
    ull ls = dfs(l, i);
    ull rs = dfs(i + 1, r);

    check_min(ans, ls, rs);
    check_min(ans,~ls, rs);
    check_min(ans, ls,~rs);
    check_min(ans,~ls,~rs);
  }

  return ans;
}

int main(int argc, char const *argv[]) {
  freopen("lucky.in", "r", stdin);
  freopen("lucky.out", "w", stdout);

  int T = read();
  while (T --) {
    int n = read();
    memset(f, -1, sizeof f);
    for (int i = 1; i <= n; ++ i) {
      f[i][i] = read();
    }
    printf("%llu\n", dfs(1, n));
  }

  return 0;
}
