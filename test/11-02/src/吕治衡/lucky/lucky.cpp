#include<bits/stdc++.h>
#define int unsigned long long
inline int read(){
	char c=getchar();while (c!='-'&&(c<'0'||c>'9'))c=getchar();
	int k=0,kk=1;if (c=='-')kk=-1,c=getchar();
	while (c>='0'&&c<='9')k=k*10+c-'0',c=getchar();return k*kk; 
}void write(int x){if (x<0)x=-x,putchar('-');if (x/10)write(x/10);putchar(x%10+'0');}
void writeln(int x){write(x);puts("");}
using namespace std;
int t,n,ans,flag,a[1000];
inline void dfs(int x,int d){
	if (d>n){
		if (!flag)flag=1,ans=x;else ans=min(ans,x);
		return;
	}dfs(x&a[d],d+1);dfs(x&(~a[d]),d+1);
}
signed main(){
	freopen("lucky.in","r",stdin);freopen("lucky.out","w",stdout);
	t=read();for (int i=1;i<=t;i++){
		n=read();for (int i=1;i<=n;i++)a[i]=read();
		if (n>10)puts("0");else{
			ans=0;flag=0;dfs(a[1],2);dfs(~a[1],2);
			writeln(ans);
		}
	}
}/*
2
2
3 6
3
1 2 3
*/
