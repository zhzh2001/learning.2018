#include<bits/stdc++.h>
#define LL long long
inline LL read(){
	char c=getchar();while (c!='-'&&(c<'0'||c>'9'))c=getchar();
	LL k=0,kk=1;if (c=='-')kk=-1,c=getchar();
	while (c>='0'&&c<='9')k=k*10+c-'0',c=getchar();return k*kk; 
}void write(LL x){if (x<0)x=-x,putchar('-');if (x/10)write(x/10);putchar(x%10+'0');}
void writeln(LL x){write(x);puts("");}
using namespace std;
struct lsg{int x,y,z;}a[100010];
int t,n,m;
LL f[100010];
inline bool cmp(const lsg &x,const lsg &y){return x.y<y.y;}
inline LL find(int x){LL ans=1e15;for (;x<=n;x+=(x&(-x)))ans=min(ans,f[x]);return ans;}//走到x的最小用时
inline void put(int x,LL y){for (;x;x-=(x&(-x)))f[x]=min(f[x],(LL)y);}//将1..x min上 y 
inline void calc(int x,LL y){//可以用y的时间变成x元 
	int l=0,r=n;
	while (l<r){//小于x的最大值 
		int mid=(l+r+1)>>1;
		if (a[mid].y<=x)l=mid;else r=mid-1;
	}put(l,y);
}
signed main(){
	freopen("gift.in","r",stdin);freopen("gift.out","w",stdout);
	t=read();while (t--){
		n=read();m=read();
		for (int i=1;i<=n;i++)a[i].x=read(),a[i].y=read(),a[i].z=read();
		sort(a+1,a+1+n,cmp);
		while (n&&a[n].y>m)n--;a[++n].y=m;
		for (int i=1;i<=n;i++)f[i]=1e15;
		calc(1,0);
		for (int i=1;i<=n;i++)
			if (a[i].x>a[i].y)calc(a[i].x,find(i)+a[i].z);
		LL ans=find(n);
		if (ans==1e15)puts("-1");else writeln(ans);
	}
}/*
保证五次nlogn不会t？ 
立字据！ 
*/ 
