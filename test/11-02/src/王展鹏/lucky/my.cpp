#include<bits/stdc++.h>
using namespace std;
#define ull unsigned long long
#define ld long double
const int N=105;
const ull inf=-1;
int n,ycl[N];
ull a[N];
int bitcount(ull x){
	if(x==0)return 0; return bitcount(x>>1)+(x&1);
}
int main(){
	freopen("lucky.in","r",stdin); freopen("lucky.out","w",stdout);
	int T; cin>>T;
	while(T--){
		cin>>n;
		for(int i=0;i<n;i++)cin>>a[i];
		ull ans=inf;
		for(int i=0;i<n;i++){
			if(bitcount(ans&a[i])<bitcount(ans&(a[i]^inf)))ans&=a[i];
			else ans&=(a[i]^inf);
		}
		if(ans==0){
			puts("0"); continue;
		}
		for(int i=ycl[0]=1;i<n;i++)ycl[i]=ycl[i-1]*3;
		for(int i=0;i<ycl[n-1];i++){
			for(int j=0;j<(1<<n);j++){
				ull sum=(j&1)?a[0]:(a[0]^inf);
				for(int k=1;k<n;k++){
					ull t=(j>>k&1)?a[k]:(a[k]^inf);
					int jb=i/ycl[k-1]%3;
					if(jb==0)sum&=t;
					else if(jb==1)sum|=t;
					else sum^=t;
				}
				ans=min(ans,sum);
			}
		}
		cout<<ans<<endl;
	}
} 
/*
32 16 8 4 2 1
*/
