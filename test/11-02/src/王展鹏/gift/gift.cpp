#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
#define ld long double
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}
inline void write(int x){
	if(x<0) putchar('-'),x=-x;
	if(x>=10) write(x/10);
	putchar(x%10+'0');
}
const int N=200005;
const ll inf=1e18;
int n,m,q[N];
struct data{
	int v,r,t;
}a[N];
ll c[N],dp[N];
inline bool cmp(data a,data b){
	return a.v<b.v;
}
#define lowbit(i) i&-i
void change(int pos,ll de){
	for(int i=pos;i<N;i+=lowbit(i))c[i]=min(c[i],de);
}
ll ask(int pos){
	ll ans=inf;
	for(int i=pos;i;i-=lowbit(i))ans=min(ans,c[i]);
	return ans;
}
int main(){
	freopen("gift.in","r",stdin); freopen("gift.out","w",stdout);
	int T=read();
	while(T--){
		n=read(); m=read();
		int tot=0;
		q[++tot]=1;
		for(int i=1;i<=n;i++){
			a[i].v=read(); a[i].r=read(); a[i].t=read();
			q[++tot]=a[i].v; q[++tot]=a[i].r;
		}
		sort(&q[1],&q[tot+1]);
		tot=unique(&q[1],&q[tot+1])-q-1;
		for(int i=1;i<=n;i++){
			a[i].v=lower_bound(&q[1],&q[tot+1],a[i].v)-q;
			a[i].r=lower_bound(&q[1],&q[tot+1],a[i].r)-q;
		}
		sort(&a[1],&a[n+1],cmp);
		ll ans=inf;
		for(int i=0;i<N;i++)c[i]=inf;
		change(N-1,0);
		if(m==1)ans=0;
		for(int i=1;i<=n;i++){
			dp[i]=ask(N-a[i].r)+a[i].t;
			change(N-a[i].v,dp[i]);
			if(q[a[i].v]>=m)ans=min(ans,dp[i]);
		}
		if(ans>=inf)puts("-1"); else cout<<ans<<endl;
	}
}
/*
1100
0101
0110
*/
