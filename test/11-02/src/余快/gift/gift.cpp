/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000000000005
#define mod 1000000007
#define put putchar('\n')
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<ll,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
#define N 500055
//#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
//#define getchar gc
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(ll x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(ll x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int vis[N],n,m,ans,tot,z[N*2],v[N],r[N],t[N],tot2,z2[N*2];
//��������
//ע��ll 
int Next[N*4],head[N*2],to[N*4],lon[N*4],nedge;
#define V to[i]
void add(int a,int b,int c){
	Next[++nedge]=head[a];head[a]=nedge;to[nedge]=b;lon[nedge]=c;
}
ll dis[N];
priority_queue <pa> q;
void djstl(){
	F(i,1,tot2) vis[i]=0,dis[i]=inf;
	dis[1]=0;q.push(mp(0,1));
	while (!q.empty()){
		pa t=q.top();q.pop();
		if (vis[t.se]) continue;
		vis[t.se]=1;
		int x=t.se;
		ll lo=-t.fi;
		go(i,x){
			if (lo+lon[i]<dis[V]){
				dis[V]=lo+lon[i];
				q.push(mp(-dis[V],V));
			}
		}
	}
}
signed main(){
	freopen("gift.in","r",stdin);
	freopen("gift.out","w",stdout);
	for (int ci=read();ci;ci--){
		n=read();m=read();tot=0;
		F(i,1,n){
			v[i]=read();r[i]=read();t[i]=read();
			z[++tot]=v[i];z[++tot]=r[i];
		}
		z[++tot]=m;z[++tot]=1;
		sort(z+1,z+tot+1);tot2=0;
		F(i,1,tot){
			if (z[i]!=z[i-1]) z2[++tot2]=z[i];
		}
		nedge=0;
		F(i,1,tot2) head[i]=0;
		F(i,1,n){
			v[i]=lower_bound(z2+1,z2+tot2+1,v[i])-z2;
			r[i]=lower_bound(z2+1,z2+tot2+1,r[i])-z2;
			add(r[i],v[i],t[i]);
		}
		m=lower_bound(z2+1,z2+tot2+1,m)-z2;
		F(i,1,tot2-1) add(i+1,i,0);
		djstl();
		if (dis[m]==inf) puts("-1");
		else wrn(dis[m]);
	}
	return 0;
}
