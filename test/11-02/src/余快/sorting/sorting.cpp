/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
#define N 1000055
//#define int ll
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
//#define getchar gc
inline int read(){char c=getchar();int tot=1;while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){tot=-1;c=getchar();}
int sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum*tot;}
inline void wr(int x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(int x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int n,m,ans,k,mo,t,d,vis[N],tot,p[N],z[N],wzp,cnt,yk[N];
char str[N],str2[N];
signed main(){
	freopen("sorting.in","r",stdin);freopen("sorting.out","w",stdout);
	scanf("%s",str);n=strlen(str);
	m=read();//n*m<=1e6
	F(ci,1,m){
		k=read();d=read();
		if (k==1||d==1){
			printf("%s\n",str);continue;
		}
		mo=0;t=0;wzp=n-k+1;
		F(i,0,k-1) vis[i]=0;
		F(i,0,k-1){
			while (vis[t]){
				t+=d;
				if (t>=k) t=++mo;
			}
			p[t]=i-1;vis[t]=1;
		}
		t=k-1;tot=0;
		F(i,0,k-1) vis[i]=0;
		while (t!=-1){
			z[++tot]=t;vis[t]=1;
			t=p[t];
		}
		cnt=0;
		//处理链 
		//链要考虑没走完的处理 
		F(i,1,tot) yk[++cnt]=z[tot-i+1];
		F(i,k,n-1) yk[++cnt]=i;
		//保证后面是的前提下全放最前面 
			F(i,2,tot){
				str2[n-k+z[i]+1]=str[yk[cnt-i+2]];
			}
			cnt=cnt-tot+1;
			F(i,0,cnt-1) str2[i]=str[yk[i+1]];
		/*else{
			F(i,0,tot-1)  
			str2[i]=str[z[tot-i]];
			F(i,tot,n-k) 
			str2[i]=str[i-tot+k];
			//肯定是一些循环和一条链，所有新来的数都会到链里去，最后出去 
			F(i,2,tot){
				str2[n-k+z[i]+1]=str[n-i+1];
			}
		}*/
		//处理环 
		F(i,0,k-1){
			if (!vis[i]){
				t=i;tot=0;
				while (!vis[t]){
					z[++tot]=t;vis[t]=1;
					t=p[t];
				}
				F(j,1,tot){
					str2[n-k+z[j]+1]=str[z[((j-n+k-2)%tot+tot)%tot+1]];
				}
			}
		}
		F(i,0,n-1) str[i]=str2[i],str2[i]='0';
		F(i,0,k-1) vis[i]=0;
		printf("%s\n",str);
	}
	return 0;
}
