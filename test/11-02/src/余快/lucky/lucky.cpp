/*#pragma comment(linker, "/stack:200000000")
#pragma GCC optimize("Ofast")
#pragma target("sse,sse2,sse3,ssse3,sse4,popcnt,abm,mmx,avx,tune=native")*/
#include<bits/stdc++.h>
#define ll long long
#define inf 1000000005
#define mod 1000000007
#define put putchar('\n')
#define F(i,a,b) for (int i=(a);i<=(b);i++)
#define D(i,a,b) for (int i=(a);i>=(b);i--)
#define go(i,t) for (int i=head[t];i;i=Next[i])
#define sqr(x) ((x)*(x))
#define re register
#define mp make_pair
#define fi first
#define se second
#define pa pair<int,int>
#define pb push_back
#define be begin()
#define en end()
#define ret return puts("-1"),0;
#define N 500055
//#define int ll
#define ull unsigned long long
using namespace std;
inline char gc(){
    static char buf[100000],*p1=buf,*p2=buf;
    return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
//#define getchar gc
inline ull read(){char c=getchar();while ((c<'0'|| c>'9')&&c!='-') c=getchar();if (c=='-'){c=getchar();}
ull sum=0;while (c>='0'&&c<='9'){sum=sum*10+c-'0';c=getchar();}return sum;}
inline void wr(ull x){if (x<0) {putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
inline void wrn(ull x){wr(x);put;}inline void wri(int x){wr(x);putchar(' ');}
inline void wrn(int x,int y){wri(x);wrn(y);}inline void wrn(int a,int b,int c){wri(a);wrn(b,c);}
int n,m;
ull a[105],b[105],num[100],ans,wzp;
inline void dfs(int x,ull k){
	if (x==n+1){
		ans=min(ans,k);return;
	}
	dfs(x+1,k&a[x]);dfs(x+1,k&b[x]);
}
signed main(){
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
	num[0]=1;F(i,1,63) num[i]=num[i-1]*2;
	F(i,0,63) wzp+=num[i]; 
	for (int ci=read();ci;ci--){
		n=read();
		F(i,1,n){
			a[i]=read();
			F(j,0,63){
				if (!(a[i]&num[j])) b[i]|=num[j];
			}
		}
		if (n<=18){
			ans=wzp;
			dfs(2,a[1]);dfs(2,b[1]);
			wrn(ans);
		}
		else puts("0");
	}
	return 0;
}
