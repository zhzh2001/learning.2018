var s,ss:ansistring;
    n,k,d,i,j,len,p:longint;
    a,b:array[0..10000]of longint;
procedure sort(l,r: longint);
      var
         i,j,x,y,xx: longint;
      begin
         i:=l;
         j:=r;
         x:=a[(l+r) div 2];
         xx:=b[(l+r)div 2];
         repeat
           while (a[i]<x)or(a[i]=x)and(b[i]<xx) do
            inc(i);
           while (x<a[j])or(x=a[j])and(xx<b[j]) do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];a[i]:=a[j];a[j]:=y;
                y:=b[i];b[i]:=b[j];b[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;
begin
 assign(input,'sorting.in');
 assign(output,'sorting.out');
 reset(input);
 rewrite(output);
 readln(s); len:=length(s); ss:=s;
 readln(n);
 if n<=100 then
 begin
  for i:=1 to n do
  begin
   readln(k,d);
   for j:=1 to k do
   begin
    a[j]:=(j-1)mod d;
    b[j]:=j;
   end;
   sort(1,k);
    //writeln('###########');
   //for j:=1 to k do writeln(a[j],' ',b[j]);
   for j:=1 to len-k+1 do
   begin
    //writeln(j);
    for p:=1 to k do ss[p]:=s[j+b[p]-1];
    for p:=j to k+j-1 do s[p]:=ss[p-j+1];
   end;
   writeln(s);
  end;
 end
 else begin
  read(k,d);
  for i:=1 to k do
  begin
   a[i]:=(i-1)mod d;
   b[i]:=i;
  end;
  sort(1,k);
  for i:=1 to n do
  begin
   for j:=1 to len-k+1 do
   begin
    //writeln(j);
    for p:=1 to k do ss[p]:=s[j+b[p]-1];
    for p:=j to k+j-1 do s[p]:=ss[p-j+1];
   end;
   writeln(s);
  end;
 end;
 close(input);
 close(output);
end.