var t,n,ans,xx,yy,tot:int64;
    i,j:longint;
    x,y,a,b,fan,d,e:array[0..100]of int64;
    c:array[0..10000,0..100]of int64;
function qufan(x:int64):int64;
var k:int64; i:longint;
begin
 while x>0 do
 begin
  inc(tot);
  d[tot]:=x mod 2;
  x:=x div 2;
 end;
 for i:=3 to tot do k:=k+(d[i]+1)mod 2*(2<<i-2);
 if d[1]=0 then inc(k);
 if d[2]=0 then inc(k,2);
 exit(k);
end;
procedure jisuan;
var i,j,k:longint;
begin
 for j:=1 to tot do
 begin
  xx:=0; yy:=0;
  for i:=1 to n do
  begin
   inc(xx);
   x[xx]:=c[tot,i];
   if i<n then
   begin
    if yy>0 then
     if y[yy]=3 then
     begin x[xx-1]:=x[xx-1]xor x[xx]; dec(xx); dec(yy); end
     else if b[i+1]<3 then
     begin
      if y[yy]=1 then x[xx-1]:=x[xx-1]or x[xx];
      if y[yy]=2 then x[xx-1]:=x[xx-1]and x[xx];
      dec(xx); dec(yy);
     end;
    inc(yy); y[yy]:=b[i+1];
   end
   else begin
    for k:=yy downto 1 do
    begin
     if y[k]=1 then x[xx-1]:=x[xx-1]or x[xx]
     else if y[k]=2 then x[xx-1]:=x[xx-1]and x[xx]
     else x[xx-1]:=x[xx-1]xor x[xx];
     dec(xx);
    end;
    if x[xx]<ans then ans:=x[xx];
   end;
  end;
 end;
end;
procedure dfs(k:int64);
begin
 if ans=0 then exit;
 if k=n+1 then
 begin
  jisuan;
  exit;
 end;
 b[k]:=1;
 dfs(k+1);
 b[k]:=2;
 dfs(k+1);
 b[k]:=3;
 dfs(k+1);
end;
procedure dfs2(k:int64);
var i:longint;
begin
 if k=n+1 then
 begin
  inc(tot);
  for i:=1 to n do c[tot,i]:=b[i];
  exit;
 end;
 b[k]:=a[k];
 dfs2(k+1);
 b[k]:=fan[k];
 dfs2(k+1);
end;
begin
 assign(input,'lucky.in');
 assign(output,'lucky.out');
 reset(input);
 rewrite(output);
 read(t);
 for i:=1 to t do
 begin
  read(n);
  for j:=1 to n do
  begin
   read(a[j]);
   fan[j]:=qufan(a[j]);
   tot:=0;
  end;
  dfs2(1);
  fillchar(b,sizeof(b),0);
  ans:=100000000;
  dfs(2);
  writeln(ans);
 end;
 close(input);
 close(output);
end.