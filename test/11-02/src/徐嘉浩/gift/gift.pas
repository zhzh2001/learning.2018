uses math;
var t,n,m,p,ans,ma,xx,sum:int64; k,i,j,tot:longint; q:boolean;
    a,b,c,dui:array[0..10000]of int64;
    f:array[0..1500,0..1500]of int64;
procedure sort(l,r: longint);
      var
         i,j,x,y: longint;
      begin
         i:=l;
         j:=r;
         x:=b[(l+r) div 2];
         repeat
           while b[i]<x do
            inc(i);
           while x<b[j] do
            dec(j);
           if not(i>j) then
             begin
                y:=a[i];a[i]:=a[j];a[j]:=y;
                y:=b[i];b[i]:=b[j];b[j]:=y;
                y:=c[i];c[i]:=c[j];c[j]:=y;
                inc(i);
                j:=j-1;
             end;
         until i>j;
         if l<j then
           sort(l,j);
         if i<r then
           sort(i,r);
      end;
begin
 assign(input,'gift.in');
 assign(output,'gift.out');
 reset(input);
 rewrite(output);
 read(t);
 for k:=1 to t do
 begin
  q:=false;
  read(n,m);
  for i:=1 to n do
  begin
   read(a[i],b[i],c[i]);
   ma:=max(ma,a[i]);
   if c[i]<>1 then q:=true;
  end;
  if q then begin
  sort(1,n);
  for i:=0 to 1500 do for j:=0 to 1500 do f[i,j]:=10000000000;
  for i:=0 to n do begin f[i,0]:=0; f[i,1]:=0; end;
  for i:=1 to n do
   for j:=1 to ma do
   begin
    f[i,j]:=f[i-1,j];
    if a[i]>=j then f[i,j]:=min(f[i,j],f[i-1,b[i]]+c[i]);
   end;
  ans:=f[n,m];
  //for i:=m+1 to ma do
   //if f[n,i]<ans then ans:=f[n,i];
  if ans=10000000000 then writeln(-1)
  else writeln(ans);
  //for i:=1 to n do
  //begin
   //writeln;
   //for j:=1 to ma do write(i,' ',j,' ',f[i,j],' ');
  //end;
  end
  else begin
   sort(1,n);
   tot:=1;  sum:=1; ma:=0; ans:=0;
   while tot<=n do
   begin
    for i:=tot to n do
    begin
     if sum>=b[i] then
     begin
      if a[i]>ma then ma:=a[i];
     end
     else break;
    end;
    inc(ans); sum:=ma; ma:=0;
    if sum>=m then break;
   end;
   writeln(ans);
  end;
 end;
 close(input);
 close(output);
end.