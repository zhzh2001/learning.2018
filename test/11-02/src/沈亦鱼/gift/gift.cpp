#include<cstdio>
#include<algorithm>
using namespace std;
int n,m,k,a[110000],b[110000],c[110000],t[110000];
long long inf=1e18,d[110000];
inline int read(){
	int x=0,f=1;
	char ch=getchar();
	while(ch<'0'||ch>'9'){
		if(ch=='-')f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}
void sor(int l,int r){
	int i=l,j=r,x=a[(l+r)>>1];
	while(i<=j){
		while(a[i]<x)i++;
		while(x<a[j])j--;
		if(i<=j){
			swap(a[i],a[j]);
			swap(b[i],b[j]);
			swap(t[i],t[j]);
			i++;
			j--;
		}
	}
	if(l<j)sor(l,j);
	if(i<r)sor(i,r);
}
void down(int i){
	int j=0;
	while(i*2<=k){
		if(d[i*2]<d[i*2+1])j=i*2;
		else j=i*2+1;
		if(d[i]>d[j]){
			swap(c[i],c[j]);
			swap(d[i],d[j]);
		}
		else return;
		i=j;
	}
}
void up(int i){
	while(i>1){
		if(d[i]<d[i/2]){
			swap(c[i],c[i/2]);
			swap(d[i],d[i/2]);
		}
		else return;
		i>>=1;
	}
}
int main(){
	freopen("gift.in","r",stdin);
	freopen("gift.out","w",stdout);
	int T;
	T=read();
	while(T--){
		n=read();
		m=read();
		for(int i=1;i<=n;i++){
			b[i]=read();
			a[i]=read();
			t[i]=read();
		}
		sor(1,n);
		k=1;
		d[k]=0;
		c[k]=1;
		d[k+1]=inf;
		for(int i=1;i<=n;i++){
			while(k>0&&c[1]<a[i]){
				d[1]=d[k];
				c[1]=c[k];
				d[k]=inf;
				k--;
				down(1);
			}
			if(!k){
				puts("-1");
				break;
			}
			k++;
			c[k]=b[i];
			d[k]=d[1]+t[i];
			up(k);
		}
		if(!k)continue;
		while(k>0&&c[1]<m){
			d[1]=d[k];
			c[1]=c[k];
			d[k]=2e9+1;
			k--;
			down(1);
		}
		if(!k)puts("-1");
		else printf("%lld\n",d[1]);
	}
	return 0;
}
