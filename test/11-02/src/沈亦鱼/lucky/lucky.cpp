#include<cstdio>
#include<algorithm>
using namespace std;
int n,nn,k;
unsigned long long sz,s,ans,x,a[110],b[110];
int c[10];
inline int rd(){
	int x=0;
	char ch=getchar();
	while(!(ch>='0'&&ch<='9'))ch=getchar();
	while(ch>='0'&&ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x;
}
inline unsigned long long read(){
	unsigned long long x=0;
	char ch=getchar();
	while(!(ch>='0'&&ch<='9'))ch=getchar();
	while(ch>='0'&&ch<='9'){
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x;
}
inline void write(long long x){
	if(x>9)write(x/10);
	putchar(x%10+48);
}
void dfs(int k){
	if(k>nn){
		if(s<ans)ans=s;
		return;
	}
	if(k==1){
		s=a[k];//c[k]=1;
		dfs(k+1);
		s=b[k];//c[k]=2;
		dfs(k+1);
		return;
	}
	unsigned long long ls=s;
	s=ls&a[k];
	dfs(k+1);
	s=ls|a[k];
	dfs(k+1);
	s=ls^a[k];
	dfs(k+1);
	s=ls&b[k];
	dfs(k+1);
	s=ls|b[k];
	dfs(k+1);
	s=ls^b[k];
	dfs(k+1);
}
int main(){
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
	int T;
	T=rd();
	sz=1;
	sz<<=63;
	sz-=1;
	sz<<=1;
	sz+=1;
	while(T--){
		n=rd();
		for(int i=1;i<=n;i++){
			a[i]=read();
//			x=a[i];
//			k=-1;
//			while(x>0){
//				x%=2;
//				k++;
//				b[i]=(unsigned long long)x*((unsigned long long)1<<k);
//				x>>=2;
//			}
			b[i]=a[i]^sz;//printf("%d %d %lld %lld\n",T,i,a[i],b[i]);
		}
		s=0;
		ans=sz;
		nn=min(8,n);
		dfs(1);
		if(n>8){
			for(int j=9;j<=n;j++){
				s=ans;
/*				if((s&a[j])<ans)ans=s&a[j];
				if((s|a[j])<ans)ans=s|a[j];
				if((s^a[j])<ans)ans=s^a[j];
				if((s&a[j])<ans)ans=s&a[j];
				if((s|a[j])<ans)ans=s|a[j];
				if((s^a[j])<ans)ans=s^a[j];*/
				ans=min(ans,s&a[j]);
				ans=min(ans,s|a[j]);
				ans=min(ans,s^a[j]);
				ans=min(ans,s&b[j]);
				ans=min(ans,s|b[j]);
				ans=min(ans,s^b[j]);
			}
		}
		write(ans);putchar('\n');
	}
	return 0;
}
