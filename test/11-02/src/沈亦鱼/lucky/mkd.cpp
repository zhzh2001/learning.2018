#include<bits/stdc++.h>
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
using namespace std;
long long rnd(){
	long long x=0;
	For(i,1,9) x=x*100+rand()%100;
	return x;
}
int main(){
	freopen("lucky.in","w",stdout);
	srand(time(0));
	int T=100;
	printf("%d\n",T);
	const int n=9;
	while(T--){
		printf("%d\n",n);
		For(i,1,n) printf("%lld ",rnd());
		puts("");
	}
}
