#include<bits/stdc++.h>
#define int long long
using namespace std;
int n,t,flag,ans;
int a[100001];
void search(int x,int y){
	if(x>n){
		if(y<0)
			return;
		if(!flag){
			ans=y;
			flag=1;
		}
		else
			ans=min(ans,y);
		return; 
	}
	search(x+1,y&(~a[x]));
	search(x+1,y|(~a[x]));
	search(x+1,y^(~a[x]));
	search(x+1,y&(a[x]));
	search(x+1,y|(a[x]));
	search(x+1,y^(a[x]));
}
signed main(){
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
	scanf("%lld",&t);
	while(t--){
		flag=0;
		scanf("%lld",&n);
		for(int i=1;i<=n;i++)
			scanf("%lld",&a[i]);
		search(2,a[1]);
		search(2,~a[1]);
		printf("%lld\n",ans); 
	}
	return 0;
}
