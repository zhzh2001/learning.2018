#include<bits/stdc++.h>
using namespace std;
struct Edge{
	int x,y,z;
}edge[100001];
bool cmp(Edge x,Edge y){
	return x.y<y.y;
}
int t,n,m,x,sum,ans;
int main(){
	freopen("gift.in","r",stdin);
	freopen("gift.out","w",stdout);
	scanf("%d",&t);
	while(t--){
		scanf("%d%d",&n,&m);
		for(int i=1;i<=n;i++)
			scanf("%d%d%d",&edge[i].x,&edge[i].y,&edge[i].z);
		sort(edge+1,edge+1+n,cmp);
		x=1;
		while(sum<m){
			while(sum>=edge[x].y)
				x++;
			x--;
			sum=edge[x].x;
			ans+=edge[x].z;
		}
	}
	printf("%d",ans);
	return 0;
}
