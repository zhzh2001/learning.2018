#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <queue>
#include <set>
using namespace std;

namespace IO {
static const int kMegaBytes = 1 << 20;
char buf[kMegaBytes], *S = buf, *T = buf;
bool isneg;
inline bool Fetch() { return T = (S = buf) + fread(buf, 1, kMegaBytes, stdin), S != T; }
inline char NC() { return S == T ? (Fetch() ? *S++ : EOF) : *S++; }
template <typename Int> inline void RI(Int &x) {
	x = isneg = 0;
	char ch;
	for (ch = NC(); isspace(ch); ch = NC())
		;
	if (ch == '-')
		isneg = true, ch = NC();
	for (; isdigit(ch); ch = NC())
		x = x * 10 + ch - '0';
	if (isneg) x = -x;
}
}

using IO::RI;

typedef long long ll;

const int kMaxN = 1e5 + 5, kInf = 0x3f3f3f3f;
const ll kInfL = 1LL << 60;
int T, n, m;
struct Atom {
	int v, r;
	ll t;
	inline bool operator<(const Atom &other) const {
		return r < other.r || (r == other.r && v < other.v);
	}
} c[kMaxN];

struct Graph {
	static const int kMaxM = kMaxN << 2;
	struct Edge { int to, nxt; ll w; } e[kMaxM << 1];
	int ecnt, head[kMaxN];
	ll dis[kMaxN];
	bool inq[kMaxN];
	inline Graph() : ecnt(0) {}
	inline void AddEdge(int u, int v, ll w) {
		e[++ecnt] = (Edge) { v, head[u], w };
		head[u] = ecnt;
	}
	inline void Clear() {
		memset(head, 0x00, sizeof head);
		ecnt = 0;
	}
	ll SPFA() {
		for (int i = 0; i < kMaxN; ++i)
			dis[i] = kInfL;
		memset(inq, 0x00, sizeof inq);
		queue<int> Q;
		dis[0] = 0;
		inq[0] = true;
		Q.push(0);
		while (!Q.empty()) {
			int u = Q.front();
			for (int i = head[u]; i; i = e[i].nxt) {
				if (dis[e[i].to] > dis[u] + e[i].w) {
					dis[e[i].to] = dis[u] + e[i].w;
					if (!inq[e[i].to]) {
						inq[e[i].to] = true;
						Q.push(e[i].to);
					}
				}
			}
			inq[u] = false;
			Q.pop();
		}
		// for (int i = 1; i <= n + 1; ++i)
		// 	printf("%lld%c", dis[i], " \n"[i == n + 1]);
		return dis[n + 1];
	}
	ll Dijkstra() {
		priority_queue<pair<ll, int>, vector<pair<ll, int> >, greater<pair<ll, int> > > Q;
		for (int i = 0; i < kMaxN; ++i)
			dis[i] = kInfL;
		dis[0] = 0;
		Q.push(make_pair(dis[0], 0));
		while (!Q.empty()) {
			pair<ll, int> now = Q.top();
			Q.pop();
			int u = now.second;
			for (int i = head[u]; i; i = e[i].nxt) {
				if (dis[e[i].to] > dis[u] + e[i].w) {
					dis[e[i].to] = dis[u] + e[i].w;
					Q.push(make_pair(dis[e[i].to], e[i].to));
				}
			}
		}
		return dis[n + 1];
	}
} g;

int main() { // 100 pts
	freopen("gift.in", "r", stdin);
	freopen("gift.out", "w", stdout);
	RI(T);
	while (T--) {
		RI(n), RI(m);
		for (int i = 1; i <= n; ++i) {
			RI(c[i].v), RI(c[i].r), RI(c[i].t);
		}
		sort(c + 1, c + n + 1);
		g.Clear();
		for (int i = 1; i <= n; ++i) {
			g.AddEdge(i, i - 1, 0);
		}
		c[0].v = 1;
		c[n + 1].r = m;
		for (int i = 0; i <= n; ++i) {
			int v = upper_bound(c + 1, c + n + 2, (Atom) { 0, c[i].v + 1, 0}) - c - 1;
			g.AddEdge(i, v, c[i].t);
		}
		ll ans = g.Dijkstra();
		if (ans < kInfL) {
			printf("%lld\n", ans);
		} else {
			puts("-1");
		}
	}
	return 0;
}