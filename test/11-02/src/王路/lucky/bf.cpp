#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <limits>
using namespace std;

namespace IO {
static const int kMegaBytes = 1 << 20;
char buf[kMegaBytes], *S = buf, *T = buf;
bool isneg;
inline bool Fetch() { return T = (S = buf) + fread(buf, 1, kMegaBytes, stdin), S != T; }
inline char NC() { return S == T ? (Fetch() ? *S++ : EOF) : *S++; }
template <typename Int> inline void RI(Int &x) {
	x = isneg = 0;
	char ch;
	for (ch = NC(); isspace(ch); ch = NC())
		;
	if (ch == '-')
		isneg = true, ch = NC();
	for (; isdigit(ch); ch = NC())
		x = x * 10 + ch - '0';
	if (isneg) x = -x;
}
}

using IO::RI;

typedef long long ll;
typedef unsigned long long ull;

const int kMaxN = 105;
int T, n, cnt;
ull a[kMaxN], ans;
int cho[kMaxN];

void DFS(int d) {
	if (d == n) {
		ull tmp = a[1];
		for (int j = 2; j <= n; ++j) {
			if (cho[j] == 1) {
				tmp &= a[j];
			} else if (cho[j] == 2) {
				tmp |= a[j];
			} else if (cho[j] == 3) {
				tmp ^= a[j];
			}
		}
		++cnt;
		// fprintf(stderr, "%llu\n", tmp);
		ans = min(ans, tmp);
		return;
	}
	for (int i = 0; i < 3; ++i) {
		cho[d + 1] = i;
		DFS(d + 1);
	}
}

int main() {
	freopen("lucky.in", "r", stdin);
	freopen("bf.out", "w", stdout);
	RI(T);
	while (T--) {
		// fprintf(stderr, "Testcase #x\n");
		RI(n);
		ans = numeric_limits<ull>::max(), cnt = 0;
		for (int i = 1; i <= n; ++i) {
			RI(a[i]);
		}
		for (int i = 0; i < (1 << n); ++i) {
			for (int j = 0; j < n; ++j) {
				if ((i >> j) & 1)
					a[j + 1] ^= numeric_limits<ull>::max();
			}
			DFS(1);
			for (int j = 0; j < n; ++j) {
				if ((i >> j) & 1)
					a[j + 1] ^= numeric_limits<ull>::max();
			}
		}
		// fprintf(stderr, "cnt = %d\n", cnt);
		printf("%llu\n", ans);
	}
	return 0;
}
