#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
using namespace std;

const int kMaxN = 1e6 + 5;

char s[kMaxN], t[kMaxN];
int n, m, k, d, id[kMaxN], cc[kMaxN], dd[kMaxN];

inline bool cmp(int a, int b) {
	int ma = a % d, mb = b % d;
	return ma < mb || (ma == mb && a < b);
}

namespace SubTask1 {
inline void Sort(int l, int r) {
	for (int i = l; i <= r; ++i)
		t[i - l] = s[i], id[i] = i - l;
	sort(id + l, id + r + 1, cmp);
	for (int i = l; i <= r; ++i) {
		s[i] = t[id[i]];
	}
}
int Solve() {
	for (int i = 0; i < n; ++i)
		id[i] = i;
	for (int i = 1; i <= m; ++i) {
		scanf("%d%d", &k, &d);
		for (int j = 0; j <= n - k; ++j) {
			Sort(j, j + k - 1);
		}
		puts(s);
	}
	return 0;
}
}

namespace SubTask2 {
inline void Sort(int l, int r) {
	for (int i = l; i <= r; ++i)
		dd[i - l] = cc[i], id[i] = i - l;
	sort(id + l, id + r + 1, cmp);
	for (int i = l; i <= r; ++i) {
		cc[i] = dd[id[i]];
	}
}
void Next() {
	for (int i = 0; i < n; ++i) {
		id[i] = cc[id[i]];
	}
}
inline void Write() {
	for (int i = 0; i < n; ++i)
		putchar(s[id[i]]);
	puts("");
}
int Solve() {
	for (int i = 0; i < n; ++i) {
		cc[i] = i;
	}
	scanf("%d%d", &k, &d);
	for (int j = 0; j <= n - k; ++j) {
		Sort(j, j + k - 1);
	}
	for (int i = 0; i < n; ++i) {
		id[i] = i;
	}
	Next();
	Write();
	for (int i = 2; i <= m; ++i) {
		Next();
		Write();
	}
	return 0;
}
}

int main() {
	freopen("sorting.in", "r", stdin);
	freopen("sorting.out", "w", stdout);
	scanf("%s\n%d", s, &m);
	n = strlen(s);
	if (n <= 100 && m <= 100) { // 20 pts, GET
		return SubTask1::Solve();
	}
	if (n <= 3000) { // 30 pts, GET
		return SubTask2::Solve();
	}
	return 0;
}