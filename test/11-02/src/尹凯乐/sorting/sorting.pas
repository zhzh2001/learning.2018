program sorting;
 var
  a,b:array[0..1000001] of longint;
  c:array[0..1000001] of char;
  i,j,k,m,n,x,y:longint;
  s:ansistring;
 begin
  assign(input,'sorting.in');
  assign(output,'sorting.out');
  reset(input);
  rewrite(output);
  readln(s);
  n:=length(s);
  readln(m);
  if m<=1000000 then
   for i:=1 to m do
    begin
     readln(x,y);
     for j:=1 to n do
      a[j]:=j;
     for j:=1 to n-x+1 do
      begin
       for k:=1 to n do
        b[k]:=a[k];
       for k:=j to j+x-1 do
        b[(k-j) mod y*y+(k-j) div y+j]:=a[k];
       for k:=j to j+x-1 do
        a[k]:=b[k];
      end;
     for j:=1 to n do
      c[j]:=s[a[j]];
     for j:=1 to n do
      write(c[j]);
     writeln;
     for j:=1 to n do
      s[j]:=c[j];
    end;
  close(input);
  close(output);
 end.
