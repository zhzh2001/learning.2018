#include<bits/stdc++.h>
using namespace std;
#define ll int
ll read(){	ll x=0,f=1;	char ch=getchar();	for(;ch<'0'||ch>'9';ch=getchar())	if (ch=='-')	f=-1;	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';	return x*f;	}
void write(ll x){	if (x<0)	putchar('-'),x=-x;	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define rep(i,x,y)	for(ll i=x;i<y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define Add(x,y)	((x)=((x)+(y))%mod)
#define Mul(x,y)	((x)=((x)*(y))%mod)
#define Abs(x)		((x)<0?(-(x)):(x))
#define Fpn			freopen
#define IL			inline
IL void Max(ll &x,ll y){x=x<y?y:x;}
IL void Min(ll &x,ll y){x=x<y?x:y;}
IL ll max(ll x,ll y){return x<y?y:x;}
IL void Print(ll *a,ll x,ll y){	For(i,x,y)	write(a[i]),putchar(' ');	}
#define Min(x,y)	((x)=((x)<(y)?(x):(y)))
const ll N=1010;
unsigned long long a[N],b[N],ans;
ll n;
void dfs(ll x,unsigned long long now){
	if (x>n){
		Min(ans,now);
		return;
	}
	dfs(x+1,now&b[x]);
	dfs(x+1,now|b[x]);
	dfs(x+1,now^b[x]);
}
void Dfs(ll x){
	if (x>n){
		dfs(2,b[1]);
	}
	b[x]=a[x];
	Dfs(x+1);
	b[x]=~a[x];
}
int main(){
	freopen("lucky.in","r",stdin);
	freopen("baoli.out","w",stdout);
	ll T=read();
	for(;T--;){
		n=read();
		For(i,1,n)cin>>a[i];
		ans=-1;
		Dfs(1);
		printf("%llu\n",ans);
	}
}
/*
2
2
3 6
3
1 2 3
*/
