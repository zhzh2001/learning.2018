#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
int read(){	int x=0,f=1;	char ch=gc();	for(;ch<'0'||ch>'9';ch=gc())	if (ch=='-')	f=-1;	for(;ch>='0'&&ch<='9';ch=gc())	x=x*10+ch-'0';	return x*f;	}
void write(ll x){	if (x<0)	putchar('-'),x=-x;	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define rep(i,x,y)	for(ll i=x;i<y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define Add(x,y)	((x)=((x)+(y))%mod)
#define Mul(x,y)	((x)=((x)*(y))%mod)
#define Abs(x)		((x)<0?(-(x)):(x))
#define Fpn			freopen
#define IL			inline
IL void Max(ll &x,ll y){x=x<y?y:x;}
IL void Min(ll &x,ll y){x=x<y?x:y;}
IL ll max(ll x,ll y){return x<y?y:x;}
IL void Print(ll *a,ll x,ll y){	For(i,x,y)	write(a[i]),putchar(' ');	}
#define Min(x,y)	((x)=((x)<(y)?(x):(y)))
const ll N=200010;
struct dt_ak_king{
	ll v,r,t;
}p[N];
ll nn,n,m,c[N],q[N],bel[N];
bool operator <(dt_ak_king a,dt_ak_king b){
	return a.r<b.r;
}
void add(int x,ll v){
	for(;x;x-=x&-x)
		c[x]=min(c[x],v);
}
ll ask(int x){
	ll ans=1e18;
	for(;x<=nn;x+=x&-x)
		Min(ans,c[x]);
	return ans;
}
int main(){
	freopen("gift.in","r",stdin);
	freopen("gift.out","w",stdout);
	ll T=read();
	for(;T--;){
		n=read(),m=read();
		For(i,1,n){
			p[i].v=read();
			p[i].r=read();
			p[i].t=read();
			Min(p[i].r,m);
			Min(p[i].v,m);
		}
		sort(p+1,p+n+1);
		if (m==1){		puts("0");	continue;	}
		if (p[1].r>1){	puts("-1");	continue;	}
		nn=0;
		For(i,1,n)q[++nn]=p[i].r,q[++nn]=p[i].v;
		sort(q+1,q+nn+1);	nn=unique(q+1,q+nn+1)-q-1;
		For(i,1,nn)c[i]=1e18;	c[1]=0;
		ll res;
		For(i,1,n){
			res=ask(lower_bound(q+1,q+nn+1,p[i].r)-q);
			add(lower_bound(q+1,q+nn+1,p[i].v)-q,res+p[i].t);
		}
		res=ask(nn);
		if (res>=1e18)	puts("-1");
		else			writeln(res);
	}
//	printf("%lf\n",1.0*clock()/CLOCKS_PER_SEC);
}
/*
*/
