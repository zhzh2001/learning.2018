#include<bits/stdc++.h>
using namespace std;
#define ll long long
ll read(){	ll x=0,f=1;	char ch=getchar();	for(;ch<'0'||ch>'9';ch=getchar())	if (ch=='-')	f=-1;	for(;ch>='0'&&ch<='9';ch=getchar())	x=x*10+ch-'0';	return x*f;	}
void write(ll x){	if (x<0)	putchar('-'),x=-x;	if (x>=10)	write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define rep(i,x,y)	for(ll i=x;i<y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define Add(x,y)	((x)=((x)+(y))%mod)
#define Mul(x,y)	((x)=((x)*(y))%mod)
#define Abs(x)		((x)<0?(-(x)):(x))
#define Fpn			freopen
#define IL			inline
IL void Max(ll &x,ll y){x=x<y?y:x;}
IL void Min(ll &x,ll y){x=x<y?x:y;}
IL ll max(ll x,ll y){return x<y?y:x;}
IL void Print(ll *a,ll x,ll y){	For(i,x,y)	write(a[i]),putchar(' ');	}
#define Min(x,y)	((x)=((x)<(y)?(x):(y)))
const ll N=10010;
ll a[N],n,k,d;
char s[N],t[N];
struct dt{ll a,b,c;}b[N];
bool operator <(dt a,dt b){
	return a.a==b.a?a.b<b.b:a.a<b.a;
}
void Dt_Ak_King(ll *a){
	rep(i,0,k)b[i]=(dt){i%d,i,a[i]};
	sort(b,b+k);
	rep(i,0,k)a[i]=b[i].c;
}
int main(){
	freopen("sorting.in","r",stdin);
	freopen("baoli.out","w",stdout);
	scanf("%s",s+1);	n=strlen(s+1);
	ll T=read();
	for(;T--;){
		k=read();	d=read();
		For(i,1,n)a[i]=i;
		For(i,1,n-k+1)Dt_Ak_King(a+i);
		For(i,1,n)t[i]=s[a[i]];
		For(i,1,n)s[i]=t[i];
		puts(s+1);
	}
}
/*
50......
*/
