#include<bits/stdc++.h>
using namespace std;
long long read()
{
	char ch=getchar();long long x=0,ff=1;
	while(ch<'0'||ch>'9') {if(ch=='-') ff=-1;ch=getchar();}
	while(ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x*ff;
}
void write(long long aa)
{
	if(aa<0) putchar('-'),aa=-aa;
	if(aa>9) write(aa/10);
	putchar('0'+aa%10);
	return;
}
const long long inf=0x3f3f3f3f3f3f3f3f;
long long T,n,m,tot,cnt;
long long val[100005],tt[400005];
struct lisan
{
	long long zhi,id;
}shu[100005];
struct node
{
	long long r,v,t;
}a[100005];
bool cmp(lisan aa,lisan bb)
{
	return aa.zhi<bb.zhi;
}
bool cmpp(node aa,node bb)
{
	return aa.v<bb.v;
}
void up(long long rt)
{
	tt[rt]=min(tt[rt<<1],tt[rt<<1|1]);
	return;
}
void bt(long long rt,long long ll,long long rr)
{
	if(ll==rr)
	{
		if(ll==1) tt[rt]=0;
		else tt[rt]=inf;
		return;
	}
	long long mid=(ll+rr)>>1;
	bt(rt<<1,ll,mid);
	bt(rt<<1|1,mid+1,rr);
	up(rt);
	return;
}
long long query(long long rt,long long ll,long long rr,long long L,long long R)
{
	if(ll==L&&rr==R) return tt[rt];
	long long mid=(ll+rr)>>1;
	if(R<=mid) return query(rt<<1,ll,mid,L,R);
	else if(L>mid) return query(rt<<1|1,mid+1,rr,L,R);
	else return min(query(rt<<1,ll,mid,L,mid),query(rt<<1|1,mid+1,rr,mid+1,R));
}
void update(long long rt,long long ll,long long rr,long long dd,long long kk)
{
	if(ll==rr) {tt[rt]=min(tt[rt],kk);return;}
	long long mid=(ll+rr)>>1;
	if(dd<=mid) update(rt<<1,ll,mid,dd,kk);
	else update(rt<<1|1,mid+1,rr,dd,kk);
	up(rt);
	return;
}
int main()
{
	freopen("gift.in","r",stdin);
	freopen("gift.out","w",stdout);
	T=read();
	while(T--)
	{
		tot=0;cnt=0;
		n=read(),m=read();//cout<<n<<" "<<m<<endl;
		for(long long i=1;i<=n;++i) 
		{
			a[i].v=read(),a[i].r=read(),a[i].t=read();
			shu[++tot]=((lisan){a[i].r,i});//cout<<a[i].r<<endl;
		}
		shu[++tot]=((lisan){1,0});
		shu[++tot]=((lisan){m,n+1});
		sort(shu+1,shu+tot+1,cmp);
		shu[0].zhi=shu[1].zhi-1;
		for(long long i=1;i<=tot;++i)
		{
			if(shu[i].zhi!=shu[i-1].zhi) val[++cnt]=shu[i].zhi;
			a[shu[i].id].r=cnt;
		}
		val[cnt+1]=inf;
		sort(a+1,a+n+1,cmpp);
		bt(1,1,cnt);
		long long now=1;
		for(long long i=1;i<=cnt;++i)
		{
			long long mx=inf;
			while(now<=n&&a[now].v>=val[i]&&a[now].v<val[i+1])
			{
				mx=min(mx,query(1,1,cnt,a[now].r,i)+a[now].t);
				now++;
			}
			update(1,1,cnt,i,mx);
		}
		long long tmp=query(1,1,cnt,cnt,cnt);
		if(tmp>=inf) puts("-1");
		else write(tmp),puts("");
	}
	return 0;
}
