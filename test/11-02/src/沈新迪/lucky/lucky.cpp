#include<bits/stdc++.h>
using namespace std;
long long read()
{
	char ch=getchar();long long x=0,ff=1;
	while(ch<'0'||ch>'9') {if(ch=='-') ff=-1;ch=getchar();}
	while(ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	return x*ff;
}
void write(long long aa)
{
	if(aa<0) putchar('-'),aa=-aa;
	if(aa>9) write(aa/10);
	putchar('0'+aa%10);
	return;
}
long long T,n,mod=9223372036854775807;
unsigned long long qwq;
long long a[105],f[105];
int main()
{
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
	T=read();
	while(T--)
	{
		n=read();
		for(long long i=1;i<=n;++i) a[i]=read();
		f[1]=min(a[1],mod^a[1]);
		for(long long i=2;i<=n;++i)
		{
			f[i]=min(f[i-1]&a[i],f[i-1]&(mod^a[i]));
		}
		write(f[n]);puts("");
	}
	return 0;
}
