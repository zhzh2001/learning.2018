#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read() {
	register int x=0,f=1;
	register char ch=getchar();
	while(ch<'0'||ch>'9') {
		if(ch=='-')f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
	return x*f;
}
inline void write(int x) {
	if(x>=10) write(x/10);
	register char ch=x%10+'0';
	putchar(ch);
}
struct node {
	int x,y,z;
	friend bool operator < (node a,node b) {
		if(a.x==b.x && a.y==b.y) return a.z<b.z;
		else if(a.x==b.x) return a.y<b.y;
		else return a.x<b.x;
	}
} bian[240000];
int t,n,m,ans,cnt;
set <int> q;
map <int,int> mp;
int dis[1200000];
struct tt {
	int mn,mnat;
} tree[5600000];
int gg[5600000],hh[5600000];
bool one;
//2333333333333333333333333333333333333333333333333333333333333333333333333333333333333333

void build(int rt,int l,int r) {
	if(l==r) {
		tree[rt].mn=dis[l];
		tree[rt].mnat=l;
		return;
	}
	int mid=(l+r)>>1;
	build(rt<<1,l,mid);
	build(rt<<1|1,mid+1,r);
//	tree[rt]=max(tree[rt<<1],tree[rt<<1|1]);
	if(tree[rt<<1].mn<tree[rt<<1|1].mn) {
		tree[rt]=tree[rt<<1];
	} else tree[rt]=tree[rt<<1|1];
}
void pushdown(int rt,int l,int r) {
	if(gg[rt]) {
		gg[rt<<1]=gg[rt];
		gg[rt<<1|1]=gg[rt];
		hh[rt<<1]=hh[rt];
		hh[rt<<1|1]=hh[rt];
//		tree[rt<<1].=gg[rt];
//		tree[rt<<1|1]=gg[rt];
		if(gg[rt]<tree[rt<<1].mn) {
			tree[rt<<1].mn=gg[rt];
			tree[rt<<1].mnat=hh[rt];	
		}
		if(gg[rt]<tree[rt<<1|1].mn) {
			tree[rt<<1|1].mn=gg[rt];
			tree[rt<<1|1].mnat=hh[rt];	
		}
		gg[rt]=hh[rt]=0;
	}	
}
void gai(int which,int C,int l,int r,int rt) {
//	cout<<"gai "<<which<<" "<<C<<" "<<l<<" "<<r<<" "<<rt<<'\n';
	if(l==r && l==which) {
		tree[rt].mn=C;
		tree[rt].mnat=l;
		gg[rt]=C;
		hh[rt]=which;
		return;
	} else if(l==r) return;
	int mid=(l+r)>>1;
	pushdown(rt,mid-l+1,r-mid);
	if(which<=mid) gai(which,C,l,mid,rt<<1);
	else gai(which,C,mid+1,r,rt<<1|1);
//	tree[rt]=max(tree[rt<<1],tree[rt<<1|1]);
	if(tree[rt<<1].mn<tree[rt<<1|1].mn) {
		tree[rt]=tree[rt<<1];
	} else {
		tree[rt]=tree[rt<<1|1];
	}
	
}
tt mn(int L,int R,int l,int r,int rt) {
//	cout<<"mn:"<<L<<" "<<R<<" "<<l<<" "<<r<<" "<<rt<<'\n';
	if(L<=l && r<=R) {
		return tree[rt];
	}
	int mid=(l+r)>>1;
	pushdown(rt,mid-l+1,r-mid);
	int xkx=1e18;
	tt now;
	if(L<=mid && mn(L,R,l,mid,rt<<1).mn<xkx) {
		xkx=mn(L,R,l,mid,rt<<1).mn;
		now=mn(L,R,l,mid,rt<<1);
	} else if(R>mid && mn(L,R,mid+1,r,rt<<1|1).mn<xkx) {
		xkx=mn(L,R,mid+1,r,rt<<1|1).mn;
		now=mn(L,R,mid+1,r,rt<<1|1);
	}
	return now;
}

//233333333333333333333333333333333333333333333333333333333333333333333333333333333333333333
void calc() {
	sort(bian+1,bian+n+1);
	for(register int i=1; i<=n; i++) {
		if(bian[i].x>m) break;
		register int mn=1e18,mnat;
		for(register int j=bian[i].x; j<=cnt; j++)
			if(dis[j] || j==1)
				if(mn>dis[j]) {
					mn=dis[j];
					mnat=j;
				}
//		for(int j=1;j<=cnt;j++) cout<<dis[j]<<" ";
//		cout<<'\n';
		if(!dis[bian[i].y]) dis[bian[i].y]=dis[mnat]+bian[i].z;
		else dis[bian[i].y]=min(dis[bian[i].y],dis[mnat]+bian[i].z);
		if(bian[i].y>m) ans=min(ans,dis[bian[i].y]);
	}
}
void calc2() {
	for(int i=1;i<=1200000;i++) dis[i]=1e18;
	dis[1]=0;
	build(1,1,1200000);
	sort(bian+1,bian+n+1);
	for(register int i=1; i<=n; i++) {
		register int sxd=1e18,zyh;
		if(bian[i].x>m) break;
		tt mm=mn(bian[i].x,cnt,1,n,1);
		sxd=mm.mn,zyh=mm.mnat; 
		
//		for(int j=1;j<=cnt;j++) cout<<dis[j]<<" ";
//		cout<<'\n';
//		if(i==2) exit(0);
//		cout<<bian[i].z;
		if(dis[bian[i].y]==1e18) dis[bian[i].y]=dis[zyh]+bian[i].z;
		else dis[bian[i].y]=min(dis[bian[i].y],dis[zyh]+bian[i].z);
		gai(bian[i].y,dis[bian[i].y],1,n,1);
//		if(i==2) exit(0);
		if(bian[i].y>m) ans=min(ans,dis[bian[i].y]);
	}
}
signed main() {
	freopen("gift.in","r",stdin);
	freopen("gift.out","w",stdout);
	t=read();
	while(t--) {
		n=read(),m=read();
		memset(bian,0,sizeof bian);
		one=true;
		q.clear();
		mp.clear();
		q.insert(m);
		q.insert(1);
		for(register int i=1; i<=n; i++) {
			bian[i].y=read();
			bian[i].x=read();
			bian[i].z=read();
			if(bian[i].z!=1) one=false;
			q.insert(bian[i].y);
			q.insert(bian[i].x);
		}
		cnt=0;
		for(register set <int> :: iterator it=q.begin(); it!=q.end(); it++) {
			mp[*it]=++cnt;
		}
		for(register int i=1; i<=n; i++) {
			bian[i].x=mp[bian[i].x];
			bian[i].y=mp[bian[i].y];
		}
		m=mp[m];
		ans=1e18;
		calc();
//		if(n<=1500) calc();
//		calc2();
		if(ans==1e18) cout<<"-1";
		else write(ans);
		puts("");
	}
}
