#include<bits/stdc++.h>
#define int long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
inline int read() {
	register int x=0,f=1;
	register char ch=getchar();
	while(ch<'0'||ch>'9') {
		if(ch=='-')f=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
	return x*f;
}
void write(int x) {
	if(x>=10) write(x/10);
	register char ch=x%10+'0';
	putchar(ch);
}
int t,n,a[120],b[120],ans;
void dfs(int at,int now) {
	if(ans==0) return;
	int w=now;
	if(at>n) ans=min(ans,abs(now));
	else {
		dfs(at+1,now|b[at]);
		dfs(at+1,now&b[at]);
		dfs(at+1,now^b[at]);
	}
}

void subtask1() {
	for(register int i=0; i<(1<<n); i++) {
		for(register int k=1; k<=n; k++) b[k]=a[k];
		for(register int k=1; k<=n; k++) {
			if(i & (1<<(k-1))) b[k]=~b[k];
		}
		register int now=b[1];
		dfs(2,now);
	}
}

signed main() {
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
	t=read();
	while(t--) {
		n=read();
		ans=2e18;
		for(register int i=1; i<=n; i++) {
			a[i]=read();
		}
		subtask1();
		write(ans);
		puts("");
	}
	return 0;
}
