#include <iostream>
#include <cstring>
#include <cstdio>
#include <queue>
#define int long long
using namespace std;
inline int read(){int tot=1;char c=getchar();while(c!='-'&&(c<'0'||c>'9'))c=getchar();if(c=='-')tot=-1,c=getchar();
int sum=0;while(c<='9'&&c>='0')sum=sum*10+c-'0',c=getchar();return sum*tot;}
void wr(int x){if(x<0){putchar('-');wr(-x);return;}if(x>=10)wr(x/10);putchar(x%10+'0');}
void wrn(int x){wr(x);putchar('\n');}
const int N=10005;
/*struct arr{
	int rt,dis;
	bool operator < (const arr& b) const{
		return dis>b.dis;
	}
};
priority_queue<arr> q;*/
int n,m,pos,head[N],nxt[N],to[N],w[N],dis[3000],vis[3000],r[N],t[N],v[N],ru[N],d[N];
void add(int u,int y,int ww){
	nxt[++pos]=head[u];
	to[pos]=y;w[pos]=ww;
	head[u]=pos;
	ru[y]++;
}
/*void dij(){
	memset(vis,0,sizeof vis);
	vis[n+1]=1; dis[n+1]=0;
	q.push((arr){n+1,0});
	while(!q.empty()){
		arr u=q.top();q.pop();
		for(int i=head[u.rt];i;i=nxt[i]){
			int j=to[i];
			if(!vis[j]){
				vis[j]=1;
				dis[j]=min(dis[j],dis[u.rt]+w[i]);
				q.push((arr){j,dis[j]});
				vis[j]=1;
			}
		}
//		vis[j]=0;
	}
}*/
void dijkstra(){
	memset(vis,0,sizeof vis);
	for(int i=1;i<=n;i++)
	{
		int kkk,mn=1000000000;
		for(int j=1;j<=n;j++)
		{
			if(!vis[j]&&dis[j]<mn)
			{
			    kkk=j;
                mn=dis[j];
			}
		}
//		cout<<"kkk"<<kkk<<endl;
        vis[kkk]=1;
        for(int k=head[kkk];k;k=nxt[k]){
        	int j=to[k];
//        	cout<<"aaa"<<j<<" "<<dis[j];
			if(!vis[k]&&dis[k]>dis[kkk]+w[k]) dis[k]=dis[kkk]+w[k];
//			cout<<" "<<dis[j]<<endl;
        }
	}
}
queue<int> q;
signed main()
{
	freopen("gift.in","r",stdin);
	freopen("gift.out","w",stdout);
	int T=read();
	while(T--){
		int ans=1000000000;
		n=read(),m=read();int flag=1;
		memset(ru,0,sizeof ru);
		memset(head,0,sizeof head);pos=0;
		for(int i=1;i<=n;i++)dis[i]=1000000000;
		for(int i=1;i<=n;i++){
			v[i]=read();r[i]=read();t[i]=read();
			if(t[i]!=1)flag=0;
		}
		for(int i=1;i<=n;i++){
			if(r[i]<=1)add(n+1,i,t[i]), dis[i]=t[i];//,cout<<"KKK"<<n+1<<" "<<i<<" "<<endl;
		}
//		for(int i=1;i<=n;i++)cout<<dis[i]<<" ";puts("");
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				if(i!=j&&r[i]<=v[j]&&v[i]>v[j]){
//					cout<<"KKK"<<j<<" "<<i<<" "<<endl;
					add(j,i,t[i]); 
				}
		/*if(flag==1){
			for(int i=1;i<=n+1;i++)if(!r[i])q.push(i),vis[i]=1;//,cout<<i<<endl;;
			while(!q.empty()){
				int u=q.front();q.pop();
				for(int i=head[u];i;i=nxt[i]){
					int j=to[i];
					if(!vis[j]){
						d[j]=d[u]+1;
						vis[j]=1;
						q.push(j);
					}
				}
			}
//			for(int i=1;i<=n;i++)cout<<d[i]<<" ";puts("");
			for(int i=1;i<=n+1;i++){
				if(v[i]>=m){
					ans=min(ans,d[i]);
				}
				d[i]=0;vis[i]=0;
			}
			cout<<ans<<endl;
			continue;
		}
//		into();
		/*for(int u=1;u<=n+1;u++){
		    puts("aaa");for(int i=head[u];i;i=nxt[i])cout<<to[i]<<" ";puts("");
		}*/
		dijkstra();
		flag=0;
//		for(int i=1;i<=n;i++)cout<<dis[i]<<" ";puts("");
		for(int i=1;i<=n;i++){
			if(v[i]>=m){
				ans=min(ans,dis[i]); flag=1;
			}
		}
		if(flag&&ans!=1000000000)wrn(ans);
		else puts("-1");
	}
	return 0;
}
/*
1
4 5
2 1 1
3 2 1
4 3 1
8 4 1
*/
