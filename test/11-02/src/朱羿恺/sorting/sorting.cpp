#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 1e6+10;
int n,m,k[N],d[N];
char s[N];
namespace Subtask1{
	struct node{
		char c;int pos,Pos;
	}t[110];
	inline bool cmp(node a,node b){return a.Pos==b.Pos?a.pos<b.pos:a.Pos<b.Pos;}
	inline void Main(){
		while (m--){
			int k=read(),d=read();
			For(i,0,n-k){
				For(j,i,i+k-1) t[j-i].c=s[j],t[j-i].pos=j-i,t[j-i].Pos=(j-i)%d;
				sort(t,t+k,cmp);
				For(j,i,i+k-1) s[j]=t[j-i].c;
			}
			printf("%s\n",s);
		}
	}
}
namespace Subtask2{
	int pos[3010],t[3010];
	char S[3010];
	inline void Main(){
		For(i,0,n-1) pos[i]=i;
		For(i,0,n-k[1]){
			int x=i,y=i;
			For(j,1,k[1]){
				t[j]=pos[x];
				if (x+d[1]>i+k[1]-1) x=++y;
					else x+=d[1]; 
			}
			For(j,1,k[1]) pos[i+j-1]=t[j];
		}
		For(i,1,m){
			For(j,0,n-1) S[j]=s[j];
			For(j,0,n-1) s[j]=S[pos[j]];
			printf("%s\n",s);
		}
	}
}
int pos[N],t[N];
int main(){
	freopen("sorting.in","r",stdin);
	freopen("sorting.out","w",stdout);
	scanf("%s",s),n=strlen(s);
	m=read();
	if (n<=100&&m<=100) return Subtask1::Main(),0;
	For(i,1,m) k[i]=read(),d[i]=read();
	bool flag=1;
	For(i,2,m) if (k[i]!=k[i-1]||d[i]!=d[i-1]) flag=0;
	if (flag) return Subtask2::Main(),0;
}
