#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef unsigned long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 110;
int T,n;
ll a[N],ans;
namespace Subtask1{
	bool flag[N];
	ll b[N],p[100];
	inline void Dfs(int k,ll ret){
		if (k>n){ans=min(ans,ret);return;}
		Dfs(k+1,ret&(!flag[k]?a[k]:b[k])),Dfs(k+1,ret|(!flag[k]?a[k]:b[k])),Dfs(k+1,ret^(!flag[k]?a[k]:b[k]));
	}
	inline void dfs(int k){
		if (k>n){Dfs(2,!flag[1]?a[1]:b[1]);return;}
		flag[k]=1,dfs(k+1);
		flag[k]=0,dfs(k+1);
	}
	inline void Main(){
		p[0]=1;For(i,1,63) p[i]=p[i-1]*2;
		For(i,1,n){
			a[i]=read(),b[i]=0;
			For(j,1,64) if (!(a[i]&p[j-1])) b[i]+=p[j-1];	
		}
		ans=a[1];For(i,2,n) ans&=a[i];
		dfs(1);
		printf("%lld\n",ans);
	}
}
int main(){
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
	T=read();
	while (T--){
		n=read();
		if (n<=6) Subtask1::Main();
		else {
			For(i,1,n) a[i]=read();
			puts("0");
		}
	}
}
