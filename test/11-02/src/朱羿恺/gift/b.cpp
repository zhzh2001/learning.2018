#include<bits/stdc++.h>
#define For(i,x,y) for (register int i=(x);i<=(y);i++)
#define Dow(i,x,y) for (register int i=(x);i>=(y);i--)
#define cross(i,k) for (register int i=first[k];i;i=last[i])
using namespace std;
typedef long long ll;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
const int N = 2e5+10;
int T,n,m;
bool flag;
struct node{
	int r,l,t;
}a[N];
namespace Subtask1{
	const int M = 3e6+10;
	int tot,first[1510],last[M],to[M];
	ll val[M];
	inline void Add(int x,int y,int z){to[++tot]=y,val[tot]=z,last[tot]=first[x],first[x]=tot;}
	struct Node{
		int u;ll dis;
		inline bool operator < (const Node &b)const{return dis>b.dis;}
	};
	priority_queue<Node>q;
	ll dis[N];
	bool vis[N];
	inline void Dij(int s){
		For(i,0,n) dis[i]=1e18,vis[i]=0;
		dis[0]=0,q.push((Node){0,0});
		while (!q.empty()){
			int u=q.top().u;q.pop();//printf("%d\n",u);
			if (vis[u]) continue;vis[u]=1;
			cross(i,u) if (dis[u]+val[i]<dis[to[i]]) dis[to[i]]=dis[u]+val[i],q.push((Node){to[i],dis[to[i]]});
		}
	}
	inline void Main(){
		tot=0,memset(first,0,sizeof first);//puts("");
		For(i,1,n) if (a[i].l<=1) Add(0,i,a[i].t);//,printf("%d %d %d\n",0,i,a[i].t);puts("");
		For(i,1,n) For(j,1,n) if (a[i].r>=a[j].l&&a[j].r>a[i].r) Add(i,j,a[j].t);//,printf("%d %d %d\n",i,j,a[j].t);
		Dij(0);ll ans=1e18;
		For(i,1,n) if (a[i].r>=m) ans=min(ans,dis[i]);
		//For(i,1,n) printf("%lld ",dis[i]);puts("");
		if (ans==1e18) puts("-1");
			else printf("%lld\n",ans);
	}
}
namespace Subtask2{
	inline bool cmp(node a,node b){return a.l<b.l;}
	int Max[N];
	inline int Find(int x){
		int l=1,r=n,mid,ans=0;
		while (l<=r){
			mid=l+r>>1;
			if (a[mid].l<=x) ans=mid,l=mid+1;
				else r=mid-1;
		}
		return ans;
	}
	inline void Main(){
		sort(a+1,a+1+n,cmp);
		For(i,1,n) Max[i]=max(Max[i-1],a[i].r);
		int now=1,ans=0;
		while (now<m){
			int x=Find(now);
			if (Max[x]<=x){ans=-1;break;}
			now=Max[x],ans++;
		}
		printf("%d\n",ans);
	}
}
int cnt,c[N];
inline bool cmp(node a,node b){return a.l<b.l;}
inline void init(){
	sort(a+1,a+1+n,cmp);cnt=0;
	For(i,1,n) c[++cnt]=a[i].l,c[++cnt]=a[i].r;
	sort(c+1,c+1+cnt);int Cnt=unique(c+1,c+1+cnt)-c-1;
	For(i,1,n) a[i].l=lower_bound(c+1,c+1+Cnt,a[i].l)-c,a[i].r=lower_bound(c+1,c+1+Cnt,a[i].r)-c;
}
ll ans,dp[N];
struct SegMengTree{
	ll Min[N<<2],lazy[N<<2];
	inline void Build(int u,int l,int r){
		Min[u]=1e18,lazy[u]=1e18;
		if (l==r) return;int mid=l+r>>1;
		Build(u<<1,l,mid),Build(u<<1^1,mid+1,r);
	}
	inline void push_up(int u){Min[u]=min(Min[u<<1],Min[u<<1^1]);}
	inline void push_down(int u){
		if (lazy[u]==1e18) return;
		int ls=u<<1,rs=u<<1^1;
		Min[ls]=min(Min[ls],lazy[u]),Min[rs]=min(Min[rs],lazy[u]);
		lazy[ls]=min(lazy[ls],lazy[u]),lazy[rs]=min(lazy[rs],lazy[u]);
		lazy[u]=1e18;
	}
	inline void update(int u,int l,int r,int ql,int qr,ll x){
		if (l>=ql&&r<=qr){Min[u]=min(Min[u],x),lazy[u]=min(lazy[u],x);return;}
		int mid=l+r>>1;push_down(u);
		if (qr<=mid) update(u<<1,l,mid,ql,qr,x);
		else if (ql>mid) update(u<<1^1,mid+1,r,ql,qr,x);
		else update(u<<1,l,mid,ql,qr,x),update(u<<1^1,mid+1,r,ql,qr,x);
		push_up(u);
	}
	inline ll Query(int u,int l,int r,int ql,int qr){
		if (l>=ql&&r<=qr) return Min[u];
		int mid=l+r>>1;push_down(u);
		if (qr<=mid) return Query(u<<1,l,mid,ql,qr);
		else if (ql>mid) return Query(u<<1^1,mid+1,r,ql,qr);
		else return min(Query(u<<1,l,mid,ql,qr),Query(u<<1^1,mid+1,r,ql,qr));
	}
}t;
inline void solve(){
	init(),t.Build(1,1,cnt);
	if (a[1].l>1){puts("-1");return;}
	else {
		For(i,1,n){
			if (a[i].l>1) break;
			dp[i]=a[i].t,t.update(1,1,cnt,1,a[i].r,dp[i]);
		}
	}
	For(i,2,n){
		dp[i]=t.Query(1,1,cnt,a[i].l,cnt)+a[i].t;
		t.update(1,1,cnt,1,a[i].r,dp[i]);
	}
	ans=1e18;
	For(i,1,n) if (c[a[i].r]>=m) ans=min(ans,dp[i]);
	if (ans==1e18) puts("-1");
		else printf("%lld\n",ans);
}
int main(){
	freopen("gift.in","r",stdin);
	freopen("b.out","w",stdout);
	T=read();
	while (T--){
		n=read(),m=read(),flag=1;
		For(i,1,n) a[i]=(node){read(),read(),read()},flag&=(a[i].t==1);
		if (n<=1500){Subtask1::Main();continue;}
		if (flag){Subtask2::Main();continue;}
	//	solve();
	}
}
/*
1
5 9
3 1 1
4 2 1
8 3 1
9 8 1
8 4 1
*/
