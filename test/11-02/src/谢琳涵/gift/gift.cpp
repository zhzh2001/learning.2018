#include<bits/stdc++.h>
#define ll long long
using namespace std;
struct aaa{
	ll x,y,v;
}a[1000001];
ll ans,t1,n,m,n1,m1,aa[1000001],f[3000001];
map<ll,ll>mp;
inline ll read(){
    ll x=0;int ch=getchar(),f=1;
    while (!isdigit(ch)&&(ch!='-')&&(ch!=EOF)) ch=getchar();
    if (ch=='-'){f=-1;ch=getchar();}
    while (isdigit(ch)){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
    return x*f;
}
bool cmp(aaa a,aaa b){
	return a.y<b.y;
}
void build(ll x,ll l,ll r){
	if(l==r){
		f[x]=1e16;
		return;
	}
	int mid=(l+r)/2;
	build(x*2,l,mid);build(x*2+1,mid+1,r);
	f[x]=1e16;
}
void ins(ll x,ll l,ll r,ll t,ll k){
	if(l==r){
		f[x]=min(f[x],k);
		return;
	}
	ll mid=(l+r)/2;
	if(mid>=t)ins(x*2,l,mid,t,k);
	 else ins(x*2+1,mid+1,r,t,k);
	f[x]=min(f[x*2],f[x*2+1]); 
}
ll query(ll x,ll l,ll r,ll t,ll k){
	if(l==t&&r==k)return f[x];
	ll mid=(l+r)/2;
	if(mid>=k)return query(x*2,l,mid,t,k);
	 else if(t>mid)return query(x*2+1,mid+1,r,t,k); 
	  else return min(query(x*2,l,mid,t,mid),query(x*2+1,mid+1,r,mid+1,k));
}
int main(){
freopen("gift.in","r",stdin);
freopen("gift.out","w",stdout);	
	ll i,now,mn;
	scanf("%lld",&t1);
	while(t1--){
		scanf("%lld%lld",&n,&m);
		mn=1e16;
		n1=2;aa[1]=1;aa[2]=m;m1=0;
		for(i=1;i<=n;i++){
		 //scanf("%lld%lld%lld",&a[i].y,&a[i].x,&a[i].v);
		 a[i].y=read();a[i].x=read();a[i].v=read();
		 aa[++n1]=a[i].x;aa[++n1]=a[i].y;
		 mn=min(mn,a[i].x);
	}
	   if(mn>1){
	   	puts("-1");continue;
	   }
	   sort(aa+1,aa+n1+1);
	   for(i=1;i<=n1;i++){
	   	if(i==1||aa[i]>aa[i-1])m1++;
	   	mp[aa[i]]=m1;
	   }
	   for(i=1;i<=n;i++){
	   	a[i].x=mp[a[i].x];a[i].y=mp[a[i].y];
	   }
	   sort(a+1,a+n+1,cmp);
	   build(1,1,m1);
	   ins(1,1,m1,1,0);
	   for(i=1;i<=n;i++){
	   	now=query(1,1,m1,a[i].x,a[i].y)+a[i].v;
	   	if(now>=1e16)now=1e16;
	   	ins(1,1,m1,a[i].y,now);
	   }
	   ans=query(1,1,m1,mp[m],m1);
	   if(ans>=1e16)puts("-1");
	    else printf("%lld\n",ans);
	}
}

/*
3
5 9
5 1 1
10 4 10
8 1 10
11 6 1
7 3 8
4 5
2 1 1
3 2 1
4 3 1
8 4 1
3 10
5 1 3
8 2 5
10 9 2
*/
