#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll unsigned long long
#define int ll
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0){x=-x;putchar('-');}if(x>=10)	write(x/10);putchar('0'+x%10);}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}
const int N=205;
ll ans=8e18,a[N],b[N];
int n;
inline void Dfs(int now,ll val)
{
	if(val==0)	{ans=0;return;}
	if(now==n+1){if(ans==8e18)	ans=val;else	ans=min(ans,val);return;}
	Dfs(now+1,val&a[now]);Dfs(now+1,val&b[now]);
	Dfs(now+1,val^a[now]);Dfs(now+1,val^b[now]);
	Dfs(now+1,val|a[now]);Dfs(now+1,val|b[now]);
}
signed main()
{
	freopen("lucky.in","r",stdin);freopen("lucky.out","w",stdout);
	int T=read();
	while(T--)
	{
		n=read();
		For(i,1,n)	a[i]=read();
		ans=8e18;
		if(n>=8){puts("0");continue;}
		For(i,1,n)	b[i]=~a[i];
		Dfs(2,a[1]);Dfs(2,b[1]);
		writeln(ans);
	}
}
