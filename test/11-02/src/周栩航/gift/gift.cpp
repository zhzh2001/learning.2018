#include<bits/stdc++.h>
#define For(i,j,k)	for(int i=j;i<=k;++i)
#define Dow(i,j,k)	for(int i=k;i>=j;--i)
#define ll long long
#define pa pair<ll,int>
#define fir first
#define sec second
#define mk make_pair
#define inf 1e18
using namespace std;
inline ll read()
{
	ll t=0,f=1;char c=getchar();
	while(!isdigit(c))	{if(c=='-')	f=-1;c=getchar();}
	while(isdigit(c))	t=t*10+c-'0',c=getchar();
	return t*f;
}
inline void write(ll x){if(x<0){x=-x;putchar('-');}if(x>=10)	write(x/10);putchar('0'+x%10);}
inline void writeln(ll x){write(x);puts("");}
inline void write_p(ll x){write(x);putchar(' ');}

const int N=5e5+5;
int n,m,r[N],t[N],poi[N*2],nxt[N*2],head[N],cnt,top,q[N];
ll ev[N*2],v[N],dis[N];
bool vis[N];
inline void add(int x,int y,int z){poi[++cnt]=y;nxt[cnt]=head[x];head[x]=cnt;ev[cnt]=z;}
inline void Build()
{
	For(i,1,n)	
	{
		int to=lower_bound(q+1,q+top+1,r[i])-q;
		add(n+to,i,0);
		to=upper_bound(q+1,q+top+1,v[i])-q-1;
		add(i,n+to,t[i]);
	}
}
priority_queue<pa> Q;
inline void Dij()
{
	For(i,1,n+top)	dis[i]=inf,vis[i]=0;
	Q.push(mk(0,n+1));dis[n+1]=0;
	int e=lower_bound(q+1,q+top+1,m)-q;
	e+=n;
	while(!Q.empty())
	{
		pa tmp=Q.top();Q.pop();
		if(vis[tmp.sec])	continue;
		vis[tmp.sec]=1;
		for(int i=head[tmp.sec];i;i=nxt[i])
			if(!vis[poi[i]])
			{
				if(dis[tmp.sec]+ev[i]<dis[poi[i]])
					dis[poi[i]]=dis[tmp.sec]+ev[i],Q.push(mk(-dis[poi[i]],poi[i]));
			}
	}
	if(dis[e]==inf)	puts("-1");else	writeln(dis[e]);
}
signed main()
{
	freopen("gift.in","r",stdin);freopen("gift.out","w",stdout);
	int T=read();
	while(T--)
	{
		n=read();m=read();
		top=0;
		For(i,1,n)
		{
			v[i]=read();r[i]=read();t[i]=read();
			q[++top]=r[i];
		}
		q[++top]=m;
		For(i,1,n+top)	head[i]=0;cnt=0;
		sort(q+1,q+top+1);
		For(i,n+1,n+top-1)	add(i+1,i,0);
		Build();
		if(q[1]>1){puts("-1");continue;}
		Dij();
	}
}
