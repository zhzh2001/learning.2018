#include <cstdio>
#include <cctype>
#include <cstring>
#include <algorithm>
int read(){
	register int x = 0;
	register char f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = 0;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
int n, m;
char s[1000005];
namespace Brute_Force{
	char b[1000005];
	void Main(){
		for (register int _ = 1; _ <= m; ++_){
			int k = read(), d = read();
			for (register int l = 0, r = k - 1; r < n; ++l, ++r){
				int len = 0, pos = l;
				for (register int i = l; i <= r; ++i) b[len++] = s[i];
				for (register int i = 0; i < d; ++i)
					for (register int j = i; j < len; j += d)
						s[pos++] = b[j];
			}
			printf("%s\n", s);
		}
	}
}
namespace K_eq_D{
	int a[1000005], b[1000005];
	char S[1000005];
	void Main(){
		int k = read(), d = read();
		for (register int i = 0; i < n; ++i) a[i] = i;
		for (register int l = 0, r = k - 1; r < n; ++l, ++r){
			int len = 0, pos = l;
			for (register int i = l; i <= r; ++i) b[len++] = a[i];
			for (register int i = 0; i < d; ++i)
				for (register int j = i; j < len; j += d)
					a[pos++] = b[j];
		}
		for (register int _ = 1; _ <= m; ++_){
			for (register int i = 0; i < n; ++i) S[i] = s[a[i]];
			printf("%s\n", S);
			memcpy(s, S, sizeof s);
		}
	}
}
int main(){
	freopen("sorting.in", "r", stdin);
	freopen("sorting.out", "w", stdout);
	scanf("%s", s), n = strlen(s);
	m = read();
	if (1ll * n * n * m > 1e8) K_eq_D :: Main();
	else Brute_Force :: Main();
}

