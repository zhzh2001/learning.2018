#include <cstdio>
#include <cctype>
#include <cstring>
#include <algorithm>
unsigned long long read(){
	register unsigned long long x = 0;
	register char ch = getchar();
	for (; !isdigit(ch); ch = getchar()) ;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return x;
}
int T, n;
unsigned long long a[105], s, ans;
void print(unsigned long long x){
	if (x < 10) putchar(x + 48);
	else print(x / 10), putchar(x % 10 + 48);
}
int main(){
	freopen("lucky.in", "r", stdin);
	freopen("lucky.out", "w", stdout);
	scanf("%d", &T);
	while (T--){
		scanf("%d", &n);
		for (register int i = 0; i < n; ++i) a[i] = read();
		if (n <= 17){
			ans = a[0];
			for (register int p = 0; p < (1 << n); ++p){
				if (!ans) break;
				for (register int i = 0; i < n; ++i)
					if ((p >> i) & 1) a[i] = ~a[i];
				s = a[0];
				for (register int i = 1; i < n; ++i) s &= a[i];
				for (register int i = 0; i < n; ++i)
					if ((p >> i) & 1) a[i] = ~a[i];
				ans = std :: min(ans, s);
			}
		}
		else{
			ans = a[0];
			for (register int i = 1; i < n; ++i) ans = std :: min(ans & a[i], ans & ~a[i]);
		}
		print(ans), putchar('\n');
	}
}

