#include <cstdio>
#include <cctype>
#include <cstring>
#include <algorithm>
int read(){
	register int x = 0;
	register char f = 1, ch = getchar();
	for (; !isdigit(ch); ch = getchar()) if (ch == '-') f = 0;
	for (; isdigit(ch); ch = getchar()) x = (x << 1) + (x << 3) + (ch ^ '0');
	return f ? x : -x;
}
#define N 100005
int T, n, c;
struct frd{
	int v, r, t;
	bool operator < (const frd b) const {
		return r < b.r;
	}
}a[N];
struct node{
	long long m, t;
	bool operator < (const node &b) const {
		return t > b.t || t == b.t && m < b.m;
	}
};
struct Heap{
	node h[N << 2];
	int sz;
	void clear(){ sz = 0; }
	bool empty(){ return !sz; }
	void push(node a){ h[++sz] = a, std :: push_heap(h + 1, h + 1 + sz); }
	node pop(){ return std :: pop_heap(h + 1, h + 1 + sz), h[sz--]; }
	node top(){ return h[1]; }
}H;
int main(){
	freopen("gift.in", "r", stdin);
	freopen("gift.out", "w", stdout);
	T = read();
	while (T--){
		n = read(), c = read();
		for (register int i = 1; i <= n; ++i) a[i] = (frd){read(), read(), read()};
		std :: sort(a + 1, a + 1 + n);
		H.clear(), H.push((node){1, 0});
		bool flag = 0;
		for (register int i = 1; !H.empty(); ){
			node p = H.pop();
			if (p.m >= c){ printf("%lld\n", p.t); flag = 1; break; }
			for (; i <= n && p.m >= a[i].r; ++i) H.push((node){a[i].v, p.t + a[i].t});
		}
		if (!flag) printf("-1\n");
	}
}
