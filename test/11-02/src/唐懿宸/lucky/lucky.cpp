#include<bits/stdc++.h>
using namespace std;

namespace TYC
{
	typedef unsigned long long ull;
	const int N=110;
	const ull inf=(1ULL<<61);
	int n;
	ull ans=inf,a[N]; 
	
	inline int read_int()
	{
		int x=0;int f=0;char ch=getchar();
		while(!isdigit(ch)) f|=(ch=='-'),ch=getchar();
		while(isdigit(ch)) x=x*10+ch-'0',ch=getchar();
		return f?-x:x;
	}
	
	inline ull read()
	{
		ull x=0;int f=0;char ch=getchar();
		while(!isdigit(ch)) f|=(ch=='-'),ch=getchar();
		while(isdigit(ch)) x=x*10+ch-'0',ch=getchar();
		return f?-x:x;
	}
	
	inline ull calc(ull x,ull y,int tag)
	{
		switch(tag)
		{
			case 1: return x&y;
			case 2: return x|y;
			case 3: return x^y;
		}
	}
	
	void place(int now,ull sum)
	{
		if(now==n+1) {ans=min(ans,sum);return;}
		place(now+1,calc(sum,a[now],1));
		place(now+1,calc(sum,a[now],2));
		place(now+1,calc(sum,a[now],3));
	}
	
	void dfs(int i)
	{
		if(i==n+1) {place(2,a[1]);return;}
		a[i]=(~a[i]);
		dfs(i+1);
		a[i]=(~a[i]);
		dfs(i+1);
	}
	
	void work()
	{
		int T=read_int();
		while(T--)
		{
			ans=inf;
			n=read_int();
			for(int i=1;i<=n;i++) a[i]=read();
			if(n==1)
			{
				printf("%llu\n",min(a[1],(~a[1])));
				continue;
			}
			if(n<=7) dfs(1);
			else ans=0;
			printf("%llu\n",ans);
		}
	}
}

int main()
{
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
	TYC::work();
	return 0;
}

