#include<bits/stdc++.h>
using namespace std;

namespace TYC
{
	typedef long long ll;
	const int N=1e5+10;
	int n;
	ll m,val[N],least[N],tim[N];
		
	inline int read()
	{
		int x=0,f=0;char ch=getchar();
		while(!isdigit(ch)) f|=(ch=='-'),ch=getchar();
		while(isdigit(ch)) x=x*10+ch-'0',ch=getchar();
		return f?-x:x;
	}
	
	namespace Subtask1
	{
		struct frnd
		{
			ll val,least;
			bool operator < (const frnd &t) const
			{
				return (least<t.least)||(least==t.least&&val<t.val);
			}
		}a[N];
		
		void work()
		{
			for(int i=1;i<=n;i++)
				a[i]=(frnd){val[i],least[i]};
			sort(a+1,a+1+n);
			int tot=0;
			for(int i=1;i<=n;i++)
				if(a[i].least==a[i+1].least) continue;
				else a[++tot]=a[i];
			int now=1;
			ll need=0;
			for(int i=1;i<=tot;i++)
			{
				if(now>=m) break;
				if(a[i].val<now) continue;
				int j=i;
				ll mx=0;
				while(a[j].least<=now) 
					mx=max(mx,a[j].val),j++;
				now=mx,need++;
				i=j-1;
			}
			if(now<m) printf("-1\n");
			else printf("%lld\n",need);
		}
	}
	
	namespace Subtask2
	{
		typedef pair<ll,int> pa;
		const int M=3010;
		const ll inf=(1LL<<61);
		
		int tot,cnt,Head[M],vis[M];
		ll a[M],dis[M];
		
		struct edge
		{
			int to,next;
			ll w;
		}E[M*M];
		
		inline void add(int u,int v,ll w)
		{
			E[++cnt]=(edge){v,Head[u],w};
			Head[u]=cnt;
		}
		
		void Dijkstra(int s)
		{
			priority_queue<pa,vector<pa>,greater<pa> > q;
			for(int i=1;i<=tot;i++) dis[i]=inf;
			dis[s]=0;
			q.push(pa(dis[s],s));
			while(!q.empty())
			{
				int u=q.top().second;q.pop();
				if(vis[u]) continue;
				vis[u]=1;
				for(int i=Head[u];i;i=E[i].next)
				{
					int v=E[i].to;
					if(dis[v]>dis[u]+E[i].w)
					{
						dis[v]=dis[u]+E[i].w;
						if(!vis[v]) q.push(pa(dis[v],v));
					}
				}
			}
		}
		
		void work()
		{
			int num=0;
			a[++num]=1;
			for(int i=1;i<=n;i++) 
			{
				if(least[i]>m) continue;
				val[i]=min(val[i],m);
				a[++num]=val[i],a[++num]=least[i];
			}
			sort(a+1,a+1+num);
			tot=unique(a+1,a+1+num)-a-1;
			for(int i=1;i<=n;i++)
			{
				int v=lower_bound(a+1,a+1+tot,val[i])-a;
				int l=lower_bound(a+1,a+1+tot,least[i])-a;
				for(int j=l;j<v;j++)
					add(j,v,tim[i]);
			}
			Dijkstra(1);
			if(dis[tot]==inf) printf("-1\n");
			else printf("%lld\n",dis[tot]);
		}
	}
	
	void work()
	{
		int T=read();
		while(T--)
		{
			n=read(),m=read();
			int flag1=1;
			for(int i=1;i<=n;i++)
			{
				val[i]=read(),least[i]=read(),tim[i]=read();
				flag1&=(tim[i]==1);
			}
			if(flag1) {Subtask1::work();continue;}
			else Subtask2::work();
		}
	}
}

int main()
{
	freopen("gift.in","r",stdin);
	freopen("gift.out","w",stdout);
	TYC::work();
	return 0;
}

