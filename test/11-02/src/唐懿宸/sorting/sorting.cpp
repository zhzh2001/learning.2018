#include<bits/stdc++.h>
using namespace std;

namespace TYC
{
	const int N=1e6+10;
	char str[N];
	int n,m;
	
	inline int read()
	{
		int x=0,f=0;char ch=getchar();
		while(!isdigit(ch)) f|=(ch=='-'),ch=getchar();
		while(isdigit(ch)) x=x*10+ch-'0',ch=getchar();
		return f?-x:x;
	}	
	
	namespace Subtask1
	{
		char tmp[N];
		
		void work()
		{
			m=read();
			while(m--)
			{
				int k=read(),d=read();
				for(int s=0;s<=n-k;s++)
				{
					int t=s+k-1,cnt=s,point;
					for(int i=s;i<=t;i++) tmp[i]=str[i];
					for(int i=0;i<d;i++)
					{
						point=s+i;
						while(point<=t)
							str[cnt++]=tmp[point],point+=d;
					}
				}
				printf("%s\n",str);
			}
		}
	}
	
	namespace Subtask2
	{
		int num[N],tmp[N],a[N];
		char p[N];
		
		void work()
		{
			m=read();
			int k=read(),d=read();
			for(int i=0;i<n;i++) a[i]=i;
			for(int s=0;s<=n-k;s++)
			{
				int t=s+k-1,cnt=s,point;
				for(int i=s;i<=t;i++) tmp[i]=a[i];
				for(int i=0;i<d;i++)
				{
					point=s+i;
					while(point<=t)
						a[cnt++]=tmp[point],point+=d;
				}
			}
			
			while(m--)
			{
				for(int i=0;i<n;i++) p[i]=str[i];
				for(int i=0;i<n;i++) str[i]=p[a[i]];
				printf("%s\n",str);
			}
		}
	}
	
	void work()
	{
		scanf("%s",str);
		n=strlen(str);
		if(n<=100) {Subtask1::work();return;}
		else if(n<=3000) {Subtask2::work();return;}
		else {Subtask1::work();return;}
	}
}

int main()
{
	freopen("sorting.in","r",stdin);
	freopen("sorting.out","w",stdout);
	TYC::work();
	return 0;
}

