#include <cstdio>
#include <cstring>
#include <iostream>
using namespace std;
void judge(){
	freopen("gift.in","r",stdin);
	freopen("gift.out","w",stdout);
}
const int N=1600,inf=1<<30;
int n,m,T,r[N],t[N],v[N],ans;
struct info{
	int to,nxt,R,V,T;
}e[2*N*N+N];
int head[N],opt;
void add(int x,int y,int rr,int vv,int tt){
	e[++opt]={y,head[x],rr,vv,tt};
	head[x]=opt;
}
bool used[N];
void dfs(int u,int M,int t){
	//cerr<<u<<" "<<M<<" "<<t<<endl;
	if (t>ans) return;
	if (M>=m) return (void) (ans=min(ans,t));
	for (int i=head[u];i;i=e[i].nxt){
		int k=e[i].to;
		if (used[k]) continue;
		if (M>=e[i].R){
			used[k]=1;
			dfs(k,e[i].V,t+e[i].T);
			used[k]=0;
		}
	}
}
int main(){
	judge();
	scanf("%d",&T);
	while (T--){
		memset(head,0,sizeof(head));
		memset(used,0,sizeof(used));
		opt=0;
		scanf("%d%d",&n,&m);
		for (int i=1;i<=n;i++) scanf("%d%d%d",&v[i],&r[i],&t[i]);
		for (int i=1;i<=n;i++) add(0,i,r[i],v[i],t[i]);
		for (int i=1;i<=n;i++){
			for (int j=i+1;j<=n;j++){
				add(i,j,r[j],v[j],t[j]);
				add(j,i,r[i],v[i],t[i]);
			}
		}
		ans=inf; dfs(0,1,0);
		if (ans==inf) ans=-1;
		printf("%d\n",ans);
	}
	return 0;
}
