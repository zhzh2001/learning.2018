#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;
void judge(){
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
}
#define ull unsigned long long
const int N=110;
int T,n;
ull a[N],s,ans;
int main(){
	judge();
	ios::sync_with_stdio(false);
	cin>>T;
	while (T--){
		cin>>n;
		for (int i=0;i<n;i++) cin>>a[i];
		ans=a[0];
		for (int p=0;p<(1<<n);p++){
			if (!ans) break;
			for (int i=0;i<n;i++){
				if ((p>>i)&1) a[i]=~a[i];
			}
			s=a[0];
			for (int i=1;i<n;i++) s&=a[i];
			for (int i=0;i<n;i++){
				if ((p>>i)&1) a[i]=~a[i];
			}
			ans=min(ans,s);
		}
		cout<<ans<<endl;
	}
}