#include<bits/stdc++.h>
#define ull unsigned long long
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
//A&B=A^((!A)^B
using namespace std;
ull rnd(){
	ull x=0;
	For(i,1,64) x=x*2+rand()%2;
	return x;
}
void solve(){
	int n=6;
	printf("%d\n",n);
	For(i,1,n) printf("%llu ",rnd());
	puts("");
}
int main(){
	srand(time(NULL));
	freopen("lucky.in","w",stdout);
	printf("%d\n",100);
	For(T,1,100) solve();
}
/*
2
2
3 6
3
1 2 3
*/
