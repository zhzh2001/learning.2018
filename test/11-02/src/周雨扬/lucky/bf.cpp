#include<bits/stdc++.h>
#define ull unsigned long long
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
//A&B=A^((!A)^B
using namespace std;
ull a[105],all=-1,ans;
int n;
void dfs2(int x,ull v){
	if (x==n+1) return ans=min(ans,v),void(0);
	dfs2(x+1,v^a[x]);
	dfs2(x+1,v&a[x]);
	dfs2(x+1,v|a[x]);
}
void dfs1(int x){
	if (x==n+1) return dfs2(2,a[1]);
	a[x]^=all; dfs1(x+1);
	a[x]^=all; dfs1(x+1);
}
void solve(){
	scanf("%d",&n);
	ans=all;
	For(i,1,n) scanf("%llu",&a[i]);
	dfs1(1);
	printf("%llu\n",ans);
}
int main(){
	freopen("lucky.in","r",stdin);
	freopen("bf.out","w",stdout);
	int T; scanf("%d",&T);
	while (T--) solve();
}
/*
2
2
3 6
3
1 2 3
*/
