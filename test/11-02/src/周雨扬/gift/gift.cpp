#include<bits/stdc++.h>
#define ll long long
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
//A&B=A^((!A)^B
using namespace std;
const int MDND=2333333;
char LZH[MDND],*SSS=LZH,*TTT=LZH;
inline char gc(){
	if (SSS==TTT){
		TTT=(SSS=LZH)+fread(LZH,1,MDND,stdin);
		if (SSS==TTT) return EOF;
	}
	return *SSS++;
}
int read(){
	int x=0;
	char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc());
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return x;
}
const int N=100005;
struct data{
	int fr,to,v;
}a[N];
int b[N*2],n,m;
ll t[N*2];
bool cmp(data x,data y){
	return x.fr<y.fr;
}
void change(int x,ll v){
	x=lower_bound(b+1,b+*b+1,x)-b;
	for (;x;x-=x&(-x)) t[x]=min(t[x],v);
}
ll ask(int x){
	x=lower_bound(b+1,b+*b+1,x)-b;
	ll ans=1ll<<60;
	for (;x<=*b;x+=x&(-x)) ans=min(ans,t[x]);
	return ans;
}
void solve(){
	n=read();
	*b=2; b[1]=m=read(); b[2]=1;
	For(i,1,n){
		b[++*b]=a[i].to=read();
		b[++*b]=a[i].fr=read();
		a[i].v=read();
	}
	//cerr<<clock()<<endl;
	sort(a+1,a+n+1,cmp);
	sort(b+1,b+*b+1);
	*b=unique(b+1,b+*b+1)-b-1;
	For(i,1,*b) t[i]=1ll<<60;
	change(1,0);
	For(i,1,n){
		ll v=ask(a[i].fr);
		change(a[i].to,v+a[i].v);
	}
	ll ans=ask(m);
	printf("%lld\n",ans>(1ll<<50)?-1:ans);
	//cerr<<clock()<<endl;
}
int main(){
	freopen("gift.in","r",stdin);
	freopen("gift.out","w",stdout);
	int T=read();
	while (T--) solve();
}
/*
2
2
3 6
3
1 2 3
*/
