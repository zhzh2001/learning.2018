#include<bits/stdc++.h>
#define ll long long
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
//A&B=A^((!A)^B
using namespace std;
const int N=1000005;
int id[N],n,Q,lg;
char s[N],ns[N];
int to[N][21];
int read(){
	int x=0;
	#define gc getchar
	char ch=gc();
	for (;ch<'0'||ch>'9';ch=gc());
	for (;ch>='0'&&ch<='9';ch=gc())
		x=x*10-48+ch;
	return x;
}
void solve(){
	int k=read(),d=read(),top=0;
	For(i,0,d) for (int j=i;j<k;j+=d) id[top++]=j;
	For(i,0,k-1) to[id[i]][0]=i-1;
	For(T,1,lg) For(i,0,k-1) to[i][T]=(to[i][T-1]==-1?-1:to[to[i][T-1]][T-1]);
	For(i,0,n-1){
		int mxT=min(n-i,n-k+1);
		int beg=min(k-1,i);
		Rep(j,lg,0) if (mxT>=(1<<j))
			if (to[beg][j]!=-1){
				mxT-=(1<<j);
				beg=to[beg][j];
			}
		int pos;
		if (mxT&&to[beg][0]==-1)
			pos=n-mxT-k+1;
		else pos=n-k+beg+1;
		ns[pos]=s[i];
	}
	For(i,0,n-1) putchar(s[i]=ns[i]);
	puts("");
}
int main(){
	freopen("sorting.in","r",stdin);
	freopen("sorting.out","w",stdout);
	scanf("%s",s);
	Q=read(); n=strlen(s);
	lg=0;
	for (;1<<(lg+1)<=n;lg++);
	while (Q--) solve();
}
/*
2
2
3 6
3
1 2 3
*/
