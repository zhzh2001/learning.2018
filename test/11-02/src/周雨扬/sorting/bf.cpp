#include<bits/stdc++.h>
#define ll long long
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
//A&B=A^((!A)^B
using namespace std;
const int N=1000005;
int id[N],n,Q;
char s[N],ns[N];
int to[N][21];
void solve(){
	int k,d,top=0;
	scanf("%d%d",&k,&d);
	For(i,0,d) for (int j=i;j<k;j+=d) id[top++]=j;
	For(i,0,n-k){
		For(j,0,k-1) ns[j]=s[i+id[j]];
		For(j,0,k-1) s[i+j]=ns[j];
	}
	For(i,0,n-1) putchar(s[i]);
	puts("");
}
int main(){
	freopen("sorting.in","r",stdin);
	freopen("bf.out","w",stdout);
	scanf("%s%d",s,&Q);
	n=strlen(s);
	while (Q--) solve();
}
/*
2
2
3 6
3
1 2 3
*/
