#include<bits/stdc++.h>
#define ll long long
#define For(i,j,k) for (int i=(int)(j);i<=(int)(k);i++)
#define Rep(i,j,k) for (int i=(int)(j);i>=(int)(k);i--)
//A&B=A^((!A)^B
using namespace std;
const char s[]="`1234567890-=qwertyuiop[]\asdfghjkl;'zxcvbnm,./~!@#$%^&*()_+QWERTYUIOP{}|ASDFGHJKL:ZXCVBNM<>?";
int main(){
	freopen("sorting.in","w",stdout);
	srand(time(NULL));
	int n=rand()%100+20,Q=1000000/n;
	For(i,1,n) putchar(s[rand()%strlen(s)]);
	printf("\n%d\n",Q);
	For(i,1,Q){
		int k=rand()%n+1,d=rand()%n+1;
		if (k<d) swap(k,d);
		printf("%d %d\n",k,d);
	}
}
/*
2
2
3 6
3
1 2 3
*/
