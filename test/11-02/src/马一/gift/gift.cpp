#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
using namespace std;
namespace Dango
{
	const int MAXN=100010;
	const long long INF=0x3f3f3f3f3f3f3f3f;
	struct peo
	{
		int v,r,t;
		bool operator <(const peo b)const 
		{
			return v<b.v;
		}
	}f[MAXN];
	struct tree
	{
		int l,r;
		long long data;
	}t[MAXN<<2];
	int T,n,m,cnt,v[MAXN],q;
	long long dp[MAXN];
	int read()
	{
		int x=0,f=0;
		char ch=getchar();
		while(!isdigit(ch)){f|=(ch=='-');ch=getchar();}
		while(isdigit(ch)){x=x*10+ch-'0';ch=getchar();}
		return f?-x:x;
	}
	void build(int a,int l,int r)
	{
		t[a].l=l;t[a].r=r;
		if(l==r) t[a].data=INF;
		else
		{
			int mid=(l+r)>>1;
			build(a<<1,l,mid);
			build(a<<1|1,mid+1,r);
			t[a].data=INF;
		}
	}
	void update(int a,int l,int r,long long data)
	{
		if(t[a].l>=l&&t[a].r<=r)
		{
			t[a].data=min(t[a].data,data);
			return;
		}
		int mid=(t[a].l+t[a].r)>>1;
		if(mid>=l)update(a<<1,l,r,data);
		if(mid<r)update(a<<1|1,l,r,data);
	}
	long long query(int a,int pos)
	{
		if(t[a].l==t[a].r)return t[a].data;
		long long result=t[a].data;
		int mid=(t[a].l+t[a].r)>>1;
		if(mid>=pos)result=min(result,query(a<<1,pos));
		if(mid<pos)result=min(result,query(a<<1|1,pos));
		return result;
	}
	int pfind(int a)
	{
		int l=0,r=q,ans=0;
		while(l<=r)
		{
			int mid=(l+r)>>1;
			if(v[mid]>=a)
			{
				ans=mid;
				r=mid-1;
			}
			else l=mid+1;
		}
		return ans+1;
	}
	void solve()
	{
		n=read();m=read();
		f[0].r=0;f[0].v=1;f[0].t=0;
		cnt=0;
		v[0]=1;
		for(int i=1;i<=n;i++)
		{
			cnt++;
			f[cnt].v=read();f[cnt].r=read();f[cnt].t=read();
			v[cnt]=f[cnt].v;
			if(f[cnt].r>=m)cnt--;
		}
		cnt++;
		f[cnt].r=m;f[cnt].v=0x3f3f3f3f;f[cnt].t=0;
		v[cnt]=0x3f3f3f3f;
		sort(f,f+cnt+1);
		sort(v,v+cnt+1);
		q=unique(v,v+cnt+1)-v;
		build(1,1,q);
		int x=lower_bound(v,v+q,f[cnt].v)-v+1;
		update(1,x,x,0);
		for(int i=cnt;i>=0;i--)
		{
			int a=lower_bound(v,v+q,f[i].v)-v+1;
			dp[i]=query(1,a);
			int b=pfind(f[i].r);
			update(1,b,a,dp[i]+f[i].t);
		}
		if(dp[0]!=INF)cout<<dp[0]<<"\n";
		else cout<<"-1\n";
	}
	int work()
	{
		T=read();
		for(int i=1;i<=T;i++)
			solve();
		return 0;
	}
}
int main()
{
	freopen("gift.in","r",stdin);
	freopen("gift.out","w",stdout);
	return Dango::work();
}
