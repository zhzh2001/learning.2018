#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
using namespace std;
namespace Dango
{
	const int MAXN=105;
	int T,n;
	unsigned long long num[MAXN],ans;
	void dfs(int a,unsigned long long sum)
	{
		if(a>n)
		{
			ans=min(ans,sum);
			return;
		}
		dfs(a+1,sum^num[a]);
		dfs(a+1,sum&num[a]);
		dfs(a+1,sum|num[a]);
		num[a]=(~num[a]);
		dfs(a+1,sum^num[a]);
		dfs(a+1,sum&num[a]);
		dfs(a+1,sum|num[a]);
		num[a]=(~num[a]);
	}
	void solve()
	{
		ans=0x3f3f3f3f3f3f3f3f;
		cin>>n;
		for(int i=1;i<=n;i++)
			cin>>num[i];
		dfs(2,num[1]);
		num[1]=(~num[1]);
		dfs(2,num[1]);
		cout<<ans<<"\n";
	}
	int work()
	{
		ios::sync_with_stdio(false);
		cin>>T;
		for(int i=1;i<=T;i++)
			solve();
		return 0;
	}
}
int main()
{
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
	return Dango::work();
}
