#include<bits/stdc++.h>
using namespace std;
inline int read() {
  int x=0,f=0;char ch=getchar();
  for(;ch<'0'||ch>'9';ch=getchar()) if (ch=='-') f=1;
  for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
  return f?-x:x;
}
const int N = 200005;
int n, m, ha[N], tot;
struct data {
  int v, r, t;
  bool operator <(const data &a)const{
    return r < a.r;
  }
}a[N];
long long f[N], oo;
inline void add(int x, long long val) {
  for (; x; x -= (x&-x)) f[x] = min(f[x], val);
}
inline long long query(int x) {
  long long sum = oo;
  for (; x <= tot; x += (x&-x)) sum = min(sum, f[x]);
  return sum;
}
signed main() {
  freopen("gift.in","r",stdin);
  freopen("gift.out","w",stdout);
  int T = read();
  while (T--) {
    n = read(); m = read();
    for (int i = 1; i <= n; i++) {
      a[i] = (data){read(), read(), read()};
      ha[++tot] = a[i].v;
      ha[++tot] = a[i].r;
    }
    ha[++tot] = 1;
    sort(ha+1,ha+1+tot);
    sort(a+1,a+1+n);
    tot = unique(ha+1,ha+1+tot)-ha-1;
    for (int i = 1; i <= n; i++) {
      a[i].v = lower_bound(ha+1,ha+1+tot,a[i].v)-ha;
      a[i].r = lower_bound(ha+1,ha+1+tot,a[i].r)-ha;
    }
//    for (int i = 1; i <= n; i++)
  //    cout << a[i].r << " "<< a[i].v << endl;
    memset(f, 127, sizeof f);
    oo = f[0];
    add(1, 0);
    for (int i = 1; i <= n; i++) {
      long long sum = query(a[i].r);
      if (sum == oo) break;
      add(a[i].v, sum+a[i].t);
    }
    m = lower_bound(ha+1,ha+1+tot,m)-ha;
    long long ans = query(m);
    if (ans == oo) puts("-1");
    else printf("%lld\n", ans);
  }
  return 0;
}
