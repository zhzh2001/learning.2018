#include<bits/stdc++.h>
using namespace std;
const int N = 1000005;
int n, m, a[2][N], b[N], mod, tnt;
char s[N], C[N];
inline int cmp(int x, int y) {
  return (x%mod==y%mod)?(x<y):(x%mod<y%mod);
}
inline void solve(int k, int d) {
  tnt = 0;
  for (int i = 0; i < n; i++) a[0][i] = i;
  for (int i = 0; i < k; i++)
    b[i] = i;
  mod = d;
  sort(b,b+k,cmp);
//  for (int i = 0; i < k; i++)
//    cout << b[i] <<" ";
  for (int i = 0; i <= n-k; i++) {
    tnt ^= 1;
    for (int j = 0; j < n; j++) a[tnt][j] = a[tnt^1][j];
    for (int j = i, t = 0; t < k; t++, j++) a[tnt][j] = a[tnt^1][b[t]+i];
  }
  for (int i = 0; i < n; i++)
    C[i] = s[a[tnt][i]];
  for (int i = 0; i < n; i++)
    s[i] = C[i];
}
inline void ssolve() {
  for (int i = 0; i < n; i++)
    C[i] = s[a[tnt][i]];
  for (int i = 0; i < n; i++)
    s[i] = C[i];
}
int main() {
  freopen("sorting.in","r",stdin);
  freopen("sorting.out","w",stdout);
  scanf("%s", s);
  scanf("%d", &m);
  n = strlen(s);
  if (n <= 100) {
    while (m--) {
      int k, d;
      scanf("%d%d",&k,&d);
      solve(k, d);
      printf("%s\n",s);
    }
  } else {
    int k, d;
    scanf("%d%d", &k, &d);
    solve(k, d);
    printf("%s\n", s);
    m--;
    while(m--) {
      ssolve();
      printf("%s\n", s);
    }
  }
  return 0;
}
