#include<bits/stdc++.h>
using namespace std;
const int N = 105;
int n;
unsigned long long a[N], ans;
int main() {
  freopen("lucky.in","r",stdin);
  freopen("lucky.out","w",stdout); 
  int T;
  scanf("%d", &T);
  while (T--) {
    scanf("%d", &n);
    for (int i = 1; i <= n; i++)
      cin >> a[i];
    sort(a+1,a+1+n);
    ans = min(a[1], (~a[1]));
    for (int i = 2; i <= n; i++) ans = min((ans&a[i]), (ans&(~a[i])));
    cout << ans << endl;
  }
  return 0;
}
