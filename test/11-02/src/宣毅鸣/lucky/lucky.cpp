#include<bits/stdc++.h>
using namespace std;
#define ull unsigned long long
const int N=105;
int n,T;
ull a[N],b[N],ans;
void dfs(int x,ull y){
	if (!ans)return;
	if (x>n){
		ans=min(ans,y);
		return;
	}
	if (!y){
		ans=0;
		return;
	}
	dfs(x+1,y&a[x]);
	dfs(x+1,y&b[x]);
	dfs(x+1,y^a[x]);
	dfs(x+1,y^b[x]);	
	dfs(x+1,y|a[x]);
	dfs(x+1,y|b[x]);	
}
int main(){
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d",&n);
		memset(b,0,sizeof b);
		for (int i=1;i<=n;i++){
			cin>>a[i];
			ull t=1;
			for (int j=0;j<64;j++,t*=2)b[i]+=t;
			b[i]-=a[i];
		}
		if (n<=9){
			ull t=0;
			ans=a[1];
			dfs(2,a[1]);dfs(2,b[1]);
			cout<<ans<<endl;
			continue;
		}
		puts("0");
	}
	return 0;
}
