#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const int N=200005;
int b[N],cnt,n,m;
ll dp[N];
struct Ask{
	int x,y,z;
}A[N];
struct Tree{
	int l,r;
	ll num,flag;
}T[N*4];
inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline int read(){
	int x=0;char c=0;
	for (;c<'0'||c>'9';c=gc());
	for (;c>='0'&&c<='9';c=gc())x=(x<<1)+(x<<3)+c-48;
	return x;
}
inline void down(int x){
	if (T[x*2].flag>T[x].flag)T[x*2].flag=T[x].flag;
	if (T[x*2].num>T[x].flag)T[x*2].num=T[x].flag,T[x*2].flag=T[x].flag;
	if (T[x*2+1].flag>T[x].flag)T[x*2+1].flag=T[x].flag;
	if (T[x*2+1].num>T[x].flag)T[x*2+1].num=T[x].flag;
	T[x].flag=1e18;
	return;
}
inline void build(int x,int l,int r){
	T[x].l=l;T[x].r=r;
	T[x].num=T[x].flag=1e18;
	if (l==r)return;
	int mid=(l+r)/2;
	build(x*2,l,mid);
	build(x*2+1,mid+1,r);
}
inline void insert(int x,int l,int r,ll y){
	if (l>T[x].r||T[x].l>r)return;
	if (y<T[x].num)T[x].num=y;	
	if (T[x].l>=l&&T[x].r<=r){
		if (y<T[x].flag)T[x].flag=y;
		return;
	}
	down(x);
	insert(x*2,l,r,y);
	insert(x*2+1,l,r,y);
}
inline ll find(int x,int l,int r){
	if (l>T[x].r||T[x].l>r)return 1e18;
	if (T[x].l>=l&&T[x].r<=r)return T[x].num;
	down(x);
	return min(find(x*2,l,r),find(x*2+1,l,r));
}
inline bool cmp(Ask x,Ask y){
	return x.x<y.x;
}
inline int ef(int x){
	int l=1,r=cnt;
	while (l<r){
		int mid=(l+r)/2;
		if (b[mid]<x)l=mid+1;
		else r=mid;
	}
	return l;
}
int main(){
	freopen("gift.in","r",stdin);
	freopen("gift.out","w",stdout);
	int TT=read();
	while (TT--){
		n=read();m=read();
		b[1]=1;b[2]=m;cnt=2;
		for (int i=1;i<=n;i++){
			A[i].y=read();A[i].x=read();A[i].z=read();
			b[++cnt]=A[i].x;b[++cnt]=A[i].y;
		}
		sort(A+1,A+n+1,cmp);
		sort(b+1,b+cnt+1);
		int num=1;
		for (int i=2;i<=cnt;i++)
			if (b[i]!=b[num])b[++num]=b[i];
		cnt=num;
		for (int i=1;i<=n;i++)A[i].x=ef(A[i].x),A[i].y=ef(A[i].y);	
		m=ef(m);
		build(1,1,cnt);
		insert(1,1,1,0);
		ll ans=1e18;
		for (int i=1;i<=n;i++){
			dp[i]=find(1,A[i].x,A[i].x)+A[i].z;
			insert(1,1,A[i].y,dp[i]);
			if (A[i].y>=m)ans=min(ans,dp[i]);
		}
		if (ans<1e18)printf("%lld\n",ans);
		else puts("-1");
	}
	return 0;
}
