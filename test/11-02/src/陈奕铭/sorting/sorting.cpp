#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0'; ch = getchar();}
	return x*f;
}

const int N = 1000005,M = 20;
int n,m,tot;
int k,d;
int st[N][M];
int getf[N][M];
char s[N],ans[N];
int Bin[N],s2[M];
struct node{
	int x,id;
	friend bool operator<(node a,node b){
		if(a.x == b.x) return a.id < b.id;
		return a.x < b.x;
	}
}a[N];
int pos[N];

int query(int now,int x,int y){
	int X = now-x;
	int fas = 0;
	for(int i = Bin[y];i >= 0;--i)
		if((y&(1<<i)) != 0){
			if(st[x][i] == -1){
				fas += getf[x][i];
				return X+fas-1;
			}
			fas += (1<<i);
			x = st[x][i];
		}
	return X+fas + x;
}

signed main(){
	freopen("sorting.in","r",stdin);
	freopen("sorting.out","w",stdout);
	scanf("%s",s);
	n = strlen(s);
	m = read();
	s2[0] = 1; for(int i = 1;i <= 19;++i) s2[i] = s2[i-1]<<1;
	Bin[1] = 0;for(int i = 2;i <= n;++i) Bin[i] = Bin[i>>1]+1;
	while(m--){
		k = read(); d = read();
		for(int i = 0;i < k;++i){
			a[i].id = i;
			a[i].x = i%d;
		}
		sort(a,a+k);
		for(int i = 0;i < k;++i){
			st[a[i].id][0] = i-1;
			if(i-1 == -1) getf[a[i].id][0] = 1;
			else getf[a[i].id][0] = 0;
		}
//		for(int i = 0;i < k;++i)
//			printf("%d ",a[i].id);
//		puts("");
		tot = n-k+1;
		for(int j = 1;j <= Bin[tot];++j)
			for(int i = 0;i < k;++i){
				if(st[i][j-1] == -1){
					st[i][j] = -1;
					getf[i][j] = getf[i][j-1];
				}else if(st[st[i][j-1]][j-1] == -1){
					st[i][j] = -1;
					getf[i][j] = (1<<(j-1))+getf[st[i][j-1]][j-1];
				}else{
					st[i][j] = st[st[i][j-1]][j-1];
					getf[i][j] = 0;
				}
			}
//		for(int j = 0;j <= Bin[k];++j,puts(""))
//			for(int i = 0;i < k;++i)
//				printf("%d ",st[i][j]);
		for(int i = 0;i < n;++i){
			int x;
			if(i < k){
				x = query(i,i,tot);
			}else{
				x = query(i,k-1,tot-i+k-1);
			}
			ans[x] = s[i];
		}
		for(int i = 0;i < n;++i)
			s[i] = ans[i];
		printf("%s\n",s);
	}
	return 0;
}
