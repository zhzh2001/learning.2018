#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0'; ch = getchar();}
	return x*f;
}
inline ull readll(){
	ull x = 0; char ch = getchar();
	while(ch < '0' || ch > '9'){ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x;
}

const int N = 105;
ull a[N],ans;
int n;

void dfs(ull pre,int x){
	if(x == n+1){
		ans = min(ans,pre);
		return;
	}
	dfs(pre&(~a[x]),x+1);
	dfs(pre&a[x],x+1);
}

signed main(){
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
	int T = read();
	while(T--){
		n = read();
		for(int i = 1;i <= n;++i){
			a[i] = readll();
		}
		if(n >= 7){
			puts("0");
			continue;
		}
		ans = a[1];
		dfs(~a[1],2);
		dfs(a[1],2);
		printf("%llu\n",ans);
	}
	return 0;
}

//观察性质发现只有于操作最优 
//
