#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0'; ch = getchar();}
	return x*f;
}

typedef pair<ll,int> pii;
const int N = 100005;
int n,m;
ll ans;
struct node{
	int r,v,t;
	friend bool operator <(node a,node b){
		return a.r < b.r;
	}
}a[N];
ll Mi[N<<2],lazy[N<<2];
int pos[N<<2];
int d[N*2],cnt;
priority_queue<pii,vector<pii>,greater<pii> > Q;
vector<int> vec[N*2];

inline int erfen(int x){
	int l = 1,r = cnt,mid;
	while(l <= r){
		mid = (l+r)>>1;
		if(d[mid] <= x){
			l = mid+1;
		}else r = mid-1;
	}
	return l-1;
}

void build(int num,int l,int r){
	pos[num] = l;
	lazy[num] = -1;
	if(l == r) return;
	int mid = (l+r)>>1;
	build(num<<1,l,mid);
	build(num<<1|1,mid+1,r);
}

inline void cove(int k,int l,int r,ll x){
	if(Mi[k] > x){
		Mi[k] = x;
		pos[k] = l;
	}
	if(lazy[k] > x || lazy[k] == -1){
		lazy[k] = x;
	}
}

inline void pushdown(int num,int L,int R){
	if(lazy[num] != -1){
		int l = num<<1,r = num<<1|1;
		int mid = (L+R)>>1;
		cove(l,L,mid,lazy[num]);
		cove(r,mid+1,R,lazy[num]);
		lazy[num] = -1;
	}
}

void change(int num,int l,int r,int x,int y,ll k){
	if(x <= l && r <= y){
		cove(num,l,r,k);
		return;
	}
	int mid = (l+r)>>1;
	pushdown(num,l,r);
	if(x <= mid) change(num<<1,l,mid,x,y,k);
	if(y > mid) change(num<<1|1,mid+1,r,x,y,k);
	if(Mi[num<<1] < Mi[num<<1|1]){
		pos[num] = pos[num<<1];
		Mi[num] = Mi[num<<1];
	}else{
		pos[num] = pos[num<<1|1];
		Mi[num] = Mi[num<<1|1];
	}
}

int query(int num,int l,int r,int x,int y){
	if(x <= l && r <= y) return Mi[num];
	int mid = (l+r)>>1,ans = 0;
	pushdown(num,l,r);
	if(x <= mid) ans = query(num<<1,l,mid,x,y);
	if(y > mid){
		int son = query(num<<1|1,mid+1,r,x,y);
		if(a[son].r < a[ans].r) ans = son;
	}
	return ans;
}

void del(int num,int l,int r,int x){
	if(l == r){
		Mi[num] = Mi[0];
		return;
	}
	int mid = (l+r)>>1;
	pushdown(num,l,r);
	if(x <= mid) del(num<<1,l,mid,x);
	else del(num<<1|1,mid+1,r,x);
	if(Mi[num<<1] < Mi[num<<1|1]){
		pos[num] = pos[num<<1];
		Mi[num] = Mi[num<<1];
	}else{
		pos[num] = pos[num<<1|1];
		Mi[num] = Mi[num<<1|1];
	}
}

signed main(){
	freopen("gift.in","r",stdin);
	freopen("gift.out","w",stdout);
	int T = read();
	a[0].r = 2e9;
	while(T--){
		n = read(); m = read(); ans = 0;
		cnt = 0;
		bool flag = false;
		int num = 0;
		for(int i = 1;i <= n;++i){
			a[i].v = read(); a[i].r = read(); a[i].t = read();
			d[++num] = a[i].r;
			if(a[i].r == 1) flag = true;
		}
		sort(a+1,a+n+1);
		if(!flag){
			puts("-1");
			continue;
		}
		d[++num] = m;
		sort(d+1,d+num+1); cnt = 1;
		for(int i = 2;i <= num;++i)
			if(d[i] != d[i-1])
				d[++cnt] = d[i];
		int EEnd = erfen(m);
		for(int i = 1;i <= cnt;++i) vec[i].clear();
		for(int i = 1;i <= n;++i){
			a[i].r = erfen(a[i].r);
			a[i].v = erfen(a[i].v);
			vec[a[i].r].push_back(i);
		}
		memset(Mi,0x3f,sizeof Mi);
		build(1,1,cnt);
		change(1,1,cnt,1,1,0);
		int head = 1;
		for(int i = 0;i <= n;++i){
			int x = pos[1];
			ll dist = Mi[1];
			if(dist == Mi[0]){
				puts("-1");
				break;
			}
			if(x == EEnd){
				printf("%lld\n",dist);
				break;
			}
			for(int j = vec[x].size()-1;j >= 0;--j){
				int y = vec[x][j];
				change(1,1,cnt,x,a[y].v,dist+a[y].t);
			}
			del(1,1,cnt,x);
		}
//		puts("@");
	}
	return 0;
}
