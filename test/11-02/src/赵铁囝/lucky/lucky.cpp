#include <bits/stdc++.h>

#define ll unsigned long long
#define Max 10005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(ll x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(ll x){
	write(x);puts("");
}

ll t,n,ans,num,a[Max];

inline void dfs(ll x,ll now){
	if(x==n+1){
		ans=min(ans,now);
		return;
	}
	dfs(x+1,now&a[x]);
	dfs(x+1,now^a[x]);
	dfs(x+1,now&(num^a[x]));
	dfs(x+1,now^(num^a[x]));
}

int main(){
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
	t=read();
	num=-1;
	while(t--){
		n=read();
		ans=num;
		for(ll i=1;i<=n;i++){
			a[i]=read();
			ans=min(ans&a[i],ans&(a[i]^num));
		}
		writeln(ans);
	}
	return 0;
}
