#include <bits/stdc++.h>

#define ll long long
#define Max 100005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define getchar gc
inline ll read(){
	ll x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(ll x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(ll x){
	write(x);puts("");
}

struct Edge{
	ll v,to,w;
}e[Max*3];

struct Node{
	ll l,r,w;
}a[Max];

struct Heap{
	ll num,id;
	inline bool operator<(const Heap&x)const{
		return num<x.num;
	}
	inline bool operator>(const Heap&x)const{
		return num>x.num;
	}
}heap[Max*2];

ll t,n,m,size,cnt,heap_size,dis[Max*2],now[Max*2],num[Max*2],head[Max*2];
map<ll,bool>vis;
map<ll,ll>id;

inline void add(ll u,ll v,ll w){
//	cout<<u<<" "<<v<<" "<<w<<endl;
	e[++size].v=v;e[size].to=head[u];e[size].w=w;head[u]=size;
}

inline void up(ll x){
	ll y=x/2;
	if(!y)return;
	while(heap[y]>heap[x]){
		swap(now[heap[x].id],now[heap[y].id]);
		swap(heap[x],heap[y]);
		x=y;
		y=x/2;
		if(!y)return;
	}
	return;
}

inline void down(ll x){
	ll y=x*2;
	if(y>heap_size)return;
	if(y<heap_size&&heap[y]>heap[y+1])y++;
	while(heap[x]>heap[y]){
		swap(now[heap[x].id],now[heap[y].id]);
		swap(heap[x],heap[y]);
		x=y;
		y=x*2;
		if(y>heap_size)return;
		if(y<heap_size&&heap[y]>heap[y+1])y++;
	}
	return;
}

inline void dijkstra(){
	memset(dis,0x3f,sizeof dis);
	dis[id[1]]=0;
	for(ll i=1;i<=cnt;i++){
		heap[++heap_size]=(Heap){dis[i],i};
		now[i]=heap_size;
		up(heap_size);
	}
	while(heap_size){
		ll u=heap[1].id;
//		cout<<u<<" "<<dis[u]<<endl;
		now[heap[heap_size].id]=1;
		heap[1]=heap[heap_size--];
		down(1);
		for(ll i=head[u];i;i=e[i].to){
			ll v=e[i].v;
			if(dis[v]>dis[u]+e[i].w){
				dis[v]=dis[u]+e[i].w;
				heap[now[v]].num=dis[v];
				up(now[v]);
			}
		}
	}
	return;
}

int main(){
	freopen("gift.in","r",stdin);
	freopen("gift.out","w",stdout);
	t=read();
	while(t--){
		n=read();m=read();
		vis.clear();
		cnt=0;
		size=0;
		memset(head,0,sizeof head);
		vis[1]=true;
		num[++cnt]=1;
		vis[m]=true;
		num[++cnt]=m;
		for(ll i=1;i<=n;i++){
			a[i].l=read();a[i].r=read();a[i].w=read();
			if(!vis[a[i].l]){
				vis[a[i].l]=true;
				num[++cnt]=a[i].l;
			}
			if(!vis[a[i].r]){
				vis[a[i].r]=true;
				num[++cnt]=a[i].r;
			}
		}
		sort(num+1,num+cnt+1);
		id.clear();
		for(ll i=1;i<=cnt;i++){
			id[num[i]]=i;
			if(i!=1)add(i,i-1,0);
		}
		for(ll i=1;i<=n;i++){
//			cout<<size<<endl;
//			cout<<id[a[i].r]<<" "<<id[a[i].l]<<endl;
			add(id[a[i].r],id[a[i].l],a[i].w);
		}
		dijkstra();
		if(dis[id[m]]>1e18){
			puts("-1");
		}else{
			writeln(dis[id[m]]);
		}
	}
	return 0;
}
