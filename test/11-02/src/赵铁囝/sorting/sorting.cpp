#include <bits/stdc++.h>

#define Max 100005

using namespace std;

inline char gc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
#define gc getchar
inline int read(){
	int x=0;char ch=gc();bool positive=1;
	for(;!isdigit(ch);ch=gc())if(ch=='-')positive=0;
	for(;isdigit(ch);ch=gc())x=x*10+ch-'0';
	return positive?x:-x;
}

inline void write(int x){
	if(x<0)x=-x,putchar('-');
	if(x>9)write(x/10);putchar(x%10+'0');
}

inline void writeln(int x){
	write(x);puts("");
}

struct Node{
	int id,d;
	inline bool operator<(const Node&x)const{
		if(id%d==x.id%d){
			return id<x.id;
		}else{
			return id%d<x.id%d;
		}
	}
}b[Max];

int n,m,k,d,a[Max];
int now[Max],ans[Max];
char num[Max];

inline void calc(int k,int d){
	for(int i=1;i<=k;i++){
		b[i]=(Node){i-1,d};
	}
	sort(b+1,b+k+1);
	for(int i=1;i<=k;i++){
		a[b[i].id+1]=i;
//		cout<<b[i].id+1<<" "<<i<<endl;
	}
	for(int i=1;i<=n-k+1;i++){
		for(int j=1;j<=n;j++)now[j]=ans[j];
		for(int j=1;j<=k;j++){
			ans[i-1+a[j]]=now[i-1+j];
		}
	}
}

int main(){
	freopen("sorting.in","r",stdin);
	freopen("sorting.out","w",stdout);
	scanf("%s",num+1);
	n=strlen(num+1);
	for(int i=1;i<=n;i++){
		ans[i]=now[i]=num[i];
	}	
	m=read();
//	cout<<n<<" "<<m<<endl;
	if(n<=500&&m<=500){
		for(int i=1;i<=m;i++){
			k=read();d=read();
			calc(k,d);
			for(int j=1;j<=n;j++){
				putchar(ans[j]);
			}
			puts("");
			memcpy(now,ans,sizeof now);
		}
		return 0;
	}else{
		for(int i=1;i<=m;i++){
			k=read();d=read();
		}
		for(int i=1;i<=n;i++)ans[i]=i;
//		for(int i=1;i<=n;i++)cout<<(int)ans[i]<<" ";cout<<endl;
		calc(k,d);
		for(int i=1;i<=n;i++){
			a[i]=ans[i];
//			cout<<a[i]<<" ";
		}
		for(int i=1;i<=n;i++){
			ans[i]=now[i]=num[i];
		}	
		for(int i=1;i<=m;i++){
			for(int j=1;j<=n;j++){
				ans[j]=now[a[j]];
			}
			for(int j=1;j<=n;j++){
				putchar(ans[j]);
			}
			puts("");	
//			puts(ans+1);
			memcpy(now,ans,sizeof now);
		}
	}
	return 0;
}
