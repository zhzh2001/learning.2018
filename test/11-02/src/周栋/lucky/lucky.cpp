#include <iostream>
#include <cstring>
#include <cstdio>
#include <set>
using namespace std;
typedef unsigned long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
ll k,T,n,a[210],ans,f[210][210];
bool vis[210];
set<ll>s[210];
inline void dfs(ll x,ll v){
	if (!ans) return;
	if (s[x].count(v)) return;
	s[x].insert(v);
	if (x>n){
		ans=min(ans,v);
		return;
	}
	vis[x]=0;
	dfs(x+1,v&a[x]);
	dfs(x+1,v|a[x]);
	dfs(x+1,v^a[x]);
	a[x]^=k;
	vis[x]=1;
	dfs(x+1,v&a[x]);
	dfs(x+1,v|a[x]);
	if (!vis[x-1]) dfs(x+1,v^a[x]);
	a[x]^=k;
	vis[x]=0;
}
int main(){
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
	for (ll i=0;i<64;++i) k|=1ll<<i;
	read(T);
	while (T--){
		read(n);
		for (ll i=1;i<=n;++i) read(a[i]);
		if (n<=9){
			ans=k;
			for (ll i=1;i<=n+1;++i) s[i].clear();
			memset(vis,0,sizeof vis);
			dfs(2,a[1]);
			vis[1]=1;
			dfs(2,a[1]^k);
			wr(ans),puts("");
			continue;
		}
		memset(f,0x7ff,sizeof f);
		ans=k;
		f[1][0]=a[1],f[1][1]=a[1]^k;
		for (ll i=2;i<=n;++i)
			for (ll j=0;j<=i;++j){
				f[i][j]=min(f[i][j],f[i-1][j]&a[i]);
				f[i][j]=min(f[i][j],f[i-1][j]|a[i]);
				f[i][j]=min(f[i][j],f[i-1][j]^a[i]);
				if (i==n) ans=min(ans,f[i][j]);
				if (j==0) continue;
				f[i][j]=min(f[i][j],f[i-1][j-1]&(a[i]^k));
				f[i][j]=min(f[i][j],f[i-1][j-1]|(a[i]^k));
				f[i][j]=min(f[i][j],f[i-1][j-1]^(a[i]^k));
				if (i==n) ans=min(ans,f[i][j]);
			}
		wr(ans),puts("");
	}
	return 0;
}
//sxdakking
/*
2
2
3 6
3
1 2 3
*/
