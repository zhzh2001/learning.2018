#include <algorithm>
#include <iostream>
#include <cstdio>
#include <queue>
#include <cstring>
using namespace std;
typedef long long ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=4e5+10,INF=0x3f3f3f3f;
ll G,n,m,s[N],tot,head[N],edge,S,T,dis[N];
struct edge{ll to,net,pay;}e[N];
bool vis[N];
struct node{
	ll v,r,t;
	inline void init(){read(v),read(r),read(t);}
}a[N];
struct Data{
	ll x,val;
	Data(){}
	Data(ll a,ll b):x(a),val(b){}
	bool operator <(const Data&res)const{
		return val>res.val;
	}
};
inline void pre(){
	memset(head,0,sizeof head);
	memset(dis,0x3f,sizeof dis);
	memset(vis,0,sizeof vis);
	edge=0;
}
inline void addedge(ll x,ll y,ll z){
	e[++edge].net=head[x],head[x]=edge,e[edge].to=y,e[edge].pay=z;
}
priority_queue<Data>H;
inline void dijstra(){
	H.push(Data(S,0));
	dis[S]=0;
	while (!H.empty()){
		Data k=H.top();H.pop();
		if (vis[k.x]) continue;
		vis[k.x]=1;
		for (ll i=head[k.x];i;i=e[i].net)
			if (dis[e[i].to]>k.val+e[i].pay){
				dis[e[i].to]=k.val+e[i].pay;
				H.push(Data(e[i].to,dis[e[i].to]));
			}
	}
}
int main(){
	freopen("gift.in","r",stdin);
	freopen("gift.out","w",stdout);
	read(G);
	while (G--){
		read(n),read(m);
		pre();
		s[tot=1]=m;
		s[++tot]=1;
		for (ll i=1;i<=n;++i) a[i].init(),s[++tot]=a[i].v,s[++tot]=a[i].r;
		sort(s+1,s+1+tot);
		tot=unique(s+1,s+1+tot)-s-1;
		for (ll i=2;i<=tot;++i) addedge(i,i-1,0);
		for (ll i=1;i<=n;++i){
			a[i].r=lower_bound(s+1,s+1+tot,a[i].r)-s;
			a[i].v=lower_bound(s+1,s+1+tot,a[i].v)-s;
			addedge(a[i].r,a[i].v,a[i].t);
		}
		S=lower_bound(s+1,s+1+tot,1)-s;
		T=lower_bound(s+1,s+1+tot,m)-s;
		dijstra();
		wr(dis[T]>INF?-1:dis[T]);
		puts("");
	}
	return 0;
}
//sxdakking
/*
3
5 9
5 1 1
10 4 10
8 1 10
11 6 1
7 3 8
4 5
2 1 1
3 2 1
4 3 1
8 4 1
3 10
5 1 3
8 2 5
10 9 2
*/
