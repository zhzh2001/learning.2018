#include <iostream>
#include <cstring>
#include <cstdio>
#include <set>
using namespace std;
typedef int ll;
inline char gc(){
	static char buf[1<<14],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,1<<14,stdin),p1==p2)?EOF:*p1++;
}
inline ll read(ll &x){
	x=0;bool f=1;char c=gc();
	for(;!isdigit(c);c=gc()){if(c=='-')f^=1;if(c==-1)return-1;}
	for(;isdigit(c);c=gc())x=(x<<1)+(x<<3)+(c^48);x=f?x:-x;
	return 1;
}
#define pc putchar
inline void wr(ll x){
	if(x<0)pc('-'),x=-x;
	if(x>9)wr(x/10);pc(x%10|48);
}
const ll N=1e6+10;
char s[N],g[N];
ll m,k,d,n,ans[N],pre[N];
set<ll>vis[N];
inline void solve(){
	if (!vis[k].count(d)){
		memset(pre,0,sizeof pre);
		for (ll i=0;i<k;++i) ++pre[i%d];
		for (ll i=1;i<k;++i) pre[i]+=pre[i-1];
		for (ll i=0;i<n;++i){
			ll now=i;
			for (ll j=0;j<=n-k;++j){
				if (now>=j+k) continue;
				if (now<=j) break;
				else{
					ll bel=(now-j)%d;
					now=(bel>0?pre[bel-1]:0)+(now-j)/d+j;
				}
			}
			ans[now]=i;
		}
		vis[k].insert(d);
	}
	for (ll i=0;i<n;++i) g[i]=s[ans[i]];
	for (ll i=0;i<n;++i) s[i]=g[i];
	printf("%s",s);
	puts("");
}
int main(){
	freopen("sorting.in","r",stdin);
	freopen("sorting.out","w",stdout);
	scanf("%s",s);
	n=strlen(s);
	read(m);
	for (ll i=1;i<=m;++i){
		read(k),read(d);
		solve();
	}
	return 0;
}
//sxdakking
/*
qwerty
3
4 2
6 3
5 2
*/
