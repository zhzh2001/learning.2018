#include<fstream>
#include<random>
#include<windows.h>
using namespace std;
ofstream fout("sorting.in");
const string s="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
const int l=s.length();
int main()
{
	minstd_rand gen(GetTickCount());
	uniform_int_distribution<> d(1000,1e4);
	int n=d(gen),m=1000000/n;
	for(int i=1;i<=n;i++)
	{
		uniform_int_distribution<> dc(0,l-1);
		fout.put(s[dc(gen)]);
	}
	fout<<endl;
	fout<<m<<endl;
	for(int i=1;i<=m;i++)
	{
		uniform_int_distribution<> dk(1,5),dd(1,5);
		fout<<dk(gen)<<' '<<dd(gen)<<endl;
	}
	return 0;
}
