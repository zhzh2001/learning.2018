#include<fstream>
#include<string>
#include<algorithm>
using namespace std;
ifstream fin("sorting.in");
ofstream fout("sorting.ans");
const int N=1e6;
int k,d,num[N];
bool cmp(int a,int b)
{
	if(a%d==b%d)
		return a<b;
	return a%d<b%d;
}
int main()
{
	string s;
	fin>>s;
	int n=s.length();
	int m;
	fin>>m;
	while(m--)
	{
		fin>>k>>d;
		for(int i=0;i<k;i++)
			num[i]=i;
		sort(num,num+k,cmp);
		for(int i=0;i<=n-k;i++)
		{
			string t=s.substr(i,k),t2(k,'\0');
			for(int j=0;j<k;j++)
				t2[j]=t[num[j]];
			s.replace(i,k,t2);
		}
		fout<<s<<endl;
	}
	return 0;
}
