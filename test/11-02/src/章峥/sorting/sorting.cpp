#include<fstream>
#include<string>
using namespace std;
ifstream fin("sorting.in");
ofstream fout("sorting.out");
const int N=1e6,L=22;
int nxt[N],nxtb[L][N];
int main()
{
	string s;
	fin>>s;
	int n=s.length();
	int m;
	fin>>m;
	while(m--)
	{
		int k,d;
		fin>>k>>d;
		int now=0;
		for(int i=0;i<d;i++)
			for(int j=i;j<k;j+=d)
				nxt[j]=now++;
		for(int i=0;i<k;i++)
			nxtb[0][i]=nxt[i]-1;
		for(int j=1;j<L;j++)
			for(int i=0;i<k;i++)
				if(~nxtb[j-1][i])
					nxtb[j][i]=nxtb[j-1][nxtb[j-1][i]];
				else
					nxtb[j][i]=-1;
		string ans(n,'\0');
		for(int i=0;i<n;i++)
		{
			int j,cc;
			if(i<k)
			{
				j=i;
				cc=0;
			}
			else
			{
				j=k-1;
				cc=i-k+1;
			}
			for(int t=L-1;t>=0;t--)
				if(cc+(1<<t)<=n-k&&~nxtb[t][j])
				{
					j=nxtb[t][j];
					cc+=1<<t;
				}
			ans[cc+nxt[j]]=s[i];
		}
		fout<<ans<<'\n';
		s=ans;
	}
	return 0;
}
