#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("gift.in");
ofstream fout("gift.out");
const int N=100005;
const long long INF=1e18;
long long tree[N*8];
void build(int id,int l,int r)
{
	tree[id]=INF;
	if(l<r)
	{
		int mid=(l+r)/2;
		build(id*2,l,mid);
		build(id*2+1,mid+1,r);
	}
}
void modify(int id,int l,int r,int x,long long val)
{
	if(l==r)
		tree[id]=min(tree[id],val);
	else
	{
		int mid=(l+r)/2;
		if(x<=mid)
			modify(id*2,l,mid,x,val);
		else
			modify(id*2+1,mid+1,r,x,val);
		tree[id]=min(tree[id*2],tree[id*2+1]);
	}
}
long long query(int id,int l,int r,int L,int R)
{
	if(L<=l&&R>=r)
		return tree[id];
	int mid=(l+r)/2;
	if(R<=mid)
		return query(id*2,l,mid,L,R);
	if(L>mid)
		return query(id*2+1,mid+1,r,L,R);
	return min(query(id*2,l,mid,L,R),query(id*2+1,mid+1,r,L,R));
}
struct edge
{
	int u,v,w;
	bool operator<(const edge& rhs)const
	{
		return v<rhs.v;
	}
}e[N];
int x[N*2];
int main()
{
	int t;
	fin>>t;
	while(t--)
	{
		int n,m;
		fin>>n>>m;
		int xn=0;
		for(int i=1;i<=n;i++)
		{
			fin>>e[i].v>>e[i].u>>e[i].w;
			x[++xn]=e[i].v;
			x[++xn]=e[i].u;
		}
		x[++xn]=1;x[++xn]=m;
		sort(x+1,x+xn+1);
		sort(e+1,e+n+1);
		build(1,1,xn);
		modify(1,1,xn,1,0);
		for(int i=1;i<=n;i++)
		{
			int u=lower_bound(x+1,x+xn+1,e[i].u)-x,v=lower_bound(x+1,x+xn+1,e[i].v)-x;
			long long res=query(1,1,xn,u,v);
			modify(1,1,xn,v,res+e[i].w);
		}
		int mid=lower_bound(x+1,x+xn+1,m)-x;
		long long ans=query(1,1,xn,mid,xn);
		if(ans==INF)
			fout<<-1<<endl;
		else
			fout<<ans<<endl;
	}
	return 0;
}
