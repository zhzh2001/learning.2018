#include <bits/stdc++.h>
typedef long long ll;
typedef unsigned long long ull;
typedef double db;
#define pa pair<int,int>
#define mk make_pair
#define LOG(format, args...) fprintf(stderr, format, args)
using namespace std;
//defs=================================
const int MAXN=110;
ull a[11000];
ull b[11000];
ull N,T; 
ull ans;
void solve();
//main================================= 
int main() {
	cin>>T;
	while(T--) {
		cin>>N;
		for(int i=1;i<=N;++i) cin>>a[i];
		solve();
	}
	return 0;
}

void dfs(int now,ull val) {
	//cerr<<"now="<<now<<" val="<<val<<endl;
	if(now>N) {
		if(ans>val) ans=val;
		return;
	}
	dfs(now+1,val&b[now]);
	dfs(now+1,val|b[now]);
	dfs(now+1,val^b[now]);
}
void solve() {
	if(N>7) {
		cout<<0<<endl;
		return;
	}
	ans=1e18;
	for(int S=0;S<(1<<N);++S) {
		for(int i=1;i<=N;++i) if((1<<(i-1))&S) b[i]=a[i]; else b[i]=~a[i];
		dfs(2,b[1]);
	}
	cout<<ans<<endl;
}
