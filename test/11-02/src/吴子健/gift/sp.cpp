#include<bits/stdc++.h>
using namespace std;
namespace IO {
    template<typename T>
    inline void read(T &x) {
        T i = 0, f = 1;
        char ch = getchar();
        for(; (ch < '0' || ch > '9') && ch != '-'; ch = getchar());
        if(ch == '-') f = -1, ch = getchar();
        for(; ch >= '0' && ch <= '9'; ch = getchar()) i = (i << 3) + (i << 1) + (ch - '0');
        x = i * f;
    }
    template<typename T>
    inline void wr(T x) {
        if(x < 0) putchar('-'), x = -x;
        if(x > 9) wr(x / 10);
        putchar(x % 10 + '0');
    }
} using namespace IO;

const int N = 5e5 + 50, M = 1e5 + 50, OO = 0x3f3f3f3f;
int n, m, p, dis[N * 10];
typedef pair<int, int> P;
vector<P> G[N * 10]; 
int tot, SuperPoint;
priority_queue<P, vector<P>, greater<P> > que;
bool vst[N * 10];

inline void addEdge(int u, int v, int c){
    G[u].push_back(P(v, c));
}
struct SegTree{
    int num[N * 4];
    inline void Build(int k, int l, int r, int type){
        num[k] = ++tot;
        if(l == r) return;
        int mid = l + r >> 1, lc = k << 1 , rc = k << 1 | 1;
        Build(lc, l, mid, type), type == 1 ? addEdge(num[lc], num[k], 0) : addEdge(num[k], num[lc], 0);
        Build(rc, mid + 1, r, type), type == 1 ? addEdge(num[rc], num[k], 0) : addEdge(num[k], num[rc], 0);
    }
    inline void BuildEdge(int k, int l, int r, int x, int y, int v, int type){
        if(x <= l && r <= y) {
            type == 1 ? addEdge(num[k], SuperPoint, v) : addEdge(SuperPoint, num[k], v);
            return;
        }
        int mid = l + r >> 1, lc = k << 1 , rc = k << 1 | 1;
        if(x <= mid) BuildEdge(lc, l, mid, x, y, v, type);
        if(y > mid) BuildEdge(rc, mid + 1, r, x, y, v, type);
    }
    inline int getNum(int k, int l, int r, int pp){
        if(l == r) return num[k];
        int mid = l + r >> 1, lc = k << 1 , rc = k << 1 | 1;
        if(pp <= mid) return getNum(lc, l, mid, pp);
        else return getNum(rc, mid + 1, r, pp);
    }
}SegIn, SegOut;
int debug[N * 18];
inline void DJ(int p){
    memset(dis, 0x3f, sizeof dis);
    dis[p] = 0;
    que.push(P(0, p));
    while(!que.empty()){
        P t = que.top();que.pop();
        int u = t.second; 
        if(vst[u]) continue;
        vst[u] = true;
        for(int e = G[u].size() - 1; e >= 0; e--){
            int v = G[u][e].first;
//          cout<<u<<"-------->"<<v<<endl;
            if(!vst[v] && dis[v] > dis[u] + G[u][e].second){
                dis[v] = dis[u] + G[u][e].second;
                debug[v] = u;
                que.push(P(dis[v], v));
            }
        }
//      cout<<endl;
    }
}

inline void BuildEdgeBet(int k, int l, int r){
    addEdge(SegIn.num[k], SegOut.num[k], 0);
    if(l == r) return;
    int mid = l + r >> 1, lc = k << 1 , rc = k << 1 | 1;
    BuildEdgeBet(lc, l, mid);
    BuildEdgeBet(rc, mid + 1, r);
}

inline void getAns(int k, int l, int r){
    if(l == r){
        wr(dis[SegIn.num[k]]), putchar('\n');
        return;
    }
    int mid = l + r >> 1, lc = k << 1 , rc = k << 1 | 1;
    getAns(lc, l, mid);
    getAns(rc, mid + 1, r);
}

int main(){
    freopen("h.in" ,"r", stdin);
    freopen("h.out", "w", stdout);
    read(n), read(m), read(p);
    SegIn.Build(1, 1, n, 2);
    SegOut.Build(1, 1, n, 1); 
    SuperPoint = tot;
    for(int i = 1; i <= m; i++){
        int a, b, c, d;
        read(a), read(b), read(c), read(d);
        SuperPoint++;
        SegOut.BuildEdge(1, 1, n, a, b, 0, 1);
        SegIn.BuildEdge(1, 1, n, c, d, 1, 2);
        SuperPoint++;
        SegOut.BuildEdge(1, 1, n, c, d, 0, 1);
        SegIn.BuildEdge(1, 1, n, a, b, 1, 2);
        
    }
    BuildEdgeBet(1, 1, n);
    int pos1 = SegOut.getNum(1, 1, n, p), pos2 = SegIn.getNum(1, 1, n, p);
    addEdge(pos1, pos2, 0);
    DJ(pos1);
    getAns(1, 1, n);
    return 0;
}
