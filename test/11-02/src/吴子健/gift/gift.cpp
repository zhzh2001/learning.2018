#include <bits/stdc++.h>
typedef long long ll;
typedef double db;
#define re register int
#define pa pair<ll,int>
#define mk make_pair
//#define LOG(format, args...) fprintf(stderr, format, args)
using namespace std;
inline char gc()
{
    static char buf[1<<13],*p1=buf,*p2=buf;
    return (p1==p2)&&(p2=(p1=buf)+fread(buf,1,1<<13,stdin),p1==p2)?EOF:*p1++;
}

#define dd c = gc()
inline void rd(int& x)
{
    x = 0;
    int dd;
    bool f = false;
    for(; !isdigit(c); dd)
        if(c == '-')
            f = true;
    for(; isdigit(c); dd)
        x = (x<<1) + (x<<3) + (c^48);
    if(f)
        x = -x;
}
#undef dd
//defs==========================================
const ll MAXVAL=1e9+100,MAXN=1e5+10;
int T,N,M;
map<ll,ll> link;
void init();
void work();
//structure=====================================
struct edge {
	int ed,nxt;ll vv;
}E[MAXN*80];
int head[MAXN*50],Ecnt;
void addEdge(int st,int ed,ll vv) {
	if(!st||!ed) return;
	//if(Ecnt>MAXN*59) exit(112);
	E[++Ecnt].ed=ed,E[Ecnt].nxt=head[st],head[st]=Ecnt;
	E[Ecnt].vv=vv;
}
int cnt;
struct Seg_Tree{//type==1 ���� type==2 ���� 
	int Rt,lc[MAXN*50],rc[MAXN*50];
	void clear() {
		Rt=0;
		memset(lc,0,(sizeof (int))*(cnt+100));
		memset(rc,0,(sizeof (int))*(cnt+100));
	}
	void add(int &x,int B,int E,int L,int R,int goal,int vv) {
		if(!x) x=++cnt;
		//LOG("[%d,%d] id=%d\n",L,R,x);
		if(B<=L&&R<=E) {
			//printf("[%d,%d] x=%d->%d\n",L,R,x,goal);
			addEdge(x,goal,vv);
			return;
		}
		int mid=(L+R)>>1;
		if(B<=mid) {
			int t=lc[x];
			add(lc[x],B,E,L,mid,goal,vv);
			if(t!=lc[x]) {
				addEdge(lc[x],x,0);
				//printf("added [%d,%d]=%d->[%d,%d]=%d\n",L,mid,lc[x],L,R,x);
			}
		}
		if(mid<=E) {
			int t=rc[x];
			add(rc[x],B,E,mid+1,R,goal,vv);
			if(t!=rc[x]) {
				addEdge(rc[x],x,0);
		//	printf("added [%d,%d]=%d->[%d,%d]=%d\n",mid+1,R,rc[x],L,R,x);
			}
		}
	}
	int find_pos(int &x,int pos,int L,int R) {
		if(!x) x=++cnt;
		if(L==R) {
		//	printf("[%d,%d] id=%d (in find)\n",L,R,x);
			return x;
		}
		int mid=(L+R)/2,ret=0;
		if(pos<=mid) {
			int t=lc[x];
			ret=find_pos(lc[x],pos,L,mid);
			if(t!=lc[x]) addEdge(lc[x],x,0);
		}
		else if(pos>mid) {
			int t=rc[x];
			ret=find_pos(rc[x],pos,mid+1,R);	
			if(t!=rc[x]) addEdge(rc[x],x,0);
		}
		return ret;
	} 
}Tout;
//main==========================================
int main() {
	freopen("gift.in","r",stdin);
	freopen("gift.out","w",stdout);
	//T=rd();
	rd(T);
	while(T--) {
		init();
		work();
		//LOG("%d\n",cnt);
	}
	return 0;
}
int tt[MAXN*50];
void init() {
	//LOG("initing\n",0);
	Tout.clear();
	link.clear();

	
	memset(head,0,(sizeof(int))*(cnt+100));
	memset(E,0,(sizeof (edge))*(Ecnt+100));
	Ecnt=0;
	cnt=0;	
	*tt=0;
	
	//N=rd();M=rd();
	rd(N),rd(M);
	link[1]=++cnt;
	if(M<=1) tt[++*tt]=link[1];
	int pos_1=Tout.find_pos(Tout.Rt,1,1,MAXVAL);
	addEdge(link[1],pos_1,0);
	//printf("added %d(1)->%d(1,1)\n",link[1],pos_1);
	
	for(re i=1;i<=N;++i) {
		//int v=rd(),r=rd(),t=rd();
		int v,r,t;
		rd(v);rd(r);rd(t);
		if(!link[v]) {
			link[v]=++cnt;
			if(M<=v) tt[++*tt]=link[v];
		}
		//printf("pos[%d]=%d\n",v,link[v]);
		Tout.add(Tout.Rt,r,MAXVAL,1,MAXVAL,link[v],t);
		int pos=Tout.find_pos(Tout.Rt,v,1,MAXVAL);
		addEdge(link[v],pos,0);
		//printf("added %d->%d\n",link[v],pos);
	}
	//LOG("ininted\n",0);
}

ll dis[MAXN*50];

void dij(int S) {
	memset(dis,0x3f,(cnt+100)*(sizeof (ll)));
	dis[S]=0;
	priority_queue<pa,vector<pa>,greater<pa> >PQ;
	while(PQ.size()) PQ.pop();
	PQ.push(mk(0,S));
	while(!PQ.empty()) {
		pa minv=PQ.top();PQ.pop();
		int st=minv.second;
		if(dis[st]!=minv.first) continue;
		for(int i=head[st];i;i=E[i].nxt) {
			int ed=E[i].ed;
			ll vv=E[i].vv;
			if(dis[ed]>dis[st]+vv) {
				dis[ed]=dis[st]+vv;
				PQ.push(mk(dis[ed],ed));
			}
		}
	}
//	for(int i=1;i<=cnt;++i) {
//		printf("dis[%d]=%lld\n",i,dis[i]);
//	}
}
void work() {
	dij(link[1]);
	ll ans=1e17;
	for(re i=1;i<=*tt;++i) {
		if(dis[tt[i]]<ans) {
			ans=dis[tt[i]];
		}
	}
	if(ans<1e17) printf("%lld\n",ans);
	else puts("-1");
}

