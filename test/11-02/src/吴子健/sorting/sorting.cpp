#include <bits/stdc++.h>
typedef long long ll;
typedef double db;
//#define pa pair<char,int>
#define mk make_pair
#define LOG(format, args...) fprintf(stderr, format, args)
using namespace std;
inline int rd() {
	int x=0,f=1;char ch=getchar();
	for(;!isdigit(ch);ch=getchar()) if(ch=='-') f=-1;
	for(;isdigit(ch);ch=getchar()) x=x*10+ch-'0';
	return x*f;
}
void solve_20();
void solve_30();
struct pa{
	int id,nowid;
	char v;
};
//defs========================
const int MAXN=1e6;
int N,M,K,D;
int IND;
char s[MAXN];
bool cmp(pa x,pa y) {
	if((x.nowid)%D!=(y.nowid)%D) return (x.nowid)%D<(y.nowid)%D;
	else return (x.nowid)<(y.nowid);
}
pa a[MAXN];
//main========================
int main() {
	freopen("sorting.in","r",stdin);
	freopen("sorting.out","w",stdout);
	scanf("%s",s);N=strlen(s);
	for(int i=0;i<N;++i) {
		a[i].v=s[i],a[i].id=i;
	}
	//for(int i=0;i<N;++i) LOG("%c",a[i].v);LOG("%c",'\n');
	M=rd();
	if(N<=100&&M<=100) {
		solve_20();
	} else if(N<=3000) {
		solve_30();
	}
	return 0;
}

void solve_20() {
	while(M--) {
		K=rd(),D=rd();
		for(int i=0;i<=N-K;++i) {
			//IND=i;
			for(int j=i;j<i+K;++j) a[j].nowid=j-i;
			sort(a+i,a+i+K,cmp);
		}
		for(int i=0;i<N;++i) printf("%c",a[i].v);
		puts("");		
	}
}

int to[MAXN];
char m[MAXN],n[MAXN],*ta,*tb;
void solve_30() {
	M--;
	K=rd(),D=rd();
	for(int i=0;i<=N-K;++i) {
		//IND=i;
		for(int j=i;j<i+K;++j) a[j].nowid=j-i;
		sort(a+i,a+i+K,cmp);
	}	
	for(int i=0;i<N;++i) printf("%c",a[i].v);
	puts("");	
		
	ta=m,tb=n;
	for(int i=0;i<N;++i) {
		to[a[i].id]=i;
		m[i]=a[i].v;
	}
	while(M--) {
		K=rd(),D=rd();
		for(int i=0;i<N;++i) {
			tb[to[i]]=ta[i];
		}
		char *tmp=ta;ta=tb;tb=tmp;
		ta[N]='\0';
		puts(ta);		
	}
}
