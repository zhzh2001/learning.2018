#include <cstdio>
#include <cstring>
#include <queue>
#include <iostream>
#include <algorithm>

using namespace std;

typedef long long LL;

namespace IO
{
	inline char gc()
	{
		static const int L = 2333333;
		static char sxd[L], *sss = NULL, *ttt = NULL;
		return (sss == ttt) && (ttt = (sss = sxd) + fread(sxd, 1, L, stdin), sss == ttt) ? EOF : *sss++;
	}
	
	#define dd c = gc()
	template<class T>
	inline int read(T& x)
	{
		x = 0;
		char dd;
		bool f = false;
		for(; !isdigit(c); dd)
		{
			if(c == EOF)
				return 0;
			if(c == '-')
				f = true;
		}
		for(; isdigit(c); dd)
			x = (x << 1) + (x << 3) + (c ^ 48);
		if(f)
			x = -x;
		return 1;
	}
	#undef dd
}

const int maxn = 200005;
const int maxm = 10000005;
const LL inf = 0x3f3f3f3f3f3f3f3f;

int n, m;

namespace sol
{
	struct Edge
	{
		int to, dist, nxt;
	} e[maxm];

	int first[maxn];

	int cnt; // 小心变量重名
	inline void add_edge(int from, int to, int dist)
	{
		e[++cnt].nxt = first[from];
		first[from] = cnt;
		e[cnt].dist = dist;
		e[cnt].to = to;
	}

	inline void clear_edge()
	{
		memset(first, 0, sizeof(first));
		cnt = 0;
	}
}

namespace jiantu
{
	struct Friend
	{
		int v, r, t;

		inline bool operator < (const Friend& other) const
		{
			return this->r < other.r;
		}
	} f[maxn];

	int R[maxn];

	inline void pre()
	{
		IO::read(n), IO::read(m);
//		cout << "n = " << n << endl;
		n++;
		for(int i = 2; i <= n; ++i)
			IO::read(f[i].v), IO::read(f[i].r), IO::read(f[i].t);
		f[1].v = 1, f[1].r = 0, f[1].t = 0;
		sort(f + 1, f + n + 1);
//		for(int i = 1; i <= n; ++i)
//			cout << f[i].v << ' ' << f[i].r << ' ' << f[i].t << endl; 
		for(int i = 1; i <= n; ++i)
		{
			R[i] = f[i].r;
			sol::add_edge(i + n, i, f[i].t);
			if(i != 1)
				sol::add_edge(i + n, i + n - 1, 0);
		}
		for(int i = 1; i <= n; ++i)
		{
			int now = upper_bound(R + 1, R + n + 1, f[i].v) - R - 1;
//			cout << i << ' ' << now << endl;
			sol::add_edge(i, now + n, 0);
		}
	}
}

namespace sol
{
	struct DIJKSTRA
	{
		LL dist[maxn];
		bool vis[maxn];

		struct haha
		{
			LL dist;
			int to;

			inline bool operator < (const haha& other) const
			{
				return this->dist > other.dist;
			}
		};

		inline void dijkstra()
		{
			memset(dist, 0x3f, sizeof(dist));
			memset(vis, 0, sizeof(vis));
			priority_queue<haha> q;
			dist[1] = 0;
			q.push((haha) {0, 1});
			while(!q.empty())
			{
				haha noww = q.top();
				int now = noww.to;
				q.pop();
				if(vis[now])
					continue;
				vis[now] = true;
//				cout << "now = " << now << endl;
				for(int i = first[now]; i; i = e[i].nxt)
				{
					int to = e[i].to;
//					cout << "to = " << to << endl;
					if(!vis[to] && dist[to] > dist[now] + e[i].dist)
					{
						dist[to] = dist[now] + e[i].dist;
						q.push((haha) {dist[to], to});
					}
				}
			}
		}
	} dij;

	inline void solve()
	{
		clear_edge();
		jiantu::pre();
		dij.dijkstra();
		LL ans = inf;
		for(int i = 2; i <= n; ++i)
			if(jiantu::f[i].v >= m)
				ans = min(dij.dist[i], ans);
//		for(int i = 1; i <= n; ++i)
//			cout << dij.dist[i] << ' ';
//		cout << endl;
//		for(int i = 1; i <= n; ++i)
//			cout << dij.dist[i + n] << ' ';
//		cout << endl;
		if(ans >= inf)
			puts("-1");
		else
			printf("%lld\n", ans);
	}
}

int main()
{
	freopen("gift.in", "r", stdin);
	freopen("gift.out", "w", stdout);
	int T;
	IO::read(T);
//	cout << T << endl;
//	return 0;
	while(T--)
		sol::solve();
	fclose(stdin);
	fclose(stdout);
	return 0;
}

/*
3
5 9
5 1 1
10 4 10
8 1 10
11 6 1
7 3 8
4 5
2 1 1
3 2 1
4 3 1
8 4 1
3 10
5 1 3
8 2 5
10 9 2
*/
