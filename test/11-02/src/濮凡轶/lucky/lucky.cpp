// 注意开unsigned long long，用文件流吧，讲道理可以用快读

#include <bitset>
#include <fstream>
#include <iostream>

using namespace std;

typedef unsigned long long LL;

ifstream fin("lucky.in");
ofstream fout("lucky.out");

int n;
LL a[103];

inline LL get(int x)
{
	LL ans = 0;
	ans--;
	for(int i = 0; i < n; ++i)
	{
//	fout << bitset<3>(x) << ' ' << bitset<5>(ans) << ' ' << bitset<5>(a[i]) << endl;
		if(x & (1 << i))
			ans &= ~a[i];
		else
			ans &= a[i];
//	fout << bitset<3>(x) << ' ' << bitset<5>(ans) << ' ' << bitset<5>(a[i]) << endl;
	}
//	cout << ans << endl;
	return ans;
}

inline void solve()
{
	fin >> n;
	if(n > 22)
	{
		fout << "0\n";
		return;
	}
	LL ans = 0;
	ans--;
//	cout << ans << endl;
	for(int i = 0; i < n; ++i)
		fin >> a[i];
	for(int i = 0; ans && i < (1 << n); ++i)
		ans = min(get(i), ans);
	fout << ans << '\n';
}

int main()
{
	int T;
	fin >> T;
	while(T--)
		solve();
	return 0;
}
