#include <bits/stdc++.h>
#define LL unsigned long long 
#define GG unsigned long long 
#define For(i, j, k) for(register int i=j; i<=k; i++)
#define Dow(i, j, k) for(register int i=j; i>=k; i--)
using namespace std;
GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
void writeln(GG x) { write(x); putchar('\n'); }

const int N = 21; 
int n, T; 
int b[N], c[N]; 
GG a[N], sum, Mn, wzt; 

void dfs_2(int x) {
	if(x>=n) {
		sum = wzt; 
		For(i, 1, n-1) {
			GG tmp = a[i+1]; 
			if(!b[i+1]) tmp = ~tmp; 
			if(c[i]==1) sum = sum&tmp; 
			if(c[i]==2) sum = sum|tmp;
			if(c[i]==3) sum = sum^tmp;
		}
		if(sum < Mn) Mn = sum; 
		return; 
	}
	c[x] = 1; dfs_2(x+1); 
	c[x] = 2; dfs_2(x+1); 
	c[x] = 3; dfs_2(x+1);
}

void dfs_1(int x) {
	if(x > n) {
		if(b[1]) wzt = a[1];
		else wzt = ~a[1]; 
		dfs_2(1); 
		return; 
	}
	b[x] = 1; dfs_1(x+1); 
	b[x] = 0; dfs_1(x+1); 
}

int main() {
	freopen("lucky.in", "r", stdin); 
	freopen("lucky.out", "w", stdout); 
	T = read(); 
	while(T--) {
		n = read(); 
		For(i, 1, n) a[i] = read(); 
		Mn = a[1];  

		For(i, 2, n) Mn = Mn&a[i]; 
		dfs_1(1); 
		writeln(Mn); 
	}
}


/*


2
2
3 6
3
1 2 3


*/






