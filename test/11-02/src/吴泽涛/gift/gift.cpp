#include <bits/stdc++.h>
#define LL long long
#define GG long long 
#define For(i, j, k) for(register int i=j; i<=k; i++)
#define Dow(i, j, k) for(register int i=j; i>=k; i--)
using namespace std;
GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
void writeln(GG x) { write(x); putchar('\n'); }

const int N = 1e5+11; 
const LL INF = 1e18; 
int T, n, m, cnt;
LL dis[N];
int visit[N], head[N];  
LL minn[N*8],b[N*2],len,c[N*4],tot,ans; 
struct node{
	int l, r, t, id; 
}gaga[N]; 
struct edge{
	int to, pre, val; 
}e[2000011];
struct data{ 
	LL l,r,v; 
}a[N];
bool cmp(data x,data y) { 
	return x.r<y.r; 
}
LL find(LL x){
	LL l=1, r=tot;
	while (l<=r){
		LL mid=l+r>>1;
		if (c[mid]==x) return mid;
		if (c[mid]<x) 
			l=mid+1;
		else 
			r=mid-1;
	}
}
inline void work(int S, int T) { 
	For(i, 1, n+2) visit[i] = 0; 
	For(i, 1, n+2) dis[i] = INF; 
	dis[S] = 0; dis[0] = INF; 
	For(i, 1, n+2) { 
		int u = 0; 
		For(j, 1, n+2) 
			if(!visit[j] && dis[u] > dis[j]) u = j;
		visit[u] = 1; 
		for(int j=head[u]; j; j=e[j].pre) {
			int v = e[j].to; 
			if(!visit[v] && dis[v] > dis[u]+e[j].val) 
				dis[v] = dis[u]+e[j].val; 
		}
	}
	if(dis[T] == INF) puts("-1"); else
	writeln(dis[T]); 
}
LL query(LL l,LL r,LL s,LL t,LL p){
	if (l==s&&r==t) return minn[p];
	LL mid=l+r>>1;
	if (t<=mid) return query(l,mid,s,t,p<<1);
	else if (s>mid) return query(mid+1,r,s,t,p<<1|1);
	else return min(query(l,mid,s,mid,p<<1),query(mid+1,r,mid+1,t,p<<1|1));
}
inline void add(int x, int y, int val) {
	e[++cnt].to = y; 
	e[cnt].pre = head[x]; 
	e[cnt].val = val; 
	head[x] = cnt; 
}

void modfiy(LL l,LL r,LL pos,LL v,LL p){
	if (l==r) { minn[p]=v; return; }
	LL mid=l+r>>1;
	if (pos<=mid) modfiy(l,mid,pos,v,p<<1);
	else modfiy(mid+1,r,pos,v,p<<1|1);
	minn[p]=min(minn[p<<1],minn[p<<1|1]);
}
inline void dij(int S, int T) { 
	For(i, 1, n+2) visit[i] = 0; 
	For(i, 1, n+2) dis[i] = INF; 
	dis[S] = 0; dis[0] = INF; 
	For(i, 1, n+2) { 
		int u = 0; 
		For(j, 1, n+2) 
			if(!visit[j] && dis[u] > dis[j]) u = j;
		visit[u] = 1; 
		for(int j=head[u]; j; j=e[j].pre) {
			int v = e[j].to; 
			if(!visit[v] && dis[v] > dis[u]+e[j].val) 
				dis[v] = dis[u]+e[j].val; 
		}
	}
	if(dis[T] == INF) puts("-1"); else
	writeln(dis[T]); 
}
void build(LL l,LL r,LL root){
	if (l==r) { minn[root]=INF; return; }
	LL mid=l+r>>1;
	build(l,mid,root*2); build(mid+1,r,root*2+1);
	minn[root]=min(minn[root*2],minn[root*2+1]);
}
signed main() {
	freopen("gift.in", "r", stdin); 
	freopen("gift.out", "w", stdout); 
	T = read(); 
	while(T--) {
		n = read(); m = read(); 
		if(n<=1500) {
 		cnt = 0; 
		For(i, 0, n+2) head[i] = 0; 
		For(i, 1, n) {
			gaga[++cnt].r = read();
			gaga[cnt].l = read(); gaga[cnt].id = cnt; 
			gaga[cnt].t = read(); 
			if(gaga[cnt].l>=gaga[cnt].r) --cnt; 
		}
		n = cnt; cnt = 0; 
		For(i, 1, n) {
			if(gaga[i].l==1) 
				add(n+1, gaga[i].id, gaga[i].t); 
			if(gaga[i].r >= m) add(gaga[i].id, n+2, 0); 
		}
		For(i, 1, n) 
			For(j, 1, n) 
			if(i!=j) 
				if(gaga[j].l<=gaga[i].r && gaga[i].r<gaga[j].r) {
					add(gaga[i].id, gaga[j].id, gaga[j].t); 
				}
		dij(n+1, n+2); 
		continue; 
		}
		memset(b,0,sizeof b);
		len=tot=0;
		For(i, 1, n){
			b[++len]=a[i].r=read();
			b[++len]=a[i].l=read();
			a[i].v=read();
		}
		memset(c,0,sizeof c);
		sort(b+1, b+1+len);
		for (LL l=1,r=1;l<=len;l=++r){
			while (r<len&&b[r+1]==b[l]) ++r;
			c[++tot]=b[l];
		}
		sort(a+1,a+1+n,cmp);
		build(1,tot,1);
		modfiy(1,tot,1,0,1);
		For(i, 1, n){
			LL l=find(a[i].l);
			LL r=find(a[i].r);
			if (l==r) continue;
			LL tmp=query(1,tot,l,r-1,1),now=query(1,tot,r,r,1);
			if (tmp+a[i].v<now) modfiy(1,tot,r,tmp+a[i].v,1);
		}
		ans=query(1,tot,find(m),tot,1);
		if (ans==INF) puts("-1");
		else writeln(ans); 
	}
}







