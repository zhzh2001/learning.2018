#include <bits/stdc++.h>
#define LL long long
#define GG int
#define For(i, j, k) for(register int i=j; i<=k; i++)
#define Dow(i, j, k) for(register int i=j; i>=k; i--)
using namespace std;
GG read() {
    GG x = 0, f = 1;
    char ch = getchar();
    while(ch<'0'||ch>'9') { if(ch=='-') f = -1; ch = getchar(); }
    while(ch>='0'&&ch<='9') { x = x*10+ch-48; ch = getchar(); }
    return x * f;
}
void write(GG x) {
    if(x<0) putchar('-'), x = -x;
    if(x>9) write(x/10);
    putchar(x%10+48);
}
void writeln(GG x) { write(x); putchar('\n'); }

const int N = 1011; 
char s[N]; 
int len, Q, K, Mod;
struct node{
	char c; 
	int id; 
}a[N]; 

inline bool cmp(node a, node b) {
	int x = a.id %Mod; int y = b.id%Mod; 
	if(x != y) return x < y; 
	return a.id < b.id; 
}

int main() {
	freopen("sorting.in", "r", stdin); 
	freopen("sorting.out", "w", stdout); 
	scanf("%s", s); 
	len = strlen(s); 
	Q = read(); 
	For(i, 1, Q) {
		K = read(); Mod = read(); 
		For(j, 0, len-K) {
			For(k, j, j+K-1) { a[k-j].c = s[k]; a[k-j].id = k-j; }
			sort(a, a+K, cmp); 
			For(k, j, j+K-1) s[k] = a[k-j].c; 
		}
		printf("%s\n", s); 
	}
}

/*

qwerty
3
4 2
6 3
5 2


*/






