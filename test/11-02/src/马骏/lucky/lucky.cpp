#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 17
#define int unsigned long long
#define inf 0xffffffffffffffff
using namespace std;
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);putchar(x%10+'0');}
int read(){int d=0,w=1;char c=getchar();for(;c<'0'||c>'9';c=getchar())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=getchar())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
int a[maxn],ans,n,cnt;
void dfs(int k,int now)
{
	cnt++;
	if(k>n||cnt>=1000000)
	{
		ans=min(now,ans);
		return;
	}
	dfs(k+1,now&a[k]);
	dfs(k+1,now|a[k]);
	dfs(k+1,now^a[k]);
	dfs(k+1,(a[k]^inf)&now);
	dfs(k+1,(a[k]^inf)|now);
	dfs(k+1,a[k]^inf^now);
}
signed main()
{
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
	for(int t=read();t--;)
	{
		n=read();
		for(int i=1;i<=n;a[i++]=read());
		ans=inf;
		dfs(2,a[1]);
		dfs(2,inf^a[1]);
		wln(ans);
	}
	return 0;
}
