#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
// #define maxn
// #define int long long
#define inf 0x3f3f3f3f
using namespace std;
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);putchar(x%10+'0');}
int read(){int d=0,w=1;char c=getchar();for(;c<'0'||c>'9';c=getchar())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=getchar())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
struct node
{
	int mst,val,t;
	friend bool operator < (node a,node b)
	{
		return a.val<b.val;
	}
};
set<node> s;
int n,m,now,f,ans;
signed main()
{
	freopen("gift.in","r",stdin);
	freopen("gift.out","w",stdout);
	for(int t=read();t--;)
	{
		n=read();
		m=read();
		for(int i=1;i<=n;i++)
		{
			node tmp;
			tmp.mst=read();
			tmp.val=read();
			tmp.t=read();
			s.insert(tmp);
		}
		for(set<node>::iterator it=++s.begin();it!=s.end();)
		{
			set<node>::iterator it2=--it;
			it++;
			if(it->mst>it2->mst)
			{
				s.erase(it);
				it=++it2;
			}
			else it++;
		}
		now=1;
		f=0;
		ans=0;
		for(;now<m;)
		{
			node lok;
			lok.mst=inf;
			for(lok=*s.begin();lok.mst<=now;s.erase(s.begin()));
			now=lok.val;
			if(lok.mst>now)
			{
				f=1;
				break;
			}
			ans+=1;
		}
		wln(f?-1:ans);
	}
	return 0;
}
