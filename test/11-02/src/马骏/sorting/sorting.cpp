#include <map>
#include <set>
#include <queue>
#include <cmath>
#include <vector>
#include <cstdio>
#include <bitset>
#include <cstring>
#include <iostream>
#include <algorithm>
#define maxn 100010
#define maxk 18
// #define int long long
using namespace std;
void write(int x){if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);putchar(x%10+'0');}
int read(){int d=0,w=1;char c=getchar();for(;c<'0'||c>'9';c=getchar())if(c=='-')
w=-1;for(;c>='0'&&c<='9';c=getchar())d=(d<<1)+(d<<3)+c-48;return d*w;}
void wln(int x){write(x);putchar('\n');}
void wrs(int x){write(x);putchar(' ');}
struct node
{
	int cnt,val;
	friend bool operator < (node a,node b)
	{
		return a.val<b.val||a.val==b.val&&a.cnt<b.cnt;
	}
};
node a[maxn];
char s[maxn],s2[maxn];
int st[maxn][maxk],n,b[maxn],c[maxn];
void change(int x,int y)
{
	for(int i=0;i<x;i++)
	{
		a[i].cnt=i;
		a[i].val=i%y;
	}
	sort(a,a+x);
	for(int i=0;i<x;i++)
		st[i][0]=a[i].cnt;
	for(int i=x;i<n;i++)
		st[i][0]=i;
	for(int i=1;1<<i<=n-x+1;i++)
	{
		for(int j=0;j<1<<i-1;j++)
			st[j][i]=st[j][i-1];
		for(int j=1<<i-1;j<(1<<i-1)+x;j++)
			st[j][i]=st[st[j-(1<<i-1)][i-1]+(1<<i-1)][i-1];
		for(int j=(1<<i-1)+x;j<n;j++)
			st[j][i]=st[j][i-1];
	}
	for(int i=0;i<n;i++)
		b[i]=i;
	int tmp=n-x+1,now=0;
	for(int i=17;i>=0;i--)
		if(tmp>=1<<i)
		{
			for(int j=now;j<n;j++)
				c[j]=b[st[j-now][i]+now];
			for(int j=now;j<n;j++)
				b[j]=c[j];
			tmp-=1<<i;
			now+=1<<i;
		}
	for(int i=0;i<n;i++)
		s2[i]=s[b[i]];
	for(int i=0;i<n;i++)
		s[i]=s2[i];
}
signed main()
{
	freopen("sorting.in","r",stdin);
	freopen("sorting.out","w",stdout);
	gets(s);
	n=strlen(s);
	for(int m=read();m--;)
	{
		int x=read(),y=read();
		change(x,y);
		puts(s);
	}
	return 0;
}
