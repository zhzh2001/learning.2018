#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 1000005
#define inf (1ll<<60)
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=(x<<1)+(x<<3)+ch-'0'; ch=getchar(); }
	return x*f;
}
char s[N],tmp[N]; ll n,m,d,k,pos[N],f[N],g[N];
struct data { ll x,y; char t; }a[N];
bool cmp(data x,data y){ return x.x==y.x?x.y<y.y:x.x<y.x; }
bool solve(ll l,ll r){
	ll len=0;
	rep(i,l,r) a[++len]=(data){(i-l)%d,i,s[i]};
	sort(a+1,a+1+len,cmp);
	rep(i,l,r) s[i]=a[i-l+1].t;
}
int main(){
	freopen("sorting.in","r",stdin);
	freopen("sorting.out","w",stdout);
	scanf("%s",s); n=strlen(s); m=read();
	if (n<=100&&m<=100){
		while (m--){
			k=read(); d=read();
			rep(i,0,n-k) solve(i,i+k-1);
			printf("%s\n",s);
		}
		return 0;
	}else{
		k=read(); d=read(); ll len=0;
		rep(i,0,k-1) a[++len]=(data){i%d,i,s[i]};
		sort(a+1,a+1+len,cmp);
		rep(i,0,k-1) f[i]=a[i+1].y;
		rep(i,0,n-1) pos[i]=i;
		rep(i,0,n-k){
			rep(j,i,i+k-1) g[j]=pos[f[j-i]+i];
			rep(j,i,i+k-1) pos[j]=g[j];
		}
		while (m--){
			rep(i,0,n-1) tmp[i]=s[pos[i]];
			rep(i,0,n-1) s[i]=tmp[i];
			printf("%s\n",s);
		}
		return 0;
	}
}
/*
51324
3
3 2
3 2
3 2

*/
