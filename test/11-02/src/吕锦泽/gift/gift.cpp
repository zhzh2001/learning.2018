#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 100005
#define inf (1ll<<60)
#define rep(i,j,k) for (ll i=j;i<=k;++i)
#define per(i,j,k) for (ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9') { if (ch=='-') f=-1; ch=getchar(); }
	while (ch>='0'&&ch<='9') { x=(x<<1)+(x<<3)+ch-'0'; ch=getchar(); }
	return x*f;
}
ll n,m,minn[N<<3],b[N<<1],len,c[N<<2],tot,ans; 
struct data{ ll l,r,v; }a[N];
bool cmp(data x,data y) { return x.r<y.r; }
void build(ll l,ll r,ll p){
	if (l==r) { minn[p]=inf; return; }
	ll mid=l+r>>1;
	build(l,mid,p<<1); build(mid+1,r,p<<1|1);
	minn[p]=min(minn[p<<1],minn[p<<1|1]);
}
void modfiy(ll l,ll r,ll pos,ll v,ll p){
	if (l==r) { minn[p]=v; return; }
	ll mid=l+r>>1;
	if (pos<=mid) modfiy(l,mid,pos,v,p<<1);
	else modfiy(mid+1,r,pos,v,p<<1|1);
	minn[p]=min(minn[p<<1],minn[p<<1|1]);
}
ll query(ll l,ll r,ll s,ll t,ll p){
	if (l==s&&r==t) return minn[p];
	ll mid=l+r>>1;
	if (t<=mid) return query(l,mid,s,t,p<<1);
	else if (s>mid) return query(mid+1,r,s,t,p<<1|1);
	else return min(query(l,mid,s,mid,p<<1),query(mid+1,r,mid+1,t,p<<1|1));
}
ll find(ll x){
	ll l=1,r=tot;
	while (l<=r){
		ll mid=l+r>>1;
		if (c[mid]==x) return mid;
		if (c[mid]<x) l=mid+1;
		else r=mid-1;
	}
}
void work(){
	n=read(); m=read();
	memset(b,0,sizeof b);
	memset(c,0,sizeof c);
	len=tot=0;
	rep(i,1,n){
		b[++len]=a[i].r=read();
		b[++len]=a[i].l=read();
		a[i].v=read();
	}
	sort(b+1,b+1+len);
	for (ll l=1,r=1;l<=len;l=++r){
		while (r<len&&b[r+1]==b[l]) ++r;
		c[++tot]=b[l];
	}
	sort(a+1,a+1+n,cmp);
	build(1,tot,1);
	modfiy(1,tot,1,0,1);
	rep(i,1,n){
		ll l=find(a[i].l);
		ll r=find(a[i].r);
		if (l==r) continue;
		ll tmp=query(1,tot,l,r-1,1),now=query(1,tot,r,r,1);
		if (tmp+a[i].v<now) modfiy(1,tot,r,tmp+a[i].v,1);
	}
	ans=query(1,tot,find(m),tot,1);
	if (ans==inf) puts("-1");
	else printf("%lld\n",ans);
}
int main(){
	freopen("gift.in","r",stdin);
	freopen("gift.out","w",stdout);
	ll T=read();
	while (T--)
		work();
}
/*

3
5 9
5 1 1
10 4 10
8 1 10
11 6 1
7 3 8
4 5
2 1 1
3 2 1
4 3 1
8 4 1
3 10
5 1 3
8 2 5
10 9 2

*/
