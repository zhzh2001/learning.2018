#include<bits/stdc++.h>
#define ll long long
#define int long long
#define ull unsigned ll
#define rep(i,j,k) for(ll i=j;i<=k;++i)
#define per(i,j,k) for(ll i=j;i>=k;--i)
using namespace std;
inline ll read(){
    ll x=0,f=1; char ch=getchar();
    while(ch<'0'||ch>'9') { if(ch=='-')f=-1;ch=getchar(); }
    while(ch>='0'&&ch<='9') { x=x*10+ch-48; ch=getchar(); }
    return x*f;
}
int T,n; ull a[110]; ull mn,M;
void dfs(int x,ull ans){
	if(ans==0) mn=0; if (mn==0) return;
	if (x==n+1) { mn=min(mn,ans); return; }
	dfs(x+1,ans&a[x]); if(mn==0)return;
	a[x]^=M; dfs(x+1,ans&a[x]); a[x]^=M;
}
signed main(){
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
	T=read(); M=(1<<63)-1;
	while(T--){
		n=read(); mn=M; rep(i,1,n) a[i]=read();
		dfs(2,a[1]); if(mn==0) { cout<<mn<<endl; continue; }
		dfs(2,a[1]^M); cout<<mn<<endl;
	}
}

/*
2
2
3 6
3 
1 2 3


*/
