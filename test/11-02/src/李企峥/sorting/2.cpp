#include<bits/stdc++.h>
using namespace std;
#define ll long long
inline ll read()
{
	ll x=0,f=1;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-')f=-1;
	for(;ch>='0'&&ch<='9';ch=getchar())x=x*10+ch-'0';
	return x*f;
}
inline void write(ll x)
{
	if(x<0)putchar('-'),x=-x;
	if(x>9)write(x/10);
	putchar('0'+x%10);
}
inline void writeln(ll x)
{
	write(x);
	puts("");
}
const int N=1000005;
char ch[N];
int n,m;
struct xxx{
	char ch;
	int x,y;
}t[N];
inline bool cmp(xxx a,xxx b){
	if(a.x==b.x)return a.y<b.y;
	return a.x<b.x;
}
inline void solve(){
	m=read();
	n=strlen(ch);
	while(m--){
		int k,d;
		k=read();d=read();
		for(int i=0;i<=n-k;i++){
			for(int j=i;j<=i+k-1;j++){
				t[j-i].ch=ch[j];
				t[j-i].x=(j-i)%d;
				t[j-i].y=(j-i);
			}
			sort(t,t+k,cmp);
			for(int j=i;j<=i+k-1;j++)
				ch[j]=t[j-i].ch;
		}
		for(int i=0;i<=n-1;i++)
			putchar(ch[i]);
		puts("");
	}
}
int main()
{
	cin>>ch;
	solve();
	return 0;
}
