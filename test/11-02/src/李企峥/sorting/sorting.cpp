#include<bits/stdc++.h>
#define ll long long
#define int long long 
#define For(i,a,b) for(int i=a;i<=b;i++)
#define Rep(i,a,b) for(int i=b;i>=a;i--)
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-48;ch=getchar();}
    return x*f;
}
char s[1001000];
int n,m,k,d;
struct D{
	char s;
	int x,y;
}t[1001000];
inline bool cmp(D a,D b)
{
	if(a.x==b.x)return a.y<b.y;
	return a.x<b.x;
}
signed main()
{
	freopen("sorting.in","r",stdin);
	freopen("sorting.out","w",stdout);
	cin>>s;	
	m=read();
	n=strlen(s);
	while(m--)
	{
		k=read();d=read();
		For(i,0,n-k)
		{
			For(j,i,i+k-1)
			{
				t[j-i].s=s[j];
				t[j-i].x=(j-i)%d;
				t[j-i].y=(j-i);
			}
			sort(t,t+k,cmp);
			For(j,i,i+k-1)s[j]=t[j-i].s;
		}
		For(i,0,n-1)cout<<s[i];
		puts("");
	}
	return 0;
}
