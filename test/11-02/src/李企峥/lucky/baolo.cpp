#include<bits/stdc++.h>
using namespace std;
#define ll unsigned long long
const int N=105;
ll ans;
int n,T;
ll a[N];
inline ll check()
{
	ll ret=a[1];
	for(int i=2;i<=n;i++)
		ret&=a[i];
//	for(int i=1;i<=n;i++)
//		cout<<a[i]<<' ';
//	cout<<endl;
//	cout<<ret<<endl;
	return ret;
}
void dfs(int k)
{
	if(k>n){
		ans=min(ans,check());
		return;
	}
	dfs(k+1);
	a[k]=~(a[k]);
	dfs(k+1);
	a[k]=~(a[k]);
}
int main()
{
	freopen("lucky.in","r",stdin);
	freopen("1.out","w",stdout);
	cin>>T;
	while(T--){
		cin>>n;
		for(int i=1;i<=n;i++)
			cin>>a[i];
//		cout<<(~a[1])<<endl;
		ans=1e18;
		dfs(1);
		cout<<ans<<endl;
	}
}
