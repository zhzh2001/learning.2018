#include<bits/stdc++.h>
#define ll long long
#define int long long
#define For(i,a,b) for(int i=a;i<=b;i++)
#define Rep(i,a,b) for(int i=b;i>=a;i--)
#define fi first
#define sd second
#define ff fi.fi
#define fs fi.sd
#define p pair<int,int>
using namespace std;
pair<p,int>a[100100];
int n,m;
int T;
int b[100100];
int sz,fr,tt;
bool flag;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-48;ch=getchar();}
    return x*f;
}
void del(int wz)
{
	b[wz]=b[sz];
	sz--;
	if(sz==0)return;
	while(wz*2<=sz)
	{
		int mn,mn1;
		mn=b[wz*2];mn1=wz*2;
		if(wz*2+1<=sz&&b[wz*2+1]<mn)
		{
			mn=b[wz*2+1];
			mn1=wz*2+1;
		}
		if(mn>b[wz])return;
		swap(b[wz],b[mn1]);
		wz=mn1;
	}
	return;
}
void up(int wz)
{
	if(wz==1)return;
	for(;b[wz]<b[wz/2]&&wz!=1;wz/=2)swap(b[wz],b[wz/2]);
	return;
}
void ins(int x)
{
	sz++;
	b[sz]=x;
	up(sz);
}
signed main() 
{
	freopen("gift.in","r",stdin);
	freopen("gift.out","w",stdout);
	T=read();
	while(T--)
	{
		n=read();m=read();
		map<int,int>f;
		flag=true;
		For(i,1,n)
		{
			a[i].fs=read();
			if(a[i].fs>=m)a[i].fs=m;
			a[i].ff=read();
			a[i].sd=read();
			f[a[i].fs]=1e12;
			if(a[i].sd!=1)flag=false;
		}
		sort(a+1,a+n+1);
		if(flag)
		{
			int x=1,mx=0;
			int ans=0;
			For(i,1,n)
			{
				if(a[i].ff<=x)mx=max(mx,a[i].fs);
					else
					{
						x=mx;
						ans++;
					}
			}
			if(mx==m&&x!=m)
			{
				x=m;
				ans++;
			}
			if(x==m)cout<<ans<<endl;
				else cout<<-1<<endl;
			continue;
		}
		sz=0;
		ins(1);
		f[1]=0;
		fr=1;
		//cout<<1<<endl;
		while(sz>0)
		{
			int x=b[1];
			del(1);
			//cout<<x<<endl;
			//cout<<sz<<endl;
			tt=fr;
			while(a[tt].fs<=x&&tt<=n)
			{
				tt++;
			}
			fr=tt;
			while(tt<=n)
			{
				if(a[tt].ff>x)break;
				if(f[a[tt].fs]==1e12)ins(a[tt].fs);
				f[a[tt].fs]=min(f[a[tt].fs],f[x]+a[tt].sd);
				tt++;
			}
		}
		if(f[m]==1e12)cout<<-1<<endl;
			else cout<<f[m]<<endl;
	}
	return 0;
}
/*
3
5 9
5 1 1
10 4 10
8 1 10
11 6 1
7 3 8
4 5
2 1 1
3 2 1
4 3 1
8 4 1
3 10
5 1 3
8 2 5
10 9 2
*/

