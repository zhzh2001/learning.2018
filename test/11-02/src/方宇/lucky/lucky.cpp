#include<bits/stdc++.h>
#define LL long long
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define fy 20021218020021218
#define N 100010
using namespace std;
LL a[20][100],c[20][100],d[20][100],boo[20];
LL n,m,i,j,k,l,r,h,T,ans;
LL read()
{
	LL x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
void get_ans(LL m)
{
	rep(i,0,60) d[0][i]=c[0][i];
	rep(i,1,n-1)
	{
		if(boo[i]==1)
		{
			rep(j,0,60) if(c[i][j]&&d[i-1][j]) d[i][j]=1; else d[i][j]=0;
		}
		if(boo[i]==2)
		{
			rep(j,0,60) if(c[i][j]||d[i-1][j]) d[i][j]=1; else d[i][j]=0;
		}
		if(boo[i]==3)
		{
			rep(j,0,60) if(c[i][j]!=d[i-1][j]) d[i][j]=1; else d[i][j]=0;
		}
	}
	LL sum=0;
	rep(i,0,60) if(d[n-1][i]) sum+=1<<i;
	if(sum<ans) ans=sum;
}
void doit(LL k,LL m)
{
	if(k==n) 
	{
		get_ans(m);
		return;
	}
	rep(i,1,3)
	{
		boo[k]=i;
		doit(k+1,m);
	}
}
int main()
{
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
	T=read();
	rep(t,1,T)
	{
		n=read(); ans=fy;
		rep(i,0,n-1)
		{
			k=read(); h=0;
			while(k)
			{
				if(k&1) a[i][h]=1;
				h++; k/=2;
			}
		}
		rep(i,0,(1<<n)-1) 
		{
			rep(j,0,n-1)
			  if(i&(1<<j)) rep(k,0,60) c[j][k]=1-a[j][k];
			  else rep(k,0,60) c[j][k]=a[j][k];
			doit(1,i);
		}
		printf("%lld\n",ans);
	}
	return 0;
}
