#include<bits/stdc++.h>
#define LL long long
#define rep(i,l,r) for(LL i=l;i<=r;i++)
#define dep(i,r,l) for(LL i=r;i>=l;i--)
#define fy 20021218020021218
#define N 100010
using namespace std;
struct node
{
	LL getv,need,time; 
}a[N],Q[N];
LL n,m,i,j,k,h,l,r,num,T;
LL read()
{
	LL x=0,k=1; char c;
	while(!isdigit(c=getchar())) if(c=='-') k=-k;
	x=c^48;
	while(isdigit(c=getchar())) x=(x<<1)+(x<<3)+(c^48);
	return x*k;
}
bool cmp(node a,node b){return a.getv<b.getv;}
LL find(LL l,LL r,LL k)
{
	while(l<=r)
	{
		LL mid=(l+r)>>1;
		if(Q[mid].getv<k) l=mid+1;
		else r=mid-1;	
	}
	return r+1;
}
int main()
{
	freopen("gift.in","r",stdin);
	freopen("gift.out","w",stdout);
	T=read();
	rep(t,1,T)
	{
		n=read(); m=read();
		rep(i,1,n) 
		{
			a[i].getv=read();
			a[i].need=read();
			a[i].time=read();
		}
		sort(a+1,a+n+1,cmp);
		num=1; Q[1].getv=1; Q[1].time=0;
		rep(i,1,n)
		{
			k=find(1,num,a[i].need);
			if(k>num) continue;
			h=a[i].time+Q[k].time;
			while(Q[num].time>=h) num--;
			num++; Q[num].getv=a[i].getv; Q[num].time=h;
		}
		k=find(1,num,m);
		if(k>num) printf("-1\n");
		else printf("%lld\n",Q[k].time);
	}
	return 0;
}
