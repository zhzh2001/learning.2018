#include <bits/stdc++.h>
#define int unsigned long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
int n,ans,b[210],a[210],c[210],bin;
inline void check(){
	int sum=bin;
	for(int i=1;i<=n;i++){
		if(b[i])sum&=(bin^a[i]);else sum&=a[i];
	}
	ans=min(ans,sum);
}
inline void dfs(int x){
	if(x==n+1){
		check();return;
	}
	b[x]=1;dfs(x+1);
	b[x]=0;dfs(x+1);
}
inline void Check(){
	int sum=bin;
	for(int i=1;i<=n;i++){
		int k=a[i];if(b[i])k=bin^k;
		if(c[i]==0)sum|=k;
		if(c[i]==1)sum&=k;
		if(c[i]==2)sum^=k;
	}
	ans=min(ans,sum);
}
inline void Dfss(int x){
	if(x==n+1){
		Check();return;
	}
	c[x]=2;Dfss(x+1);
	c[x]=1;Dfss(x+1);
	c[x]=0;Dfss(x+1);
}
inline void Dfs(int x){
	if(x==n+1){
		Dfss(2);return;
	}
	b[x]=1;Dfs(x+1);
	b[x]=0;Dfs(x+1);
}
signed main()
{
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
	bin=0;c[1]=1;
	for(int i=0;i<64;i++)bin|=(1ll<<i);
	for(int T=read();T;T--){
		n=read();ans=bin;
		for(int i=1;i<=n;i++)a[i]=read();
		if(n>17){puts("0");continue;}
		if(n<10)Dfs(1);else dfs(1);
		writeln(ans);
	}
	return 0;
}
