#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=1e5+10;
struct ppap{int v,r,t;}a[N];
queue<int>q;
int nedge=0,p[20*N],nex[20*N],head[20*N],c[20*N];
inline void addedge(int a,int b,int v){
	p[++nedge]=b;nex[nedge]=head[a];head[a]=nedge;
	c[nedge]=v;
}
int dist[N],vis[N],n,m;
inline void spfa(){
	memset(vis,0,sizeof vis);
	memset(dist,0x3f,sizeof dist);dist[n+1]=0;vis[n+1]=1;
	q.push(n+1);
	while(!q.empty()){
		int now=q.front();q.pop();
		for(int k=head[now];k;k=nex[k])if(dist[p[k]]>dist[now]+c[k]){
			dist[p[k]]=dist[now]+c[k];
			if(!vis[p[k]]){
				vis[p[k]]=1;q.push(p[k]);
			}
		}vis[now]=0;
	}
}
signed main()
{
	freopen("gift.in","r",stdin);
	freopen("baoli.out","w",stdout);
	for(int T=read();T;T--){
		n=read();m=read();
		memset(head,0,sizeof head);nedge=0;
		for(int i=1;i<=n;i++){
			a[i].v=read();a[i].r=read();a[i].t=read();
			for(int j=1;j<i;j++){
				if(a[i].v>=a[j].r)addedge(i,j,a[j].t);
				if(a[i].r<=a[j].v)addedge(j,i,a[i].t);
			}
			if(a[i].r==1)addedge(n+1,i,a[i].t);
		}
		spfa();int ans=1e18;
		for(int i=1;i<=n;i++)if(a[i].v>=m)ans=min(ans,dist[i]);
		if(ans>=1e18)ans=-1;
		writeln(ans);
	}
	return 0;
}
