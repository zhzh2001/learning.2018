#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=1e5+10;
struct ppap{int v,r,t;}a[N];
int f[N],n,m,t[4*N];
inline bool cmp(ppap a,ppap b){
	return a.v<b.v;
}
inline void build(int l,int r,int nod){
	t[nod]=1e18;
	if(l==r){
		if(l==0)t[nod]=0;
		return;
	}
	int mid=l+r>>1;
	build(l,mid,nod<<1);build(mid+1,r,nod<<1|1);
	t[nod]=min(t[nod<<1],t[nod<<1|1]);
}
inline void xg(int l,int r,int x,int v,int nod){
	if(l==r){t[nod]=v;return;}
	int mid=l+r>>1;
	if(x<=mid)xg(l,mid,x,v,nod<<1);else xg(mid+1,r,x,v,nod<<1|1);
	t[nod]=min(t[nod<<1],t[nod<<1|1]);
}
inline int smin(int l,int r,int i,int j,int nod){
	if(l>=i&&r<=j)return t[nod];
	int mid=l+r>>1,ans=1e18;
	if(i<=mid)ans=min(ans,smin(l,mid,i,j,nod<<1));
	if(j>mid)ans=min(ans,smin(mid+1,r,i,j,nod<<1|1));
	return ans;
}
signed main()
{
	freopen("gift.in","r",stdin);
	freopen("gift.out","w",stdout);
	for(int T=read();T;T--){
		n=read();m=read();
		for(int i=1;i<=n;i++){
			a[i].v=read();a[i].r=read();a[i].t=read();
		}
		a[0].v=1;
		sort(a+1,a+n+1,cmp);
		memset(f,0x3f,sizeof f);f[0]=0;
		build(0,n,1);int ans=1e18;
		for(int i=1;i<=n;i++){
			int l=0,r=i-1,p=1e18;
			while(l<=r){
				int mid=l+r>>1;
				if(a[i].r<=a[mid].v)p=mid,r=mid-1;
				else l=mid+1;
			}
			if(p<i)f[i]=smin(0,n,p,i-1,1)+a[i].t;
			xg(0,n,i,f[i],1);
			if(a[i].v>=m)ans=min(ans,f[i]);
		}
		if(ans>=1e18)ans=-1;
		writeln(ans);
	}
	return 0;
}
