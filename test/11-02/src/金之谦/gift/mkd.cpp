#include <bits/stdc++.h>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
signed main()
{
	freopen("gift.in","w",stdout);
	srand(time(0));
	writeln(1);
	int n=rand()%100+1,m=rand()*rand()%(int)(1e9)+1;
	cout<<n<<' '<<m<<endl;
	for(int i=1;i<=n;i++){
		int p=rand()%10;
		int x=rand()*rand()%(int)(1e9)+1,y=rand()*rand()%x+1,t=rand()*rand()%(int)(1e9)+1;
		if(!p)y=1;
		write(x);putchar(' ');write(y);putchar(' ');writeln(t);
	}
	return 0;
}
