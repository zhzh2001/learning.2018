#include <bits/stdc++.h>
#define pa pair<int,int>
#define mkp make_pair
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){
	if(x<0){putchar('-');x=-x;}if(x>9)write(x/10);
	putchar(x%10+'0');
}
inline void writeln(int x){
	write(x);puts("");
}
const int N=1e6+10;
char s[N],t[N];
int n,m,pt[N];
vector<int>e[N];
map<pa,int>mp;
int main()
{
	freopen("sorting.in","r",stdin);
	freopen("sorting.out","w",stdout);
	scanf("%s",s);n=strlen(s);m=read();
	int cnt=0;
	for(int T=1;T<=m;T++){
		int k=read(),d=read();
		int ck;
		if(mp[mkp(k,d)])ck=mp[mkp(k,d)];
		else{
			ck=++cnt;mp[mkp(k,d)]=ck;
			for(int i=0;i<n;i++)e[ck].push_back(i);
			for(int i=0;i<=n-k;i++){
				int pnt=-1;
				for(int j=0;j<d;j++)
					for(int l=i+j;l<i+k;l+=d){
						pt[++pnt]=e[ck][l];
					}
				for(int j=0;j<k;j++)e[ck][i+j]=pt[j];
			}
		}
		for(int i=0;i<n;i++)t[i]=s[e[ck][i]];
		for(int i=0;i<n;i++)s[i]=t[i];
		printf("%s\n",s);
	}
	return 0;
}
