#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
const int N=1e6+7;
int n,m,K,d;
char s[N],t[N];
int id[N],tmp[N];
void change1(int x) {
	for (int i=0;i<K;i++) t[i]=s[x+i];
	int now=-1;
	for (int i=0;i<d;i++)
		for (int j=i;j<K;j+=d) now++,s[now+x]=t[j];
}
void solve1() {
	scanf("%d",&m);
	while (m--) {
		scanf("%d%d",&K,&d);
		for (int j=0;j<=n-K;j++) change1(j);
		puts(s);
	}
}
void change2(int x) {
	for (int i=0;i<K;i++) tmp[i]=id[x+i];
	int now=-1;
	for (int i=0;i<d;i++)
		for (int j=i;j<K;j+=d) now++,id[now+x]=tmp[j];
}
void solve2() {
	for (int i=0;i<n;i++) id[i]=i;
	scanf("%d",&m);scanf("%d%d",&K,&d);
	for (int j=0;j<=n-K;j++) change2(j);
	for (int i=1;i<=m;i++) {
		for (int j=0;j<n;j++) t[j]=s[j];
		for (int j=0;j<n;j++) s[j]=t[id[j]];
		puts(s);
	}
}
int main() {
	freopen("sorting.in","r",stdin);
	freopen("sorting.out","w",stdout);
	scanf("%s",s);n=strlen(s);
	if (n<=100) solve1();
	else solve2();
}
