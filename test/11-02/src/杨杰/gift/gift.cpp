#include <cstdio>
#include <algorithm>
using namespace std;
#define dd c=getchar()
int read() {int s=0,w=1;char c;while (dd,c>'9' || c<'0') if (c=='-') w=-1;while (c>='0' && c<='9') s=(s<<3)+(s<<1)+c-'0',dd;return s*w;}
#undef dd
#define ll long long
const int N=1e5+7;
const ll inf=1e15;
struct node {
	int v,r,t;
	bool operator <(node a) const {
		return r<a.r;
	}
}p[N];
int T,n,m,cnt;
int b[N<<1];
ll tr[N<<4];
#define ls (ts<<1)
#define rs (ts<<1|1)
void build(int ts,int l,int r) {
	if (l==1) tr[ts]=0;
	else tr[ts]=inf;
	if (l==r) return;
	int mid=(l+r)>>1;
	build(ls,l,mid);build(rs,mid+1,r);
}
ll query(int ts,int l,int r,int L,int R) {
	if (l==L && r==R) return tr[ts];
	int mid=(l+r)>>1;
	if (R<=mid) return query(ls,l,mid,L,R);
	else if (L>mid) return query(rs,mid+1,r,L,R);
	else return min(query(ls,l,mid,L,mid),query(rs,mid+1,r,mid+1,R));
}
void insert(int ts,int l,int r,int pos,ll v) {
	tr[ts]=min(tr[ts],v);
	if (l==r) return;
	int mid=(l+r)>>1;
	if (pos<=mid) insert(ls,l,mid,pos,v);
	else insert(rs,mid+1,r,pos,v);
}
int main() {
	freopen("gift.in","r",stdin);
	freopen("gift.out","w",stdout);
	T=read();
	while (T--) {
		n=read(),m=read();
		cnt=0;
		b[++cnt]=1;b[++cnt]=m;
		for (int i=1;i<=n;++i) p[i].v=read(),p[i].r=read(),p[i].t=read(),b[++cnt]=p[i].r,b[++cnt]=p[i].v;
		sort(b+1,b+cnt+1);
		cnt=unique(b+1,b+cnt+1)-b-1;
		build(1,1,cnt);
		sort(p+1,p+n+1);
		for (int i=1;i<=n;i++) {
			p[i].v=lower_bound(b+1,b+cnt+1,p[i].v)-b;
			p[i].r=lower_bound(b+1,b+cnt+1,p[i].r)-b;
		}
		for (int i=1;i<=n;i++) {
			ll v=query(1,1,cnt,p[i].r,cnt)+p[i].t;
			insert(1,1,cnt,p[i].v,v);
		}
		m=lower_bound(b+1,b+cnt+1,m)-b;
		ll ans=query(1,1,cnt,m,cnt);
		if (ans==inf) puts("-1");
		else printf("%lld\n",ans);	
	}
}
