#include <cstdio>
#include <set>
using namespace std;
#define ll unsigned long long
int n,T,flg;
ll v;
set<ll>s[105][105];
set<ll>::iterator it1,it2;
int main() {
	freopen("lucky.in","r",stdin);
	freopen("lucky.out","w",stdout);
	scanf("%d",&T);
	while (T--) {
		for (int i=1;i<=n;i++) for (int j=1;j<=n;j++) s[i][j].clear();
		scanf("%d",&n);flg=0;
		for (int i=1;i<=n;i++) scanf("%llu",&v),s[i][i].insert(v),s[i][i].insert(~v);
		for (int j=1;j<=n;j++)
			for (int i=j-1;i>=1;i--) {
				for (int k=i;k<j;k++) {
					for (it1=s[i][k].begin();it1!=s[i][k].end();it1++)
						for (it2=s[k+1][j].begin();it2!=s[k+1][j].end();it2++) {
							ll x=*it1,y=*it2;
							s[i][j].insert(x|y);s[i][j].insert(x&y);s[i][j].insert(x^y);
						}
				}
				if (*(s[i][j].begin())==0) {flg=1;break;}
			}
		if (flg) puts("0");
		else printf("%llu\n",*(s[1][n].begin()));
	}
}
