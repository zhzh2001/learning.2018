#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cmath>
#define ll long long
#define file "sorting"
using namespace std;
int read(){
	char c;int num,f=1;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;num=c-'0';
	while(c=getchar(), isdigit(c))num=num*10+c-'0';
	return f*num;
}
int n,m,d,k;
char c[2000009];
char t[2000009];
int cmp(char a,char b){
	if(a%d==b%d)return a<b;
	return a%d<b%d;
}
int main()
{
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	scanf("%s",c);
	n=strlen(c);m=read();
	for(int Case=1;Case<=m;Case++){
		k=read();d=read();
		int now;
		for(int i=0;i<=n-k;i++){
			now=i;
			for(int j=0;j<d;j++){
				for(int p=0;p<k&&p+j<k;p+=d,now++){
					t[now]=c[i+p+j];
				}
			}
			for(int j=0;j<now;j++){
				c[j]=t[j];
			}
		}
		cout<<c<<endl;
	}
	
	return 0;
}

