#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cmath>
#define ll long long
#define ull unsigned long long
#define file "lucky"
using namespace std;
ull read(){
	char c;ull num,f=1;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;num=c-'0';
	while(c=getchar(), isdigit(c))num=num*10+c-'0';
	return f*num;
}
int n;
ull a[109],now,ans;
void dfs(int d){
	if(d==n+1){
		ans=min(ans,now);
		return ;
	}
	ull tmp=now;
	now=tmp&a[d];dfs(d+1);
	if(ans==0)return ;
	now=tmp&(~a[d]);dfs(d+1);
	if(ans==0)return ;
	now=tmp^a[d];dfs(d+1);
	if(ans==0)return ;
	now=tmp^(~a[d]);dfs(d+1);
	if(ans==0)return ;
	now=tmp|a[d];dfs(d+1);
	if(ans==0)return ;
	now=tmp|(~a[d]);dfs(d+1);
	if(ans==0)return ;
}
void work(){
	scanf("%d",&n);
	/*if(n>=12){
		cout<<0<<endl;
		return ;
	}*/
	ans=(1<<64)-1;
	for(int i=1;i<=n;i++)a[i]=read();
	now=a[1];dfs(2);
	if(ans!=0){now=~a[1];dfs(2);}
	cout<<ans<<endl;
}
int main()
{
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	int Case;
	scanf("%d",&Case);
	while(Case--)work();
	return 0;
}
