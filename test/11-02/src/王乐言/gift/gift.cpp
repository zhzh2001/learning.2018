#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <bits/stdc++.h>
#define ll long long
#define file "gift"
using namespace std;
const int M=2e5+1000;
struct edge{
	int u,v;
	ll t;
}e[M];
int read(){
	char c;int num,f=1;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;num=c-'0';
	while(c=getchar(), isdigit(c))num=num*10+c-'0';
	return f*num;
}
int n,m,tmp[M*3],cnt;
ll f[M*3];
ll tree[M*20];
bool cmp1(int a,int b){return a<b;}
bool cmp(edge a,edge b){return a.u<b.u;}
void update(int rt){
	tree[rt]=min(tree[rt<<1],tree[rt<<1|1]);
}
void build(int l,int r,int rt){
	if(l>r)return ;
	if(l==r){tree[rt]=f[l];return;}
	int mid=(l+r)>>1;
	build(l,mid,rt<<1);
	if(mid+1<=r)build(mid+1,r,rt<<1|1);
	update(rt);
}
ll ask(int l,int r,int L,int R,int rt){
	if(l<=L&&R<=r){return tree[rt];}
	int mid=(L+R)>>1;
	ll ans=1ll<<61;
	if(mid>=l)ans=min(ans,ask(l,r,L,mid,rt<<1));
	if(mid+1<=r)ans=min(ans,ask(l,r,mid+1,R,rt<<1|1));
	return ans;
}
void change(int x,int L,int R,int rt,ll w){
	if(L==R){tree[rt]=w;return ;}
	int mid=(L+R)>>1;
	if(mid>=x)change(x,L,mid,rt<<1,w);
	else change(x,mid+1,R,rt<<1|1,w);
	update(rt);
}
int fd(int x){
	int l=1,r=cnt,mid;
	while(l<=r){
		mid=(l+r)>>1;
		if(tmp[mid]==x)return mid;
		if(tmp[mid]>x)r=mid-1;
		else l=mid+1;
	}
}
void work(){
	cnt=0;n=read();tmp[++cnt]=m=read();
	tmp[++cnt]=1;
	for(int i=1;i<=n;i++){
		tmp[++cnt]=e[i].v=read();
		tmp[++cnt]=e[i].u=read();
		e[i].t=read();
		if(e[i].u>=e[i].v){
			n--;i--;
			continue;
		}	
	}
	sort(tmp+1,tmp+1+cnt,cmp1);
	sort(e+1,e+1+n,cmp);
	cnt=unique(tmp+1,tmp+1+cnt)-(tmp+1);
	for(int i=1;i<=n;i++){
		e[i].v=fd(e[i].v);
		e[i].u=fd(e[i].u);
	}
	memset(f,0x3f,sizeof(f));
	m=fd(m);f[1]=0;
	build(1,cnt,1);
	//cout<<tree[3]<<endl;
	//cout<<ask(3,cnt,1,cnt,1)<<endl;
	ll minn;
	for(int i=1;i<=n;i++){
		minn=(1<<31)-1;
		minn=ask(e[i].u,cnt,1,cnt,1);
		if(minn+e[i].t<f[e[i].v]){
			f[e[i].v]=minn+e[i].t;
			change(e[i].v,1,cnt,1,minn+e[i].t);
		}
	}
	minn=(1<<31)-1;
	minn=ask(m,cnt,1,cnt,1);
	printf("%d\n",((minn<(1ll<<61))?minn:-1));
}
int main()
{
	freopen(file".in","r",stdin);
	freopen(file".out","w",stdout);
	int Case=read();
	while(Case--)work();
	return 0;
}
/* 发现每次换钱必须变得更多，那么先去除那些变少的边。
 * 可以离散化。
 * 我们对起点进行排序，然后升序跑一遍dp。 
 * 考虑线段树维护最小值，单点修改。 
 * f[v]=min{min{f[u]}+t}; 
 * 时间复杂度
 * 离散化nlogn，线段树logn。  
 * 循环n
 * 总复杂度O(nlogn) 
 */ 
