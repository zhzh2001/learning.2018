#include <iostream>
#include <random>
#include <algorithm>
using namespace std;
const int N = 15, INF = 1e9;
int mat[N][N], p[N], d[N];
int main()
{
	minstd_rand gen(19260817);
	int n, m;
	cin >> n >> m;
	fill_n(&mat[0][0], sizeof(mat) / sizeof(int), INF);
	while (m--)
	{
		int u, v, w;
		cin >> u >> v >> w;
		if (w < mat[u][v])
			mat[u][v] = mat[v][u] = w;
	}
	for (int i = 1; i <= n; i++)
		p[i] = i;
	int ans = INF;
	for (int t = 1; t <= 10000; t++)
	{
		shuffle(p + 1, p + n + 1, gen);
		d[p[1]] = 1;
		int now = 0;
		for (int i = 2; i <= n; i++)
		{
			int j = 0;
			for (int k = 1; k < i; k++)
				if (!j || mat[p[k]][p[i]] < mat[p[j]][p[i]])
					j = k;
			if (!j || mat[p[j]][p[i]] == INF)
			{
				now = INF;
				break;
			}
			now += mat[p[j]][p[i]] * d[p[j]];
			d[p[i]] = d[p[j]] + 1;
		}
		ans = min(ans, now);
	}
	cout << ans << endl;
	return 0;
}