#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("treasure.in");
ofstream fout("treasure.ans");
const int N = 15, INF = 1e9;
int n, m, mat[N][N], d[N], ans;
void dfs(int k, int now)
{
	if (now > ans)
		return;
	if (k == n)
		ans = now;
	for (int i = 1; i <= n; i++)
		if (d[i] < INF)
			for (int j = 1; j <= n; j++)
				if (mat[i][j] && d[j] == INF)
				{
					d[j] = d[i] + 1;
					dfs(k + 1, now + mat[i][j] * (d[i] + 1));
					d[j] = INF;
				}
}
int main()
{
	fin >> n >> m;
	while (m--)
	{
		int u, v, w;
		fin >> u >> v >> w;
		if (!mat[u][v] || w < mat[u][v])
			mat[u][v] = mat[v][u] = w;
	}
	ans = INF;
	for (int i = 1; i <= n; i++)
	{
		fill(d + 1, d + n + 1, INF);
		d[i] = 0;
		dfs(1, 0);
	}
	fout << ans << endl;
	return 0;
}