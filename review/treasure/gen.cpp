#include <fstream>
#include <random>
#include <ctime>
using namespace std;
ofstream fout("treasure.in");
const int n = 5, m = 15;
int main()
{
	minstd_rand gen(time(NULL));
	fout << n << ' ' << m << endl;
	for (int i = 1; i <= m;)
	{
		uniform_int_distribution<> d(1, n);
		int u = d(gen), v = d(gen);
		if (u != v)
		{
			uniform_int_distribution<> dw1(1, 100000), dw2(1, 5), bin(0, 1);
			int w;
			if (bin(gen))
				w = dw1(gen);
			else
				w = dw2(gen);
			fout << u << ' ' << v << ' ' << w << endl;
			i++;
		}
	}
	return 0;
}