#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("treasure.in");
ofstream fout("treasure.out");
const int N = 12, INF = 1e9;
int mat[N][N], f[1 << N], d[1 << N][N];
int main()
{
	int n, m;
	fin >> n >> m;
	while (m--)
	{
		int u, v, w;
		fin >> u >> v >> w;
		u--;
		v--;
		if (!mat[u][v] || w < mat[u][v])
			mat[u][v] = mat[v][u] = w;
	}
	int ans = INF;
	for (int i = 0; i < n; i++)
	{
		fill(f, f + (1 << n), INF);
		f[1 << i] = 0;
		for (int j = 0; j < n; j++)
			d[1 << i][j] = i == j ? 0 : INF;
		for (int j = 0; j < (1 << n) - 1; j++)
			for (int k = 0; k < n; k++)
				if (j & (1 << k))
					for (int v = 0; v < n; v++)
						if (mat[k][v] && d[j][v] == INF && f[j] + (d[j][k] + 1) * mat[k][v] < f[j | (1 << v)])
						{
							f[j | (1 << v)] = f[j] + (d[j][k] + 1) * mat[k][v];
							for (int u = 0; u < n; u++)
								d[j | (1 << v)][u] = u == v ? d[j][k] + 1 : d[j][u];
						}
		ans = min(ans, f[(1 << n) - 1]);
	}
	fout << ans << endl;
	return 0;
}