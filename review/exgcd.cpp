#include <iostream>
using namespace std;
int exgcd(int a, int b, int &x, int &y)
{
	if (!b)
	{
		x = 1;
		y = 0;
		return a;
	}
	int ret = exgcd(b, a % b, x, y), t = x;
	x = y;
	y = t - a / b * x;
	return ret;
}
int main()
{
	int a, b;
	while (cin >> a >> b)
	{
		int x, y;
		cout << exgcd(a, b, x, y) << ' ' << x << ' ' << y << endl;
	}
	return 0;
}