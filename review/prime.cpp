#include <iostream>
using namespace std;
const int N = 1000005;
int p[N / 10], minp[N], phi[N];
int main()
{
	int n;
	cin >> n;
	int pn = 0;
	for (int i = 2; i <= n; i++)
	{
		if (!minp[i])
		{
			p[++pn] = i;
			minp[i] = i;
			phi[i] = i - 1;
		}
		for (int j = 1; j <= pn && i * p[j] <= n; j++)
		{
			minp[i * p[j]] = p[j];
			if (i % p[j] == 0)
			{
				phi[i * p[j]] = phi[i] * p[j];
				break;
			}
			else
				phi[i * p[j]] = phi[i] * (p[j] - 1);
		}
	}
	for (int i = 1; i <= pn; i++)
		cout << p[i] << ' ';
	cout << endl;
	for (int i = 2; i <= n; i++)
		cout << minp[i] << ' ';
	cout << endl;
	for (int i = 2; i <= n; i++)
		cout << phi[i] << ' ';
	cout << endl;
	return 0;
}