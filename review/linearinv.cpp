#include <iostream>
using namespace std;
const int N = 1000005;
int fact[N], inv[N];
int qpow(long long a, int b, int p)
{
	long long ans = 1;
	do
	{
		if (b & 1)
			ans = ans * a % p;
		a = a * a % p;
	} while (b /= 2);
	return ans;
}
int main()
{
	int n, p;
	cin >> n >> p;
	inv[1] = 1;
	for (int i = 2; i <= n; i++)
		inv[i] = 1ll * (p - p / i) * inv[p % i] % p;
	for (int i = 1; i <= n; i++)
		cout << inv[i] << ' ';
	cout << endl;
	fact[0] = 1;
	for (int i = 1; i <= n; i++)
		fact[i] = 1ll * fact[i - 1] * i % p;
	inv[n] = qpow(fact[n], p - 2, p);
	for (int i = n - 1; i >= 0; i--)
		inv[i] = 1ll * inv[i + 1] * (i + 1) % p;
	for (int i = 1; i <= n; i++)
		cout << 1ll * fact[i - 1] * inv[i] % p << ' ';
	cout << endl;
	return 0;
}