#include <iostream>
using namespace std;
int gcd(int a, int b)
{
	return a % b ? gcd(b, a % b) : b;
}
int phi(int n)
{
	int ans = n;
	for (int i = 2; i * i <= n; i++)
		if (n % i == 0)
		{
			while (n % i == 0)
				n /= i;
			ans = ans / i * (i - 1);
		}
	if (n > 1)
		ans = ans / n * (n - 1);
	return ans;
}
int main()
{
	int n;
	while (cin >> n)
	{
		int ans = 0;
		for (int i = 1; i <= n; i++)
			ans += gcd(i, n) == 1;
		cout << ans << endl
			 << phi(n) << endl;
	}
	return 0;
}