#include <iostream>
#include <gmpxx.h>
using namespace std;
int main()
{
	int n, m, p;
	cin >> n >> m >> p;
	mpz_class ans;
	mpz_bin_ui(ans.get_mpz_t(), mpz_class(n).get_mpz_t(), m);
	cout << ans % p << endl;
	return 0;
}