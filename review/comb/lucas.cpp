#include <iostream>
using namespace std;
const int P = 1000005;
int fact[P], inv[P];
int qpow(long long a, int b, int p)
{
	long long ans = 1;
	do
	{
		if (b & 1)
			ans = ans * a % p;
		a = a * a % p;
	} while (b /= 2);
	return ans;
}
int comb(int n, int m, int p)
{
	if (m > n)
		return 0;
	return 1ll * fact[n] * inv[n - m] * inv[m] % p;
}
int lucas(long long n, long long m, int p)
{
	if (!n)
		return 1;
	return 1ll * comb(n % p, m % p, p) * lucas(n / p, m / p, p) % p;
}
int main()
{
	long long n, m;
	int p;
	cin >> n >> m >> p;
	fact[0] = 1;
	for (int i = 1; i < p; i++)
		fact[i] = 1ll * fact[i - 1] * i % p;
	inv[p - 1] = qpow(fact[p - 1], p - 2, p);
	for (int i = p - 2; i >= 0; i--)
		inv[i] = 1ll * inv[i + 1] * (i + 1) % p;
	cout << lucas(n, m, p) << endl;
	return 0;
}