#include <iostream>
using namespace std;
const int N = 1e6, MOD = 1e9 + 7;
int fact[N + 5], inv[N + 5];
int qpow(long long a, int b)
{
	long long ans = 1;
	do
	{
		if (b & 1)
			ans = ans * a % MOD;
		a = a * a % MOD;
	} while (b /= 2);
	return ans;
}
int comb(int n, int m)
{
	return 1ll * fact[n] * inv[n - m] % MOD * inv[m] % MOD;
}
int main()
{
	fact[0] = 1;
	for (int i = 1; i <= N; i++)
		fact[i] = 1ll * fact[i - 1] * i % MOD;
	inv[N] = qpow(fact[N], MOD - 2);
	for (int i = N - 1; i >= 0; i--)
		inv[i] = 1ll * inv[i + 1] * (i + 1) % MOD;
	int n, m;
	while (cin >> n >> m)
		cout << comb(n, m) << endl;
	return 0;
}