#include <iostream>
using namespace std;
const int N = 1e6;
int p[N / 10], minp[N + 5], cnt[N + 5];
int qpow(long long a, int b, int p)
{
	long long ans = 1;
	do
	{
		if (b & 1)
			ans = ans * a % p;
		a = a * a % p;
	} while (b /= 2);
	return ans;
}
int main()
{
	int pn = 0;
	for (int i = 2; i <= N; i++)
	{
		if (!minp[i])
			p[++pn] = minp[i] = i;
		for (int j = 1; j <= pn && i * p[j] <= N; j++)
		{
			minp[i * p[j]] = p[j];
			if (i % p[j] == 0)
				break;
		}
	}
	int n, m, mod;
	cin >> n >> m >> mod;
	for (int i = n - m + 1; i <= n; i++)
		cnt[i]++;
	for (int i = 1; i <= m; i++)
		cnt[i]--;
	for (int i = n; i > 1; i--)
		if (minp[i] < i)
		{
			cnt[minp[i]] += cnt[i];
			cnt[i / minp[i]] += cnt[i];
		}
	int ans = 1;
	for (int i = 1; i <= pn && p[i] <= n; i++)
		ans = 1ll * ans * qpow(p[i], cnt[p[i]], mod) % mod;
	cout << ans << endl;
	return 0;
}