#include <iostream>
using namespace std;
int main(int argc, char *argv[])
{
	long long x;
	if (argc > 1)
		x = atoll(argv[1]);
	else
		cin >> x;
	for (long long i = 2; i * i <= x; i++)
		if (x % i == 0)
		{
			int cnt = 0;
			while (x % i == 0)
			{
				x /= i;
				cnt++;
			}
			cout << i << '^' << cnt << ' ';
		}
	if (x > 1)
		cout << x << '^' << 1 << endl;
	else
		cout << endl;
	system("pause");
	return 0;
}
